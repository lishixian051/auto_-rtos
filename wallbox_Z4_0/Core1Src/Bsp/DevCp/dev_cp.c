/********************************************************************************************/
/* 	\file			dev_led.c
 *	\date
 *	\author
 *	\model
 *	\description
 *	\version
 *
 *********************************************************************************************
 *	\par	Revision History:
 *	<!-----	No.		Date		Revised by		Details	------------------------------------->
 *
 ********************************************************************************************/

/********************************************************************************************/
/*							Include File Section											*/
/********************************************************************************************/
#include <string.h>
#include <stdbool.h>
#include "TypeDef.h"
#include "rtos.h"
#include "rtos_config.h"

#include "interrupt_manager.h"
#include "pit.h"
#include "pit_driver.h"
#include "pins_driver.h"


#include "eMIOS_Mc1.h"
#include "eMIOS_Pwm1.h"
#include "eMIOS_Ic1.h"
#include "emios_common.h"
#include "emios_ic_driver.h"
#include "emios_mc_driver.h"

#include "adc_sar_driver.h"
#include "adConv0.h"
#include "adConv1.h"

#include "dev_cp.h"

/********************************************************************************************/
/*							Macro Definition Section										*/
/********************************************************************************************/

/********************************************************************************************/
/*							Type Definition Section											*/
/********************************************************************************************/

/********************************************************************************************/
/*							Enumeration Type Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Structure/Union Type Definition Section							*/
/********************************************************************************************/

/********************************************************************************************/
/*							Global Variable Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Static Variable Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Static Prototype Declaration Section							*/
/********************************************************************************************/

/********************************************************************************************/
/*							ROM Table Section												*/
/********************************************************************************************/

/********************************************************************************************/
/*							Function Definition Section										*/
/********************************************************************************************/

void vdCpDevice_Init(void){
    /*ADC1_Scanf*/
    ADC_DRV_ChainConfig(INST_ADCONV1,&adConv1_ChainCfg0);
    ADC_DRV_StartConversion(INST_ADCONV1,ADC_CONV_CHAIN_NORMAL);

    /* Disable eMIOS Global */
    //EMIOS_DRV_DisableGlobalEmios(INST_EMIOS_MC1);
	/*CLockBusASet*/
    EMIOS_DRV_MC_InitCounterMode(INST_EMIOS_MC1, EMIOS_CNT_BUSA_DRIVEN, &eMIOS_Mc1_CntChnConfig0);
    /*PWM_Out*/
    EMIOS_DRV_PWM_InitMode(INST_EMIOS_MC1, EMIOS_PWM1_CHANNEL0,&eMIOS_Pwm1_PWMChnConfig0);
    /*PWM_Capture*/
    EMIOS_DRV_IC_InitInputCaptureMode(INST_EMIOS_MC1,EMIOS_IC1_CHANNEL15,&eMIOS_Ic1_ICChnConfig0);
    /* Enable eMIOS Global */
    EMIOS_DRV_EnableGlobalEmios(INST_EMIOS_MC1);
    /*PWM Set*/
    //EMIOS_DRV_PWM_SetDutyCycle(INST_EMIOS_MC1,EMIOS_PWM1_CHANNEL0,EMIOS_DRV_PWM_GetPeriod(INST_EMIOS_MC1,EMIOS_PWM1_CHANNEL0)/5);


    
}

#if 0
static void vdCpDevice_EMIOS_Callback(void){

	portNOP();
}
#endif





