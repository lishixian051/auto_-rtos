/******************************************************************************
 *	The System Information Header File for TELECHIPD_TCC803X [V0.01]
 *
 *
 *****************************************************************************/

#ifndef	__rtos_h__
#define	__rtos_h__

/********************************************************************************************/
/* 						       Include File Section											 */
/********************************************************************************************/
#include "TypeDef.h"
/********************************************************************************************/
/*							Macro Definition Section										*/
/********************************************************************************************/
#define RTOS_TIMEOUT_FOREVER        (0)

/*RTOS Error Define*/
#define RTOS_ERR_OTHER              (2)         // other
#define RTOS_ERR_NG                 (1)         // ng
#define RTOS_ERR_NONE               (0)         // ok
#define RTOS_ERR_INIT               (-1)        // initialization error
#define RTOS_ERR_NO_SPACE           (-2)        // need more space(memory, channel.etc.)
#define RTOS_ERR_INVALID_PARAMETER  (-3)        // passed invalid parameter
#define RTOS_ERR_INIT_TASK          (-4)        // error occurred in task api
#define RTOS_ERR_DEL                (-5)        // error occurred deleting loop		
#define RTOS_ERR_QUEUE              (-6)        // error occurred in queue api		
#define RTOS_ERR_SEMA               (-7)        // error occurred in semaphore api
#define RTOS_ERR_EVENT              (-8)        // error occurred in event api
#define RTOS_ERR_NOT_SUPPORT        (-9)        // not supported
#define RTOS_ERR_TIMEOUT            (-10)       // timed out error
#define RTOS_ERR_INVALID_HANDLE     (-11)       // invalid handle
#define RTOS_ERR_NO_DATA            (-12)       // no data
#define RTOS_ERR_UNDEF_STATE        (-13)       // not defined state

/* Task Option */
#define RTOS_EVENT_OPT_OR           (1)
#define RTOS_EVENT_OPT_AND          (2)

/* Task ID */
#define TSK_ID_MAX                  (3)
#define TSK_ID_TEST01_APP           (0)
#define TSK_ID_TEST02_APP           (1)
#define TSK_ID_TEST03_APP           (2)

/* Queue ID */
#define QUEUE_ID_MAX                (3)
#define QUEUE_ID_TEST01_APP         (0)
#define QUEUE_ID_TEST02_APP         (1)
#define QUEUE_ID_TEST03_APP         (2)


/* Eventflag ID */
#define EVENT_ID_MAX                (0)
#define EVENT_ID_TEST_APP           (0)


/* Semaphore ID */
#define SEMA_ID_MAX                 (0)
#define SEMA_ID_TEST_APP            (0)

/* Cyclic handler ID */
#define CYC_ID_MAX                  (3)
#define CYC_ID_TEST01_APP           (0)
#define CYC_ID_TEST02_APP           (1)
#define CYC_ID_TEST03_APP           (2)


/********************************************************************************************/
/*							Type Definition Section											*/
/********************************************************************************************/

/********************************************************************************************/
/*							Enumeration Type Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Structure/Union Type Definition Section							*/
/********************************************************************************************/

/********************************************************************************************/
/*							Extern Declaration												*/
/********************************************************************************************/

/********************************************************************************************/
/*							Global Function Prototype Declaration							*/
/********************************************************************************************/
void    RTOS_Init(void);
void    RTOS_TaskDelay(uint32 millisecond);
uint64  RTOS_GetSystemTick(void);
uint64  RTOS_GetSystemTick_uS(void);
void    RTOS_DI(void);
void    RTOS_EI(void);
int32   RTOS_QueuePut(uint32 u32QueueID,void* vdData_p,uint32 u2Size,uint32 u32Timeout);
int32   RTOS_QueueGet(uint32 u32QueueID,void* vdData_p,uint32 u2Size,uint32 u32Timeout);
int32   RTOS_EventSet(uint32 u32ID,uint32 u32Event);
int32   RTOS_EventGet(uint32 u32ID,uint32 u32WaitEvent,uint32* u32Event_p,uint32 u32Option,uint32 u32Timeout);
int32   RTOS_EventClr(uint32 u32ID,uint32 u32Event);
int32   RTOS_SemaphoreObtain( uint32 u32ID, uint32 u32Timeout);
int32   RTOS_SemaphoreRelease(uint32 u32ID);
void    RTOS_MemSet(void *pmem,uint8 val,uint32 size);
void    RTOS_MemCopy( void *pdest,const void *psrc,uint32 size);
int32   RTOS_MemCmp(const void *pmem1, const void *pmem2, uint32 size);
int8 *  RTOS_StrCopy(uint8 *dest_str,const uint8 *src_srt);
int16   RTOS_StrCmp(const uint8 *p1,const uint8 *p2);
int16   RTOS_StrNCmp(const uint8 *p1,const uint8 *p2,int32 len);
uint32  RTOS_StrLen(const uint8 *pstr);


#endif	/* __rtos_h__ */
/* EOF */
