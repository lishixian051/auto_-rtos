/******************************************************************************
 *	The System Information Header File for TELECHIPD_TCC803X [V0.01]
 *
 *
 *****************************************************************************/

#ifndef	__rtos_config_h__
#define	__rtos_config_h__

/********************************************************************************************/
/* 						       Include File Section											 */
/********************************************************************************************/
#include "TypeDef.h"
#include "rtos.h"

/********************************************************************************************/
/*							Macro Definition Section										*/
/********************************************************************************************/
#if 0
#define TASK_STK_TOTAL              (4096u)     /*(16KBytes)*/
#define TASK_STK_RESERVED_INTERVAL  (32u)       /*(128Bytes)*/
#else
#define	SET_STK_SIZE(size)	        ((size + sizeof(uint32_t) - 1) / sizeof(uint32_t))
#define TASK_STK_TOTAL              SET_STK_SIZE(8192 * TSK_ID_MAX * 2 * 2)
#define TASK_STK_RESERVED_INTERVAL  SET_STK_SIZE(0)
#endif
#define TASK_ID_NAME_LENGTH         (16u)       /*Must Multiple of 4Bytes(sizeof(int))*/
#define QUEUE_ID_NAME_LENGTH        (16u)       /*Must Multiple of 4Bytes(sizeof(int))*/
#define EVENT_ID_NAME_LENGTH        (16u)       /*Must Multiple of 4Bytes(sizeof(int))*/
#define SEMA_ID_NAME_LENGTH         (16u)       /*Must Multiple of 4Bytes(sizeof(int))*/

/********************************************************************************************/
/*							Type Definition Section											*/
/********************************************************************************************/
typedef void * CycHandle_t;
typedef void (*CycCbFunc_t)( CycHandle_t );

/********************************************************************************************/
/*							Enumeration Type Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Structure/Union Type Definition Section							*/
/********************************************************************************************/
typedef struct Set_Tsk_Info_s{
    uint32      u32ID;
    int8        i8Name[TASK_ID_NAME_LENGTH];
    const void *vdFunc;
    uint32      u32StackSize;           /*Multiple of 4Bytes(1KBytes=256)*/
    uint32      u32Priority;            /*0~15*/
}Set_Tsk_Info_ts;
typedef struct Set_Cycle_Info_s{
    uint32          u32ID;
    int8            *i8Name_p;
    uint32          u32Period;
    uint32          u32Reload;
    void            (*Cb_Func_p)(void);
}Set_Cycle_Info_ts;
typedef struct	Set_Event_Info_s{
    uint32      u32ID;
    int8        i8Name[EVENT_ID_NAME_LENGTH];
}Set_Event_Info_ts;
typedef struct Set_Queue_Info_s{
    uint32      u32ID;
    int8        i8Name[QUEUE_ID_NAME_LENGTH];
    uint32      u32Depth;               /**/
    uint32      u32Size;                /**/
}Set_Queue_Info_ts;
typedef struct Set_Sema_Info_s{
    uint32      u32ID;
    int8        i8Name[SEMA_ID_NAME_LENGTH];
    uint32      u32Init;               /**/
}Set_Sema_Info_ts;

/********************************************************************************************/
/*							Extern Declaration												*/
/********************************************************************************************/
extern const Set_Tsk_Info_ts stSetTskInfoTbl[TSK_ID_MAX];
extern const Set_Cycle_Info_ts stSetCycleInfoTbl[CYC_ID_MAX];
extern const Set_Event_Info_ts stSetEventInfoTbl[EVENT_ID_MAX];
extern const Set_Queue_Info_ts stSetQueueInfoTbl[QUEUE_ID_MAX];
extern const Set_Sema_Info_ts stSetSemaInfoTbl[SEMA_ID_MAX];

/********************************************************************************************/
/*							Global Function Prototype Declaration							*/
/********************************************************************************************/
int32   RTOS_TaskCreate(void);
int32   RTOS_QueueCreate(void);
int32   RTOS_EventCreate(void);
int32   RTOS_SemaphoreCreate(void);
int32   RTOS_CycleCreate(void);


#endif	/* __rtos_config_h__ */
/* EOF */
