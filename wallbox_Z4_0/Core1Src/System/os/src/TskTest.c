
#include <stdio.h> 
#include "rtos.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"


#include "adc_sar_driver.h"
#include "adConv0.h"
#include "adConv1.h"

#include "eMIOS_Mc1.h"
#include "eMIOS_Pwm1.h"
#include "eMIOS_Ic1.h"
#include "emios_common.h"
#include "emios_ic_driver.h"
#include "emios_mc_driver.h"

#include "Mcu.h"

void TSK_Test01(void * exinf)
{
    int32 i32Rtn = RTOS_ERR_NONE;
    uint32 u32Data[3] = {0,0,0};
    uint32 u32Pluse = 0;
    //RTOS_TaskDelay(1000);
    while(1)
    {
        i32Rtn = RTOS_QueueGet(QUEUE_ID_TEST01_APP,u32Data,sizeof(u32Data),0);
        switch(i32Rtn){
            case RTOS_ERR_NONE:
                portNOP();
                break;
            case RTOS_ERR_QUEUE:
                portNOP();
                break;
            default:
                portNOP();
                break;
        }
        adc_chan_result_t adc_chan_result[8];
        ADC_DRV_GetConvInfoToArray(INST_ADCONV1,ADC_CONV_CHAIN_NORMAL,adc_chan_result,8);
        //printf("ADC[%d,%d,%d,]\r\n",adc_chan_result[3].chnIdx,adc_chan_result[3].valid,adc_chan_result[3].cdata);
        
        i32Rtn = EMIOS_DRV_IC_GetLastMeasurement(INST_EMIOS_MC1,EMIOS_IC1_CHANNEL15,(uint32_t * const)&u32Pluse);
        printf("Pluse,%d,\r\n",u32Pluse);
        //EMIOS_DRV_PWM_SetDutyCycle(INST_EMIOS_MC1,EMIOS_PWM1_CHANNEL0,EMIOS_DRV_PWM_GetPeriod(INST_EMIOS_MC1,EMIOS_PWM1_CHANNEL0));
        //EMIOS_DRV_PWM_SetDutyCycle(INST_EMIOS_MC1,EMIOS_PWM1_CHANNEL0,0);

        //printf("ADC[%d,%d,%d,]\r\n",adc_chan_result[4].chnIdx,adc_chan_result[4].validadc_chan_result[4].cdata);
        //printf("ADC0[%d,%d,%d,%d,]\r\n",adc_chan_result[0].chnIdx,adc_chan_result[0].valid,adc_chan_result[0].overWritten,adc_chan_result[0].cdata);
        //printf("ADC1[%d,%d,%d,%d,]\r\n",adc_chan_result[1].chnIdx,adc_chan_result[1].valid,adc_chan_result[1].overWritten,adc_chan_result[1].cdata);
        //printf("ADC2[%d,%d,%d,%d,]\r\n",adc_chan_result[2].chnIdx,adc_chan_result[2].valid,adc_chan_result[2].overWritten,adc_chan_result[2].cdata);
        //printf("ADC3[%d,%d,%d,%d,]\r\n",adc_chan_result[3].chnIdx,adc_chan_result[3].valid,adc_chan_result[3].overWritten,adc_chan_result[3].cdata);
        //printf("ADC4[%d,%d,%d,%d,]\r\n",adc_chan_result[4].chnIdx,adc_chan_result[4].valid,adc_chan_result[4].overWritten,adc_chan_result[4].cdata);
        //printf("ADC5[%d,%d,%d,%d,]\r\n",adc_chan_result[5].chnIdx,adc_chan_result[5].valid,adc_chan_result[5].overWritten,adc_chan_result[5].cdata);
        //printf("ADC6[%d,%d,%d,%d,]\r\n",adc_chan_result[6].chnIdx,adc_chan_result[6].valid,adc_chan_result[6].overWritten,adc_chan_result[6].cdata);
        //printf("ADC7[%d,%d,%d,%d,]\r\n",adc_chan_result[7].chnIdx,adc_chan_result[7].valid,adc_chan_result[7].overWritten,adc_chan_result[7].cdata);
        //printf("[%08d,%06d]11111\r\n",(uint32)(RTOS_GetSystemTick_uS()/1000000),(uint32)(RTOS_GetSystemTick_uS()%1000000));
        portNOP();
    }
}

void TSK_Test02(void * exinf)
{
    int32 i32Rtn = RTOS_ERR_NONE;
    uint32 u32Data[3] = {0,0,0};
    //RTOS_TaskDelay(1000);
    while(1)
    {
        while(1)
    {
        i32Rtn = RTOS_QueueGet(QUEUE_ID_TEST02_APP,u32Data,sizeof(u32Data),0);
        switch(i32Rtn){
            case RTOS_ERR_NONE:
                portNOP();
                break;
            case RTOS_ERR_QUEUE:
                portNOP();
                break;
            default:
                portNOP();
                break;
        }
        //printf("[%08d,%06d]22222\r\n",(uint32)(RTOS_GetSystemTick_uS()/1000000),(uint32)(RTOS_GetSystemTick_uS()%1000000));
        portNOP();
    }
    }
}

void TSK_Test03(void * exinf)
{
    int32 i32Rtn = RTOS_ERR_NONE;
    uint32 u32Data[3] = {0,0,0};
    //RTOS_TaskDelay(1000);
    while(1)
    {
        i32Rtn = RTOS_QueueGet(QUEUE_ID_TEST03_APP,u32Data,sizeof(u32Data),0);
        switch(i32Rtn){
            case RTOS_ERR_NONE:
                portNOP();
                break;
            case RTOS_ERR_QUEUE:
                portNOP();
                break;
            default:
                portNOP();
                break;
        }
        //printf("[%08d,%06d]33333\r\n",(uint32)(RTOS_GetSystemTick_uS()/1000000),(uint32)(RTOS_GetSystemTick_uS()%1000000));
        portNOP();
    }
}


void CYC_Test01(void){
    uint32 u32Data[3] = {0,0,0};
    RTOS_QueuePut(QUEUE_ID_TEST01_APP,u32Data,sizeof(u32Data),0);
    portNOP();

    Test_10ms_task();
}

void CYC_Test02(void){
    uint32 u32Data[3] = {0,0,0};
    RTOS_QueuePut(QUEUE_ID_TEST02_APP,u32Data,sizeof(u32Data),0);
    portNOP();
}

void CYC_Test03(void){
    uint32 u32Data[3] = {0,0,0};
    RTOS_QueuePut(QUEUE_ID_TEST03_APP,u32Data,sizeof(u32Data),0);
    Test_1ms_task();
    portNOP();
}

