/*********************************************************************************
 * Copyright(C),Ningbo Joyson Preh Car Connect Co.,Ltd.
 * FileName:    TypeDef.h
 * Author:      MengLinglei 
 * Version :    Ver0.1
 * Date:        2019-03-18
 * Description: Common definition
 *
 *********************************************************************************/

#ifndef _TYPEDEF_H
#define _TYPEDEF_H

/*********************************************************************************
 *                      Include                                                  *
**********************************************************************************/
#include "Platform_Types.h"
/*********************************************************************************
 *                      Macro                                                    *
**********************************************************************************/

/*********************************************************************************
 *                      Defien                                                   *
**********************************************************************************/
/* System related definitions. */
#ifndef     TRUE
#define		TRUE	(1)
#endif

#ifndef     FALSE
#define		FALSE	(0)
#endif

#ifndef     NULL
#define		NULL	(0)
#endif

#define ARRAY_COUNT_OF(x)       ((sizeof(x)) / (sizeof(x[0])))
//#define     WHOLE_VEHICLE_GUARD
/*********************************************************************************
 *                      Typedef/Struct                                           *
**********************************************************************************/
typedef char int8;
typedef short int16;
typedef int int32;
typedef long long int64;


typedef unsigned int addr_t;
typedef unsigned int vaddr_t;
typedef unsigned int paddr_t;
/* Type definitions. */
typedef signed char Type_sByte;
typedef short int   Type_sHWord;
typedef long int    Type_sWord;

typedef unsigned char       Type_uByte;
typedef unsigned short int  Type_uHWord;
typedef unsigned long int   Type_uWord;

#endif  /* _TYPEDEF_H */
