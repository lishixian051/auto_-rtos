/********************************************************************************************/
/* 	\file			rtos_config.c
 *	\date			
 *	\author			
 *	\model			
 *	\description	
 *	\version
 *
 *********************************************************************************************
 *	\par	Revision History:
 *	<!-----	No.		Date		Revised by		Details	------------------------------------->
 *
 ********************************************************************************************/

/********************************************************************************************/
/*							Include File Section											*/
/********************************************************************************************/
#include "TypeDef.h"
#include "rtos.h"
#include "rtos_config.h"
#include "TskTest.h"
/********************************************************************************************/
/*							Macro Definition Section										*/
/********************************************************************************************/

/********************************************************************************************/
/*							Type Definition Section											*/
/********************************************************************************************/

/********************************************************************************************/
/*							Enumeration Type Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Structure/Union Type Definition Section							*/
/********************************************************************************************/

/********************************************************************************************/
/*							Global Variable Definition Section								*/
/********************************************************************************************/

/********************************************************************************************/
/*							Static Variable Definition Section								*/
/********************************************************************************************/
#if 0
const Set_Tsk_Info_ts stSetTskInfoTbl[TSK_ID_MAX]={
	{TSK_ID_TEST01_APP,     "TSK_TEST01_APP",   (const void *)TSK_Test01,   512,    11},
	{TSK_ID_TEST02_APP,     "TSK_TEST02_APP",   (const void *)TSK_Test02,   512,    12},
	{TSK_ID_TEST03_APP,     "TSK_TEST03_APP",   (const void *)TSK_Test03,   512,    13},
};
#else


//const Set_Tsk_Info_ts stSetTskInfoTbl[TSK_ID_MAX]={
//	{TSK_ID_TEST01_APP,     "TSK_TEST01_APP",   (const void *)TSK_Test01,   2048,    11},
//	{TSK_ID_TEST02_APP,     "TSK_TEST02_APP",   (const void *)TSK_Test02,   2048,    12},
//	{TSK_ID_TEST03_APP,     "TSK_TEST03_APP",   (const void *)TSK_Test03,   2048,    13},
//};

const Set_Tsk_Info_ts stSetTskInfoTbl[TSK_ID_MAX]={
	{TSK_ID_TEST01_APP,     "TSK_TEST01_APP",   (const void *)TSK_Test01,   4096,    11},
	{TSK_ID_TEST02_APP,     "TSK_TEST02_APP",   (const void *)TSK_Test02,   4096,    12},
	{TSK_ID_TEST03_APP,     "TSK_TEST03_APP",   (const void *)TSK_Test03,   4096,    13},
};

//const Set_Tsk_Info_ts stSetTskInfoTbl[TSK_ID_MAX]={
//	{TSK_ID_TEST01_APP,     "TSK_TEST01_APP",   (const void *)TSK_Test01,   5120,    11},
//	{TSK_ID_TEST02_APP,     "TSK_TEST02_APP",   (const void *)TSK_Test02,   5120,    12},
//	{TSK_ID_TEST03_APP,     "TSK_TEST03_APP",   (const void *)TSK_Test03,   5120,    13},
//};
#endif
const Set_Queue_Info_ts stSetQueueInfoTbl[QUEUE_ID_MAX]={
    {QUEUE_ID_TEST01_APP,   "QUEUE_TEST01_APP",     4,     12},
    {QUEUE_ID_TEST02_APP,   "QUEUE_TEST02_APP",     4,     12},
    {QUEUE_ID_TEST03_APP,   "QUEUE_TEST03_APP",     4,     12},
};

const Set_Event_Info_ts stSetEventInfoTbl[EVENT_ID_MAX]={
//    {EVENT_ID_KEYAPP,   "EVENT_KEY_APP"},
//    {EVENT_ID_STGAPP,   "EVENT_STG_APP"},
};

const Set_Sema_Info_ts stSetSemaInfoTbl[SEMA_ID_MAX]={
//    {SEMA_ID_STGAPP,    "SEMA_STG_APP",     1},
};

const Set_Cycle_Info_ts stSetCycleInfoTbl[CYC_ID_MAX]={
    {CYC_ID_TEST01_APP, "CYC_TEST01_APP",   10,   TRUE,    (void *)CYC_Test01},
    {CYC_ID_TEST02_APP, "CYC_TEST02_APP",   200,   TRUE,    (void *)CYC_Test02},
    {CYC_ID_TEST03_APP, "CYC_TEST02_APP",   1,   TRUE,    (void *)CYC_Test03},
};



/********************************************************************************************/
/*							Static Prototype Declaration Section							*/
/********************************************************************************************/

/********************************************************************************************/
/*							ROM Table Section												*/
/********************************************************************************************/

/********************************************************************************************/
/*							Function Definition Section										*/
/********************************************************************************************/





/* End of File */
