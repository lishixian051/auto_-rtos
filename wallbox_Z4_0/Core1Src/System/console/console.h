
#ifndef CONSOLE_H
#define CONSOLE_H

#include "ewl_misra_types.h"
#include "file_struc.h"

#define DRV_UART_BLOCK_TIME 500

int_t __read_console(__file_handle handle, uchar_t * buffer, size_t * count);
int_t __write_console(__file_handle handle, uchar_t * buffer, size_t * count);
int_t __close_console(__file_handle handle);
void InitializeConsole(void);

#endif

