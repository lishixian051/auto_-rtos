/*
 * console.c
 *
 */
#include "console.h"
#include "TypeDef.h"

#include <stdio.h>
#include <string.h>
#include "cpu.h"
#include "clockMan.h"
#include "pin_mux.h"
#include "task.h"
#include "rtos.h"

#define INST_LINFLEXD_UART_CONSOLE (6U)

int_t __read_console(__file_handle handle, uint8_t * buffer, size_t * count){
	//uint32_t bytesRemain;
    //uint32_t bytesRemaining;
	bool MsgDone=false;
	uchar_t new_lin[]={"\n"};
	status_t retValue = STATUS_SUCCESS;
	int i=0;
	return 0;								/****************************************************/
	//int buffersize = strlen((const int8 *)buffer)+1;
    
	while (MsgDone==false)
	{

		retValue = LINFLEXD_UART_DRV_ReceiveDataBlocking(INST_LINFLEXD_UART, &buffer[i], 1, DRV_UART_BLOCK_TIME);
		retValue += LINFLEXD_UART_DRV_SendDataBlocking(INST_LINFLEXD_UART, &buffer[i], 1, DRV_UART_BLOCK_TIME);
        /*do{
            portNOP();
        }
        while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART_CONSOLE,&bytesRemaining));*/
        if (retValue != STATUS_SUCCESS)
		{
			buffer[0]='\r';
			buffer[1]='\n';
			buffer[2]=0;
			*count = (size_t)3;
			return 0;
		}
		if(buffer[i++] == '\r')
		 {
		   buffer[i-1]='\n';
		   MsgDone = true;
		 }
	}

	retValue += LINFLEXD_UART_DRV_SendDataBlocking(INST_LINFLEXD_UART, new_lin, 1, DRV_UART_BLOCK_TIME);

    /*do{
        portNOP();
    }
    while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART_CONSOLE,&bytesRemaining));*/

    if (retValue )
	{
		buffer[0]='\r';
		buffer[1]='\n';
		buffer[2]=0;
		*count = (size_t)3;
		return 0;
	}

	buffer[i]=0;
	*count = (size_t)i;

	return 0;
}


int_t __write_console(__file_handle handle, uchar_t * buffer, size_t * count){
    uint32_t bytesRemaining;
    size_t bytes=*count;
    uchar_t ret_car[]={"\r"};
    status_t retValue = STATUS_SUCCESS;

    retValue = LINFLEXD_UART_DRV_SendData(INST_LINFLEXD_UART, (uint8_t *)buffer, bytes);
    do{
		portNOP();
	}
	while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART,&bytesRemaining));
     retValue += LINFLEXD_UART_DRV_SendData(INST_LINFLEXD_UART, (uint8_t *)ret_car, 1);
    do{
        portNOP();
    }
    while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART,&bytesRemaining));
    
    return 0;
}


int_t __close_console(__file_handle handle){
  return 0;
}

#define  CONSOLE_TASK_STK_SIZE  512

typedef void* HAL_HANDLE;

typedef	void (CmdFuncType)(uint8 argc, void *argv[]);

typedef struct COMMANDLIST_tag {
	uint8 *pucCommand;
	CmdFuncType *func;
	uint8 *pucRemark;
	struct COMMANDLIST_tag *pNext;
} COMMANDLIST;

typedef struct {
	int8 ServiceState;
	COMMANDLIST CommandListHead;
	HAL_HANDLE halHandle;
} CONSOLESERVICE_CONTEXT;



static void Tsk_Console(void * exinf){
	//uint32_t bytesRemaining;
    static uint8 data;
    //static uint8 data_index;
    static status_t rtn = STATUS_SUCCESS;
    //data_index = 0;
    memset(&data,0,sizeof(data));
    //RTOS_TaskDelay(1000);
    while(1){
        //__read_console(0,data,&length);
        rtn = LINFLEXD_UART_DRV_ReceiveDataBlocking(INST_LINFLEXD_UART,&data,1,OSIF_WAIT_FOREVER);
        if(rtn == STATUS_SUCCESS){
            portNOP();
            printf("%c \b",data);
        }
        portNOP();
        RTOS_TaskDelay(10);
        #if 0
            switch(data){
                case 0:
                    portNOP();
                    break;
                case 0x75:      //for putty
                case ('\b'):    //backspace
                    LINFLEXD_UART_DRV_SendData(INST_LINFLEXD_UART, (uint8_t *)data[data_index], 1);
                    do{
                        portNOP();
                    }
                    while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART,&bytesRemaining));
                    memset(&data[data_index],0,1);
                    break;
                case ('\n'):
                case ('\r'):
                    LINFLEXD_UART_DRV_SendData(INST_LINFLEXD_UART, (uint8_t *)data[0], (data_index + 1));
                    do{
                        portNOP();
                    }
                    while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART,&bytesRemaining));
                    //printf("[%s]\n",data);
                    memset(&data[0],0,sizeof(data));
                    data_index = 0;
                    
                    break;
                default:
                    LINFLEXD_UART_DRV_SendData(INST_LINFLEXD_UART, (uint8_t *)data[data_index], 1);
                    do{
                        portNOP();
                    }
                    while(LINFLEXD_UART_DRV_GetTransmitStatus(INST_LINFLEXD_UART,&bytesRemaining));
                    //printf("%c \b",data[data_index]);
                    data_index++;
                    break;
            }
    	#endif
    }
}

void InitializeConsole(void){
    static uint32 u32ConsoleTaskHandle;
    static StaticTask_t Console_Task_Obj;
	static uint32 ConsoleTaskStk[CONSOLE_TASK_STK_SIZE]; // QAC
	static COMMANDLIST gDefaultHelpCommandList;
    memset(&gDefaultHelpCommandList, 0x00, sizeof(COMMANDLIST));
    /* Initialize CONSOLE(UART) */
    //UART_Init(&uart6_pal_instance,&uart6_pal_Config0);
    u32ConsoleTaskHandle = 0;
    u32ConsoleTaskHandle = (uint32)xTaskCreateStatic((TaskFunction_t)Tsk_Console,
        											(const char * const)"TSK_Console",
        											(const uint32)CONSOLE_TASK_STK_SIZE,
        											0,
        											(UBaseType_t)(((UBaseType_t)5) | portPRIVILEGE_BIT ),
        											(StackType_t * const)ConsoleTaskStk, /**/
        											(StaticTask_t * const)&Console_Task_Obj); /**/
    if(u32ConsoleTaskHandle != 0){

    }
	#if 0//CS :Empty if Statement
	if ( err == OSAL_ERR_NONE)
	{
	}
	#endif
}












