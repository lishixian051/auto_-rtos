/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Dcm_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-07-10 17:31:50>
 */
/*============================================================================*/

/******************************* references ************************************/
#include "Dcm_Types.h"
#include "Rte_Dcm.h"
#include "Dcm_Cfg.h"
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
#include "UDS.h"
#endif
#if(STD_ON == DCM_OBD_FUNC_ENABLED)
#include "OBD.h"
#endif

/**********************************************************************
 ***********************DcmGeneral Container***************************
 **********************************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_GeneralCfgType,DCM_CONST)Dcm_GeneralCfg =
{
    FALSE, /*DcmDDDIDStorage*/
	DCM_DEV_ERROR_DETECT, /*DcmDevErrorDetect*/			
	NULL_PTR, /*DcmHeaderFileInclusion*/							
	DCM_RESPOND_ALL_REQUEST, /*DcmRespondAllRequest*/			
	DCM_VERSION_INFO_API, /*DcmVersionInfoApi*/				
	10, /*DcmTaskTime*/
	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************************************************
 ********************************* DSP container configration*****************************
 *****************************************************************************************/


/**********************Clear DTC**************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspClearDTCType, DCM_CONST) Dcm_DspClearDTCCfg =
{
	NULL_PTR, /*DcmDsp_ClearDTCCheckFnc*/
	NULL_PTR, /*DcmDspClearDTCModeRuleRef*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************Control DTC Setting********************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspControlDTCSettingType, DCM_CONST) Dcm_DspControlDTCSettingCfg =	
{
	FALSE, /*DcmSupportDTCSettingControlOptionRecord*/
	NULL_PTR, /*DcmDspControlDTCSettingReEnableModeRuleRef*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************Com Control********************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DspComControlAllChannelType, DCM_CONST) Dcm_DspComControlAllChannelCfg[1] = 
{
	{
		TRUE, /*DcmDspComControlAllChannelUsed*/
		0, /*DcmDspComMChannelId*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DspComControlType, DCM_CONST) Dcm_DspComControlCfg =
{
	1u, /*DcmDspComControlAllChannelNum*/
	&Dcm_DspComControlAllChannelCfg[0], /*DcmDspComControlAllChannel*/	
	NULL_PTR, /*DcmDspComControlSetting*/								
	0u, /*DcmDspComControlSpecificChannelNum*/								
	NULL_PTR, /*DcmDspComControlSpecificChannel*/	
	0u, /*DcmDspComControlSubNodeNum*/								
	NULL_PTR /*DcmDspComControlSubNode*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************Common Authorization********************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)DcmDspCommonAuthorization_0_SecRef[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)DcmDspCommonAuthorization_0_SesRef[2] = {4u, 3u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)DcmDspCommonAuthorization_1_SecRef[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)DcmDspCommonAuthorization_1_SesRef[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspCommonAuthorizationType, DCM_CONST) Dcm_DspCommonAuthorizationCfg[2] =
{
	{
		NULL_PTR, /*DcmDspCommonAuthorizationModeRuleRef*/
		1u,	 /*DcmDspCommonAuthorizationSecurityLevelRefNum*/
		&DcmDspCommonAuthorization_0_SecRef[0], /*DcmDspCommonAuthorizationSecurityLevelRef*/
		2u, /*DcmDspCommonAuthorizationSessionRefNum*/
		&DcmDspCommonAuthorization_0_SesRef[0], /*DcmDspCommonAuthorizationSessionRef*/	
	},
	{
		NULL_PTR, /*DcmDspCommonAuthorizationModeRuleRef*/
		1u,	 /*DcmDspCommonAuthorizationSecurityLevelRefNum*/
		&DcmDspCommonAuthorization_1_SecRef[0], /*DcmDspCommonAuthorizationSecurityLevelRef*/
		1u, /*DcmDspCommonAuthorizationSessionRefNum*/
		&DcmDspCommonAuthorization_1_SesRef[0], /*DcmDspCommonAuthorizationSessionRef*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDataType, DCM_CONST) Dcm_DspDataCfg[72] =
{
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0301,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0301, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		160u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0302,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0302, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		96u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0303,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0303, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		160u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_ASYNCH_FNC_ERROR, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0304,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0304, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		96u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_03FF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_03FF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		512u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0A01,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0A01, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0AFF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0AFF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0B01,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0B01, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0BFF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0BFF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0E01,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0E01, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_SINT16, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0E02,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0E02, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0EFF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0EFF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		80u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_FFFF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_FFFF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		664u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_020E,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_020E, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0210,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0210, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0212,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0212, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0214,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0214, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_02FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_02FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		112u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_030A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_030A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		960u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_030E,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_030E, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		160u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0310,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0310, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		160u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_0311, /*DcmDspDataWriteFnc*/		
		304u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0314,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0314, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0316,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0316, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		1040u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_0317, /*DcmDspDataWriteFnc*/		
		1040u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_031A, /*DcmDspDataWriteFnc*/		
		656u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_031B,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_031B, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_03FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_03FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		4568u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		Rte_FreezeCurrentState_040A,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_040A, /*DcmDspDataReadFnc*/		
		Rte_RestToDefault_040A, /*DcmDspDataResetToDefaultFnc*/	
		Rte_ReturnControlToEcu_040A, /*DcmDspDataReturnControlToECUFnc*/		
		Rte_ShortTermAdjustment_040A, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_04FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_04FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_060C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_060C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_060E,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_060E, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0610,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0610, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_0611, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0612,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0612, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_0613, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_06FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_06FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_070A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_070A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_070C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_070C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_07FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_07FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_080A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_080A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		512u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_080C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_080C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_08FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_08FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		520u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_090A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_090A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_09FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_09FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0B0A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0B0A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		400u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_0B0B, /*DcmDspDataWriteFnc*/		
		40u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0BFE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0BFE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		440u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0C0A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0C0A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0CFE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0CFE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0D0A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0D0A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0D0C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0D0C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0DFE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0DFE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		64u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0E0A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0E0A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_SINT16, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0EFE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0EFE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_FEFF,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_FEFF, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		5352u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_020A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_020A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_030C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_030C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		960u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_030D, /*DcmDspDataWriteFnc*/		
		96u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_031E, /*DcmDspDataWriteFnc*/		
		96u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_031F, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_190A,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_190A, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_191B, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_19FE,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_19FE, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		48u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_090B, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_SINT16, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_110A, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_110C, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_110E, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_1110, /*DcmDspDataWriteFnc*/		
		32u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_0D10,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_0D10, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		64u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		Rte_ReadDataLength_090C,  /*DcmDspDataReadDataLengthFnc*/		
		Rte_ReadData_090C, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		NULL_PTR, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	},
	{
		NULL_PTR,	 /*DcmDspDataConditionCheckReadFnc*/	
		FALSE, /*DcmConditionCheckReadFncUsed*/	
		NULL_PTR,	 /*DcmDspDataEcuSignalFnc*/	
		NULL_PTR, /*DcmDspDataReadEcuSignalFnc*/	
		DCM_OPAQUE, /*DcmDspDataEndianness*/	
		NULL_PTR,	 /*DcmDspDataFreezeCurrentsStateFnc*/	
		NULL_PTR,	 /*DcmDspDataGetScalingInfoFnc*/	
		NULL_PTR,  /*DcmDspDataReadDataLengthFnc*/		
		NULL_PTR, /*DcmDspDataReadFnc*/		
		NULL_PTR, /*DcmDspDataResetToDefaultFnc*/	
		NULL_PTR, /*DcmDspDataReturnControlToECUFnc*/		
		NULL_PTR, /*DcmDspDataShortTermAdjustmentFnc*/		
		Rte_WriteData_090D, /*DcmDspDataWriteFnc*/		
		8u, /*DcmDspDataSize*/	
		DCM_UINT8, /*DcmDspDataType*/	
		USE_DATA_SYNCH_FNC, /*DcmDspDataUsePort*/	
		0u, /*DcmDspDataBlockId*/	
		0xffu, /*DcmDspDataInfoIndex*/	
		NULL_PTR, /*DcmDspDiagnosisScaling*/	
		NULL_PTR /*DcmDspExternalSRDataElementClass*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/********************Dsp Did******************************/
/******************************************
 *DcmDspDidRead container configration
 *****************************************/																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_0_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_0_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_0_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_1_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_1_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_1_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_2_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_2_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_2_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_3_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_3_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_3_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_4_Read_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_4_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    2u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_4_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_5_Read_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_5_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    2u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_5_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_6_Read_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_6_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    2u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_6_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_7_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_7_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_7_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_8_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_8_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_8_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_9_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_9_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_9_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_10_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_10_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_10_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_11_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_11_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_11_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_12_Read_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_12_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    2u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_12_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_13_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_13_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_13_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_14_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_14_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_14_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_15_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_15_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_15_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_16_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_16_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_16_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_17_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_17_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_17_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_17_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_17_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_18_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_18_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_18_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_19_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_19_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_19_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_20_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_20_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_20_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_22_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_22_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_22_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_22_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_22_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_23_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_23_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_23_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_23_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_23_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_26_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_26_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_26_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_27_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_27_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_27_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_27_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_27_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_29_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_29_Read_SesRefCfg[3] = {3u, 4u, 1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_29_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_29_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_29_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_30_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_30_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_30_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_31_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_31_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_31_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_32_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_32_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_32_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_34_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_34_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_34_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_36_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_36_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_36_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_36_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_36_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_37_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_37_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_37_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_38_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_38_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_38_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_39_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_39_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_39_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_39_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_39_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_40_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_40_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_40_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_41_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_41_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_41_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_42_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_42_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_42_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_42_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_42_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_43_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_43_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_43_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_44_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_44_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_44_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_44_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_44_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_45_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_45_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_45_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_47_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_47_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_47_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_47_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_47_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_48_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_48_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_48_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_49_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_49_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_49_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_49_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_49_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_50_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_50_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_50_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_51_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_51_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_51_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_52_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_52_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_52_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_52_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_52_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_53_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_53_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_53_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_54_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_54_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_54_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_54_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_54_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_55_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_55_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_55_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_55_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_55_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_56_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_56_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_56_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_57_Read_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_57_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    2u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_57_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_61_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_61_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_61_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_63_Read_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_63_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_63_ReadCfg =
{
    1u, /*DcmDspDidReadSecurityLevelRefNum*/
    &Dcm_DidInfo_63_Read_SecRefCfg[0], /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_63_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_69_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_69_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_69_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_70_Read_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidReadType,DCM_CONST)Dcm_DidInfo_70_ReadCfg =
{
    0u, /*DcmDspDidReadSecurityLevelRefNum*/
    NULL_PTR, /*pDcmDspDidReadSecurityLevelRow*/
    3u, /*DcmDspDidReadSessionRefNum*/
    &Dcm_DidInfo_70_Read_SesRefCfg[0], /*pDcmDspDidReadSessionRow*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************************************
 *DcmDspDidWrite container configuration,
 which is in the DcmDspDidInfo container
 ******************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_21_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_21_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_21_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_21_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_21_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_24_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_24_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_24_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_24_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_24_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_25_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_25_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_25_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_25_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_25_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_33_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_33_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_33_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_33_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_33_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_35_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_35_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_35_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_35_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_35_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_46_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_46_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_46_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_46_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_46_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_58_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_58_Write_SesRefCfg[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_58_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_58_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    2u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_58_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_59_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_59_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_59_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_59_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_59_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_60_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_60_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_60_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_60_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_60_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_62_Write_SecRefCfg[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_62_Write_SesRefCfg[1] = {4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_62_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_62_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    1u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_62_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_64_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_64_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_64_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_64_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_64_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_65_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_65_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_65_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_65_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_65_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_66_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_66_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_66_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_66_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_66_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_67_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_67_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_67_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_67_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_67_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_68_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_68_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_68_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_68_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_68_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_71_Write_SecRefCfg[1] = {2u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_DidInfo_71_Write_SesRefCfg[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
																		
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DspDidWriteType,DCM_CONST)Dcm_DidInfo_71_WriteCfg= 
{
    1u,/*DcmDspDidWriteSecurityLevelRefNum*/
    &Dcm_DidInfo_71_Write_SecRefCfg[0],	/*pDcmDspDidWriteSecurityLevelRow*/
    3u,	/*DcmDspDidWriteSessionRefNum*/
    &Dcm_DidInfo_71_Write_SesRefCfg[0], /*pDcmDspDidWriteSessionRow*/						
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************************************
 *DcmDspDidControl container configuration,
 which is in the DcmDspDidInfo container
 ******************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(uint8,DCM_CONST)Dcm_DidInfo_28_Control_SecRefCfg[1]= {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(uint8,DCM_CONST)Dcm_DidInfo_28_Control_SesRefCfg[1]={4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidControlType,DCM_CONST) Dcm_DidInfo_28_ControlCfg = 
{
	DCM_CONTROLMASK_NO, /*DcmDspDidControlMask*/
	0u, /*DcmDspDidControlMaskSize*/	
    1u, /*DcmDspDidControlSecurityLevelRefNum*/
    &Dcm_DidInfo_28_Control_SecRefCfg[0], /*pDcmDspDidControlSecurityLevelRow*/
    1u, /*DcmDspDidControlSessionRefNum*/
    &Dcm_DidInfo_28_Control_SesRefCfg[0], /*pDcmDspDidControlSessionRow*/
    TRUE, /*DcmDspDidFreezeCurrentState*/
	TRUE, /*DcmDspDidResetToDefault*/
    TRUE, /*DcmDspDidShortTermAdjustement*/	
	NULL_PTR, /*DcmDspDidControlEnableMask*/	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
	
/******************************************
 *DcmDspDidInfo container Configuration ***
 ******************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidInfoType,DCM_CONST)Dcm_DspDidInfoCfg[72] = 
{
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_0_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_1_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_2_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_3_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_4_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_5_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_6_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_7_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_8_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_9_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_10_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_11_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_12_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_13_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_14_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_15_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_16_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_17_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_18_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_19_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_20_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_21_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_22_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_23_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_24_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_25_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_26_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_27_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		&Dcm_DidInfo_28_ControlCfg, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_29_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_30_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_31_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_32_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_33_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_34_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_35_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_36_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_37_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_38_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_39_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_40_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_41_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_42_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_43_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_44_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_45_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_46_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_47_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_48_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_49_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_50_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_51_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_52_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_53_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_54_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_55_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_56_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_57_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_58_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_59_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_60_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_61_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_62_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_63_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_64_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_65_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_66_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_67_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_68_WriteCfg, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_69_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		&Dcm_DidInfo_70_ReadCfg, /*pDcmDspDidRead*/	
		NULL_PTR, /*pDcmDspDidWrite*/	
    },
    {
        0u, /*DcmDspDDDIDMaxElements*/	
    	FALSE, /*DcmDspDidDynamicallyDefined*/	
		NULL_PTR, /*pDcmDspDidControl*/	
		NULL_PTR, /*pDcmDspDidRead*/	
		&Dcm_DidInfo_71_WriteCfg, /*pDcmDspDidWrite*/	
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_301_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[0],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_302_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[1],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_303_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[2],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_304_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[3],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_3FF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[4],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_A01_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[5],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_AFF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[6],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_B01_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[7],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_BFF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[8],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_E01_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[9],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_E02_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[10],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_EFF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[11],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_FFFF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[12],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_20E_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[13],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_210_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[14],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_212_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[15],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_214_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[16],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_2FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[17],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_30A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[18],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_30E_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[19],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_310_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[20],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_311_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[21],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_314_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[22],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_316_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[23],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_317_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[24],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_31A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[25],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_31B_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[26],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_3FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[27],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_40A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[28],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_4FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[29],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_60C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[30],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_60E_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[31],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_610_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[32],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_611_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[33],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_612_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[34],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_613_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[35],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_6FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[36],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_70A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[37],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_70C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[38],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_7FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[39],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_80A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[40],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_80C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[41],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_8FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[42],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_90A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[43],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_9FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[44],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_B0A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[45],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_B0B_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[46],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_BFE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[47],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_C0A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[48],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_CFE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[49],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_D0A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[50],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_D0C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[51],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_DFE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[52],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_E0A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[53],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_EFE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[54],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_FEFF_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[55],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_20A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[56],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_30C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[57],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_30D_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[58],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_31E_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[59],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_31F_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[60],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_190A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[61],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_190B_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[62],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_19FE_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[63],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_90B_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[64],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_110A_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[65],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_110C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[66],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_110E_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[67],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_1110_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[68],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_D10_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[69],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_90C_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[70],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidSignalType,DCM_CONST)Dcm_Did_90D_SignalCfg[1] = 
{
	{
		0u,                   /*DcmDspDidDataPos*/		
		&Dcm_DspDataCfg[71],	 /*pDcmDspDidData*/	
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/**********************************************
 *DcmDspDid container configration*************
 **********************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspDidType,DCM_CONST)Dcm_DspDidCfg[72] =
{
    { /* Did_0x301 */
        0x301u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        0u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_301_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x302 */
        0x302u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        1u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_302_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x303 */
        0x303u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        2u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_303_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x304 */
        0x304u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        3u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_304_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x3FF */
        0x3FFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        4u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_3FF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xA01 */
        0xA01u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        5u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_A01_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xAFF */
        0xAFFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        6u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_AFF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xB01 */
        0xB01u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        7u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_B01_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xBFF */
        0xBFFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        8u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_BFF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xE01 */
        0xE01u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        9u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_E01_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xE02 */
        0xE02u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        10u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_E02_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xEFF */
        0xEFFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        11u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_EFF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xFFFF */
        0xFFFFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        12u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_FFFF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x20E */
        0x20Eu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        13u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_20E_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x210 */
        0x210u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        14u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_210_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x212 */
        0x212u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        15u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_212_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x214 */
        0x214u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        16u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_214_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x2FE */
        0x2FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        17u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_2FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x30A */
        0x30Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        18u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_30A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x30E */
        0x30Eu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        19u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_30E_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x310 */
        0x310u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        20u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_310_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x311 */
        0x311u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        21u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_311_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x314 */
        0x314u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        22u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_314_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x316 */
        0x316u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        23u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_316_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x317 */
        0x317u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        24u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_317_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x31A */
        0x31Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        25u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_31A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x31B */
        0x31Bu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        26u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_31B_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x3FE */
        0x3FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        27u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_3FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x40A */
        0x40Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        28u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_40A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x4FE */
        0x4FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        29u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_4FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x60C */
        0x60Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        30u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_60C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x60E */
        0x60Eu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        31u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_60E_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x610 */
        0x610u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        32u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_610_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x611 */
        0x611u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        33u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_611_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x612 */
        0x612u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        34u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_612_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x613 */
        0x613u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        35u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_613_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x6FE */
        0x6FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        36u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_6FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x70A */
        0x70Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        37u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_70A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x70C */
        0x70Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        38u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_70C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x7FE */
        0x7FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        39u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_7FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x80A */
        0x80Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        40u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_80A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x80C */
        0x80Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        41u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_80C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x8FE */
        0x8FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        42u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_8FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x90A */
        0x90Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        43u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_90A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x9FE */
        0x9FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        44u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_9FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xB0A */
        0xB0Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        45u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_B0A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xB0B */
        0xB0Bu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        46u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_B0B_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xBFE */
        0xBFEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        47u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_BFE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xC0A */
        0xC0Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        48u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_C0A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xCFE */
        0xCFEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        49u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_CFE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xD0A */
        0xD0Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        50u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_D0A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xD0C */
        0xD0Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        51u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_D0C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xDFE */
        0xDFEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        52u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_DFE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xE0A */
        0xE0Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        53u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_E0A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xEFE */
        0xEFEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        54u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_EFE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xFEFF */
        0xFEFFu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        55u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_FEFF_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x20A */
        0x20Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        56u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_20A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x30C */
        0x30Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        57u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_30C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x30D */
        0x30Du,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        58u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_30D_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x31E */
        0x31Eu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        59u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_31E_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x31F */
        0x31Fu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        60u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_31F_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x190A */
        0x190Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        61u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_190A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x190B */
        0x190Bu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        62u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_190B_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x19FE */
        0x19FEu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        63u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_19FE_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x90B */
        0x90Bu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        64u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_90B_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x110A */
        0x110Au,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        65u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_110A_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x110C */
        0x110Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        66u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_110C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x110E */
        0x110Eu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        67u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_110E_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x1110 */
        0x1110u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        68u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_1110_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0xD10 */
        0xD10u,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        69u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_D10_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x90C */
        0x90Cu,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        70u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_90C_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    },
    { /* Did_0x90D */
        0x90Du,	 /*DcmDspDidId*/	
		TRUE,	 /*DcmDspDidUsed*/	
        71u,	 /*DcmDspDidInfoIndex*/		
        0u,	 /*DcmDspRefDidNum*/	
        NULL_PTR,	 /*pDcmDspRefDidIdArray*/	
        1u, /*DcmDspDidSignalNum*/	
        &Dcm_Did_90D_SignalCfg[0],	 /*pDcmDspDidSignal*/	
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"






/******************Dsp Routine**************/
/***********************************
 *DcmDspRequestRoutineResults container
 **********************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x101_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x102_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x103_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x104_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x105_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x106_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x107_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x108_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x109_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10A_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10B_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10C_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10D_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_SINT16, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10E_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10F_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x110_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x111_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x112_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x113_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x114_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1FF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x201_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x202_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x2FF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x301_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x302_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x303_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x3FF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x601_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x6FF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1001_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x10FF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xB01_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xBFF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xFDFF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1701_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xFCFF_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1801_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1802_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1803_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0x1804_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		0u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		8u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xFF01_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		848u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		832u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutSignalType,DCM_CONST)Dcm_Routine_0xFF02_SignalCfg[2] = 
{
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		64u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},
	{
		DCM_OPAQUE, /*DcmDspRoutineSignalEndianness*/	
		832u, /*DcmDspRoutineSignalLength*/	
		0u, /*DcmDspRoutineSignalPos*/
		DCM_UINT8, /*DcmDspRoutineSignalType*/	
		NULL_PTR, /*DcmDspArgumentScaling*/										
	},	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/***********************************
 *DcmDspRequestRoutineResults container
 **********************************/
/***********************************
 *DcmDspRoutineStart container
 **********************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x101_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x101_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x101_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x101_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x101_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0101_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x101_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x101_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x102_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x102_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x102_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x102_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x102_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0102_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x102_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x102_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x103_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x103_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x103_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x103_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x103_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0103_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x103_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x103_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x104_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x104_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x104_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x104_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x104_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0104_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x104_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x104_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x105_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x105_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x105_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x105_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x105_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0105_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x105_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x105_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x106_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x106_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x106_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x106_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x106_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0106_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x106_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x106_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x107_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x107_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x107_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x107_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x107_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0107_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x107_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x107_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x108_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x108_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x108_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x108_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x108_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0108_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x108_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x108_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x109_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x109_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x109_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x109_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x109_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0109_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x109_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x109_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10A_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10A_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10A_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10A_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10A_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010A_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10A_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10A_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10B_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10B_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10B_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10B_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10B_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010B_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10B_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10B_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10C_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10C_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10C_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10C_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10C_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010C_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10C_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10C_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10D_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10D_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10D_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10D_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10D_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010D_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10D_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10D_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10E_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10E_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10E_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10E_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10E_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010E_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10E_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10E_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10F_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10F_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10F_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10F_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10F_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010F_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10F_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10F_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x110_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x110_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x110_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x110_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x110_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0110_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x110_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x110_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x111_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x111_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x111_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x111_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x111_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0111_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x111_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x111_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x112_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x112_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x112_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x112_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x112_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0112_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x112_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x112_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x113_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x113_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x113_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x113_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x113_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0113_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x113_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x113_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x114_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x114_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x114_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x114_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x114_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0114_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x114_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x114_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1FF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1FF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1FF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1FF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1FF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x01FF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1FF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1FF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x201_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x201_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x201_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x201_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x201_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0201_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x201_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x201_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x202_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x202_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x202_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x202_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x202_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0202_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x202_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x202_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x2FF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x2FF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x2FF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x2FF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x2FF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x02FF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x2FF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x2FF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x301_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x301_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x301_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x301_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x301_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0301_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x301_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x301_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x302_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x302_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x302_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x302_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x302_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0302_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x302_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x302_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x303_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x303_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x303_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x303_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x303_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0303_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x303_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x303_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x3FF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x3FF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x3FF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x3FF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x3FF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x03FF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x3FF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x3FF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x601_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x601_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x601_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x601_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x601_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0601_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x601_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x601_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x6FF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x6FF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x6FF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x6FF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x6FF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x06FF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x6FF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x6FF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1001_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1001_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1001_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1001_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1001_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1001_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1001_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1001_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10FF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10FF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x10FF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x10FF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x10FF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x10ff_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x10FF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x10FF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xB01_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xB01_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xB01_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xB01_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xB01_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0B01_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xB01_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xB01_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xBFF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xBFF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xBFF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xBFF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xBFF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0BFF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xBFF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xBFF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFDFF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFDFF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFDFF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFDFF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xFDFF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFDFF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xFDFF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xFDFF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1701_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1701_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1701_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1701_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1701_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1701_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1701_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1701_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFCFF_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFCFF_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFCFF_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFCFF_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xFCFF_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFCFF_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xFCFF_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xFCFF_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1801_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1801_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1801_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1801_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1801_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1801_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1801_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1801_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1802_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1802_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1802_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1802_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1802_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1802_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1802_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1802_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1803_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1803_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1803_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1803_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1803_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1803_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1803_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1803_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1804_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1804_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0x1804_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0x1804_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0x1804_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1804_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0x1804_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0x1804_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFF01_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFF01_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFF01_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFF01_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xFF01_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF01_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xFF01_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xFF01_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFF02_StartRoutineInCfg =
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFF02_SignalCfg[0],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineInOutType,DCM_CONST)Dcm_Routine_0xFF02_StartRoutineOutCfg = 
{	
	1u,                                  /*RoutineInOutSignalNum*/	
	&Dcm_Routine_0xFF02_SignalCfg[1],	/*DcmDspRoutineInOutSignal*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspStartRoutineType,DCM_CONST)Dcm_RoutineInfo_0xFF02_StartCfg =
{
     
     Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF02_Start, /*DcmDspStartRoutineFnc*/
     NULL_PTR, /*DcmDspStartRoutineCommonAuthorizationRef*/
     &Dcm_Routine_0xFF02_StartRoutineInCfg, /*DcmDspStartRoutineIn*/
     &Dcm_Routine_0xFF02_StartRoutineOutCfg, /*DcmDspStartRoutineOut*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/***********************************
 *DcmDspRoutineStop container
 **********************************/


/***********************************
 *DcmDspRoutine container configration
 **********************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspRoutineType,DCM_CONST)Dcm_DspRoutineCfg[43] =
{
    { /* DcmDspRoutine_0x0101 */
        0x101u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x101_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0102 */
        0x102u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x102_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0103 */
        0x103u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x103_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0104 */
        0x104u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x104_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0105 */
        0x105u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x105_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0106 */
        0x106u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x106_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0107 */
        0x107u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x107_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0108 */
        0x108u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x108_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0109 */
        0x109u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x109_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010A */
        0x10Au, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10A_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010B */
        0x10Bu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10B_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010C */
        0x10Cu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10C_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010D */
        0x10Du, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10D_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010E */
        0x10Eu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10E_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x010F */
        0x10Fu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10F_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0110 */
        0x110u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x110_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0111 */
        0x111u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x111_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0112 */
        0x112u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x112_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0113 */
        0x113u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x113_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0114 */
        0x114u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x114_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x01FF */
        0x1FFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1FF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0201 */
        0x201u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x201_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0202 */
        0x202u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x202_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x02FF */
        0x2FFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x2FF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0301 */
        0x301u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x301_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0302 */
        0x302u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x302_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0303 */
        0x303u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x303_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x03FF */
        0x3FFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x3FF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0601 */
        0x601u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x601_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x06FF */
        0x6FFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x6FF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1001 */
        0x1001u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1001_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x10ff */
        0x10FFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x10FF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0B01 */
        0xB01u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xB01_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x0BFF */
        0xBFFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xBFF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0xFDFF */
        0xFDFFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[1],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xFDFF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1701 */
        0x1701u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1701_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0xFCFF */
        0xFCFFu, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xFCFF_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1801 */
        0x1801u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1801_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1802 */
        0x1802u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1802_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1803 */
        0x1803u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1803_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0x1804 */
        0x1804u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0x1804_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0xFF01 */
        0xFF01u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xFF01_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    },
    { /* DcmDspRoutine_0xFF02 */
        0xFF02u, /*DcmDspRoutineId*/
		0u, /*DcmDspRoutineInfoByte*/ 
		FALSE, /*DcmDspRoutineUsePort*/	
		TRUE,	 /*DcmDspRoutineUsed*/
		&Dcm_DspCommonAuthorizationCfg[0],	 /*DcmDspCommonAuthorizationRef*/	
		NULL_PTR, /*DcmDspStopRoutineIn*/					
		&Dcm_RoutineInfo_0xFF02_StartCfg, /*DcmDspRequestRoutineResults*/
		NULL_PTR, /*DcmDspStopRoutine*/			
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/******************Dsp Security Row**************/
/************************************************
 ****DcmDspSecurityRow container(Multiplicity=0..31)****
 ************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspSecurityRowType,DCM_CONST)Dcm_DspSecurityRow[2] =
{
    { /* DcmDspSecurityRow_Level_K1 */	
        1u,      	/*DcmDspSecurityLevel*/			
        4u,      	/*DcmDspSecuritySeedSize*/		
        4u,      	/*DcmDspSecurityKeySize*/		
        0u,      	/*DcmDspSecurityADRSize*/	
		TRUE,		/*DcmDspSecurityAttemptCounterEnabled*/
        3u,    /*DcmDspSecurityNumAttDelay*/	
        10000u,  /*DcmDspSecurityDelayTime,10s */			
        0u,/*DcmDspSecurityDelayTimeOnBoot*/				
        Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetSeed,	/*Dcm_GetSeedFnc*/	
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_CompareKey,	/*Dcm_CompareKeyFnc*/
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetAttemptCounter,	/*Dcm_GetSecurityAttemptCounterFnc*/
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_SetAttemptCounter,	/*DcmDspSecurityUsePort*/
		USE_ASYNCH_FNC,	/*DcmDspSecurityUsePort*/
    },
    { /* DcmDspSecurityRow_Level_K2 */	
        2u,      	/*DcmDspSecurityLevel*/			
        4u,      	/*DcmDspSecuritySeedSize*/		
        4u,      	/*DcmDspSecurityKeySize*/		
        0u,      	/*DcmDspSecurityADRSize*/	
		TRUE,		/*DcmDspSecurityAttemptCounterEnabled*/
        3u,    /*DcmDspSecurityNumAttDelay*/	
        10000u,  /*DcmDspSecurityDelayTime,10s */			
        0u,/*DcmDspSecurityDelayTimeOnBoot*/				
        Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetSeed,	/*Dcm_GetSeedFnc*/	
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_CompareKey,	/*Dcm_CompareKeyFnc*/
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetAttemptCounter,	/*Dcm_GetSecurityAttemptCounterFnc*/
		Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_SetAttemptCounter,	/*DcmDspSecurityUsePort*/
		USE_ASYNCH_FNC,	/*DcmDspSecurityUsePort*/
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/************************************************
 ****DcmDspSecurity container(Multiplicity=1)****
 ************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspSecurityType,DCM_CONST)Dcm_DspSecurity =
{
    &Dcm_DspSecurityRow[0],	/*pDcm_DspSecurityRow*/								
    2u,	/*DcmDspSecurityRow_Num*/	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/******************Dsp Session Row**************/
/************************************************
 ****DcmDspSessionRow container(Multiplicity=0..31)
 ************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspSessionRowType,DCM_CONST)Dcm_DspSessionRow[4] =
{
    { /* DefaultSession_0x01 */
        DCM_NO_BOOT,	/*DcmDspSessionForBoot*/
    	1u,	/*DcmDspSessionLevel*/
        50u,	/*DcmDspSessionP2ServerMax*/	
        2000u,	/*DcmDspSessionP2StarServerMax*/	
    },
    { /* ProgramSession_0x02 */
        DCM_SYS_BOOT_RESPAPP,	/*DcmDspSessionForBoot*/
    	2u,	/*DcmDspSessionLevel*/
        50u,	/*DcmDspSessionP2ServerMax*/	
        2000u,	/*DcmDspSessionP2StarServerMax*/	
    },
    { /* ExtendSession_0x03 */
        DCM_NO_BOOT,	/*DcmDspSessionForBoot*/
    	3u,	/*DcmDspSessionLevel*/
        50u,	/*DcmDspSessionP2ServerMax*/	
        2000u,	/*DcmDspSessionP2StarServerMax*/	
    },
    { /* EOL_Session_0x04 */
        DCM_NO_BOOT,	/*DcmDspSessionForBoot*/
    	4u,	/*DcmDspSessionLevel*/
        50u,	/*DcmDspSessionP2ServerMax*/	
        2000u,	/*DcmDspSessionP2StarServerMax*/	
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/************************************************
 *******Dcm_DspSession container(Multiplicity=1)*
 ************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static CONST(Dcm_DspSessionType,DCM_CONST)Dcm_DspSession =
{
    &Dcm_DspSessionRow[0],	/*pDcmDspSessionRow*/								
    4u,		/*DcmDspSessionRow_Num*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*****************************************************
 ****************DcmDsp container configration********
 ****************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DspCfgType,DCM_CONST)Dcm_DspCfg =
{
	NULL_PTR,	/*DcmDspDDDIDcheckPerSourceDID*/
	DCM_BIG_ENDIAN,	/*DcmDspDataDefaultEndianness*/								
	FALSE,/*DcmDspEnableObdMirror*/			
    0,/*DcmDspMaxDidToRead*/								
    DCM_DSP_MAX_PERIODIC_DID_TO_READ,/*DcmDspMaxPeriodicDidToRead*/						
	0u,/*DcmDspPowerDownTime*/								
	BEFORE_RESET,	/*DcmResponseToEcuReset*/	

	&Dcm_DspClearDTCCfg,		/*pDcmDspClearDTC*/	
	&Dcm_DspComControlCfg,		/*pDcmDspComControl*/	
	&Dcm_DspCommonAuthorizationCfg[0],		/*pDcmDspCommonAuthorization*/	
	&Dcm_DspControlDTCSettingCfg,	/*pDcmDspControlDTCSetting*/	
	
	&Dcm_DspDataCfg[0],	/*pDcmDspData*/	
	
	NULL_PTR,	/*pDcmDspDataInfo*/	
	
	72u,	/*DcmDspDidNum*/	
    &Dcm_DspDidCfg[0],		/*pDcmDspDid*/									
	72u,	/*DcmDspDidInfoNum*/	
    &Dcm_DspDidInfoCfg[0],		/*pDcmDspDidInfo*/								
	0u,	/*DcmDspDidRangeNum*/	
	NULL_PTR,		/*pDcmDspDidRange*/								
	
	NULL_PTR, /*pDcmDspMemory*/
	
    NULL_PTR,	/*DcmDspRequestFileTransfer*/	
    
    43u,		/*DcmDspRoutineNum*/	
    &Dcm_DspRoutineCfg[0],	/*pDcmDspRoutine*/									
   
    &Dcm_DspSecurity,  /* pDcm_DspSecurity */				
    &Dcm_DspSession,  /* pDcm_DspSession */	
   					
    
    DCM_DSP_MAX_PERIODIC_DID_SCHEDULER,
    NULL_PTR
}; 
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************************************************
 ********************************* DSD container configration*****************************
 *****************************************************************************************/

/*DcmDsdService_0x10 SubService*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x10_1_SesRef[4] = {1u, 2u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x10_2_SesRef[2] = {2u, 3u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x10_3_SesRef[2] = {1u, 3u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x10_4_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x10[4] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x10_1_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		4u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x2u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x10_2_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x3u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x10_3_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x4u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x10_4_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x27 SubService*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x27_2_SesRef[3] = {3u, 4u, 1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x27_4_SesRef[3] = {3u, 4u, 1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x27_1_SesRef[3] = {3u, 4u, 1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x27_3_SesRef[3] = {3u, 4u, 1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x27[4] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x2u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x27_2_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		3u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x4u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x27_4_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		3u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x27_1_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		3u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x3u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x27_3_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		3u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x31 SubService*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x31_1_SecRef[1] = {1u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x31_1_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x31[1] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		&Dcm_UDS0x31_1_SecRef[0],	/*DcmDsdSubServiceSecurityLevelRef*/	
		1u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x31_1_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x19 SubService*/

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x19[4] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x2u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x4u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0xAu,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x28 SubService*/

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x28[4] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x0u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x2u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x3u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x85 SubService*/

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x85[2] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x2u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		NULL_PTR,	/*DcmDsdSubServiceSessionLevelRef*/	
		0u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x3e SubService*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x3E_0_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x3E[1] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x0u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x3E_0_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*DcmDsdService_0x11 SubService*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x11_1_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x11_3_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdSubServiceCfgType,DCM_CONST)DcmDsdSubService_UDS0x11[2] =	
{
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x1u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x11_1_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	},
	{
		NULL_PTR,	/*DcmDsdSubServiceFnc*/	
		0x3u,	/*DcmDsdSubServiceId*/	
		TRUE,	/*DcmDsdSubServiceUsed*/	
		NULL_PTR,	/*DcmDsdSubServiceModeRuleRef*/	
		NULL_PTR,	/*DcmDsdSubServiceSecurityLevelRef*/	
		0u,	/*DcmDsdSubServiceSecurityLevel_Num*/		
		&Dcm_UDS0x11_3_SesRef[0],	/*DcmDsdSubServiceSessionLevelRef*/	
		2u	/*DcmDsdSubServiceSessionLevel_Num*/		
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*UDS Service session and security configuration*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x22_SesRef[3] = {1u, 3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x19_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x14_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x28_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x85_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x3E_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(uint8,DCM_CONST)Dcm_UDS0x11_SesRef[2] = {3u, 4u};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"



#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
/*DcmDsdService DcmDsdServiceTable*/	
CONST(Dcm_DsdServiceCfgType,DCM_CONST)DcmDsdServiceTable_Service[12] =
{
	{ /*DiagnosticSessionControl*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x10,	/*DcmDsdSidTabFnc*/
		0x10u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		0u,	/*DcmDsdSessionLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSessionLevelRef*/
		4u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x10[0],	/*DcmDsdSubService*/
	},
	{ /*SecurityAccess*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x27,	/*DcmDsdSidTabFnc*/
		0x27u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYSICAL, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		0u,	/*DcmDsdSessionLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSessionLevelRef*/
		4u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x27[0],	/*DcmDsdSubService*/
	},
	{ /*ReadDataByIdentifier*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x22,	/*DcmDsdSidTabFnc*/
		0x22u,	/*DcmDsdServiceId*/
		FALSE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		3u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x22_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		0u,	/*DcmDsdSubService_Num*/
		NULL_PTR,	/*DcmDsdSubService*/
	},
	{ /*WriteDataByIdentifier*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x2E,	/*DcmDsdSidTabFnc*/
		0x2Eu,	/*DcmDsdServiceId*/
		FALSE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		0u,	/*DcmDsdSessionLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSessionLevelRef*/
		0u,	/*DcmDsdSubService_Num*/
		NULL_PTR,	/*DcmDsdSubService*/
	},
	{ /*InputOutputControlByIdentifier*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x2F,	/*DcmDsdSidTabFnc*/
		0x2Fu,	/*DcmDsdServiceId*/
		FALSE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		0u,	/*DcmDsdSessionLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSessionLevelRef*/
		0u,	/*DcmDsdSubService_Num*/
		NULL_PTR,	/*DcmDsdSubService*/
	},
	{ /*RoutineControl*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x31,	/*DcmDsdSidTabFnc*/
		0x31u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		0u,	/*DcmDsdSessionLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSessionLevelRef*/
		1u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x31[0],	/*DcmDsdSubService*/
	},
	{ /*ReadDTCInformation*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x19,	/*DcmDsdSidTabFnc*/
		0x19u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x19_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		4u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x19[0],	/*DcmDsdSubService*/
	},
	{ /*ClearDiagnosticInformation*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x14,	/*DcmDsdSidTabFnc*/
		0x14u,	/*DcmDsdServiceId*/
		FALSE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x14_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		0u,	/*DcmDsdSubService_Num*/
		NULL_PTR,	/*DcmDsdSubService*/
	},
	{ /*CommunicationControl*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x28,	/*DcmDsdSidTabFnc*/
		0x28u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x28_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		4u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x28[0],	/*DcmDsdSubService*/
	},
	{ /*ControlDTCSetting*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x85,	/*DcmDsdSidTabFnc*/
		0x85u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x85_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		2u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x85[0],	/*DcmDsdSubService*/
	},
	{ /*TesterPresent*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x3E,	/*DcmDsdSidTabFnc*/
		0x3Eu,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x3E_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		1u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x3E[0],	/*DcmDsdSubService*/
	},
	{ /*ECUReset*/
		TRUE,	/*DcmDsdServiceUsed*/
		Dcm_UDS0x11,	/*DcmDsdSidTabFnc*/
		0x11u,	/*DcmDsdServiceId*/
		TRUE,	/*DcmDsdSubfuncAvial*/
		DCM_PHYANDFUNC, /*DcmDsdSidTabAddressingFormat*/
		NULL_PTR,	/*DcmDsdModeRuleRef*/	
		0u, /*DcmDsdSecurityLevel_Num*/	
		NULL_PTR,	/*pDcmDsdSecurityLevelRef*/
		2u,	/*DcmDsdSessionLevel_Num*/	
		&Dcm_UDS0x11_SesRef[0],	/*pDcmDsdSessionLevelRef*/
		2u,	/*DcmDsdSubService_Num*/
		&DcmDsdSubService_UDS0x11[0],	/*DcmDsdSubService*/
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/**********************************************************************/
/*DCM Support Service Table(Multiplicity=1..256)*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdServiceTableCfgType,DCM_CONST)Dcm_DsdServiceTable[DCM_SERVICE_TAB_NUM]=
{
    {
        0x0u,	/*DcmDsdSidTabId*/
        &DcmDsdServiceTable_Service[0],	/*pDcmDsdService*/
        12u	/*DcmDsdSidTab_ServiceNum*/
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/**********************************************************************/

/**********************************************************************/
/*Dsd container(Multiplicity=1)*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DsdCfgType,DCM_CONST)Dcm_DsdCfg =
{
		/*DcmDsdServiceReqManufacturerNoti_PortNum*/
		/*DcmDsdServiceReqSupplierNoti_PortNum*/
    &Dcm_DsdServiceTable[0],	/*pDcmDsdServiceTable*/	
    DCM_SERVICE_TAB_NUM		/*DcmDsdServiceTable_Num*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************************************************
 ********************************* DSL container configration*****************************
 *****************************************************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
/*DcmDslBuffer container(Multiplicity=1..256)*/
static  CONST(Dcm_DslBufferType,DCM_CONST)Dcm_DslBufferCfg[DCM_CHANNEL_NUM] =
{	
    {/* DcmDslBuffer_Rx*/
    	0x0u,	/*Dcm_DslBufferId*/
		512u,	/*Dcm_DslBufferSize*/
		0u	/*offset*/
    },
    {/* DcmDslBuffer_Tx*/
    	0x1u,	/*Dcm_DslBufferId*/
		1100u,	/*Dcm_DslBufferSize*/
		512u	/*offset*/
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/***********************************/
/*DcmDslDiagResp container(Multiplicity=1)*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DslDiagRespType,DCM_CONST)Dcm_DslDiagRespCfg =
{	
    DCM_DSLDIAGRESP_FORCERESPENDEN,		/*DcmDslDiagRespOnSecondDeclinedRequest*/
    255u		/*DcmDslDiagRespMaxNumRespPend*/	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************
 *DcmDslCallbackDCMRequestService port configuration(Multiplicity=1..*)
 *****************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
static  CONST(Dcm_DslCallbackDCMRequestServiceType,DCM_CONST)Dcm_DslCallbackDCMRequestServiceCfg[0] =	
{	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/********************UDS protocol Connection configuration*******************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DslProtocolRxType,DCM_CONST)Dsl_Protocol_Connection_RxCfg[DCM_DSL_RX_ID_NUM]=
{
	{
    	0x0u,      		/*DcmDslParentConnectionCtrlId*/
        DCM_PHYSICAL,	/*DcmDslProtocolRxAddrType*/
        DCM_Dcm_Pdu_767_Diag_Phy_Rx,       /*DcmDslProtocolRxPduId*/	
		0x0u			/*DcmDslMetaDataLength*/	
    },
	{
    	0x0u,      		/*DcmDslParentConnectionCtrlId*/
        DCM_FUNCTIONAL,	/*DcmDslProtocolRxAddrType*/
        DCM_Dcm_Pdu_7DF_Diag_Fun_Rx,       /*DcmDslProtocolRxPduId*/	
		0x0u			/*DcmDslMetaDataLength*/	
    },
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DslProtocolTxType,DCM_CONST)Dsl_Protocol_Connection_TxCfg[DCM_DSL_TX_ID_NUM]=
{
	{
    	0x0u,			/*parent connection id*/
        DCM_Dcm_Pdu_76F_Diag_Respone_Tx,       /*DcmDslProtocolTxPduId*/
		DCM_PDUR_Dcm_Pdu_76F_Diag_Respone_Tx,	/*DcmDslProtocolTx Pdu Id of PduR*/
    },	
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"


/*Connection1,Mainconnection,ProtocolTx configration(Multiplicity=1..*)*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DslMainConnectionType,DCM_CONST) Dsl_Protocol_MainConnectionCfg[DCM_MAINCONNECTION_NUM] =
{
	{
		0x00000000u,							/*DcmDslProtocolRxTesterSourceAddr*/
		NULL_PTR,  /*pDcmDslPeriodicTranmissionConRef*/	
		0, 									/*DcmDslProtocolComMChannelId*/	
		NULL_PTR,  /*pDcmDslROEConnectionRef*/	
		&Dsl_Protocol_Connection_RxCfg[0],	/*pDcmDslProtocolRx*/
		2u,                   				/*DcmDslProtocolRx_Num*/
		&Dsl_Protocol_Connection_TxCfg[0],  /*pDcmDslProtocolTx*/	
		1u									/*DcmDslProtocolTx_Num*/
	}
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*Connection1 configration*/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DslConnectionType,DCM_CONST)Dsl_Protocol_ConnectionCfg[DCM_CONNECTION_NUM]=
{
    {
    	0x0u,        						/*parent protocolRow id*/
        &Dsl_Protocol_MainConnectionCfg[0],	/*pDcmDslMainConnection*/
        NULL_PTR,         					/*pDcmDslPeriodicTransmission*/
        NULL_PTR          					/*pDcmDslResponseOnEvent*/
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************
 ****Dcm_DslProtocolRow container configration(Multiplicity=1..*)*******
 ****************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h" 
CONST(Dcm_DslProtocolRowType,DCM_CONST)Dsl_ProtocolRowCfg[DCM_DSLPROTOCOLROW_NUM_MAX] =
{	
    {
        DCM_UDS_ON_CAN,		/*DcmDslProtocolID*/
        0x0u,					/*This parameter is mandatory and defines the maximum length of the response message in case DcmPagedBufferEnabled == TRUE*/
        0u,					/*DcmDslProtocolPriority*/
		TRUE,				/*true-protocol is available*/
		DCM_PROTOCAL_TRAN_NOT_VALID,
		FALSE,				/*True-send 0x78 before transitioning to the bootloader */
		0u,	 				/*DcmTimStrP2ServerAdjust*/	
		0u,				/*DcmTimStrP2StarServerAdjust*/	
		&Dcm_DslBufferCfg[0],/*DcmDslProtocolRxBuffer*/
        &Dcm_DslBufferCfg[1],/*DcmDslProtocolTxBuffer*/
        0u, 				/*DcmDslServiceTableId*/
        &Dsl_Protocol_ConnectionCfg[0],/*DcmDslConnection*/
        1u,					/*Number of connection*/
    }
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************
 *DcmDslProtocol container configration(Multiplicity=1)
 ****************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h"
static  CONST(Dcm_DslProtocolType,DCM_CONST)Dcm_DslProtocol =
{
    &Dsl_ProtocolRowCfg[0],	/*pDcmDslProtocolRow*/
    DCM_DSLPROTOCOLROW_NUM_MAX,	/*DcmDslProtocolRow_Num*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

/*****************************************************
 ****************DcmDsl container configration*****
 ****************************************************/
#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h"
CONST(Dcm_DslCfgType,DCM_CONST)Dcm_DslCfg =
{
	DCM_CHANNEL_NUM,		/*Number of Channel configration*/					
    &Dcm_DslBufferCfg[0],	/*DcmDslBuffer*/									
	0u,						/*Number of DslCallbackDCMRequestService port*/
    &Dcm_DslCallbackDCMRequestServiceCfg[0],	/*pDcmDslCallback_DCMRequestService*/									
    &Dcm_DslDiagRespCfg,   	/*reference to DcmDslDiagResp configration*/		
    &Dcm_DslProtocol,		/*reference to DcmDslProtocol configration*/		
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"

#define   DCM_START_SEC_CONST_UNSPECIFIED
#include  "Dcm_MemMap.h"
CONST(Dcm_CfgType,DCM_CONST)Dcm_Cfg =
{
	&Dcm_DslCfg,	/*pDcmDslCfg*/
	&Dcm_DsdCfg,	/*pDcmDsdCfg*/
	&Dcm_DspCfg,	/*pDcmDspCfg*/
	NULL_PTR,	    /*pDcmPageBufferCfg*/
	NULL_PTR,	    /*pDcmProcessingConditionsCfg*/
};
#define  DCM_STOP_SEC_CONST_UNSPECIFIED
#include "Dcm_MemMap.h"
