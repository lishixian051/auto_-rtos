/* ###################################################################
**     This component module is generated by Processor Expert. Do not modify it.
**     Filename    : adConv0.c
**     Project     : wallbox_Z4_0
**     Processor   : MPC5746C_256
**     Component   : adc_sar
**     Version     : Component C55_Repository, Driver 01.00, CPU db: 3.00.000
**     Repository  : SDK_S32_PA_11
**     Compiler    : GNU C Compiler
**     Date/Time   : 2020-02-19, 22:11, # CodeGen: 229
**
**     Copyright 1997 - 2015 Freescale Semiconductor, Inc. 
**     Copyright 2016-2017 NXP 
**     All Rights Reserved.
**     
**     THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
**     IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
**     OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**     IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
**     INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
**     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
**     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
**     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
**     IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
**     THE POSSIBILITY OF SUCH DAMAGE.
** ###################################################################*/
/*!
** @file adConv0.c
** @version 01.00
*/         
/*!
**  @addtogroup adConv0_module adConv0 module documentation
**  @{
*/         

/* MODULE adConv0. */

/**
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 8.7, External variable could be made static.
 * The external variables will be used in other source files, with the same initialized values.
 */

#include "adConv0.h"
#include <stddef.h>

/*! adConv0 configuration structure */

    
    
    

adc_chain_config_t adConv0_ChainCfg0 =
{
	.chanMaskNormal = {24615u, 1048576u, 0u},
	.chanMaskInjected = {24743u, 0u, 0u},
	.interruptMask = {24615u, 0u, 0u},
};


const adc_conv_config_t adConv0_ConvCfg0 = {
  .convMode = ADC_CONV_MODE_SCAN,
  .clkSelect = ADC_CLK_HALF_BUS,
  .refSelect = ADC_REF_VREFH,
  .ctuMode = ADC_CTU_MODE_DISABLED,
  .injectedEdge = ADC_INJECTED_EDGE_FALLING,
  .sampleTime0 = 8U,
  .sampleTime1 = 8U,
  .sampleTime2 = 8U,
  .autoClockOff = false,
  .overwriteEnable = true,
  .dataAlign = ADC_DATA_ALIGNED_RIGHT,
  .decodeDelay = 0U,
  .powerDownDelay = 0U,
};


/* END adConv0. */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the NXP C55 series of microcontrollers.
**
** ###################################################################
*/

