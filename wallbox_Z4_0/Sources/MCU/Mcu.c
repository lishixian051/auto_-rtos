/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <Mcu.c>
 *  @brief      < >
 *
 * <Compiler:     MCU:MPC5746C>
 *
 *  @author     <.wu>
 *  @date       <03-11-2018>
 */
/*============================================================================*/


#include "Mcu.h"
#include "cpu.h"


#include "Can.h"
#include "CanIf.h"
#include "CanTp.h"
#include "PduR.h"
#include "Dcm.h"
#include "Dem.h"
#include "Det.h"
#include "ComStack_Types.h"

#include "Xcp_test.h"
#include "Xcp.h"
#include "FreeRTimer.h"

#if (INTEGRATION_PROJECT_SWITCH == JUN_SHENG_INTEGRATION_PROJECT)
/*Enable XOSC, PLL0, PLL1, and enter LPU_STANDBY with LPU_SYS_CLK as system clock (160 MHz).*/
void SysClk_Init(void)
{
	/* 1: USE RUN_PC[1] */
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_0_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_1_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_2_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_3_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_4_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_5_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_6_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_7_INDEX] = 1u;
}
#else

#include "Can_Irq.h"
/*Enable XOSC, PLL0, PLL1, and enter LPU_STANDBY with LPU_SYS_CLK as system clock (160 MHz).*/
void SysClk_Init(void)
{
	MC_CGM->AC5_SC = 0x01000000;  //Connect FXOSC to the FMPLL input.
	/* Set FMPLL to  MHz with 40 MHz FXOSC reference. */
	PLLDIG->PLLDV = 0x04012020; //PREDIV = 2, MFD = 32, RFDPHI = 1, RFDPHI1 = 2
	PLLDIG->PLLFD = 0x00000000; //PLL fractional numerators
	PLLDIG->PLLCAL3 = 0x00004000;  //PLL fractional denominator
	/* Turn on clock sources and select system clock source */
	/*switch off the 32k extern OSC */
	MC_ME->DRUN_MC |= 0x00000F2u;  //Configure DRUN mode settings with sysclk FMPLL_PHI0 (160 MHz).

	MC_ME->RUN3_MC |= 0x00000F2u;  //Configure RUN3 mode settings with sysclk FMPLL_PHI0 (160 MHz).

	MC_ME->STOP_MC |= 0x0000090u;  //Configure RUN3 mode settings with 16 MHz int. RC osc. (16 MHz).

	/* Configure the oscillator divided clocks */
	SIRC->CTL = 0x00000011; //SIRC_DIV_CLK is SIRC divided by 1 (0.128 MHz).
	FIRC->CTL = 0x00000011;  //FIRC_DIV_CLK is FIRC divided by 1 (16 MHz).
	SXOSC->CTL |= SXOSC_CTL_OSCDIV(0x00);  //SXOSC_DIV_CLK is SXOSC divided by 1 (0.032 MHz).
	FXOSC->CTL |= FXOSC_CTL_OSCDIV(0x00);  //FXOSC_DIV_CLK is FXOSC divided by 1 (40 MHz).

	MC_ME->RUN_PC[1] = 0x000000FEu;  /* Enable peripherals to run in all modes */
	MC_ME->LP_PC[1] |= (1 << 10u) ;  /* Enable peripherals to run in Low Power stop modes */

	/* 1: USE RUN_PC[1] */
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_0_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_1_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_2_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_3_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_4_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_5_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_6_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_FLEXCAN_7_INDEX] = 1u;

	MC_ME->PCTLn[MC_ME_PCTL_LIN_0_INDEX] = 1u;
	MC_ME->PCTLn[MC_ME_PCTL_LIN_2_INDEX] = 1u;

	MC_ME->PCTLn[MC_ME_PCTL_RTC_API_INDEX] = 1u;

	MC_ME->PCTLn[MC_ME_PCTL_WKPU_INDEX] = 9u;

	/*Mode transition to DRUN mode:*/
	MC_ME->MCTL = 0x70005AF0;  /* Enter RUN3 mode & Key */
	MC_ME->MCTL = 0x7000A50F;  /* Enter RUN3 mode & Inverted Key */
	while(MC_ME->GS  & MC_ME_GS_S_MTRANS_MASK);  /* Wait for mode transition to complete */
	while(((MC_ME->GS & MC_ME_GS_S_CURRENT_MODE_MASK) >> MC_ME_GS_S_CURRENT_MODE_SHIFT) != 0x7u);  //Verify RUN3 is the current mode

}

void InitPeriClkGen(void)
{
	/* F160 domain is the undivided system clock. */
	MC_CGM->SC_DC0 = 0x80000000;  /* S160 at system clock divided by 1 (160 MHz).*/
	MC_CGM->SC_DC1 = 0x80010000;  /* S80 at system clock divided by 2 (80 MHz). */
	MC_CGM->SC_DC2 = 0x80030000;  /* S40 at system clock divided by 4 (40 MHz). */

	/*F40 is fixed (40 MHz)�� F80 is fixed (80 MHz). */
	MC_CGM->SC_DC5 = 0x01000000; /* FS80 at system clock divided by 2 (80 MHz). */
	/* F20 is fixed (20 MHz). */
	MC_CGM->AC2_SC = 0x01000000; /* ENET timers use TXD_CLK/RMII_CLK as source (50 MHz). */
	/* AUX Clock 3��4 not supported in MPC574xC */
	MC_CGM->AC5_SC = 0x01000000; /* FMPLL uses FXOSC as source (40 MHz). */
	MC_CGM->AC6_SC = 0x02000000;  /* CLKOUT0 uses as source PLL_CLKOUT1 (160 MHz.) */
	MC_CGM->AC6_DC0 = 0x80000000;  /* Divide CLKOUT0 source by 1 (160 MHz). */
	MC_CGM->AC8_SC = 0x00000000;  /* SPI0 source is F40 (40 MHz). */
	MC_CGM->AC9_SC = 0x01000000;  /* FlexCAN0 source is FXOSC (40 MHz). */
	MC_CGM->CLKOUT1_SC = 0x0E000000;  /* CLKOUT1 uses as source PLL_CLKOUT1 (160 MHz).*/
	MC_CGM->CLKOUT1_DC = 0x80000000;  /* CLKOUT1 is source divided by 1 (160 MHz). */
}
#endif

void software_reset(void)
{
	SystemSoftwareReset();
}

void Hardware_Reset( void )
{
	/* It needs to be deleted when it is released    21657  */
	SystemSoftwareReset();
}
void CleanVectorFlag(void)
{
	uint8 i;

	for(i = 0;i<=3;i++)
	{
		Clean_Current_Interrupt_flag(0, 3, i);
	}
	for(i = 4;i<=7;i++)
	{
		Clean_Current_Interrupt_flag(0, 7, i);
	}
	for(i = 8;i<=11;i++)
	{
		Clean_Current_Interrupt_flag(0, 11, i);
	}
	for(i = 12;i<=15;i++)
	{
		Clean_Current_Interrupt_flag(0, 15, i);
	}
	for(i = 16;i<=31;i++)
	{
		Clean_Current_Interrupt_flag(0, 31, i);
	}
	for(i = 32;i<=63;i++)
	{
		Clean_Current_Interrupt_flag(0, 63, i);
	}
	for(i = 64;i<=95;i++)
	{
		Clean_Current_Interrupt_flag(0, 95, i);
	}
}

void TestInit(void)
{
#if (INTEGRATION_PROJECT_SWITCH == I_SOFT_INTEGRATION_PROJECT) /* JUN SHEN */
    /*Enable XOSC, PLL0, PLL1, and enter LPU_STANDBY with LPU_SYS_CLK as system clock (160 MHz).*/
    SysClk_Init();

    InitPeriClkGen();
#endif
    /*Initialize CAN*/
	Can_Init(&Can_ConfigSet);
	CanIf_Init(&CanIf_InitCfgSet);
#if (INTEGRATION_PROJECT_SWITCH == I_SOFT_INTEGRATION_PROJECT) /* JUN SHEN */
	CleanVectorFlag();
	/* Enable Interrupt for CAN_x 00-95 MB*/
	Can_EnableIRQ();
#endif


    /* DIag Init */
    Det_Init(NULL_PTR);
    CanTp_Init(&CanTp_CfgData);
    Dcm_Init(&Dcm_Cfg);
    Dem_Init(&DemPbCfg);

    // Xcp_Init(&XcpConfig);

	Can_SetControllerMode(0,CAN_T_START);			//software triggered state transitions  of the CAN controller state machine
	CanIf_SetControllerMode(0,CANIF_CS_STARTED); 	//set PDU mode ONLINE
	CanIf_SetPduMode(0,CANIF_ONLINE);
    Dem_SetOperationCycleState((uint8)DemOperationCycle_ID, DEM_CYCLE_STATE_START);

}



void Test_1ms_task(void)
{
	Run_msCounter();
}
void Test_10ms_task(void)
{
#if (INTEGRATION_PROJECT_SWITCH == I_SOFT_INTEGRATION_PROJECT) /* JUN SHEN */
	uint8_t test_data[8]={1,2,3,4,5,6,7,8};

	static uint8_t DtcTestFlag = FALSE;

	static Cnt = 0;

	if(Cnt == 0)
	{
		Cnt++;

		/* Enable Interrupt for CAN_x 00-95 MB*/
		TestInit();
	}

	PduInfoType PduInfo_CANIF_TEST =
	{
		test_data,
		8
	};
	//CanIf_Transmit(1, &PduInfo_CANIF_TEST);
	//CanIf_Transmit(0, &PduInfo_CANIF_TEST);
	DtcTestFlag++;

	if(DtcTestFlag >= 100)
	{


		Dem_SetEventStatus(1,0x01u );
		Dem_SetEventStatus(2,0x01u );
		Dem_SetEventStatus(3,0x01u );
		Dem_SetEventStatus(4,0x01u );
		Dem_SetEventStatus(5,0x01u );
		Dem_SetEventStatus(6,0x01u );
		Dem_SetEventStatus(7,0x01u );
		Dem_SetEventStatus(8,0x01u );
		Dem_SetEventStatus(9,0x01u );
		Dem_SetEventStatus(10,0x01u );
		Dem_SetEventStatus(11,0x01u );
		Dem_SetEventStatus(12,0x01u );
		Dem_SetEventStatus(13,0x01u );
		Dem_SetEventStatus(14,0x01u );
		Dem_SetEventStatus(15,0x01u );
		Dem_SetEventStatus(16,0x01u );
		Dem_SetEventStatus(17,0x01u );
		Dem_SetEventStatus(18,0x01u );
		Dem_SetEventStatus(19,0x01u );
		Dem_SetEventStatus(20,0x01u );
		Dem_SetEventStatus(21,0x01u );
		Dem_SetEventStatus(22,0x01u );
		Dem_SetEventStatus(23,0x01u );
		Dem_SetEventStatus(24,0x01u );
		Dem_SetEventStatus(25,0x01u );
//		Dem_SetEventStatus(26,0x01u );
//		Dem_SetEventStatus(27,0x01u );
//		Dem_SetEventStatus(28,0x01u );
		DtcTestFlag = 0;
	}
	else
	{
		/* empty */
	}

#endif

	/* Diag main function */
	CanTp_MainFunction();
	Dcm_MainFunction();
	Dem_MainFunction();


	/* XCP For test */
   // Xcp_MainFunction();
	//DaqChangeTest();

	//Xcp_EventIndication(EventChannel_0);
}

