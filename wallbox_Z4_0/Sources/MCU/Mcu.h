/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file       <Mcu.h>
 *  @brief      < >
 *
 * <Compiler:     MCU:MPC5746C>
 *
 *  @author     <.wu>
 *  @date       <03-11-2018>
 */
/*============================================================================*/
#ifndef MCU_MCU_H_
#define MCU_MCU_H_

#define JUN_SHENG_INTEGRATION_PROJECT          0
#define I_SOFT_INTEGRATION_PROJECT             1
#define INTEGRATION_PROJECT_SWITCH  I_SOFT_INTEGRATION_PROJECT
extern void software_reset(void);
extern void Hardware_Reset(void);
extern void TestInit(void);
extern void Test_10ms_task(void);
extern void Test_1ms_task(void);
extern void CleanVectorFlag(void);
#endif /* MCU_MCU_H_ */
