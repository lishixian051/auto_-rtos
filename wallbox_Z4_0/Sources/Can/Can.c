/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can.c>
 *  brief      <>
 *
 *  <MCU:MPC5746C>
 *
 *  author     <>
 *  date       <2018-03-11 >
 */
/*============================================================================*/

#include "Can.h"
#include "CanIf_Types.h"
#include "CanIf_Cbk.h"
#if (STD_ON == CAN_DEV_ERROR_DETECT)
#endif

#if (STD_ON == CAN_DEV_ERROR_DETECT)
/* Can Driver State Machine */
typedef enum
{
	CAN_UNINIT = 0,
	CAN_READY
}Can_DriverStatusType;

/* can module status: (CAN_UNINIT, CAN_READY */
Can_DriverStatusType Can_DriverStatus = CAN_UNINIT;
#endif


/*******************************Global variable**************************/
const Can_ConfigType *Can_ConfigPtr = NULL_PTR;

CanControllerStatusType CanControllerStatus[CAN_MAX_CONTROLLERS];


/*************************************************************************/
/*
 * Brief               This function initializes the module.
 * ServiceId           0x00
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      Config: Pointer to driver configuration.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 * PreCondition        None
 * CallByAPI           Up layer
 *
 */
/*************************************************************************/
void Can_Init(const Can_ConfigType* Config)
{
	uint8 i = 0;
	#if (STD_ON == CAN_DEV_ERROR_DETECT)

    boolean detNoErr = TRUE;
    /* for post build,report error if NULL_PTR */
    if (NULL_PTR == Config)
    {
//    	Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_INIT_ID, CAN_E_PARAM_POINTER);
    	detNoErr = FALSE;
    }
    else if (CAN_UNINIT != Can_DriverStatus)
    {
//        Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_INIT_ID, CAN_E_TRANSITION);
        detNoErr = FALSE;
    }
    else
    {
    	for(i = 0; i < CAN_MAX_CONTROLLERS; i++)
    	{
    		if(CAN_T_UNINIT != CanControllerStatus[i].CntrlMode)
    		{
//    			Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_INIT_ID, CAN_E_TRANSITION);
    	        detNoErr = FALSE;
    			break;
    		}
    	}
    }
    if (TRUE == detNoErr)
    #endif/* STD_ON == CAN_DEV_ERROR_DETECT */
    {

        Can_ConfigPtr = Config;

    	Can_Hw_ControllerInit(Can_ConfigPtr);

    	for(i = 0; i < CAN_MAX_CONTROLLERS; i++)
    	{
    		CanControllerStatus[i].IntLockCount = 0;
    		CanControllerStatus[i].CntrlMode  = CAN_T_STOP;
    		CanControllerStatus[i].WakeupStatus = FALSE;
    	}

		#if (STD_ON == CAN_DEV_ERROR_DETECT)
		Can_DriverStatus = CAN_READY;
		#endif
    }
}
/*************************************************************************/
/*
 * Brief               This function Deinitializes the module.
 * ServiceId           0x00
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      Config: Pointer to driver configuration.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 * PreCondition        None
 * CallByAPI           Up layer
 *
 */
/*************************************************************************/
#define CAN_MCR_SOFTREST    0x02000000uL

void Can_Deinit(void)
{
    /* Get The CAN Base Address */
	Can_RegType *Can_Ptr = (Can_RegType *)(Can_ConfigSet.CanController->CanControllerBaseAddr);

   /* Clear Can registers */
   Can_Ptr->MCR |= CAN_MCR_SOFTREST;

    return;
}
/*************************************************************************/
/*
 * Brief               This service shall set the baud rate configuration
 * 					   of the CAN controller. Depending on necessary baud
 * 					   rate modifications the controller might have to
 * 					   reset..
 * ServiceId           0x0f
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different Controllers. Non reentrant
 * 					   for the same Controller.
 * Param-Name[in]      Controller: CAN controller, whose baud rate shall
 * 					   be set.
 * 					   BaudRateConfigID: references a baud rate configuration
 * 					   by ID (see CanControllerBaudRateConfigID).
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType:
 * 					   E_OK: Service request accepted, setting of (new)
 * 					   baud rate started.
					   E_NOT_OK: Service request not accepted.
 * PreCondition        Can_Init
 * CallByAPI           Up layer
 *
 */
/*************************************************************************/
Std_ReturnType Can_SetBaudrate(uint8 Controller, uint16 BaudRateConfigID)
{
	Std_ReturnType ret = E_OK;
	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)	/* req <SWS_CAN_00492> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
//				CAN_DISABLECONTROLLERINTERRUPT_ID, CAN_E_UNINIT);
		ret = E_NOT_OK;
	}
	else if(Controller >= CAN_MAX_CONTROLLERS)	/* req <SWS_CAN_00494> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
//				CAN_SETBAUDRATE_ID, CAN_E_PARAM_CONTROLLER);
		ret = E_NOT_OK;
	}
	else if(BaudRateConfigID >= CAN_MAX_BAUDRATE_CFG) /* req <SWS_CAN_00493> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
//				CAN_SETBAUDRATE_ID, CAN_E_PARAM_BAUDRATE);
		ret = E_NOT_OK;
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		Can_HW_SetBaudrate(Controller, BaudRateConfigID);
	}
	return ret;
}

/*************************************************************************/
/*
 * Brief               This function performs software triggered state
 * 					   transitions of the CAN controller State machine.
 * ServiceId           0x03
 * Sync/Async          ASynchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      Controller: CAN controller for which the status
 * 				 	   shall be changed¡£
 * 					   Transition: Transition value to request new CAN
 * 					   controller state.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType:
 * 					   E_OK: request accepted.
					   E_NOT_OK: request not accepted, a development error
					   occurred
 * PreCondition        Can_Init
 * CallByAPI           Up layer
 *
 */
/*************************************************************************/
Can_ReturnType Can_SetControllerMode(uint8 Controller, Can_StateTransitionType Transition)
{
	Can_ReturnType ret = CAN_OK;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)          /* req <SWS_Can_00198> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_UNINIT);
		ret = CAN_NOT_OK;
	}
	else if(Controller >= CAN_MAX_CONTROLLERS) /* req <SWS_Can_00199> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
//				CAN_SETCONTROLLERMODE_ID, CAN_E_PARAM_CONTROLLER);
		ret = CAN_NOT_OK;
	}
	else if((Transition > CAN_T_WAKEUP) || (Transition == CAN_T_UNINIT) )	/* req <SWS_Can_00200> */
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
		ret = CAN_NOT_OK;
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		switch(Transition)
		{
			case CAN_T_START:
				ret = Can_StartMode(Controller);
				break;
			case CAN_T_STOP:
				ret = Can_StopMode(Controller);
				break;
			case CAN_T_SLEEP:
				ret = Can_SleepMode(Controller);
				break;
			case CAN_T_WAKEUP:
				ret = Can_WakeUpMode(Controller);
				break;
			default:
				ret = CAN_NOT_OK;
				break;
		}

	}
	return ret;
}

/************************************************************************************/
/*
 * Brief                This function disables all interrupts for this CAN controller.
 * ServiceId            0x04
 * Sync/Async           Synchronous
 * Reentrancy           Reentrant
 * Param-Name[in]       Controller: CAN controller for which interrupts shall be disabled.
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         Can_Init
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_DisableControllerInterrupts(uint8 Controller)
{

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)	/* req <SWS_Can_00205> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
				CAN_DISABLECONTROLLERINTERRUPT_ID, CAN_E_UNINIT);
	}
	else if(Controller >= CAN_MAX_CONTROLLERS)	/* req <SWS_Can_00206> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
				CAN_DISABLECONTROLLERINTERRUPT_ID, CAN_E_PARAM_CONTROLLER);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		if(0U == CanControllerStatus[Controller].IntLockCount)
		{
			Can_Hw_DisableInterrupt(Controller);
		}
		/* req <SWS_Can_00202> */
		CanControllerStatus[Controller].IntLockCount++;
	}
}

/************************************************************************************/
/*
 * Brief                This function enables all allowed interrupts.
 * ServiceId            0x05
 * Sync/Async           Synchronous
 * Reentrancy           Reentrant
 * Param-Name[in]       Controller: CAN controller for which interrupts shall be re-enabled.
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         Can_Init
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_EnableControllerInterrupts(uint8 Controller)
{
	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)	/* req <SWS_Can_00209> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
				CAN_ENABLECONTROLLERINTERRUPT_ID, CAN_E_UNINIT);
	}
	else if(Controller >= CAN_MAX_CONTROLLERS)		/* req <SWS_Can_00210> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID,
				CAN_ENABLECONTROLLERINTERRUPT_ID, CAN_E_PARAM_CONTROLLER);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		/* req <SWS_Can_00208> */
		if(CanControllerStatus[Controller].IntLockCount > 0U)
		{
			CanControllerStatus[Controller].IntLockCount--;

			if(0U == CanControllerStatus[Controller].IntLockCount)
			{
				Can_Hw_EnableInterrupt(Controller);
			}
		}
	}
}


/*************************************************************************/
/*
 * Brief               This function is called by CanIf to pass a CAN
 * 					   message to CanDrv for transmission.
 * ServiceId           0x06
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant (thread-safe)
 * Param-Name[in]      Hth: information which HW-transmit handle shall be
 * 					   used for transmit.Implicitly this is also the
 * 					   information about the controller to use because the
 * 					   Hth numbers are unique inside one hardware unit.¡£
 * 					   PduInfo: Pointer to SDU user memory, DLC and Identifier.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Can_ReturnType:
 * 					   CAN_OK: Write command has been accepted.
					   CAN_NOT_OK: development error occurred.
					   CAN_BUSY:No TX hardware buffer available or pre-emptive
					   call of Can_Write that can't be implemented re-entrant.
 * PreCondition        Can_Init, Can_SetCntrlMode
 * CallByAPI           Up layer
 *
 */
/*************************************************************************/
Can_ReturnType Can_Write(Can_HwHandleType Hth, const Can_PduType* PduInfo)
{
	uint8 Hw_MB_Id;
	uint8 i = 0u;
	uint16 Used_Max_Mb = 0u;
	Can_ReturnType ret = CAN_NOT_OK;
	Can_Hw_MbTypeRef Can_Mb = NULL_PTR;
	uint8 Controller = Can_ConfigPtr->Can_HardwareObject[Hth].CanControllerId;
	Can_Hw_RegTypeRef Can_Reg =
        (Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	boolean detNoErr = FALSE;

	if(CAN_READY != Can_DriverStatus)
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_WRITE_ID, CAN_E_UNINIT);
		detNoErr = TRUE;
		ret = CAN_NOT_OK;
	}
	/* check hth is invalid */
	else if((Hth > CAN_MAX_OBJECT) || (CAN_OBJECT_TYPE_TRANSMIT != Can_ConfigPtr->Can_HardwareObject[Hth].CanObjectType))
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_WRITE_ID, CAN_E_PARAM_HANDLE);
		detNoErr = TRUE;
		ret = CAN_NOT_OK;
	}
	else if((NULL_PTR == PduInfo) || (NULL_PTR == PduInfo->sdu))
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_WRITE_ID, CAN_E_PARAM_POINTER);
		detNoErr = TRUE;
		ret = CAN_NOT_OK;
	}
	/* req <SWS_CAN_00218> */
	else if(((PduInfo->length > CAN_MAX_DATA_LENGTH) &&
			(FALSE == Can_ConfigPtr->CanController[Controller].CanFD_Support))
			|| ((PduInfo->length >CANFD_MAX_DATA_LENGTH) &&
			   (TRUE == Can_ConfigPtr->CanController[Controller].CanFD_Support))
			)
	{
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_WRITE_ID, CAN_E_PARAM_DLC);
		detNoErr = TRUE;
		ret = CAN_NOT_OK;
	}

	if (FALSE == detNoErr)
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		if(CAN_T_START == CanControllerStatus[Controller].CntrlMode)
		{
		    if(Controller == 0u)
			{
		    	Hw_MB_Id = Hth;
			}
			else
			{
				for(i = 0; i < Controller; i++)
				{
					Used_Max_Mb += Can_ConfigPtr->CanController[i].CanControllerMaxCounter;
				}

				Hw_MB_Id = (Hth - Used_Max_Mb);
			}

		    Can_Mb = Obtain_Mb_Addr(Controller, Can_Reg, Hw_MB_Id);
			/** - Check for pending message:
			 *     - pending message, return 0
			 *     - no pending message, start new transmission
			 */
			if((Can_Mb->CONFIG & DRV_CAN_MB_CODE) == (CAN_TX_INACTIVE << 24u))
			{
				Can_Hw_Write_Mb(Controller, Hw_MB_Id, PduInfo);
				ret = CAN_OK;
			}
			else
			{
				/* Message-box has a pending TX request */
				ret = CAN_BUSY;
			}
		}
	}
	return ret;

}


#if (STD_ON == CAN_TX_POLLING)
/************************************************************************************/
/*
 * Brief                This function performs the polling of TX confirmation when
						CAN_TX_PROCESSING is set to POLLING.
 * ServiceId            0x01
 * Param-Name[in]       None
 * Param-Name[out]      None
 * Param-Name[in/out]   None
 * Return               None
 * PreCondition         None
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_MainFunction_Write(void)
{
	uint8 Controller;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	/*req <CAN187>*/
	if(CAN_READY != Can_DriverStatus)	/*req <SWS_Can_00179>*/
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_MAINFUNCTION_WRITE_ID, CAN_E_UNINIT);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		for(Controller=0; Controller < CAN_MAX_CONTROLLERS; Controller++)
		{
			if((CAN_PROCESSING_POLLING == Can_ConfigPtr->CanController[Controller].CanTxProcessing)
			  && (CAN_T_START == CanControllerStatus[Controller].CntrlMode))
			{
				Can_Hw_Polling_TxProcess(Controller);
			}
		}
	}

}
#endif

#if (STD_ON == CAN_RX_POLLING)
/************************************************************************************/
/*
 * Brief                This function performs the polling of RX indications when
						CAN_RX_PROCESSING is set to POLLING.
 * ServiceId            <0x08>
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_MainFunction_Read(void)
{
	uint8 Controller;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	/*req <CAN187>*/
	if(CAN_READY != Can_DriverStatus)		/*req <SWS_Can_00181>*/
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_MAINFUNCTION_READ_ID, CAN_E_UNINIT);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{

		for(Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
		{
			if((CAN_PROCESSING_POLLING == Can_ConfigPtr->CanController[Controller].CanRxProcessing)
			  && (CAN_T_START == CanControllerStatus[Controller].CntrlMode))
			{
				Can_Hw_Polling_RxProcess(Controller);
			}
		}
	}

}
#endif

#if (STD_ON == CAN_BUSOFF_POLLING)
/************************************************************************************/
/*
 * Brief                This function performs the polling of RX indications when
						CAN_RX_PROCESSING is set to POLLING.
 * ServiceId            <0x09>
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_MainFunction_BusOff(void)
{
	uint8 Controller;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)  /* req <SWS_Can_00184> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_MAINFUNCTION_BUSOFF_ID, CAN_E_UNINIT);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		for(Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
		{
			if((CAN_PROCESSING_POLLING == Can_ConfigPtr->CanController[Controller].CanBusoffProcessing)
			  && (CAN_T_START == CanControllerStatus[Controller].CntrlMode))
			{
				Can_Hw_Polling_BusOffProcess(Controller);
			}
		}
	}

}
#endif

#if (STD_ON == CAN_WAKEUP_POLLING)
/************************************************************************************/
/*
 * Brief                This function performs the polling of wake-up events that are
 * 						configured statically as 'to be polled'.
 * ServiceId            <0x0a>
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            Up Layer
 */
/***********************************************************************************/
void Can_MainFunction_WakeUp(void)
{

	uint8 Controller;

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if(CAN_READY != Can_DriverStatus)  /* req <SWS_Can_00186> */
	{
		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_MAINFUNCTION_WAKEUP_ID, CAN_E_UNINIT);
	}
	else
	#endif /* STD_ON == CAN_DEV_ERROR_DETECT */
	{
		for(Controller = 0; Controller < CAN_MAX_CONTROLLERS; Controller++)
		{
			if((CAN_PROCESSING_POLLING == Can_ConfigPtr->CanController[Controller].CanBusoffProcessing)
			  && (CAN_T_SLEEP == CanControllerStatus[Controller].CntrlMode))
			{
				Can_Hw_Polling_WakeUpProcess(Controller);

			}
		}
	}

}
#endif

/************************************************************************************/
/*
 * Brief                <This function performs Controller start Mode process>
 * Param-Name[in]       <Controller-CAN Controller enable interrupt>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/***********************************************************************************/
Can_ReturnType Can_StartMode(uint8 Controller)
{
	Can_ReturnType ret = CAN_NOT_OK;

	if(CAN_T_START == CanControllerStatus[Controller].CntrlMode)
	{
		ret = CAN_OK;
		CanIf_ControllerModeIndication(Controller, CANIF_CS_STARTED);
	}
	else if(CAN_T_STOP == CanControllerStatus[Controller].CntrlMode)
	{
		/* req <SWS_Can_00261>*/
		Can_Hw_StartMode(Controller);
		CanControllerStatus[Controller].CntrlMode = CAN_T_START;
		CanIf_ControllerModeIndication(Controller, CANIF_CS_STARTED);
		ret = CAN_OK;
	}
	else
	{
		#if(STD_ON == CAN_DEV_ERROR_DETECT)
//			Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
		#endif
		ret = CAN_NOT_OK;
	}
    return ret;
}

/************************************************************************************/
/*
 * Brief                <This function performs Controller stop Mode process>
 * Param-Name[in]       <Controller-CAN Controller enable interrupt>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            <None>
 */
/***********************************************************************************/
Can_ReturnType Can_StopMode(uint8 Controller)
{
	Can_ReturnType ret = CAN_NOT_OK;

	if(CAN_T_STOP == CanControllerStatus[Controller].CntrlMode)
	{
		CanIf_ControllerModeIndication(Controller, CANIF_CS_STOPPED);
		ret = CAN_OK;
	}
	else if(CAN_T_START == CanControllerStatus[Controller].CntrlMode)
	{
		CanControllerStatus[Controller].CntrlMode = CAN_T_STOP;
		Can_Hw_StopMode(Controller);
		CanIf_ControllerModeIndication(Controller, CANIF_CS_STOPPED);
		ret = CAN_OK;
	}
	else
	{
		#if(STD_ON == CAN_DEV_ERROR_DETECT)
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
		#endif
		ret = CAN_NOT_OK;
	}
    return ret;
}

/******************************************************************************/
/*
 * Brief               <This function performs Controller sleep Mode  process>
 * Param-Name[in]      <Controller- CAN Controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
/* req <CAN265> */
Can_ReturnType Can_SleepMode(uint8 Controller)
{
	Can_ReturnType ret = CAN_NOT_OK;

	if(CAN_T_SLEEP == CanControllerStatus[Controller].CntrlMode)
	{
		CanIf_ControllerModeIndication(Controller, CANIF_CS_SLEEP);
		ret = CAN_OK;
	}
	else if(CAN_T_STOP == CanControllerStatus[Controller].CntrlMode)
	{
		CanControllerStatus[Controller].CntrlMode = CAN_T_SLEEP;
		/* not do anything, now is always in the stop mode. */
		Can_Hw_SleepMode(Controller);
		CanIf_ControllerModeIndication(Controller, CANIF_CS_SLEEP);
		ret = CAN_OK;
	}
	else
	{
		#if(STD_ON == CAN_DEV_ERROR_DETECT)
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
		#endif
		ret = CAN_NOT_OK;
	}
	return ret;
}

/******************************************************************************/
/*
 * Brief               <This function performs Controller wakeup Mode  process>
 * Param-Name[in]      <Controller- CAN Controller to be Mode process>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
Can_ReturnType Can_WakeUpMode(uint8 Controller)
{
	Can_ReturnType ret = CAN_NOT_OK;
	if(CAN_T_WAKEUP == CanControllerStatus[Controller].CntrlMode)
	{
		ret = CAN_OK;
	}
	else if(CAN_T_SLEEP == CanControllerStatus[Controller].CntrlMode)
	{
		CanControllerStatus[Controller].CntrlMode = CAN_T_STOP;
		/* not do anything, sleep is in the stop mode. */
		Can_Hw_WakeUpMode(Controller);
		CanIf_ControllerModeIndication(Controller, CANIF_CS_STOPPED);
		ret = CAN_OK;
	}
	else if(CAN_T_STOP == CanControllerStatus[Controller].CntrlMode)
	{	/* SWS_Can_00412 */
		ret = CAN_OK;
	}

	else
	{
		#if(STD_ON == CAN_DEV_ERROR_DETECT)
//		Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
		#endif
		ret = CAN_NOT_OK;
	}
    return ret;
}

#if (CAN_VERSION_INFO_API == STD_ON)
/******************************************************************************/
/*
 * Brief               <This function returns the version information of this module>
 * ServiceId           <0x07>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <versioninfo-pointer to location to store version information>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/******************************************************************************/
void Can_GetVersionInfo(Std_VersionInfoType* VersionInfo)
{

	#if(STD_ON == CAN_DEV_ERROR_DETECT)
	if (NULL_PTR == (VersionInfo))
	{
         Det_ReportError(CAN_MODULE_ID, CAN_INSTANCE_ID, CAN_SETCONTROLLERMODE_ID, CAN_E_TRANSITION);
	}
	#endif

	((Std_VersionInfoType *)(VersionInfo))->vendorID   = CAN_H_VENDOR_ID;
	((Std_VersionInfoType *)(VersionInfo))->moduleID   = CAN_MODULE_ID;
	((Std_VersionInfoType *)(VersionInfo))->instanceID = CAN_INSTANCE_ID;
	((Std_VersionInfoType *)(VersionInfo))->sw_major_version = CAN_H_SW_MAJOR_VERSION;
	((Std_VersionInfoType *)(VersionInfo))->sw_minor_version = CAN_H_SW_MINOR_VERSION;
	((Std_VersionInfoType *)(VersionInfo))->sw_patch_version = CAN_H_SW_PATCH_VERSION;

}

#endif

