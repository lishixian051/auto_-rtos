/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can_Hw.c>
 *  brief      < >
 *
 * <Compiler:     MCU:MPC5746C >
 *
 *  author     <yongxing.jia>
 *  date       <03-12-2018>
 */
/*============================================================================*/
#ifndef _CAN_HW_H_
#define _CAN_HW_H_

uint32 Can_Hw_Get_MsgBuff_16Payload_Addr(Can_RegType* Can_Reg, uint8 MsgBuffIdx);
uint32 Can_Hw_Get_MsgBuff_64Payload_Addr(Can_RegType* Can_Reg, uint8 MsgBuffIdx);
static uint8 Can_Hw_ComputeDLCValue(const Can_PduType* PduInfo);
static uint8 Can_Hw_ComputeLengthValue(uint8 dlcValue);
static void Can_Hw_Rx_Tx_Mb_Init(
		uint8 Controller,
		uint8 Can_Controller_Mb_Index,
		uint32 Can_HardwareObject_Index,
		const Can_ConfigType* Config,
		Can_MBDirectionType Direction
	);
static void Can_Hw_Cancel(uint8 Controller , uint8 obj);
static void Can_Hw_Mb_Clean(uint8 Controller);
void Can_Hw_EnterFreezeMode(Can_RegType* Can_Reg);
void Can_Hw_ExitFreezeMode(Can_RegType* Can_Reg);
void Can_Hw_ControllerInit(const Can_ConfigType* Config);
void Can_Hw_StopMode(uint8 Controller);
void Can_Hw_StartMode(uint8 Controller);
void Can_Hw_WakeUpMode(uint8 Controller);
void Can_Hw_SleepMode(uint8 Controller);
extern void Can_Hw_SleepMode(uint8 Controller);
extern void Can_Hw_WakeupMode(uint8 Controller);
extern void Can_Hw_Get_MB_Info(uint8 Controller, uint8 HwObjId, Can_PduType* PduInfo);
Can_Hw_MbTypeRef Obtain_Mb_Addr(uint8 Controller, Can_Hw_RegTypeRef Can_Reg, uint8 Can_Controller_Mb_Index);
void Can_Hw_Tx_Mb_Init
(
	uint8 Controller,
	uint8 Can_Controller_Mb_Index,
	uint32 Can_HardwareObject_Index,
	const Can_ConfigType* Config
);
void Can_Hw_Rx_Mb_Init
(
	uint8 Controller,
	uint8 Can_Controller_Mb_Index,
	uint32 Can_HardwareObject_Index,
	const Can_ConfigType* Config
);
void Can_Hw_Mb_Init(uint8 Controller, const Can_ConfigType* Config);
void Can_Hw_Write_Mb(uint8 Controller, uint8 Hw_MB_Id, const Can_PduType* PduInfo);

void Can_Hw_EnableInterrupt(uint8 Controller);
void Can_Hw_DisableInterrupt(uint8 Controller);
void Can_HW_SetBaudrate(uint8 Controller, uint8 BaudRateConfigID);

void Can_Hw_Polling_RxProcess(uint8 Controller);
void Can_Hw_Polling_TxProcess(uint8 Controller);
void Can_Hw_Polling_BusOffProcess(uint8 Controller);
void Can_Hw_Polling_WakeUpProcess(uint8 Controller);

#endif








