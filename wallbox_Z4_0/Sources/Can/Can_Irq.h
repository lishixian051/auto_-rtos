/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can_Irq.h>
 *  brief      <Can Irq Isr header file>
 *
 * <Compiler:     MCU:MPC5746C>
 *
 *  author     <yongxing.jia>
 *  date       <03-12-2018>
 */
/*============================================================================*/

#ifndef  CAN_IRQ_H
#define  CAN_IRQ_H


/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"
#include "Can_Cfg.h"

void CAN0_ORed_00_03_MB_IRQHandler(void);
void CAN0_ORed_04_07_MB_IRQHandler(void);
void CAN0_ORed_08_11_MB_IRQHandler(void);
void CAN0_ORed_12_15_MB_IRQHandler(void);
void CAN0_ORed_16_31_MB_IRQHandler(void);
void CAN0_ORed_32_63_MB_IRQHandler(void);
void CAN0_ORed_64_95_MB_IRQHandler(void);
void CAN0_ORed_IRQHandler(void);
void Can_EnableIRQ(void);
void Can_DisableIRQ(void);
void MB_00_95_IRQHandler(uint8 Controller, uint8 StartID, uint8 EndID);
void CAN_Busoff_IRQHandle(uint8  Controller);
extern void Clean_Current_Interrupt_flag(uint8 Controller, uint8 Can_Controller_EndID, uint8 Mb_Index);
#endif
