/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can_Reg.h>
 *  brief      < >
 *
 *  <Compiler:     MCU: MPC5746C>
 *
 *  author     <yongxing.jia>
 *  date       <03-12-2018>
 */
/*============================================================================*/


#ifndef __CAN_REG_H__
#define __CAN_REG_H__

#include "cstdint"
#include "TypeDef.h"

#define  CAN_MAX_MB                              96u

#define CAN_RAMn_COUNT                           384u
#define CAN_RXIMR_COUNT                          96u
#define CAN_WMB_COUNT                            4u

#define DRV_CAN_MCR_FDEN                      (0x1u << 11u)
#define DRV_CAN_MCR_AEN                       (0x1u << 12u)
#define DRV_CAN_MCR_IRMQ                      (0x1u << 16u)
#define DRV_CAN_MCR_SRXDIS                    (0x1u << 17u)
#define DRV_CAN_MCR_LPMACK                    (0x1u << 20u)
#define DRV_CAN_MCR_WRNEN                     (0x1u << 21u)
#define DRV_CAN_MCR_SUPV 	                  (0x1u << 23u)
#define DRV_CAN_MCR_FRZACK                    (0x1u << 24u)
#define DRV_CAN_MCR_SOFTRST                   (0x1u << 25u)
#define DRV_CAN_MCR_HALT                      (0x1u << 28u)
#define DRV_CAN_MCR_FRZ                       (0x1u << 30u)
#define DRV_CAN_MCR_MDIS                      (0x1u << 31u)

#define DRV_CAN_CTRL1_LBUF                    (0x1u << 4u )
#define DRV_CAN_CTRL1_BOFFREC                 (0x1u << 6u )
#define DRV_CAN_CTRL1_CLKSRC                  (0x1u << 13u)
#define DRV_CAN_CTRL1_ERRMSK                  (0x1u << 14u)
#define DRV_CAN_CTRL1_BOFFMSK                 (0x1u << 15u)

#define DRV_CAN_CTRL2_BOFFDONEMSK             (0x1u << 30u)
#define DRV_CAN_CTRL2_ERRMSK_FAST             (0x1u << 31u)
#define DRV_CAN_CTRL2_ISOCANFDEN_MASK         (0x1u << 12u)

#define DRV_CAN_CBT_BTF                       (0x1u << 31u)
#define DRV_CAN_CBT_EPROPSEG(x)               ( (x) << 10u)
#define DRV_CAN_CBT_EPSEG2(x)                 ( (x) <<  0u)
#define DRV_CAN_CBT_EPSEG1(x)                 ( (x) <<  5u)
#define DRV_CAN_CBT_EPRESDIV(x)               ( (x) << 21u)
#define DRV_CAN_CBT_ERJW(x)                   ( (x) << 16u)

#define DRV_CAN_FDCBT_FPROPSEG(x)             ( (x) << 10u)
#define DRV_CAN_FDCBT_FPSEG2(x)               ( (x) <<  0u)
#define DRV_CAN_FDCBT_FPSEG1(x)               ( (x) <<  5u)
#define DRV_CAN_FDCBT_FRJW(x)                 ( (x) << 16u)

#define DRV_CAN_FDCTRL_MBDSR0(x)              ( (x) << 16u)
#define DRV_CAN_FDCTRL_MBDSR1(x)              ( (x) << 19u)
#define DRV_CAN_FDCTRL_MBDSR2(x)              ( (x) << 22u)
#define DRV_CAN_FDCTRL_FDRATE                 (0x1u << 31u)
#define DRV_CAN_FDCTRL_TDCOFF(x)              ( (x) <<  8u)
#define DRV_CAN_FDCTRL_TDCEN                  (0x1u << 15u)

#define DRV_CAN_MB_DLC                        (0xFu << 16u)
#define DRV_CAN_MB_IDE                        (0x1u << 21u)
#define DRV_CAN_MB_SRR                        (0x1u << 22u)
#define DRV_CAN_MB_CODE                   	  (0xFu << 24u)
#define DRV_CAN_MB_BRS                        (0x1u << 30u)
#define DRV_CAN_MB_EDL                        (0x1u << 31u)
#define DRV_CAN_MB_ABORT_CODE                 (0x9u << 24u)

/* CAN FD extended data length DLC encoding */
#define DRV_CAN_MB_DLC_12_BYTES                9u
#define DRV_CAN_MB_DLC_16_BYTES                10u
#define DRV_CAN_MB_DLC_20_BYTES                11u
#define DRV_CAN_MB_DLC_24_BYTES                12u
#define DRV_CAN_MB_DLC_32_BYTES                13u
#define DRV_CAN_MB_DLC_48_BYTES                14u
#define DRV_CAN_MB_DLC_64_BYTES                15u

#define DRV_CAN_ESR1_ERRINT 	              (0x1u << 1u )
#define DRV_CAN_ESR1_BOFFINT 	              (0x1u << 2u )
#define DRV_CAN_ESR1_BOFFDONEINT 	          (0x1u << 19u )

/** CAN - Register Layout Typedef */
typedef volatile struct {
  uint32 MCR;                               /**< Module Configuration Register, offset: 0x0 */
  uint32 CTRL1;                             /**< Control 1 register, offset: 0x4 */
  uint32 TIMER;                             /**< Free Running Timer, offset: 0x8 */
       uint8 RESERVED_0[4];
  uint32 RXMGMASK;                          /**< Rx Mailboxes Global Mask Register, offset: 0x10 */
  uint32 RX14MASK;                          /**< Rx 14 Mask register, offset: 0x14 */
  uint32 RX15MASK;                          /**< Rx 15 Mask register, offset: 0x18 */
  uint32 ECR;                               /**< Error Counter, offset: 0x1C */
  uint32 ESR1;                              /**< Error and Status 1 register, offset: 0x20 */
  uint32 IMASK2;                            /**< Interrupt Masks 2 register, offset: 0x24 */
  uint32 IMASK1;                            /**< Interrupt Masks 1 register, offset: 0x28 */
  uint32 IFLAG2;                            /**< Interrupt Flags 2 register, offset: 0x2C */
  uint32 IFLAG1;                            /**< Interrupt Flags 1 register, offset: 0x30 */
  uint32 CTRL2;                             /**< Control 2 register, offset: 0x34 */
  uint32 ESR2;                              /**< Error and Status 2 register, offset: 0x38 */
       uint8 RESERVED_1[8];
  uint32 CRCR;                              /**< CRC Register, offset: 0x44 */
  uint32 RXFGMASK;                          /**< Rx FIFO Global Mask register, offset: 0x48 */
  uint32 RXFIR;                             /**< Rx FIFO Information Register, offset: 0x4C */
  uint32 CBT;                               /**< CAN Bit Timing Register, offset: 0x50 */
       uint8 RESERVED_2[24];
  uint32 IMASK3;                            /**< Interrupt Masks 3 Register, offset: 0x6C */
       uint8 RESERVED_3[4];
  uint32 IFLAG3;                            /**< Interrupt Flags 3 Register, offset: 0x74 */
       uint8 RESERVED_4[8];
  uint32 RAMn[CAN_RAMn_COUNT];              /**< Embedded RAM, array offset: 0x80, array step: 0x4 */
       uint8 RESERVED_5[512];
  uint32 RXIMR[CAN_RXIMR_COUNT];            /**< Rx Individual Mask Registers, array offset: 0x880, array step: 0x4 */
       uint8 RESERVED_6[256];
  uint32 CTRL1_PN;                          /**< Pretended Networking Control 1 Register, offset: 0xB00 */
  uint32 CTRL2_PN;                          /**< Pretended Networking Control 2 Register, offset: 0xB04 */
  uint32 WU_MTC;                            /**< Pretended Networking Wake Up Match Register, offset: 0xB08 */
  uint32 FLT_ID1;                           /**< Pretended Networking ID Filter 1 Register, offset: 0xB0C */
  uint32 FLT_DLC;                           /**< Pretended Networking DLC Filter Register, offset: 0xB10 */
  uint32 PL1_LO;                            /**< Pretended Networking Payload Low Filter 1 Register, offset: 0xB14 */
  uint32 PL1_HI;                            /**< Pretended Networking Payload High Filter 1 Register, offset: 0xB18 */
  uint32 FLT_ID2_IDMASK;                    /**< Pretended Networking ID Filter 2 Register / ID Mask Register, offset: 0xB1C */
  uint32 PL2_PLMASK_LO;                     /**< Pretended Networking Payload Low Filter 2 Register / Payload Low Mask Register, offset: 0xB20 */
  uint32 PL2_PLMASK_HI;                     /**< Pretended Networking Payload High Filter 2 low order bits / Payload High Mask Register, offset: 0xB24 */
       uint8 RESERVED_7[24];
  struct {                                         /* offset: 0xB40, array step: 0x10 */
      uint32 WMBn_CS;                           /**< Wake Up Message Buffer Register for C/S, array offset: 0xB40, array step: 0x10 */
      uint32 WMBn_ID;                           /**< Wake Up Message Buffer Register for ID, array offset: 0xB44, array step: 0x10 */
      uint32 WMBn_D03;                          /**< Wake Up Message Buffer Register for Data 0-3, array offset: 0xB48, array step: 0x10 */
      uint32 WMBn_D47;                          /**< Wake Up Message Buffer Register Data 4-7, array offset: 0xB4C, array step: 0x10 */
  } WMB[CAN_WMB_COUNT];
       uint8 RESERVED_8[128];
  uint32 FDCTRL;                            /**< CAN FD Control Register, offset: 0xC00 */
  uint32 FDCBT;                             /**< CAN FD Bit Timing Register, offset: 0xC04 */
  uint32 FDCRC;                             /**< CAN FD CRC Register, offset: 0xC08 */
} Can_RegType;

/* Message buffer structure type */
typedef struct
{
	uint32 CONFIG;
	uint32 ID;
	uint8  *DATA[];
}Can_MbType;

/* CAN message buffer CODE FOR buffers */
typedef enum
{
	CAN_TX_INACTIVE = 0x08u,             /* MB is inactive. */
	CAN_TX_ABORT    = 0x09u,             /* MB is aborted. MB does not participate in arbitration process. */
	CAN_TX_DATA     = 0x0Cu,			 /* MB is a TX Data Frame(MB RTR must be 0). */
    CAN_RX_EMPTY    = 0x04u              /* MB is inactive. */
} CAN_Msgbuff_Code_Type;

/*  HardWare */
typedef Can_RegType *Can_Hw_RegTypeRef;

typedef Can_MbType *Can_Hw_MbTypeRef;

#endif
