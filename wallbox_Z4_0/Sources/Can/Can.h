/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can.h>
 *  brief      <>
 *
 *  <MCU: MPC5746C >
 *
 *  author     <>
 *  date       <2018-03-11 >
 */
/*============================================================================*/

#ifndef CAN_H_
#define CAN_H_
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Can_GeneralTypes.h"
#include "Can_Irq.h"
#include "Can_Reg.h"
#include "Can_Cfg.h"
#include "Can_Hw.h"

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define CAN_MODULE_ID            80U
#define CAN_H_VENDOR_ID          62U
#define CAN_H_AR_MAJOR_VERSION   4U
#define CAN_H_AR_MINOR_VERSION   2U
#define CAN_H_AR_PATCH_VERSION   2U
#define CAN_H_SW_MAJOR_VERSION   1U
#define CAN_H_SW_MINOR_VERSION   0U
#define CAN_H_SW_PATCH_VERSION   1U

#define CAN_INSTANCE_ID                   (1U)

#if(STD_ON == CAN_DEV_ERROR_DETECT)

/* Service (API) ID for DET reporting */
#define CAN_INIT_ID                            0x00U
#define CAN_MAINFUNCTION_WRITE_ID              0x01U
#define CAN_SETCONTROLLERMODE_ID               0x03U
#define CAN_DISABLECONTROLLERINTERRUPT_ID      0x04U
#define CAN_ENABLECONTROLLERINTERRUPT_ID       0x05U
#define CAN_WRITE_ID                           0x06U
#define CAN_GETVERSIONINFO_ID                  0x07U
#define CAN_MAINFUNCTION_READ_ID               0x08U
#define CAN_MAINFUNCTION_BUSOFF_ID             0x09U
#define CAN_MAINFUNCTION_WAKEUP_ID             0x0AU
#define CAN_CBK_CHECKWAKEUP_ID                 0x0BU
#define CAN_SETBAUDRATE_ID                     0x0FU
/* Error Detection */
/* API parameter checking */
#define CAN_E_PARAM_POINTER                    0x01U
#define CAN_E_PARAM_HANDLE                     0x02U
#define CAN_E_PARAM_DLC                        0x03U
#define CAN_E_PARAM_CONTROLLER                 0x04U
/* CAN state checking */
#define CAN_E_UNINIT                           0x05U
#define CAN_E_TRANSITION                       0x06U
#define CAN_E_DATALOST				           0x07U
#define CAN_E_PARAM_BAUDRATE				   0x08U
#define CAN_E_ICOM_CONFIG_INVALID			   0x09U
#define CAN_E_INIT_FAILED				   	   0x0AU

#endif

#define CAN_MAX_DATA_LENGTH	                   8u
#define CANFD_MAX_DATA_LENGTH	               64u

extern const Can_ConfigType Can_ConfigSet;
extern CanControllerStatusType CanControllerStatus[CAN_MAX_CONTROLLERS];

Can_ReturnType Can_StartMode(uint8 Controller);
Can_ReturnType Can_StopMode(uint8 Controller);
Can_ReturnType Can_SleepMode(uint8 Controller);
Can_ReturnType Can_WakeUpMode(uint8 Controller);
/* SWS_Can_00223 */
extern void Can_Init(const Can_ConfigType* Config);

extern void Can_Deinit(void);

#if (CAN_VERSION_INFO_API == STD_ON)
/* SWS_Can_00224 */
void Can_GetVersionInfo(Std_VersionInfoType* VersionInfo);
#endif

/* SWS_Can_00491 */
Std_ReturnType Can_SetBaudrate(uint8 Controller, uint16 BaudRateConfigID);

/* SWS_Can_00230 */
Can_ReturnType Can_SetControllerMode( uint8 Controller, Can_StateTransitionType Transition);

/* SWS_Can_00231 */
void Can_DisableControllerInterrupts(uint8 Controller);

/* SWS_Can_00232 */
void Can_EnableControllerInterrupts(uint8 Controller);

/* SWS_Can_00360 */
Can_ReturnType Can_CheckWakeup(uint8 Controller);

/* SWS_Can_00233 */ 
Can_ReturnType Can_Write(Can_HwHandleType Hth, const Can_PduType* PduInfo);

/* SWS_Can_00225 */ 
void Can_MainFunction_Write(void);

/* SWS_Can_00226 */ 
void Can_MainFunction_Read(void);

/* SWS_Can_00227 */ 
void Can_MainFunction_BusOff(void);

/* SWS_Can_00228 */ 
void Can_MainFunction_Wakeup(void);

/* SWS_Can_00368 */ 
void Can_MainFunction_Mode(void);

#endif
