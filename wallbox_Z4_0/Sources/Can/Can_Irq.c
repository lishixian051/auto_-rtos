/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can_Irq.c>
 *  brief      < >
 *
 * <Compiler:     MCU:MPC5746C >
 *
 *  author     <yongxing.jia>
 *  date       <03-12-2018>
 */
/*============================================================================*/

/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "Can_Irq.h"
#include "Can.h"
#include "MPC5746C.h"
#include "CanIf_Cbk.h"
#include "interrupt_manager.h"

extern const Can_ConfigType *Can_ConfigPtr;
static uint32 Obtain_MB_Iflag_Status(uint8 Controller,uint8 EndID);
static uint8 Obtain_MB_Actual_StartID_EndID(uint8 MB_ID);
void Clean_Current_Interrupt_flag(uint8 Controller, uint8 Can_Controller_EndID, uint8 Mb_Index);


#ifdef CAN_CONTROLLER_HW_0
void CAN0_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

    MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN0_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

    MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN0_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN0_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN0_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}


void CAN0_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN0_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER0_BUSOFF_INTERRUPT == STD_ON)
void CAN0_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_0;

	CAN_Busoff_IRQHandle(Controller);

}
#endif

#endif

#ifdef CAN_CONTROLLER_HW_1

void CAN1_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN1_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN1_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}


void CAN1_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN1_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN1_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN1_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER1_BUSOFF_INTERRUPT == STD_ON)
void CAN1_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_1;

	CAN_Busoff_IRQHandle(Controller);
}
#endif

#endif

#ifdef  CAN_CONTROLLER_HW_2
void CAN2_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN2_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN2_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN2_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN2_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN2_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN2_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER2_BUSOFF_INTERRUPT == STD_ON)
void CAN2_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_2;

	CAN_Busoff_IRQHandle(Controller);

}

#endif

#endif

#ifdef  CAN_CONTROLLER_HW_3
void CAN3_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN3_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN3_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN3_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN3_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN3_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN3_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER3_BUSOFF_INTERRUPT == STD_ON)
void CAN3_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_3;

	CAN_Busoff_IRQHandle(Controller);
}

#endif

#endif

#ifdef  CAN_CONTROLLER_HW_4
void CAN4_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN4_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN4_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN4_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN4_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN4_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN4_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER4_BUSOFF_INTERRUPT == STD_ON)
void CAN4_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_4;

	CAN_Busoff_IRQHandle(Controller);
}
#endif

#endif

#ifdef CAN_CONTROLLER_HW_5

void CAN5_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN5_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN5_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}


void CAN5_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN5_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN5_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN5_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER5_BUSOFF_INTERRUPT == STD_ON)
void CAN5_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_5;

	CAN_Busoff_IRQHandle(Controller);
}
#endif
#endif

#ifdef CAN_CONTROLLER_HW_6

void CAN6_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN6_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN6_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}


void CAN6_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN6_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN6_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN6_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

#if (CAN_CONTROLLER6_BUSOFF_INTERRUPT == STD_ON)
void CAN6_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_6;

	CAN_Busoff_IRQHandle(Controller);
}
#endif
#endif

#ifdef CAN_CONTROLLER_HW_7

void CAN7_ORed_00_03_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 0u;
	uint8 EndID = 3u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN7_ORed_04_07_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 4u;
	uint8 EndID = 7u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
void CAN7_ORed_08_11_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 8u;
	uint8 EndID = 11u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}


void CAN7_ORed_12_15_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 12u;
	uint8 EndID = 15u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN7_ORed_16_31_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 16u;
	uint8 EndID = 31u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN7_ORed_32_63_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 32u;
	uint8 EndID = 63u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}

void CAN7_ORed_64_95_MB_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;
	uint8 StartID = 64u;
	uint8 EndID = 95u;

	MB_00_95_IRQHandler(Controller, StartID, EndID);
}
#if (CAN_CONTROLLER7_BUSOFF_INTERRUPT == STD_ON)
void CAN7_ORed_IRQHandler(void)
{
	uint8 Controller = CAN_CONTROLLER_HW_7;

	CAN_Busoff_IRQHandle(Controller);
}
#endif

#endif

static uint32 Obtain_MB_Iflag_Status(uint8 Controller,uint8 EndID)
{
	uint32 iflag_Status = 0u;
    Can_Hw_RegTypeRef Can_Reg =
            (Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;

	if(EndID == 3u)
	{
		iflag_Status = ((Can_Reg->IFLAG1) & 0xfu);
	}
	else if(EndID == 7u)
	{
		iflag_Status = ((Can_Reg->IFLAG1) & 0xf0u);
	}
	else if(EndID == 11u)
	{
		iflag_Status = ((Can_Reg->IFLAG1) & 0xf00u);
	}
	else if(EndID == 15u)
	{
		iflag_Status = ((Can_Reg->IFLAG1) & 0xf000u);
	}
	else if(EndID == 31u)
	{
		iflag_Status = ((Can_Reg->IFLAG1) & 0xffff0000u);
	}
	else if(EndID == 63u)
	{
		iflag_Status = (Can_Reg->IFLAG2);
	}
	else if(EndID == 95u)
	{
		iflag_Status = (Can_Reg->IFLAG3);
	}
	else
	{

	}

	return iflag_Status;
}

static uint8 Obtain_MB_Actual_StartID_EndID(uint8 MB_ID)
{
	if(MB_ID < 32u)
	{
		MB_ID = MB_ID;
	}
	else if(MB_ID < 64u)
	{
		MB_ID = MB_ID - 32u;
	}
	else if(MB_ID < 96u)
	{
		MB_ID = MB_ID - 64u;
	}
	else
	{

	}

	return MB_ID;
}

void Clean_Current_Interrupt_flag(uint8 Controller, uint8 Can_Controller_EndID, uint8 Mb_Index)
{
	Can_Hw_RegTypeRef Can_Reg =
			(Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;

	if(Can_Controller_EndID < 32u)
	{
		Can_Reg->IFLAG1 |= 1u << Mb_Index;
	}
	else if(Can_Controller_EndID < 64u)
	{
		Can_Reg->IFLAG2 |= 1u << Mb_Index;
	}
	else if(Can_Controller_EndID < 96u)
	{
		Can_Reg->IFLAG3 |= 1u << Mb_Index;
	}
	else
	{

	}
}
void MB_00_95_IRQHandler(uint8 Controller, uint8 StartID, uint8 EndID)
{
	Can_PduType Pdu;
	uint8 RxData[64u] = {0u};
	uint8 Mb_Index = 0u;
	uint8 i = 0u;
	uint8 Can_Controller_StartID = 0u;
	uint8 Can_Controller_EndID = 0u;
	uint32 Can_Controller_Actual_MB_ID = 0u;

	PduInfoType PduInfo;
	Can_HwType Mailbox;
	uint32 iflag_Status = 0u;
	uint32 Can_HardwareObject_Index = 0u;

	Can_Controller_EndID = EndID;
	Can_Controller_StartID = StartID;
    iflag_Status = Obtain_MB_Iflag_Status(Controller, EndID);

    StartID = Obtain_MB_Actual_StartID_EndID(StartID);
    EndID   = Obtain_MB_Actual_StartID_EndID(EndID);

	for(i = 0; i < Controller; i++)
	{
		Can_HardwareObject_Index += Can_ConfigPtr->CanController[i].CanControllerMaxCounter;
	}

    Can_HardwareObject_Index = Can_HardwareObject_Index + Can_Controller_StartID;

	for(Mb_Index = StartID; Mb_Index <= EndID; Mb_Index++)
	{
		if (((iflag_Status >> Mb_Index) & 1u) == 1u)
		{
			if(Can_ConfigPtr->Can_HardwareObject[Can_HardwareObject_Index].CanObjectType == CAN_OBJECT_TYPE_RECEIVE)
			{

				Can_Controller_Actual_MB_ID = Can_HardwareObject_Index - Can_ConfigPtr->CanController[Controller].CanRxStart;

				Pdu.sdu = RxData;
				Can_Hw_Get_MB_Info(Controller, Can_Controller_Actual_MB_ID, &Pdu);
				/* Clean current interrupt flag */
				Clean_Current_Interrupt_flag(Controller, Can_Controller_EndID, Mb_Index);

				PduInfo.SduDataPtr = Pdu.sdu;
				PduInfo.SduLength = Pdu.length;
				Mailbox.CanId = Pdu.id;
				Mailbox.ControllerId = Controller;
				Mailbox.Hoh = (Can_HwHandleType)(Can_HardwareObject_Index);
				CanIf_RxIndication(&Mailbox, &PduInfo);
		    }
			else if(Can_ConfigPtr->Can_HardwareObject[Can_HardwareObject_Index].CanObjectType == CAN_OBJECT_TYPE_TRANSMIT)
			{
				/* Clean current interrupt flag */
				Clean_Current_Interrupt_flag(Controller, Can_Controller_EndID, Mb_Index);

				CanIf_TxConfirmation(CanControllerStatus[Controller].TxPduHandles[Can_HardwareObject_Index]);
			}
			else
			{

			}
        }

		Can_HardwareObject_Index++;
	}
}

void CAN_Busoff_IRQHandle(uint8  Controller)
{
	Can_ReturnType ret;

	Can_Hw_RegTypeRef Can_Reg =
						(Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;
	if((Can_Reg->CTRL1 & DRV_CAN_ESR1_BOFFINT) == DRV_CAN_ESR1_BOFFINT)
	{
		ret = Can_StopMode(Controller);

		if(CAN_OK == ret)
		{
			CanIf_ControllerBusOff(Controller);

			Can_Hw_StartMode(Controller);
			Can_SetControllerMode(Controller,CAN_T_START);
			CanIf_SetControllerMode(Controller, CANIF_CS_STARTED);
			CanIf_SetPduMode(0,CANIF_ONLINE);
		}
		Can_Reg->CTRL1  |= DRV_CAN_ESR1_BOFFINT;  /* 21657 */
	}

}


void Can_Hw_EnableInterrupt(uint8 Controller)
{
	Can_Hw_RegTypeRef Can_Reg =
	            (Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;
    uint8 Can_Controller_Mb_Index = 0u;

	Can_Reg->CTRL1 |= DRV_CAN_CTRL1_BOFFMSK;                                  /* Enable BUSOFF interrupt. */
	for(Can_Controller_Mb_Index = 0u; Can_Controller_Mb_Index < Can_ConfigPtr->CanController[Controller].CanControllerMaxCounter; Can_Controller_Mb_Index++)
	{
		if(Can_Controller_Mb_Index <= 31u)
		{
			Can_Reg->IMASK1 |= (1u << Can_Controller_Mb_Index);               /* Enable current MB interrupt. */
		}
		else if(Can_Controller_Mb_Index <= 63u)
		{
			Can_Reg->IMASK2 |= (1u << (Can_Controller_Mb_Index - 32u));       /* Enable current MB interrupt. */
		}
		else if(Can_Controller_Mb_Index <= 95u)
		{
			Can_Reg->IMASK3 |= (1u << (Can_Controller_Mb_Index - 64u));       /* Enable current MB interrupt. */
		}
		else
		{
			/* Do nothing */
		}
	}
}

void Can_Hw_DisableInterrupt(uint8 Controller)
{
	Can_Hw_RegTypeRef Can_Reg =
				(Can_Hw_RegTypeRef)Can_ConfigPtr->CanController[Controller].CanControllerBaseAddr;
	uint8 Can_Controller_Mb_Index = 0u;

	Can_Reg->CTRL1 &= ~(DRV_CAN_CTRL1_BOFFMSK);                                  /* Disable BUSOFF interrupt. */
	for(Can_Controller_Mb_Index = 0u; Can_Controller_Mb_Index < Can_ConfigPtr->CanController[Controller].CanControllerMaxCounter; Can_Controller_Mb_Index++)
	{
		if(Can_Controller_Mb_Index <= 31u)
		{
			Can_Reg->IMASK1 &= (~(1u << Can_Controller_Mb_Index));               /* Disable current MB interrupt. */
		}
		else if(Can_Controller_Mb_Index <= 63u)
		{
			Can_Reg->IMASK2 &= (~(1u << (Can_Controller_Mb_Index - 32u)));       /* Disable current MB interrupt. */
		}
		else if(Can_Controller_Mb_Index <= 95u)
		{
			Can_Reg->IMASK3 &= (~(1u << (Can_Controller_Mb_Index - 64u)));       /* Disable current MB interrupt. */
		}
		else
		{
			/* Do nothing */
		}
	}
}

void Can_EnableIRQ(void)
{

#ifdef  CAN_CONTROLLER_HW_0
	INT_SYS_EnableIRQ(CAN0_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN0_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_1
	INT_SYS_EnableIRQ(CAN1_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN1_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_2
	INT_SYS_EnableIRQ(CAN2_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN2_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_3
	INT_SYS_EnableIRQ(CAN3_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN3_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_4
	INT_SYS_EnableIRQ(CAN4_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN4_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_5
	INT_SYS_EnableIRQ(CAN5_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN5_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_6
	INT_SYS_EnableIRQ(CAN6_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN6_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_7
	INT_SYS_EnableIRQ(CAN7_ORed_00_03_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_04_07_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_08_11_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_12_15_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_16_31_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_32_63_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_64_95_MB_IRQn);
	INT_SYS_EnableIRQ(CAN7_ORed_IRQn);
#endif
}

void Can_DisableIRQ(void)
{

#ifdef  CAN_CONTROLLER_HW_0
	INT_SYS_DisableIRQ(CAN0_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN0_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_1
	INT_SYS_DisableIRQ(CAN1_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN1_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_2
	INT_SYS_DisableIRQ(CAN2_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN2_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_3
	INT_SYS_DisableIRQ(CAN3_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN3_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_4
	INT_SYS_DisableIRQ(CAN4_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN4_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_5
	INT_SYS_DisableIRQ(CAN5_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN5_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_6
	INT_SYS_DisableIRQ(CAN6_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN6_ORed_IRQn);
#endif

#ifdef  CAN_CONTROLLER_HW_7
	INT_SYS_DisableIRQ(CAN7_ORed_00_03_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_04_07_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_08_11_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_12_15_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_16_31_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_32_63_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_64_95_MB_IRQn);
	INT_SYS_DisableIRQ(CAN7_ORed_IRQn);
#endif
}


