/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       <Can_Irq.c>
 *  brief      < >
 *
 * <Compiler:     MCU: MPC5746C>
 *
 *  author     <>
 *  date       <03-12-2018>
 */
/*============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============*/
#ifndef CAN_GENERALTYPES_H_
#define CAN_GENERALTYPES_H_

#include "ComStack_Types.h"
#include "Can_Reg.h"
#include "TypeDef.h"

#define CAN_TYPES_H_AR_MAJOR_VERSION   4
#define CAN_TYPES_H_AR_MINOR_VERSION   2
#define CAN_TYPES_H_AR_PATCH_VERSION   2
#define CAN_TYPES_H_SW_MAJOR_VERSION   1
#define CAN_TYPES_H_SW_MINOR_VERSION   0
#define CAN_TYPES_H_SW_PATCH_VERSION   0

/* Enumeration Interrupt or Polling mode */
typedef enum
{
	CAN_PROCESSING_POLLING = 0u,
    CAN_PROCESSING_INTERRUPT
}Can_ProcessingType;

/* Specifies the type (Full-Can or Basic-CAN) of a hardware object */
typedef enum
{
    /* several L_PDUs*/
    CAN_HANDLE_TYPE_BASIC = 0u,
    /* only one L_PDU*/
    CAN_HANDLE_TYPE_FULL
}Can_HohType;

typedef enum
{
	/* standard id */
	CAN_ID_TYPE_STANDARD = 0u,
    /* extended id */
    CAN_ID_TYPE_EXTENDED,
    /* mixed id */
    CAN_ID_TYPE_MIXED
}Can_IdTypeType;

typedef enum
{
    CAN_OBJECT_TYPE_RECEIVE = 0u,
    CAN_OBJECT_TYPE_TRANSMIT
}Can_ObjectTypeType;

/* SWS_Can_00429 TODO:*/
typedef uint16 Can_HwHandleType;

/* SWS_Can_00417 */
/* State transitions that are used by the function CAN_SetControllerMode. */
typedef enum
{
	CAN_T_UNINIT = 0,
    CAN_T_START,
    CAN_T_STOP,
    CAN_T_SLEEP,
    CAN_T_WAKEUP
}Can_StateTransitionType;

typedef enum
{
	CAN_8_BYTES_PAYLOAD = 0u,
	CAN_16_BYTES_PAYLOAD,
	CAN_32_BYTES_PAYLOAD,
	CAN_64_BYTES_PAYLOAD
}Can_PayloadType;

/* SWS_Can_00039 */
/* Return value of CAN Driver API */
typedef enum
{
    CAN_OK = 0u,
    CAN_NOT_OK,
    CAN_BUSY
}Can_ReturnType;

typedef enum
{
	RX = 0u,
	TX = 1u
}Can_MBDirectionType;

/* SWS_Can_00416 TODO:*/
typedef uint32 Can_IdType;

/* SWS_CAN_00496 */
typedef struct
{
	Can_IdType        CanId;
	Can_HwHandleType  Hoh;
	uint8             ControllerId;
}Can_HwType;

/*This type unites PduId (swPduHandle), SduLength (length), SduData (sdu), and CanId (id) for any CAN L-SDU.*/
typedef struct
{
	PduIdType     swPduHandle;
	uint8         length;
	Can_IdType    id;
	uint8*        sdu;
}Can_PduType;

typedef struct
{
	boolean CanControllerFdEnable;
	uint32  CanControllerFdBaudRate;           /* 0 .. 2000kbps */
	uint32  CanControllerFdCbtRegister;
	boolean CanControllerTxBitRateSwitch;
	boolean CanControllerTDCEnable;
	uint16	CanControllerTrcvDelayCompensationOffset;
}Can_ControllerFdBaudrateConfigType;

typedef struct
{
	uint32  CanControllerBaudRateConfigID;
	uint16	CanControllerBaudRate;	  /* 0 .. 2000kbps */
	uint32  CanControllerBaudRateReg;
	const Can_ControllerFdBaudrateConfigType CanControllerFdBaudrateConfig;
}Can_ControllerBaudrateConfigType;

typedef struct
{
	const uint32	CanControllerBaseAddr;
    const uint8		CanControllerId;
    const uint32    CanControllerMaxCounter;
	const boolean  	CanControllerActivation;
	const Can_ProcessingType CanBusoffProcessing;
	const Can_ProcessingType CanRxProcessing;
	const Can_ProcessingType CanTxProcessing;
    #if(STD_ON == CAN_WAKEUP_SUPPORT)
		const Can_ProcessingType CanWakeupProcessing;                   /* CanWakeupProcessing */
		uint8	CanWakeupSourceRef;
    #endif /* #if(STD_ON == CAN_WAKEUP_SUPPORT) */
	const Can_ControllerBaudrateConfigType* CanControllerBaudrateConfig;
	const Can_ControllerBaudrateConfigType* CanControllerBaudrateDefaultConfig;
	const boolean  	CanFD_Support;
	const Can_PayloadType CanControllerPayload;
	const uint8        CanControllerMaxMessageBuffer;
    uint8              CanRxStart;
    uint8              CanRxEnd;
    uint8              CanTxStart;
    uint8              CanTxEnd;
}Can_ControllerConfigType;

typedef struct
{
    const Can_HohType  CanHandleType;
    Can_IdTypeType     CanIdType;
    const Can_IdType   CanIdValue;
    uint32             CanObjectId;
    const uint32       CanFilterMask;
    Can_ObjectTypeType CanObjectType;
    const uint8        CanControllerId;
    uint32             CanHwObjectCount;
}Can_HardwareObjectType;

/* This container contains the parameter related each CAN Driver Unit */
typedef struct
{
	const Can_ControllerConfigType* CanController;
	const Can_HardwareObjectType* Can_HardwareObject;
}Can_ConfigType;

typedef struct
{
	uint16 CanMainFunctionReadPeriod;
	uint16 CanMainFunctionWritePeriod;
}Can_MainFunction_RWPeriodsType;

typedef struct
{
	boolean CanDevErrorDetection;
	uint8 	CanIndex;
	boolean CanSetBaudrateApi;
	boolean CanVersionInfoApi;
}Can_GeneralType;

typedef struct
{
	Can_StateTransitionType CntrlMode;/* controller mode */
	uint32                 IntLockCount;
	PduIdType 				TxPduHandles[CAN_MAX_MB];
	boolean                 WakeupStatus;
}CanControllerStatusType;

/*Operating modes of the CAN Transceiver Driver*/
typedef enum
{
	CANTRCV_TRCVMODE_NORMAL = 0, /*Transceiver mode NORMAL*/
	CANTRCV_TRCVMODE_SLEEP,      /*Transceiver mode SLEEP*/
	CANTRCV_TRCVMODE_STANDBY     /*Transceiver mode STANDBY*/
} CanTrcv_TrcvModeType;

/*This type shall be used to control the CAN transceiver concerning wake up
 * events and wake up notifications*/
typedef enum
{
	CANTRCV_WUMODE_ENABLE = 0, /*notification for wakeup events enabled on the
	                            addressed transceiver*/
	CANTRCV_WUMODE_DISABLE,    /*notification for wakeup events disabled on the
	                            addressed transceiver*/
	CANTRCV_WUMODE_CLEAR       /*A stored wakeup event cleared on the addressed
	                            transceiver*/
} CanTrcv_TrcvWakeupModeType;

/*This type denotes the wake up reason detected by the CAN transceiver in detail*/
typedef enum
{
	CANTRCV_WU_ERROR = 0,     /*Due to an error wake up reason was not detected.
	                           This value may only be reported when error was
                               reported to DEM before*/
	CANTRCV_WU_NOT_SUPPORTED, /*The transceiver not support any information for
	                           the wake up reason*/
	CANTRCV_WU_BY_BUS,        /*The transceiver has detected, that the network
	                           has caused the wake up of the ECU*/
	CANTRCV_WU_INTERNALLY,    /*The transceiver has detected that the network
	                          has woken up by the ECU via a request to NORMAL mode*/
	CANTRCV_WU_RESET,         /*The transceiver has detected, that the "wake up"
	                           is due to an ECU reset*/
	CANTRCV_WU_POWER_ON,      /*The transceiver has detected, that the "wake up"
	                           is due to an ECU reset after power on*/
	CANTRCV_WU_BY_PIN,        /*The transceiver has detected a wake-up event
	                           at one of the transceiver's pins(not at the CAN bus)*/
	CANTRCV_WU_BY_SYSERR      /*The transceiver has detected, that the wake up
	                          of the ECU was caused by a HW related device failure*/
} CanTrcv_TrcvWakeupReasonType;

#endif /* CAN_TYPES_H_ */
