/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : ComM_MemMap.h                                               **
**                                                                            **
**  Created on  : 2018-08-12                                                  **
**  Author      : ss                                                          **
**  Vendor      :                                                             **
**  DESCRIPTION : Memory mapping abstraction declaration of ComM              **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define COMM_MEMMAP_VENDOR_ID  62u
#define COMM_MEMMAP_MODULE_ID  195u
#define COMM_MEMMAP_AR_MAJOR_VERSION  4u
#define COMM_MEMMAP_AR_MINOR_VERSION  2u
#define COMM_MEMMAP_AR_PATCH_VERSION  2u
#define COMM_MEMMAP_SW_MAJOR_VERSION  1u
#define COMM_MEMMAP_SW_MINOR_VERSION  0u
#define COMM_MEMMAP_SW_PATCH_VERSION  0u
#define COMM_MEMMAP_VENDOR_API_INFIX  0u


#include "MemMap.h"

/*=======[E N D   O F   F I L E]==============================================*/
