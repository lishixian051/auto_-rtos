/************************************************************************
*				        ESEC UESTC
* 	 Copyright (C) 2005-2011 ESEC UESTC. All Rights Reserved.
***********************************************************************/

/******************************* references ************************************/
#include "ComM_Dcm.h"
#include "Dcm_Cbk.h"

#if 1
uint8  Dcm_ComM_ActiveDiagnostic = 0u;
/**************************************
 * 函数原型:  ComM_DCM_ActiveDiagnostic
 * 功能描述:  通知ComM模块进入“FULL COMMUNICATION”状态
 * 服务ID:    0x1f
 ************************************/
void ComM_DCM_ActiveDiagnostic(NetworkHandleType Channel)
{
    Dcm_ComM_ActiveDiagnostic++;
    Dcm_ComM_FullComModeEntered(Channel);
    return;
}

/**************************************
 * 函数原型:  ComM_DCM_InactiveDiagnostic
 * 功能描述:  通知ComM模块退出“FULL COMMUNICATION”状态
 * 服务ID:    0x20
 ************************************/
void ComM_DCM_InactiveDiagnostic(NetworkHandleType Channel)
{
    Dcm_ComM_ActiveDiagnostic--;
    Dcm_ComM_NoComModeEntered(Channel);
    return;
}
#endif

/* add a function interface to JUNSHEN  2020.05.25 */
uint8 ReturnDcm_ComM_ActiveDiagnostic(void)
{
	return Dcm_ComM_ActiveDiagnostic;
}
