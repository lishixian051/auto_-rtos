/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : ComM_Dcm.h                                                  **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION : Callback declaration of COMM for DCM                        **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef COMM_DCM_H
#define COMM_DCM_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "ComStack_Types.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/

/*=======[V E R S I O N  I N F O R M A T I O N]===================================================*/
#define COMM_DCM_H_AR_MAJOR_VERSION  4U
#define COMM_DCM_H_AR_MINOR_VERSION  2U
#define COMM_DCM_H_AR_PATCH_VERSION  0U
#define COMM_DCM_H_SW_MAJOR_VERSION  1U
#define COMM_DCM_H_SW_MINOR_VERSION  0U
#define COMM_DCM_H_SW_PATCH_VERSION  0U


/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/



/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/



/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define COMM_START_SEC_DCM_CODE
#include "ComM_MemMap.h"
/**
 * Indication of active diagnostic by the DCM.
 * Service ID: 0x1f
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): Channel, Channel needed for Diagnostic communication
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, COMM_DCM_CODE)
ComM_DCM_ActiveDiagnostic(
    NetworkHandleType Channel
);

/**
 * Indication of inactive diagnostic by the DCM.
 * Service ID: 0x20
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): Channel, Channel no longer needed for Diagnostic communication
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, COMM_DCM_CODE)
ComM_DCM_InactiveDiagnostic(
    NetworkHandleType Channel
);
/* add a function interface to JUNSHEN  2020.05.25 */
extern uint8 ReturnDcm_ComM_ActiveDiagnostic(void);

#define COMM_STOP_SEC_DCM_CODE
#include "ComM_MemMap.h"

#endif /* COMM_DCM_H */
