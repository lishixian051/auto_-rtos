/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <FreeRTimer.h>
 *  @brief       <>
 *
 *  < Compiler:S32DS for ARM 2018.R1     MCU:S32K144/S32K146 >
 *
 *  @author     <chen maosen>
 *  @date       <2013-03-20>
 */
/*============================================================================*/

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>                         */
/*  V1.0.0       2013-3-20  chenms    Initial version                         */
/*============================================================================*/
#ifndef FREERTIMER_H
#define FREERTIMER_H

/****************************** references *********************************/
#include "Std_Types.h"
//#include "Os.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

/****************************** definitions *********************************/
#define     TICKS2MS_SystemTimer(ticks)    (ticks*1000/1000)
#define     MS2TICKS_SystemTimer(ms)       (ms*1000/1000)

#define GPT_5msCnt       5uL
#define GPT_10msCnt      10uL
#define GPT_100msCnt     100uL
#define GPT_1000msCnt    1000uL

#define TICK_DURATION 1000uL

typedef uint32 TickType;

/*=======[E X T E R N A L   V A R I A B L E   D E C L A R A T I O N S]========*/

extern boolean Gpt_1msFlag;

extern boolean Gpt_5msFlag;

extern boolean Gpt_10msFlag;

extern boolean Gpt_100msFlag;

extern boolean Gpt_1000msFlag;


/****************************** declarations *********************************/
extern uint32 Frt_ReadOutMS(void);
extern uint32 Frt_CalculateElapsedMS(uint32 OldCurMs);
extern void Run_msCounter(void);
extern Std_ReturnType GetCounterValue(uint16 id, TickType* tick);
Std_ReturnType GetElapsedCounterValue(uint16 id, TickType* tick, TickType* elapsed);



#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* FREERTIMER_H */
