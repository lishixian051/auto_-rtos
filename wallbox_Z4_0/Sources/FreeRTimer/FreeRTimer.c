/*============================================================================*/
/*  Copyright (C) 2009-2018,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  file       < FreeRTimer.c>
 *  brief      < FreeRTimer module file >
 *
 *  < Compiler:S32DS for ARM 2018.R1     MCU:S32K144/S32K146 >
 *
 *  author     < yongxing.jia >
 *  date       < 2018-12-12 >
 */
/*============================================================================*/
/******************************* references ************************************/
#include "FreeRTimer.h"


boolean Gpt_1msFlag;
boolean Gpt_5msFlag;
boolean Gpt_10msFlag;
boolean Gpt_100msFlag;
boolean Gpt_1000msFlag;


TickType msCounter = 0;

/****************************** implementations ********************************/
uint32 Frt_ReadOutMS(void)
{
	uint32  OSCurMs;

	(void)GetCounterValue(0, &OSCurMs);
	            /*tick to ms*/
	OSCurMs = TICKS2MS_SystemTimer(OSCurMs);

	return(OSCurMs);
}

uint32 Frt_CalculateElapsedMS(uint32 OldCurMs)
{
	uint32  ElapsedMs;

	OldCurMs = MS2TICKS_SystemTimer(OldCurMs);
    (void)GetElapsedCounterValue(0, &OldCurMs, &ElapsedMs);
    ElapsedMs = TICKS2MS_SystemTimer(ElapsedMs);

    return(ElapsedMs);
}

/*the function defined in an other file*/

void Run_msCounter(void)
{
    msCounter++;
}

Std_ReturnType GetCounterValue(uint16 id, TickType* tick)
{
	*tick = msCounter;
	return E_OK;
}

Std_ReturnType GetElapsedCounterValue(uint16 id, TickType* tick, TickType* elapsed)
{
	*elapsed = msCounter - *tick;
	*tick = msCounter;
	return E_OK;
}
