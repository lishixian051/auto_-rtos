/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : XcpOnCan.h                                           		  **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : Type declaration on Can				               		  **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/

#ifndef XCPONCAN_H_
#define XCPONCAN_H_

/*=======[R E V I S I O N   H I S T O R Y]====================================*/

/*============================================================================*/

/*=======[I N C L U D E S]====================================================*/
#include "CanIf.h"
/*=======[T Y P E   D E F I N I T I O N S]====================================*/
typedef struct {
    uint16 XcpLocalRxPduId;
} Xcp_RxPduType;

typedef struct {
    uint16 XcpLocalTxPduId;
    uint32 LowLayerTxPduId;
} Xcp_TxPduType;

typedef struct {
	P2CONST(Xcp_RxPduType, TYPEDEF, XCP_CONST_PBCFG)XcpRxPdu;
	P2CONST(Xcp_TxPduType, TYPEDEF, XCP_CONST_PBCFG)XcpTxPdu;
} Xcp_PduType;

#endif  /* endof XCPONCAN_H */

/*=======[E N D   O F   F I L E]==============================================*/

