/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Xcp.h                                                      **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration and type definitions of XCP                 **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef  XCP_H
#define  XCP_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define XCP_H_VENDOR_ID 		(uint16)62
#define XCP_H_MODULE_ID  		(uint16)212

#define XCP_H_AR_MAJOR_VERSION  4u
#define XCP_H_AR_MINOR_VERSION  2u
#define XCP_H_AR_PATCH_VERSION  2u
#define XCP_H_SW_MAJOR_VERSION  1u
#define XCP_H_SW_MINOR_VERSION  0u
#define XCP_H_SW_PATCH_VERSION  0u

#define XCP_INSTANCE_ID       	((uint8)(0u))
/*=======[I N C L U D E S]====================================================*/
#include "Xcp_Cfg.h"
#include "Xcp_Interface.h"
#if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
#include "Os.h"
#endif
#if (XCP_ON_CAN_ENABLE == STD_ON)
#include "XcpOnCan.h"
#include "XcpOnCan_Cbk.h"
#endif
#if ((XCP_ON_ETHERNET_ENABLE == STD_ON)||(XCP_ON_CCD_ENABLE == STD_ON))
#include "XcpOnEth.h"
#include "XcpOnEth_Cbk.h"
#endif
#if (XCP_ON_FLEXRAY_ENABLE == STD_ON)
#include "XcpOnFr.h"
#include "XcpOnFr_Cbk.h"
#endif
/*
 * Development errors used by XCP
 */
#define XCP_E_INV_POINTER				0x01u
#define XCP_E_NOT_INITIALIZED			0x02u
#define XCP_E_INVALID_PDUID				0x03u
#define XCP_E_INIT_FAILED				0x04u
#define XCP_E_PARAM_POINTER				0x12u
/*Service IDs*/
#define XCP_SERVICE_ID_INIT             	0x00u
#define XCP_SERVICE_ID_GET_VERSION_INFO  	0X01u
#define XCP_SERVICE_ID_TXCONFIRMATION   	0X02u
#define XCP_SERVICE_ID_RXINDICATION  	 	0x03u
#define XCP_SERVICE_ID_MAINFUNCTION    		0x04U
#define XCP_SERVICE_ID_SETTRANSMISSOINMODE 	0x03u
#define XCP_SERVICE_ID_TRIGGER_TRANSMIT  	0x41u
/*=======[M A C R O S]========================================================*/
#define XCP_PROTOCOL_VERSIOM_MAJOR              1
#define XCP_PROTOCOL_VERSIOM_MINOR              1
#define XCP_CAN_TRANSPORT_LAYER_VERION_MAJOR    1
#define XCP_CAN_TRANSPORT_LAYER_VERION_MINOR    1

/*=======[T Y P E   D E F I N I T I O N S]====================================*/
typedef struct
{
	uint8 numOfRxPdu;
	uint8 numberOfTxPdu;
	P2CONST(Xcp_PduType, TYPEDEF, XCP_CONST_PBCFG) XcpPduRef;
}Xcp_PbConfigType;
typedef enum
{
    XCP_TX_OFF,                     /*disable the transmission capability*/
    XCP_TX_ON                       /*enble the transmission capability*/
}Xcp_TransmissionModeType;

#if (XCP_GET_ID == STD_ON)
typedef struct
{
    uint8 type;
    uint8 mode;
    uint32 length;
    P2CONST(uint8, TYPEDEF, XCP_CONST_PBCFG) ptr;
}Xcp_IdInfoType;
#endif

#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
typedef struct	{
	uint8 XcpDtoPid;
	P2CONST(Xcp_PduType, TYPEDEF, XCP_CONST_PBCFG) XcpDto2PduMapping;
}XcpDtoType;

typedef struct	{
	uint8 XcpOdtEntryBitOffset;  /*now dont support*/
	uint8 XcpOdtEntryNumber;
    uint8 XcpOdtEntryLength;
	P2VAR(Xcp_AGType, TYPEDEF, XCP_VAR)XcpOdtEntryAddr;
}Xcp_EntryType;

typedef struct
{
    uint8 	XcpOdtEntryCount;
	uint8	XcpOdtNumber;
	P2VAR(Xcp_EntryType, TYPEDEF, XCP_VAR)XcpOdtEntry;
}Xcp_OdtType;

typedef struct	{
	uint16     XcpDaqListNumber;
	Xcp_DaqDirectionType    XcpDaqListType;
    #if (XCP_DAQ_STATIC == XCP_DAQ_CONFIG_TYPE)
    uint8      XcpMaxOdt;
    uint8      XcpMaxOdtEntries;
	#else
	uint8 		XcpDaqSize;
    #endif
    P2CONST(XcpDtoType, TYPEDEF, XCP_CONST_PBCFG) XcpDto;
    P2VAR(Xcp_OdtType, TYPEDEF, XCP_CONST_PBCFG) XcpOdt;
}Xcp_DaqListConfigType;

typedef struct	{
	P2CONST(uint8, TYPEDEF, XCP_CONST_PBCFG)
								XcpEvChNamePtr;
	uint8                       XcpEvChNameLength;
	Xcp_EvChConsistType 		XcpEvChConsistency;
	uint8                       XcpEvChMaxDaqList;
	uint16     					XcpEvChNumber;
	uint8                       XcpEvChPriority;
	uint8                       XcpEvChTimeCycle;
    XcpTimestampUnitType        XcpEvChTimeUnit;
	Xcp_DaqDirectionType        XcpEvChType;
    uint32                      XcpEvChBuffDpth; /* for consistency */
    P2VAR(Xcp_AGType, TYPEDEF, XCP_CONST_PBCFG)
                                XcpEvChBuffPtr;
	P2CONST(Xcp_DaqNumType, TYPEDEF, XCP_VAR)
                                XcpEvChTrigDaqListRef;
}Xcp_EvChConfigType;
#endif

typedef struct	{
	#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
    P2CONST(Xcp_DaqListConfigType, TYPEDEF, XCP_CONST_PBCFG) XcpDaqList;
    P2CONST(Xcp_EvChConfigType, TYPEDEF, XCP_CONST_PBCFG) XcpEvCh;
	#endif
	#if (STD_ON == XCP_GET_ID)
	uint8   Xcp_IdInfoNum;
	P2CONST(Xcp_IdInfoType, TYPEDEF, XCP_CONST_PBCFG)
			Xcp_IdInfoPtr;
	#endif
}Xcp_ConfigType;


#if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))
typedef struct
{
    uint32 size;
    uint32 ramStart;
    uint32 ramEnd;
    uint32 romStart;
    uint32 romEnd;
	#if ((STD_ON == XCP_PAG_SUPPORT) \
	&& (STD_ON == XCP_SWITCH_PAG_SUPPORT)\
	&& (STD_OFF == XCP_MMU_SUPPORT)\
	&& (STD_OFF == XCP_OVC_SUPPORT))
    uint32 bufferPtr;
    #endif
}Xcp_PageInfoType;
#endif /* (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE)) */

typedef struct
{
    #if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))
    uint8 maxSegNum;
    #endif
    uint32 measurementRomStart;	//new
    uint32 measurementRomEnd;	//new
    uint32 measurementRamStart;	//new
    uint32 measurementRamEnd;	//new
    #if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))
    P2CONST(Xcp_PageInfoType, AUTOMATIC, XCP_CONST_PBCFG) pageInfoPtr;
    #endif
}Xcp_SegmentInfoType;

#if (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))
/* Xcp program_clear type in functional access mode */
typedef enum
{
	XCP_FLASH_RANGE_CAL = 0x01u,
	XCP_FLASH_RANGE_CODE = 0x02u,
	XCP_FLASH_RANGE_NVRAM = 0x04u
}Xcp_FlashAearType;

/* xcp sector information,used in GET_SECTOR_INFO command */
typedef struct
{
    P2CONST(uint8, TYPEDEF, XCP_CONST_PBCFG) sectorNamePtr;      /**< SECTOR_NAME */
    uint8   sectorNameLength;   /**< SECTOR_NAME length */
    uint8   sectorNo;           /**< SECTOR_NUMBER */
    uint32  sectorclrStartAddr; /**< Address */
    uint32  sectorclrLength;       /**< Length  */
    VAR(Xcp_FlashAearType, TYPEDEF) rangeType; /**< flash range type */
    uint32  progStartAddr;   /**< Program Start Address */
    uint32  progDataSize;       /**< Program Data Size */
    uint8   sectorclrSequenceNo;  /**< CLEAR_SEQUENCE_NUMBER */
    uint8   sectorPgmSequenceNo;    /**< PROGRAM_SEQUENCE_NUMBER */
    uint8   sectorPgmMethod;        /**< PROGRAM_METHOD */
}Xcp_SectorInfoType;
#endif /* (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE)) */

/**********Post-Build configuration parameter declarations*************/
#define XCP_START_SEC_CONST_UNSPECIFIED
#include "XCP_MemMap.h"
extern const Xcp_PbConfigType Xcp_PBConfig;
extern const Xcp_PduType Xcp_Daq_Pdu[XCP_MAX_DAQ];
#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include "XCP_MemMap.h"

/*=======[E X T E R N A L   D A T A]==========================================*/
#define XCP_START_SEC_CONST_UNSPECIFIED
#include "XCP_MemMap.h"
extern CONST(Xcp_ConfigType,XCP_CONST_PBCFG) XcpConfig;


extern CONST(Xcp_SegmentInfoType,XCP_CONST_PBCFG) Xcp_SegmentInfo;

#if (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))
extern CONST(Xcp_SectorInfoType,XCP_CONST_PBCFG) Xcp_SectorInfo[XCP_MAX_SECTOR];
#endif
#define XCP_STOP_SEC_CONST_UNSPECIFIED
#include "XCP_MemMap.h"
/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

#define XCP_START_SEC_CODE
#include "XCP_MemMap.h"
extern FUNC(void,XCP_CODE) Xcp_Init(CONSTP2CONST(Xcp_ConfigType,
                            AUTOMATIC, XCP_APPL_DATA)Xcp_ConfigPtr);

extern FUNC(void,XCP_CODE) Xcp_MainFunction(void);

#if (XCP_VERSION_INFO_API == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetVersionInfo(P2VAR(Std_VersionInfoType,
                                          AUTOMATIC,XCP_APPL_DATA)versioninfo);
#endif

#if (XCP_SUPPRESS_TX_SUPPORTED == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_SetTransmissionMode(NetworkHandleType Channel,
                                               Xcp_TransmissionModeType Mode);
#endif

extern FUNC(void,XCP_CODE)
Xcp_EventIndication(uint16 eventNum);
#define XCP_STOP_SEC_CODE
#include "XCP_MemMap.h"

#endif  /* endof XCP_H */
