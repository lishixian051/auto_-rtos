/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Xcp_Interface.c                                             **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : Interface functions with other modules		              **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*=======[I N C L U D E S]====================================================*/
#include "Xcp_Interface.h"
#include "Xcp_Internal.h"
#if (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))
//#include "Fls.h"
#endif
#if (XCP_BUILD_CHECKSUM == STD_ON)
static uint16 XcpCrc16Ccitt(const uint8* pu1data, uint16 u2Crc);
static uint16 Xcp_GetCrc16Citt(uint8* data, uint16 crc);
#endif
#if (STD_ON == XCP_SEED_AND_UNLOCK )
Std_ReturnType Xcp_CreateSeed(uint8* length, uint8* seed, const uint8 resource)
{
    uint8 i;
    *length = resource;
    for (i = 0; i < *length; i++)
    {
        seed[i] = i;
    }

    return E_OK;
}

boolean Xcp_IsKeyRight(const uint8* seed, const uint8* key, const uint8 lengthS,const uint8 lengthK, const uint8 resource)
{
    uint8 i = 0;
    if ((lengthK == resource)
    && (lengthS == resource))
    {
        for (i = 0; i < lengthK; i++)
        {
            if (key[i] != (seed[i] + seed[i/2]))
            {
                break;
            }
        }
        if (resource != i)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
    else
    {
        return FALSE;
    }
}
#endif /* (STD_ON == XCP_SEED_AND_UNLOCK ) */

#if (STD_ON == XCP_BUILD_CHECKSUM)
static uint16 XcpCrc16Ccitt(const uint8* pu1data, uint16 u2Crc)
{
	uint8 CrCCounter;
	uint16 t_u2_ccitt16 = (uint16)0x1021u;
	u2Crc ^= ((uint16)(*pu1data) << 8u);
	for (CrCCounter = 0; CrCCounter < 8; CrCCounter++)
	{
		if ((u2Crc & (uint16)0x8000u) == (uint16)0x8000u)
		{
			u2Crc <<= 1;
			u2Crc ^= t_u2_ccitt16;
		}
		else
		{
			u2Crc <<= 1;
		}
	}
	return u2Crc;
}

/* Init the Xcp_Crc16CittTable */
void Xcp_InitCrc16CcittTable()
{
	uint16 i;
	for (i=0; i<256; i++)
	{
		Xcp_Crc16CittTable[i] = XcpCrc16Ccitt(&i, 0);
	}
}

static uint16 Xcp_GetCrc16Citt(uint8* data, uint16 crc)
{
	uint8 c;
	c = crc >> 8;
	crc <<= 8;
	crc ^= Xcp_Crc16CittTable[(*data) ^ c];
	return crc;
}

FUNC(void, XCP_CODE) Xcp_ChecksumCompute(const uint32 checksumStartAddress, const uint32 blockSize, uint32* crcResult)
{
	uint32 i;
	uint16 t_u2Result;
	t_u2Result = (uint16)0xffffu;

	for (i = 0; i < blockSize; i++)
	{
		t_u2Result = Xcp_GetCrc16Citt((uint8 *)(checksumStartAddress + i), t_u2Result);
	}
    *crcResult = (uint32)t_u2Result;

	return ;
}

#endif /* (STD_ON == XCP_BUILD_CHECKSUM) */

#if (STD_ON == XCP_SET_MTA)
FUNC(uint32, XCP_CODE) Xcp_Mta2Ptr(const uint8 u1AddExt, const uint32 u4Add)
{
	#if(STD_ON == XCP_OVC_SUPPORT)
    uint32 offSet = 0x0;
	#endif
    uint32 u4DestAddr = u4Add;

    return u4DestAddr;
}
#endif /* (STD_ON == XCP_SET_MTA) */

#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
extern Std_ReturnType Xcp_FlsReq(uint8 Mode, uint32 u4Address, uint32 u4Size, const uint8* pu1DataPtr)
{
    Std_ReturnType ret = E_OK;
	switch(Mode)
	{
		case XCP_FLS_WRITE:
			/* Fls_Write */
			break;

		case XCP_FLS_ERASE:
			/* Fls_Erase */
			break;

		default:
			/* Do nothing */
			break;
	}
    return ret;
}

uint8 Xcp_FlsGetStatus(void)
{
    uint8 result = E_OK;
    return result;
}

void Xcp_ProgramResetNoticeApp(void)
{
    return;
}
Std_ReturnType Xcp_ProgramStartNoticeApp(void)
{
    return E_OK;
}
#endif/*(XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))*/

#if ((STD_ON == XCP_SET_REQUEST) && (STD_ON == XCP_RESUME_SUPPORT))
Std_ReturnType Xcp_NvramWriteDaq(void)
{
	Std_ReturnType result = E_NOT_OK;

	return result;
}

Std_ReturnType Xcp_NvramWriteCal(void)
{
	Std_ReturnType result = E_NOT_OK;

	return result;
}
#endif /* ((STD_ON == XCP_SET_REQUEST) && (STD_ON == XCP_RESUME_SUPPORT)) */

#if ((STD_ON == XCP_PAG_SUPPORT)&& (XCP_SWITCH_PAG_SUPPORT == STD_ON))
#if (XCP_MMU_SUPPORT == STD_ON)
/* To do something about MMU initial */
void Xcp_mmuInit()
{
	return;
}
#elif(XCP_OVC_SUPPORT == STD_ON)
/* To do something about Overlay initial */
FUNC(void,XCP_CODE) Xcp_ovcInit(void)
{

	return;
}
#endif /* (XCP_OVC_SUPPORT == STD_ON) */
#endif /* ((STD_ON == XCP_PAG_SUPPORT)&& (XCP_SWITCH_PAG_SUPPORT == STD_ON)) */
