/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Xcp_Interface.h                                             **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration and type definitions of interface functions **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*=======[I N C L U D E S]====================================================*/
#ifndef XCP_INTERFACE_H_
#define XCP_INTERFACE_H_

/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"
#include "Xcp_Cfg.h"
/*=======[M A C R O S]========================================================*/
/* return code of interface function */
#define XCP_IF_OK           0x00u
#define XCP_IF_NOT_OK       0x01u
#define XCP_IF_BUSY         0x02u
/* mode of nvm/fls interface function */
#define XCP_NVM_ERASE       0x00u
#define XCP_NVM_READ        0x01u
#define XCP_NVM_WRITE       0x02u
#define XCP_FLS_ERASE       0x03u
#define XCP_FLS_WRITE       0x04u
/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
#if (STD_ON == XCP_SEED_AND_UNLOCK)
extern FUNC(Std_ReturnType, XCP_CODE) Xcp_CreateSeed(uint8* length, uint8* seed, const uint8 resource);
extern FUNC(boolean, XCP_CODE) Xcp_IsKeyRight(const uint8* seed, const uint8* key, const uint8 lengthS,const uint8 lengthK, const uint8 resource);
#endif/* (STD_ON == XCP_SEED_AND_UNLOCK) */
#if (STD_ON == XCP_BUILD_CHECKSUM)
extern void Xcp_InitCrc16CcittTable();
extern FUNC(void, XCP_CODE) Xcp_ChecksumCompute(const uint32 checksumStartAddress, const uint32 blockSize, uint32* crcResult);
#endif/* (STD_ON == XCP_BUILD_CHECKSUM) */
#if (STD_ON == XCP_SET_MTA)
extern FUNC(uint32, XCP_CODE) Xcp_Mta2Ptr(const uint8 u1AddExt, const uint32 u4Add);
#endif
#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
extern Std_ReturnType Xcp_FlsReq(uint8 Mode, uint32 u4Address, uint32 u4Size, const uint8* pu1DataPtr);
uint8 Xcp_FlsGetStatus(void);
extern FUNC(void, XCP_CODE) Xcp_ProgramResetNoticeApp(void);
extern Std_ReturnType Xcp_ProgramStartNoticeApp(void);
#endif /* (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE)) */
#if ((STD_ON == XCP_SET_REQUEST) && (STD_ON == XCP_RESUME_SUPPORT))
extern Std_ReturnType Xcp_NvramWriteDaq(void);
extern Std_ReturnType Xcp_NvramWriteCal(void);
#endif

#if ((STD_ON == XCP_PAG_SUPPORT)&& (XCP_SWITCH_PAG_SUPPORT == STD_ON))
#if (XCP_MMU_SUPPORT == STD_ON)
/* To do something about MMU initial */
FUNC(void,XCP_CODE) Xcp_mmuInit();
#elif(XCP_OVC_SUPPORT == STD_ON)
/* To do something about Overlay initial */
FUNC(void,XCP_CODE) Xcp_ovcInit(void);
#endif /* (XCP_OVC_SUPPORT == STD_ON) */
#endif /* ((STD_ON == XCP_PAG_SUPPORT)&& (XCP_SWITCH_PAG_SUPPORT == STD_ON)) */

#endif /* XCP_INTERFACE_H_ */
