/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Xcp_Internal.c                                              **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : Internal Implementation for XCP	                          **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef XCP_INTERNAL_H_
#define XCP_INTERNAL_H_

/*=======[I N C L U D E S]====================================================*/
#include "Xcp.h"

/*=======[M A C R O S]========================================================*/
#define XCP_PID_OFFSET             0x00u

/*
 * Command Code
 */
/* Standard commands */
#define XCP_CMD_CONNECT                 0xFFu
#define XCP_CMD_DISCONNECT              0xFEu
#define XCP_CMD_GET_STATUS              0xFDu
#define XCP_CMD_SYNCH                   0xFCu
#define XCP_CMD_GET_COMM_MODE_INFO      0xFBu
#define XCP_CMD_GET_ID                  0xFAu
#define XCP_CMD_SET_REQUEST             0xF9u
#define XCP_CMD_GET_SEED                0xF8u
#define XCP_CMD_UNLOCK                  0xF7u
#define XCP_CMD_SET_MTA                 0xF6u
#define XCP_CMD_UPLOAD                  0xF5u
#define XCP_CMD_SHORT_UPLOAD            0xF4u
#define XCP_CMD_BUILD_CHECKSUM          0xF3u
#define XCP_CMD_TRANSPORT_LAYER_CMD     0xF2u
#define XCP_CMD_TL_SUB_CODE_OFFSET      0x01u
#define XCP_CMD_TL_GET_SLAVE_ID         0xFFu
#define XCP_CMD_TL_GET_DAQ_ID           0xFEu
#define XCP_CDM_TL_SET_DAQ_ID           0xFDu
#define XCP_CMD_USER_CMD                0xF1u

/* Calibration commands */
#define XCP_CMD_DOWNLOAD                0xF0u
#define XCP_CMD_DOWNLOAD_NEXT           0xEFu
#define XCP_CMD_DOWNLOAD_MAX            0xEEu
#define XCP_CMD_SHORT_DOWNLOAD          0xEDu
#define XCP_CMD_MODIFY_BITS             0xECu

/* Page switching commands */
#define XCP_CMD_SET_CAL_PAGE            0xEBu
#define XCP_CMD_GET_CAL_PAGE            0xEAu
#define XCP_CMD_GET_PAG_PROCESSOR_INFO  0xE9u
#define XCP_CMD_GET_SEGMENT_INFO        0xE8u
#define XCP_CMD_GET_PAGE_INFO           0xE7u
#define XCP_CMD_SET_SEGMENT_MODE        0xE6u
#define XCP_CMD_GET_SEGMENT_MODE        0xE5u
#define XCP_CMD_COPY_CAL_PAGE           0xE4u

/* Data acquisition and stimulation commands */
#define XCP_CMD_CLEAR_DAQ_LIST          0xE3u
#define XCP_CMD_SET_DAQ_PTR             0xE2u
#define XCP_CMD_WRITE_DAQ               0xE1u
#define XCP_CMD_SET_DAQ_LIST_MODE       0xE0u
#define XCP_CMD_GET_DAQ_LIST_MODE       0xDFu
#define XCP_CMD_START_STOP_DAQ_LIST     0xDEu
#define XCP_CMD_START_STOP_SYNCH        0xDDu
#define XCP_CMD_GET_DAQ_CLOCK           0xDCu
#define XCP_CMD_READ_DAQ                0xDBu
#define XCP_CMD_GET_DAQ_PROCESSOR_INFO  0xDAu
#define XCP_CMD_GET_DAQ_RESOLUTION_INFO 0xD9u
#define XCP_CMD_GET_DAQ_LIST_INFO       0xD8u
#define XCP_CMD_GET_DAQ_EVENT_INFO      0xD7u
#define XCP_CMD_FREE_DAQ                0xD6u
#define XCP_CMD_ALLOC_DAQ               0xD5u
#define XCP_CMD_ALLOC_ODT               0xD4u
#define XCP_CMD_ALLOC_ODT_ENTRY         0xD3u

/* Non-volatile memory programming commands */
#define XCP_CMD_PROGRAM_START           0xD2u
#define XCP_CMD_PROGRAM_CLEAR           0xD1u
#define XCP_CMD_PROGRAM                 0xD0u
#define XCP_CMD_PROGRAM_RESET           0xCFu
#define XCP_CMD_GET_PGM_PROCESSOR_INFO  0xCEu
#define XCP_CMD_GET_SECTOR_INFO         0xCDu
#define XCP_CMD_PROGRAM_PREPARE         0xCCu
#define XCP_CMD_PROGRAM_FORMAT          0xCBu
#define XCP_CMD_PROGRAM_NEXT            0xCAu
#define XCP_CMD_PROGRAM_MAX             0xC9u
#define XCP_CMD_PROGRAM_VERIFY          0xC8u

#define XCP_CMD_CODE_MIN                0xC0u

/*
 * End of Command Code
 */

#define XCP_RESP_ERROR_OFFSET           0x01u
#define XCP_RESP_POS_PID                0xFFu
#define XCP_RESP_ERR_PID                0xFEu


/*
 * Error Code
 */
#define XCP_ERR_CMD_SYNCH               0x00u
#define XCP_ERR_CMD_BUSY                0x10u
#define XCP_ERR_DAQ_ACTIVE              0x11u
#define XCP_ERR_PGM_ACTIVE              0x12u
#define XCP_ERR_CMD_UNKNOWN             0x20u
#define XCP_ERR_CMD_SYNTAX              0x21u
#define XCP_ERR_OUT_OF_RANGE            0x22u
#define XCP_ERR_WRITE_PROTECTED         0x23u
#define XCP_ERR_ACCESS_DENIED           0x24u
#define XCP_ERR_ACCESS_LOCKED           0x25u
#define XCP_ERR_PAGE_NOT_VALID          0x26u
#define XCP_ERR_MODE_NOT_VALID          0x27u
#define XCP_ERR_SEGMENT_NOT_VALID       0x28u
#define XCP_ERR_SEQUENCE                0x29u
#define XCP_ERR_DAQ_CONFIG              0x2Au
#define XCP_ERR_MEMORY_OVERFLOW         0x30u
#define XCP_ERR_GENERIC                 0x31u
#define XCP_ERR_VERIFY                  0x32u
#define XCP_ERR_RES_TEMP_NOT_A          0x33u
/*
 * End of Error Code
 */
#define XCP_EV_PID              0xFDu
#define XCP_EV_CODE_OFFEST      0x01u
/*
 *  Event Code
 */
#define XCP_EV_RESUME_MODE          0x00u
#define XCP_EV_CLEAR_DAQ            0x01u
#define XCP_EV_STORE_DAQ            0x02u
#define XCP_EV_STORE_CAL            0x03u
#define XCP_EV_CMD_PENDING          0x05u
#define XCP_EV_DAQ_OVERLOAD         0x06u
#define XCP_EV_SESSION_TERMINATED   0x07u
#define XCP_EV_TIME_SYNC            0x08u
#define XCP_EV_STIM_TIMEOUT         0x09u
#define XCP_EV_SLEEP                0x0Au
#define XCP_EV_WAKE_UP              0x0Bu
#define XCP_EV_USER                 0xFEu
#define XCP_EV_TRANSPORT            0xFFu
/*
 * End of Event Code
 */
/* Bitmask for SessionStatus */
#define XCP_SESSION_STATE_STORE_CAL     0x01u
#define XCP_SESSION_STATE_STORE_DAQ     0x04u
#define XCP_SESSION_STATE_CLEAR_DAQ     0x08u
#define XCP_SESSION_STATE_DAQ_RUNNING   0x40u
#define XCP_SESSION_STATE_RESUME        0x80u

/* Bitmasks for sendStatus */
#define XCP_RESP_REQUEST        0x01u
#define XCP_EV_REQUEST          0x02u
#define XCP_DAQ_REQUEST         0x04u
#define XCP_SERV_REQUEST        0x08u
#define XCP_SEND_REQUEST        0x0fu
#define XCP_RESP_PENDING        0x10u
#define XCP_EV_PENDING          0x20u
#define XCP_DAQ_PENDING         0x40u
#define XCP_SERV_PENDING        0x80u
#define XCP_SEND_PENDING        0xf0u
#define XCP_SEND_IDLE           0x00u

/* Bitmask for DAQ sendStatus */
#define XCP_DAQ_SS_AL 			0x01u
#define XCP_DAQ_SS_QUEUE        0x02u
#define XCP_DAQ_SS_SENDING      0x04u
#define XCP_DAQ_SS_OVL		    0x08u /* OVERLOAD */
#define XCP_DAQ_SS_DONE         0x10u


#define XCP_DAQ_MOD_RESUME      0x80u
#define XCP_DAQ_MOD_RUNNING     0x40u
/* Bit 4 is Time Stamp */
#define XCP_DAQ_MOD_PIDOFF      0x20u
/* Bit 4 is Time Stamp */
#define XCP_DAQ_MOD_AL_BIT		0x10u
#define XCP_DAQ_MOD_TS          0x10u
/* Bit 1 is Direction */
#define XCP_DAQ_MOD_DIR_BIT		0x02u /* DIRECTION */
#define XCP_DAQ_MOD_DIR_DAQ     0X00u
#define XCP_DAQ_MOD_DIR_STIM    0x02u
/* Bit 0 is Alternating */
#define XCP_DAQ_MOD_SELE	0x01u /* SELECTED */
#define XCP_DAQ_MOD_AL		0x01u /* ALTERNATING */


#if (XCP_SET_REQUEST == STD_ON)
#define XCP_STORE_DAQ_BASE_ADDR 0xafe00000
#define XCP_STORE_DAQ_MAX_BUFSIZE 128u
#define XCP_STORE_DAQ_RESUME_STATUS_OFFSET   0u
#define XCP_STORE_DAQ_SESSION_ID_OFFSET 	 1u
#define XCP_STORE_DAQ_SUM_LENGTH			 3u
#define XCP_STORE_DAQ_CFG_OFFSET			 (XCP_MAX_DAQ+3u)

#define XCP_STORE_DAQ_PID_OFFSET			 0u
#define XCP_STORE_DAQ_NUM_OFFSET			 1u
#define XCP_STORE_DAQ_ENTRYNUM_OFFSET		 2u

#define XCP_STORE_DAQ_ENTRY_ADDR_OFFSET 	 (XCP_STORE_DAQ_ENTRYNUM_OFFSET+1u)
#define XCP_STORE_DAQ_ENTRY_LENGTH_OFFSET 	 (XCP_STORE_DAQ_ENTRY_ADDR_OFFSET+4u)
#define XCP_STORE_DAQ_ONE_FRAME_LENGTH 		 (XCP_STORE_DAQ_ENTRY_LENGTH_OFFSET+1u)
/* save daq's Inherent attribute */
#define XCP_STORE_DAQ_MODE_OFFSET			 0u
#define XCP_STORE_DAQ_EVCH_OFFSET			 1u
#define XCP_STORE_DAQ_PRESCALER_OFFSET		 2u
#define XCP_STORE_DAQ_PRIORITY_OFFSET		 3u
#define XCP_STORE_DAQ_ATTRIBUTE_LENGTH 		 4U


/* first byte to save Xcp_sessionstatus
 * second byte to save Session configuration id
 * */

#endif

/* XCP checksum types */
#define XCP_CHECKSUM_TYPE_ADD_11        0x01u
#define XCP_CHECKSUM_TYPE_ADD_12        0x02u
#define XCP_CHECKSUM_TYPE_ADD_14        0x03u
#define XCP_CHECKSUM_TYPE_ADD_22        0x04u
#define XCP_CHECKSUM_TYPE_ADD_24        0x05u
#define XCP_CHECKSUM_TYPE_ADD_44        0x06u
#define XCP_CHECKSUM_TYPE_CRC_16        0x07u
#define XCP_CHECKSUM_TYPE_CRC_16_CITT   0x08u
#define XCP_CHECKSUM_TYPE_CRC_32        0x09u
#define XCP_CHECKSUM_TYPE_USER          0xFFu

#if (XCP_ADDRESS_GRANULARITY == XCP_AG_BYTE)
#define XCP_UPLOAD_DATA_OFFSET          (0x01u)
#elif (XCP_ADDRESS_GRANULARITY == XCP_AG_WORD)
#define XCP_UPLOAD_DATA_OFFSET          (0x02u)
#elif (XCP_ADDRESS_GRANULARITY == XCP_AG_DWORD)
#define XCP_UPLOAD_DATA_OFFSET          (0x04u)
#endif

#define XCP_PROGRAM_LEN_OFFSET  0x01u
#if (XCP_ADDRESS_GRANULARITY != XCP_AG_DWORD)
#define XCP_PROGRAM_DATA_OFFSET     0x02u
#else
#define XCP_PROGRAM_DATA_OFFSET     0x04u
#endif
#if (XCP_ADDRESS_GRANULARITY != XCP_AG_DWORD)
#define XCP_PROGRAM_SIZE           ((XCP_MAX_CTO_PGM - 2u)/XCP_AG)	/* ((XCP_MAX_CTO_PGM - 2u)/XCP_AG) */
#else
#define XCP_PROGRAM_SIZE           ((XCP_MAX_CTO_PGM/XCP_AG) - 0x01u)    /* ((XCP_MAX_CTO_PGM/XCP_AG) - 0x01u) */
#endif
#define XCP_PROGRAM_NEXT_SIZE      (XCP_PROGRAM_SIZE*XCP_MAX_BS_PGM)  /* (XCP_PROGRAM_SIZE*XCP_MAX_BS_PGM) */

#define XCP_DOWNLOAD_LEN_OFFSET 0x01u
#if (XCP_ADDRESS_GRANULARITY != XCP_AG_DWORD)
#define XCP_DOWNLOAD_DATA_OFFSET    0X02u
#else
#define XCP_DOWNLOAD_DATA_OFFSET    0X04u
#endif

#define XCP_DOWNLOADMAX_SIZE     ((XCP_MAX_CTO/XCP_AG) - 0x01u)  		 /* ((XCP_MAX_CTO/XCP_AG) - 0x01u) */
#if (XCP_ADDRESS_GRANULARITY != XCP_AG_DWORD)
#define XCP_DOWNLOAD_SIZE        ((XCP_MAX_CTO - 2u)/XCP_AG) 		 /*  ((XCP_MAX_CTO - 2u)/XCP_AG)  */
#else
#define XCP_DOWNLOAD_SIZE        ((XCP_MAX_CTO/XCP_AG) - 0x01u)  		 /* ((XCP_MAX_CTO/XCP_AG) - 0x01u) */
#endif
#define XCP_DOWNLOAD_NEXT_SIZE   (XCP_DOWNLOAD_SIZE*XCP_MAX_BS)  		 /* (XCP_DOWNLOAD_SIZE*XCP_MAX_BS) */
#define XCP_UPLOAD_SIZE          ((XCP_MAX_CTO / XCP_AG) - 0x01u)  		 /* ((XCP_MAX_CTO / XCP_AG) - 0x01u) */
/*=======[T Y P E   D E F I N I T I O N S]====================================*/
#if (XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE))
typedef struct
{
    uint8   sendStatus;
    uint8   mode;
    uint8   daqListSize;
    uint8   currentPid;
    #if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
    TickType  timeStamp;
    #endif
	#if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
    uint8   priority;
	#endif
    #if (XCP_PRESCALER_SUPPORTED == STD_ON)
    uint8   prescaler;
    uint8   prescalerCnt;
    #endif
    #if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
    Xcp_DaqCfgStatusType    DynaCfgStatus;
    #endif
    uint8 firstPid;
    uint16  eventChannelNum;
    uint32  eventBufferPos;
    P2VAR(Xcp_AGType, AUTOMATIC, XCP_VAR)
            eventBufferPtr;
    P2VAR(Xcp_OdtType, AUTOMATIC, XCP_VAR)
            odt;
}Xcp_DaqType;


#endif /*(XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE))*/
typedef enum
{
    XCP_MEM_CAL_RAM = 0,
    XCP_MEM_CAL_ROM,
    XCP_MEM_CAL_ALL,
    XCP_MEM_ALL
}Xcp_MemAddrType;

#if ((STD_ON == XCP_MASTER_BLOCK_MODE)||(STD_ON == XCP_SLAVE_BLOCK_MODE))
typedef VAR(uint16,TYPEDEF) Xcp_BlockBufIndxType;
#endif

#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
/* Xcp program Status */
typedef enum
{
	XCP_PGM_IDLE,
	XCP_PGM_START,
	XCP_PGM_CLEARED,
	XCP_PGM_PROGRAMMING,
	XCP_PGM_PROGRAMMED
}Xcp_PgmStautsType;
#endif
/*=======[E X T E R N A L   D A T A]==========================================*/

/*
 * Set_request
 * */
#if (XCP_SET_REQUEST == STD_ON)
#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
extern uint8 Xcp_SetReqMode;
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#endif /* (XCP_SET_REQUEST == STD_ON) */

#define XCP_START_SEC_VAR_INIT_16
#include "XCP_MemMap.h"
extern uint16 Xcp_storeBufPos;
#define XCP_STOP_SEC_VAR_INIT_16
#include "XCP_MemMap.h"


#define XCP_START_SEC_VAR_INIT_32
#include "XCP_MemMap.h"
#if (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE)
extern uint32 Xcp_DynDaqOdtPos;
extern uint32 Xcp_DynDaqOdtEntryPos;
#endif/* (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE) */
#define XCP_STOP_SEC_VAR_INIT_32
#include "XCP_MemMap.h"

/*
 * CMD SET_REQUEST:store daq config
 * */

#if (XCP_SET_REQUEST == STD_ON)
#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
extern uint8 Xcp_storedaqBuf[XCP_STORE_DAQ_MAX_BUFSIZE];
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#endif
/*
 * Block transfer
 * */
#if (XCP_MASTER_BLOCK_MODE == STD_ON)
#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_AGType, XCP_VAR) Xcp_BlockBuffer[XCP_BLOCK_BUFFER_SIZE];
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
#endif /* (XCP_MASTER_BLOCK_MODE == STD_ON) */


#if ((XCP_MASTER_BLOCK_MODE == STD_ON)||(XCP_SLAVE_BLOCK_MODE == STD_ON))  /* Var for Block transfer */
#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_BlockBufIndxType, XCP_VAR) Xcp_BlockBufferPos;
extern VAR(Xcp_BlockBufIndxType, XCP_VAR) Xcp_BlockBufferLen;
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
#endif /* (XCP_MASTER_BLOCK_MODE == STD_ON)||(XCP_SLAVE_BLOCK_MODE == STD_ON) */


#define XCP_START_SEC_VAR_CLEARED_16
#include "XCP_MemMap.h"
extern VAR(uint16, XCP_VAR) Xcp_SessionCfgId;
#define XCP_STOP_SEC_VAR_CLEARED_16
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
extern VAR(uint8, XCP_VAR_NO_INIT_8) Xcp_CmdBuffer[XCP_MAX_CTO];
extern VAR(uint8, XCP_VAR_NO_INIT_8) Xcp_RespBuffer[XCP_MAX_CTO];
extern VAR(uint8, XCP_VAR) Xcp_EvBuffer[XCP_CTO_BUFFER_SIZE];
extern VAR(uint8, XCP_VAR) Xcp_ServBuffer[XCP_CTO_BUFFER_SIZE];
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(PduLengthType, XCP_VAR) Xcp_CmdLength;
extern VAR(PduLengthType, XCP_VAR) Xcp_RespLength;
extern VAR(PduLengthType, XCP_VAR) Xcp_EvLength;
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_INIT_8
#include "XCP_MemMap.h"
extern VAR(uint8, XCP_VAR_INIT_8) Xcp_ProtectionStatus;
extern VAR(uint8, XCP_VAR_INIT_8) Xcp_SessionStatus;
extern VAR(uint8, XCP_VAR_INIT_8)  Xcp_SendStatus;
#define XCP_STOP_SEC_VAR_INIT_8
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
extern VAR(uint8, XCP_VAR) Xcp_TransferTimeOutCnt;
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"



#define XCP_START_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_StatusType, XCP_VAR_INIT_UNSPECIFIED)Xcp_Status;
extern VAR(Xcp_CommandStatusType, XCP_VAR_INIT_UNSPECIFIED) Xcp_CommandStatus;
#define XCP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"


#if (XCP_SET_MTA == STD_ON)
#define XCP_START_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_AddressType, XCP_VAR_INIT_UNSPECIFIED) Xcp_MTA;
#define XCP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
#endif

/*
 * Build_checksum
 * */
#if (XCP_BUILD_CHECKSUM == STD_ON)
#define XCP_START_SEC_VAR_CLEARED_16
#include "XCP_MemMap.h"
extern VAR(uint16, XCP_VAR) Xcp_Crc16CittTable[256u];
#define XCP_STOP_SEC_VAR_CLEARED_16
#include "XCP_MemMap.h"
#endif

/*
 * Std Seed&key
 *  */
#define XCP_START_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_SeedandKeyStatusType, XCP_VAR_INIT_UNSPECIFIED) Xcp_SeedandKeyStatus;
#define XCP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#if (XCP_SEED_AND_UNLOCK == STD_ON)
extern VAR(uint8, XCP_VAR_INIT_8) Xcp_SeedandKeyRes;
#endif
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"

/*
 * DAQ
 *  */
#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))

#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_DaqNumType, XCP_VAR) Xcp_DaqListQueue[XCP_MAX_DAQ];
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_DaqType, XCP_VAR) Xcp_Daq[XCP_MAX_DAQ];
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
extern VAR(uint8, XCP_VAR) Xcp_DaqBuffer[XCP_DTO_BUFFER_SIZE];
extern VAR(uint8, XCP_VAR_NO_INIT_8) Xcp_EvChActivCnt[XCP_MAX_EVENT_CHANNEL]; /* counts of running daq in eventchannel */

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
#define XCP_DYNAMIC_DAQ_ODT		(0x32*XCP_MAX_DAQ)
#define XCP_DYNAMIC_DAQ_ODT_ENTRY   (XCP_DYNAMIC_DAQ_BUFFER_SIZE/sizeof(Xcp_EntryType))
extern VAR(Xcp_OdtType, XCP_VAR) Xcp_DynDaqOdt[XCP_DYNAMIC_DAQ_ODT]; /* Dynamic : size of allocArry[]  */
extern VAR(Xcp_EntryType, XCP_VAR) Xcp_DynDaqBuffer[XCP_DYNAMIC_DAQ_ODT_ENTRY]; /* Dynamic : size of allocArry[]  */
#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC) */
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_32
#include "XCP_MemMap.h"
extern VAR(uint32, XCP_VAR) Xcp_EvChBufferUsage[XCP_MAX_EVENT_CHANNEL];
#define XCP_STOP_SEC_VAR_CLEARED_32
#include "XCP_MemMap.h"

#if (XCP_GET_DAQ_CLOCK == STD_ON)
#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(TickType, XCP_VAR)  Xcp_RxTimeStamp;
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
#endif /* XCP_GET_DAQ_CLOCK == STD_ON */

#endif /* (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE)) */

#define XCP_START_SEC_VAR_CLEARED_32
#include "XCP_MemMap.h"
#if ((XCP_GET_ID == STD_ON) || (XCP_GET_DAQ_EVENT_INFO == STD_ON) || (XCP_GET_SECTOR_INFO == STD_ON))
extern VAR(uint32, XCP_VAR) Xcp_UploadInfoLen;
#endif
#define XCP_STOP_SEC_VAR_CLEARED_32
#include "XCP_MemMap.h"

/* Global initial point */
extern P2CONST(Xcp_ConfigType, XCP_VAR_CLEARED_32, XCP_APPL_DATA) Xcp_GlobalCfgPtr;

/*
 * CAL
 * */
#if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))
#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#if (XCP_PAG_SUPPORT == STD_ON)
extern VAR(uint8, XCP_VAR) Xcp_ActivPagNum[XCP_MAX_SEGMENT];
#endif
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#endif
/*
 * PGM
 * */
#if (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))
#define XCP_START_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
extern VAR(Xcp_PgmStautsType, XCP_VAR) Xcp_PgmStauts;
#define XCP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
#endif /* (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE)) */

/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

/*
 * common services
 */

#define XCP_START_SEC_CODE
#include "XCP_MemMap.h"
extern FUNC(void, XCP_CODE) Xcp_CommandInit(void);
extern FUNC(void, XCP_CODE) Xcp_ClearStatus(void);
extern FUNC(void, XCP_CODE) Xcp_CopyU4ToU1Buffer(const uint32 u4Data, P2VAR(uint8,AUTOMATIC,AUTOMATIC) pu1BufferPtr, uint8 byteOrder);
extern FUNC(void, XCP_CODE) Xcp_CopyU2ToU1Buffer(const uint16 u2Data, P2VAR(uint8,AUTOMATIC,AUTOMATIC) pu1BufferPtr, uint8 byteOrder);
extern FUNC(void, XCP_CODE) Xcp_CopyU1BufferToU4(P2CONST(uint8,AUTOMATIC,AUTOMATIC) pu1BufferPtr, P2VAR(uint32,AUTOMATIC,AUTOMATIC) pu4Data, uint8 byteOrder);
extern FUNC(void, XCP_CODE) Xcp_CopyU1BufferToU2(P2CONST(uint8,AUTOMATIC,AUTOMATIC) pu1BufferPtr, P2VAR(uint16,AUTOMATIC,AUTOMATIC) pu2Data, uint8 byteOrder);
extern FUNC(void,XCP_CODE) Xcp_CalBufferCopy(uint32 u4SourceAddr, uint32 u4DestAddr, uint32 u4Length);

extern FUNC(void, XCP_CODE) Xcp_SetErrorCode(uint8 u1ErrorCode);
extern FUNC(void, XCP_CODE) Xcp_SendResp(void);
extern FUNC(void, XCP_CODE) Xcp_SendEv(void);
extern FUNC(Std_ReturnType, XCP_CODE) Xcp_Transmit (PduIdType pduId, PduLengthType len, P2VAR(uint8,AUTOMATIC,XCP_VAR) dataPtr);


/*
 * STD
 */
/* Xcp STD mandatory commands */
extern FUNC(void, XCP_CODE) Xcp_Connect(void);
extern FUNC(void, XCP_CODE) Xcp_Disconnect(void);
extern FUNC(void,XCP_CODE) Xcp_DisconnectHal(void);
extern FUNC(void, XCP_CODE) Xcp_GetStatus(void);
extern FUNC(void, XCP_CODE) Xcp_Synch(void);

/* Xcp STD optional commands */
#if (XCP_GET_COMM_MODE_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetCommModeInfo(void);
#endif
#if (XCP_GET_ID == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetId(void);
#endif
#if (XCP_SET_REQUEST == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_SetRequest(void);
#endif

#if (XCP_SEED_AND_UNLOCK == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetSeed(void);
extern FUNC(void, XCP_CODE) Xcp_Unlock(void);
#endif
/* Xcp STD optional commands */
#if (XCP_SET_MTA == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_SetMta(void);
extern FUNC(boolean, XCP_CODE)Xcp_CheckAddress(uint32 addr, uint32 size, Xcp_MemAddrType type);
extern FUNC(void, XCP_CODE) Xcp_UpdateMTA(uint32 u4Length);
#endif

#if (XCP_UPLOAD == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_Upload(void);
#endif
#if (XCP_SHORT_UPLOAD == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ShortUpload(void);
#endif
#if (XCP_BUILD_CHECKSUM == STD_ON)
extern FUNC(void,XCP_CODE) Xcp_BuildChecksum(void);
#endif
#if (XCP_GET_SLAVE_ID == STD_ON)
extern FUNC(void,XCP_CODE) Xcp_GetSlaveId(void);
#endif

/*
 * CAL/PAG local Function
 */
#if (STD_ON == XCP_SET_REQUEST)
extern FUNC(void, XCP_CODE)XCP_DoStoreCAL(void);
#endif

extern FUNC(Std_ReturnType, XCP_CODE) Xcp_CALInit(void);
#if (STD_ON == XCP_OVC_SUPPORT)
extern FUNC(void,XCP_CODE) Xcp_ovcInit(void);
#endif
/*
 * CAL/PAG Mandatory Function
 */
extern FUNC(void, XCP_CODE) Xcp_Download(void);
/*
 * CAL/PAG Optional Function
 */
#if (XCP_DOWNLOAD_NEXT == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_DownloadNext(void);
#endif
#if (XCP_DOWNLOAD_MAX == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_DownloadMax(void);
#endif
#if (XCP_MODIFY_BITS == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ModifyBits(void);
#endif
#if (XCP_PAG_SUPPORT == STD_ON)
#if (XCP_SET_CAL_PAGE == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_SetCalPage(void);
#endif
#if (XCP_GET_CAL_PAGE == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetCalPage(void);
#endif
#endif
/*
 * DAQ
 */
#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
extern FUNC(Std_ReturnType, XCP_CODE)Xcp_DAQInit(void);
extern FUNC(void, XCP_CODE) Xcp_DAQReinit(void);
extern FUNC(void, XCP_CODE) Xcp_SendDaq(void);

/*
 * DAQ mandatory
 */
extern FUNC(void, XCP_CODE) Xcp_SetDaqPtr (void);
extern FUNC(void, XCP_CODE) Xcp_WriteDaq (void);
extern FUNC(void, XCP_CODE) Xcp_SetDaqListMode (void);
extern FUNC(void, XCP_CODE) Xcp_StartStopDaqList (void);
extern FUNC(void, XCP_CODE) Xcp_StartStopSynch (void);
#if (XCP_PL_STIM == (XCP_PL_STIM & XCP_RESOURCE))
extern FUNC(void,XCP_CODE) Xcp_RxStimHal(const PduLengthType len, P2CONST(uint8,AUTOMATIC,XCP_APPL_DATA) dataPtr);
#endif
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC)
extern FUNC(void, XCP_CODE) Xcp_ClearDaqList (void);
#if (XCP_GET_DAQ_LIST_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqListInfo(void);
#endif
#else
extern FUNC(void, XCP_CODE) Xcp_FreeDaq (void);
extern FUNC(void, XCP_CODE) Xcp_AllocDaq(void);
extern FUNC(void, XCP_CODE) Xcp_AllocOdt(void);
extern FUNC(void, XCP_CODE) Xcp_AllocOdtEntry(void);
#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC) */

/*
 * DAQ optional
 */
#if (XCP_GET_DAQ_CLOCK == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqClock (void);
#endif
#if (XCP_GET_DAQ_PROCESSOR_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqProcessorInfo(void);
#endif
#if (XCP_GET_DAQ_RESOLUTION_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqResolutionInfo(void);
#endif
#if (XCP_GET_DAQ_LIST_MODE == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqListMode(void);
#endif
#if (XCP_GET_DAQ_EVENT_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetDaqEventInfo(void);
#endif
#if (XCP_READ_DAQ == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ReadDaq(void);
#endif

/*
 * store DAQ for SET_REQUEST
 */
#if (XCP_SET_REQUEST == STD_ON)
extern FUNC(void, XCP_CODE)
XCP_DoStoreDaq(boolean resumeFlag);
extern FUNC(void, XCP_CODE)
Xcp_ClearDaqCfg(void);
#endif
#endif /* (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE)) */

/*
 * PGM
 */


#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
extern Std_ReturnType Xcp_InPgmStatus(void);

extern FUNC(void, XCP_CODE) Xcp_ProgramInit(void);
extern FUNC(void, XCP_CODE) Xcp_PgmPendingTask(void);
/*
 * PGM mandatory
 */
extern FUNC(void, XCP_CODE) Xcp_ProgramStart(void);
extern FUNC(void, XCP_CODE) Xcp_ProgramClear(void);
extern FUNC(void, XCP_CODE) Xcp_Program (void);
extern FUNC(void, XCP_CODE) Xcp_ProgramReset(void);
/*
 * PGM optional
 */
#if (XCP_GET_PGM_PROCESSOR_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetPgmProcessorInfo(void);
#endif
#if (XCP_GET_SECTOR_INFO == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_GetSectorInfo(void);
#endif
#if (XCP_PROGRAM_PREPARE == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ProgramPrepare(void);
#endif
#if (XCP_PROGRAM_FORMAT == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ProgramFormat(void);
#endif
#if (XCP_PROGRAM_NEXT == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ProgramNext(void);
#endif
#if (XCP_PROGRAM_MAX == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ProgramMax(void);
#endif
#if (XCP_PROGRAM_VERIFY == STD_ON)
extern FUNC(void, XCP_CODE) Xcp_ProgramVerify(void);
#endif
/*
 * End of PGM
 */
#endif /*(XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))*/
#define XCP_STOP_SEC_CODE
#include "XCP_MemMap.h"
#endif /* XCP_INTERNAL_H_ */
