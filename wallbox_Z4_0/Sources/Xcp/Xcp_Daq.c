/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Xcp_Daq.c                                                   **
**                                                                            **
**  Created on  :                                                             **
**  Author      : qinchun.yang                                                **
**  Vendor      :                                                             **
**  DESCRIPTION : Implementation of the XCP_Daq command			              **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/

#define XCP_DAQ_C_AR_MAJOR_VERSION  4u
#define XCP_DAQ_C_AR_MINOR_VERSION  2u
#define XCP_DAQ_C_AR_PATCH_VERSION  2u
#define XCP_DAQ_C_SW_MAJOR_VERSION  1u
#define XCP_DAQ_C_SW_MINOR_VERSION  0u
#define XCP_DAQ_C_SW_PATCH_VERSION  0u
/*=======[I N C L U D E S]====================================================*/
#include "Xcp_Internal.h"
/*=======[V E R S I O N  C H E C K]===========================================*/
#if (XCP_DAQ_C_AR_MAJOR_VERSION != XCP_H_AR_MAJOR_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif
#if (XCP_DAQ_C_AR_MINOR_VERSION != XCP_H_AR_MINOR_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif
#if (XCP_DAQ_C_AR_PATCH_VERSION != XCP_H_AR_PATCH_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif
#if (XCP_DAQ_C_SW_MAJOR_VERSION != XCP_H_SW_MAJOR_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif
#if (XCP_DAQ_C_SW_MINOR_VERSION != XCP_H_SW_MINOR_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif
#if (XCP_DAQ_C_SW_PATCH_VERSION != XCP_H_SW_PATCH_VERSION)
  #error "Xcp_Daq.c : Mismatch in Specification Major Version"
#endif


#if (XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE))
/*=======[M A C R O S]========================================================*/
/* XCP start stop mode */
#define XCP_START_STOP_MOD_STOP         0x00u
#define XCP_START_STOP_MOD_START        0x01u
#define XCP_START_STOP_MOD_SELECT       0x02u

/* XCP start stop synch */
#define XCP_START_STOP_MOD_STOP_ALL         0x00u
#define XCP_START_STOP_MOD_START_SELECT     0x01u
#define XCP_START_STOP_MOD_STOP_SELECT      0x02u

/* DAQ-DTO data position offset */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
#define XCP_DATA_OFFSET  0x01u
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_BYTE)
#define XCP_DAQ_NUM_OFFSET  0x01u
#define XCP_DATA_OFFSET  0x02u
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_WORD)
#define XCP_DAQ_NUM_OFFSET  0x01u
#define XCP_DATA_OFFSET  0x03u
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_WORD_ALIGNED)
#define XCP_DAQ_NUM_OFFSET  0x02u
#define XCP_DATA_OFFSET  0x04u
#endif /* (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE) */

/*=======[I N T E R N A L   D A T A]==========================================*/

#define XCP_START_SEC_VAR_INIT_16
#include "XCP_MemMap.h"
/* DAQ Ptr ,for CMD:SET_DAQ_PTR*/
static VAR(uint16, XCP_VAR_INIT_16) Xcp_PtrDaq = 0xffffu;
#define XCP_STOP_SEC_VAR_INIT_16
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_INIT_8
#include "XCP_MemMap.h"
/* DAQ Ptr ,for CMD:SET_DAQ_PTR*/
static VAR(uint8, XCP_VAR_INIT_8) Xcp_PtrOdt = 0xffu;
static VAR(uint8, XCP_VAR_INIT_8) Xcp_PtrEntry = 0xffu;
#define XCP_STOP_SEC_VAR_INIT_8
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
/* dynamic config sequence statemachine */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
static VAR(uint8, XCP_VAR) Xcp_DynamicDaqFirstPIDCnt;
#endif
#endif
#define XCP_STOP_SEC_VAR_CLEARED_8
#include "XCP_MemMap.h"

/* DAQ Queue */
#define XCP_START_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"
static VAR(Xcp_DaqNumType, XCP_VAR) Xcp_DaqListQueuePos;
static VAR(Xcp_DaqNumType, XCP_VAR) Xcp_DaqListQueueLength;
static VAR(Xcp_DaqNumType, XCP_VAR) Xcp_MaxQueueLength;
#define XCP_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include "XCP_MemMap.h"

#define XCP_START_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
/* dynamic config sequence statemachine */
static VAR(Xcp_DaqCfgStatusType, XCP_VAR) Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_IDLE;
#endif
#define XCP_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "XCP_MemMap.h"

#if (XCP_GET_DAQ_CLOCK == STD_ON)
TickType  Xcp_RxTimeStamp = 0u;
#endif /* XCP_GET_DAQ_CLOCK == STD_ON */

/*=======[I N T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/

/*
 * DAQ local Function
 */
#if 0
#if (STD_ON == XCP_RESUME_SUPPORT )
#define XCP_START_SEC_CODE
#include "XCP_MemMap.h"
static FUNC(Std_ReturnType,XCP_CODE) Xcp_DAQResume(void);
#define XCP_STOP_SEC_CODE
#include "XCP_MemMap.h"
#endif
#endif

#define XCP_START_SEC_CODE
#include "XCP_MemMap.h"
static FUNC(void,XCP_CODE) Xcp_DaqCfgBufferInit(void);
static FUNC(void,XCP_CODE) Xcp_ClearAllDaq(void);
static FUNC(void,XCP_CODE) Xcp_ClearDaq(Xcp_DaqNumType daqNum);
static FUNC(void,XCP_CODE) Xcp_ClearDaqOdt(Xcp_DaqNumType daqNum);

#if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
static FUNC(void,XCP_CODE) Xcp_SortDaqbypriority(void);
#endif
static FUNC(void,XCP_CODE) Xcp_FillEventBuffer(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr);
static FUNC(void,XCP_CODE) Xcp_EventChannelDaqHal(uint16 eventNum, const Xcp_DaqNumType daqNum);
static FUNC(Std_ReturnType,XCP_CODE) Xcp_SampleAndTransmit(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr);
static FUNC(Std_ReturnType,XCP_CODE) Xcp_SendDaqFrame(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr, boolean overLoad);
static FUNC(void,XCP_CODE) Xcp_EventChannelStimHal(uint16 daqNum);
static FUNC(Std_ReturnType, XCP_CODE) Xcp_StartDaq(Xcp_DaqNumType daqNum);
static FUNC(void,XCP_CODE) Xcp_StopDaq(Xcp_DaqNumType daqNum);
static FUNC(void,XCP_CODE) Xcp_WriteDaqHal(void);
static FUNC(void,XCP_CODE) Xcp_SetDaqListModeHal(void);
static FUNC(void,XCP_CODE) Xcp_StartStopDaqListHal(void);
static FUNC(void,XCP_CODE) Xcp_StartStopSynchHal(void);

#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
static FUNC(void,XCP_CODE) Xcp_AllocDaqHal(void);
static FUNC(void,XCP_CODE) Xcp_AllocOdtHal(void);
static FUNC(void,XCP_CODE) Xcp_AllocOdtEntryHal(void);
#endif
#define XCP_STOP_SEC_CODE
#include "XCP_MemMap.h"



#define XCP_START_SEC_CODE
#include "XCP_MemMap.h"

#if (XCP_SET_REQUEST == STD_ON)
#if (XCP_STORE_CAL == STD_ON)
/*
 * The format of the DAQ configuration information to save:
 * 1th:Xcp_SessionStatus
 * 2~3th:Xcp_SessionCfgId
 * 4~XCP_MAX_DAQ:Save the total length of the DAQ-specific information
 * {
	  (XCP_MAX_DAQ+1)~(XCP_MAX_DAQ+1)+n*8:Save the unique information of DAQ(odtnum(/daqnum/entrynum/entry addr(4byte)/entry length)
	  The next four bytes:Save the general information of DAQ(mode/eventchannelnum/priority/prescaler)
 * }loop
 * */
FUNC(void, XCP_CODE)
XCP_DoStoreDaq(boolean resumeFlag)
{
	uint8 daqNum=0;
	uint8 odtNum=0;
	uint8 entryNum=0;
	uint16 old_storeBufPos = 0;
	Xcp_EntryType* entryPtr;
	/* clear the Xcp_storeBufPos */
	Xcp_storeBufPos = XCP_STORE_DAQ_CFG_OFFSET;
    for (daqNum = 0; daqNum < XCP_MAX_DAQ; daqNum++)
    {
    	if (XCP_DAQ_MOD_SELE == (XCP_DAQ_MOD_SELE & Xcp_Daq[daqNum].mode))
    	{
    		old_storeBufPos = Xcp_storeBufPos;
    		/* 1.clear the SELECTED flag */
    		Xcp_Daq[daqNum].mode &= (~XCP_DAQ_MOD_SELE);
    		if(TRUE == resumeFlag)
    		{
    			Xcp_Daq[daqNum].mode |= XCP_DAQ_MOD_RESUME;
    		}
    		else
    		{
    			Xcp_Daq[daqNum].mode &= (~XCP_DAQ_MOD_RESUME);
    		}
    		for (odtNum = 0; odtNum < Xcp_Daq[daqNum].daqListSize; odtNum++)
    		{
    	        if (0x00 == Xcp_Daq[daqNum].odt[odtNum].XcpOdtEntry[0].XcpOdtEntryLength)
    	        {
    	            break;
    	        }
    	        entryPtr = Xcp_Daq[daqNum].odt[odtNum].XcpOdtEntry;
    	        for (entryNum = 0; entryNum < Xcp_Daq[daqNum].odt[odtNum].XcpOdtEntryCount; entryNum++)
    	        {
    	            if (0x00 == entryPtr[entryNum].XcpOdtEntryLength)
    	            {
    	                break;
    	            }
    	            /* Save each daq-specific information */
    	            Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PID_OFFSET] = Xcp_Daq[daqNum].currentPid;
    	            #if (XCP_PID_RELATIVE_BYTE == XCP_IDENTIFICATION_FIELD_TYPE)
    	            Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_NUM_OFFSET] = daqNum;
					#elif (XCP_PID_ABSOLUTE == XCP_IDENTIFICATION_FIELD_TYPE)
					Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_NUM_OFFSET] = 0xff;
					#endif /*(XCP_PID_RELATIVE_BYTE == XCP_IDENTIFICATION_FIELD_TYPE)*/
    	            Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_ENTRYNUM_OFFSET] = entryNum;
    	            Xcp_CopyU4ToU1Buffer((uint32)(entryPtr[entryNum].XcpOdtEntryAddr), &Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_ENTRY_ADDR_OFFSET], CPU_BYTE_ORDER);
    	            Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_ENTRY_LENGTH_OFFSET] = entryPtr[entryNum].XcpOdtEntryLength;
    	            Xcp_storeBufPos = Xcp_storeBufPos + XCP_STORE_DAQ_ONE_FRAME_LENGTH;
    	        }
    	        Xcp_Daq[daqNum].currentPid++;
    		}
    		/* A complete DAQ message is saved,now to save daq's property */
    		/* Save the total length of each daq-specific information */
    		Xcp_storedaqBuf[XCP_STORE_DAQ_SUM_LENGTH+daqNum]				= Xcp_storeBufPos - old_storeBufPos;
    		/* Save each daq general information */
    		Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_MODE_OFFSET] 		= Xcp_Daq[daqNum].mode;
    		Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_EVCH_OFFSET]		= (uint8)Xcp_Daq[daqNum].eventChannelNum;
			#if (XCP_PRESCALER_SUPPORTED == STD_ON)
    		Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PRESCALER_OFFSET] = Xcp_Daq[daqNum].prescaler;
			#endif
    		Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PRIORITY_OFFSET]  = Xcp_Daq[daqNum].priority;
    		Xcp_storeBufPos = Xcp_storeBufPos + XCP_STORE_DAQ_ATTRIBUTE_LENGTH;
    	}
    }
    if(Xcp_storeBufPos != 0)
    {
		Xcp_storedaqBuf[XCP_STORE_DAQ_RESUME_STATUS_OFFSET] = Xcp_SessionStatus;
		Xcp_CopyU2ToU1Buffer(Xcp_SessionCfgId,&Xcp_storedaqBuf[XCP_STORE_DAQ_SESSION_ID_OFFSET],CPU_BYTE_ORDER);
    	while(Xcp_storeBufPos < XCP_STORE_DAQ_MAX_BUFSIZE)
    	{
    		Xcp_storedaqBuf[Xcp_storeBufPos] = 0xff;
    		Xcp_storeBufPos++;
    	}

    	if(E_OK == Xcp_NvramWriteDaq())
    	{
    		/* Clear the STORE_DAQ_REQ */
    		Xcp_SessionStatus &= (~XCP_SESSION_STATE_STORE_DAQ);
            Xcp_EvBuffer[XCP_PID_OFFSET] = XCP_EV_PID;
            Xcp_EvBuffer[XCP_EV_CODE_OFFEST] = XCP_EV_STORE_DAQ;
            Xcp_EvLength = 2;
            Xcp_SendEv();
    	}
    	else
    	{
            Xcp_SetErrorCode(XCP_ERR_ACCESS_DENIED);
            Xcp_RespLength = 0x02u;
    	}
    }
}
FUNC(void, XCP_CODE)
Xcp_ClearDaqCfg(void)
{
	/*
	 * clear any DAQ list configuration that might already be
		stored in non-volatile memory
	 * */
	return;
}
#endif
#endif

FUNC(Std_ReturnType, XCP_CODE)
Xcp_DAQInit(void)
{
    Std_ReturnType result = E_OK;
    Xcp_DAQReinit();
    return result;
}

FUNC(void,XCP_CODE)
Xcp_DAQReinit(void)
{
	#if ((STD_ON == XCP_DAQ_STORE_SUPPORT) && (STD_ON == XCP_SET_REQUEST))
	uint8 daqNum=0;
	uint8 odtNum=0;
	uint32 tempAddr;
	uint16 old_storeBufPos;
	#endif
    /* initlization the daq odt Ptr */
    Xcp_DaqCfgBufferInit();
    /* clear all DAQ */
    Xcp_ClearAllDaq();

	#if ((STD_ON == XCP_DAQ_STORE_SUPPORT) && (STD_ON == XCP_SET_REQUEST))
	Xcp_SessionStatus = *(uint8*)(XCP_STORE_DAQ_BASE_ADDR);
    Xcp_CopyU1BufferToU2((uint8*)(XCP_STORE_DAQ_BASE_ADDR+1), &Xcp_SessionCfgId, CPU_BYTE_ORDER);
    /* the 0xaabb  is just a hypothetical value
     * use SET_REQUEST cmd to send session configuration id is "0xaabb"
     *   */
	if (((0xaabb+(uint16)XCP_IDENTIFICATION_FIELD_TYPE) == Xcp_SessionCfgId)
		&& (XCP_SESSION_STATE_STORE_DAQ == (XCP_SESSION_STATE_STORE_DAQ & Xcp_SessionStatus)))
	{
		Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_STORE_DAQ);

		Xcp_storeBufPos = XCP_STORE_DAQ_CFG_OFFSET;
		Xcp_CalBufferCopy((uint32)XCP_STORE_DAQ_BASE_ADDR,(uint32)Xcp_storedaqBuf,(uint32)XCP_STORE_DAQ_MAX_BUFSIZE);

		for(daqNum = 0;daqNum < XCP_MAX_DAQ;daqNum++)
		{
			#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
			if(Xcp_Daq[daqNum].currentPid == Xcp_storedaqBuf[Xcp_storeBufPos + XCP_STORE_DAQ_PID_OFFSET])
			#elif(XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_BYTE)
			if(daqNum == Xcp_storedaqBuf[Xcp_storeBufPos + XCP_STORE_DAQ_NUM_OFFSET])
			#endif
			{
				old_storeBufPos = Xcp_storeBufPos;
				while(((Xcp_storeBufPos - old_storeBufPos) < Xcp_storedaqBuf[XCP_STORE_DAQ_SUM_LENGTH+daqNum])
						&&  (0xff != Xcp_storedaqBuf[Xcp_storeBufPos + XCP_STORE_DAQ_ENTRY_LENGTH_OFFSET]))
				{
					Xcp_CopyU1BufferToU4(&Xcp_storedaqBuf[Xcp_storeBufPos + XCP_STORE_DAQ_ENTRY_ADDR_OFFSET], &tempAddr, CPU_BYTE_ORDER);
					#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
						odtNum =  Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PID_OFFSET] - Xcp_Daq[daqNum].firstPid;
					#elif(XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_BYTE)
						odtNum = Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PID_OFFSET];
					#endif
					Xcp_Daq[daqNum].odt[odtNum].XcpOdtEntry[Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_ENTRYNUM_OFFSET]].XcpOdtEntryAddr = (Xcp_AGType*)tempAddr;
					Xcp_Daq[daqNum].odt[odtNum].XcpOdtEntry[Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_ENTRYNUM_OFFSET]].XcpOdtEntryLength = Xcp_storedaqBuf[Xcp_storeBufPos + XCP_STORE_DAQ_ENTRY_LENGTH_OFFSET];
					Xcp_storeBufPos += XCP_STORE_DAQ_ONE_FRAME_LENGTH;
				}
				Xcp_Daq[daqNum].mode = Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_MODE_OFFSET];
				Xcp_Daq[daqNum].eventChannelNum = Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_EVCH_OFFSET];
				Xcp_Daq[daqNum].prescaler = Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PRESCALER_OFFSET];
				Xcp_Daq[daqNum].priority = Xcp_storedaqBuf[Xcp_storeBufPos+XCP_STORE_DAQ_PRIORITY_OFFSET];
				Xcp_storeBufPos = Xcp_storeBufPos + XCP_STORE_DAQ_ATTRIBUTE_LENGTH;
			}
			if(XCP_DAQ_MOD_RESUME == (Xcp_Daq[daqNum].mode & XCP_DAQ_MOD_RESUME))
			{
				Xcp_SessionStatus |= XCP_SESSION_STATE_RESUME;
			#if (STD_ON == XCP_RESUME_SUPPORT )
				Xcp_StartDaq((Xcp_DaqNumType)daqNum);
			#endif
			}
			else
			{
				Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_RESUME);
			}
		}
	}
	#endif
    return;
}

static FUNC(void,XCP_CODE)
Xcp_DaqCfgBufferInit(void)
{
    P2CONST(Xcp_DaqListConfigType, AUTOMATIC, XCP_CONST_PBCFG) daqCfgRef
                                          = Xcp_GlobalCfgPtr->XcpDaqList;
	uint16 Num=0;
    /* clear event usage counter */
    for (Num = 0; Num < XCP_MAX_EVENT_CHANNEL; Num++)
    {
        Xcp_EvChBufferUsage[Num] = 0;
        Xcp_EvChActivCnt[Num] = 0;
    }
    for (Num = 0; Num < XCP_MAX_DAQ; Num++)
    {
        Xcp_Daq[Num].odt = daqCfgRef[Num].XcpOdt;
        #if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC)
        Xcp_Daq[Num].daqListSize = daqCfgRef[Num].XcpMaxOdt;
        #else
        Xcp_Daq[Num].daqListSize = daqCfgRef[Num].XcpDaqSize;  /*will be init 0*/
        #endif
        #if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
        Xcp_Daq[Num].firstPid = daqCfgRef[Num].XcpDto->XcpDtoPid;
        #else
        Xcp_Daq[Num].firstPid = 0;
        #endif
    }
    return;
}

static FUNC(void,XCP_CODE)
Xcp_ClearAllDaq(void)
{
	Xcp_DaqNumType daqNum =0;
    Xcp_PtrDaq = 0xffffu; /*set the ptr is invalid*/
    Xcp_PtrOdt = 0xffu;
    Xcp_PtrEntry = 0xffu;

    Xcp_DaqListQueueLength = 0;
    Xcp_DaqListQueuePos = 0;
    Xcp_MaxQueueLength = 0;
    for (daqNum = 0; daqNum < XCP_MAX_DAQ; daqNum++)
     {
         Xcp_ClearDaq(daqNum);
     }
    /* initial Xcp_SessionStatus and keep the info of the DAQ Store Request */
    Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_RESUME);

	#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
	Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_IDLE;
	#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
	Xcp_DynamicDaqFirstPIDCnt = 0;
	#endif
	Xcp_DynDaqOdtPos = 0;
	Xcp_DynDaqOdtEntryPos = 0;
	#endif
}

static FUNC(void,XCP_CODE)
Xcp_ClearDaq(Xcp_DaqNumType daqNum)
{
	if (daqNum < XCP_MAX_DAQ)
	{
		/* clear send state machine */
		Xcp_Daq[daqNum].sendStatus = 0x00;
		/* reinit. mode and if the DAQ list only support STIM mode shall be 0x02 */
		Xcp_Daq[daqNum].mode = 0x00;
		#if (XCP_PL_STIM == (XCP_PL_STIM & XCP_RESOURCE))
		if (Xcp_GlobalCfgPtr->XcpDaqList[daqNum].XcpDaqListType == STIM)
		{
			Xcp_Daq[daqNum].mode |= XCP_DAQ_MOD_DIR_STIM;
		}
		#endif
		/* init currentPid */
		Xcp_Daq[daqNum].currentPid = Xcp_Daq[daqNum].firstPid;
		/* init priority */
		#if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
		Xcp_Daq[daqNum].priority = 0x00;
		#endif
		/* init prescaler and cnt */
		#if (XCP_PRESCALER_SUPPORTED == STD_ON)
		Xcp_Daq[daqNum].prescaler = 0x01;
		Xcp_Daq[daqNum].prescalerCnt = 0x00;
		#endif
		/* set eventchannel Num to a invaild value */
		Xcp_Daq[daqNum].eventChannelNum = XCP_MAX_EVENT_CHANNEL;
		/* init event buffer */
		Xcp_Daq[daqNum].eventBufferPos = 0x00;
		Xcp_Daq[daqNum].eventBufferPtr = NULL_PTR;

		/* for configurable DAQ clear ODT and ODT Entries */
		if (daqNum >= XCP_MIN_DAQ)
		{
			#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
			Xcp_Daq[daqNum].DynaCfgStatus = XCP_DAQ_CFG_FREE;
			#endif
			Xcp_ClearDaqOdt(daqNum);
		}
		else
		{
			#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
			Xcp_Daq[daqNum].DynaCfgStatus = XCP_DAQ_CFG_DONE;
			#endif
		}
	}
    return;
}

static FUNC(void,XCP_CODE)
Xcp_ClearDaqOdt(Xcp_DaqNumType daqNum)
{
    uint8 odtNum = 0;
    uint8 entryNum = 0;
    P2VAR(Xcp_OdtType, AUTOMATIC, XCP_VAR) odtRef = Xcp_Daq[daqNum].odt;
    P2VAR(Xcp_EntryType, AUTOMATIC, XCP_VAR) entryRef;
	#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
	if (NULL_PTR != odtRef)
	#endif
	{
        for (odtNum = 0; odtNum < Xcp_Daq[daqNum].daqListSize; odtNum++)
        {
            entryRef = odtRef[odtNum].XcpOdtEntry;
			#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
            if (entryRef != NULL_PTR)
            #endif
            {
            	for (entryNum = 0; entryNum < odtRef[odtNum].XcpOdtEntryCount; entryNum++)
            	{
					entryRef[entryNum].XcpOdtEntryNumber  = 0xFF; /*set entry index is invalid*/
					entryRef[entryNum].XcpOdtEntryAddr = NULL_PTR;
					entryRef[entryNum].XcpOdtEntryLength  = 0;
            	}
				#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
				odtRef[odtNum].XcpOdtEntry = NULL_PTR;
				#endif
            }
			#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
            odtRef[odtNum].XcpOdtEntryCount = 0;
            #endif
        }
		#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
        Xcp_Daq[daqNum].odt = NULL_PTR;
        #endif
	}
	#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC)
	Xcp_Daq[daqNum].daqListSize = 0x00;
	#endif
	return;
}


/* DAQ-DAQ proceessor */
/******************************************************************************/
/*
 * @brief               <fill event Buffer for event consistency purpose >
 *
 * < fill event Buffer for event consistency purpose >.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <daqPtr pointer to the daq that shall be put into a buffer>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
static FUNC(void,XCP_CODE)
Xcp_FillEventBuffer(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr)
{
    uint8 odtNum = 0;  /* odt counter */
    uint8 entryNum = 0;  /* entry conter */
    uint8 elementSizeCnt = 0;  /* element counter */
	uint32 eventBufPos = 0;  /* dest. counter */
    Xcp_EntryType* entryPtr;

    /* traverse all odt */
    for (odtNum = 0; odtNum < daqPtr->daqListSize; odtNum++)
    {
        /* if length of the first entry of the current odt is 0 goto next daq ,ASAM_XCP_Part1-page32*/
        if (0x00 == daqPtr->odt[odtNum].XcpOdtEntry[0].XcpOdtEntryLength)
        {
            break;
        }
        /* update entry ptr */
        entryPtr = daqPtr->odt[odtNum].XcpOdtEntry;
        /* traverse all entry */
        for (entryNum = 0; entryNum < daqPtr->odt[odtNum].XcpOdtEntryCount; entryNum++)
        {
            /* if length of current entry is 0, goto next odt */
            if (0x00 == entryPtr[entryNum].XcpOdtEntryLength)
            {
                break;
            }
            /* copy element to event buffer */
            for (elementSizeCnt = 0; elementSizeCnt < entryPtr[entryNum].XcpOdtEntryLength; elementSizeCnt++)
            {
                daqPtr->eventBufferPtr[eventBufPos] = entryPtr[entryNum].XcpOdtEntryAddr[elementSizeCnt];
                eventBufPos++;
            }
        }
    }
    /* update timestamp if needed */
    #if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
    (void)GetCounterValue(XCP_OS_COUNTER_ID,&(daqPtr->timeStamp));
	#endif
    return;
}

/******************************************************************************/
/*
 * @brief      <package a DAQ pdu and send>
 *
 * < > .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <daqPtr     pointer to the DAQ that shall be sent
 *                       overload   flag to indicated wheather the first bit of the
                                    PID shall be set to reportting the overload>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <Std_ReturnType>
 */
/******************************************************************************/

static FUNC(Std_ReturnType,XCP_CODE)
Xcp_SendDaqFrame
    (P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr, boolean overLoad)
{
    P2VAR(Xcp_OdtType, AUTOMATIC, XCP_VAR) odtPtr = &(daqPtr->odt[daqPtr->currentPid - daqPtr->firstPid]);
    P2VAR(Xcp_EntryType, AUTOMATIC, XCP_VAR) entryPtr;

    Xcp_DaqNumType DaqNum = Xcp_DaqListQueue[Xcp_DaqListQueuePos];

    uint16 pos;   /* DTO buffer Position */
    P2VAR(Xcp_AGType, AUTOMATIC, XCP_VAR) sourPtr;
    P2VAR(Xcp_AGType, AUTOMATIC, XCP_VAR) destPtr;
    uint8 entryNum = 0; /* entry counter */
    uint8 elementCnt = 0; /* element counter */
    uint16 destCnt = 0; /* dest Position counter */

    /* set PID Num*/
    if (TRUE == overLoad)
    {									/* to pid overflow indication flag */
        Xcp_DaqBuffer[XCP_PID_OFFSET] = ((daqPtr->currentPid)|(XCP_PID_OVFLOW));
    }
    else
    {
        Xcp_DaqBuffer[XCP_PID_OFFSET] = daqPtr->currentPid;
    }

    #if (XCP_PID_OFF_SUPPORT == STD_ON)
    if (XCP_DAQ_MOD_PIDOFF == (XCP_DAQ_MOD_PIDOFF & Xcp_Daq[DaqNum].mode))
    {
        pos = 0;
    }
    else
    #endif
    {
        /* set DAQ Num */
        #if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_BYTE)
        Xcp_DaqBuffer[XCP_DAQ_NUM_OFFSET] = DaqNum;
        #elif (XCP_IDENTIFICATION_FIELD_TYPE != XCP_PID_ABSOLUTE)
        Xcp_CopyU2ToU1Buffer(DaqNum, &Xcp_DaqBuffer[XCP_DAQ_NUM_OFFSET], CPU_BYTE_ORDER);
        #endif
        pos = XCP_DATA_OFFSET;
    }
    /* if this is the first odt of a DAQ   */
    if (daqPtr->currentPid == daqPtr->firstPid)
    {
        /* update send Status */
        daqPtr->sendStatus |= (XCP_DAQ_SS_SENDING);
        daqPtr->sendStatus &= (~(XCP_DAQ_SS_QUEUE));

        /* if event consistent is daq then update the event buffer */
        if (XCP_EVENT_CONSIST_DAQ == Xcp_GlobalCfgPtr->XcpEvCh[daqPtr->eventChannelNum].XcpEvChConsistency)
        {
            Xcp_FillEventBuffer(daqPtr);
        }

        /*add timestamp ( if needed )*/
        #if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
        if (XCP_DAQ_MOD_TS == (XCP_DAQ_MOD_TS&(daqPtr->mode)))
        {
            if (XCP_EVENT_CONSIST_ODT == Xcp_GlobalCfgPtr->XcpEvCh[daqPtr->eventChannelNum].XcpEvChConsistency)
            {
                (void)GetCounterValue(XCP_OS_COUNTER_ID,&(daqPtr->timeStamp));
            }
            #if (XCP_TIMESTAMP_TYPE == XCP_TS_ONE_BYTE)
            Xcp_DaqBuffer[XCP_DATA_OFFSET] = (Xcp_TSType)(daqPtr->timeStamp);
            #elif (XCP_TIMESTAMP_TYPE == XCP_TS_TWO_BYTE)
            Xcp_CopyU2ToU1Buffer((Xcp_TSType)(daqPtr->timeStamp), &Xcp_DaqBuffer[XCP_DATA_OFFSET], CPU_BYTE_ORDER);
            #elif (XCP_TIMESTAMP_TYPE == XCP_TS_FOUR_BYTE)
            Xcp_CopyU4ToU1Buffer((Xcp_TSType)(daqPtr->timeStamp), &Xcp_DaqBuffer[XCP_DATA_OFFSET], CPU_BYTE_ORDER);
            #endif
            /* update the pos */
            pos += XCP_TIMESTAMP_TYPE;
        }
        #endif
    }

    destPtr = (Xcp_AGType*)&Xcp_DaqBuffer[pos];

    /* if ODT consistent copy data directly from source*/
    if (XCP_EVENT_CONSIST_ODT == Xcp_GlobalCfgPtr->XcpEvCh[daqPtr->eventChannelNum].XcpEvChConsistency)
    {
        entryPtr = odtPtr->XcpOdtEntry;
        for (entryNum = 0;
            entryNum < odtPtr->XcpOdtEntryCount;
            entryNum++)
        {
            if (0x00 == entryPtr[entryNum].XcpOdtEntryLength)
            {
                break;
            }
            sourPtr = entryPtr[entryNum].XcpOdtEntryAddr;
            for (elementCnt = 0; elementCnt < entryPtr[entryNum].XcpOdtEntryLength; elementCnt++)
            {
                if (XCP_MAX_DTO >= (((destCnt + 1) * XCP_AG) + pos))
                {
                    destPtr[destCnt] = sourPtr[elementCnt];
                    destCnt++;
                }
            }
        }
    }
    /* for other consistent copy data from eventBuffer */
    else
    {
        entryPtr = odtPtr->XcpOdtEntry;
        for (entryNum = 0;
            entryNum < odtPtr->XcpOdtEntryCount;
            entryNum++)
        {
            if (0x00 == entryPtr[entryNum].XcpOdtEntryLength)
            {
                break;
            }
            sourPtr = &(daqPtr->eventBufferPtr[daqPtr->eventBufferPos]);
            for (elementCnt = 0; elementCnt < entryPtr[entryNum].XcpOdtEntryLength; elementCnt++)
            {
                if (XCP_MAX_DTO >= (((destCnt + 1) * XCP_AG) + pos))
                {
                    destPtr[destCnt] = sourPtr[elementCnt];
                    destCnt++;
                }
            }
            /* update the BufferPos */
            daqPtr->eventBufferPos += elementCnt;
        }
    }
    return (Xcp_Transmit(Xcp_GlobalCfgPtr->XcpDaqList[DaqNum].XcpDto->XcpDto2PduMapping->XcpTxPdu->LowLayerTxPduId,
                                      (uint16)((destCnt*XCP_AG) + pos), Xcp_DaqBuffer));
}


/******************************************************************************/
/*
 * @brief      <call SendDaqframe according to the overload state>
 *
 * < > .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <Std_ReturnType>
 */
/******************************************************************************/
static FUNC(Std_ReturnType,XCP_CODE)
Xcp_SampleAndTransmit(P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) daqPtr)
{
    Std_ReturnType result = E_NOT_OK;

    if (XCP_DAQ_SS_OVL == (XCP_DAQ_SS_OVL & (daqPtr->sendStatus)))
    {
        /* if consistency of the associated event channel is DAQ,
         * send the DAQ List from the beginning.
         */
        if (XCP_EVENT_CONSIST_DAQ
            == (Xcp_GlobalCfgPtr->XcpEvCh[daqPtr->eventChannelNum].XcpEvChConsistency))
        {
            daqPtr->currentPid = daqPtr->firstPid;
        }
        /* indicated by event */
        #if (XCP_DAQ_OVL_EV == XCP_DAQ_OVL_INDICATION)
        result = Xcp_SendDaqFrame(daqPtr, FALSE);
        Xcp_EvBuffer[XCP_PID_OFFSET] = XCP_EV_PID;
        Xcp_EvBuffer[XCP_EV_CODE_OFFEST] = XCP_EV_DAQ_OVERLOAD;
        Xcp_EvLength = 2;
        Xcp_SendEv();
        daqPtr->sendStatus &= (uint8)(~(XCP_DAQ_SS_OVL));
        /* indicated by pid */
        #elif (XCP_DAQ_OVL_PID == XCP_DAQ_OVL_INDICATION)
        result = Xcp_SendDaqFrame(daqPtr,TRUE);
        if (E_OK == result)
        {
            daqPtr->sendStatus &= (uint8)(~(XCP_DAQ_SS_OVL));
        }
        /* not indicated */
        #else
        result = Xcp_SendDaqFrame(daqPtr, FALSE);
        daqPtr->sendStatus &= (uint8)(~(XCP_DAQ_SS_OVL));
        #endif
    }
    else
    {
       result = Xcp_SendDaqFrame(daqPtr,FALSE);
    }
    return result;
}

/******************************************************************************/
/*
 * @brief      <this function is used to send a DAQ frame >
 *
 * <>.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_SendDaq(void)
{
    P2VAR(Xcp_DaqType, AUTOMATIC, XCP_VAR) currentDaqPtr;

    Xcp_SendStatus |= XCP_DAQ_REQUEST;

    /* if daq processor disabled */
    if(0x00 == Xcp_DaqListQueueLength)
    {
        Xcp_SendStatus &= (uint8)(~XCP_DAQ_REQUEST);
    }
    else
    {
		#if (XCP_BUS_TX_POLLING_MODE == STD_OFF)
		if (XCP_DAQ_PENDING != (XCP_DAQ_PENDING & Xcp_SendStatus))
		{
		#endif
			currentDaqPtr = &Xcp_Daq[Xcp_DaqListQueue[Xcp_DaqListQueuePos]];

			#if (XCP_BUS_TX_POLLING_MODE == STD_OFF)
			if (E_OK == Xcp_SampleAndTransmit(currentDaqPtr))
			#else
			while (E_OK == Xcp_SampleAndTransmit(currentDaqPtr))
			#endif
			{
				/* send successed  */
				(currentDaqPtr->currentPid)++;
				Xcp_SendStatus |= XCP_DAQ_PENDING;
				Xcp_TransferTimeOutCnt = 0;
				if (XCP_DAQ_SS_AL == (XCP_DAQ_SS_AL & currentDaqPtr->sendStatus))
				{
					(currentDaqPtr->sendStatus) &= ~(XCP_DAQ_SS_SENDING);
					(currentDaqPtr->sendStatus) &= ~(XCP_DAQ_SS_QUEUE);
					/*
					 * updata the DAQ queue length and position
					 */

					Xcp_DaqListQueueLength--;
					Xcp_DaqListQueuePos++;

					if ((Xcp_DaqListQueuePos >= Xcp_MaxQueueLength) || (0x00 == Xcp_DaqListQueueLength))
					{
						Xcp_DaqListQueuePos = 0x00u;
					}
					if (
						((currentDaqPtr->daqListSize + currentDaqPtr->firstPid)<=
						(currentDaqPtr->currentPid))
					/* size of new entry is zero */
					|| (0x00 == (currentDaqPtr->odt[currentDaqPtr->currentPid - currentDaqPtr->firstPid].XcpOdtEntry[0].XcpOdtEntryLength))
					)
					{
						currentDaqPtr->eventBufferPos = 0;
						currentDaqPtr->currentPid = currentDaqPtr->firstPid;
					}
					#if (XCP_BUS_TX_POLLING_MODE == STD_ON)
					break;
					#endif
				}
				#if (XCP_BUS_TX_POLLING_MODE == STD_ON)
				if (
				/* Reach the end of the daq List */
				((currentDaqPtr->daqListSize + currentDaqPtr->firstPid)<
					(currentDaqPtr->currentPid))
				/* size of new entry is zero */
				|| (0x00 == (currentDaqPtr->odt[currentDaqPtr->currentPid - currentDaqPtr->firstPid].XcpOdtEntry[0].XcpOdtEntryLength))
				)
				{
					/* clear the DAQ sending ongoing flag */
					(currentDaqPtr->sendStatus) &= ~(XCP_DAQ_SS_SENDING);
					break;
				}
				#endif
			}
			/* update the queque if needed */
			if (
			/* DAQ List in Queue has been stoped */
			(0x00 == (XCP_DAQ_MOD_RUNNING&(currentDaqPtr->mode)))
			/* Reach the end of the daq List */
			|| ((currentDaqPtr->daqListSize + currentDaqPtr->firstPid)<=
				(currentDaqPtr->currentPid))
			/* size of new entry is zero */
			|| (0x00 == (currentDaqPtr->odt[currentDaqPtr->currentPid - currentDaqPtr->firstPid ].XcpOdtEntry[0].XcpOdtEntryLength))
			)
			{
				/* clear the DAQ sending ongoing flag */
				(currentDaqPtr->sendStatus) &= ~(XCP_DAQ_SS_SENDING);
				/*
				 * updata the DAQ queue length and position
				 */
				currentDaqPtr->eventBufferPos = 0;
				currentDaqPtr->currentPid = currentDaqPtr->firstPid;

				Xcp_DaqListQueueLength--;
				Xcp_DaqListQueuePos++;
				if (Xcp_DaqListQueuePos >= Xcp_MaxQueueLength)
				{
					Xcp_DaqListQueuePos = 0x00u;
				}
			}
		#if (XCP_BUS_TX_POLLING_MODE == STD_OFF)
		}
		else
		{
			/* Do Nothing */
		}
		#endif
    }
    return;
}
#if (XCP_PL_STIM == (XCP_PL_STIM & XCP_RESOURCE))
/******************************************************************************/
/*
 * @brief               <Xcp Module STIM Rx Handeler>
 *
 * <This function copy stim data into associated buffer>.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Non Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE) Xcp_RxStimHal(const PduLengthType len, P2CONST(uint8,AUTOMATIC,XCP_APPL_DATA) dataPtr)
{
	Xcp_DaqNumType daqNum;
	uint8 pid=dataPtr[XCP_PID_OFFSET];
	/* Find daq num */
#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
    for (daqNum = 0; daqNum < XCP_MAX_DAQ; daqNum++)
    {
    	 if ((pid >= Xcp_Daq[daqNum].firstPid)
    	       && (pid < (Xcp_Daq[daqNum].firstPid + Xcp_Daq[daqNum].daqListSize)))
    	 {
    		 break;
    	 }
    }
	if ((XCP_DAQ_MOD_RUNNING|XCP_DAQ_MOD_DIR_STIM) == (Xcp_Daq[daqNum].mode & (XCP_DAQ_MOD_DIR_STIM | XCP_DAQ_MOD_RUNNING)))
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_BYTE)
	daqNum = dataPtr[XCP_DAQ_NUM_OFFSET];
	if ((daqNum < XCP_MAX_DAQ)
			&& ((XCP_DAQ_MOD_RUNNING|XCP_DAQ_MOD_DIR_STIM) == (Xcp_Daq[daqNum].mode & (XCP_DAQ_MOD_DIR_STIM | XCP_DAQ_MOD_RUNNING))))
#elif (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_RELATIVE_WORD)
	Xcp_CopyU1BufferToU2(&dataPtr[XCP_DAQ_NUM_OFFSET], &daqNum,CPU_BYTE_ORDER);
	if ((daqNum < XCP_MAX_DAQ)
			&& ((XCP_DAQ_MOD_RUNNING|XCP_DAQ_MOD_DIR_STIM) == (Xcp_Daq[daqNum].mode & (XCP_DAQ_MOD_DIR_STIM | XCP_DAQ_MOD_RUNNING))))
#endif
	{
		if(pid == Xcp_Daq[daqNum].currentPid)
		{
			uint8 odtNum=pid - Xcp_Daq[daqNum].firstPid;
			Xcp_OdtType *t_odt=&(Xcp_Daq[daqNum].odt[odtNum]);
			/* Count length of odtentry in odt */
			uint8 cntLength=0,cnt;
			uint16 dataPos = XCP_DATA_OFFSET;
			for(cnt=0;cnt<t_odt->XcpOdtEntryCount;cnt++)
			{
				cntLength += t_odt->XcpOdtEntry[cnt].XcpOdtEntryLength;
			}
			/* if there is a timestamp modify len and dataPos */
			/* First dot need to send timestump if(timestump = std_on) */
			if ((0x00 == odtNum)
			&& (0x00 != (XCP_DAQ_MOD_TS & Xcp_Daq[daqNum].mode)))
			{
				dataPos += XCP_TIMESTAMP_TYPE;
			}
			if((((cntLength * XCP_AG) + dataPos) <= len)
	#if ((XCP_CAN_MAX_DLC_REQUIRED == STD_ON) && (XCP_ON_CAN_ENABLE == STD_ON))
				&& (XCP_CAN_MAX_DLC == len)
	#endif
			)
			{
				Xcp_AGType* destPtr=&(Xcp_Daq[daqNum].eventBufferPtr[Xcp_Daq[daqNum].eventBufferPos]);;
				const Xcp_AGType* sourPtr=(const Xcp_AGType*)(&dataPtr[dataPos]);
                for (cnt = 0; cnt < cntLength; cnt++)
                {
                    destPtr[cnt] = sourPtr[cnt];
                }
                Xcp_Daq[daqNum].eventBufferPos += cntLength;
				/* An odt has saved,curentpid++ */
				Xcp_Daq[daqNum].currentPid++;
				odtNum++;
				/* Last Odt,need clear Currentpid */
				if (odtNum >= Xcp_Daq[daqNum].daqListSize)
				{
					Xcp_Daq[daqNum].currentPid = Xcp_Daq[daqNum].firstPid;
					/* Storage from 0 */
					Xcp_Daq[daqNum].eventBufferPos = 0;
				}
			}
		}
	}
}
#endif
/******************************************************************************/
/*
 * @brief      <begin a DAQ list sending trigered by fixed eventchannel>
 *
 * < > .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <eventNum, daqNum>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
static FUNC(void, XCP_CODE)
Xcp_EventChannelDaqHal(uint16 eventNum, const Xcp_DaqNumType daqNum)
{
	Xcp_DaqNumType queueIndex = 0;
    /* overload without refresh the buffer */
    if (XCP_DAQ_SS_SENDING == (XCP_DAQ_SS_SENDING & Xcp_Daq[daqNum].sendStatus))
    {
        Xcp_Daq[daqNum].sendStatus |= XCP_DAQ_SS_OVL;
        queueIndex = (Xcp_DaqListQueueLength + Xcp_DaqListQueuePos)%(XCP_MAX_DAQ);
        Xcp_DaqListQueue[queueIndex] = daqNum;
        Xcp_DaqListQueueLength++;
        if(Xcp_DaqListQueueLength > Xcp_MaxQueueLength)
        {
        	Xcp_MaxQueueLength = Xcp_DaqListQueueLength;
        }
    }
    /* overload refresh the buffer, Eventchannel has more than one daq */
    else if (XCP_DAQ_SS_QUEUE == (XCP_DAQ_SS_QUEUE & Xcp_Daq[daqNum].sendStatus))
    {
        Xcp_Daq[daqNum].sendStatus |= XCP_DAQ_SS_OVL;
        /* if there is a buffer refresh it */
        if (XCP_EVENT_CONSIST_EVENT
            == (Xcp_GlobalCfgPtr->XcpEvCh[eventNum].XcpEvChConsistency))
        {
            Xcp_FillEventBuffer(&Xcp_Daq[daqNum]);
        }
        queueIndex = (Xcp_DaqListQueueLength + Xcp_DaqListQueuePos)%(XCP_MAX_DAQ);
        Xcp_DaqListQueue[queueIndex] = daqNum;
        Xcp_DaqListQueueLength++;
        if(Xcp_DaqListQueueLength > Xcp_MaxQueueLength)
        {
        	Xcp_MaxQueueLength = Xcp_DaqListQueueLength;
        }

    }
    else
    {
        if (XCP_EVENT_CONSIST_EVENT
            == (Xcp_GlobalCfgPtr->XcpEvCh[eventNum].XcpEvChConsistency))
        {
            Xcp_FillEventBuffer(&Xcp_Daq[daqNum]);
        }


        /* Regardless of whether the priority is opened or not, the daq unified into the array, followed by the priority for processing */
        if(0 == Xcp_DaqListQueueLength)
        {
        	Xcp_MaxQueueLength = Xcp_DaqListQueueLength;
        }
        queueIndex = (Xcp_DaqListQueueLength + Xcp_DaqListQueuePos)%(XCP_MAX_DAQ);
        Xcp_DaqListQueue[queueIndex] = daqNum;
        Xcp_DaqListQueueLength++;
        if(Xcp_DaqListQueueLength > Xcp_MaxQueueLength)
        {
        	Xcp_MaxQueueLength = Xcp_DaqListQueueLength;
        }

        Xcp_Daq[daqNum].sendStatus |= XCP_DAQ_SS_QUEUE;

    }
	return;
}


#if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
/* sort daq by priority */
static FUNC(void,XCP_CODE)
Xcp_SortDaqbypriority(void)
{
	Xcp_DaqNumType tdaqnum = 0;
	uint8 loop,cnt;
	for(loop=0;loop<Xcp_DaqListQueueLength;loop++)
	{
		for(cnt=loop+1;cnt<Xcp_DaqListQueueLength;cnt++)
		{
			if(Xcp_Daq[Xcp_DaqListQueue[loop]].priority < Xcp_Daq[Xcp_DaqListQueue[cnt]].priority)
			{
				tdaqnum = Xcp_DaqListQueue[loop];
				Xcp_DaqListQueue[loop] = Xcp_DaqListQueue[cnt];
				Xcp_DaqListQueue[cnt] = tdaqnum;
			}
		}
	}
}
#endif

/******************************************************************************/
/*
 * @brief      < Event Channel triggered indication >
 *
 * <This function inform the stack that a event channel is triggered>.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <eventNum>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_EventIndication(uint16 eventNum)
{
    uint16 cnt;/* loop counter */
    Xcp_DaqNumType daqNum = 0xff;
    boolean SenddaqFlag = FALSE;
    const Xcp_EvChConfigType* eventPtr = &Xcp_GlobalCfgPtr->XcpEvCh[eventNum];
    for (cnt = 0; cnt < eventPtr->XcpEvChMaxDaqList; cnt++)
    {
    	/* get daq Num */
    	daqNum = eventPtr->XcpEvChTrigDaqListRef[cnt];
    	/* Find the DAQ that is running and matching eventNum */
    	if ((XCP_DAQ_MOD_RUNNING == (XCP_DAQ_MOD_RUNNING &Xcp_Daq[daqNum].mode))
    	     && (eventNum == Xcp_Daq[daqNum].eventChannelNum)
    	   )
    	{

			#if (XCP_PRESCALER_SUPPORTED == STD_ON)
            Xcp_Daq[daqNum].prescalerCnt++;	/* to prescaler */
            if (Xcp_Daq[daqNum].prescalerCnt >= Xcp_Daq[daqNum].prescaler)
            {
                Xcp_Daq[daqNum].prescalerCnt = 0;
            #endif
#if (XCP_PL_STIM == (XCP_PL_STIM & XCP_RESOURCE))
				if (XCP_DAQ_MOD_DIR_STIM == (Xcp_Daq[daqNum].mode & XCP_DAQ_MOD_DIR_BIT))
				{
					/* Not testing */
					Xcp_EventChannelStimHal(daqNum);
				}
				else
#else
				if (XCP_DAQ_MOD_DIR_DAQ == (Xcp_Daq[daqNum].mode & XCP_DAQ_MOD_DIR_BIT))
#endif
				{
					SenddaqFlag = TRUE;
					Xcp_EventChannelDaqHal(eventNum, daqNum);
				}
			#if (XCP_PRESCALER_SUPPORTED == STD_ON)
            }
			#endif
    	}

        if(
        (eventPtr->XcpEvChMaxDaqList == (cnt+1))
        	&& (Xcp_DaqListQueueLength > 0 )
          )
        {
			#if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
        	/* if enable the priority,so Sort according to the priority of DAQ */
        	Xcp_SortDaqbypriority();
			#endif
        }

    }
    if((TRUE == SenddaqFlag) && (0x00 == (Xcp_SendStatus & XCP_DAQ_REQUEST)))
    {
    	Xcp_SendDaq();
    }
    return;
}
/******************************************************************************/
/*
 * @brief      <Event triggered STIM DAQ handler >
 *
 * < copy data into proper destination >.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <eventNum, daqNum>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
static FUNC(void,XCP_CODE) Xcp_EventChannelStimHal(uint16 daqNum)
{
	Xcp_OdtType* OdtPtr = Xcp_Daq[daqNum].odt;
    Xcp_EntryType* EntryPtr;
    Xcp_AGType* destPtr;
    uint8 odtNum; /* odt Num */
    uint8 odtCnt; /* odt counter */
    uint8 entryCnt; /* entry counter */
    uint8 elementCnt; /* element counter */
    uint32 bufferPos = 0; /* data buffer counter */
    for (odtNum = 0; odtNum < Xcp_Daq[daqNum].daqListSize; odtNum++)
    {
        if (0x00u == OdtPtr[odtNum].XcpOdtEntry[0].XcpOdtEntryLength)
        {
            break;
        }
    }

    for (odtCnt = 0; odtCnt < odtNum; odtCnt++)
    {
        EntryPtr = OdtPtr[odtCnt].XcpOdtEntry;
        for (entryCnt = 0; entryCnt < OdtPtr[odtCnt].XcpOdtEntryCount; entryCnt++)
        {
            if (0x00 == EntryPtr[entryCnt].XcpOdtEntryLength)
            {
                break;
            }
            destPtr = EntryPtr[entryCnt].XcpOdtEntryAddr;
            for (elementCnt = 0;
                 elementCnt < EntryPtr[entryCnt].XcpOdtEntryLength;
                 elementCnt++)
            {
                destPtr[elementCnt] = Xcp_Daq[daqNum].eventBufferPtr[bufferPos++];
            }
        }
    }

}

/******************************************************************************/
/*
 * @brief      <Start a DAQ list>
 *
 * <>.
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <daqNum>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <Std_ReturnType>
 */
/******************************************************************************/
static FUNC(Std_ReturnType, XCP_CODE)
Xcp_StartDaq(Xcp_DaqNumType daqNum)
{
	uint16 evChNum = Xcp_Daq[daqNum].eventChannelNum;
    uint8 odtCnt = 0;
    uint8 entryCnt = 0;
    uint32 len = 0;
	Std_ReturnType result = E_OK;
	const Xcp_EvChConfigType *eventChannelPtr = Xcp_GlobalCfgPtr->XcpEvCh;

	if (XCP_DAQ_MOD_RUNNING != (XCP_DAQ_MOD_RUNNING & Xcp_Daq[daqNum].mode))
	{
		Xcp_Daq[daqNum].eventBufferPos = 0;
		#if (XCP_PRESCALER_SUPPORTED == STD_ON)
        Xcp_Daq[daqNum].prescalerCnt = 0u;
        #endif
        Xcp_Daq[daqNum].currentPid = Xcp_Daq[daqNum].firstPid;
        if ((evChNum < XCP_MAX_EVENT_CHANNEL)    /* evChNum is valid and running daq in eventchannel < maxdaqlist */
        	&& (Xcp_EvChActivCnt[evChNum] <= Xcp_GlobalCfgPtr->XcpEvCh[evChNum].XcpEvChMaxDaqList) )
        {
        	for (odtCnt = 0; odtCnt < Xcp_Daq[daqNum].daqListSize; odtCnt++)
        	{
        	    for (entryCnt = 0;
        	         entryCnt < Xcp_Daq[daqNum].odt[odtCnt].XcpOdtEntryCount;
        	         entryCnt++)
        	    {
        	        len += Xcp_Daq[daqNum].odt[odtCnt].XcpOdtEntry[entryCnt].XcpOdtEntryLength;
        	    }
        	}
        	/* set buffer ptr */
        	if ((XCP_DAQ_MOD_DIR_STIM == (Xcp_Daq[daqNum].mode & XCP_DAQ_MOD_DIR_BIT))
        	|| (XCP_EVENT_CONSIST_EVENT == (eventChannelPtr[evChNum].XcpEvChConsistency)))
        	{	/* for event consistency,put all daq to the Xcp_EvChxBuffer[] */
        	    Xcp_Daq[daqNum].eventBufferPtr = &(eventChannelPtr[evChNum].XcpEvChBuffPtr[Xcp_EvChBufferUsage[evChNum]]);
        	    Xcp_EvChBufferUsage[evChNum] += len;
        	}
        	/* for DAQ consist and DAQ direction */
        	else if (XCP_EVENT_CONSIST_DAQ == (eventChannelPtr[evChNum].XcpEvChConsistency))
        	{

        	    len = eventChannelPtr[evChNum].XcpEvChBuffDpth - len; /* put the daq buffer in the end */
        	    Xcp_Daq[daqNum].eventBufferPtr = &(eventChannelPtr[evChNum].XcpEvChBuffPtr[len]);
        	}
        	/* event buffer memory overflow */

        	else /* odt consistency,do nothing */
        	{
        	    /* Do Nothing */
        	}
        	Xcp_Daq[daqNum].mode |= XCP_DAQ_MOD_RUNNING;
        	Xcp_SessionStatus |= XCP_SESSION_STATE_DAQ_RUNNING;
        	Xcp_EvChActivCnt[evChNum]++;
        }
	}
	else
	{
	    result = E_NOT_OK;
	}
	return result;
}
static FUNC(void, XCP_CODE)
Xcp_StopDaq(Xcp_DaqNumType daqNum)
{
    uint16 evChNum = Xcp_Daq[daqNum].eventChannelNum;

    if (XCP_DAQ_MOD_RUNNING == (XCP_DAQ_MOD_RUNNING&(Xcp_Daq[daqNum].mode)))
    {
        Xcp_Daq[daqNum].mode &= (uint8)(~XCP_DAQ_MOD_RUNNING);
        Xcp_Daq[daqNum].sendStatus &= XCP_DAQ_SS_AL;  /* clear DAQ sendStatus flag */
		Xcp_Daq[daqNum].eventBufferPtr = NULL_PTR;
        Xcp_EvChActivCnt[evChNum]--;
        if (0x00 == Xcp_EvChActivCnt[evChNum])
        {
            Xcp_EvChBufferUsage[evChNum] = 0;
        }
    }
    return;
}

/******************************************************************************/
/*
 * @brief               <Xcp_SetDaqPtr>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_SetDaqPtr(void)
{
    uint16 daqListNo = 0xff;
    uint8 odtNo = 0xff;
    uint8 odtEntryNo = 0xff;
	#if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
	if ((Xcp_CmdLength != 6u)
	&& (Xcp_CmdLength != XCP_CAN_MAX_DLC))
	{
		Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
    #endif
		if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
		{
			Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
			Xcp_RespLength = 0x02u;
		}
		else
		{
			/* get the daq List No from CommandRxObject[2-3] buffer */
			Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &daqListNo, CPU_BYTE_ORDER);
			/* get the Odt number from CommandRxObject[4] buffer */
			odtNo = Xcp_CmdBuffer[4u];
			/* get the Odt Entry number from CommandRxObject[5] buffer */
			odtEntryNo =  Xcp_CmdBuffer[5u];
			/* check the daq/odt/entry num */
			if ((daqListNo < XCP_MAX_DAQ)
			#if ( XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE)
			&& (0u != Xcp_Daq[daqListNo].daqListSize)
			#endif
			&& (odtNo < Xcp_Daq[daqListNo].daqListSize)
			&& (odtEntryNo < Xcp_Daq[daqListNo].odt[odtNo].XcpOdtEntryCount)
			)
			{
				/* check whether DAQ is running at this moment */
				if (XCP_DAQ_MOD_RUNNING == (Xcp_Daq[daqListNo].mode & XCP_DAQ_MOD_RUNNING))
				{
					Xcp_SetErrorCode(XCP_ERR_DAQ_ACTIVE);
					Xcp_RespLength = 0x02u;
				}
				else
				{
					Xcp_PtrDaq   = daqListNo; /* set daq point */
					Xcp_PtrOdt 	= odtNo;
					Xcp_PtrEntry = odtEntryNo;
				}
			}
			else /* DAQ/ODT/entry num check failed */
			{
				Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
				Xcp_RespLength = 0x02u;
			}
		}
	#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    }
	#endif
	Xcp_SendResp();
	return;
}


/******************************************************************************/
/*
 * @brief               <Xcp_WriteDaq>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_WriteDaq(void)
{

    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 0x08)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    /* Daq List Number used in the previous SET_DAQ_PTR has to be in the range */
    else if ((Xcp_PtrDaq < XCP_MIN_DAQ)
    || (Xcp_PtrDaq >= XCP_MAX_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_WRITE_PROTECTED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_WriteDaqHal();
    }
    Xcp_SendResp();
    return;
}
static FUNC(void,XCP_CODE)
Xcp_WriteDaqHal(void)
{
	#if 0
	uint8 bitOffSet;
	#endif /* don't support bit temporary */

    uint8 sizeOfDaqElement = 0;
    uint8 addressExtension = 0;
    uint32 addressOfDaqElement = 0;
    uint32 addressOfOdtEntry = 0;

    uint8 entryNum;
    uint16 len = 0;

	#if 0
	bitOffSet = Xcp_CmdBuffer[1u];
	#endif /* we don't support bit_stim in this version */
	sizeOfDaqElement = Xcp_CmdBuffer[2u];
	addressExtension = Xcp_CmdBuffer[3u];
	/* get the addressOfDaqElement from CommandRxObject[4-7] buffer */
	Xcp_CopyU1BufferToU4(&Xcp_CmdBuffer[4u], &addressOfDaqElement, CPU_BYTE_ORDER);
	/* get the address */
	addressOfOdtEntry = Xcp_Mta2Ptr(addressExtension,addressOfDaqElement);
	 if (
	    ((DAQ == Xcp_GlobalCfgPtr->XcpDaqList[Xcp_PtrDaq].XcpDaqListType)
	        && (sizeOfDaqElement > XCP_ODT_ENTRY_SIZE_DAQ))
	    || ((STIM == Xcp_GlobalCfgPtr->XcpDaqList[Xcp_PtrDaq].XcpDaqListType)
	        && (sizeOfDaqElement > XCP_ODT_ENTRY_SIZE_STIM))
	    || ((DAQ_STIM == Xcp_GlobalCfgPtr->XcpDaqList[Xcp_PtrDaq].XcpDaqListType)
	        && (sizeOfDaqElement > XCP_ODT_ENTRY_SIZE_DAQ)
	        && (sizeOfDaqElement > XCP_ODT_ENTRY_SIZE_STIM))
	    || (FALSE == Xcp_CheckAddress(addressOfOdtEntry,
	                                    (sizeOfDaqElement*XCP_AG), XCP_MEM_ALL))
	    )
	 {
		 Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
		 Xcp_RespLength = 0x02u;
	 }
	 /* check ptr whether valid */
	 else if ((Xcp_PtrOdt >= Xcp_Daq[Xcp_PtrDaq].daqListSize)
	 || (Xcp_PtrEntry >= Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntryCount))
	 {
	     Xcp_SetErrorCode(XCP_ERR_ACCESS_DENIED);
	     Xcp_RespLength = 0x02u;
	 }
	 /* check whether DAQ Running at this moment */
	 else if (XCP_DAQ_MOD_RUNNING == (Xcp_Daq[Xcp_PtrDaq].mode & XCP_DAQ_MOD_RUNNING))
	 {
	     Xcp_SetErrorCode(XCP_ERR_DAQ_ACTIVE);
	     Xcp_RespLength = 0x02u;
	 }
	 else
	 {
	    for (entryNum = 0; entryNum < Xcp_PtrEntry; entryNum++)
	    {
	    	len += Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[entryNum].XcpOdtEntryLength;
	    }
	    len += sizeOfDaqElement;
	    len *= XCP_AG;
		 /* check length of this daq-dto frame */
		 if (XCP_MAX_WRITEDAQ_SIZE >= len)
		 {
		     Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[Xcp_PtrEntry].XcpOdtEntryLength  = sizeOfDaqElement;
		     Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[Xcp_PtrEntry].XcpOdtEntryAddr	   = (Xcp_AGType*)addressOfOdtEntry;
		     Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[Xcp_PtrEntry].XcpOdtEntryNumber  = Xcp_PtrEntry;
		     /* The DAQ list pointer is auto post incremented*/
		     Xcp_PtrEntry ++;
		 }
		 else
		 {
		     Xcp_SetErrorCode(XCP_ERR_DAQ_CONFIG);
		     Xcp_RespLength = 0x02u;
		 }
	 }
}

/******************************************************************************/
/*
 * @brief               <Xcp_SetDaqListMode>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_SetDaqListMode(void)
{
	#if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
	if ((Xcp_CmdLength != 8u)
	&& (Xcp_CmdLength != XCP_CAN_MAX_DLC))
	{
		Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
	if (Xcp_PgmStauts != XCP_PGM_IDLE)
	{
		Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	/* check protection*/
	if ((XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
	|| (XCP_PL_STIM == (Xcp_ProtectionStatus & XCP_PL_STIM)))
	{
		Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
		Xcp_RespLength = 0x02u;
	}
	else
	{
		Xcp_SetDaqListModeHal();
	}
	Xcp_SendResp();
	return;
}
static FUNC(void,XCP_CODE)
Xcp_SetDaqListModeHal(void)
{
    uint8 mode = 0;
    uint16 daqListNo = 0;
    uint16 evChNo = 0;
    uint8 transRatePrescaler = 0;
    uint8 daqPriority = 0;
    P2CONST(Xcp_EvChConfigType, AUTOMATIC, XCP_CONST_PBCFG)evchPtr = Xcp_GlobalCfgPtr->XcpEvCh;
    Xcp_DaqNumType cnt;
    /* get the detail parmeters from CommandRxObject[1] buffer*/
    mode = Xcp_CmdBuffer[1u];
    /* get the mode from CommandRxObject[2-3] buffer */
    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &daqListNo, CPU_BYTE_ORDER);
    /* get the mode from CommandRxObject[4-5] buffer */
    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[4u], &evChNo, CPU_BYTE_ORDER);
    transRatePrescaler = Xcp_CmdBuffer[6u];
    daqPriority = Xcp_CmdBuffer[7u];
    if ( /* check input parameter */
    	(evChNo >= XCP_MAX_EVENT_CHANNEL)
    	|| (daqListNo >= XCP_MAX_DAQ)
		#if (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE )
		|| (0u == Xcp_Daq[daqListNo].daqListSize) /* dynamic,the daqlistsize can't eaqul 0 */
		#endif
		#if (STD_OFF == XCP_PRESCALER_SUPPORTED )
		|| (0x01 != transRatePrescaler) /*without prescaler,the Prescaler must = 1*/
		#else
		|| (0x00 == transRatePrescaler) /*with prescaler,the Prescaler must >= 1*/
		#endif
		#if (STD_OFF == XCP_DAQ_PRIORITY_SUPPORT)
		|| (daqPriority > 0u)
		#endif
       )
    {
        Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
        Xcp_RespLength = 0x02u;
    }
    else if(/* check if alternating is illegal */
    		/* 1. only  DIRECTION=DAQ ,can set alternating flag*/
    		((XCP_DAQ_MOD_AL_BIT|XCP_DAQ_MOD_DIR_STIM)== (mode & (XCP_DAQ_MOD_AL_BIT|XCP_DAQ_MOD_DIR_BIT)))
    		/* can not allowed to set the ALTERNATING flag and the TIMESTAMP flag at the same time. */
    		|| ((XCP_DAQ_MOD_AL_BIT|XCP_DAQ_MOD_TS) == (mode & (XCP_DAQ_MOD_AL_BIT|XCP_DAQ_MOD_TS)))
    		/* illegal timestamp disable */
			#if (XCP_TS_NO_TS != XCP_TIMESTAMP_TYPE)
			#if (XCP_TIMESTAMP_FIXED == STD_ON)  /* If the Tinmestump fixed is on,so the mode must set XCP_DAQ_MOD_TS*/
			|| (XCP_DAQ_MOD_TS != (XCP_DAQ_MOD_TS & tMode))
			#endif
			#endif
		    /* illegal timestamp enable */
		    #if (XCP_TS_NO_TS == XCP_TIMESTAMP_TYPE)
		    || (XCP_DAQ_MOD_TS == (XCP_DAQ_MOD_TS & mode)) /* when ts_type is no_ts,so the mode can't set XCP_DAQ_MOD_TS */
		    #endif
		    /* illegal pid_off enable */
		    #if (STD_OFF == XCP_PID_OFF_SUPPORT)
		    || (XCP_DAQ_MOD_PIDOFF == (XCP_DAQ_MOD_PIDOFF & mode))
		    #endif
		    /* illegal direction */
		    || ((XCP_DAQ_MOD_DIR_STIM == (mode & XCP_DAQ_MOD_DIR_BIT))  /* DaqListtype in mode parameter is not match to XcpDaqList*/
		        && (DAQ == (Xcp_GlobalCfgPtr->XcpDaqList[daqListNo].XcpDaqListType)))
		    || ((XCP_DAQ_MOD_DIR_DAQ == (mode & XCP_DAQ_MOD_DIR_BIT))
		        && (STIM == (Xcp_GlobalCfgPtr->XcpDaqList[daqListNo].XcpDaqListType)))
    	   )
    {
        Xcp_SetErrorCode(XCP_ERR_MODE_NOT_VALID);
        Xcp_RespLength = 0x02u;
    }
    /* check whether DAQ Running at this moment */
    else if (XCP_DAQ_MOD_RUNNING == (Xcp_Daq[daqListNo].mode & XCP_DAQ_MOD_RUNNING))
    {
        Xcp_SetErrorCode(XCP_ERR_DAQ_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
    	/* check if the eventchannel Num is valid */
    	for (cnt = 0;cnt < evchPtr[evChNo].XcpEvChMaxDaqList;cnt++)
    	{
    		if (evchPtr[evChNo].XcpEvChTrigDaqListRef[cnt] == daqListNo)
    		{
    			break;
    		}
    	}
    	if (cnt != evchPtr[evChNo].XcpEvChMaxDaqList)
    	{
    	    Xcp_Daq[daqListNo].eventChannelNum = evChNo;
    	    /* first clear these flag */
    	    Xcp_Daq[daqListNo].sendStatus  &= (uint8)(~XCP_DAQ_SS_AL);
    	    Xcp_Daq[daqListNo].sendStatus |= (mode & XCP_DAQ_SS_AL);
    	    Xcp_Daq[daqListNo].mode &= (uint8)(~(XCP_DAQ_MOD_DIR_BIT|XCP_DAQ_MOD_TS|XCP_DAQ_MOD_PIDOFF));
    	    Xcp_Daq[daqListNo].mode |= (mode & (XCP_DAQ_MOD_DIR_BIT|XCP_DAQ_MOD_TS|XCP_DAQ_MOD_PIDOFF));

    	    #if (XCP_PRESCALER_SUPPORTED == STD_ON)
    	    Xcp_Daq[daqListNo].prescaler = transRatePrescaler;
    	    #endif

    	    #if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
    	    Xcp_Daq[daqListNo].priority = daqPriority;
    	    #endif
    	}
    	else
    	{
    	    Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
    	    Xcp_RespLength = 0x02u;
    	}
    }
}

/******************************************************************************/
/*
 * @brief               <Xcp_StartStopDaqList>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_StartStopDaqList(void)
{

    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 4u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_StartStopDaqListHal();
    }
    Xcp_SendResp();
    return;
}
static FUNC(void,XCP_CODE)
Xcp_StartStopDaqListHal(void)
{
	Std_ReturnType result = E_NOT_OK;
	uint8 tMode = 0xff;
	uint16 daqListNo = 0;
    tMode = Xcp_CmdBuffer[0x01];
    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[0x02], &daqListNo, CPU_BYTE_ORDER);
    if (	/* check daqListNo  */
    	(daqListNo >= XCP_MAX_DAQ)
    #if (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE)
    || (0u == Xcp_Daq[daqListNo].daqListSize)
    || (Xcp_Daq[daqListNo].DynaCfgStatus != XCP_DAQ_CFG_DONE)
    #endif
    )
    {
        Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_RespBuffer[1u] = Xcp_Daq[daqListNo].firstPid;
        Xcp_RespLength = 0x02u;
        switch (tMode)
        {
            case XCP_START_STOP_MOD_STOP:
            {
                Xcp_StopDaq((Xcp_DaqNumType)daqListNo);
                /* Check that whether all DAQs are not running */
                for (daqListNo = 0; daqListNo < XCP_MAX_DAQ; daqListNo++)
                {
                    if (XCP_DAQ_MOD_RUNNING == (XCP_DAQ_MOD_RUNNING & Xcp_Daq[daqListNo].mode))
                    {
                        break;
                    }
                }
                if (daqListNo == XCP_MAX_DAQ) /* mean no daq is running */
                {
                    Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_DAQ_RUNNING);
                }
                break;
            }
            case XCP_START_STOP_MOD_START:
            {
            	result = Xcp_StartDaq((Xcp_DaqNumType)daqListNo);
            	if (E_NOT_OK == result)
            	{
            		Xcp_SetErrorCode(XCP_ERR_DAQ_CONFIG);
            		Xcp_RespLength = 0x02u;
            	}
                break;
            }
            case XCP_START_STOP_MOD_SELECT:
            {
                Xcp_Daq[daqListNo].mode |= XCP_DAQ_MOD_SELE;
                break;
            }
            default:
            {
                Xcp_SetErrorCode(XCP_ERR_MODE_NOT_VALID);
                Xcp_RespLength = 0x02u;
                break;
            }
        }
    }
}

/******************************************************************************/
/*
 * @brief               <Xcp_StartStopSynch>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_StartStopSynch(void)
{
    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 2u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
	{
	    Xcp_StartStopSynchHal();
    }
    Xcp_SendResp();
    return;
}

static FUNC(void,XCP_CODE)
Xcp_StartStopSynchHal(void)
{
	Std_ReturnType result = E_NOT_OK;
	boolean runningFlag = FALSE;
	boolean findSeleDaqFlag = FALSE;
	uint8 tMode = 0xff;
	uint16 cnt = 0;
	tMode = Xcp_CmdBuffer[1u];
	switch (tMode)
	{
		case XCP_START_STOP_MOD_STOP_ALL:
	    {
	        for (cnt = 0; cnt < XCP_MAX_DAQ; cnt++)
	        {
	            Xcp_Daq[cnt].mode &= (uint8)(~XCP_DAQ_MOD_RUNNING);
	        }
	        Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_DAQ_RUNNING);
	        for (cnt = 0; cnt < XCP_MAX_EVENT_CHANNEL; cnt++)
	        {
	            Xcp_EvChBufferUsage[cnt] = 0;
	            Xcp_EvChActivCnt[cnt] = 0;
	        }
	        break;
	    }

		case XCP_START_STOP_MOD_START_SELECT:
		{
			for (cnt = 0; cnt < XCP_MAX_DAQ; cnt++)
			{
				if (XCP_DAQ_MOD_SELE == (XCP_DAQ_MOD_SELE & Xcp_Daq[cnt].mode))
				{
					findSeleDaqFlag = TRUE;
					result = Xcp_StartDaq((Xcp_DaqNumType)cnt);
					if (E_OK == result)
					{
						Xcp_Daq[cnt].mode &= (uint8)(~XCP_DAQ_MOD_SELE);
					}
					else
					{
						Xcp_SetErrorCode(XCP_ERR_DAQ_CONFIG);
						Xcp_RespLength = 0x02u;
					}
				}
			}
			if(FALSE == findSeleDaqFlag)
			{
				Xcp_SetErrorCode(XCP_ERR_DAQ_CONFIG);
				Xcp_RespLength = 0x02u;
			}
			break;
		}
		case XCP_START_STOP_MOD_STOP_SELECT:
		{
			for (cnt = 0; cnt < XCP_MAX_DAQ; cnt++)
			{
				if (XCP_DAQ_MOD_SELE == (XCP_DAQ_MOD_SELE & Xcp_Daq[cnt].mode))
				{
					Xcp_StopDaq((Xcp_DaqNumType)cnt);
					Xcp_Daq[cnt].mode &= (uint8)(~XCP_DAQ_MOD_SELE);
				}
				/* Check if there is running daq which without selected */
				else if (XCP_DAQ_MOD_RUNNING == (XCP_DAQ_MOD_RUNNING & Xcp_Daq[cnt].mode))
				{
					runningFlag = TRUE;
				}
				else
				{
					/* Nothing to do */
				}
			}
            if (FALSE == runningFlag)
            {
                Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_DAQ_RUNNING);
            }
			break;
		}
        default:
        {
            Xcp_SetErrorCode(XCP_ERR_MODE_NOT_VALID);
            Xcp_RespLength = 0x02u;
            break;
        }
	}

}

/* CMD:WRITE_DAQ_MULTIPLE:now don't support */

#if (STD_ON == XCP_READ_DAQ)
/******************************************************************************/
/*
 * @brief               <Xcp_ReadDaq>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_ReadDaq(void)
{
    uint32 elementaddress;

    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 0x01u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    /* Daq List Number used in the previous SET_DAQ_PTR has to be in the range */
    else if ((Xcp_PtrDaq >= XCP_MAX_DAQ)
    || (Xcp_PtrOdt >= Xcp_Daq[Xcp_PtrDaq].daqListSize)
    || (Xcp_PtrEntry >= Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntryCount))
    {
        Xcp_SetErrorCode(XCP_ERR_RES_TEMP_NOT_A);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        /* Position of bit in 32-bit variable */
        Xcp_RespBuffer[1u] = 0xFFu;

        /* Size of DAQ element [AG] */
        Xcp_RespBuffer[2u] = Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[Xcp_PtrEntry].XcpOdtEntryLength;

        /* Address extension of DAQ element */
        Xcp_RespBuffer[3u] =  0x0u;

        /* Address of DAQ element */
        elementaddress =  (uint32)(Xcp_Daq[Xcp_PtrDaq].odt[Xcp_PtrOdt].XcpOdtEntry[Xcp_PtrEntry].XcpOdtEntryAddr);

        /* Put the ElementAddress to RespBuffer[4]-[7] buffer */
        Xcp_CopyU4ToU1Buffer(elementaddress, &Xcp_RespBuffer[4u], CPU_BYTE_ORDER);

        Xcp_RespLength = 0x08u;
    }
    Xcp_SendResp();

    return;
}
#endif /* (STD_ON == XCP_READ_DAQ) */

/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqClock>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
#if ((STD_ON == XCP_GET_DAQ_CLOCK ) && (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS))
FUNC(void,XCP_CODE)
Xcp_GetDaqClock (void)
{
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 1u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus&XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {

        /* Put the Receive Timestamp to RespBuffer[4]-[7] buffer */
        Xcp_CopyU4ToU1Buffer((uint32)Xcp_RxTimeStamp, &(Xcp_RespBuffer[4u]), CPU_BYTE_ORDER);
        Xcp_RespLength = 0x08u;
    }
    Xcp_SendResp();
    return;
}
#endif /* ((STD_ON == XCP_GET_DAQ_CLOCK ) && (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)) */

#if (STD_ON == XCP_GET_DAQ_PROCESSOR_INFO )
/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqProcessorInfo>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_GetDaqProcessorInfo(void)
{
	uint8 daqProperties = 0;
	#if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
	if ((Xcp_CmdLength != 1u)
	&& (Xcp_CmdLength != XCP_CAN_MAX_DLC))
	{
		Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
	if (Xcp_PgmStauts != XCP_PGM_IDLE)
	{
		Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	/* check protection status */
	if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
	{
		Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
		Xcp_RespLength = 0x02u;
	}
	else
	{
		/* daq properties depends on the macros configured in xcp_cfg.h */
		daqProperties = (XCP_DAQ_CONFIG_TYPE
		|(XCP_PRESCALER_SUPPORTED << 1u)
		|(XCP_RESUME_SUPPORT << 2u)
		|(XCP_BIT_STIM_SUPPORT << 3u)
		|(XCP_PID_OFF_SUPPORT << 5u)
		|(XCP_DAQ_OVL_INDICATION << 6u));
		/* TIMESTAMP_SUPPORTED */
		#if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
		    daqProperties |= (uint8)(1u << 4u);
		#else /**<  TIMESTAMP NOT SUPPORTED */
		    daqProperties |= (uint8)(0u << 4u);
		#endif
		Xcp_RespBuffer[1u] = daqProperties;
		Xcp_CopyU2ToU1Buffer((uint16)XCP_MAX_DAQ, &Xcp_RespBuffer[2u], CPU_BYTE_ORDER);
		Xcp_CopyU2ToU1Buffer((uint16)XCP_MAX_EVENT_CHANNEL, &Xcp_RespBuffer[4u], CPU_BYTE_ORDER);

		Xcp_RespBuffer[6u] = XCP_MIN_DAQ;

		Xcp_RespBuffer[7u] = ((XCP_IDENTIFICATION_FIELD_TYPE << 6u)/**< Identification Field Type*/
		                     |(0x3u << 4u)/**< The ADDR EXTENSION flag */
		                     |(0x0u));/**< The Optimisation Type */

		Xcp_RespLength = 0x08u;
	}
	Xcp_SendResp();
	return;
}
#endif /* (STD_ON == XCP_GET_DAQ_PROCESSOR_INFO ) */

#if (STD_ON == XCP_GET_DAQ_RESOLUTION_INFO)
/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqProcessorInfo>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_GetDaqResolutionInfo(void)
{
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 1u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_RespBuffer[1u] = XCP_AG;
        Xcp_RespBuffer[2u] = XCP_ODT_ENTRY_SIZE_DAQ;
        Xcp_RespBuffer[3u] = XCP_AG;
        Xcp_RespBuffer[4u] = XCP_ODT_ENTRY_SIZE_STIM;
        #if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
        Xcp_RespBuffer[5u] = (XCP_TIMESTAMP_TYPE
                             |(XCP_TIMESTAMP_FIXED << 3u)
                             |(XCP_TIMESTAMP_UNIT << 4u));

        Xcp_CopyU2ToU1Buffer(XCP_TIMESTAMP_TICKS, &Xcp_RespBuffer[6u], CPU_BYTE_ORDER);
        #endif
        Xcp_RespLength = 0x08u;
    }
    Xcp_SendResp();
    return;
}
#endif /* (STD_ON == XCP_GET_DAQ_RESOLUTION_INFO) */

#if (STD_ON == XCP_GET_DAQ_LIST_MODE)
/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqProcessorInfo>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_GetDaqListMode(void)
{
    uint16 daqListNo = 0xff;

    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 4u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &daqListNo, CPU_BYTE_ORDER);
        if (daqListNo < XCP_MAX_DAQ)
        {
            Xcp_RespBuffer[1u] =  Xcp_Daq[daqListNo].mode;

            /* Put the currentEventChannelNo to RespBuffer[4]-[5] buffer */
            Xcp_CopyU2ToU1Buffer(Xcp_Daq[daqListNo].eventChannelNum, &Xcp_RespBuffer[4u], CPU_BYTE_ORDER);

            #if (XCP_PRESCALER_SUPPORTED == STD_ON)
            Xcp_RespBuffer[6u] = Xcp_Daq[daqListNo].prescaler;
            #endif

            #if (XCP_DAQ_PRIORITY_SUPPORT == STD_ON)
            Xcp_RespBuffer[7u] = Xcp_Daq[daqListNo].priority;
            #endif

            /* response length is 8 */
            Xcp_RespLength = 0x08u;
        }
        else
        {
            Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
            Xcp_RespLength = 0x02u;
        }
    }
    Xcp_SendResp();
    return;
}
#endif /* (STD_ON == XCP_GET_DAQ_LIST_MODE) */

#if (STD_ON == XCP_GET_DAQ_EVENT_INFO)
/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqEventInfo>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE) Xcp_GetDaqEventInfo(void)
{
    uint16 eventChannelNo = 0xff;

    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 4u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &eventChannelNo, CPU_BYTE_ORDER);

        if (eventChannelNo <  XCP_MAX_EVENT_CHANNEL)
        {
            Xcp_RespBuffer[1u] = (uint8)((Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChType << 2u)
                                | (Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChConsistency << 6u));
            Xcp_RespBuffer[2u] = Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChMaxDaqList;

            /* EVENT_CHANNEL_NAME_LENGTH in bytes ,0 If not available */
            Xcp_RespBuffer[3u] = Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChNameLength;

            /* set MTA for upload the event channel name */
            Xcp_MTA.transferAddr = (uint32)(Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChNamePtr);
            Xcp_MTA.extensionAddr = 0x0u;
            Xcp_UploadInfoLen = ((((uint32)Xcp_RespBuffer[3u] - 1u) / XCP_AG) + 1u);

            Xcp_RespBuffer[4u] = Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChTimeCycle;
            Xcp_RespBuffer[5u] = Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChTimeUnit;
            Xcp_RespBuffer[6u] = Xcp_GlobalCfgPtr->XcpEvCh[eventChannelNo].XcpEvChPriority;

            Xcp_RespLength = 0x07u;
        }
        else
        {
            Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
            Xcp_RespLength = 0x02u;
        }
    }
    Xcp_SendResp();
    return;
}
#endif /* (XCP_GET_DAQ_EVENT_INFO == STD_ON) */

#if (XCP_DAQ_STATIC == XCP_DAQ_CONFIG_TYPE )
/******************************************************************************/
/*
 * @brief               <Xcp_ClearDaqList>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_ClearDaqList(void)
{
	uint16 daqListNo;
    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 4u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus&XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
    	Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2], &daqListNo, CPU_BYTE_ORDER);
    	if (daqListNo < XCP_MAX_DAQ)
    	{
    		Xcp_StopDaq((Xcp_DaqNumType)daqListNo);
    		Xcp_ClearDaq((Xcp_DaqNumType)daqListNo);
    		/* Check if there is no daq is running */
            for (daqListNo = 0; daqListNo < XCP_MAX_DAQ; daqListNo++)
            {
                if (XCP_DAQ_MOD_RUNNING == (XCP_DAQ_MOD_RUNNING & Xcp_Daq[daqListNo].mode))
                {
                    break;
                }
            }
            /* if no daq is running,reset the Running flag */
            if (daqListNo == XCP_MAX_DAQ)
            {
                Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_DAQ_RUNNING);
            }
    	}
    	else
    	{
    		Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
    		Xcp_RespLength = 0x02u;
    	}
    }
    Xcp_SendResp();
    return;
}

#if (XCP_GET_DAQ_LIST_INFO == STD_ON)
/******************************************************************************/
/*
 * @brief               <Xcp_GetDaqListInfo>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_GetDaqListInfo(void)
{
	uint8 daqproperty = 0;
	uint16 daqListNo = 0;
	Xcp_DaqNumType daqNum = 0;
	uint16 evChNum = 0;
	uint16 fixedEvent = 0;
	uint16 findEVCount = 0;

	Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &daqListNo, CPU_BYTE_ORDER);
	if((daqListNo < XCP_MIN_DAQ) || (daqListNo >= XCP_MAX_DAQ))
	{
		Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
		Xcp_RespLength = 0x02u;
	}
	else
	{
		/* find event channel Number for this Daq */
		for (evChNum = 0u;evChNum < XCP_MAX_EVENT_CHANNEL;evChNum++)
		{
			for(daqNum = 0u;daqNum < (Xcp_GlobalCfgPtr->XcpEvCh[evChNum].XcpEvChMaxDaqList);daqNum++)
			{
				 if (daqListNo == Xcp_GlobalCfgPtr->XcpEvCh[evChNum].XcpEvChTrigDaqListRef[daqNum])
				 {
					 /* get the No of the Event Channel for the current daq list No */
					 fixedEvent = Xcp_GlobalCfgPtr->XcpEvCh[evChNum].XcpEvChNumber;
					 findEVCount++;
					 break;
				 }
			}
			if (findEVCount >= 2u)  /* Indicating that this DAQ has no fixed event channel,so the 4th response is null */
			{
				break;
			}
		}
	}
	#if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
	if ((Xcp_CmdLength != 4u)
	&& (Xcp_CmdLength != XCP_CAN_MAX_DLC))
	{
		Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
	if (Xcp_PgmStauts != XCP_PGM_IDLE)
	{
		Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
		Xcp_RespLength = 0x02u;
	}
	else
	#endif
	/* check protection status */
	if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
	{
		Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
		Xcp_RespLength = 0x02u;
	}
	else if (daqListNo < XCP_MAX_DAQ)
	{
		 /* max_odt & max_odt_entries*/
		 #if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC)
		 Xcp_RespBuffer[2u] = Xcp_GlobalCfgPtr->XcpDaqList[daqListNo].XcpMaxOdt;
		 Xcp_RespBuffer[3u] = Xcp_GlobalCfgPtr->XcpDaqList[daqListNo].XcpMaxOdtEntries;
		 #endif
		 /* get daq direction */
		 daqproperty = (uint8)(Xcp_GlobalCfgPtr->XcpDaqList[daqListNo].XcpDaqListType << 2u);
	     /* predef of properties */
	     if (daqListNo < XCP_MIN_DAQ)
	     {
	    	 daqproperty |= 0x01u;
	     }
	     Xcp_RespBuffer[1u] = daqproperty;
	     /* Bit fixed event */
	     switch (findEVCount)
	     {

	     	 /* No event channel can be asigned to this daq */
	         case 0x00u:
	         {
	             Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
	             Xcp_RespLength = 0x02u;
	             break;
	         }
	         case 0x01u:
	         {	/* EVENT_FIXED */
	        	 Xcp_RespBuffer[1u] |= 0x02u;
	             //Xcp_CopyU2ToU1Buffer(fixedEvent, &Xcp_RespBuffer[4u],CPU_BIT_ORDER);
	             Xcp_RespLength = 0x06u;
	             break;
	         }
	         /* not fixed */
	         default:
	         {
	             Xcp_RespLength = 0x06u;
	             break;
	         }
	     }
	}
	else
	{
	    Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
	    Xcp_RespLength = 0x02u;
	}
	Xcp_SendResp();
	return;
}
#endif /* (XCP_GET_DAQ_LIST_INFO == STD_ON) */
/*
 * DYNAMIC CONFIG COMMAND
 */
#else
/******************************************************************************/
/*
 * @brief               <Xcp_FreeDaq>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_FreeDaq (void)
{
    uint16 cnt;

    /* check frame length */
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 1u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection status */
    if (XCP_PL_DAQ == (Xcp_ProtectionStatus & XCP_PL_DAQ))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        for (cnt = 0; cnt < XCP_MAX_DAQ; cnt++)
        {
            Xcp_Daq[cnt].mode &= (uint8)(~XCP_DAQ_MOD_RUNNING);
        }
        Xcp_SessionStatus &= (uint8)(~XCP_SESSION_STATE_DAQ_RUNNING);
        for (cnt = 0; cnt < XCP_MAX_EVENT_CHANNEL; cnt++)
        {
            Xcp_EvChBufferUsage[cnt] = 0;
            Xcp_EvChActivCnt[cnt] = 0;
        }
        Xcp_ClearAllDaq();
        /* The XCP_DAQ_CFG_FREE state must be modified after FREE_DAQ is successful*/
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_FREE;
    }
    Xcp_SendResp();
    return;
}

/******************************************************************************/
/*
 * @brief               <Xcp_AllocDaq>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_AllocDaq(void)
{
    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 4u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    if (XCP_PL_DAQ == (XCP_PL_DAQ & Xcp_ProtectionStatus))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    /* check daq cfg sequence */
    /* ALLOC_DAQ can use after Free_Daq or Alloc_daq */
    else if ((XCP_DAQ_CFG_FREE != Xcp_DaqCfgSeqStat)
    && (XCP_DAQ_CFG_DAQ != Xcp_DaqCfgSeqStat))

    {
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_IDLE;
        Xcp_SetErrorCode(XCP_ERR_SEQUENCE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_AllocDaqHal();
    }
    Xcp_SendResp();
    return;
}

static FUNC(void,XCP_CODE)
Xcp_AllocDaqHal(void)
{
    uint16 daqCount = 0;
    Xcp_DaqNumType daqNum = 0;

    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2], &daqCount, CPU_BYTE_ORDER);
    /* check daqCount */
    if (daqCount > XCP_DAQ_COUNT)
    {
        Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        for (daqNum = XCP_MIN_DAQ;
             daqNum < (XCP_MIN_DAQ + daqCount);
             daqNum++)
        {
            Xcp_Daq[daqNum].DynaCfgStatus = XCP_DAQ_CFG_DAQ;
        }
        /* The remaining DAQ status is configured as XCP_DAQ_CFG_FREE */
        for (; daqNum < XCP_MAX_DAQ; daqNum++)
        {
            Xcp_Daq[daqNum].DynaCfgStatus = XCP_DAQ_CFG_FREE;
        }
		#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
		Xcp_DynamicDaqFirstPIDCnt = 0;
		#endif
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_DAQ;
    }
    return;
}

/******************************************************************************/
/*
 * @brief               <Xcp_AllocOdt>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE) Xcp_AllocOdt(void)
{

    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 5u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection */
    if (XCP_PL_DAQ == (XCP_PL_DAQ & Xcp_ProtectionStatus))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    /* check daq cfg sequence */
    /* ALLOC_ODT can use after ALLOC_DAQ or ALLOC_ODT */
    else if ((XCP_DAQ_CFG_DAQ != Xcp_DaqCfgSeqStat)
    && (XCP_DAQ_CFG_ODT != Xcp_DaqCfgSeqStat))
    {
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_IDLE;
        Xcp_SetErrorCode(XCP_ERR_SEQUENCE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_AllocOdtHal();
    }
    Xcp_SendResp();
    return;
}

static FUNC(void,XCP_CODE)
Xcp_AllocOdtHal(void)
{
    uint16 daqListNo = 0x0;
    uint8 odtCount = Xcp_CmdBuffer[4u];

    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2u], &daqListNo, CPU_BYTE_ORDER);

    if (
    	((daqListNo < XCP_MIN_DAQ) || (daqListNo >= XCP_MAX_DAQ))
    	|| ((XCP_DAQ_CFG_DAQ != Xcp_Daq[daqListNo].DynaCfgStatus)
    		 && (XCP_DAQ_CFG_ODT != Xcp_Daq[daqListNo].DynaCfgStatus))
       )
    {
        Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
        Xcp_RespLength = 0x02u;
    }

    else if (XCP_DYNAMIC_DAQ_ODT_ENTRY < Xcp_DynDaqOdtEntryPos)
    {
        Xcp_SetErrorCode(XCP_ERR_MEMORY_OVERFLOW);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_Daq[daqListNo].daqListSize = odtCount;

		#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
        Xcp_Daq[daqListNo].firstPid = Xcp_DynamicDaqFirstPIDCnt;
        Xcp_DynamicDaqFirstPIDCnt += odtCount;
		#endif
        Xcp_Daq[daqListNo].odt = (Xcp_OdtType*)&Xcp_DynDaqOdt[Xcp_DynDaqOdtPos];
        Xcp_DynDaqOdtPos ++;
        do
        {
            odtCount--;
            Xcp_Daq[daqListNo].odt[odtCount].XcpOdtEntryCount = 0;
        }while (odtCount != 0x00);
        Xcp_Daq[daqListNo].DynaCfgStatus = XCP_DAQ_CFG_ODT;
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_ODT;
    }
    return;
}

/******************************************************************************/
/*
 * @brief               <Xcp_AllocOdtEntry>
 *
 * <> .
 * Service ID   :       <None>
 * Sync/Async   :       <Synchronous>
 * Reentrancy           <Not Reentrant>
 * @param[in]           <None>
 * @param[out]          <None>
 * @param[in/out]       <None>
 * @return              <None>
 */
/******************************************************************************/
FUNC(void,XCP_CODE)
Xcp_AllocOdtEntry(void)
{

    #if ((STD_OFF == XCP_CAN_MAX_DLC_REQUIRED) && (STD_ON == XCP_ON_CAN_ENABLE))
    if ((Xcp_CmdLength != 6u)
    && (Xcp_CmdLength != XCP_CAN_MAX_DLC))
    {
        Xcp_SetErrorCode(XCP_ERR_CMD_SYNTAX);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check pgm active status */
    #if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
    if (Xcp_PgmStauts != XCP_PGM_IDLE)
    {
        Xcp_SetErrorCode(XCP_ERR_PGM_ACTIVE);
        Xcp_RespLength = 0x02u;
    }
    else
    #endif
    /* check protection */
    if (XCP_PL_DAQ == (XCP_PL_DAQ & Xcp_ProtectionStatus))
    {
        Xcp_SetErrorCode(XCP_ERR_ACCESS_LOCKED);
        Xcp_RespLength = 0x02u;
    }
    /* check daq cfg sequence */
    else if ((XCP_DAQ_CFG_ODT != Xcp_DaqCfgSeqStat)
    && (XCP_DAQ_CFG_ENTRY != Xcp_DaqCfgSeqStat))
    {
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_IDLE;
        Xcp_SetErrorCode(XCP_ERR_SEQUENCE);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_AllocOdtEntryHal();
    }
    Xcp_SendResp();
    return;
}

static FUNC(void,XCP_CODE)
Xcp_AllocOdtEntryHal(void)
{
    uint16 daqListNo = 0;
    uint8 odtNum = Xcp_CmdBuffer[4];
    uint8 entryCount = Xcp_CmdBuffer[5];

    Xcp_CopyU1BufferToU2(&Xcp_CmdBuffer[2], &daqListNo, CPU_BYTE_ORDER);
    if ((daqListNo < XCP_MIN_DAQ) || (daqListNo >= XCP_MAX_DAQ)
    || (XCP_DAQ_CFG_DAQ == Xcp_Daq[daqListNo].DynaCfgStatus)
    || (XCP_DAQ_CFG_FREE == Xcp_Daq[daqListNo].DynaCfgStatus))
    {
        Xcp_SetErrorCode(XCP_ERR_OUT_OF_RANGE);
        Xcp_RespLength = 0x02u;
    }
    else if (XCP_DYNAMIC_DAQ_ODT_ENTRY < Xcp_DynDaqOdtEntryPos)
    {
        Xcp_SetErrorCode(XCP_ERR_MEMORY_OVERFLOW);
        Xcp_RespLength = 0x02u;
    }
    else
    {
        Xcp_Daq[daqListNo].odt[odtNum].XcpOdtEntryCount = entryCount;
        Xcp_Daq[daqListNo].odt[odtNum].XcpOdtEntry = &Xcp_DynDaqBuffer[Xcp_DynDaqOdtEntryPos];
        do
        {
            entryCount--;
            Xcp_DynDaqOdtEntryPos ++;
            Xcp_Daq[daqListNo].odt[odtNum].XcpOdtEntry[entryCount].XcpOdtEntryLength = 0;
        }while (entryCount != 0x00);
        Xcp_Daq[daqListNo].DynaCfgStatus = XCP_DAQ_CFG_ENTRY;
        Xcp_DaqCfgSeqStat = XCP_DAQ_CFG_ENTRY;
        for (odtNum = 0; odtNum < Xcp_Daq[daqListNo].daqListSize; odtNum++)
        {
            if (0x00 == Xcp_Daq[daqListNo].odt[odtNum].XcpOdtEntryCount)
            {
                break;
            }
        }
        /* When all ODTs in the DAQ are assigned ENTRY */
        if (odtNum == Xcp_Daq[daqListNo].daqListSize)
        {
            Xcp_Daq[daqListNo].DynaCfgStatus = XCP_DAQ_CFG_DONE;
        }
    }
    return;
}
#endif /* (XCP_DAQ_STATIC == XCP_DAQ_CONFIG_TYPE ) */
#define XCP_STOP_SEC_CODE
#include "XCP_MemMap.h"

#endif /* (XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE)) */
