/*==========================================================================*/
/*
	SOURCE_FILE:	<CcpMeasuredata.h>
	APPLICATION:	<RCP>
	COMPONENT:		<CCP>
	PACKAGE:		<package name i.e. MN: Menu>
	DATE:			<01-03-2012>
	AUTHOR:			<YunXin.Chen & Guanfeng.Chen >

	Copyright (C) 2009  INFRASTRUCTURE SOFTWARE CO.,LTD.

	All rights are reserved. Reproduction in whole or in part is
	prohibited without the prior written consent of the copyright
	owner. The information presented in this document does not
	form part of any quotation or contract, is believed to be
	accurate and reliable and may be changed without notice.
	No liability will be accepted by the publisher for any
	consequence of its use. Publication thereof does not convey
	nor imply any license under patent- or other industrial or
	intellectual property rights.
*/
/*==========================================================================*/
#ifndef XCP_MEASURE_DATA_H_
#define XCP_MEASURE_DATA_H_

/*==========================================================================*/
/*		I N C L U D E S														*/
/*==========================================================================*/
#include"Platform_Types.h"
#include "Xcp_test.h"

/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S     
	DESCRIPTION:
		<add your measurement variable for application here>
*/
/*==========================================================================*/

extern  MEADATA_SECTION   uint8 u8_daq_test1_0;
extern  MEADATA_SECTION   uint8 u8_daq_test1_1;
extern  MEADATA_SECTION   uint16 u16_daq_test1_2;
extern  MEADATA_SECTION   uint16 u16_daq_test1_3;
extern  MEADATA_SECTION   uint32 u32_daq_test1_4;
extern  MEADATA_SECTION   uint8 u8_daq_test2_0;
extern  MEADATA_SECTION   uint8 u8_daq_test2_1;
extern  MEADATA_SECTION   uint16 u16_daq_test2_2;
extern  MEADATA_SECTION   uint16 u16_daq_test2_3;
extern  MEADATA_SECTION   uint32 u32_daq_test2_4;
#if 1
extern MEADATA_SECTION sint16 Sa1_TDC1_IGN;
extern MEADATA_SECTION uint16 Sa1_nmot;
extern MEADATA_SECTION sint16 Sa1_tmot;
extern MEADATA_SECTION uint8 Sa1_igniteCloseTime;
extern MEADATA_SECTION uint16 ZW_2_OUT;
extern MEADATA_SECTION uint8 g_u1_one;
extern MEADATA_SECTION uint8 g_u1_two;
extern MEADATA_SECTION uint8 g_u1_three;

extern MEADATA_SECTION uint16 g_u2_one;
extern MEADATA_SECTION uint16 g_u2_three;
extern MEADATA_SECTION uint16 g_u2_two;

extern MEADATA_SECTION uint32 g_u4_one;
extern MEADATA_SECTION uint32 g_u4_three;
extern MEADATA_SECTION uint32 g_u4_two;
#endif
/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S   E N D  
	DESCRIPTION:
		<add your measurement variable for application here END>
*/
/*==========================================================================*/


#endif

 /*==========================================================================*/
/*		E N D   O F   F I L E                                               */
/*==========================================================================*/
