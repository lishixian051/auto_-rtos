/*============================================================================*/
/** Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Ccp_Caldata.c>
 *  @brief      <Ccp Module Calibration Data Declearation c file>
 *  
 *  <Ccp Module Calibration Data Declearation c file>
 *  
 *  @author     <Dongyuan.Sun>
 *  @date       <2012-09-05>
 */
/*============================================================================*/


/*=======[I N C L U D E S]====================================================*/
#include"Xcp_Caldata.h"

/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S     
	DESCRIPTION:
		<add your calibration variable for application here>
*/
/*==========================================================================*/

/*************
*    Example
*	uint8 u8_example = 0xff;	
*
*/ 


CALDATA_SECTION  uint8 u8_cal_test1_0 = 0x01;
CALDATA_SECTION  uint8 u8_cal_test1_1 = 0x02;
CALDATA_SECTION  uint16 u16_cal_test1_2 = 0x03;
CALDATA_SECTION  uint16 u16_cal_test1_3 = 0x04;
CALDATA_SECTION  uint32 u32_cal_test1_4 = 0x05;

#if 0
uint16 Sa1_Three_dim_table[216] =
{
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600,
100,200,300,400,500,600,100,200,300,400,500,600
};
uint16 Sa1_Three_dim_axis_1[6] =
{
	1100,1200,1300,1400,1500,1600
};		
 uint16 Sa1_Three_dim_axis_2[6] =
{
	1100,1200,1300,1400,1500,1600
};
 uint16 Sa1_Three_dim_axis_3[6] =
{
	1100,1200,1300,1400,1500,1600
};	
   uint8 Sa2_PIA_BattRatio_Cur_table[18] =
{
   /*[0..9]*/ 76, 76, 80, 84, 96, 108, 108, 120, 140, 144, 
   /*[10..17]*/ 144, 144, 144, 144, 144, 144, 144, 144
   /* 19., 19., 20., 21., 24., 27., 27., 30., 35., 36., 
   36., 36., 36., 36., 36., 36., 36., 36. */
};

   uint8 Sa3_FSZTM_table[8]  =
{
   /*[0..7]*/ 64, 64, 64, 64, 64, 64, 64, 64
   /* 1., 1., 1., 1., 1., 1., 1., 1. */
}; /* LSB: 2^-6 OFF:  0 MIN/MAX:  0 .. 3.984375 */

   uint8 Sa3_TSUB_table[8] =
{
   /*[0..7]*/ 191, 167, 114, 69, 58, 46, 41, 36
   /* 11.9375, 10.4375, 7.125, 4.3125, 3.625, 2.875, 2.5625, 2.25 */
} ;

 sint16 Sa3_FSZTM_axis[8]=
{
   /*[0..7]*/ /* 1, 2, 3, 5, 27, 50, 80, 140*/
    -48, -30, -18, 5, 27, 50, 80, 140 
} ;

 sint16  Sa2_PIA_BattRatio_Cur_axis[18] =
{
   /*[0..9]*/ 500, 600, 700, 800, 1000, 1200, 1600, 2000, 2400, 2800, 
   /*[10..17]*/ 3200, 3600, 4000, 4400, 4800, 5200, 5600, 6000
   /* 500., 600., 700., 800., 1000., 1200., 1600., 2000., 2400., 2800., 
   3200., 3600., 4000., 4400., 4800., 5200., 5600., 6000. */
} ;/*  different constrained ranges */
 uint16 Sa3_TSUB_axis[8]  =
{
   /*[0..7]*/ 6, 8, 10, 12, 13, 14, 15, 16
   /* 6., 8., 10., 12., 13., 14., 15., 16. */
} ;/*  different constrained ranges */

 uint16 Sa1_KFZW_axis_1[12]
	=
	{
	1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200	
	};
 uint16  Sa1_KFZW_axis_2[16]
	=
	{
	1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600	
	};
	
 uint16 Sa1_KFZW_table[192]
	=
	{
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800,
	100,200,300,400,500,600,700,800,100,200,300,400,500,600,700,800
	};
#endif

/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S   E N D  
	DESCRIPTION:
		<add your calibration variable for application here END>
*/
/*==========================================================================*/
//#pragma RENAMECLASS
/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/** <VERSION>  <DATE>  <AUTHOR>     <REVISION LOG>
 *  V1.0    20120703    Guanfeng.Chen       Initial version
 * 
 *  V1.1    20120720    Dongyuan.Sun 
 *          The following changes are made:
 *          1. cut off the buffer used for DAQ queue, modify the structrue of 
 *             DAQ proccessor. use DAQ list queue instead of DAQ frame list. 
 *             copy the data into frame just before frame is to be sent.
 *          2. Add the support the  prescaler function.
 *          3. Add the report of the DAQ overload by Event frame.
 *          4. Add the support of the multiple DAQ use one Eventchannel.
 *          5. Add the check of MTA[1] in MOVE command handler.




 *          6. Make it compatible for both byECU & byTOOL function of INCA
 *          7. Make it compatible for both Motorola or Intel endian of Transport
 *             layer.
 *          8. Move the jump point to Bootloader to Clear_Memory.
 *          9. Add the Slave ID upload function
 * 
 *  V1.2    20120726    Dongyuan.Sun     Use MMU to realize the select page function
 *                                       cut off the cal page buffer.
 *
 *  V1.3    20131204    Li.Zhu           Fix some issue.
 *          1. When receive "Get Seed" and the source is not locked,this API should 
 *          not return the KEY in DTO data.
 *          2. Use the global definition instead the number. "l_pud_MTA[0]" to 
 *          "l_pud_MTA[MAIN_MTA_NUM]" and "l_pud_MTA[0]" to "l_pud_MTA[MOVE_MTA_NUM]".
 *          3. Before use the value in array, it should check the value is available 
 *          or not.
 *          4. The size of DAQ should check the size equal zero.
 *          5. When receive "START_STOP" and "START_STOP_ALL" and the mode is not in 
 *          "DAQ_STOP", "DAQ_START", "DAQ_PREPARE", module should fill "CRC_OUT_OF_RANGE" 
 *          in DTO data.
 *          6. When receive "Move Memory", it should check the address is not NULL.
 *          7. Check the return value to "Ccp_WriteEEPInterface".
 * 
 */
/*=======[E N D   O F   F I L E]==============================================*/
 
