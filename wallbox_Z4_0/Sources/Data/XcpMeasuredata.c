/*==========================================================================*/
/*
	SOURCE_FILE:	<CcpMeasuredata.c>
	APPLICATION:	<RCP>
	COMPONENT:		<CCP>
	PACKAGE:		<package name i.e. MN: Menu>
	DATE:			<01-03-2012>
	AUTHOR:			<YunXin.Chen & Guanfeng.Chen >

	Copyright (C) 2009  INFRASTRUCTURE SOFTWARE CO.,LTD.

	All rights are reserved. Reproduction in whole or in part is
	prohibited without the prior written consent of the copyright
	owner. The information presented in this document does not
	form part of any quotation or contract, is believed to be
	accurate and reliable and may be changed without notice.
	No liability will be accepted by the publisher for any
	consequence of its use. Publication thereof does not convey
	nor imply any license under patent- or other industrial or
	intellectual property rights.
*/
/*==========================================================================*/
/*==========================================================================*/
/*		I N C L U D E S														*/
/*==========================================================================*/
#include"XcpMeasuredata.h"


//#pragma section all "mp_cal"
/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S     
	DESCRIPTION:
		<add your measurement variable for application here>
*/
/*==========================================================================*/
MEADATA_SECTION uint8 u8_daq_test1_0;
MEADATA_SECTION uint8 u8_daq_test1_1;
MEADATA_SECTION uint16 u16_daq_test1_2;
MEADATA_SECTION uint16 u16_daq_test1_3;
MEADATA_SECTION uint32 u32_daq_test1_4;
MEADATA_SECTION uint8 u8_daq_test2_0;
MEADATA_SECTION uint8 u8_daq_test2_1;
MEADATA_SECTION uint16 u16_daq_test2_2;
MEADATA_SECTION uint16 u16_daq_test2_3;
MEADATA_SECTION uint32 u32_daq_test2_4;
#if 1
MEADATA_SECTION sint16 Sa1_TDC1_IGN=0;
MEADATA_SECTION uint16 Sa1_nmot=0;
MEADATA_SECTION sint16 Sa1_tmot=0;
MEADATA_SECTION uint8 Sa1_igniteCloseTime;
MEADATA_SECTION uint16 ZW_2_OUT=0;
MEADATA_SECTION uint8 g_u1_one;
MEADATA_SECTION uint8 g_u1_two;
MEADATA_SECTION uint8 g_u1_three;

MEADATA_SECTION uint16 g_u2_one;
MEADATA_SECTION uint16 g_u2_three;
MEADATA_SECTION uint16 g_u2_two;

MEADATA_SECTION uint32 g_u4_one;
MEADATA_SECTION uint32 g_u4_three;
MEADATA_SECTION uint32 g_u4_two;
#endif

/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S   E N D  
	DESCRIPTION:
		<add your measurement variable for application here END>
*/
/*==========================================================================*/
//#pragma pop

 /*==========================================================================*/
/*		E N D   O F   F I L E                                               */
/*==========================================================================*/

