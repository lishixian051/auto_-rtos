/*============================================================================*/
/** Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CcpCaldata.h>
 *  @brief      <Ccp module Calibration Data H file>
 *  
 *  <Ccp module Calibration Data H file, full function version based on MMU 
 *  of MPC5634. Here user can declare their own Calibration Object Pram.>
 *  
 *  @author     <Dongyuan.Sun>
 *  @date       <26-07-2012>
 */
/*============================================================================*/
#ifndef XCP_CAL_DATA_H
#define XCP_CAL_DATA_H

/*=======[I N C L U D E S]====================================================*/
#include"Std_Types.h"
#include"Platform_Types.h"
#include "Xcp_test.h"
/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S     
	DESCRIPTION:
		<add your calibration variable for application here for extern>
*/
/*==========================================================================*/

/****************************
*	Example:
*	extern uint8 u8_example;
*****************************/
extern  CALDATA_SECTION    uint8 u8_cal_test1_0;
extern  CALDATA_SECTION    uint8 u8_cal_test1_1;
extern  CALDATA_SECTION    uint16 u16_cal_test1_2;
extern  CALDATA_SECTION    uint16 u16_cal_test1_3;
extern  CALDATA_SECTION    uint32 u32_cal_test1_4;
#if 0
extern   uint16  Sa1_Three_dim_table[216];
extern  sint16 Sa3_ant;
extern   uint8 Sa2_PIA_BattRatio_Cur_table[18];
extern   uint8 Sa3_FSZTM_table[8];
extern   uint8 Sa3_TSUB_table[8];
extern  sint16	Sa2_PIA_BattRatio_Cur_axis[18];
extern  sint16	Sa3_FSZTM_axis[8];
extern  uint16	Sa3_TSUB_axis[8];


extern  uint16  Sa1_Three_dim_axis_1[6];
extern  uint16  Sa1_Three_dim_axis_2[6] ;
extern  uint16  Sa1_Three_dim_axis_3[6];
extern  uint16 Sa1_KFZW_axis_1[12];
extern  uint16  Sa1_KFZW_axis_2[16] ;
extern  uint16 Sa1_KFZW_table[192] ;
#endif

/*==========================================================================*/
/*	
	G L O B A L   D A T A   D E F I N I T I O N S   E N D  
	DESCRIPTION:
		<add your calibration variable for application here for extern END>
*/
/*==========================================================================*/

#endif

/*=======[R E V I S I O N   H I S T O R Y]====================================*/
/** <VERSION>  <DATE>  <AUTHOR>     <REVISION LOG>
 *  V1.0    20120703    Guanfeng.Chen       Initial version
 * 
 *  V1.1    20120720    Dongyuan.Sun 
 *          The following changes are made:
 *          1. cut off the buffer used for DAQ queue, modify the structrue of 
 *             DAQ proccessor. use DAQ list queue instead of DAQ frame list. 
 *             copy the data into frame just before frame is to be sent.
 *          2. Add the support the  prescaler function.
 *          3. Add the report of the DAQ overload by Event frame.
 *          4. Add the support of the multiple DAQ use one Eventchannel.
 *          5. Add the check of MTA[1] in MOVE command handler.
 *          6. Make it compatible for both byECU & byTOOL function of INCA
 *          7. Make it compatible for both Motorola or Intel endian of Transport
 *             layer.
 *          8. Move the jump point to Bootloader to Clear_Memory.
 *          9. Add the Slave ID upload function
 * 
 *  V1.2    20120726    Dongyuan.Sun     Use MMU to realize the select page function
 *                                       cut off the cal page buffer.
 *
 *  V1.3    20131204    Li.Zhu           Fix some issue.
 *          1. When receive "Get Seed" and the source is not locked,this API should 
 *          not return the KEY in DTO data.
 *          2. Use the global definition instead the number. "l_pud_MTA[0]" to 
 *          "l_pud_MTA[MAIN_MTA_NUM]" and "l_pud_MTA[0]" to "l_pud_MTA[MOVE_MTA_NUM]".
 *          3. Before use the value in array, it should check the value is available 
 *          or not.
 *          4. The size of DAQ should check the size equal zero.
 *          5. When receive "START_STOP" and "START_STOP_ALL" and the mode is not in 
 *          "DAQ_STOP", "DAQ_START", "DAQ_PREPARE", module should fill "CRC_OUT_OF_RANGE" 
 *          in DTO data.
 *          6. When receive "Move Memory", it should check the address is not NULL.
 *          7. Check the return value to "Ccp_WriteEEPInterface".
 * 
 */
/*=======[E N D   O F   F I L E]==============================================*/

