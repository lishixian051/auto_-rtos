#include "Platform_Types.h"
#include "XcpMeasuredata.h"
#include "Xcp_Caldata.h"
#include "Xcp_Cal.h"

void DaqChangeTest(void)
{	
	#if 1
	uint8 table_count = 0;
	uint8 Test_Count1 = 0;
	uint16 ccc = 0;
	uint32 usedfor0 = 0;
	uint32 usedfor1 = 0;
	//usedfor = Sa1_Three_dim_axis_1[0] + Sa1_Three_dim_axis_2[1] + Sa1_Three_dim_axis_3[2];
	//usedfor += (Sa3_FSZTM_axis[0]+Sa1_KFZW_axis_1[0]+Sa1_KFZW_axis_2[0]+Sa3_TSUB_axis[0]);

	#endif
	u8_daq_test1_0 = u8_cal_test1_0;
	u8_daq_test1_1 = u8_cal_test1_1;
	u16_daq_test1_2 = u16_cal_test1_2;
	u16_daq_test1_3 = u16_cal_test1_3;
	u32_daq_test1_4 = u32_cal_test1_4;

	u8_daq_test2_0 = u8_cal_test2_0 ;
	u8_daq_test2_1 = u8_cal_test2_1;
	u16_daq_test2_2 = u16_cal_test2_2;
	u16_daq_test2_3 = u16_cal_test2_3;
	u32_daq_test2_4 = u32_cal_test2_4;

	#if 0
    for(table_count=0;table_count<216;table_count++)
	{
		ccc=ccc+Sa1_Three_dim_table[table_count] + Sa1_KFZW_table[table_count];
		ZW_2_OUT=ccc ;
	}

	ccc=0;
	for(Test_Count1=0;Test_Count1<18;Test_Count1++)
	{
		ccc=ccc+Sa2_PIA_BattRatio_Cur_table[Test_Count1];
		Sa1_TDC1_IGN=ccc;
	}
	ccc=0;
	for(Test_Count1=0;Test_Count1<8;Test_Count1++)
	{
		ccc=ccc+Sa3_FSZTM_table[Test_Count1]+Sa3_TSUB_table[Test_Count1];
		Sa1_tmot=ccc;
	}
	ccc=0;
	for(Test_Count1=0;Test_Count1<192;Test_Count1++)
	{
		ccc= ccc+ Sa1_KFZW_table[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_Three_dim_axis_1[Test_Count1];
		Sa1_nmot= ccc;
	}

	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_KFZW_axis_2[Test_Count1];
		Sa1_nmot= ccc;
	}

	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa2_PIA_BattRatio_Cur_axis[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa3_TSUB_axis[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_KFZW_axis_1[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa3_FSZTM_axis[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa2_PIA_BattRatio_Cur_table[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_Three_dim_axis_3[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_Three_dim_axis_2[Test_Count1];
		Sa1_nmot= ccc;
	}

	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_Three_table[Test_Count1];
		Sa1_nmot= ccc;
	}
	for(Test_Count1=0;Test_Count1<6;Test_Count1++)
	{
		ccc= ccc+ Sa1_KFZW_test_table[Test_Count1];
		Sa1_nmot= ccc;
	}
#endif

#if 1
    {
        g_u1_one =  (u8_cal_test1_0 + 1);
        g_u1_two = (u8_cal_test1_0 + 1);
        g_u1_three = (u8_cal_test1_0 + 1);

        g_u2_one =  (u8_cal_test1_0 + 2);
        g_u2_three = (u8_cal_test1_0 + 2);
        g_u2_two = (u8_cal_test1_0 + 2);

        g_u4_one =  (u8_cal_test1_0 + 4);
        g_u4_three = (u8_cal_test1_0 + 4);
        g_u4_two = (u8_cal_test1_0 + 4);
    }

#endif
}

#if 0
#include "Can.h"
//#include "Fls.h"
#if 0
typedef struct
{
    P2VAR(uint8, AUTOMATIC, CAN_APPL_DATA) sdu;  /* Pointer to L-PDU */
    Can_IdType id;                               /* CANID */
    PduIdType  swPduHandle;                      /* Handle */
    uint8      length;                           /* DLC */
}Can_PduType;
#endif

uint8 pduData[8] = {88,24,32,48,59,65,73,81};
uint8 pduTestData[8] = {0x88,0x24,0x32,0x48,0x59,0x65,0x73,0x81};
uint8 flsData[1024] = {0};
void Ccp_TestCanDriver(uint8 nIndex)
{
    Can_PduType pduInfo;
    pduInfo.id = 0x7b0;
    pduInfo.length = 8;
    if(nIndex == 0)
        pduInfo.sdu = pduData;
    else
        pduInfo.sdu = pduTestData;
    pduInfo.swPduHandle = 0;
    Can_Write(1, &pduInfo);
}

void Ccp_TestFlsDriver(void)
{
    ;
}
#endif
