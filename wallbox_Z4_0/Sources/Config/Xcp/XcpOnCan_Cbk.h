/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <XcpOnCan_Cbk.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-29 15:36:35>
 */
/*============================================================================*/

#ifndef XCPONCAN_CBK_H
#define XCPONCAN_CBK_H

/*=======[I N C L U D E S]====================================================*/
#include "ComStack_Types.h"
#include "Xcp_GenericTypes.h"
#include "Xcp_Cfg.h"


/*=======[E X T E R N A L   F U N C T I O N   D E C L A R A T I O N S]========*/
#define XCP_START_SEC_CANIFTXINDICATION_CODE
#include "XCP_MemMap.h"

extern FUNC(void, XCP_CANIFTXINDICATION_CODE)
	   Xcp_CanIfTxConfirmation(
			PduIdType XcpTxPduId
		);

#define XCP_STOP_SEC_CANIFTXINDICATION_CODE
#include "XCP_MemMap.h"

#define XCP_START_SEC_CANIFRXINDICATION_CODE
#include "XCP_MemMap.h"

extern FUNC(void, XCP_CANIFRXINDICATION_CODE)
	   Xcp_CanIfRxIndication(
			PduIdType XcpRxPduId, 
			P2CONST(PduInfoType,AUTOMATIC,XCP_APPL_DATA) XcpRxPduPtr
		);
	
#define XCP_STOP_SEC_CANIFRXINDICATION_CODE
#include "XCP_MemMap.h"
	
#define XCP_START_SEC_CANIFTRIGGERTRANSMIT_CODE
#include "XCP_MemMap.h"

extern FUNC(Std_ReturnType , XCP_CANIFTRIGGERTRANSMIT_CODE)
	   Xcp_CanIfTriggerTransmit(
			PduIdType TxPduId, 
			P2VAR(PduInfoType,AUTOMATIC,XCP_APPL_DATA) PduInfoPtr
		);
	
#define XCP_STOP_SEC_CANIFTRIGGERTRANSMIT_CODE
#include "XCP_MemMap.h"	
#endif
