/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Xcp_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-29 15:36:35>
 */
/*============================================================================*/

#include "Xcp.h"

/*
 *  GetId CMD Config
 */
#if XCP_GET_ID == STD_ON
static CONST(uint8, XCP_CONST_PBCFG) IdInfo1[MAX_ID_LENGTH] = {"test"};


static CONST(Xcp_IdInfoType, XCP_CONST_PBCFG) Xcp_IdInfo[XCP_NUM_OF_ID] =
{
	{
		1, 0, sizeof(IdInfo1), &IdInfo1[0]
	}
};
#endif

#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))

static CONST(XcpDtoType, XCP_CONST_PBCFG) Xcp_Dto[XCP_MAX_DAQ] =
{
	{
		#if (XCP_IDENTIFICATION_FIELD_TYPE == XCP_PID_ABSOLUTE)
		0,/*XcpFirstPid*/
		#else
		0,
		#endif
		&Xcp_Daq_Pdu[0] /*XcpDto2PduMapping*/	
	}
};
																	
static CONST(Xcp_DaqListConfigType, XCP_CONST_PBCFG) Xcp_DaqConfig[XCP_MAX_DAQ] =
{
	{
		0,/*XcpDaqListNumber*/
		DAQ, /* XcpDaqListtype */
        0,                          /* XcpDaqSize */
        &Xcp_Dto[0], 	
        NULL_PTR	     										
	}																
};

static VAR(Xcp_AGType, XCP_VAR) Xcp_EvCh0Buffer[XCP_EVCHBUFFER_0_DEPTH]; 

static CONST(Xcp_DaqNumType, XCP_CONST_PBCFG) Xcp_EvCh0DaqList[1] = {0 };

static CONST(uint8, XCP_CONST_PBCFG) Xcp_Evch_0[MAX_EVCH_NAME_LENGTH] = { "EventChannel_0" };
																								
static CONST(Xcp_EvChConfigType, XCP_CONST_PBCFG) Xcp_EvChConfig[XCP_MAX_EVENT_CHANNEL] =
{
	{
		Xcp_Evch_0,
  		sizeof(Xcp_Evch_0),
		XCP_EVENT_CONSIST_DAQ,	
  		1, /*XcpEvChMaxDaqList*/
		0,/*XcpEvChNumber*/	
  		1,/*XcpEvChPriority*/				
  		1,/*XcpEvChTimeCycle*/				
  		TIMESTAMP_UNIT_10MS,/*XcpEvChTimeUnit*/		
  		DAQ,/*XcpEvChType*/		
        XCP_EVCHBUFFER_0_DEPTH,
        &Xcp_EvCh0Buffer[0],
        &Xcp_EvCh0DaqList[0]			
	}
};

#endif /* (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE)) */


#if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))

#if ((XCP_PAG_SUPPORT == STD_ON) \
&& (STD_ON == XCP_SWITCH_PAG_SUPPORT)\
&& (STD_OFF == XCP_MMU_SUPPORT)\
&& (STD_OFF == XCP_OVC_SUPPORT))
static VAR(uint8, XCP_VAR) Xcp_PageBuffer0[XCP_PAGE_BUF0_SIZE]; 
#endif

static CONST(Xcp_PageInfoType, XCP_CONST_PBCFG) Xcp_PageInfo[XCP_MAX_SEGMENT] = 
{
    {	
    	0x400,
        0x4003fc00,
        0x4003ffff,
        0xfa8000,
        0xfa83ff,
        #if ((STD_ON == XCP_PAG_SUPPORT) \
		&& (STD_ON == XCP_SWITCH_PAG_SUPPORT)\
        && (STD_OFF == XCP_MMU_SUPPORT)\
        && (STD_OFF == XCP_OVC_SUPPORT))
        (uint32)Xcp_PageBuffer0,
		#endif 
	}    
};

CONST(Xcp_SegmentInfoType,XCP_CONST_PBCFG) Xcp_SegmentInfo = 
{
	XCP_MAX_SEGMENT,
    0x0,
    0x0,
    0x40000000,
    0x4003fbff,
    Xcp_PageInfo, 
};
#endif
#if (XCP_PL_PGM == (XCP_PL_PGM&XCP_RESOURCE))

CONST(Xcp_SectorInfoType, XCP_CONST_PBCFG) Xcp_SectorInfo[XCP_MAX_SECTOR] = 
{    
};
#endif

CONST(Xcp_ConfigType, XCP_CONST_PBCFG) XcpConfig =
{
	#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
    &Xcp_DaqConfig[0],
    &Xcp_EvChConfig[0],
	#endif
	#if (STD_ON == XCP_GET_ID)
	XCP_NUM_OF_ID,
	&Xcp_IdInfo[0]
	#endif
};

