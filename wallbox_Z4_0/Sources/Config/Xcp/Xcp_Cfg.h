/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Xcp_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-29 15:36:34>
 */
/*============================================================================*/

#ifndef  XCP_CFG_H
#define  XCP_CFG_H
/* Bus Interface Select */
#define XCP_ON_CCD_ENABLE           STD_OFF		/* XcpOnCddEnabled */
#define XCP_ON_ETHERNET_ENABLE      STD_OFF		/* XcpOnEthernetEnabled */
#define XCP_ON_FLEXRAY_ENABLE       STD_OFF		/* XcpOnFlexRayEnabled */
#define XCP_ON_CAN_ENABLE           STD_ON		/* XcpOnCanEnabled */
#include "Xcp_GenericTypes.h"

/* Resource & Protection Enable */ 
#define XCP_RESOURCE                (0|XCP_PL_DAQ|XCP_PL_CAL|XCP_PL_PGM)
#define XCP_PROTECTION              (0|XCP_PL_DAQ|XCP_PL_CAL|XCP_PL_PGM)

#define XCP_OS_COUNTER_ID 				0	/* XcpCounterRef */

/* Transport Layer Spec. */
/* check transmit Timeout in Xcp_mainfunction() */
#define XCP_BUS_TX_POLLING_MODE     STD_OFF

#define XCP_TANSFER_TIME_OUT        3u /* advance option default is 3 */

/* GetId CMD name length */
#define MAX_ID_LENGTH 10U

/* Optional Feature Enable */
/* now the following feature are all not supportted */
#define XCP_CAL_STORE_SUPPORT       STD_OFF
#define XCP_DAQ_STORE_SUPPORT       STD_OFF
#define XCP_RESUME_SUPPORT          STD_OFF
#define XCP_BIT_STIM_SUPPORT        STD_OFF

/* Max DTO & CTO */
#define XCP_MAX_CTO	                8u /* XcpMaxCto */
#if (XCP_PL_PGM == (XCP_RESOURCE&XCP_PL_PGM))
#define XCP_MAX_CTO_PGM             8u
#endif /* (XCP_PL_PGM == (XCP_RESOURCE&XCP_PL_PGM)) */
#define XCP_MAX_DTO                 8u /* XcpMaxDto */

#define XCP_CTO_BUFFER_SIZE         8u  /* max(XCP_MAX_CTO, XCP_MAX_CTO_PGM)*/
#define XCP_DTO_BUFFER_SIZE         XCP_MAX_DTO  /* XCP_MAX_DTO */

#define XCP_MAIN_FUNCTION_PERIOD    10u	/* XcpMainFunctionPeriod */

/* Optional API */
#define XCP_DEV_ERROR_DETECT        STD_OFF  /* XcpDevErrorDetect */
#define XCP_VERSION_INFO_API        STD_OFF 	/* XcpVersionInfoApi */
#define XCP_SUPPRESS_TX_SUPPORTED   STD_OFF 	/* XcpSuppressTxSupport */



/* DAQ Format */
#if (XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE))

#define XCP_DAQ_CONFIG_TYPE             XCP_DAQ_DYNAMIC /* XcpDaqConfigType */
#define XCP_IDENTIFICATION_FIELD_TYPE   XCP_PID_ABSOLUTE /* XcpIdentificationFieldType */
#define XCP_PID_OFF_SUPPORT             STD_OFF /* pid off support */
#define XCP_PRESCALER_SUPPORTED		    STD_OFF  /* XcpPrescalerSupported */
#define XCP_DAQ_PRIORITY_SUPPORT        STD_ON	/* daq priority support */
#define XCP_DAQ_OVL_INDICATION    		XCP_DAQ_OVL_NO_INDIC /* daq overload indication */
/* pid overload flow indication flag */
#define XCP_PID_OVFLOW 					0X80

/* the Max length for Write Daq */
#if (XCP_PID_ABSOLUTE == XCP_IDENTIFICATION_FIELD_TYPE)
#define XCP_MAX_WRITEDAQ_SIZE	 7u /* XCP_MAX_DTO-1 */
#elif (XCP_PID_RELATIVE_BYTE == XCP_IDENTIFICATION_FIELD_TYPE)
#define XCP_MAX_WRITEDAQ_SIZE	 6u /* XCP_MAX_DTO-2 */
#elif (XCP_PID_RELATIVE_WORD == XCP_IDENTIFICATION_FIELD_TYPE)
#define XCP_MAX_WRITEDAQ_SIZE	 5u /* XCP_MAX_DTO-3 */
#elif (XCP_PID_RELATIVE_WORD_ALIGNED == XCP_IDENTIFICATION_FIELD_TYPE)
#define XCP_MAX_WRITEDAQ_SIZE	 4u /* XCP_MAX_DTO-4 */
#endif

#if (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE)
#define XCP_DAQ_COUNT               1u /* XcpDaqCount */
#define XCP_ODT_COUNT               8u /* XcpOdtCount */
#define XCP_ODTENTRY_COUNT          XCP_MAX_WRITEDAQ_SIZE /* XcpOdtEntriesCount */
#define XCP_DYNAMIC_DAQ_BUFFER_SIZE	1024u /* size of Dynamic Daq buffer */

#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC) */



/* DAQ Num limitation */
#define XCP_MIN_DAQ					0u	/* XcpMinDaq */
#if (XCP_DAQ_DYNAMIC == XCP_DAQ_CONFIG_TYPE)
#define XCP_MAX_DAQ                 1u/*(XCP_MIN_DAQ + XCP_DAQ_COUNT)*/
#else /* XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC */
#define XCP_MAX_DAQ                 1u
#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_DYNAMIC) */

#define XCP_ODT_ENTRY_SIZE_STIM     0u /* XcpOdtEntrySizeStim */
#define XCP_ODT_ENTRY_SIZE_DAQ      6u/* XcpOdtEntrySizeDaq */

/* Event Channel */
#define EventChannel_0       0u
#define MAX_EVCH_NAME_LENGTH		32u /* the length of Eventchannel name */
#define XCP_MAX_EVENT_CHANNEL       1u /* XcpMaxEventChannel */

/* The EVCHBUFFER_number is related to XCP_MAX_EVENT_CHANNEL */
/* the size is related to XcpEvChConsistency */
/*
 * ODT 	 Consistency:1
 * DAQ   Consistency:20  ---(buffersize = daqlistsize * XCP_MAX_WRITEDAQ_SIZE)
 * EVENT Consistency:50	 ---(buffersize = max_daq * daqlistsize * XCP_MAX_WRITEDAQ_SIZE)
 * */ 							 /* Size */
#define XCP_EVCHBUFFER_0_DEPTH       64u

#endif /* (XCP_PL_DAQ == (XCP_PL_DAQ&XCP_RESOURCE)) */

/* TimeStamp */
#define XCP_TIMESTAMP_TYPE          XCP_TS_NO_TS  		/* XcpTimestampType */
#if (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS)
#define XCP_TIMESTAMP_FIXED          STD_OFF
#define XCP_TIMESTAMP_TICKS         0u				/* XcpTimestampTicks */
#define XCP_TIMESTAMP_UNIT          TIMESTAMP_UNIT_100MS 	/* XcpTimestampUnit */
#if (XCP_TS_ONE_BYTE == XCP_TIMESTAMP_TYPE)
#define Xcp_TSType	 uint8
#elif (XCP_TS_TWO_BYTE == XCP_TIMESTAMP_TYPE)
#define Xcp_TSType	 uint16
#elif (XCP_TS_FOUR_BYTE == XCP_TIMESTAMP_TYPE)
#define Xcp_TSType	 uint32
#endif
#endif /* (XCP_TIMESTAMP_TYPE != XCP_TS_NO_TS) */

/* Interleaved Mode */
#define XCP_INTERLEAVED_MODE        STD_OFF /* Not Supported by CAN */
#if (XCP_INTERLEAVED_MODE == STD_ON)
#define XCP_QUEUE_SIZE              8u
    #if (XCP_PL_PGM == (XCP_RESOURCE&XCP_PL_PGM))
    #define XCP_QUEUE_SIZE_PGM          8u
    #endif /* (XCP_PL_PGM == (XCP_RESOURCE&XCP_PL_PGM)) */
#endif /* (XCP_INTERLEAVED_MODE == STD_ON) */

/* Blcok Mode */
#define XCP_SLAVE_BLOCK_MODE         STD_ON
#define XCP_MASTER_BLOCK_MODE        STD_ON

#if (STD_ON == XCP_MASTER_BLOCK_MODE)
#define XCP_MAX_BS      8u /*  the maximum allowed block size as the number of consecutive command packets  */
#define XCP_MIN_ST      10u          /* Uint 100us */
    #if (XCP_PL_PGM == (XCP_RESOURCE&XCP_PL_PGM))
    #define XCP_MAX_BS_PGM  22u
    #define XCP_MIN_ST_PGM  10u          /* Uint 100us */
    #endif  /* XCP_PL_PGM enable */
#define XCP_BLOCK_BUFFER_SIZE       132u /* max(XCP_MAX_BS,XCP_MAX_BS_PGM)*((XCP_CTO_BUFFER_SIZE - 2)/AG) */
#endif  /* XCP_MASTER_BLOCK_MODE == STD_ON */


#if (STD_ON == XCP_ON_CAN_ENABLE)
#if (STD_ON == XCP_MASTER_BLOCK_MODE)
#define XCP_INCREASED_CAN_ID            STD_OFF
#endif
#define XCP_CAN_MAX_DLC_REQUIRED        STD_ON /* whether need MAX_DLC */
#define XCP_CAN_MAX_DLC                 0x08u   /* fixed value on CAN */
#endif /* (XCP_ON_CAN_ENABLE == STD_ON) */

/* 
 * CAL Related
 */

#if (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE))
/* PAG Related */
#define XCP_MAX_SEGMENT             1u
#define XCP_PAG_SUPPORT             STD_ON

    #if (STD_ON == XCP_PAG_SUPPORT)
    /* Num of Page define */
    #define XCP_RAM_PAGE_NUM            0u
    #define XCP_FLASH_PAGE_NUM          1u
    #define XCP_MMU_SUPPORT             STD_OFF

    #if (XCP_MMU_SUPPORT == STD_ON)
	/* To do MMU */
    #endif
    #endif /* (STD_ON == XCP_PAG_SUPPORT) */

#if ((STD_ON == XCP_SWITCH_PAG_SUPPORT) \ 
&& (STD_ON == XCP_OVC_SUPPORT))

#endif  /* (STD_ON == XCP_OVC_SUPPORT) */

							/* size of alloc arry for page switch */
#define XCP_PAGE_BUF0_SIZE 0x400u

#endif /* (XCP_PL_CAL == (XCP_PL_CAL&XCP_RESOURCE)) */

/* 
 * PGM Related
 */
#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))

/* fill the gaps of flash */
#define XCP_PGM_FILLER      0xffu  /* fixed*/

/* falsh retry number */
#define XCP_PGM_REQRETRIE   0x03u  /* fixed */

/* Xcp Total Sector used in GET_PGM_PROCESSOR_INFO & GET_SECTOR_INFO command */
#define XCP_MAX_SECTOR      0x1u  /* fixed */

#define XCP_CLEAR_AND_PROGRAM_SEQUENCE_CHECK  STD_OFF  

#define MAX_SECTOR_NAME_LENGTH		32u

#endif /* (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE)) */

/* 
 * Optional Command Enable 
 */
/* STD */
#define XCP_GET_COMM_MODE_INFO              STD_ON    /*  */
#define XCP_GET_ID                          STD_OFF    /*  */
#if (XCP_GET_ID == STD_ON)
#define XCP_NUM_OF_ID                       0x1u
#endif /* (XCP_GET_ID == STD_ON) */
#define XCP_SET_REQUEST			            STD_ON   
#define XCP_SEED_AND_UNLOCK                 STD_ON    /* get seed&unlock Command shall be enable in pairs */
#define XCP_SET_MTA		                    STD_ON    /* shall be supported if upload/download ect. is enabled */
#define XCP_UPLOAD                          STD_ON    /*  */
#define XCP_UPLOAD_FLOAT_DATA_SUPPORT    STD_ON    /*  */
	
#define XCP_SHORT_UPLOAD                    STD_ON    /*  */
#define XCP_BUILD_CHECKSUM                  STD_ON    /*  */
#if (XCP_BUILD_CHECKSUM == STD_ON)
#define XCP_MAX_CHECKSUM_SIZE               0x100u
#endif /* (XCP_BUILD_CHECKSUM == STD_ON) */
#define XCP_TRANSPORT_LAYER_CMD             STD_ON    /*  */
#if (XCP_ON_CAN_ENABLE == STD_ON)
#define	XCP_GET_SLAVE_ID                    STD_OFF    /* Required by AUTOSAR */
#define	XCP_GET_DAQ_ID                      STD_OFF   /*  */
#define	XCP_SET_DAQ_ID                      STD_OFF   /* No suitable API provided by AUTOSAR */
#endif /* (XCP_ON_CAN_ENABLE == STD_ON) */
#define XCP_USER_CMD                        STD_OFF   /* User defined Command temp. out of scope */

/* CAL */
#if (XCP_PL_CAL == (XCP_PL_CAL & XCP_RESOURCE))
#define XCP_DOWNLOAD_NEXT		            STD_ON    /* if MASTER_BLOCK is enable this command shall be supported */
#define XCP_DOWNLOAD_MAX                    STD_ON	
#define XCP_SHORT_DOWNLOAD	                STD_ON   /* Not Supported on CAN */
#define XCP_MODIFY_BITS                     STD_OFF	
/* PAG */
#if (XCP_PAG_SUPPORT == STD_ON)
#define XCP_SET_CAL_PAGE                    XCP_PAG_SUPPORT  
#define XCP_GET_CAL_PAGE                    XCP_PAG_SUPPORT  
#define XCP_GET_PAG_PROCESSOR_INFO          STD_OFF
#define XCP_GET_SEGMENT_INFO                STD_OFF
#define XCP_GET_PAGE_INFO                   STD_OFF
#define XCP_SET_SEGMENT_MODE	            STD_OFF
#define XCP_GET_SEGMENT_MODE                STD_OFF
#define XCP_COPY_CAL_PAGE                   STD_OFF
#endif /* (XCP_PAG_SUPPORT == STD_ON) */
#endif /* (XCP_PL_CAL == (XCP_PL_CAL & XCP_RESOURCE)) */

#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
#define XCP_WRITE_DAQ_MULTIPLE              STD_OFF   /* Not Supported on CAN */
#define XCP_READ_DAQ                        STD_ON
#define XCP_GET_DAQ_CLOCK                   STD_OFF   /* if no timestamp this command shall be disable */
#define XCP_GET_DAQ_PROCESSOR_INFO          STD_ON  		
#define XCP_GET_DAQ_RESOLUTION_INFO         STD_ON			
#define XCP_GET_DAQ_LIST_MODE               STD_ON			
#define XCP_GET_DAQ_EVENT_INFO              STD_ON			
#if (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC)
#define XCP_GET_DAQ_LIST_INFO               STD_ON			
#endif /* (XCP_DAQ_CONFIG_TYPE == XCP_DAQ_STATIC) */

#endif  /* end of  (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE)) */

#if (XCP_PL_PGM == (XCP_PL_PGM & XCP_RESOURCE))
#define XCP_PROGRAM_NEXT                    STD_OFF		
#define XCP_GET_SECTOR_INFO                 STD_OFF		
#define XCP_GET_PGM_PROCESSOR_INFO          STD_ON
#define XCP_PROGRAM_PREPARE                 STD_OFF   /* Not supported yet */
#define XCP_PROGRAM_FORMAT                  STD_ON    /* for now only access mode can be changed */
#define XCP_PROGRAM_MAX                     STD_OFF		
#define XCP_PROGRAM_VERIFY                  STD_OFF   /* Not supported yet */
#endif
/*
 * End of Optional Command
 */
#define XCP_STORE_CAL          STD_OFF
#if (XCP_MAX_DAQ < 0xFF)
typedef VAR(uint8,TYPEDEF)     Xcp_DaqNumType;
#else
typedef VAR(uint16,TYPEDEF)    Xcp_DaqNumType;
#endif

#define XCP_ADDR_GRANULARITY       XCP_AG_BYTE /* address granularity */

#if (XCP_ADDR_GRANULARITY == XCP_AG_BYTE)
#define XCP_AG                      (0x01u)
#elif (XCP_ADDR_GRANULARITY == XCP_AG_WORD)
#define XCP_AG                      (0x02u)
#elif (XCP_ADD_GRANULARITY == XCP_AG_DWORD)
#define XCP_AG                      (0x04u)

#endif

/* typedef according to the [AG] */
#if (XCP_ADDR_GRANULARITY==XCP_AG_BYTE)
typedef VAR(uint8,TYPEDEF)   Xcp_AGType;
#elif (XCP_ADDR_GRANULARITY==XCP_AG_WORD)
typedef VAR(uint16,TYPEDEF)  Xcp_AGType;
#elif (XCP_ADDR_GRANULARITY==XCP_AG_DWORD)
typedef VAR(uint32,TYPEDEF)  Xcp_AGType;
#endif

/*symbols generated for RXPDU*/
#define XCP_RX_PDU_NUMBER_MAX                                         1u
#define XCP_XCP_CanIf_7A0_Rx_CMD                                                  0u

/*symbols generated for TXPDU*/
#define XCP_TX_PDU_NUMBER_MAX                                         2u
#define XCP_XCP_CanIf_7B0_Tx_RSP                                                  0u
#define XCP_XCP_CanIf_7B1_Tx_DAQ                                                  1u

#endif  /* endof XCP_CFG_H */

/*=======[E N D   O F   F I L E]==============================================*/
