/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Xcp_PBcfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-29 15:36:35>
 */
/*============================================================================*/

#include "Xcp.h"
#include "Xcp_Cfg.h"
#include "CanIf_Cfg.h"

/*
 *  Tx & Rx Pdu Config
 */

const Xcp_TxPduType Xcp_TxPdu[XCP_TX_PDU_NUMBER_MAX] =
{
	{
  		XCP_XCP_CanIf_7B0_Tx_RSP,	/* XcpLocalTxPduId */
		CANIF_TXPDU_XCP_CanIf_7B0_Tx_RSP				/* LowLayerTxPduId */
	},
	{
  		XCP_XCP_CanIf_7B1_Tx_DAQ,	/* XcpLocalTxPduId */
		CANIF_TXPDU_XCP_CanIf_7B1_Tx_DAQ				/* LowLayerTxPduId */
	}
};

const Xcp_RxPduType Xcp_RxPdu[XCP_RX_PDU_NUMBER_MAX] =
{
	{
  		XCP_XCP_CanIf_7A0_Rx_CMD	/* XcpLocalRxPduId */
	}
};

const Xcp_PduType Xcp_Pdu =
{

		&Xcp_RxPdu[0],
		&Xcp_TxPdu[0]
};
#if (XCP_PL_DAQ == (XCP_PL_DAQ & XCP_RESOURCE))
const Xcp_PduType Xcp_Daq_Pdu[XCP_MAX_DAQ] =
{
	
	{
		NULL_PTR,
		&Xcp_TxPdu[0]
	}
};
#endif
const Xcp_PbConfigType Xcp_PBConfig =
{
	XCP_RX_PDU_NUMBER_MAX,
	XCP_TX_PDU_NUMBER_MAX,
	&Xcp_Pdu
};
