/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanTp_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-28 17:21:55>
 */
/*============================================================================*/


/*=====[R E V I S I O N   H I S T O R Y]====================*/

/*====================================================*/
/*=======[V E R S I O N  I N F O R M A T I O N]===============*/
#define CANTP_CFG_C_AR_MAJOR_VERSION   4
#define CANTP_CFG_C_AR_MINOR_VERSION   2
#define CANTP_CFG_C_AR_PATCH_VERSION   2
#define CANTP_CFG_C_SW_MAJOR_VERSION   1
#define CANTP_CFG_C_SW_MINOR_VERSION   0
#define CANTP_CFG_C_SW_PATCH_VERSION   0

/*=======[I N C L U D E S]================================*/
#include "CanTp_Cfg.h"
#include "CanTp_Types.h"
/*=======[V E R S I O N  C H E C K]============================*/
/*check version information with CanTp_Types.h*/
#if(CANTP_CFG_C_AR_MAJOR_VERSION != CANTP_TYPES_H_AR_MAJOR_VERSION)
	#error "CanTp_Cfg.c : Mismatch in Specification Major Version with CanTp_Types.h"
#endif
#if(CANTP_CFG_C_AR_MINOR_VERSION != CANTP_TYPES_H_AR_MINOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Specification Minor Version with CanTp_Types.h"
#endif
#if(CANTP_CFG_C_AR_PATCH_VERSION != CANTP_TYPES_H_AR_PATCH_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Specification Patch Version with CanTp_Types.h"
#endif
#if(CANTP_CFG_C_SW_MAJOR_VERSION != CANTP_TYPES_H_SW_MAJOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Software Implementation Major Version with CanTp_Types.h"
#endif
#if(CANTP_CFG_C_SW_MINOR_VERSION != CANTP_TYPES_H_SW_MINOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Software Implementation Major Version with CanTp_Types.h"
#endif
/*check version information with CanTp_Cfg.h*/
#if(CANTP_CFG_C_AR_MAJOR_VERSION != CANTP_CFG_H_AR_MAJOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Specification Major Version with CanTp_Cfg.h"
#endif
#if(CANTP_CFG_C_AR_MINOR_VERSION != CANTP_CFG_H_AR_MINOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Specification Minor Version with CanTp_Cfg.h"
#endif
#if(CANTP_CFG_C_AR_PATCH_VERSION != CANTP_CFG_H_AR_PATCH_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Specification Patch Version with CanTp_Cfg.h"
#endif
#if(CANTP_CFG_C_SW_MAJOR_VERSION != CANTP_CFG_H_SW_MAJOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Software Implementation Major Version with CanTp_Cfg.h"
#endif
#if (CANTP_CFG_C_SW_MINOR_VERSION != CANTP_CFG_H_SW_MINOR_VERSION)
  #error "CanTp_Cfg.c : Mismatch in Software Implementation Major Version with CanTp_Cfg.h"
#endif
/*=======[I N T E R N A L   D A T A]=======================*/
#define CANTP_START_SEC_CONST_UNSPECIFIED
#include "Cantp_MemMap.h"

/*CanTpChannel RxSdu address information*/
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_Bs = 0u;
static CONST(uint32, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_Ncr = 150u;
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_RxWftMax = 255u;
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_STmin = 10u;
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_Bs = 0u;
static CONST(uint32, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_Ncr = 150u;
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_RxWftMax = 255u;
static CONST(uint8, CANTP_CONST) CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_STmin = 10u;

static CONST(CanTp_RxNSduType, CANTP_CONST) CanTp_Ch0RxNSdus[CANTP_CH0_RXNSDU_NUMBER] =
{
	{
		&CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_Bs,
	    NULL_PTR,
	    50u,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_Ncr,
	    CANTP_STANDARD,
	    CANTP_PDUR_Dcm_Pdu_767_Diag_Phy_Rx,
	    CANTP_Dcm_Pdu_767_Diag_Phy_Rx,
		CANTP_CanIf_Pdu_767_Diag_Phy_Rx,
		CANTP_CanIf_Pdu_76F_Diag_Respone_Tx,				
		CANTP_CANIF_CanIf_Pdu_76F_Diag_Respone_Tx,	
		0u,
	    CANTP_PADDING_ON,
	    CANTP_PHYSICAL_RX,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_RxWftMax,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_767_Diag_Phy_Rx_STmin,
		NULL_PTR,
		NULL_PTR,
		NULL_PTR,
		CAN20,
	},
	{
		&CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_Bs,
	    NULL_PTR,
	    50u,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_Ncr,
	    CANTP_STANDARD,
	    CANTP_PDUR_Dcm_Pdu_7DF_Diag_Fun_Rx,
	    CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx,
		CANTP_CanIf_Pdu_7DF_Diag_Fun_Rx,
		0xFFu,
		0xFFu,	
		0u,
	    CANTP_PADDING_ON,
	    CANTP_FUNCTIONAL_RX,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_RxWftMax,
	    &CanTp_Ch0RxSdu_CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx_STmin,
		NULL_PTR,
		NULL_PTR,
		NULL_PTR,
		CAN20,
	},
};

/*CanTpChannel TxSdu address information*/
static CONST(uint32, CANTP_CONST) CanTp_Ch0TxSdu_CANTP_Dcm_Pdu_76F_Diag_Respone_Tx_Nbs = 150u;
static CONST(uint32, CANTP_CONST) CanTp_Ch0TxSdu_CANTP_Dcm_Pdu_76F_Diag_Respone_Tx_Ncs = 21u;

static CONST(CanTp_TxNSduType, CANTP_CONST) CanTp_Ch0TxNSdus[CANTP_CH0_TXNSDU_NUMBER] =
{	
	{
		70u,
		&CanTp_Ch0TxSdu_CANTP_Dcm_Pdu_76F_Diag_Respone_Tx_Nbs,
		&CanTp_Ch0TxSdu_CANTP_Dcm_Pdu_76F_Diag_Respone_Tx_Ncs,
		FALSE,
		CANTP_STANDARD,
		CANTP_PDUR_Dcm_Pdu_76F_Diag_Respone_Tx,	
		CANTP_Dcm_Pdu_76F_Diag_Respone_Tx,
		CANTP_CanIf_Pdu_76F_Diag_Respone_Tx,	
		CANTP_CanIf_Pdu_767_Diag_Phy_Rx,
		CANTP_CANIF_CanIf_Pdu_76F_Diag_Respone_Tx,
		0u,
		8u,	
		CANTP_PADDING_ON,
		CANTP_PHYSICAL_TX,
		NULL_PTR,
		NULL_PTR,
		NULL_PTR,
		CAN20,
    },
};

static CONST(CanTp_ChannelType, CANTP_CONST) CanTp_CfgChannel[CANTP_CHANNEL_NUMBER] =
{	
	{
		CANTP_MODE_FULL_DUPLEX,
		CANTP_CH0_RXNSDU_NUMBER,
		&CanTp_Ch0RxNSdus[0],
		CANTP_CH0_TXNSDU_NUMBER,
		&CanTp_Ch0TxNSdus[0]
	},
};

CONST(CanTp_ConfigType, CANTP_CONST) CanTp_CfgData =
{
	CANTP_MAIN_FUNCTION_PERIOD,
	0u,
	&CanTp_CfgChannel[0]
};

#define CANTP_STOP_SEC_CONST_UNSPECIFIED
#include "Cantp_MemMap.h"
/*=======[E X T E R N A L   D A T A]===========================*/
/*=======[E N D   O F   F I L E]==============================*/
