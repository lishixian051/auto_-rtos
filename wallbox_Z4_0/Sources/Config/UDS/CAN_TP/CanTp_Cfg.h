/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <CanTp_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-06-17 10:10:54>
 */
/*============================================================================*/


/*============================================================================*/
#ifndef CANTP_CFG_H
#define CANTP_CFG_H

#include "CanIf_Cfg.h"
#include "Dcm_Cfg.h"

/*========[V E R S I O N  I N F O R M A T I O N]=========*/
#define CANTP_CFG_H_AR_MAJOR_VERSION  4
#define CANTP_CFG_H_AR_MINOR_VERSION  2
#define CANTP_CFG_H_AR_PATCH_VERSION  2
#define CANTP_CFG_H_SW_MAJOR_VERSION  1
#define CANTP_CFG_H_SW_MINOR_VERSION  0
#define CANTP_CFG_H_SW_PATCH_VERSION  0

/*======== [I N C L U D E S]========================*/
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */


/*========[M A C R O S] ==========================*/
#define CANTP_INSTANCE_ID						0x0u

/* Switches the Development Error Detection and Notification ON or OFF */
#define CANTP_DEV_ERROR_DETECT 					STD_OFF

/* The time for MainFunction,expressed as the value with ms */
#define CANTP_MAIN_FUNCTION_PERIOD 				0x0u

/*configuration MACRO addressing mode for cutting*/
#define CANTP_EXTENDED_ADDRESSING_SUPPORT    	STD_OFF
#define CANTP_NORMAL_FIXED_ADDRESSING_SUPPORT	STD_OFF
#define CANTP_MIXED_ADDRESSING_SUPPORT    		STD_OFF
#define CANTP_MIXED29_ADDRESSING_SUPPORT    	STD_OFF

/* Used for the initialization of unused bytes with a certain value */
#define CANTP_PADDING_BYTE 						0xaau

/* Preprocessor switch for enabling Transmit Cancellation and Receive Cancellation. */
#define CANTP_TC 					    		STD_OFF

/* Preprocessor switch for enabling CanTp_ChangeParameterRequest Api*/
#define CANTP_CHANGE_PARAMETER					STD_OFF

/* Preprocessor switch for enabling CanTpReadParameterApi Api*/
#define CANTP_READ_PARAMETER					STD_OFF

#define CANTP_VERSION_INFO_API 					STD_OFF

/*Enable support for CAN FD frames*/
#define CANTP_FD								STD_OFF

#define CANTP_DYN_ID_SUPPORT                	STD_OFF
#define CANTP_GENERIC_CONNECTION_SUPPORT		STD_OFF

/* Total number of channel used in CanTp module. */
#define CANTP_CHANNEL_NUMBER 		   			0x1u

 
/* -------------------- CanTpChannel -------------------- */

#define CANTP_CH0_RXNSDU_NUMBER     			0x2u
#define CANTP_CH0_TXNSDU_NUMBER     			0x1u


/* CanTpRxNSdu_0x767_Diag_Phy_Rx */	
#define CANTP_PDUR_Dcm_Pdu_767_Diag_Phy_Rx     					DCM_Dcm_Pdu_767_Diag_Phy_Rx/*RxIPdu Id*/
#define CANTP_Dcm_Pdu_767_Diag_Phy_Rx								0x3u/*RxNSdu Id*/
#define CANTP_CanIf_Pdu_767_Diag_Phy_Rx		     					0x0u/*RxNPdu Id*/
#define CANTP_CanIf_Pdu_76F_Diag_Respone_Tx   							0x1u/*TxFcNPdu Id*/
#define CANTP_CANIF_CanIf_Pdu_76F_Diag_Respone_Tx  						CANIF_TXPDU_CanIf_Pdu_76F_Diag_Respone_Tx/*TxFcLPdu Id*/

/* CanTpRxNSdu_0x7df_Diag_Fun_Rx */	
#define CANTP_PDUR_Dcm_Pdu_7DF_Diag_Fun_Rx     					DCM_Dcm_Pdu_7DF_Diag_Fun_Rx/*RxIPdu Id*/
#define CANTP_Dcm_Pdu_7DF_Diag_Fun_Rx								0x5u/*RxNSdu Id*/
#define CANTP_CanIf_Pdu_7DF_Diag_Fun_Rx		     					0x2u/*RxNPdu Id*/


/* CanTpTxNSdu_0x76f_Diag_Respone_Tx */	
#define CANTP_PDUR_Dcm_Pdu_76F_Diag_Respone_Tx     					DCM_Dcm_Pdu_76F_Diag_Respone_Tx/*TxIPdu Id*/
#define CANTP_Dcm_Pdu_76F_Diag_Respone_Tx								0x4u/*TxNSdu Id*/
#define CANTP_CanIf_Pdu_76F_Diag_Respone_Tx		     				0x1u/*TxNPdu Id*/
#define CANTP_CanIf_Pdu_767_Diag_Phy_Rx   							0x0u/*RxFcNPdu Id*/
#define CANTP_CANIF_CanIf_Pdu_76F_Diag_Respone_Tx						CANIF_TXPDU_CanIf_Pdu_76F_Diag_Respone_Tx/*TxLPdu Id*/

#ifdef __cplusplus
}
#endif  /* __cplusplus */
#endif /* CANTP_CFG_H */