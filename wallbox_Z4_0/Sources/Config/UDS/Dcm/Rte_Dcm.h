/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Dcm.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-06-05 16:00:36>
 */
/*============================================================================*/

#ifndef RTE_DCM_H_
#define RTE_DCM_H_

#include "Rte_Dcm_Type.h"
#include "Dcm_Types.h"

extern uint8 TestNRC78_Flagg;  /* 21657 */
/***************************Security Part****************************************/
extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_CompareKey( const  uint8*  Key,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_CompareKey( const  uint8*  Key,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8*  AttemptCounter );
extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8*  AttemptCounter );

extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetSeed( Dcm_OpStatusType  OpStatus,uint8*  Seed, Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetSeed( Dcm_OpStatusType  OpStatus,uint8*  Seed, Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_SetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8  AttemptCounter );
extern  Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_SetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8  AttemptCounter );
/***************************Did Part****************************************/

extern  Std_ReturnType  Rte_FreezeCurrentState_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode );


extern  Std_ReturnType  Rte_ReadDataLength_0301( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0302( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0303( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0304( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_03FF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0A01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0AFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0B01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0BFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0E01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0E02( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0EFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_FFFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_020E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0210( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0212( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0214( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_02FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_030A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_030E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0310( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0314( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0316( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_031B( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_03FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_04FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_060C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_060E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0610( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0612( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_06FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_070A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_070C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_07FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_080A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_080C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_08FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_090A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_09FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0B0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0BFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0C0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0CFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0D0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0D0C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0DFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0E0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0EFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_FEFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_020A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_030C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_190A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_19FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_0D10( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );
extern  Std_ReturnType  Rte_ReadDataLength_090C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength );

extern  Std_ReturnType  Rte_ReadData_0301( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0302( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0303( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0304( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_03FF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0A01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0AFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0B01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0BFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0E01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0E02( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0EFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_FFFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_020E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0210( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0212( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0214( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_02FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_030A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_030E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0310( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0314( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0316( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_031B( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_03FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_040A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_04FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_060C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_060E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0610( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0612( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_06FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_070A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_070C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_07FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_080A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_080C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_08FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_090A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_09FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0B0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0BFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0C0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0CFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0D0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0D0C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0DFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0E0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0EFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_FEFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_020A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_030C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_190A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_19FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_0D10( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_ReadData_090C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_RestToDefault_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_ReturnControlToEcu_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_ShortTermAdjustment_040A( uint8*  ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode );

extern  Std_ReturnType  Rte_WriteData_0311( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_0317( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_031A( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_0611( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_0613( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_0B0B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_030D( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_031E( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_031F( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_191B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_090B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_110A( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_110C( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_110E( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_1110( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_WriteData_090D( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode );
/***************************Routine Part****************************************/

extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0101_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0102_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0103_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0104_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0105_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0106_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0107_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0108_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0109_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010A_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010B_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010C_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010D_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010E_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010F_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0110_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0111_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0112_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0113_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0114_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x01FF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0201_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0202_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x02FF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0301_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0302_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0303_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x03FF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0601_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x06FF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1001_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x10ff_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0B01_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0BFF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFDFF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1701_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFCFF_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1801_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1802_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1803_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1804_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF01_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );
extern  Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF02_Start( uint8* OutBuffer,
													uint8* InBuffer,
													Dcm_NegativeResponseCodeType*  ErrorCode );


#endif  /* #ifndef */
