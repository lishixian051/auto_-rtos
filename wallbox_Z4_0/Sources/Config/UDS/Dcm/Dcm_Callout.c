/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Dcm_Callout.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-06-05 16:00:36>
 */
/*============================================================================*/

#include "Dcm_Internal.h"

/** The physical memory location of boot request flag. LOCAL address*/
/* @type:uint32 range:0x00000000~0xFFFFFFFF note:NONE */
#define FL_BOOT_MODE      0x40001000
/** The physical memory location of application software update flag. LOCAL address*/
/* @type:uint32 range:0x00000000~0xFFFFFFFF note:NONE */
#define FL_APPL_UPDATE	

Std_ReturnType Dcm_SetProgConditions(
Dcm_OpStatusType OpStatus,
Dcm_ProgConditionsType * ProgConditions
)
{
    /*Set ReProgramingRequest Flag*/
    (*(uint32 *)FL_BOOT_MODE) = 0x2A2A2A2A;
    return E_OK;
};

Dcm_EcuStartModeType Dcm_GetProgConditions(
Dcm_ProgConditionsType * ProgConditions
)
{
	return DCM_COLD_START;
};


void Rte_DcmControlCommunicationMode(NetworkHandleType DcmDspComMChannelId,Dcm_CommunicationModeType RequestedMode)
{
	switch(RequestedMode)
	{
		case DCM_ENABLE_RX_TX_NORM:
		break;
		case DCM_DISABLE_RX_TX_NM:
		break;
		case DCM_ENABLE_RX_TX_NM:
		break;
		case DCM_ENABLE_RX_TX_NORM_NM:
		break;
		case DCM_DISABLE_RX_TX_NORMAL:
		break;
		case DCM_DISABLE_RX_TX_NORM_NM:
		break;
		default:
		break;
	}
}
