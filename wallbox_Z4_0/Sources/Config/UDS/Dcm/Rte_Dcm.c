/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Dcm.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-06-05 16:00:36>
 */
/*============================================================================*/


#include "Rte_Dcm.h"
#include "Dcm_Internal.h"
const  uint8  DataLength_FalseAcessCount_DcmDspSecurityRow_Level_K1  =  1;
uint8  Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K1[1]  =  {0x0};
#define  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K1 
const  uint8  DataLength_FalseAcessCount_DcmDspSecurityRow_Level_K2  =  1;
uint8  Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K2[1]  =  {0x0};
#define  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K2 
uint8 TestNRC78_Flagg = FALSE;  /* 21657 */
uint8 Stop_NRC78 = FALSE;

/***************************Security Part****************************************/
Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_CompareKey( const  uint8*  Key,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode )
{
    if(NULL_PTR == Key)
    {
    	return  E_COMPARE_KEY_FAILED;
    }
	else
	{
		if((Key[0] == 0x11)&&(Key[1] == 0x33)&&(Key[2] == 0x55)&&(Key[3] == 0x77))
		{
		    return E_OK;
		}
		else
		{
	    	return  E_COMPARE_KEY_FAILED;
		}
	}
}
Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_CompareKey( const  uint8*  Key,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode )
{
    if(NULL_PTR == Key)
    {
    	return  E_COMPARE_KEY_FAILED;
    }
	else
	{
		if((Key[0] == 0x11)&&(Key[1] == 0x33)&&(Key[2] == 0x55)&&(Key[3] == 0x77))
		{
		    return E_OK;
		}
		else
		{
	    	return  E_COMPARE_KEY_FAILED;
		}
	}
}

Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetAttemptCounter( Dcm_OpStatusType  OpStatus,uint8*  AttemptCounter )
{
#ifdef  DCM_DDDID_STORAGE_BLOCKID
#ifdef  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K1
    (*AttemptCounter) = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_FalseAcessCount_DcmDspSecurityRow_Level_K1);
#endif
#else
    (*AttemptCounter) = Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K1[0];

    *AttemptCounter = *(uint8*)(0x40001000); /* 21657  K1 remove relase  */
#endif
	return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetAttemptCounter( Dcm_OpStatusType  OpStatus,uint8*  AttemptCounter )
{
#ifdef  DCM_DDDID_STORAGE_BLOCKID
#ifdef  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K2
    (*AttemptCounter) = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_FalseAcessCount_DcmDspSecurityRow_Level_K2);
#endif
#else
    (*AttemptCounter) = Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K2[0];

    *AttemptCounter = *(uint8*)(0x40001004); /* 21657  K2 remove relase  */
#endif
	return E_OK;
}

Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_GetSeed( Dcm_OpStatusType  OpStatus,uint8*  Seed,Dcm_NegativeResponseCodeType*  ErrorCode )
{
   Seed[0] = 0x55;
   Seed[1] = 0x11;
   Seed[2] = 0x11;
   Seed[3] = 0x11;
   
    return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_GetSeed( Dcm_OpStatusType  OpStatus,uint8*  Seed,Dcm_NegativeResponseCodeType*  ErrorCode )
{
   Seed[0] = 0x55;
   Seed[1] = 0x22;
   Seed[2] = 0x22;
   Seed[3] = 0x22;
    return E_OK;
}

Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K1_SetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8  AttemptCounter )
{
#ifdef  DCM_DDDID_STORAGE_BLOCKID
#ifdef  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K1
    *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_FalseAcessCount_DcmDspSecurityRow_Level_K1) = AttemptCounter;
NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
#else
    Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K1[0] = AttemptCounter;

	 *(uint8*)(0x40001000) = AttemptCounter; /* 21657  K1 remove relase  */
#endif
	return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_SecurityAccess_DcmDspSecurityRow_Level_K2_SetAttemptCounter( Dcm_OpStatusType  OpStatus,  uint8  AttemptCounter )
{
#ifdef  DCM_DDDID_STORAGE_BLOCKID
#ifdef  Offset_FalseAcessCount_DcmDspSecurityRow_Level_K2
    *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_FalseAcessCount_DcmDspSecurityRow_Level_K2) = AttemptCounter;
NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
#else
    Buffer_FalseAcessCount_DcmDspSecurityRow_Level_K2[0] = AttemptCounter;

	 *(uint8*)(0x40001004) = AttemptCounter; /* 21657 K2 remove relase  */
#endif
	return E_OK;
}
/***************************Did Part****************************************/
//uint8 Buffer_DcmDspData_0301[ 20 ] = {"i2224e-s_200214_r158"} ;
uint8 Buffer_DcmDspData_0301[ 20 ] = {0x42,0x54,0x31,0x31,0x50,0x33,0x31,0x20,0x56,0x31,0x2E,0x30,0x20,0x00,0x00,0x00,0x00,0x00,0x00} ;
const uint16 DataLength_DcmDspData_0301  =  20; 
//uint8 Buffer_DcmDspData_0302[ 12 ] = {"CD15757AF93D"} ;
uint8 Buffer_DcmDspData_0302[ 12 ] = {0x43,0x44,0x31,0x35,0x37,0x35,0x37,0x41,0x46,0x39,0x33,0x30} ;
const uint16 DataLength_DcmDspData_0302  =  12; 
uint8 Buffer_DcmDspData_0303[ 20 ] = {"i2224e-s_200214_r158"} ;
const uint16 DataLength_DcmDspData_0303  =  20; 
uint8 Buffer_DcmDspData_0304[ 12 ] = {"CD15757AF93D"} ;
const uint16 DataLength_DcmDspData_0304  =  12; 
uint8 Buffer_DcmDspData_03FF[ 64 ] = {0x20} ;
const uint16 DataLength_DcmDspData_03FF  =  64; 
uint8 Buffer_DcmDspData_0A01[ 5 ] = {"V0.85"} ;
const uint16 DataLength_DcmDspData_0A01  =  5; 
uint8 Buffer_DcmDspData_0AFF[ 5 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0AFF  =  5; 
uint8 Buffer_DcmDspData_0B01[ 4 ] = {0x01,0x02,0x03,0x04} ;
const uint16 DataLength_DcmDspData_0B01  =  4; 
uint8 Buffer_DcmDspData_0BFF[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0BFF  =  4; 
uint8 Buffer_DcmDspData_0E01[ 5 ] = {"V0.86"} ;
const uint16 DataLength_DcmDspData_0E01  =  5; 
uint8 Buffer_DcmDspData_0E02[ 5 ] = {"V0.87"} ;
const uint16 DataLength_DcmDspData_0E02  =  5; 
uint8 Buffer_DcmDspData_0EFF[ 10 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0EFF  =  10; 
uint8 Buffer_DcmDspData_FFFF[ 83 ] = {0x20} ;
const uint16 DataLength_DcmDspData_FFFF  =  83; 
uint8 Buffer_DcmDspData_020E[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_020E  =  1; 
uint8 Buffer_DcmDspData_0210[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0210  =  1; 
uint8 Buffer_DcmDspData_0212[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0212  =  4; 
uint8 Buffer_DcmDspData_0214[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0214  =  4; 
uint8 Buffer_DcmDspData_02FE[ 14 ] = {0x20} ;
const uint16 DataLength_DcmDspData_02FE  =  14; 
uint8 Buffer_DcmDspData_030A[ 120 ] = {0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
									   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28
} ;
const uint16 DataLength_DcmDspData_030A  =  120; 
uint8 Buffer_DcmDspData_030E[ 20 ] = {"200214GXYT200214GXYT"} ;
const uint16 DataLength_DcmDspData_030E  =  20; 
uint8 Buffer_DcmDspData_0310[ 20 ] = {"200214GXYT200214GXYT"} ;
const uint16 DataLength_DcmDspData_0310  =  20; 
uint8 Buffer_DcmDspData_0311[ 38 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0311  =  38; 
uint8 Buffer_DcmDspData_0314[ 6 ] = {"123456"} ;
const uint16 DataLength_DcmDspData_0314  =  6; 
uint8 Buffer_DcmDspData_0316[ 130 ] = {"200214GXYT200214GXYT200214GXYT200214GXYT200214GXYT200214GXYT1234"} ;
const uint16 DataLength_DcmDspData_0316  =  130; 
uint8 Buffer_DcmDspData_0317[ 130 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0317  =  130; 
uint8 Buffer_DcmDspData_031A[ 82 ] = {0x20} ;
const uint16 DataLength_DcmDspData_031A  =  82; 
uint8 Buffer_DcmDspData_031B[ 1 ] = {0x01} ;
const uint16 DataLength_DcmDspData_031B  =  1; 
uint8 Buffer_DcmDspData_03FE[ 571 ] = {0x20} ;
const uint16 DataLength_DcmDspData_03FE  =  571; 
uint8 Buffer_DcmDspData_040A[ 6 ] = {0x20} ;
const uint16 DataLength_DcmDspData_040A  =  6; 
uint8 Buffer_DcmDspData_04FE[ 6 ] = {0x20} ;
const uint16 DataLength_DcmDspData_04FE  =  6; 
uint8 Buffer_DcmDspData_060C[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_060C  =  1; 
uint8 Buffer_DcmDspData_060E[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_060E  =  1; 
uint8 Buffer_DcmDspData_0610[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0610  =  1; 
uint8 Buffer_DcmDspData_0611[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0611  =  1; 
uint8 Buffer_DcmDspData_0612[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0612  =  1; 
uint8 Buffer_DcmDspData_0613[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0613  =  1; 
uint8 Buffer_DcmDspData_06FE[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_06FE  =  4; 
uint8 Buffer_DcmDspData_070A[ 6 ] = {0x55,0xE2,0x7D,0xE2,0x69,0xE2} ;
const uint16 DataLength_DcmDspData_070A  =  6; 
uint8 Buffer_DcmDspData_070C[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_070C  =  4; 
uint8 Buffer_DcmDspData_07FE[ 5 ] = {0x20} ;
const uint16 DataLength_DcmDspData_07FE  =  5; 
uint8 Buffer_DcmDspData_080A[ 64 ] = {0x20} ;
const uint16 DataLength_DcmDspData_080A  =  64; 
uint8 Buffer_DcmDspData_080C[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_080C  =  1; 
uint8 Buffer_DcmDspData_08FE[ 65 ] = {0x20} ;
const uint16 DataLength_DcmDspData_08FE  =  65; 
uint8 Buffer_DcmDspData_090A[ 4 ] = {0x42,0xc8,0x33,0x33} ;
const uint16 DataLength_DcmDspData_090A  =  4; 
uint8 Buffer_DcmDspData_09FE[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_09FE  =  4; 
uint8 Buffer_DcmDspData_0B0A[ 50 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0B0A  =  50; 
uint8 Buffer_DcmDspData_0B0B[ 5 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0B0B  =  5; 
uint8 Buffer_DcmDspData_0BFE[ 55 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0BFE  =  55; 
uint8 Buffer_DcmDspData_0C0A[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0C0A  =  1; 
uint8 Buffer_DcmDspData_0CFE[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0CFE  =  1; 
uint8 Buffer_DcmDspData_0D0A[ 4 ] = {0x00,0x00,0x55,0x44} ;
const uint16 DataLength_DcmDspData_0D0A  =  4; 
uint8 Buffer_DcmDspData_0D0C[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0D0C  =  4; 
uint8 Buffer_DcmDspData_0DFE[ 8 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0DFE  =  8; 
uint8 Buffer_DcmDspData_0E0A[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0E0A  =  1; 
uint8 Buffer_DcmDspData_0EFE[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_0EFE  =  1; 
uint8 Buffer_DcmDspData_FEFF[ 669 ] = {0x20} ;
const uint16 DataLength_DcmDspData_FEFF  =  669; 
uint8 Buffer_DcmDspData_020A[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_020A  =  4; 
uint8 Buffer_DcmDspData_030C[ 120 ] = {0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
		   	   	   	   	   	   	   	   0x20,0x3E,0x28,0x20,0x3E,0x28,0x28,0x20,0x3E,0x28,
} ;
const uint16 DataLength_DcmDspData_030C  =  120; 
uint8 Buffer_DcmDspData_030D[ 12 ] = {0x20} ;
const uint16 DataLength_DcmDspData_030D  =  12; 
uint8 Buffer_DcmDspData_031E[ 12 ] = {0x20} ;
const uint16 DataLength_DcmDspData_031E  =  12; 
uint8 Buffer_DcmDspData_031F[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_031F  =  1; 
uint8 Buffer_DcmDspData_190A[ 6 ] = {0x14,0x05,0x15,0x0f,0x14,0x10} ;
const uint16 DataLength_DcmDspData_190A  =  6; 
uint8 Buffer_DcmDspData_190B[ 6 ] = {0x20} ;
const uint16 DataLength_DcmDspData_190B  =  6; 
uint8 Buffer_DcmDspData_19FE[ 6 ] = {0x20} ;
const uint16 DataLength_DcmDspData_19FE  =  6; 
uint8 Buffer_DcmDspData_090B[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_090B  =  4; 
uint8 Buffer_DcmDspData_110A[ 4 ] = {0x00,0x00,0x00,0x20} ;
const uint16 DataLength_DcmDspData_110A  =  4; 
uint8 Buffer_DcmDspData_110C[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_110C  =  4; 
uint8 Buffer_DcmDspData_110E[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_110E  =  4; 
uint8 Buffer_DcmDspData_1110[ 4 ] = {0x20} ;
const uint16 DataLength_DcmDspData_1110  =  4; 
uint8 Buffer_DcmDspData_0D10[ 8 ] = {0x3F,0xF0,0x00,0x00,0x00,0x00,0x00,0x00} ;
const uint16 DataLength_DcmDspData_0D10  =  8; 
uint8 Buffer_DcmDspData_090C[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_090C  =  1; 
uint8 Buffer_DcmDspData_090D[ 1 ] = {0x20} ;
const uint16 DataLength_DcmDspData_090D  =  1; 

Std_ReturnType  Rte_FreezeCurrentState_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode )
{
return E_OK;
}


Std_ReturnType  Rte_ReadDataLength_0301( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0302( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0303( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0304( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_03FF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0A01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0AFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0B01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0BFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0E01( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0E02( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0EFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_FFFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_020E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0210( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0212( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0214( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_02FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_030A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_030E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0310( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0314( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0316( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_031B( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_03FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_04FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_060C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_060E( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0610( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0612( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_06FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_070A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_070C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_07FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_080A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_080C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_08FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_090A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_09FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0B0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0BFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0C0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0CFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0D0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0D0C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0DFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0E0A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0EFE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_FEFF( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_020A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_030C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_190A( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_19FE( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_0D10( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}
Std_ReturnType  Rte_ReadDataLength_090C( Dcm_OpStatusType  OpStatus,  uint16*  DataLength ) 
{
return E_OK;
}

Std_ReturnType  Rte_ReadData_0301( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;


	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{
		if(Stop_NRC78 == 1)
		{
			while(1);
		}

		for(index = 0;index < DataLength_DcmDspData_0301;index++)
		{
		#ifdef  Offset_DcmDspData_0301
		#ifdef  DCM_DDDID_STORAGE_BLOCKID
			Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0301 + index);
		#endif
		#else
			Data[index] = Buffer_DcmDspData_0301[index];
		#endif
		}

		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;

	}

}
Std_ReturnType  Rte_ReadData_0302( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{
	    for(index = 0;index < DataLength_DcmDspData_0302;index++)
	    {
	#ifdef  Offset_DcmDspData_0302
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0302 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0302[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_ReadData_0303( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;

	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{

		if(Stop_NRC78 == 2)
		{
			while(1);
		}

		for(index = 0;index < DataLength_DcmDspData_0303;index++)
		{
	#ifdef  Offset_DcmDspData_0303
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
			Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0303 + index);
	#endif
	#else
			Data[index] = Buffer_DcmDspData_0303[index];
	#endif
		}
		TestNRC78_Flagg = FALSE;

		//return E_OK;  /* 21657 */
		*ErrorCode = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
		return E_NOT_OK;

	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_ReadData_0304( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;

	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{

	    for(index = 0;index < DataLength_DcmDspData_0304;index++)
	    {
	#ifdef  Offset_DcmDspData_0304
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0304 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0304[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		//return E_OK;  /* 21657 */
		*ErrorCode = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
		return E_NOT_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_ReadData_03FF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_03FF;index++)
    {
#ifdef  Offset_DcmDspData_03FF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_03FF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_03FF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0A01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;

	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{
	    for(index = 0;index < DataLength_DcmDspData_0A01;index++)
	    {
	#ifdef  Offset_DcmDspData_0A01
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0A01 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0A01[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_ReadData_0AFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0AFF;index++)
    {
#ifdef  Offset_DcmDspData_0AFF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0AFF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0AFF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0B01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;

	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{
	    for(index = 0;index < DataLength_DcmDspData_0B01;index++)
	    {
	#ifdef  Offset_DcmDspData_0B01
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0B01 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0B01[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_ReadData_0BFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0BFF;index++)
    {
#ifdef  Offset_DcmDspData_0BFF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0BFF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0BFF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0E01( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;

	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{
	    for(index = 0;index < DataLength_DcmDspData_0E01;index++)
	    {
	#ifdef  Offset_DcmDspData_0E01
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0E01 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0E01[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}

}
Std_ReturnType  Rte_ReadData_0E02( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
	if(TRUE == TestNRC78_Flagg)   /* 21657 */
	{

	    for(index = 0;index < DataLength_DcmDspData_0E02;index++)
	    {
	#ifdef  Offset_DcmDspData_0E02
	#ifdef  DCM_DDDID_STORAGE_BLOCKID
	        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0E02 + index);
	#endif
	#else
	        Data[index] = Buffer_DcmDspData_0E02[index];
	#endif
	    }
		TestNRC78_Flagg = FALSE;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}

}
Std_ReturnType  Rte_ReadData_0EFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0EFF;index++)
    {
#ifdef  Offset_DcmDspData_0EFF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0EFF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0EFF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_FFFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_FFFF;index++)
    {
#ifdef  Offset_DcmDspData_FFFF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_FFFF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_FFFF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_020E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_020E;index++)
    {
#ifdef  Offset_DcmDspData_020E
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_020E + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_020E[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0210( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0210;index++)
    {
#ifdef  Offset_DcmDspData_0210
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0210 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0210[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0212( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0212;index++)
    {
#ifdef  Offset_DcmDspData_0212
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0212 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0212[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0214( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0214;index++)
    {
#ifdef  Offset_DcmDspData_0214
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0214 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0214[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_02FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_02FE;index++)
    {
#ifdef  Offset_DcmDspData_02FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_02FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_02FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_030A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_030A;index++)
    {
#ifdef  Offset_DcmDspData_030A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_030A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_030A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_030E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_030E;index++)
    {
#ifdef  Offset_DcmDspData_030E
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_030E + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_030E[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0310( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0310;index++)
    {
#ifdef  Offset_DcmDspData_0310
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0310 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0310[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0314( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0314;index++)
    {
#ifdef  Offset_DcmDspData_0314
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0314 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0314[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0316( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0316;index++)
    {
#ifdef  Offset_DcmDspData_0316
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0316 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0316[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_031B( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_031B;index++)
    {
#ifdef  Offset_DcmDspData_031B
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_031B + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_031B[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_03FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_03FE;index++)
    {
#ifdef  Offset_DcmDspData_03FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_03FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_03FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_040A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_040A;index++)
    {
#ifdef  Offset_DcmDspData_040A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_040A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_040A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_04FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_04FE;index++)
    {
#ifdef  Offset_DcmDspData_04FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_04FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_04FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_060C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_060C;index++)
    {
#ifdef  Offset_DcmDspData_060C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_060C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_060C[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_060E( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_060E;index++)
    {
#ifdef  Offset_DcmDspData_060E
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_060E + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_060E[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0610( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0610;index++)
    {
#ifdef  Offset_DcmDspData_0610
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0610 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0610[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0612( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0612;index++)
    {
#ifdef  Offset_DcmDspData_0612
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0612 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0612[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_06FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_06FE;index++)
    {
#ifdef  Offset_DcmDspData_06FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_06FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_06FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_070A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_070A;index++)
    {
#ifdef  Offset_DcmDspData_070A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_070A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_070A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_070C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_070C;index++)
    {
#ifdef  Offset_DcmDspData_070C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_070C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_070C[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_07FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_07FE;index++)
    {
#ifdef  Offset_DcmDspData_07FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_07FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_07FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_080A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_080A;index++)
    {
#ifdef  Offset_DcmDspData_080A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_080A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_080A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_080C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_080C;index++)
    {
#ifdef  Offset_DcmDspData_080C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_080C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_080C[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_08FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_08FE;index++)
    {
#ifdef  Offset_DcmDspData_08FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_08FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_08FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_090A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_090A;index++)
    {
#ifdef  Offset_DcmDspData_090A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_090A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_090A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_09FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_09FE;index++)
    {
#ifdef  Offset_DcmDspData_09FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_09FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_09FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0B0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0B0A;index++)
    {
#ifdef  Offset_DcmDspData_0B0A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0B0A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0B0A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0BFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0BFE;index++)
    {
#ifdef  Offset_DcmDspData_0BFE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0BFE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0BFE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0C0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0C0A;index++)
    {
#ifdef  Offset_DcmDspData_0C0A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0C0A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0C0A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0CFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0CFE;index++)
    {
#ifdef  Offset_DcmDspData_0CFE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0CFE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0CFE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0D0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0D0A;index++)
    {
#ifdef  Offset_DcmDspData_0D0A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0D0A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0D0A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0D0C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0D0C;index++)
    {
#ifdef  Offset_DcmDspData_0D0C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0D0C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0D0C[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0DFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0DFE;index++)
    {
#ifdef  Offset_DcmDspData_0DFE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0DFE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0DFE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0E0A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0E0A;index++)
    {
#ifdef  Offset_DcmDspData_0E0A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0E0A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0E0A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0EFE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0EFE;index++)
    {
#ifdef  Offset_DcmDspData_0EFE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0EFE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0EFE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_FEFF( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_FEFF;index++)
    {
#ifdef  Offset_DcmDspData_FEFF
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_FEFF + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_FEFF[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_020A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_020A;index++)
    {
#ifdef  Offset_DcmDspData_020A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_020A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_020A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_030C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_030C;index++)
    {
#ifdef  Offset_DcmDspData_030C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_030C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_030C[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_190A( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_190A;index++)
    {
#ifdef  Offset_DcmDspData_190A
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_190A + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_190A[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_19FE( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_19FE;index++)
    {
#ifdef  Offset_DcmDspData_19FE
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_19FE + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_19FE[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_0D10( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_0D10;index++)
    {
#ifdef  Offset_DcmDspData_0D10
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0D10 + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_0D10[index];
#endif
    }
	return E_OK;
}
Std_ReturnType  Rte_ReadData_090C( Dcm_OpStatusType  OpStatus,uint8*  Data,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint16  index;
    for(index = 0;index < DataLength_DcmDspData_090C;index++)
    {
#ifdef  Offset_DcmDspData_090C
#ifdef  DCM_DDDID_STORAGE_BLOCKID
        Data[index] = *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_090C + index);
#endif
#else
        Data[index] = Buffer_DcmDspData_090C[index];
#endif
    }
	return E_OK;
}

Std_ReturnType  Rte_RestToDefault_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode )
{
return E_OK;
}

Std_ReturnType  Rte_ReturnControlToEcu_040A( uint8* ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode )
{
return E_OK;
}

Std_ReturnType  Rte_ShortTermAdjustment_040A( uint8*  ControlOptionRecord,uint8*  ControlEnableMaskRecord,Dcm_NegativeResponseCodeType*  ErrorCode )
{
	uint8  index;
    for(index = 0;index < DataLength_DcmDspData_040A;index++)
    {
        Buffer_DcmDspData_040A[index] = ControlOptionRecord[index];
    }
	return E_OK;
}

Std_ReturnType  Rte_WriteData_0311( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_0311; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_0311
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0311 + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_0311[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_0317( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_0317; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_0317
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0317 + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_0317[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_031A( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_031A; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_031A
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_031A + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_031A[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_0611( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_0611; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_0611
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0611 + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_0611[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif

    if(*Data == 1)
    {
    	Stop_NRC78 = 1;
    }
    else if(*Data == 2)
    {
    	Stop_NRC78 = 2;
    }
    else
    {

    }
	return E_OK;
}
Std_ReturnType  Rte_WriteData_0613( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_0613; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_0613
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0613 + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_0613[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_0B0B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_0B0B; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_0B0B
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_0B0B + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_0B0B[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_030D( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_030D; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_030D
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_030D + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_030D[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_031E( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_031E; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_031E
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_031E + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_031E[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_031F( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_031F; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_031F
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_031F + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_031F[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_191B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_190B; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_190B
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_190B + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_190B[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_090B( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_090B; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_090B
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_090B + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_090B[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_110A( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_110A; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_110A
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_110A + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_110A[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_110C( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_110C; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_110C
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_110C + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_110C[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_110E( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_110E; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_110E
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_110E + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_110E[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_1110( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_1110; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_1110
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_1110 + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_1110[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
Std_ReturnType  Rte_WriteData_090D( const  uint8*  Data,uint16  DataLength,Dcm_OpStatusType  OpStatus,Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	uint8  index = 0;
    for(index = 0; index < DataLength_DcmDspData_090D; index++)
    {
#ifdef DCM_DDDID_STORAGE_BLOCKID
#ifdef Offset_DcmDspData_090D
        *(NvM_BlockDescriptor[DCM_DDDID_STORAGE_BLOCKID - 1].NvmRamBlockDataAddress + Offset_DcmDspData_090D + index) = Data[index] ;
#endif
#else
        Buffer_DcmDspData_090D[index] = Data[index];
#endif
    }
#ifdef DCM_DDDID_STORAGE_BLOCKID
    NvM_WriteBlock(DCM_DDDID_STORAGE_BLOCKID, NULL_PTR);
#endif
	return E_OK;
}
/***************************Routine Part****************************************/		

Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0101_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0102_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0103_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0104_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0105_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0106_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0107_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0108_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0109_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010A_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010B_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010C_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010D_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010E_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x010F_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0110_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0111_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0112_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0113_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0114_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x01FF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0201_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	*OutBuffer = 0x01;
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0202_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	*OutBuffer = 0x02;
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x02FF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0301_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0302_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0303_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x03FF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0601_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x06FF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1001_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x10ff_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0B01_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x0BFF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFDFF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1701_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFCFF_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1801_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	*OutBuffer = 0x01;
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1802_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	*OutBuffer = 0x02;
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1803_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0x1804_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
return E_OK;
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF01_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	if(TRUE== TestNRC78_Flagg)
	{
		TestNRC78_Flagg = FALSE;

		/* Receive ID */
		*OutBuffer = 0x00;
		*(OutBuffer+1) = 0x00;
		*(OutBuffer+2) = 0x01;
		*(OutBuffer+3) = 0xFF;
		/* Data size [When ECU Have a CF is 0xFFFF,else is not 0xFFFF]*/
		*(OutBuffer+4) = 0xFF;
		*(OutBuffer+5) = 0xFF;

		/* Seq[Reserved] */
		*(OutBuffer+6) = 0x00;
		*(OutBuffer+7) = 0x00;
		/* Data[96 bytes] */
		*(OutBuffer+8) = 0x10;
		*(OutBuffer+9) = 0x08;
		*(OutBuffer+10) = 0x21;
		*(OutBuffer+11) = 0x08;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
Std_ReturnType  Rte_Call_Dcm_RoutineServices_DcmDspRoutine_0xFF02_Start( uint8* OutBuffer,
											uint8* InBuffer,
											Dcm_NegativeResponseCodeType*  ErrorCode ) 
{
	static uint8 Call_Cnt = 0 ;
	if(TRUE== TestNRC78_Flagg)
	{

		TestNRC78_Flagg = FALSE;
		Call_Cnt++;
		/* Receive ID */
		*OutBuffer = 0x00;
		*(OutBuffer+1) = 0x00;
		*(OutBuffer+2) = 0x01;
		*(OutBuffer+3) = 0xFF;

		if(Call_Cnt >= 20)
		{
			Call_Cnt = 0;
			/* Data size [When ECU Have a CF is 0xFFFF,else is not 0xFFFF]*/
			*(OutBuffer+4) = 0x00;
			*(OutBuffer+5) = 0x20;
		}
		else
		{
			/* Data size [When ECU Have a CF is 0xFFFF,else is not 0xFFFF]*/
			*(OutBuffer+4) = 0xFF;
			*(OutBuffer+5) = 0xFF;
		}


		/* Seq[Reserved] */
		*(OutBuffer+6) = 0x00;
		*(OutBuffer+7) = 0x00;
		/* Data[96 bytes] */
		*(OutBuffer+8) = 0x10;
		*(OutBuffer+9) = 0x08;
		*(OutBuffer+10) = 0x21;
		*(OutBuffer+11) = 0x08;

		return E_OK;
	}
	else
	{

		return DCM_E_PENDING;
	}
}
