/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Dem.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-05-22 17:14:49>
 */
/*============================================================================*/


#include "Std_Types.h"
/***************************DemDataElement Part****************************************/

Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF01_ReadData( uint8* Buffer )
{
	/* Time 2020.05.21 15:20:16 */
	Buffer[0] = 0x14;
	Buffer[1] = 0x05;
	Buffer[2] = 0x15;
	Buffer[3] = 0x0F;
	Buffer[4] = 0x14;
	Buffer[5] = 0x10;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF02_ReadData( uint8* Buffer )
{
	/* Current 100.1A */
	Buffer[0] = 0x42;
	Buffer[1] = 0xC8;
	Buffer[2] = 0x33;
	Buffer[3] = 0x33;

    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF03_ReadData( uint8* Buffer )
{
	/* Voltage  42c83333 100.1v*/
	Buffer[0] = 0x42;
	Buffer[1] = 0xC8;
	Buffer[2] = 0x33;
	Buffer[3] = 0x33;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF04_ReadData( uint8* Buffer )
{
	/* Reserved  */
	Buffer[0] = 0x00;
	Buffer[1] = 0x00;
	Buffer[2] = 0x00;
	Buffer[3] = 0x01;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF05_ReadData( uint8* Buffer )
{
	/* Error Message  */
	Buffer[0] = 0x00;
	Buffer[1] = 0x00;
	Buffer[2] = 0x01;
	Buffer[3] = 0x01;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF06_ReadData( uint8* Buffer )
{
	/* AbnormityCount  */
	Buffer[0] = 0x01;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF07_ReadData( uint8* Buffer )
{
	/* CheckAbnormity */
	Buffer[0] = 0x14;
	Buffer[1] = 0x05;
	Buffer[2] = 0x15;
	Buffer[3] = 0x0F;
	Buffer[4] = 0x14;
	Buffer[5] = 0x10;
	Buffer[6] = 0x01;
	Buffer[7] = 0x01;
	Buffer[8] = 0xFF;
	Buffer[9] = 0xFF;
    return E_OK;
};
Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF08_ReadData( uint8* Buffer )
{
	/* StroedAbnormity  */
	Buffer[0] = 0x14;
	Buffer[1] = 0x05;
	Buffer[2] = 0x15;
	Buffer[3] = 0x0F;
	Buffer[4] = 0x14;
	Buffer[5] = 0x10;
	Buffer[6] = 0x05;
	Buffer[7] = 0x05;
	Buffer[8] = 0x05;
	Buffer[9] = 0x05;
    return E_OK;
};
