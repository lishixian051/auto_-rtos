/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Dem.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-05-22 17:14:49>
 */
/*============================================================================*/


/***************************DemDataElement Part****************************************/
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF01_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF02_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF03_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF04_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF05_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF06_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF07_ReadData( uint8* Buffer );
extern Std_ReturnType  Rte_Call_Dem_CS_DataServices_DemDataElementClass_DF08_ReadData( uint8* Buffer );

