/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Det_Cfg.c                                                   **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION :                                                             **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/



/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Det.h"
/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/


/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/


/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/



/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/
/*This parameter defines the existence and the name of a callout function for
 *the corresponding runtime error handler.*/
Det_CalloutFnctPtrType Det_ReportRuntimeErrorCallout = NULL_PTR;

/*This parameter defines the existence and the name of a callout function for
 * the corresponding transient fault handler.*/
Det_CalloutFnctPtrType Det_ReportTransientFaultCallout = NULL_PTR;

#if (DET_ERROR_HOOK_NUM > 0)
/*list of functions to be called by the Default Error Tracer in context of each
 * call of Det_ReportError*/
Det_CalloutFnctPtrType Det_ErrorHook[DET_ERROR_HOOK_NUM] =
{
    NULL_PTR
};
#endif

/*******************************************************************************
**                            General Notes                                   **
*******************************************************************************/

