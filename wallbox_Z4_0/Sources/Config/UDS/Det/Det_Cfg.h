/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Det_Cfg.h                                                   **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION :                                                             **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef DET_CFG_H
#define DET_CFG_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "Std_Types.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
/*Only if the parameter is present and set to true, the Det requires the Dlt
 *interface and forwards it's call to the function Dlt_DetForwardErrorTrace.
 *In this case the optional interface to Dlt_Det is required.*/
#define DET_FORWARD_TO_DLT   STD_OFF

/*Pre-processor switch to enable / disable the API to read out the modules
 * version information.*/
#define DET_VERSION_INFO_API   STD_OFF

#define DET_ERROR_HOOK_NUM   1


#endif /* DET_CFG_H */
