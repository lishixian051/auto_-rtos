
/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : CanIf_Cfg.c                                                 **
**                                                                            **
**  Created on  :2020/03/11 10:18:05                                   **
**  Author      : yutao                                                       **
**  Vendor      :                                                             **
**  DESCRIPTION :parameter declaration of CanIf                               **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/


/*******************************************************************************
**                      Version Information                                   **
*******************************************************************************/

#define CANIF_CFG_C_AR_MAJOR_VERSION  4
#define CANIF_CFG_C_AR_MINOR_VERSION  2
#define CANIF_CFG_C_AR_PATCH_VERSION  2
#define CANIF_CFG_C_SW_MAJOR_VERSION  1
#define CANIF_CFG_C_SW_MINOR_VERSION  0
#define CANIF_CFG_C_SW_PATCH_VERSION  0

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "CanIf.h"
#include "CanTp_Cbk.h"
#include "Xcp.h"
#include "Can.h"

/*******************************************************************************
**                      Version Check                                         **
*******************************************************************************/
#if(CANIF_CFG_C_AR_MAJOR_VERSION != CANIF_CFG_H_AR_MAJOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Major Version"
#endif

#if(CANIF_CFG_C_AR_MINOR_VERSION != CANIF_CFG_H_AR_MINOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Minor Version"
#endif

#if(CANIF_CFG_C_AR_PATCH_VERSION != CANIF_CFG_H_AR_PATCH_VERSION)
    #error "CanIf.c:Mismatch in Specification Patch Version"
#endif

#if(CANIF_CFG_C_SW_MAJOR_VERSION != CANIF_CFG_H_SW_MAJOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Major Version"
#endif

#if(CANIF_CFG_C_SW_MINOR_VERSION != CANIF_CFG_H_SW_MINOR_VERSION)
    #error "CanIf.c:Mismatch in Specification Minor Version"
#endif

/*******************************************************************************
**                      Macros                                                **
*******************************************************************************/

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/


void User_ControllerBusOff(uint8 ControllerId)
{
}

void User_ControllerModeIndication(uint8 ControllerId,CanIf_ControllerModeType ControllerMode)
{
}

/* Callout functions with respect to the upper layers */
CONST(CanIf_DispatchConfigType,CANIF_CONST) CanIf_DispatchConfigData =
{
	/* void XXX_ControllerBusOff(uint8 ControllerId) */
	
	&User_ControllerBusOff,
	
	/* void XXX_ControllerModeIndication(uint8 ControllerId,CanIf_ControllerModeType ControllerMode)  */
	
	&User_ControllerModeIndication,
	
	/* void XXX_TransceiverModeIndication(uint8 TransceiverId,CanTrcv_TrcvModeType TransceiverMode) */
	
    NULL_PTR,
	
};


CONST(Can_DriverApiType,CANIF_CONST_PBCFG) Can_DriverApi[CANIF_CANDRIVER_NUMBER] =
{
   {
        &Can_SetControllerMode,
        &Can_Write,
	},
};


CONST(CanIfCtrlCanCtrlRefType,CANIF_CONST_PBCFG) CanIf_CtrlRef[CANIF_CANCONTROLLER_NUMBER] =
{
    {
        0,/*Symbolic name reference to CanCtrl,the lower CanCtrl id*/
        0,/*wake up source*/
	},
};

CONST(CanIf_ControllerCfgType,CANIF_CONST) CanIf_CtrlCfgData[CANIF_CANCONTROLLER_NUMBER] =
{
    {
    	CANIF_CANDRV_0_CanIfCtrlCfg_0,/*CanIf uniform definition all controller Id*/
        CANIF_CAN,/*the controller belong to which CanDriver*/
        FALSE,/* This parameter defines if a respective controller of the referenced CAN Driver modules is queriable for wake up events*/
        &CanIf_CtrlRef[0],/*from Can Driver configuration*/
	},
};



CONST(CanIfHrhIdSymRefType,CANIF_CONST_PBCFG) CanIf_HrhRef[CANIF_HRH_NUMBER] =
{
    {
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x767,/*The parameter refers to a particular HRH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HRH type*/
	},
    {
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x7DF,/*The parameter refers to a particular HRH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HRH type*/
	},
    {
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x7A0,/*The parameter refers to a particular HRH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HRH type*/
	},
};




CONST(CanIfHrhCfgType,CANIF_CONST) CanIf_HrhCfgData[CANIF_HRH_NUMBER] =
{
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HRH belongs to*/
		0,/*CanIfHrhCanIdRangeNum*/
		NULL_PTR,/*CanIfHrhCanIdRange*/
		&CanIf_HrhRef[0],/*from Can Driver configuration*/
	},
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HRH belongs to*/
		0,/*CanIfHrhCanIdRangeNum*/
		NULL_PTR,/*CanIfHrhCanIdRange*/
		&CanIf_HrhRef[1],/*from Can Driver configuration*/
	},
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HRH belongs to*/
		0,/*CanIfHrhCanIdRangeNum*/
		NULL_PTR,/*CanIfHrhCanIdRange*/
		&CanIf_HrhRef[2],/*from Can Driver configuration*/
	},	
};


CONST(CanIfHthIdSymRefType,CANIF_CONST_PBCFG) CanIf_HthRef[CANIF_HTH_NUMBER] =
{
    {
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x76F,/*The parameter refers to a particular HTH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HTH type*/
	},
    {
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B0,/*The parameter refers to a particular HTH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HTH type*/
	},
    {
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B1,/*The parameter refers to a particular HTH object in the CanDrv configuration*/
		CANIF_FULL_CAN,/*Defines the HTH type*/
	},
};


CONST(CanIfHthCfgType,CANIF_CONST) CanIf_HthCfgData[CANIF_HTH_NUMBER] =
{
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HTH belongs to*/
		&CanIf_HthRef[0],/*from Can Driver configuration*/
	},
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HTH belongs to*/
		&CanIf_HthRef[1],/*from Can Driver configuration*/
	},
    {
		CANIF_CANDRV_0_CanIfCtrlCfg_0,/* Reference to controller Id to which the HTH belongs to*/
		&CanIf_HthRef[2],/*from Can Driver configuration*/
	},
};


CONST(CanIf_InitHohCfgType,CANIF_CONST) CanIf_InitHohCfgData =
{
    CanIf_HrhCfgData,
    CanIf_HthCfgData,
};


CONST(CanIfRxPduRefType,CANIF_CONST_PBCFG) CanIf_RxPduRef[CANIF_RXPDU_NUMBER] =
{
   {
		CANTP_CanIf_Pdu_767_Diag_Phy_Rx,/*the up layer pdu id.*/
        0   /*RxMetaDataLength*/ 
	},
   {
		CANTP_CanIf_Pdu_7DF_Diag_Fun_Rx,/*the up layer pdu id.*/
        0   /*RxMetaDataLength*/ 
	},
   {
		XCP_XCP_CanIf_7A0_Rx_CMD,/*the up layer pdu id.*/
        0   /*RxMetaDataLength*/ 
	},
};




CONST(CanIf_RxPduConfigType,CANIF_CONST) CanIf_RxPduConfigData[CANIF_RXPDU_NUMBER] =
{
    {
        CANIF_RXPDU_CanIf_Pdu_767_Diag_Phy_Rx,/*RxPduId*/
        0x767,/*CanIfRxPduCanId*/
        0x7ff,/*CanIfRxPduCanIdMask*/
        CANIF_RX_STANDARD_CAN,/*CanIfRxPduCanIdType*/
        8,/*CanIfRxPduDlc*/
        CANIF_HOH0_HRH_0,/*CanIfRxPduHrhId*/
        FALSE,/*CanIfRxPduForNM*/
        &CanTp_RxIndication,/*User_RxIndication*/
        &CanIf_RxPduRef[0],/*from Ecu configuration*/
        0x0,/*CanIfRxPduCanIdRangeLowerCanId*/
        0x7ff,/*CanIfRxPduCanIdRangeUpperCanId*/
	},
    {
        CANIF_RXPDU_CanIf_Pdu_7DF_Diag_Fun_Rx,/*RxPduId*/
        0x7df,/*CanIfRxPduCanId*/
        0x7ff,/*CanIfRxPduCanIdMask*/
        CANIF_RX_STANDARD_CAN,/*CanIfRxPduCanIdType*/
        8,/*CanIfRxPduDlc*/
        CANIF_HOH0_HRH_1,/*CanIfRxPduHrhId*/
        FALSE,/*CanIfRxPduForNM*/
        &CanTp_RxIndication,/*User_RxIndication*/
        &CanIf_RxPduRef[1],/*from Ecu configuration*/
        0x0,/*CanIfRxPduCanIdRangeLowerCanId*/
        0x7ff,/*CanIfRxPduCanIdRangeUpperCanId*/
	},
    {
        CANIF_RXPDU_XCP_CanIf_7A0_Rx_CMD,/*RxPduId*/
        0x7a0,/*CanIfRxPduCanId*/
        0x7ff,/*CanIfRxPduCanIdMask*/
        CANIF_RX_STANDARD_CAN,/*CanIfRxPduCanIdType*/
        8,/*CanIfRxPduDlc*/
        CANIF_HOH0_HRH_2,/*CanIfRxPduHrhId*/
        FALSE,/*CanIfRxPduForNM*/
        &Xcp_CanIfRxIndication,/*User_RxIndication*/
        &CanIf_RxPduRef[2],/*from Ecu configuration*/
        0x0,/*CanIfRxPduCanIdRangeLowerCanId*/
        0x7ff,/*CanIfRxPduCanIdRangeUpperCanId*/
	},
};


CONST(CanIfTxPduRefType,CANIF_CONST_PBCFG) CanIf_TxPduRef[CANIF_TXPDU_NUMBER] =
{
   {
        CANTP_CanIf_Pdu_76F_Diag_Respone_Tx,/*the up layer pdu id.*/
        0,/*TxMetaDataLength*/
        8,/*CanIfTxPduDlc*/
    },
   {
        XCP_XCP_CanIf_7B0_Tx_RSP,/*the up layer pdu id.*/
        0,/*TxMetaDataLength*/
        8,/*CanIfTxPduDlc*/
    },
   {
        XCP_XCP_CanIf_7B1_Tx_DAQ,/*the up layer pdu id.*/
        0,/*TxMetaDataLength*/
        8,/*CanIfTxPduDlc*/
    },
};


CONST(CanIf_TxPduConfigType,CANIF_CONST) CanIf_TxPduConfigData[CANIF_TXPDU_NUMBER] =
{
    {
		CANIF_TXPDU_CanIf_Pdu_76F_Diag_Respone_Tx,/*TxPduId*/
		0x76F,/*CanIfTxPduCanId*/
		0x7ff,/*CanIfTxPduCanIdMask*/
		CANIF_TX_STANDARD_CAN,/*CanIfTxPduCanIdType*/
		CANIF_HOH0_HTH_0,/*CanIfTxPduHrhId*/
        0xffff,/*TxBufferIdndex*/                
        CANID_STATIC,/*CanIfTxPduType*/
        0xffff,  /*CanIfDynamicCanIdIndex*/
        &CanTp_TxConfirmation,/*User_TxConfirmation*/
		&CanIf_TxPduRef[0],/*from Ecu configuration*/
	},
    {
		CANIF_TXPDU_XCP_CanIf_7B0_Tx_RSP,/*TxPduId*/
		0x7b0,/*CanIfTxPduCanId*/
		0x7ff,/*CanIfTxPduCanIdMask*/
		CANIF_TX_STANDARD_CAN,/*CanIfTxPduCanIdType*/
		CANIF_HOH0_HTH_1,/*CanIfTxPduHrhId*/
        0xffff,/*TxBufferIdndex*/                
        CANID_STATIC,/*CanIfTxPduType*/
        0xffff,  /*CanIfDynamicCanIdIndex*/
        &Xcp_CanIfTxConfirmation,/*User_TxConfirmation*/
		&CanIf_TxPduRef[1],/*from Ecu configuration*/
	},
    {
		CANIF_TXPDU_XCP_CanIf_7B1_Tx_DAQ,/*TxPduId*/
		0x7b1,/*CanIfTxPduCanId*/
		0x7ff,/*CanIfTxPduCanIdMask*/
		CANIF_TX_STANDARD_FD_CAN,/*CanIfTxPduCanIdType*/
		CANIF_HOH0_HTH_2,/*CanIfTxPduHrhId*/
        0xffff,/*TxBufferIdndex*/                
        CANID_STATIC,/*CanIfTxPduType*/
        0xffff,  /*CanIfDynamicCanIdIndex*/
        &Xcp_CanIfTxConfirmation,/*User_TxConfirmation*/
		&CanIf_TxPduRef[2],/*from Ecu configuration*/
	},
};

CONST(uint8,CANIF_CONST) CanIf_TxBufferSizeCfg[CANIF_TXBUFFER_NUMBER];





CONST(CanIf_ConfigType,CANIF_CONST) CanIf_InitCfgSet =
{
    NULL_PTR,
	CanIf_CtrlCfgData,
    &CanIf_InitHohCfgData,
    CanIf_RxPduConfigData,
    CanIf_TxPduConfigData,
    NULL_PTR,
};



/*=======[E N D   O F   F I L E]==============================================*/
