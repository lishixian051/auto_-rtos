
/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : CanIf_Cfg.h                                                 **
**                                                                            **
**  Created on  : 2020/03/11 10:18:05                                                                                                                                                                                                **
**  Author      : yutao                                                       **
**  Vendor      :                                                             **
**  DESCRIPTION : cfg parameter declaration of CanIf                          **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef CANIF_CFG_H
#define CANIF_CFG_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define CANIF_CFG_H_AR_MAJOR_VERSION    4
#define CANIF_CFG_H_AR_MINOR_VERSION    2
#define CANIF_CFG_H_AR_PATCH_VERSION    2
#define CANIF_CFG_H_SW_MAJOR_VERSION    1
#define CANIF_CFG_H_SW_MINOR_VERSION    0
#define CANIF_CFG_H_SW_PATCH_VERSION    0

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
/*CanIfPublicCddHeaderFile(Complex Driver):
 Defines header files for callback functions which shall be included in
case of CDDs. Range of characters is 1.. 32.*/

/*******************************************************************************
**                      Macros                                                **
*******************************************************************************/
/* define CanIf Software Filter Type */
#define CANIF_SOFTWARE_FILTER_BINARY                0
#define CANIF_SOFTWARE_FILTER_INDEX                 1
#define CANIF_SOFTWARE_FILTER_LINEAR                2
#define CANIF_SOFTWARE_FILTER_TABLE                 3

/*defines if the buffer element length shall be fixed to 8 Bytes for buffers to which only PDUs < 8 Bytes are assigned*/
#define CANIF_FIXED_BUFFER            STD_ON

/*Selects whether the DLC check is supported*/
#define CANIF_PRIVATE_DLC_CHECK       STD_ON

/*Selects the desired software filter mechanism for reception only*/
#define CANIF_SOFTWARE_FILTER_TYPE    CANIF_SOFTWARE_FILTER_BINARY

/*Defines whether TTCAN(Time Trigger Can) is supported*/
#define CANIF_SUPPORT_TTCAN           STD_OFF

/*Enable support for dynamic ID handling using L-SDU MetaData*/
#define CANIF_META_DATA_SUPPORT       STD_OFF

/*Enable/disable dummy API(CanIf_CancelTransmit) for upper layer modules */
#define CANIF_PUBLIC_CANCEL_TRANSMIT_SUPPORT       STD_OFF

/*Switches the Default Error Tracer (Det) detection and notification ON or OFF.*/
#define CANIF_PUBLIC_DEV_ERROR_DETECT       STD_OFF

/*Selects support of Pretended Network features in CanIf,API:CanIf_SetIcomConfiguration*/
#define CANIF_PUBLIC_ICOM_SUPPORT          STD_OFF

/*Selects support for multiple CAN Drivers*/
#define CANIF_PUBLIC_MULTIPLE_DRV_SUPPORT       STD_ON

/*Selects support of Partial Network features in CanIf*/
#define CANIF_PUBLIC_PN_SUPPORT            STD_OFF

/*Enables / Disables the API CanIf_ReadRxPduData() for reading received L-SDU data.*/
#define CANIF_PUBLIC_READ_RX_PDU_DATA_API            STD_OFF

/*Enables and disables the API for reading the notification status of receive L-PDUs.*/
#define CANIF_PUBLIC_READ_RX_PDU_NOTIFY_STATUS_API            STD_OFF

/*Enables and disables the API for reading the notification status of transmit L-PDUs.*/
#define CANIF_PUBLIC_READ_TX_PDU_NOTIFY_STATUS_API            STD_OFF

/*Enables and disables the API for reconfiguration of the CAN Identifier for each Transmit L-PDU*/
#define CANIF_PUBLIC_SET_DYNAMIC_TX_ID_API            STD_ON

/*Enables and disables the buffering of transmit L-PDUs (rejected by the CanDrv) within the CAN Interface module*/
#define CANIF_PUBLIC_TX_BUFFERING            STD_OFF

/*Configuration parameter to enable/disable the API to poll for Tx Confirmation state*/
#define CANIF_PUBLIC_TX_CONFIRM_POLLING_SUPPORT            STD_ON

/*Enables and disables the API for reading the version information about the CAN Interface*/
#define CANIF_PUBLIC_VERSION_INFO_API            STD_ON

/*If enabled, only NM messages shall validate a detected wake-up event
in CanIf. If disabled, all received messages corresponding to a
configured Rx PDU shall validate such a wake-up event. This
parameter depends on CanIfPublicWakeupCheckValidSupport and
shall only be configurable, if it is enabled*/
#define CANIF_PUBLIC_WAKEUP_CHECK_VALID_BY_NM              STD_OFF

/*Selects support for wake up validation*/
#define CANIF_PUBLIC_WAKEUP_CHECK_VALID_SUPPORT            STD_OFF

/*enable/disable the CanIf_SetBaudrate API to change the baud rate of a CAN Controller*/
#define CANIF_SET_BAUDRATE_API                              STD_OFF

/*Enables the CanIf_TriggerTransmit API at Pre-Compile-Time*/
#define CANIF_TRIGGER_TRANSMIT_SUPPORT            STD_OFF

/*Determines wether TxOffLineActive feature is supported by CanIf*/
#define CANIF_TX_OFFLINE_ACTIVE_SUPPORT           STD_OFF

/*Enables the CanIf_CheckWakeup API at Pre-Compile-Time.
Therefore, this parameter defines if there shall be support for wake-up.*/
#define CANIF_WAKE_UP_SUPPORT                     STD_OFF
/*******************************************************************************************/

#define CANIF_RXPDU_NUMBER              3
#define CANIF_RXPDU_CanIf_Pdu_767_Diag_Phy_Rx         0
#define CANIF_RXPDU_CanIf_Pdu_7DF_Diag_Fun_Rx         1
#define CANIF_RXPDU_XCP_CanIf_7A0_Rx_CMD         2

#define CANIF_TXPDU_NUMBER              3
#define CANIF_DYNAMIC_TXPDU_NUMBER      0
#define CANIF_TXPDU_CanIf_Pdu_76F_Diag_Respone_Tx         0
#define CANIF_TXPDU_XCP_CanIf_7B0_Tx_RSP         1
#define CANIF_TXPDU_XCP_CanIf_7B1_Tx_DAQ         2

#define CANIF_HRH_NUMBER           3
#define CANIF_HOH0_HRH_0           0
#define CANIF_HOH0_HRH_1           1
#define CANIF_HOH0_HRH_2           2

#define CANIF_HTH_NUMBER           3
#define CANIF_HOH0_HTH_0           0
#define CANIF_HOH0_HTH_1           1
#define CANIF_HOH0_HTH_2           2


/*CanIfBufferSize!=0 TxBuffer num*/
#define CANIF_TXBUFFER_NUMBER          0
#define CANIF_RXBUFFER_NUMBER          0

#define CANIF_RXNOTIFYSTATUS_BUFFER    0
#define CANIF_TXNOTIFYSTATUS_BUFFER    0

#define CANIF_CANDRIVER_NUMBER      1
#define CANIF_CAN    0 
#define CANIF_CANCONTROLLER_NUMBER      1
#define CANIF_CANDRV_0_CanIfCtrlCfg_0    0 

#define CANIF_TRCVDRIVER_NUMBER      0
#define CANIF_TRCV_NUMBER      0

#endif
/*******************************************************************************
**                      End of file                                           **
*******************************************************************************/