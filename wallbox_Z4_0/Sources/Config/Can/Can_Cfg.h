/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Can_Cfg.h>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-06 15:32:08>
 */
/*============================================================================*/

#ifndef CAN_CFG_H
#define CAN_CFG_H
 
/*=======[M A C R O S]========================================================*/


/*=======[V E R S I O N  I N F O R M A T I O N]===============*/
#define CAN_CFG_H_AR_MAJOR_VERSION   		4u
#define CAN_CFG_H_AR_MINOR_VERSION  		2u
#define CAN_CFG_H_AR_PATCH_VERSION  		2u
#define CAN_CFG_H_SW_MAJOR_VERSION   		1u
#define CAN_CFG_H_SW_MINOR_VERSION   		0u
#define CAN_CFG_H_SW_PATCH_VERSION   		0u

/* ==================================CanGeneral===================================*/
/* ECUC_Can_00064 */
/* public error detection */
#define CAN_DEV_ERROR_DETECT                STD_ON

#define CAN_WAKEUP_SUPPORT                  STD_OFF    /* wakeup */

/* Can controller id */
#ifndef CAN_CONTROLLER_0
	#define CAN_CONTROLLER_0                0u
#endif

#ifndef CAN_CONTROLLER_HW_0
	#define CAN_CONTROLLER_HW_0             0u
#endif

#define CAN_CONTROLLER0_BUSOFF_INTERRUPT    STD_ON
#define CAN_CONTROLLER0_RX_INTERRUPT        STD_ON
#define CAN_CONTROLLER0_TX_INTERRUPT        STD_ON

#define CAN_BUSOFF_POLLING                  STD_OFF
#define CAN_WAKEUP_POLLING                  STD_OFF
#define CAN_RX_POLLING                      STD_OFF
#define CAN_TX_POLLING                      STD_OFF

#define CAN_MAX_CONTROLLERS                 1u

#define CAN_CONTROLLER0_MAX_BAUDRATE_CFG    1u

#define CAN_CONTROLLER0_BAUDRATE_CFG_0      0u

#define CAN_MAX_BAUDRATE_CFG                1u

#define CAN_CONTROLLER0_MAX_OBJECT          6u

#define CAN_CONTROLLER_0_CANOBJECTID_RX_0x767              0u
#define CAN_CONTROLLER_0_CANOBJECTID_RX_0x7DF              1u
#define CAN_CONTROLLER_0_CANOBJECTID_RX_0x7A0              2u
#define CAN_CONTROLLER_0_CANOBJECTID_TX_0x76F              3u
#define CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B0              4u
#define CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B1              5u

#define CAN_MAX_OBJECT                      6u

/* ECUC_Can_00482 */
/* The support of the Can_SetBaudrate API is optional. */
#define CAN_SET_BAUDRATE_API		        STD_OFF

/* ECUC_Can_00106 */
/* Version Info Get API */
#define CAN_VERSION_INFO_API                STD_OFF

/* ECUC_Can_00466 */
#define	CanWakeupFunctionalityAPI			STD_OFF

#endif /* #define CAN_CFG_H */

/*=======[E N D   O F   F I L E]==============================================*/