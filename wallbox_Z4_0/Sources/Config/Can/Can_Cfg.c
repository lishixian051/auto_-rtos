/*============================================================================*/
/*  Copyright (C) 2019,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Can_Cfg.c>
 *  @brief      <>
 *  
 *  <MCU:MPC5746C>
 *  
 *  @author     <>
 *  @date       <2020-03-06 15:32:08>
 */
/*============================================================================*/

/*=====[R E V I S I O N   H I S T O R Y]====================*/

/*====================================================*/
/*=======[V E R S I O N  I N F O R M A T I O N]===============*/
#define CAN_CFG_C_AR_MAJOR_VERSION   4
#define CAN_CFG_C_AR_MINOR_VERSION   2
#define CAN_CFG_C_AR_PATCH_VERSION   2
#define CAN_CFG_C_SW_MAJOR_VERSION   1
#define CAN_CFG_C_SW_MINOR_VERSION   0
#define CAN_CFG_C_SW_PATCH_VERSION   0

/*=======[I N C L U D E S]================================*/
#include "Can_Cfg.h"
#include "Can_GeneralTypes.h"
/*=======[V E R S I O N  C H E C K]============================*/
/*Check version information with Can_Types.h*/
#if(CAN_CFG_C_AR_MAJOR_VERSION != CAN_TYPES_H_AR_MAJOR_VERSION)
	#error "Can_Cfg.c : Mismatch in Specification Major Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_AR_MINOR_VERSION != CAN_TYPES_H_AR_MINOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Specification Minor Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_AR_PATCH_VERSION != CAN_TYPES_H_AR_PATCH_VERSION)
  #error "Can_Cfg.c : Mismatch in Specification Patch Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_SW_MAJOR_VERSION != CAN_TYPES_H_SW_MAJOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Software Implementation Major Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_SW_MINOR_VERSION != CAN_TYPES_H_SW_MINOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Software Implementation Minor Version with Can_Cfg.h"
#endif
/*Check version information with Can_Cfg.h*/
#if(CAN_CFG_C_AR_MAJOR_VERSION != CAN_CFG_H_AR_MAJOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Specification Major Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_AR_MINOR_VERSION != CAN_CFG_H_AR_MINOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Specification Minor Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_AR_PATCH_VERSION != CAN_CFG_H_AR_PATCH_VERSION)
  #error "Can_Cfg.c : Mismatch in Specification Patch Version with Can_Cfg.h"
#endif
#if(CAN_CFG_C_SW_MAJOR_VERSION != CAN_CFG_H_SW_MAJOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Software Implementation Major Version with Can_Cfg.h"
#endif
#if (CAN_CFG_C_SW_MINOR_VERSION != CAN_CFG_H_SW_MINOR_VERSION)
  #error "Can_Cfg.c : Mismatch in Software Implementation Minor Version with Can_Cfg.h"
#endif
/*=======[I N T E R N A L   D A T A]=======================*/
static CONST(Can_ControllerBaudrateConfigType, CAN_CONST) Can_ControllerBaudrateConfig[CAN_MAX_BAUDRATE_CFG] =
{
    { 
		0u,				    /* BaudRateConfigID*/
		500u,			    /* BaudRate */
		0x04610007u,        /* CanControllerBaudRateReg */
		{
			FALSE,          /* CanControllerFdEnable */
			500u,		    /* CanControllerFdBaudRate */		
			0x00431ce3u,    /* CanControllerFdCbtRegister */
			FALSE,          /* TxBitRateSwitch */
			FALSE,          /* CanControllerTDCEnable */
			0u,				/* TrcvDelayCompensationOffset */
		}
    }
};

static CONST(Can_ControllerConfigType, CAN_CONST) Can_Controller_Config[CAN_MAX_CONTROLLERS] =
{
	{
		0xffec0000u,		                            /* Can_Controller_0_BaseAddr */
		CAN_CONTROLLER_0,                               /* CanControllerId */
		CAN_CONTROLLER0_MAX_OBJECT,                     /* CanControllerMaxCounter */
		STD_ON,                                         /* CanControllerActivation */
		CAN_PROCESSING_INTERRUPT,                       /* CanBusoffProcessing */
		CAN_PROCESSING_INTERRUPT,                       /* CanRxProcessing */
		CAN_PROCESSING_INTERRUPT,                       /* CanTxProcessing */
        #if(STD_ON == CAN_WAKEUP_SUPPORT) 
			CAN_PROCESSING_INTERRUPT,                   /* CanWakeupProcessing */
			0u,                 						/* CanWakeupSourceRef */
		#endif                                          
		&Can_ControllerBaudrateConfig[CAN_CONTROLLER0_BAUDRATE_CFG_0], 				/* Can_ControllerBaudrateConfig */
		&Can_ControllerBaudrateConfig[CAN_CONTROLLER0_BAUDRATE_CFG_0],              /* Can_ControllerBaudrateDefaultConfig */
		FALSE,                                          /* CanFD_Support */
		CAN_8_BYTES_PAYLOAD,                            /* CanControllerPayload */
		95u,                                            /* CanControllerMaxMessageBuffer */
		0u,                                             /* CanRxStart */                                              
		2u,                                             /* CanRxEnd */
		3u,                                             /* CanTxStart */
		5u                                              /* CanTxEnd */
	}
};

const Can_HardwareObjectType Can_Controller_HardwareObject[CAN_MAX_OBJECT] =
{
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x767u,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x767,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_RECEIVE,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		1u,                                         	/* CanHwObjectCount */
	},
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x7DFu,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x7DF,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_RECEIVE,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		3u,                                         	/* CanHwObjectCount */
	},
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x7A0u,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_RX_0x7A0,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_RECEIVE,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		4u,                                         	/* CanHwObjectCount */
	},
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x76Fu,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x76F,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_TRANSMIT,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		2u,                                         	/* CanHwObjectCount */
	},
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x7B0u,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B0,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_TRANSMIT,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		5u,                                         	/* CanHwObjectCount */
	},
	{
		CAN_HANDLE_TYPE_FULL,                      	    /* CanHandleType */
		CAN_ID_TYPE_STANDARD,							/* CanIdType */
		0x7B1u,											/* CanIdValue */
		CAN_CONTROLLER_0_CANOBJECTID_TX_0x7B1,			/* CanObjectId */
		0x7ffu,                                    /* CanFilterMask */
		CAN_OBJECT_TYPE_TRANSMIT,                    	/* CanObjectType */
		CAN_CONTROLLER_0,                           	/* CanControllerRef */
		6u,                                         	/* CanHwObjectCount */
	}
};

const Can_ConfigType Can_ConfigSet =
{
	Can_Controller_Config,
	Can_Controller_HardwareObject
};
