
/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_Cfg.h                                                  **
**                                                                            **
**  Created on  : 2020/02/28 11:15:53                                         
**  Author      : yutao                                                       **
**  Vendor      :                                                             **
**  DESCRIPTION : cfg parameter declaration of PDUR                           **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


#ifndef  PDUR_CFG_H
#define  PDUR_CFG_H

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/

/*******************************************************************************
**                      Version Information                                   **
*******************************************************************************/
#define PDUR_CFG_H_AR_MAJOR_VERSION  4U
#define PDUR_CFG_H_AR_MINOR_VERSION  2U
#define PDUR_CFG_H_AR_PATCH_VERSION  2U
#define PDUR_CFG_H_SW_MAJOR_VERSION  1U
#define PDUR_CFG_H_SW_MINOR_VERSION  0U
#define PDUR_CFG_H_SW_PATCH_VERSION  0U
/*******************************************************************************
**                      Macros                                                **
*******************************************************************************/
/* Zero cost operation only support */
#define PDUR_ZERO_COST_OPERATION        STD_ON
#define PDUR_CANIF_SUPPORT      STD_ON
#define PDUR_CANTP_SUPPORT      STD_ON
#define PDUR_J1939TP_SUPPORT    STD_OFF
#define PDUR_COM_SUPPORT        STD_OFF
#define PDUR_DCM_SUPPORT        STD_ON
#define PDUR_J1939DCM_SUPPORT   STD_OFF
#define PDUR_IPDUM_SUPPORT      STD_OFF
#define PDUR_J1939RM_SUPPORT    STD_OFF
#define PDUR_LDCOM_SUPPORT      STD_OFF
#define PDUR_SECOC_SUPPORT      STD_OFF
#define PDUR_DBG_SUPPORT        STD_OFF
#define PDUR_CANNM_SUPPORT      STD_OFF
#define PDUR_OSEKNM_SUPPORT      STD_OFF
#define PDUR_LINIF_SUPPORT      STD_OFF
#define PDUR_LINTP_SUPPORT      STD_OFF
#define PDUR_FRIF_SUPPORT       STD_OFF
#define PDUR_FRNM_SUPPORT       STD_OFF
#define PDUR_DOIP_SUPPORT         STD_OFF
#define PDUR_SOAD_SUPPORT         STD_OFF
#define PDUR_UDPNM_SUPPORT         STD_OFF

#endif  /* end of PDUR_CFG_H */

/*******************************************************************************
**                      End of file                                           **
*******************************************************************************/

