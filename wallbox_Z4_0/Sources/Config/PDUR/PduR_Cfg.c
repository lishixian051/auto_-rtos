
/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_Cfg.c                                                  **
**                                                                            **
**  Created on  : 2020/02/28 11:15:53                                         
**  Author      : yutao                                                       **
**  Vendor      :                                                             **
**  DESCRIPTION : parameter declaration of PDUR                               **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/


/*******************************************************************************
**                      Version Information                                   **
*******************************************************************************/
#define PDUR_CFG_C_AR_MAJOR_VERSION  4U
#define PDUR_CFG_C_AR_MINOR_VERSION  2U
#define PDUR_CFG_C_AR_PATCH_VERSION  2U
#define PDUR_CFG_C_SW_MAJOR_VERSION  1U
#define PDUR_CFG_C_SW_MINOR_VERSION  0U
#define PDUR_CFG_C_SW_PATCH_VERSION  0U

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "PduR.h"
/*******************************************************************************
**                      Version Check                                         **
*******************************************************************************/
#if (PDUR_CFG_C_AR_MAJOR_VERSION != PDUR_CFG_H_AR_MAJOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_CFG_C_AR_MINOR_VERSION != PDUR_CFG_H_AR_MINOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_CFG_C_AR_PATCH_VERSION != PDUR_CFG_H_AR_PATCH_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_CFG_C_SW_MAJOR_VERSION != PDUR_CFG_H_SW_MAJOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_CFG_C_SW_MINOR_VERSION != PDUR_CFG_H_SW_MINOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif

/*******************************************************************************
**                      Macros                                                **
*******************************************************************************/

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/

/*******************************************************************************
**                      End of file                                           **
*******************************************************************************/

