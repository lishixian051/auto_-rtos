/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dem_OBD.c                                                   **
**                                                                            **
**  Created on  :                                                             **
**  Author      : tao.yu                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : API definitions of DEM for OBD                              **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>                         */
/*  V1.0.0       2018-4-20  tao.yu    Initial version                         */
/*  V1.0.1       2019-9-17  tao.yu    fix some bug,change event callback      */
/*  V1.0.2       2019-12-25  tao.yu    QAC fix     							  */
/*  V1.0.3       2020-1-7   tao.yu    Commercial project problem modification */
/*******************************************************************************
**                       Version  information                                 **
*******************************************************************************/
#define DEM_OBD_C_AR_MAJOR_VERSION  4
#define DEM_OBD_C_AR_MINOR_VERSION  2
#define DEM_OBD_C_AR_PATCH_VERSION  2
#define DEM_OBD_C_SW_MAJOR_VERSION  1
#define DEM_OBD_C_SW_MINOR_VERSION  0
#define DEM_OBD_C_SW_PATCH_VERSION  3

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Dem_Internal.h"
#include "Dem.h"
#include "Dem_Dcm.h"
#if(STD_ON == DEM_TRIGGER_FIM_REPORTS)
#include "FiM.h"
#endif

/*******************************************************************************
**                       Version  Check                                       **
*******************************************************************************/
#if (DEM_OBD_C_AR_MAJOR_VERSION != DEM_INTERNAL_H_AR_MAJOR_VERSION)
  #error "Dem_OBD.c : Mismatch in Specification Major Version"
#endif
#if (DEM_OBD_C_AR_MINOR_VERSION != DEM_INTERNAL_H_AR_MINOR_VERSION)
  #error "Dem_OBD.c : Mismatch in Specification Major Version"
#endif
#if (DEM_OBD_C_AR_PATCH_VERSION != DEM_INTERNAL_H_AR_PATCH_VERSION)
  #error "Dem_OBD.c : Mismatch in Specification Major Version"
#endif
#if (DEM_OBD_C_SW_MAJOR_VERSION != DEM_INTERNAL_H_SW_MAJOR_VERSION)
  #error "Dem_OBD.c : Mismatch in Specification Major Version"
#endif
#if (DEM_OBD_C_SW_MINOR_VERSION != DEM_INTERNAL_H_SW_MINOR_VERSION)
  #error "Dem_OBD.c : Mismatch in Specification Major Version"
#endif

/*******************************************************************************
**                      macros                                                **
*******************************************************************************/


/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
Dem_IndicatorStatusType OBDMilStatus;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDDistanceMILOn;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDTimeMILOn;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDTimeDTCClear;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 DistSinceDtcCleared;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 ContinuousMICounter = 0;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#if(DEM_OBD_SUPPORT == DEM_OBD_MASTER_ECU)
#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 MasterContinuousMICounter = 0;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
#endif

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint8 WarmUpCycleCounter;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16  IgnUpCycleCounter;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
boolean CurrentPTOStatus;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDB1Counter = 0;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDDistanceMILLastOn;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDTimeMILLastOn;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDDistanceDTCClear;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
uint16 OBDTimeDTCLastClear;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
#if(DEM_RATIO_NUM > 0)
#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
IUMPRType IUMPRValue[DEM_RATIO_NUM];
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
#endif
#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
static Dem_IumprDenomCondStatusType ConditionStatu[5] = {3,3,3,3,3};
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#if (DEM_DTR_NUM > 0)
#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
static DTRInfoType DTRInfo[DEM_DTR_NUM];
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
#endif
#endif

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
static boolean SetDataOfPid21;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
static boolean PFCStatu = FALSE;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
/*******************************************************************************
**                      Private Function Definitions                         **
*******************************************************************************/
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)

#if(DEM_RATIO_NUM > 0)
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_CalIUMPRValue(Dem_RatioIdType Dem_RatioId);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_CalIUMPRDenValue(Dem_RatioIdType Dem_RatioId);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_CalIUMPRNumValue(Dem_RatioIdType Dem_RatioId);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(uint16, DEM_CODE) Dem_GetIUMPRDenValueByGroup(Dem_IUMPRGroupType Dem_IUMPRGroup);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(uint16, DEM_CODE) Dem_GetIUMPRNumValueByGroup(Dem_IUMPRGroupType Dem_IUMPRGroup);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif
#endif

#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(P2VAR(Dem_EventMemEntryType, AUTOMATIC, DEM_VAR), DEM_CODE) Dem_SelectOBDFreezeFrame(
		P2VAR(Dem_EventIdType,AUTOMATIC, DEM_APPL_DATA) IntId);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif

#if((DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT) || (DEM_J1939_SUPPORT == STD_ON))
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_B1CounterProcess(void);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif

/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*************************************************************************/
/*
 * Brief               Init the OBD related data.
 * ServiceId           Internal Function
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_OBDInit(void)
{
    WarmUpCycleCounter = 0;
    /*distance related*/
    OBDDistanceMILOn = 0;
    OBDDistanceDTCClear = 0;
    DistSinceDtcCleared = 0;
    OBDDistanceMILLastOn = 0;
    /*time related*/
    OBDTimeMILOn = 0;
    OBDTimeDTCClear = 0;
    OBDTimeDTCLastClear = 0;
    OBDTimeMILLastOn = 0;
    SetDataOfPid21 = FALSE;
    PFCStatu = FALSE;
    OBDMilStatus = DEM_INDICATOR_OFF;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"





/*************************************************************************/
/*
 * Brief               Update the Current OBD Mil Status.
 * ServiceId           Internal Function
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_UpdateOBDMilStatus(uint8 indicatorRef,uint16 IntID)
{
    Dem_IndicatorStatusType currentMilStatus = DEM_INDICATOR_OFF;
    uint16 CurrentDistance = 0;
    uint16 CurrentTime = 0;
    static Dem_IndicatorStatusType lastMilStatus = DEM_INDICATOR_OFF;

    if(indicatorRef == DEM_MALFUNCTION_LAMP_INDICATOR)
    {
#if(DEM_INDICATOR_NUM > 0)
        currentMilStatus = DemWIRState[indicatorRef];
#endif
    }
    else
    {
        return;
    }
    if(currentMilStatus != DEM_INDICATOR_OFF)
    {
        /* MIL recently activated */
        if (((DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemDtcValue < 0xFFFF33UL)
                && (DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemDtcValue != 0UL))
                || (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemObdDTCRef].DemDtcValue != 0U))
        {/*SWS_Dem_01139] */
            OBDMilStatus = currentMilStatus;
        }
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
        /* Read current distance information */
        if (lastMilStatus == DEM_INDICATOR_OFF)
		{
			OBDDistanceMILLastOn = Dem_ReadDistanceInformation();
			OBDTimeMILLastOn = Dem_ReadTimeInformation();
        }
#endif
    }
    else
    {
        /* MIL recently de-activated */
        if (((DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemDtcValue < 0xFFFF33UL)
                && (DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemDtcValue != 0UL))
                || (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[IntID].DemDTCRef].DemObdDTCRef].DemDtcValue != 0U))
        {/*SWS_Dem_01139] */
            OBDMilStatus = currentMilStatus;
        }
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
        CurrentDistance = Dem_ReadDistanceInformation();
#endif
        if (CurrentDistance > OBDDistanceMILLastOn)
        {
            if ((CurrentDistance - OBDDistanceMILLastOn + OBDDistanceMILOn) < 0xFFFFu)
            {
                OBDDistanceMILOn += CurrentDistance - OBDDistanceMILLastOn;
                OBDDistanceMILLastOn = CurrentDistance;
            }
            else
            {
                OBDDistanceMILOn = 0xFFFFu;
            }
        }
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
        CurrentTime = Dem_ReadTimeInformation();
#endif
        if (CurrentTime > OBDTimeMILLastOn)
        {
            if ((CurrentTime - OBDTimeMILLastOn + OBDTimeMILOn) < 0xFFFFu)
            {
                OBDTimeMILOn += CurrentTime - OBDTimeMILLastOn;
                OBDTimeMILLastOn = CurrentTime;
            }
            else
            {
                OBDTimeMILOn = 0xFFFFu;
            }
        }
    }
    lastMilStatus = currentMilStatus;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               Clear the OBD Information on clearing dtc.
 * ServiceId           Internal Function
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_ClearOBDInfo(void)
{
    /* Clear counters if all OBD DTCs have been cleared */
    WarmUpCycleCounter = 0;
#if(DEM_OBD_SUPPORT != DEM_OBD_NO_OBD_SUPPORT)
    OBDDistanceDTCClear = Dem_ReadDistanceInformation();
    OBDTimeDTCLastClear = Dem_ReadTimeInformation();
#endif
    OBDTimeDTCClear = 0;
    ContinuousMICounter = 0;/*SWS_Dem_01146]*/
    OBDB1Counter = 0;
    OBDDistanceMILOn = 0;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"


/*************************************************************************/
/*
 * Brief               Gets the DTC Severity availability mask.
 * ServiceId           0xb2
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     DTCSeverityMask:The value DTCSeverityMask indicates the
 *                      supported DTC severity bits from the Dem.
 * Param-Name[in/out]  none
 * Return              E_OK: get of DTC severity mask was successful
                        E_NOT_OK: get of DTC severity mask failed
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(Dem_ReturnClearDTCType, DEM_CODE)Dem_DcmGetDTCSeverityAvailabilityMask(
        Dem_DTCSeverityType* DTCSeverityMask)
{
    if (DEM_STATE_INIT != DemInitState)
    {
        DEM_DET_REPORT(DEM_SID_DCMGETDTCSEVERITYAVAILABILITYMASK, DEM_E_UNINIT);
        return E_NOT_OK;
    }
    if (NULL_PTR == DTCSeverityMask)
    {
        DEM_DET_REPORT(DEM_SID_DCMGETDTCSEVERITYAVAILABILITYMASK, DEM_E_PARAM_POINTER);
        return E_NOT_OK;
    }
    *DTCSeverityMask = DemDTCFilterInfo.DTCSeverityMask;
    return E_OK;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"



/*******************************************************************************
**                      end of file                                           **
*******************************************************************************/

