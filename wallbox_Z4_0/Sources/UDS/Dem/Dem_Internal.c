/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dem_Internal.c                                              **
**                                                                            **
**  Created on  :                                                             **
**  Author      : tao.yu                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : API definitions of DEM for internal                         **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>                         */
/*  V1.0.0       2018-4-20  tao.yu    Initial version                         */
/*  V1.0.1       2019-9-17  tao.yu    fix some bug,change event callback      */
/*  V1.0.2       2019-12-25  tao.yu    QAC fix     							  */
/*  V1.0.3       2020-1-7   tao.yu    Commercial project problem modification */
/*******************************************************************************
**                       Version  information                                 **
*******************************************************************************/
#define DEM_INTERNAL_C_AR_MAJOR_VERSION   4U
#define DEM_INTERNAL_C_AR_MINOR_VERSION   2U
#define DEM_INTERNAL_C_AR_PATCH_VERSION   2U
#define DEM_INTERNAL_C_SW_MAJOR_VERSION   1U
#define DEM_INTERNAL_C_SW_MINOR_VERSION   0U
#define DEM_INTERNAL_C_SW_PATCH_VERSION   3U

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Dem_Internal.h"
#include "Dem.h"
#if(DEM_TRIGGER_DCM_REPORTS == STD_ON)
#include "Dcm.h"
#endif

/*******************************************************************************
**                       Version  Check                                       **
*******************************************************************************/
#if (DEM_INTERNAL_C_AR_MAJOR_VERSION != DEM_INTERNAL_H_AR_MAJOR_VERSION)
  #error "Dem_Internal.c : Mismatch in Specification Major Version"
#endif
#if (DEM_INTERNAL_C_AR_MINOR_VERSION != DEM_INTERNAL_H_AR_MINOR_VERSION)
  #error "Dem_Internal.c : Mismatch in Specification Major Version"
#endif
#if (DEM_INTERNAL_C_AR_PATCH_VERSION != DEM_INTERNAL_H_AR_PATCH_VERSION)
  #error "Dem_Internal.c : Mismatch in Specification Major Version"
#endif
#if (DEM_INTERNAL_C_SW_MAJOR_VERSION != DEM_INTERNAL_H_SW_MAJOR_VERSION)
  #error "Dem_Internal.c : Mismatch in Specification Major Version"
#endif
#if (DEM_INTERNAL_C_SW_MINOR_VERSION != DEM_INTERNAL_H_SW_MINOR_VERSION)
  #error "Dem_Internal.c : Mismatch in Specification Major Version"
#endif
/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
#define DEM_START_SEC_VAR_POWER_ON_INIT_UINT8
#include "Dem_MemMap.h"
VAR(uint8,AUTOMATIC) DemClearDTCLock = 2u;
#define DEM_STOP_SEC_VAR_POWER_ON_INIT_UINT8
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_POWER_ON_INIT_BOOLEAN
#include "Dem_MemMap.h"
VAR(boolean,AUTOMATIC) DemClearNonvolatile = FALSE;
#define DEM_STOP_SEC_VAR_POWER_ON_INIT_BOOLEAN
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_POWER_ON_INIT_UINT8
#include "Dem_MemMap.h"
VAR(uint8,AUTOMATIC) DemClearNonvolatileOK = 0x02u;
#define DEM_STOP_SEC_VAR_POWER_ON_INIT_UINT8
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
VAR(Dem_ClearDTCInfoType,AUTOMATIC) DemClearDTCInfo;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
VAR(InternalDataType,AUTOMATIC) InternalData;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

#define DEM_START_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"
VAR(boolean,AUTOMATIC) Dem_Pending = FALSE;
#define DEM_STOP_SEC_VAR_UNSPECIFIED
#include "Dem_MemMap.h"

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Function Definitions                         **
*******************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearAllDTC(uint8 MemDest);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#if(DEM_CLEAR_DTCLIMITATION == DEM_ALL_SUPPORTED_DTCS)
#if(DEM_GROUP_OF_DTC_NUM > 0)
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearGroupDTC(uint8 MemDest,
    uint8 GroupIndex);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif

#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearOneDTC(uint8 MemDest,
    uint16 DTCIndex);
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
/*************************************************************************/
/*
 * Brief               MemSet
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      Val && Size
 * Param-Name[out]     none
 * Param-Name[in/out]  Dest
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_MemSet(
    P2VAR(uint8, AUTOMATIC, DEM_VAR) Dest,
    uint8 Val,
    uint32 Size)
{
    while (Size > 0UL)
    {
        *(Dest) = Val;
        Dest++;
        Size--;
    }
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               MemCopy
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      Src && Size
 * Param-Name[out]     Dest
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_MemCopy(
    P2VAR(void, AUTOMATIC, DEM_VAR) Dest,
    P2CONST(void, AUTOMATIC, DEM_VAR) Src,
    uint32 Size)
{
    uint8 * pDest = (uint8 *)Dest;
    const uint8 * pSrc = (const uint8 *)Src;
    while (Size > 0u)
    {
    	Size--;
        pDest[Size] = pSrc[Size];
    }
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               CheckCondictionFulfilled
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      cond && group && len
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              boolean
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(boolean, DEM_CODE) Dem_CheckCondictionFulfilled(
    P2CONST(uint8, AUTOMATIC, DEM_VAR) cond,
    P2CONST(uint8, AUTOMATIC, DEM_VAR) group,
    uint8 len)
{
    boolean res = TRUE;

    while ((len > 0u) && (res != FALSE))
    {
        len--;
        if (((*cond) & (*group)) != *group)
        {
            res = FALSE;
        }
        else
        {
            cond++;
            group++;
        }
    }
    return res;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#if(DEM_DTC_ATTRIBUTES_NUM > 0)
/*************************************************************************/
/*
 * Brief               Get EventDTCAttributesCfg
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      IntId
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              const Dem_DTCAttributesType*
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(P2CONST(Dem_DTCAttributesType, AUTOMATIC, DEM_CONST), DEM_CODE)
Dem_EventDTCAttributesCfg(Dem_EventIdType IntId)
{
    uint16 Ref;
    P2CONST(Dem_DTCAttributesType, AUTOMATIC, DEM_CONST) pRes = NULL_PTR;

    /* DtcRef */
    Ref = DemPbCfg.DemEventParameter[IntId].DemDTCRef;
    if (Ref != DEM_DTC_REF_INVALID)
    {
        /* DTCAttributesRef */
        Ref = DemPbCfg.DemDTC[Ref].DemDTCAttributesRef;
        pRes = &DemPbCfg.DemDTCAttributes[Ref];
    }

    return pRes;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif
/*************************************************************************/
/*
 * Brief               GetInternalMemDest
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      DTCOrigin
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              uint8
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(uint8, DEM_CODE) Dem_GetInternalMemDest(Dem_DTCOriginType DTCOrigin)
{
    uint8 iloop = 0;

    while (iloop < DEM_MEM_DEST_TOTAL_NUM)
    {
        if (DemMemDestCfg[iloop].ExtId == DTCOrigin)
        {
            return iloop;
        }
        iloop++;
    }
    return DEM_MEM_DEST_INVALID;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               BRIEF DESCRIPTION
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(Dem_EventIdType, DEM_CODE) Dem_GetEventIdByDTC(uint32 DTC)
{
    Dem_EventIdType id = 0;

    while (id < DEM_EVENT_PARAMETER_NUM)
    {
        if ((DemPbCfgPtr->DemEventParameter[id].DemDTCRef != DEM_DTC_REF_INVALID)
            && ((DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[id].DemDTCRef].DemDtcValue == DTC)
            || (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[id].DemDTCRef].DemObdDTCRef].DemJ1939DTCValue == DTC)
            || (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[DemPbCfgPtr->DemEventParameter[id].DemDTCRef].DemObdDTCRef].DemDtcValue == DTC))
            )
        {
            return id;
        }
        id++;
    }
    return DEM_EVENT_PARAMETER_INVALID;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               BRIEF DESCRIPTION
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_TriggerOnEventStatus(Dem_EventIdType IntId,
    Dem_UdsStatusByteType OldStatus,
    Dem_UdsStatusByteType NewStatus)
{
#if(DEM_CALLBACK_EVENT_STATUS_CHANGED_TOTAL_NUM > 0)
    P2CONST(Dem_EventParameterType, AUTOMATIC, DEM_CONST) pEventCfg = &DemPbCfgPtr->DemEventParameter[IntId];
    uint8 iloop = 0;
#endif
#if(DEM_CALLBACK_EVENT_STATUS_CHANGED_TOTAL_NUM > 0)
    P2CONST(Dem_TriggerOnEventStatusType, AUTOMATIC, DEM_CONST) pCbk =
        &DemCallbackEventStatusChanged[pEventCfg->StatusChangedCbkStartIndex];
#endif
#if (DEM_TRIGGER_DCM_REPORTS == STD_ON)
    uint32 DTC = 0x00;
#endif
    /*SWS_Dem_00828]*/
    if (DemDTCStatusChangedInd == TRUE)
    {
#if (DEM_TRIGGER_DCM_REPORTS == STD_ON)
        /*SWS_Dem_00284]*/
        Dem_GetEventDTC(IntId, DEM_DTC_FORMAT_UDS, &DTC);
        if (DTC != 0x00UL)
        {
            (void)Dcm_DemTriggerOnDTCStatus(DTC,OldStatus,NewStatus);
#if(DEM_CALLBACK_DTC_STATUS_CHANGED_NUM > 0)
            while(iloop < DEM_CALLBACK_DTC_STATUS_CHANGED_NUM)
            {
                if(DemCallbackDTCStatusChanged[iloop] != NULL_PTR)
                {
                    (void)(DemCallbackDTCStatusChanged[iloop])(DTC,OldStatus,NewStatus);
                }
                iloop++;
            }
#endif
        }
        /*SWS_Dem_00986]*/
        Dem_GetEventDTC(IntId, DEM_DTC_FORMAT_OBD, &DTC);
        iloop = 0;
        if (DTC != 0x00UL)
        {
#if(DEM_CALLBACK_OBD_DTC_STATUS_CHANGED_NUM > 0)
            while(iloop < DEM_CALLBACK_OBD_DTC_STATUS_CHANGED_NUM)
            {
                if(DemCallbackObdDTCStatusChanged[iloop] != NULL_PTR)
                {
                    (void)(DemCallbackObdDTCStatusChanged[iloop])(DTC,OldStatus,NewStatus);
                }
                iloop++;
            }
#endif
        }
        /*SWS_Dem_00987]*/
        Dem_GetEventDTC(IntId, DEM_DTC_FORMAT_J1939, &DTC);
        iloop = 0;
        if (DTC != 0x00UL)
        {
#if(DEM_CALLBACK_J1939_DTC_STATUS_CHANGED_NUM > 0)
            while(iloop < DEM_CALLBACK_J1939_DTC_STATUS_CHANGED_NUM)
            {
                if(DemCallbackJ1939DTCStatusChanged[iloop] != NULL_PTR)
                {
                    (void)(DemCallbackJ1939DTCStatusChanged[iloop])(DTC,OldStatus,NewStatus);
                }
                iloop++;
            }
#endif
        }
#endif
    }
    /*SWS_Dem_00016] */
#if(DEM_CALLBACK_EVENT_STATUS_CHANGED_TOTAL_NUM > 0)
    iloop = 0;
    while (iloop < pEventCfg->StatusChangedCbkNum)
    {
        if (*pCbk != NULL_PTR)
        {
            (void)(*pCbk)(IntId + 1u, OldStatus, NewStatus);
        }
        pCbk++;
        iloop++;
    }
#endif
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               ClearAllDTC
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      MemDest
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearAllDTC(uint8 MemDest)
{
    P2VAR(Dem_EventMemEntryType, AUTOMATIC, DEM_VAR) pEntry;
    uint8 iloop = 0;
    P2VAR(Dem_EventInfoType, AUTOMATIC, DEM_VAR) pEvent;
    P2VAR(Dem_ClearDTCInfoType, AUTOMATIC, DEM_VAR) pClr = &DemClearDTCInfo;
#if(DEM_J1939_NODE_NUM > 0)
    P2CONST(uint8, AUTOMATIC, DEM_VAR) Nodeindex;
    uint8 NodeNum;
    uint8 index = 0;
    uint16 dtcRef = 0;
#endif

    //while (iloop < 5)/* Test ,change iloop is 5 ;21657*/
    while (iloop < DEM_EVENT_PARAMETER_NUM)
    {
		pEvent = &DemEventInfo[iloop];
		if ((pClr->SID == DEM_SID_J1939DCMCLEARDTC)
			&& (((pClr->DTCTypeFilter == DEM_J1939DTC_CLEAR_PREVIOUSLY_ACTIVE)
					&& ((0x00u != DEM_FLAGS_ISSET(pEvent->UdsStatus, DEM_UDS_STATUS_CDTC))
					&& (0x00u == DEM_FLAGS_ISSET(pEvent->UdsStatus, DEM_UDS_STATUS_TF))
#if(DEM_INDICATOR_ATTRIBUTE_TOTAL_NUM > 0)
					&& (DemWIRState[DemIndicatorAttribute[DemPbCfgPtr->DemEventParameter[pEvent->IntId].AttrStartIndex].DemIndicatorRef] == DEM_INDICATOR_OFF)
#endif
					))
			|| ((pClr->DTCTypeFilter == DEM_J1939DTC_CLEAR_ALL)
					&& (((0x00u != DEM_FLAGS_ISSET(pEvent->UdsStatus, DEM_UDS_STATUS_CDTC))
					&& (0x00u != DEM_FLAGS_ISSET(pEvent->UdsStatus, DEM_UDS_STATUS_TF)))
#if(DEM_INDICATOR_ATTRIBUTE_TOTAL_NUM > 0)
					|| (DemWIRState[DemIndicatorAttribute[DemPbCfgPtr->DemEventParameter[pEvent->IntId].AttrStartIndex].DemIndicatorRef] != DEM_INDICATOR_OFF)
#endif
					))))
		{
#if(DEM_J1939_NODE_NUM > 0)
			index = 0;
			dtcRef = DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemDTCRef;
			Nodeindex = DemPbCfgPtr->J1939Node[DemPbCfgPtr->DemDTCAttributes[DemPbCfgPtr->DemDTC[dtcRef].DemDTCAttributesRef].DemJ1939DTC_J1939NodeRef].NodeIDRef;
			NodeNum = DemPbCfgPtr->J1939Node[DemPbCfgPtr->DemDTCAttributes[DemPbCfgPtr->DemDTC[dtcRef].DemDTCAttributesRef].DemJ1939DTC_J1939NodeRef].NodeNum;
			while (index < NodeNum)
			{
				if (pClr->node == *Nodeindex)
				{
					Dem_Clear(pEvent);/*find the node*/
					pEntry = Dem_MemEntryGet(Dem_GetEventExternalId(pEvent->IntId), MemDest);
					if (pEntry != NULL_PTR)
					{
						Dem_MemEntryDelete(pEntry,MemDest);/*SWS_Dem_00660] */
						DemMemDestInfo[MemDest].RecordNum--;
					}
					break;
				}
				index++;
				Nodeindex++;
			}
#endif
		}
		else if ((0x00u == DEM_FLAGS_ISSET(pEvent->Status, DEM_EVENT_STATUS_ENABLED_CONDICTION)))
		{
			;/*do nothing*/
		}
		else
		{
			Dem_Clear(pEvent);
			pEntry = Dem_MemEntryGet(Dem_GetEventExternalId(pEvent->IntId), MemDest);
			if (pEntry != NULL_PTR)
			{
				Dem_MemEntryDelete(pEntry,MemDest);/*SWS_Dem_00660] */
				DemMemDestInfo[MemDest].RecordNum--;
			}
		}
		iloop++;
    }
    /*SWS_Dem_00399] */
    if (DemMemDestInfo[MemDest].RecordNum == 0u)
    {
        DemMemDestInfo[MemDest].OverFlow = FALSE;
    }
    Dem_ClearOBDInfo();
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#if(DEM_CLEAR_DTCLIMITATION == DEM_ALL_SUPPORTED_DTCS)
#if(DEM_GROUP_OF_DTC_NUM > 0)
/*************************************************************************/
/*
 * Brief               ClearGroupDTC
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearGroupDTC(uint8 MemDest,
    uint8 GroupIndex)
{
    P2VAR(Dem_EventMemEntryType, AUTOMATIC, DEM_VAR) pEntry;
    uint8 iloop = 0;
    uint16 Ref;
    P2VAR(Dem_EventInfoType, AUTOMATIC, DEM_VAR) pEvent;

    while (iloop < DEM_EVENT_PARAMETER_NUM)
    {
		Ref = DemPbCfgPtr->DemEventParameter[iloop].DemDTCRef;
		pEvent = &DemEventInfo[iloop];
		if ((DemPbCfgPtr->DemDTC[Ref].GroupRef == GroupIndex) && (0x00u != DEM_FLAGS_ISSET(pEvent->Status, DEM_EVENT_STATUS_ENABLED_CONDICTION)))
		{
			Dem_Clear(pEvent);
			pEntry = Dem_MemEntryGet(Dem_GetEventExternalId(pEvent->IntId), MemDest);
			if (pEntry != NULL_PTR)
			{
				Dem_MemEntryDelete(pEntry,MemDest);/*SWS_Dem_00660] */
				DemMemDestInfo[MemDest].RecordNum--;
			}
		}
		iloop++;
    }
    /*SWS_Dem_00399] */
    if (DemMemDestInfo[MemDest].RecordNum == 0u)
    {
        DemMemDestInfo[MemDest].OverFlow = FALSE;
    }
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif
/*************************************************************************/
/*
 * Brief               ClearOneDTC
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
static FUNC(void, DEM_CODE) Dem_ClearOneDTC(uint8 MemDest,
    uint16 DTCIndex)
{
    uint16 iloop = 0;
    P2VAR(Dem_EventMemEntryType, AUTOMATIC, DEM_VAR) pEntry;
    P2VAR(Dem_EventInfoType, AUTOMATIC, DEM_VAR) pEvent;
    while (iloop < DEM_EVENT_PARAMETER_NUM)
    {
        if (DemPbCfgPtr->DemEventParameter[iloop].DemDTCRef == DTCIndex)
        {
			pEvent = &DemEventInfo[iloop];
			if (0x00u != DEM_FLAGS_ISSET(pEvent->Status, DEM_EVENT_STATUS_ENABLED_CONDICTION))
			{
				Dem_Clear(pEvent);
		        pEntry = Dem_MemEntryGet(Dem_GetEventExternalId(pEvent->IntId), MemDest);
		        if (pEntry != NULL_PTR)
		        {
					Dem_MemEntryDelete(pEntry,MemDest);/*SWS_Dem_00660] */
					DemMemDestInfo[MemDest].RecordNum--;
		        }
			}
        }
        iloop++;
    }
    /*SWS_Dem_00399] */
    if (DemMemDestInfo[MemDest].RecordNum == 0u)
    {
        DemMemDestInfo[MemDest].OverFlow = FALSE;
    }
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif

/*************************************************************************/
/*
 * Brief               ClearDTCProcess
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      none
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_ClearDTCProcess(void)
{
    P2VAR(Dem_ClearDTCInfoType, AUTOMATIC, DEM_VAR) pClr = &DemClearDTCInfo;

    if (pClr->MemDest == DEM_MEM_DEST_INVALID)
    {
        return;/*SWS_Dem_00171]*/
    }

    if (pClr->ClearAllGroup == TRUE)
    {
        Dem_ClearAllDTC(pClr->MemDest);
    }
#if(DEM_CLEAR_DTCLIMITATION == DEM_ALL_SUPPORTED_DTCS)
    else
    {
        if (pClr->DTCGroupIndex != DEM_GROUP_OF_DTC_INVALID)
        {
#if(DEM_GROUP_OF_DTC_NUM > 0)
            Dem_ClearGroupDTC(pClr->MemDest, pClr->DTCGroupIndex);
#endif
        }
        else
        {
            if (pClr->DTCIndex != DEM_DTC_REF_INVALID)
            {
                Dem_ClearOneDTC(pClr->MemDest, pClr->DTCIndex);
            }
        }
    }
#endif
    Dem_Pending = FALSE;
    DemClearDTCLock = 0u;
    return;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               BRIEF DESCRIPTION
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      DTC && DTCFormat
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              DTC Index
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(uint16, DEM_CODE) Dem_GetDTCIndex(uint32 DTC,
    Dem_DTCFormatType DTCFormat)
{
    uint16 index = 0;

    switch(DTCFormat)
    {
        case DEM_DTC_FORMAT_UDS:
        while (index < DEM_DTC_NUM)
        {
            if (DemPbCfgPtr->DemDTC[index].DemDtcValue == DTC)
            {
                return index;
            }
            index++;
        }
        break;

        case DEM_DTC_FORMAT_OBD:
        while (index < DEM_DTC_NUM)
        {
            if ((DemPbCfgPtr->DemDTC[index].DemObdDTCRef != DEM_OBD_DTC_INVALID)
                    && (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[index].DemObdDTCRef].DemDtcValue == DTC))
            {
                return index;
            }
            index++;
        }
        break;

        case DEM_DTC_FORMAT_J1939:
        while (index < DEM_DTC_NUM)
        {
            if ((DemPbCfgPtr->DemDTC[index].DemObdDTCRef != DEM_OBD_DTC_INVALID)
                    && (DemPbCfgPtr->DemObdDTC[DemPbCfgPtr->DemDTC[index].DemObdDTCRef].DemJ1939DTCValue == DTC))
            {
                return index;
            }
            index++;
        }
        break;
        default:
        break;
    }
    return DEM_DTC_REF_INVALID;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

#if(DEM_GROUP_OF_DTC_NUM > 0)
/*************************************************************************/
/*
 * Brief               GetDTCGroupIndex
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      Id
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              DTCGroupIndex
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(uint8, DEM_CODE) Dem_GetDTCGroupIndex(uint32 Id)
{
    uint8 iloop = 0;

    while (iloop < DEM_GROUP_OF_DTC_NUM)
    {
        if (DemGroupOfDTC[iloop] == Id)
        {
            return iloop;
        }
        iloop++;
    }
    return DEM_GROUP_OF_DTC_INVALID;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
#endif

/*************************************************************************/
/*
 * Brief               Dem_UPdateInternalData
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      IntId
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_UPdateInternalData(uint16 IntId)
{
    P2CONST(Dem_EventParameterType, AUTOMATIC, DEM_CONST) pEventCfg = &DemPbCfg.DemEventParameter[IntId];
    P2VAR(Dem_EventInfoType, AUTOMATIC, DEM_VAR) pEvent = &DemEventInfo[IntId];
    P2CONST(Dem_DTCAttributesType, AUTOMATIC, DEM_CONST) pDTCAttrCfg;
    P2CONST(uint8, AUTOMATIC, DEM_CONST) pMemDest;
    uint8 iloop = 0;

    InternalData.Occctr = pEvent->OccurrenceCounter;/*SWS_Dem_00471]*/
	if (pEventCfg->DemDTCRef != DEM_DTC_REF_INVALID)
	{
		pDTCAttrCfg =
					&DemPbCfg.DemDTCAttributes[DemPbCfg.DemDTC[pEventCfg->DemDTCRef].DemDTCAttributesRef];

		if (pDTCAttrCfg->DemAgingAllowed == FALSE)
		{
			InternalData.AgingUpCnt = 0;/*SWS_Dem_01044]*/
		}
		else
		{
			InternalData.AgingUpCnt = pEvent->AgingCounter;/*SWS_Dem_00643]*/
		}
		if (pDTCAttrCfg->DemAgingAllowed == FALSE)
		{
			InternalData.AgingDownCnt = pDTCAttrCfg->DemAgingCycleCounterThreshold;/*SWS_Dem_01043]*/
		}
		else
		{
			InternalData.AgingDownCnt = pDTCAttrCfg->DemAgingCycleCounterThreshold - pEvent->AgingCounter;/*SWS_Dem_00673]*/
		}
		pMemDest = pDTCAttrCfg->DemMemoryDestinationRef;
		InternalData.Ovflind = FALSE;
		while (iloop < DEM_MEM_DEST_MAX_NUM_OF_DTC)
		{
			if (pMemDest[iloop] != DEM_MEM_DEST_INVALID)
			{
				if (DemMemDestInfo[pMemDest[iloop]].OverFlow == TRUE)
				{/*if the MemDest have one OverFlow is true, then the internal OverFlow is true*/
					InternalData.Ovflind = TRUE;/*SWS_Dem_00473] */
				}
			}
			iloop++;
		}
		InternalData.Significance = pDTCAttrCfg->DemDTCSignificance;/*SWS_Dem_01084]*/
		InternalData.MaxFDCDuringCurrentCycle = FDCInfo[pEventCfg->DemDTCRef].MaxFDCDuringCurrentCycle;/*SWS_Dem_00819]*/
		InternalData.MaxFDCSinceLastClear = FDCInfo[pEventCfg->DemDTCRef].MaxFDCSinceLastClear;/*SWS_Dem_00818]*/
		if (FDCInfo[pEventCfg->DemDTCRef].FDC < 0)/*SWS_Dem_01084]*/
		{
			InternalData.CurrentFDC = (uint8)(0xFF + FDCInfo[pEventCfg->DemDTCRef].FDC +1);
		}
		else
		{
			InternalData.CurrentFDC = (uint8)FDCInfo[pEventCfg->DemDTCRef].FDC;
		}
	}
    InternalData.CyclesSinceLastFailed = pEvent->CyclesSinceLastFailed;/*SWS_Dem_00820]*/
    InternalData.CyclesSinceFirstFailed = pEvent->CyclesSinceFirstFailed;/*SWS_Dem_00821] */
    InternalData.FailedCycles = pEvent->FailureCounter;/*SWS_Dem_00822] */
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
/*************************************************************************/
/*
 * Brief               Dem_Clear
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      MemDest && pEntry && pEvent
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              none
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_Clear(
        P2VAR(Dem_EventInfoType, AUTOMATIC, DEM_VAR) pEvent)
{
    boolean allowed = FALSE;
    boolean clear = FALSE;

	/*SWS_Dem_00514] [SWS_Dem_00515]*/
	/* req SWS_Dem_00680 Monitor re-initialization  */
	if(DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackInitMForE != NULL_PTR)
	{
		DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackInitMForE(DEM_INIT_MONITOR_CLEAR);
	}

	if ((DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackClearEventAllowed != NULL_PTR)
		 && (DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackClearEventAllowed->ClearEventAllowed != NULL_PTR))
	{
		if (E_OK == DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackClearEventAllowed->ClearEventAllowed(&allowed))
		{
			if (allowed == TRUE)
			{
				/*SWS_Dem_00385] */
				pEvent->UdsStatus = 0x00;
				DEM_FLAGS_SET(pEvent->UdsStatus,DEM_UDS_STATUS_TNCSLC|DEM_UDS_STATUS_TNCTOC);/* bit 4 6 the initialized value 0x50 */

			}
			else
			{
				clear = TRUE;/*SWS_Dem_00667]*/
				if (DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemCallbackClearEventAllowed->DemClearEventAllowedBehavior
						== DEM_ONLY_THIS_CYCLE_AND_READINESS)
				{
					/*SWS_Dem_00669]*/
					DEM_FLAGS_SET(pEvent->UdsStatus,DEM_UDS_STATUS_TNCSLC|DEM_UDS_STATUS_TNCTOC);/* bit 4 6 reset */
					DEM_FLAGS_CLR(pEvent->UdsStatus,DEM_UDS_STATUS_TFTOC|DEM_UDS_STATUS_TFSLC);/* bit 1 5 reset */
				}/*SWS_Dem_00668]*/
			}
		}
		/*SWS_Dem_00516] */
		else
		{
			/*SWS_Dem_00385] */
			pEvent->UdsStatus = 0x00;
			DEM_FLAGS_SET(pEvent->UdsStatus,DEM_UDS_STATUS_TNCSLC|DEM_UDS_STATUS_TNCTOC);/* bit 4 6 the initialized value 0x50 */
		}
	}
	else
	{
		/*SWS_Dem_00385] */
		pEvent->UdsStatus = 0x00;
		DEM_FLAGS_SET(pEvent->UdsStatus,DEM_UDS_STATUS_TNCSLC|DEM_UDS_STATUS_TNCTOC);/* bit 4 6 the initialized value 0x50 */
	}

    Dem_UpdateCombinedDtcStatus(pEvent->IntId);/* update the combination event dtc status*/
    if (clear == FALSE)
    {
        Dem_DebounceReset(pEvent->IntId);/*SWS_Dem_00343]*/
        if (DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemDTCRef != DEM_DTC_REF_INVALID)
        {
        	FDCInfo[DemPbCfgPtr->DemEventParameter[pEvent->IntId].DemDTCRef].MaxFDCSinceLastClear = 0;/*SWS_Dem_00794]*/
        }
#if (DEM_DTR_NUM > 0)
        Dem_ClearDTRInfoByEventID(pEvent->IntId);/*SWS_Dem_00763]*/
#endif
    }
}

#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"

/*************************************************************************/
/*
 * Brief               GetDTCGroupIndex
 * ServiceId           --
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant/Non Reentrant
 * Param-Name[in]      Id
 * Param-Name[out]     none
 * Param-Name[in/out]  none
 * Return              DTCGroupIndex
 */
/*************************************************************************/
#define DEM_START_SEC_CODE
#include "Dem_MemMap.h"
FUNC(void, DEM_CODE) Dem_CleanDemClearDTCInfo(void)
{
    P2VAR(Dem_ClearDTCInfoType, AUTOMATIC, DEM_VAR) pClr = &DemClearDTCInfo;
    pClr->DTCIndex = DEM_DTC_REF_INVALID;
    pClr->DTCGroupIndex = DEM_GROUP_OF_DTC_INVALID;
    pClr->ClearAllGroup = FALSE;
    DemClearDTCLock = 2u;
}
#define DEM_STOP_SEC_CODE
#include "Dem_MemMap.h"
/*******************************************************************************
**                      end of file                                           **
*******************************************************************************/
