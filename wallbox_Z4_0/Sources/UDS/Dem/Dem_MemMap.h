/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dem_MemMap.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : tao.yu                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : Memory mapping abstraction declaration of DEM               **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*  <VERSION>    <DATE>    <AUTHOR>    <REVISION LOG>                         */
/*  V1.0.0       2018-4-20  tao.yu    Initial version                         */
/*  V1.0.1       2019-9-17  tao.yu    fix some bug,change event callback      */
/*  V1.0.2       2019-12-25  tao.yu    QAC fix     							  */
/*  V1.0.3       2020-1-7   tao.yu    Commercial project problem modification */



#ifndef DEM_MEMMAP_H
#define DEM_MEMMAP_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DEM_MEMMAP_VENDOR_ID        62
#define DEM_MEMMAP_MODULE_ID        54
#define DEM_MEMMAP_AR_MAJOR_VERSION  4
#define DEM_MEMMAP_AR_MINOR_VERSION  2
#define DEM_MEMMAP_AR_PATCH_VERSION  2
#define DEM_MEMMAP_SW_MAJOR_VERSION  1
#define DEM_MEMMAP_SW_MINOR_VERSION  0
#define DEM_MEMMAP_SW_PATCH_VERSION  3

/*=======[M E M M A P  S Y M B O L  D E F I N E]==============================*/

#include "MemMap.h"


#endif /* DEM_MEMMAP_H */
