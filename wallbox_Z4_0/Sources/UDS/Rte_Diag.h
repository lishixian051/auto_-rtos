/*============================================================================*/
/*  Copyright (C) 2016,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Diag.h>
 *  @brief      <>
 *  
 *  <MCU:TC27x>
 *  
 *  @author     <>
 *  @date       <2020-02-19 09:49:20>
 */
/*============================================================================*/


#ifndef RTE_DIAG_H_
#define RTE_DIAG_H_


#include "Dcm_Types.h"
#include "Rte_Dcm.h"
#include "Dcm_Cfg.h"

















extern Std_ReturnType RTE_PreConditonCheck();

extern Std_ReturnType Rte_DcmEcuReset(uint8 ResetType,Dcm_NegativeResponseCodeType* ErrorCode);

extern void Rte_EnableAllDtcsRecord(void);


#endif /* RTE_DIAG_H_ */
