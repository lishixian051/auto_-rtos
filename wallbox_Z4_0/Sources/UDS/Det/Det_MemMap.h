/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Det_MemMap.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION :                                                             **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef DET_MEMMAP_H
#define DET_MEMMAP_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DET_MEMMAP_VENDOR_ID  		62
#define DET_MEMMAP_MODULE_ID  		195
#define DET_MEMMAP_AR_MAJOR_VERSION  4
#define DET_MEMMAP_AR_MINOR_VERSION  2
#define DET_MEMMAP_AR_PATCH_VERSION  2
#define DET_MEMMAP_SW_MAJOR_VERSION  1
#define DET_MEMMAP_SW_MINOR_VERSION  0
#define DET_MEMMAP_SW_PATCH_VERSION  0
#define DET_MEMMAP_VENDOR_API_INFIX  0

/*=======[M E M M A P  S Y M B O L  D E F I N E]==============================*/

#include "MemMap.h"


#endif /* DET_MEMMAP_H */
