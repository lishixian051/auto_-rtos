/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Det.h                                                       **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION : Type definition and API declaration for DET                 **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef DET_H
#define DET_H

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "Std_Types.h"
#include "Det_Cfg.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define DET_H_AR_MAJOR_VERSION  4
#define DET_H_AR_MINOR_VERSION  2
#define DET_H_AR_PATCH_VERSION  2
#define DET_H_SW_MAJOR_VERSION  1
#define DET_H_SW_MINOR_VERSION  0
#define DET_H_SW_PATCH_VERSION  0

#define DET_MODULE_ID  15
#define DET_VENDOR_ID  62

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
/*functions to be called by the DET for callout or notification*/
typedef P2FUNC(Std_ReturnType, DET_APPL_CODE, Det_CalloutFnctPtrType)(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
);

/*Configuration data structure of the Det module*/
typedef struct
{
} Det_ConfigType;


/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/
/*This parameter defines the existence and the name of a callout function for
 *the corresponding runtime error handler.*/
extern Det_CalloutFnctPtrType Det_ReportRuntimeErrorCallout;

/*This parameter defines the existence and the name of a callout function for
 * the corresponding transient fault handler.*/
extern Det_CalloutFnctPtrType Det_ReportTransientFaultCallout;

#if (DET_ERROR_HOOK_NUM > 0)
/*list of functions to be called by the Default Error Tracer in context of each
 * call of Det_ReportError*/
extern Det_CalloutFnctPtrType Det_ErrorHook[DET_ERROR_HOOK_NUM];
#endif

/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define DET_START_SEC_CODE
#include "Det_MemMap.h"
/**
 * Service to initialize the Default Error Tracer.
 * The configuration pointer shall always have a NULL_PTR value, because DET
 * does not have post build parameters
 * Service ID: 0x00
 * Sync/Async: Synchronous
 * Reentrancy: Non Reentrant
 * Parameters(IN): ConfigPtr, Pointer to the selected configuration set
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, DET_CODE) Det_Init(
    P2CONST(Det_ConfigType, AUTOMATIC, DET_APPL_DATA) ConfigPtr
);

/**
 * Service to report development errors.
 * Service ID: 0x01
 * Sync/Async: no return
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 ErrorId, ID of detected development error
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: returns always E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportError(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
);

/**
 * Service to start the Default Error Tracer.
 * Service ID: 0x02
 * Sync/Async: Synchronous
 * Reentrancy: Non Reentrant
 * Parameters(IN): NA
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, DET_CODE) Det_Start(
    void
);

/**
 * Service to report runtime errors.
 * Service ID: 0x04
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 ErrorId, ID of detected runtime error
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: returns always E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportRuntimeError(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
);

/**
 * Service to report transient faults.
 * Service ID: 0x05
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 FaultId, ID of detected transient fault
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value:Propagates return value of assigned callout if exists,
 *              otherwise E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportTransientFault(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 FaultId
);

/**
 * Returns the version information of this module.
 * Service ID: 0x03
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): NA
 * Parameters(INOUT): NA
 * Parameters(OUT): versioninfo, Pointer to where to store the version information
 * Return value:Propagates return value of assigned callout if exists,
 *              otherwise E_OK
 */
FUNC(void, DET_CODE) Det_GetVersionInfo(
    P2VAR(Std_VersionInfoType, AUTOMATIC, DET_APPL_DATA) versioninfo
);

#define DET_STOP_SEC_CODE
#include "Det_MemMap.h"
#endif /* DET_H */
