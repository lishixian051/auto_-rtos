/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Det.c                                                       **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION :                                                             **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/



/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Det.h"

/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
#define DET_C_AR_MAJOR_VERSION  4
#define DET_C_AR_MINOR_VERSION  2
#define DET_C_AR_PATCH_VERSION  2
#define DET_C_SW_MAJOR_VERSION  1
#define DET_C_SW_MINOR_VERSION  0
#define DET_C_SW_PATCH_VERSION  0

/*******************************************************************************
**                       Version  Check                                       **
*******************************************************************************/
#if ((DET_C_AR_MAJOR_VERSION != DET_H_AR_MAJOR_VERSION)\
   ||(DET_C_AR_MINOR_VERSION != DET_H_AR_MINOR_VERSION)\
   ||(DET_C_AR_PATCH_VERSION != DET_H_AR_PATCH_VERSION)\
   ||(DET_C_SW_MAJOR_VERSION != DET_H_SW_MAJOR_VERSION)\
   ||(DET_C_SW_MINOR_VERSION != DET_H_SW_MINOR_VERSION)\
   ||(DET_C_SW_PATCH_VERSION != DET_H_SW_PATCH_VERSION))

#error " Det.c version mismatching with Det.h"

#endif
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
/*module state type*/
typedef enum
{
    DET_STATE_OFF,
    DET_STATE_ON
}Det_ModuleStateType;

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/



/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/



/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/


/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/*module state */
#define DET_START_SEC_VAR_INIT_UNSPECIFIED
#include "Det_MemMap.h"
VAR(Det_ModuleStateType, DET_VAR_INIT_UNSPECIFIED)
Det_ModuleState = DET_STATE_OFF;
#define DET_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Det_MemMap.h"

/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#define DET_START_SEC_CODE
#include "Det_MemMap.h"
/**
 * Service to initialize the Default Error Tracer.
 * The configuration pointer shall always have a NULL_PTR value, because DET
 * does not have post build parameters
 * Service ID: 0x00
 * Sync/Async: Synchronous
 * Reentrancy: Non Reentrant
 * Parameters(IN): ConfigPtr, Pointer to the selected configuration set
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, DET_CODE) Det_Init(
    P2CONST(Det_ConfigType, AUTOMATIC, DET_APPL_DATA) ConfigPtr
)
{
    if (DET_STATE_OFF == Det_ModuleState)
    {
        Det_ModuleState = DET_STATE_ON;
    }
}

/**
 * Service to report development errors.
 * Service ID: 0x01
 * Sync/Async: no return
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 ErrorId, ID of detected development error
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: returns always E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportError(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
)
{
    uint8 index;

    if (DET_STATE_ON == Det_ModuleState)
    {
        /*call error hooks*/
        for (index = 0; index < DET_ERROR_HOOK_NUM; index++)
        {
            if (NULL_PTR != Det_ErrorHook[index])
            {
                Det_ErrorHook[index](ModuleId, InstanceId, ApiId, ErrorId);
            }
        }
    }

    while (TRUE)
    {
        /*stop here not return*/
    }
    return E_OK;
}

/**
 * Service to start the Default Error Tracer.
 * Service ID: 0x02
 * Sync/Async: Synchronous
 * Reentrancy: Non Reentrant
 * Parameters(IN): NA
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: NA
 */
FUNC(void, DET_CODE) Det_Start(
    void
)
{
    /*nothing to do*/
}

/**
 * Service to report runtime errors.
 * Service ID: 0x04
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 ErrorId, ID of detected runtime error
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value: returns always E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportRuntimeError(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 ErrorId
)
{
    if (DET_STATE_ON == Det_ModuleState)
    {
        if (NULL_PTR != Det_ReportRuntimeErrorCallout)
        {
            Det_ReportRuntimeErrorCallout(ModuleId, InstanceId, ApiId, ErrorId);
        }
    }
    return E_OK;
}

/**
 * Service to report transient faults.
 * Service ID: 0x05
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): ModuleId, Module ID of calling module
 *                 InstanceId, ID of the index based instance of a module
 *                 ApiId, ID of API service in which error is detected
 *                 FaultId, ID of detected transient fault
 * Parameters(INOUT): NA
 * Parameters(OUT): NA
 * Return value:Propagates return value of assigned callout if exists,
 *              otherwise E_OK
 */
FUNC(Std_ReturnType, DET_CODE) Det_ReportTransientFault(
    uint16 ModuleId,
    uint8 InstanceId,
    uint8 ApiId,
    uint8 FaultId
)
{
    Std_ReturnType ret = E_NOT_OK;
    if (DET_STATE_ON == Det_ModuleState)
    {
        if (NULL_PTR != Det_ReportTransientFaultCallout)
        {
            ret = Det_ReportTransientFaultCallout(
                    ModuleId,
                    InstanceId,
                    ApiId,
                    FaultId);
        }
    }
    return ret;
}

/**
 * Returns the version information of this module.
 * Service ID: 0x03
 * Sync/Async: Synchronous
 * Reentrancy: Reentrant
 * Parameters(IN): NA
 * Parameters(INOUT): NA
 * Parameters(OUT): versioninfo, Pointer to where to store the version information
 * Return value:Propagates return value of assigned callout if exists,
 *              otherwise E_OK
 */
FUNC(void, DET_CODE) Det_GetVersionInfo(
    P2VAR(Std_VersionInfoType, AUTOMATIC, DET_APPL_DATA) versioninfo
)
{
    if (NULL_PTR != versioninfo)
    {
        versioninfo->moduleID = DET_MODULE_ID;
        versioninfo->sw_major_version = DET_C_AR_MAJOR_VERSION;
        versioninfo->sw_minor_version = DET_C_AR_MINOR_VERSION;
        versioninfo->sw_patch_version = DET_C_AR_PATCH_VERSION;
        versioninfo->vendorID = DET_VENDOR_ID;
    }
}

#define DET_STOP_SEC_CODE
#include "Det_MemMap.h"
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/


/*******************************************************************************
**                            General Notes                                   **
*******************************************************************************/

