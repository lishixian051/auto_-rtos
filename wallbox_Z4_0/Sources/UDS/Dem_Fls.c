/*============================================================================*/
/*  Copyright (C) 2016,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Diag.c>
 *  @brief      <>
 *  
 *  <MCU:TC27x>
 *  
 *  @author     <>
 *  @date       <2020-02-19 09:49:20>
 */
/*============================================================================*/




/******************************* references ************************************/
#include "Dem_Fls.h"




/**************************************************************************
 ********************* callback function for ECU Reset********************
 *************************************************************************/
const Fls_AddressType FLSAddr_DEM_Flag = 0;
const Fls_AddressType FLSAddr_DEM_Entry = 0;
const Fls_AddressType FLSAddr_DEM_Event = 0;
const Fls_LengthType  FlsSize_DEM = 0;
uint8 Fls_Erase(Fls_AddressType EraseAddr,Fls_LengthType DataLength)
{
	/* Customer adds erasing function */
	return 0;
}


uint8 Fls_Write(Fls_AddressType WriteAddr,uint8* StoreDataPtr,Fls_LengthType DataLength)
{
	/* Customer adds writing function */
	return 0;
}

uint8 Fls_Read(Fls_AddressType ReadAddr,uint8* StoreDataPtr,Fls_LengthType DataLength)
{
	/* Customer adds reading function */
	return 0;
}

uint8 Fls_GetErrorStatus(NvM_BlockIdType BlockId, NvM_RequestResultType* RequestResultPtr)
{
	*RequestResultPtr = NVM_REQ_OK;
	/* Customer adds get error function */
	return 0;
}



















