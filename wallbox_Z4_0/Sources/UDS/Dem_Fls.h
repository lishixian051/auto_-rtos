/*============================================================================*/
/*  Copyright (C) 2016,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Diag.h>
 *  @brief      <>
 *  
 *  <MCU:TC27x>
 *  
 *  @author     <>
 *  @date       <2020-02-19 09:49:20>
 */
/*============================================================================*/


#ifndef DEM_FLS_H_
#define DEM_FLS_H_


#include "Dcm_Types.h"
#include "Rte_Dcm.h"
#include "Dcm_Cfg.h"



typedef enum
{
	/*The last asynchronous request has been finished successfully. This shall be the default value after rese*/
	NVM_REQ_OK,
	/*The last asynchronous read/write/control request has been finished unsuccessfully*/
	NVM_REQ_NOT_OK,
	/*An asynchronous read/write/control request is currently pending*/
	NVM_REQ_PENDING,
	 /* The result of the last asynchronous request NvM_ReadBlock or
	 * NvM_ReadAll is a data integrity failure.
	 * In case of NvM_ReadBlock the content of the RAM block has changed but has become invalid.
	 * The application is responsible to renew and validate the RAM block content.*/
	NVM_REQ_INTEGRITY_FAILED,
	/* The referenced block was skipped during execution of NvM_ReadAll or NvM_WriteAll,
	 * Dataset NVRAM blocks (NvM_ReadAll) or NVRAM blocks without a permanently configured RAM block.*/
	NVM_REQ_BLOCK_SKIPPED,
	/*The referenced NV block is invalidated*/
	NVM_REQ_NV_INVALIDATED,
	/*The multi block request NvM_WriteAll was cancelled by calling NvM_CancelWriteAll.*/
	NVM_REQ_CANCELED,
	/*The required redundancy of the referenced NV block is lost*/
	NVM_REQ_REDUNDANCY_FAILED,
	/*The referenced NV block had the default values copied to the RAM image.*/
	NVM_REQ_RESTORED_FROM_ROM,
} NvM_RequestResultType;

/*********************************************************************************************************************/
/*****************************   Data types defined by the customer  *************************************************/
/*********************************************************************************************************************/
typedef unsigned long          Fls_AddressType;

typedef unsigned long          Fls_LengthType;

typedef unsigned short         NvM_BlockIdType;


/*********************************************************************************************************************/
/***********************   The stored function interface set aside by i-SOFT  ****************************************/
/*********************************************************************************************************************/


extern const Fls_AddressType FLSAddr_DEM_Flag;
extern const Fls_AddressType FLSAddr_DEM_Entry;
extern const Fls_AddressType FLSAddr_DEM_Event;
extern const Fls_LengthType  FlsSize_DEM;

extern uint8 Fls_Erase(Fls_AddressType EraseAddr,Fls_LengthType DataLength);

extern uint8 Fls_Write(Fls_AddressType EraseAddr,uint8* StoreDataPtr,Fls_LengthType DataLength);

extern uint8 Fls_Read(Fls_AddressType ReadAddr,uint8* StoreDataPtr,Fls_LengthType DataLength);

extern uint8 Fls_GetErrorStatus(NvM_BlockIdType BlockId, NvM_RequestResultType* RequestResultPtr);
#endif /* DEM_FLS_H */
