/*============================================================================*/
/*  Copyright (C) 2016,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  @file       <Rte_Diag.c>
 *  @brief      <>
 *  
 *  <MCU:TC27x>
 *  
 *  @author     <>
 *  @date       <2020-02-19 09:49:20>
 */
/*============================================================================*/




/******************************* references ************************************/
#include "Rte_Diag.h"
#include "Dcm_Internal.h"

#include "Mcu.h"


uint8 RestExcuteType = RTE_MODE_DcmEcuReset_NONE;
/**************************************************************************
 ********************* callback function for ECU Reset********************
 *************************************************************************/
Std_ReturnType Rte_DcmEcuReset(
	uint8 ResetType,
	Dcm_NegativeResponseCodeType* ErrorCode)
{
	(void)ErrorCode;
    uint8 ret = E_NOT_OK;


    switch(ResetType)
    {
        case RTE_MODE_DcmEcuReset_EXECUTE:

        	 if(RTE_MODE_DcmEcuReset_HARD == RestExcuteType)
        	 {
        		 Hardware_Reset();
        	 }
        	 else
        	 {
            	 software_reset();
        	 }

        	 RestExcuteType = RTE_MODE_DcmEcuReset_NONE;

             ret = E_OK;
             break;
        case RTE_MODE_DcmEcuReset_HARD:
        	 RestExcuteType = RTE_MODE_DcmEcuReset_HARD;
             ret = E_OK;
             break;
        case RTE_MODE_DcmEcuReset_SOFT:
        	 RestExcuteType = RTE_MODE_DcmEcuReset_SOFT;
             ret = E_OK;
             break;
        case RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER:
        	 RestExcuteType = RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER;
             ret = E_OK;
             break;
        case RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER:
        	 RestExcuteType = RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER;
             ret = E_OK;
             break;

        default:
            ret = E_OK;
            break;
    }

    return ret;
}


/*PreConditon Check*/
Std_ReturnType  Dcm_Rte_PreConditonCheck = E_OK;
Std_ReturnType RTE_PreConditonCheck()
{
	//Dcm_Rte_PreConditonCheck = RTE_getChargingState();
	return Dcm_Rte_PreConditonCheck;
}


void Rte_EnableAllDtcsRecord(void)
{
   /*The update of the DTC status bit information shall continue once a ControlDTCSetting request is performed
     with sub-function set to on or a session layer timeout occurs (server transitions to defaultSession. */
    (void)Dem_DcmEnableDTCSetting(DEM_DTC_GROUP_ALL_DTCS, DEM_DTC_KIND_ALL_DTCS);
}






























