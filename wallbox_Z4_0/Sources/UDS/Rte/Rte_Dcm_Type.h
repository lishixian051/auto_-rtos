#ifndef COMMUNICATION_DCM_RTE_DCM_TYPE_H_
#define COMMUNICATION_DCM_RTE_DCM_TYPE_H_

#include "Std_Types.h"

typedef uint8 EcuSignalDataType;


/****************************************************************************************
 ********Security level type definition (0x02 ~ 0x7F: dependent on configuration)********
 ****************************************************************************************/
typedef  uint8   Dcm_SecLevelType;
#define  DCM_SEC_LEV_LOCKED      ((Dcm_SecLevelType)0x00)
#define  DCM_SEC_LEV_L1          ((Dcm_SecLevelType)0x01)
#define  DCM_SEC_LEV_L2          ((Dcm_SecLevelType)0x02)
#define  DCM_SEC_LEV_L3          ((Dcm_SecLevelType)0x03)
#define  DCM_SEC_LEV_L11         ((Dcm_SecLevelType)0x0B)
#define  DCM_SEC_LEV_ALL         ((Dcm_SecLevelType)0xFF)

/****************************************************************************************
 *******Session Control Type Definition (0x05 to 0x7F: dependent on configuration)*******
 ****************************************************************************************/
typedef  uint8	 Dcm_SesCtrlType;
#define  DCM_DEFAULT_SESSION                  ((Dcm_SesCtrlType)0x01)
#define  DCM_PROGRAMMING_SESSION              ((Dcm_SesCtrlType)0x02)
#define  DCM_EXTENDED_DIAGNOSTIC_SESSION      ((Dcm_SesCtrlType)0x03)
#define  DCM_SAFETY_SYSTEM_DIAGNOSTIC_SESSION ((Dcm_SesCtrlType)0x04)
#define  DCM_ALL_SESSION_LEVEL                ((Dcm_SesCtrlType)0xFF)

/****************************************************************************************
 ***************************Protocol Type Definition**********************************
 ****************************************************************************************/
typedef  uint8   Dcm_ServiceType;
#define  DCM_OBD		                       ((Dcm_ServiceType)0x00)
#define  DCM_UDS			                    ((Dcm_ServiceType)0x01)
#define  DCM_ROE          			            ((Dcm_ServiceType)0x02)
#define  DCM_PERIODIC  			                ((Dcm_ServiceType)0x03)
#define  DCM_NO_SERVICE							((Dcm_ServiceType)0x04)

typedef  uint8   Dcm_ProtocolType;
#define  DCM_OBD_ON_CAN                       ((Dcm_ProtocolType)0x00)
#define	 DCM_OBD_ON_FLEXRAY					  ((Dcm_ProtocolType)0x01)
#define	 DCM_OBD_ON_IP						  ((Dcm_ProtocolType)0x02)
#define  DCM_UDS_ON_CAN                       ((Dcm_ProtocolType)0x03)
#define  DCM_UDS_ON_FLEXRAY                   ((Dcm_ProtocolType)0x04)
#define	 DCM_UDS_ON_IP						  ((Dcm_ProtocolType)0x05)
#define  DCM_ROE_ON_CAN                       ((Dcm_ProtocolType)0x06)
#define  DCM_ROE_ON_FLEXRAY                   ((Dcm_ProtocolType)0x07)
#define	 DCM_ROE_ON_IP						  ((Dcm_ProtocolType)0x08)
#define  DCM_PERIODIC_ON_CAN                  ((Dcm_ProtocolType)0x09)
#define  DCM_PERIODIC_ON_FLEXRAY              ((Dcm_ProtocolType)0x0A)
#define  DCM_PERIODIC_ON_IP		              ((Dcm_ProtocolType)0x0B)
#define	 DCM_NO_ACTIVE_PROTOCOL				  ((Dcm_ProtocolType)0x0C)
#define  DCM_SUPPLIER_1						  ((Dcm_ProtocolType)0xF0)
#define  DCM_SUPPLIER_2						  ((Dcm_ProtocolType)0xF1)
#define  DCM_SUPPLIER_3						  ((Dcm_ProtocolType)0xF2)
#define  DCM_SUPPLIER_4						  ((Dcm_ProtocolType)0xF3)
#define  DCM_SUPPLIER_5						  ((Dcm_ProtocolType)0xF4)
#define  DCM_SUPPLIER_6						  ((Dcm_ProtocolType)0xF5)
#define  DCM_SUPPLIER_7						  ((Dcm_ProtocolType)0xF6)
#define  DCM_SUPPLIER_8						  ((Dcm_ProtocolType)0xF7)
#define  DCM_SUPPLIER_9						  ((Dcm_ProtocolType)0xF8)
#define  DCM_SUPPLIER_10					  ((Dcm_ProtocolType)0xF9)
#define  DCM_SUPPLIER_11					  ((Dcm_ProtocolType)0xFA)
#define  DCM_SUPPLIER_12					  ((Dcm_ProtocolType)0xFB)
#define  DCM_SUPPLIER_13					  ((Dcm_ProtocolType)0xFC)
#define  DCM_SUPPLIER_14					  ((Dcm_ProtocolType)0xFD)
#define  DCM_SUPPLIER_15					  ((Dcm_ProtocolType)0xFE)
#define  DCM_NO_PROTOCOL                      ((Dcm_ProtocolType)0xFF)

/****************************************************************************************
 *************************Negative response code types defined**************************
 ****************************************************************************************/
/****@req DCM-FUNR-083[DCM228]****/
typedef  uint8   Dcm_NegativeResponseCodeType;
#define  DCM_E_GENERALREJECT                             ((Dcm_NegativeResponseCodeType)0x10)	/*generalReject*/
#define  DCM_E_SERVICENOTSUPPORTED                       ((Dcm_NegativeResponseCodeType)0x11)	/*serviceNotSupported */
#define  DCM_E_SUBFUNCTIONNOTSUPPORTED                   ((Dcm_NegativeResponseCodeType)0x12)	/*subFunctionNotSupported*/
#define  DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT     ((Dcm_NegativeResponseCodeType)0x13) 	/*incorrectMessageLengthOrInvalidFormat */
#define	 DCM_E_RESPONSETOOLONG							 ((Dcm_NegativeResponseCodeType)0x14)
#define  DCM_E_BUSYREPEATREQUEST                         ((Dcm_NegativeResponseCodeType)0x21)   /*busyRepeatRequest*/
#define  DCM_E_CONDITIONSNOTCORRECT                      ((Dcm_NegativeResponseCodeType)0x22)	/*conditionsNotCorrect*/
#define  DCM_E_REQUESTSEQUENCEERROR                      ((Dcm_NegativeResponseCodeType)0x24)   /*requestSequenceError */
#define  DCM_E_NORESPONSEFROMSUBNETCOMPONENT			 ((Dcm_NegativeResponseCodeType)0x25)
#define  DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION ((Dcm_NegativeResponseCodeType)0x26)
#define  DCM_E_REQUESTOUTOFRANGE                         ((Dcm_NegativeResponseCodeType)0x31)   /*requestOutOfRange */
#define  DCM_E_SECURITYACCESSDENIED                      ((Dcm_NegativeResponseCodeType)0x33)   /*securityAccessDenied*/
#define  DCM_E_INVALIDKEY                                ((Dcm_NegativeResponseCodeType)0x35) 	/*invalidKey*/
#define  DCM_E_EXCEEDEDNUMBEROFATTEMPTS                  ((Dcm_NegativeResponseCodeType)0x36) 	/*exceedNumberOfAttempts*/
#define  DCM_E_REQUIREDTIMEDELAYNOTEXPIRED               ((Dcm_NegativeResponseCodeType)0x37) 	/*requiredTimeDelayNotExpired*/
#define  DCM_E_UPLOADDOWNLOADNOTACCEPTED                 ((Dcm_NegativeResponseCodeType)0x70)
#define  DCM_E_TRANSFERDATASUSPENDED                     ((Dcm_NegativeResponseCodeType)0x71)
#define  DCM_E_GENERALPROGRAMMINGFAILURE                 ((Dcm_NegativeResponseCodeType)0x72)   /*generalProgrammingFailure*/
#define  DCM_E_WRONGBLOCKSEQUENCECOUNTER                 ((Dcm_NegativeResponseCodeType)0x73)
#define  DCM_E_RESPONSE_PENDING                          ((Dcm_NegativeResponseCodeType)0x78)   /*requestCorrectlyReceived-ResponsePending */
#define  DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION    ((Dcm_NegativeResponseCodeType)0x7E)   /*subFunctionNotSupportedInActiveSession*/
#define  DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION        ((Dcm_NegativeResponseCodeType)0x7F)   /*serviceNotSupportedInActiveSession*/
#define  DCM_E_RPMTOOHIGH                                ((Dcm_NegativeResponseCodeType)0x81)   /*rpmTooHigh*/
#define  DCM_E_RPMTOOLOW                                 ((Dcm_NegativeResponseCodeType)0x82)   /*rpmTooLow*/
#define  DCM_E_ENGINEISRUNNING                           ((Dcm_NegativeResponseCodeType)0x83)   /*engineIsRunning*/
#define  DCM_E_ENGINEISNOTRUNNING                        ((Dcm_NegativeResponseCodeType)0x84)   /*engineIsNotRunning*/
#define  DCM_E_ENGINERUNTIMETOOLOW                       ((Dcm_NegativeResponseCodeType)0x85)   /*engineRunTimeTooLow */
#define  DCM_E_TEMPERATURETOOHIGH                        ((Dcm_NegativeResponseCodeType)0x86)   /*temperatureTooHigh */
#define  DCM_E_TEMPERATURETOOLOW                         ((Dcm_NegativeResponseCodeType)0x87)   /*temperatureTooLow*/
#define  DCM_E_VEHICLESPEEDTOOHIGH                       ((Dcm_NegativeResponseCodeType)0x88)   /*vehicleSpeedTooHigh*/
#define  DCM_E_VEHICLESPEEDTOOLOW                        ((Dcm_NegativeResponseCodeType)0x89)   /*vehicleSpeedTooLow*/
#define  DCM_E_THROTTLE_PEDALTOOHIGH                     ((Dcm_NegativeResponseCodeType)0x8A)   /*throttle/PedalTooHigh*/
#define  DCM_E_THROTTLE_PEDALTOOLOW                      ((Dcm_NegativeResponseCodeType)0x8B)   /*throttle/PedalTooLow*/
#define  DCM_E_TRANSMISSIONRANGENOTINNEUTRAL             ((Dcm_NegativeResponseCodeType)0x8C)
#define  DCM_E_TRANSMISSIONRANGENOTINGEAR                ((Dcm_NegativeResponseCodeType)0x8D)
#define  DCM_E_BRAKESWITCH_NOTCLOSED                     ((Dcm_NegativeResponseCodeType)0x8F)
#define  DCM_E_SHIFTERLEVERNOTINPARK                     ((Dcm_NegativeResponseCodeType)0x90)
#define  DCM_E_TORQUECONVERTERCLUTCHLOCKED               ((Dcm_NegativeResponseCodeType)0x91)
#define  DCM_E_VOLTAGETOOHIGH                            ((Dcm_NegativeResponseCodeType)0x92)
#define  DCM_E_VOLTAGETOOLOW                             ((Dcm_NegativeResponseCodeType)0x93)
#define  DCM_E_VMSCNC_0                             ((Dcm_NegativeResponseCodeType)0xF0)
#define  DCM_E_VMSCNC_1                             ((Dcm_NegativeResponseCodeType)0xF1)
#define  DCM_E_VMSCNC_2                             ((Dcm_NegativeResponseCodeType)0xF2)
#define  DCM_E_VMSCNC_3                             ((Dcm_NegativeResponseCodeType)0xF3)
#define  DCM_E_VMSCNC_4                             ((Dcm_NegativeResponseCodeType)0xF4)
#define  DCM_E_VMSCNC_5                             ((Dcm_NegativeResponseCodeType)0xF5)
#define  DCM_E_VMSCNC_6                             ((Dcm_NegativeResponseCodeType)0xF6)
#define  DCM_E_VMSCNC_7                             ((Dcm_NegativeResponseCodeType)0xF7)
#define  DCM_E_VMSCNC_8                             ((Dcm_NegativeResponseCodeType)0xF8)
#define  DCM_E_VMSCNC_9                             ((Dcm_NegativeResponseCodeType)0xF9)
#define  DCM_E_VMSCNC_A                             ((Dcm_NegativeResponseCodeType)0xFA)
#define  DCM_E_VMSCNC_B                             ((Dcm_NegativeResponseCodeType)0xFB)
#define  DCM_E_VMSCNC_C                             ((Dcm_NegativeResponseCodeType)0xFC)
#define  DCM_E_VMSCNC_D                             ((Dcm_NegativeResponseCodeType)0xFD)
#define  DCM_E_VMSCNC_E                             ((Dcm_NegativeResponseCodeType)0xFE)
/*************************************************************************************************/
typedef  uint8   Dcm_ConfirmationStatusType;
#define  DCM_RES_POS_OK                             ((Dcm_ConfirmationStatusType)0x00)
#define  DCM_RES_POS_NOT_OK                         ((Dcm_ConfirmationStatusType)0x01)
#define  DCM_RES_NEG_OK                             ((Dcm_ConfirmationStatusType)0x02)
#define  DCM_RES_NEG_NOT_OK                         ((Dcm_ConfirmationStatusType)0x03)

typedef  uint8   Dcm_OpStatusType;
#define  DCM_INITIAL                             ((Dcm_OpStatusType)0x00)
#define  DCM_PENDING                             ((Dcm_OpStatusType)0x01)
#define  DCM_CANCEL                              ((Dcm_OpStatusType)0x02)
#define  DCM_FORCE_RCRRP_OK                      ((Dcm_OpStatusType)0x03)

typedef  uint8   Dcm_EcuResetType;
#define  DCM_NONE                                ((Dcm_EcuResetType)0x00)
#define  DCM_HARD                                ((Dcm_EcuResetType)0x01)
#define  DCM_KEYONOFF                            ((Dcm_EcuResetType)0x02)
#define  DCM_SOFT                                ((Dcm_EcuResetType)0x03)
#define  DCM_ENABLERAPIDPOWERSHUTDOWN            ((Dcm_EcuResetType)0x04)
#define  DCM_DISABLERAPIDPOWERSHUTDOWN           ((Dcm_EcuResetType)0x05)

typedef  uint8   RTE_MODE_EcuResetType;
#define  RTE_MODE_DcmEcuReset_NONE                                ((RTE_MODE_EcuResetType)0x00)
#define  RTE_MODE_DcmEcuReset_HARD                                ((RTE_MODE_EcuResetType)0x01)
#define  RTE_MODE_DcmEcuReset_KEYONOFF                            ((RTE_MODE_EcuResetType)0x02)
#define  RTE_MODE_DcmEcuReset_SOFT                                ((RTE_MODE_EcuResetType)0x03)
#define  RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER                    ((RTE_MODE_EcuResetType)0x04)
#define  RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER         ((RTE_MODE_EcuResetType)0x05)
#define  RTE_MODE_DcmEcuReset_EXECUTE                             ((RTE_MODE_EcuResetType)0x06)
#define	 RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION     ((RTE_MODE_EcuResetType)0x07)



#endif /* COMMUNICATION_DCM_RTE_DCM_TYPE_H_ */
