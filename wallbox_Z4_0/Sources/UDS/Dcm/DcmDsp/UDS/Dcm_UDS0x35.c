/*
 * Dcm_UDS0x35.c
 *
 *  Created on: 2018-8-20
 *      Author: tao.yu
 */

#include "UDS.h"


/****************************************************************
     UDS: RequestUpload (35 hex) service
 ***************************************************************/
#if (STD_ON == DCM_UDS_SERVICE0X35_ENABLED)

/******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_0x35MemorySecurityCheck(
        uint8 MemoryIdInfoIndex,
        uint8 MemoryRangeInfoIndex)
{
    uint8 index = 0u;
    Std_ReturnType ret = E_NOT_OK;
    uint8 SecNum;
    P2CONST(Dcm_DspReadMemoryRangeInfoType, AUTOMATIC, DCM_VAR_NOINIT)
     pDcmDspReadMemoryRangeInfo = NULL_PTR;

    pDcmDspReadMemoryRangeInfo =
            &(Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfo[MemoryIdInfoIndex].
                    DcmDspReadMemoryRangeInfo[MemoryRangeInfoIndex]);
    SecNum = pDcmDspReadMemoryRangeInfo->DcmDspReadMemorySecurityLevelRefNum;
    if (SecNum != 0u)
    {
        /*[SWS_Dcm_00494] */
        for (index = 0; (index < SecNum); index++)
        {
            if (Dcm_MkCtrl.Dcm_ActiveSec == pDcmDspReadMemoryRangeInfo->
                    pDcmDspReadMemorySecurityLevelRow[index])
            {
                ret = E_OK;
            }
        }
    }
    else
    {
         ret = E_OK;
    }

    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


/******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_0x35MemoryRangeCheck(
        uint32 MemoryAddress,
        uint32 Memorysize,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryIdInfoIndex,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryRangeInfoIndex,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryAddressMatchNum)
{
    uint8 IdInfoIndex = 0u;
    uint8 RangeInfoIndex = 0;
    Std_ReturnType ret = E_NOT_OK;
    P2CONST(Dcm_DspReadMemoryRangeInfoType, AUTOMATIC, DCM_VAR_NOINIT)
     pDcmDspReadMemoryRangeInfo = NULL_PTR;
    P2CONST(Dcm_DspMemoryIdInfoType, AUTOMATIC, DCM_VAR_NOINIT)
     pDcmDspMemoryIdInfo = NULL_PTR;

    for(IdInfoIndex = 0u;
            IdInfoIndex < Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfoNum;
            IdInfoIndex++)
    {
        pDcmDspMemoryIdInfo =
                &(Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfo[IdInfoIndex]);
        if (pDcmDspMemoryIdInfo != NULL_PTR)
        {
            for(RangeInfoIndex = 0;
                    RangeInfoIndex < pDcmDspMemoryIdInfo->
                    DcmDspReadMemoryRangeInfoNum;
                    RangeInfoIndex++)
            {
                pDcmDspReadMemoryRangeInfo =
                    &(pDcmDspMemoryIdInfo->DcmDspReadMemoryRangeInfo[RangeInfoIndex]);
                if (pDcmDspReadMemoryRangeInfo != NULL_PTR)
                {
                    if ((pDcmDspReadMemoryRangeInfo->DcmDspReadMemoryRangeLow
                            <= MemoryAddress)
                        && (pDcmDspReadMemoryRangeInfo->DcmDspReadMemoryRangeHigh
                                >= (MemoryAddress + Memorysize - 1u)))
                    {
                        *MemoryIdInfoIndex = IdInfoIndex;
                        *MemoryRangeInfoIndex = RangeInfoIndex;
                        *MemoryAddressMatchNum += 1u;
                        ret = E_OK;
                    }
                }
            }
        }
    }
    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x35ServiceAddressAndLengthFormatIdentifierCheck(
                const uint8 addressAndLengthFormatIdentifier)
{
    Std_ReturnType ret = E_NOT_OK;
    uint8 iloop = 0;

    for (iloop = 0;
            (iloop < Dcm_DspCfg.pDcmDspMemory->DcmDspAddressAndLengthFormatIdentifierNum)
                    && (ret == E_NOT_OK);
            iloop++)
    {
        if(addressAndLengthFormatIdentifier ==
          Dcm_DspCfg.pDcmDspMemory->DcmDspAddressAndLengthFormatIdentifier[iloop].
          DcmDspSupportedAddressAndLengthFormatIdentifier)
        {
            ret = E_OK;
        }
    }
    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/***************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x35ServiceConditionCheck(
        uint8 ProtocolCtrlId,
        uint8 MsgCtrlId)
{
    Std_ReturnType ret = E_OK;

    /*************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    /*session check,check whether the current
     * session supports the request service*/
    ret = DsdInternal_SesCheck(ProtocolCtrlId,
            SID_REQUEST_UPLOAD);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-073[DCM211]****/
        /*the current session does not support
         *  the request service,send NRC = 0x7F*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif

    /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    /*security check,check whether the current
     *  security supports the request service*/
    ret = DsdInternal_SecurityCheck(ProtocolCtrlId,
            SID_REQUEST_UPLOAD);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-074[DCM217]****/
        /*the current security does not
         *  support the request service,send NRC = 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId
                ,DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif
    /*min-length check*/
    if (DCM_UDS0X35_REQ_DATA_MINLENGTH
            > Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
    {
        /*the min length of message is not correct,send NRC 0x13*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


/*******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x35(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode)
{
    uint8  MsgCtrlId = 0u;
    Std_ReturnType ret = E_OK;
    uint8  AddressAndLengthFormatIdentifier = 0u;
    uint32 MemoryAddress = 0u;
    uint32 Memorysize = 0u;
    uint8  TxChannelCtrlIndex = 0u;
    uint8  TxChannelCfgIndex = 0u;
    uint16 Offset = 0u;
    uint8 MemoryIdInfoIndex = 0u;
    uint8 MemoryRangeInfoIndex = 0u;
    uint8 MemoryAddressSize = 0u;
    uint8 MemoryLengthSize = 0u;
    uint8 index = 0u;
    uint8 MemoryAddressMatchNum = 0;
    uint8 DataFormatIdentifier = 0;
    uint32 BlockLength = 0;

    /*************************************************/
    /*if the required protocol is configured,get the index of runtime datum*/
    MsgCtrlId =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;

    /*************************************************/
    ret = Dcm_Uds0x35ServiceConditionCheck(ProtocolCtrlId, MsgCtrlId);
    if(E_OK != ret)
    {
     return ret;
    }

    DataFormatIdentifier = Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[1];
    AddressAndLengthFormatIdentifier =
            Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[2];
    /*[SWS_Dcm_00857]*/
    ret = Dcm_Uds0x35ServiceAddressAndLengthFormatIdentifierCheck(
            AddressAndLengthFormatIdentifier);
    if (E_NOT_OK == ret)
    {
        /*the processing is not successful,send NRC 0x31 */
        (void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_REQUESTOUTOFRANGE);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    MemoryAddressSize = AddressAndLengthFormatIdentifier & 0x0Fu;
    MemoryLengthSize = (AddressAndLengthFormatIdentifier & 0xF0u) >> 4u;

    /*total length check*/
    if ((Dcm_MsgLenType)((Dcm_MsgLenType)3 + MemoryAddressSize + MemoryLengthSize)
            != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
    {
        /*the length of message is not correct,send NRC 0x13*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /*caculate the MemoryAddress of the request message*/
    for(index = 0u;index < MemoryAddressSize;index++)
    {
        MemoryAddress = MemoryAddress << 8u;
        MemoryAddress = MemoryAddress
               | (uint32)((uint32)((uint32)Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[3u + index])
                       & 0xFFUL);
    }

    for(index = 0u;index < MemoryLengthSize;index++)
    {
        Memorysize = Memorysize << 8u;
        Memorysize = Memorysize
                | (uint32)((uint32)((uint32)Dcm_MsgCtrl[MsgCtrlId].MsgContext.
                    pReqData[3u + MemoryAddressSize + index]) & 0xFFUL);
    }
    /*MemoryAddress Range Check*/
    ret = Dcm_0x35MemoryRangeCheck(MemoryAddress,
                                    Memorysize,
                                    &MemoryIdInfoIndex,
                                    &MemoryRangeInfoIndex,
                                    &MemoryAddressMatchNum);
    if((E_NOT_OK == ret) || (MemoryAddressMatchNum > 1u))
    {
        /*[SWS_Dcm_01055]different MemoryIdValue compare
         *  to the request memoryAddress,send NRC 0x31*/
        /*memoryAddress is not inside the allowed memory ranges,send NRC 0x31 */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_REQUESTOUTOFRANGE);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /*MemoryAddress Security Accsess Check*/
    ret = Dcm_0x35MemorySecurityCheck(MemoryIdInfoIndex,MemoryRangeInfoIndex);
    if(E_NOT_OK == ret)
    {
        /*security check not ok for requested memory interval,send NRC 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;
    TxChannelCfgIndex  = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
    Offset = (Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;

    ret = Dcm_ProcessRequestUpload(OpStatus,
                                    DataFormatIdentifier,
                                    MemoryAddress,
                                    Memorysize,
                                    &BlockLength,
                                    ErrorCode);
    if (E_NOT_OK == ret)
    {/*[SWS_Dcm_01133][SWS_Dcm_00758] */
        /*the processing is not successful,send NRC */
        (void)DsdInternal_SetNrc(ProtocolCtrlId, *ErrorCode);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    else if (DCM_E_PENDING == ret)
    {
        Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus = DCM_PENDING;
        return ret;
    }
    else
    {
        /*idle*/
    }

    /* check tx data length */
    if ((6u) > (Dcm_DslCfg.pDcmChannelCfg[TxChannelCfgIndex].Dcm_DslBufferSize))
    {
        /*Pdu length is bigger than buffer size */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_RESPONSETOOLONG);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /* get address */
    Dcm_DspProgram.address = MemoryAddress;
    /* set index to 0, for service 0x36 */
    Dcm_DspProgram.blockId = 0x00;
    /* set 0x36 service status to DCM_UDS0X36_TO_UPLOAD*/
    Dcm_DspProgram.Status = DCM_UDS0X36_TO_UPLOAD;
    /*memory the MemoryIdInfoIndex*/
    Dcm_DspProgram.MemoryIdInfoIndex = MemoryIdInfoIndex;
    /*memory one block size*/
    Dcm_DspProgram.BlockLength = BlockLength;

    /**********************************************************
     * assemble positive response
     *********************************************************/
    /*the processing is successful,assemble positive response*/
    SchM_Enter_Dcm(Dcm_Channel);
    Dcm_Channel[Offset] = 0x75;             /*response SID*/
    Dcm_Channel[Offset + 1u] = 0x40;
    /*lengthFormatIdentifier*/
    Dcm_Channel[Offset + 2u] = (uint8)(BlockLength >> (uint8)24);
    /*maxNumberOfBlockLength*/
    Dcm_Channel[Offset + 3u] = (uint8)(BlockLength >> (uint8)16);
    /*maxNumberOfBlockLength*/
    Dcm_Channel[Offset + 4u] = (uint8)(BlockLength >> (uint8)8);
    /*maxNumberOfBlockLength*/
    Dcm_Channel[Offset + 5u] = (uint8)BlockLength;
    /*maxNumberOfBlockLength*/
    SchM_Exit_Dcm(Dcm_Channel);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen = 6u;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen    = 6u;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData      = &Dcm_Channel[Offset];
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    DsdInternal_ProcessingDone(ProtocolCtrlId);

    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#endif
