/*
 * Dcm_UDS0x11.c
 *
 *  Created on: 2018-1-16
 *      Author: Shute
 */
#include "UDS.h"



/****************************************************************
	 			UDS:ECUReset(0x11) service
 ***************************************************************/
#if(STD_ON == DCM_UDS_SERVICE0X11_ENABLED)

static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x11ServiceConditionCheck(
        uint8 ProtocolCtrlId,
		P2VAR(uint8, AUTOMATIC, AUTOMATIC)MsgCtrlId);
		
/********************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x11ServiceConditionCheck(
        uint8 ProtocolCtrlId,
		P2VAR(uint8, AUTOMATIC, AUTOMATIC)MsgCtrlId)
{
	Std_ReturnType ret;

    /*************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    /*session check,check whether the current session supports the request service*/
    ret = DsdInternal_SesCheck(ProtocolCtrlId,SID_ECU_RESET);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-073[DCM211]****/
        /*the current session does not support the request service,send NRC = 0x7F*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif

    /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    /*security check,check whether the current security supports the request service*/
    ret = DsdInternal_SecurityCheck(ProtocolCtrlId, SID_ECU_RESET);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-074[DCM217]****/
        /*the current security does not support the request service,send NRC = 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif
	/*************************************************/
	/*if the required protocol is configured,get the index of runtime datum*/
	*MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;

	/*check the massage length*/
	if (DCM_UDS0X11_REQ_DATA_LENGTH != Dcm_MsgCtrl[*MsgCtrlId].MsgContext.ReqDataLen)
	{
		/*the length of massage is not correct,send NRC 0x13*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
		return E_NOT_OK;
	}

	/*check the required reset type is supported*/
	ret = Dcm_UdsSubServicesCheck(ProtocolCtrlId);
	if (E_NOT_OK == ret)
	{
		/*if the required reset type is not supported,send NRC 0x12*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_SUBFUNCTIONNOTSUPPORTED);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
		return E_NOT_OK;
	}

#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    ret = DsdInternal_SubSesCheck(ProtocolCtrlId,SID_ECU_RESET);
    if(E_NOT_OK == ret)
    {
        /****SWS_Dcm_00616****/
        /*the current session does not support the request sub service,send NRC = 0x7E*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif

    /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    /*security check,check whether the current security supports the request service*/
    ret = DsdInternal_SubSecurityCheck(ProtocolCtrlId, SID_ECU_RESET);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-074[DCM217]****/
        /*the current security does not support the request service,send NRC = 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif
	return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/********************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x11(
		Dcm_OpStatusType OpStatus,
		uint8 ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode)
{
#if(STD_ON == DCM_DSP_ECU_RESET_FUNC_ENABLED)
	uint8  MsgCtrlId = 0;
	uint8  resetType;
	uint8  TxChannelCtrlIndex;
	uint8  TxChannelCfgIndex;
	uint16 Offset;
	Std_ReturnType ret;

	/*************************************************/
	ret = Dcm_Uds0x11ServiceConditionCheck(ProtocolCtrlId, &MsgCtrlId);
	if(E_OK != ret)
	{
		return ret;
	}

	/*************************************************/

//   	ret = RTE_PreConditonCheck();
//
//   	if(E_OK != ret)
//   	{
//   		(void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_CONDITIONSNOTCORRECT);
//   		DsdInternal_ProcessingDone(ProtocolCtrlId);
//   		return E_NOT_OK;
//   	}

	/*get the reset type*/
	resetType = Dcm_MsgCtrl[MsgCtrlId].Subfunction;

	/*if all return values are OK,assemble and send positive response*/
	TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;
	TxChannelCfgIndex = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
	Offset = (Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;

    /* check tx data length */
    if((2u) > (Dcm_DslCfg.pDcmChannelCfg[TxChannelCfgIndex].Dcm_DslBufferSize))
    {
        /*Pdu length is bigger than buffer size,ignore the request message */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_RESPONSETOOLONG);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

	/*assemble positive response*/
	SchM_Enter_Dcm(Dcm_Channel);
	Dcm_Channel[Offset]   = 0x51;/*response SID*/
	Dcm_Channel[Offset+1u] = resetType;/*an echo of bits 6 - 0 of the sub-function parameter*/
	SchM_Exit_Dcm(Dcm_Channel);
	SchM_Enter_Dcm(Dcm_MsgCtrl);
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen = 2u;
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen  = 2u;
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData  = &Dcm_Channel[Offset];
	SchM_Exit_Dcm(Dcm_MsgCtrl);
	DsdInternal_ProcessingDone(ProtocolCtrlId);
	return  E_OK;

#else
	(void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_REQUESTOUTOFRANGE);
	DsdInternal_ProcessingDone(ProtocolCtrlId);
	return E_NOT_OK;
#endif
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
