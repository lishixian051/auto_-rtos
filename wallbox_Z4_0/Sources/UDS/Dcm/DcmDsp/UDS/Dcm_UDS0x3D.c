/*
 * Dcm_UDS0x3D.c
 *
 *  Created on: 2018-8-7
 *      Author: tao.yu
 */
#include "UDS.h"

#if(STD_ON == DCM_UDS_SERVICE0X3D_ENABLED)
/****************************************************************
             UDS:WriteMemoryByAddress (3D hex) service
 ***************************************************************/
/******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_0x3DMemoryWriteSecurityCheck(
        uint8 MemoryIdInfoIndex,
        uint8 MemoryRangeInfoIndex)
{
    uint8 index;
    Std_ReturnType ret = E_NOT_OK;
    uint8 SecNum;
    P2CONST(Dcm_DspWriteMemoryRangeInfoType, AUTOMATIC, DCM_VAR_NOINIT)
    pDcmDspWriteMemoryRangeInfo;

    pDcmDspWriteMemoryRangeInfo =
            &(Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfo[MemoryIdInfoIndex].
                    DcmDspWriteMemoryRangeInfo[MemoryRangeInfoIndex]);
    SecNum = pDcmDspWriteMemoryRangeInfo->DcmDspWriteMemorySecurityLevelRefNum;
    if (SecNum != 0u)
    {
        /*[SWS_Dcm_00490]  */
        for (index = 0; (index < SecNum); index++)
        {
            if (Dcm_MkCtrl.Dcm_ActiveSec
                    == pDcmDspWriteMemoryRangeInfo->
                    pDcmDspWriteMemorySecurityLevelRow[index])
            {
                ret = E_OK;
            }
        }
    }
    else
    {
         ret = E_OK;
    }

    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x3DServiceAddressAndLengthFormatIdentifierCheck(
                const uint8 addressAndLengthFormatIdentifier)
{
    Std_ReturnType ret = E_NOT_OK;
    uint8 iloop;

    for (iloop = 0;
            (iloop < Dcm_DspCfg.pDcmDspMemory->DcmDspAddressAndLengthFormatIdentifierNum)
                    && (ret == E_NOT_OK);
            iloop++)
    {
        if(addressAndLengthFormatIdentifier ==
          Dcm_DspCfg.pDcmDspMemory->DcmDspAddressAndLengthFormatIdentifier[iloop].
          DcmDspSupportedAddressAndLengthFormatIdentifier)
        {
            ret = E_OK;
        }
    }
    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/***************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)Dcm_Uds0x3DServiceConditionCheck(
        uint8 ProtocolCtrlId,
        uint8 MsgCtrlId)
{
    Std_ReturnType ret;

    /*************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
        /*session check,check whether the current
         *  session supports the request service*/
        ret = DsdInternal_SesCheck(ProtocolCtrlId,
                SID_WIRTE_DATA_BY_MEMORYADDRESS);
        if(E_NOT_OK == ret)
        {
            /****@req DCM-FUNR-073[DCM211]****/
            /*the current session does not support
             * the request service,send NRC = 0x7F*/
            (void)DsdInternal_SetNrc(ProtocolCtrlId,
                    DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION);
            DsdInternal_ProcessingDone(ProtocolCtrlId);
            return E_NOT_OK;
        }
#endif

        /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
        /*security check,check whether the current
         *  security supports the request service*/
        ret = DsdInternal_SecurityCheck(ProtocolCtrlId,
                SID_WIRTE_DATA_BY_MEMORYADDRESS);
        if(E_NOT_OK == ret)
        {
            /****@req DCM-FUNR-074[DCM217]****/
            /*the current security does not support the request service,send NRC = 0x33*/
            (void)DsdInternal_SetNrc(ProtocolCtrlId,
                    DCM_E_SECURITYACCESSDENIED);
            DsdInternal_ProcessingDone(ProtocolCtrlId);
            return E_NOT_OK;
        }
#endif
    /*min-length check*/
    if (DCM_UDS0X3D_REQ_DATA_MINLENGTH
            > Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
    {
        /*the length of message is not correct,send NRC 0x13*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    return E_OK;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)DsdInternal_0x3DWriteMemoryRangeCheck(
        uint32 MemoryAddress,
        uint32 Memorysize,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryIdInfoIndex,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryRangeInfoIndex,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)MemoryAddressMatchNum)
{
    uint8 IdInfoIndex;
    uint8 RangeInfoIndex;
    Std_ReturnType ret = E_NOT_OK;
    P2CONST(Dcm_DspWriteMemoryRangeInfoType, AUTOMATIC, DCM_VAR_NOINIT)
     pDcmDspWriteMemoryRangeInfo;
    P2CONST(Dcm_DspMemoryIdInfoType, AUTOMATIC, DCM_VAR_NOINIT)
     pDcmDspMemoryIdInfo;

    for(IdInfoIndex = 0u;
            IdInfoIndex < Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfoNum;
            IdInfoIndex++)
    {
        pDcmDspMemoryIdInfo =
                &(Dcm_DspCfg.pDcmDspMemory->DcmDspMemoryIdInfo[IdInfoIndex]);
        if (pDcmDspMemoryIdInfo != NULL_PTR)
        {
            for(RangeInfoIndex = 0;
                    RangeInfoIndex < pDcmDspMemoryIdInfo->DcmDspWriteMemoryRangeInfoNum;
                    RangeInfoIndex++)
            {
                pDcmDspWriteMemoryRangeInfo =
                        &(pDcmDspMemoryIdInfo->DcmDspWriteMemoryRangeInfo[RangeInfoIndex]);
                if (pDcmDspWriteMemoryRangeInfo != NULL_PTR)
                {
                    if ((pDcmDspWriteMemoryRangeInfo->DcmDspWriteMemoryRangeLow
                            <= MemoryAddress)
                        && (pDcmDspWriteMemoryRangeInfo->DcmDspWriteMemoryRangeHigh
                                >= (MemoryAddress + Memorysize - 1u)))
                    {
                        *MemoryIdInfoIndex = IdInfoIndex;
                        *MemoryRangeInfoIndex = RangeInfoIndex;
                        *MemoryAddressMatchNum += 1u;
                        ret = E_OK;
                    }
                }
            }
        }
    }
    return ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*******************************/
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x3D(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode)
{
    uint8  MsgCtrlId;
    Std_ReturnType ret;
    uint8  AddressAndLengthFormatIdentifier;
    uint32 MemoryAddress = 0u;
    uint32 Memorysize = 0u;
    uint8  TxChannelCtrlIndex;
    uint8  TxChannelCfgIndex;
    uint16 Offset;
    uint8* DCM_MemoryData;
    uint8 MemoryAddressSize;
    uint8 MemoryLengthSize;
    uint8 index;
    uint8 MemoryIdInfoIndex = 0u;
    uint8 MemoryRangeInfoIndex = 0u;
    uint8 MemoryAddressMatchNum = 0;
    uint8 MemoryIdentifier;

    /*************************************************/
    /*if the required protocol is configured,get the index of runtime datum*/
    MsgCtrlId =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;

    /*************************************************/
    ret = Dcm_Uds0x3DServiceConditionCheck(ProtocolCtrlId, MsgCtrlId);
    if(E_OK != ret)
    {
     return ret;
    }

    /*[SWS_Dcm_00855]*/
    AddressAndLengthFormatIdentifier =
            Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[1];

    ret = Dcm_Uds0x3DServiceAddressAndLengthFormatIdentifierCheck(
            AddressAndLengthFormatIdentifier);
    if (E_NOT_OK == ret)
    {
        /*the processing is not successful,send NRC 0x31*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_REQUESTOUTOFRANGE);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    MemoryAddressSize = AddressAndLengthFormatIdentifier & 0x0Fu;
    MemoryLengthSize = (AddressAndLengthFormatIdentifier & 0xF0u) >> 4u;

    /*caculate the MemoryAddress of the request message*/
    for(index = 0u;index < MemoryAddressSize;index++)
    {
        MemoryAddress = MemoryAddress << 8u;
        MemoryAddress = MemoryAddress
                | (uint32)(Dcm_MsgCtrl[MsgCtrlId].MsgContext.
                        pReqData[2u + index]);
    }

    for(index = 0u;index < MemoryLengthSize;index++)
    {
        Memorysize = Memorysize << 8u;
        Memorysize = Memorysize
                | (uint32)(Dcm_MsgCtrl[MsgCtrlId].MsgContext.
                        pReqData[2u + MemoryAddressSize + index]);
    }

    /*total length check*/
    if ((2UL + (uint32)MemoryAddressSize + (uint32)MemoryLengthSize + Memorysize)
            != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
    {
        /*the length of message is not correct,send NRC 0x13*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /*MemoryAddress Range Check*/
    ret = DsdInternal_0x3DWriteMemoryRangeCheck(MemoryAddress,
                                                Memorysize,
                                                &MemoryIdInfoIndex,
                                                &MemoryRangeInfoIndex,
                                                &MemoryAddressMatchNum);
    if((E_NOT_OK == ret) || (MemoryAddressMatchNum > 1u))
    {
        /*[SWS_Dcm_01052]different MemoryIdValue
         *  compare to the request memoryAddress,send NRC 0x31*/
        /*memoryAddress is not inside the allowed memory ranges,send NRC 0x31 */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_REQUESTOUTOFRANGE);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /*MemoryAddress Security Accsess Check*/
    ret = Dcm_0x3DMemoryWriteSecurityCheck(
            MemoryIdInfoIndex,
            MemoryRangeInfoIndex);
    if(E_NOT_OK == ret)
    {
        /*security check not ok for requested memory interval,send NRC 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,
                DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;
    TxChannelCfgIndex  = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
    Offset = (Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;
    DCM_MemoryData = &(Dcm_MsgCtrl[MsgCtrlId].MsgContext.
            pReqData[2u + MemoryAddressSize + MemoryLengthSize]);
    MemoryIdentifier = Dcm_DspCfg.pDcmDspMemory->
            DcmDspMemoryIdInfo[MemoryIdInfoIndex].DcmDspMemoryIdValue;


    /*@req SWS_Dcm_00495*/
    ret = Dcm_WriteMemory(OpStatus,
                            MemoryIdentifier,
                            MemoryAddress,
                            Memorysize,
                            DCM_MemoryData,
                            ErrorCode);

    if (DCM_WRITE_FAILED == ret)
    {
        /*the processing is not successful,send NRC [SWS_Dcm_00643]*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId, *ErrorCode);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    else if(DCM_WRITE_FORCE_RCRRP == ret)
    {
        /*the processing is pending,send NRC 0x78[SWS_Dcm_00837][SWS_Dcm_00838] */
        (void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_RESPONSE_PENDING);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        Dcm_MsgCtrl[MsgCtrlId].Dcm_Ret = DCM_E_FORCE_RCRRP;
        return DCM_E_PENDING;
    }
    else if (DCM_WRITE_PENDING == ret)
    {
        Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus = DCM_PENDING;
        return ret;
    }
    else
    {
        /*idle*/
    }

    /**********************************************************
     * assemble positive response
     *********************************************************/
    /* check tx data length */
    if (((uint16)((uint16)2 + MemoryAddressSize + MemoryLengthSize))
            > (Dcm_DslCfg.pDcmChannelCfg[TxChannelCfgIndex].Dcm_DslBufferSize))
    {
        /*Pdu length is bigger than buffer size */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_RESPONSETOOLONG);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }

    /*the processing is successful,assemble positive response*/
    SchM_Enter_Dcm(Dcm_Channel);
    for(index = 0;index < (MemoryAddressSize + MemoryLengthSize);index++)
    {
        Dcm_Channel[Offset + 2u +(uint16)index] =
                Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[2u + index];
    }
    Dcm_Channel[Offset] = 0x7D;             /*response SID*/
    Dcm_Channel[Offset + 1u] = AddressAndLengthFormatIdentifier;
    /*response AddressAndLengthFormatIdentifier*/

    SchM_Exit_Dcm(Dcm_Channel);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen =
            (uint32)((uint32)2 + MemoryAddressSize + MemoryLengthSize);
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen    =
            (uint32)((uint32)2 + MemoryAddressSize + MemoryLengthSize);
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData      = &Dcm_Channel[Offset];
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    DsdInternal_ProcessingDone(ProtocolCtrlId);
    return  ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif


