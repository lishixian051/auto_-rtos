/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *  
 *  All rights reserved. This software is  property. Duplication 
 *  or disclosure without  written authorization is prohibited.
 *  
 *  
 *  @file        <UDS.h>
 *  @brief      <declarations of UDS services> 
 *  
 *  <Compiler: CodeWarrior    MCU:XXX>
 *  
 *  @author     <Guan Shengyong>
 *  @date       <27-03-2013>
 */
/*============================================================================*/

#ifndef UDS_H
#define UDS_H

/****************************** references *********************************/
#include "Dcm_Types.h"
#include "Compiler.h"
#include "Compiler_Cfg.h"
#include "Dcm_Include.h"
#include "Dem_Dcm.h"
#include "SchM_Dcm.h"
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */



/**************************************************************************
 **********************Service request packet length defined***************
 **************************************************************************/
#define  DCM_UDS0X10_REQ_DATA_LENGTH    			(2U) /*0x10 Service request packet length defined*/
#define  DCM_UDS0X11_REQ_DATA_LENGTH    			(2U) /*0x11 Service request packet length defined*/
#define  DCM_UDS0X14_REQ_DATA_LENGTH    			(4U) /*0x14 Service request packet length defined*/
#define  DCM_UDS0X19_REQ_DATA_MINLENGTH 			(2U) /*0x19 Service request packet length defined*/
#define  DCM_UDS0X19_SUBFUNC0X01_REQ_DATA_LENGTH 	(3U) /*sub-function 0x01 request message length*/
#define  DCM_UDS0X19_SUBFUNC0X02_REQ_DATA_LENGTH 	(3U) /*sub-function 0x02 request message length*/
#define  DCM_UDS0X19_SUBFUNC0X03_REQ_DATA_LENGTH    (2U) /*sub-function 0x03 request message length*/
#define  DCM_UDS0X19_SUBFUNC0X04_REQ_DATA_LENGTH 	(6U) /*sub-function 0x04 request message length*/
#define  DCM_UDS0X19_SUBFUNC0X06_REQ_DATA_LENGTH 	(6U) /*sub-function 0x06 request message length*/
#define  DCM_UDS0X19_SUBFUNC0X0A_REQ_DATA_LENGTH 	(2U) /*sub-function 0x0A request message length*/
#define  DCM_UDS0X27_REQ_DATA_MINLENGTH             (2U) /*0x27 Service request packet length defined*/
#define  DCM_UDS0X22_REQ_DATA_MINLENGTH 			(3U) /*0x22 Service request packet length defined*/
#define  DCM_UDS0X23_REQ_DATA_MINLENGTH             (4u) /*0x23 Service request packet length defined*/
#define  DCM_UDS0X24_REQ_DATA_LENGTH                (3u) /*0x24 Service request packet length defined*/
#define  DCM_UDS0X2A_SUBFUNC0X04_REQ_DATA_MINLENGTH (2U) /*0x2A 04 Service request packet length defined*/
#define  DCM_UDS0X2A_REQ_DATA_MINLENGTH             (3U) /*0x2A other Service request packet length defined*/
#define  DCM_UDS0X2C_REQ_DATA_MINLENGTH             (4U) /*0x2C Service request packet length defined*/
#define  DCM_UDS0X2E_REQ_DATA_MINLENGTH 			(4U) /*0x2E Service request packet length defined*/
#define  DCM_UDS0X2F_REQ_DATA_MINLENGTH 			(4U) /*0x2F Service request packet length defined*/
#define  DCM_UDS0X31_REQ_DATA_MINLENGTH 			(4U) /*0x31 Service request packet length defined*/
#define  DCM_UDS0X34_REQ_DATA_MINLENGTH             (5u) /*0x34 Service request packet length defined*/
#define  DCM_UDS0X35_REQ_DATA_MINLENGTH             (5u) /*0x35 Service request packet length defined*/
#define  DCM_UDS0X36_REQ_DATA_UPLOAD_MINLENGTH      (2u) /*0x36 Service request upload packet length defined*/
#define  DCM_UDS0X36_REQ_DATA_DOWNLOAD_MINLENGTH    (3u) /*0x36 Service request download packet length defined*/
#define  DCM_UDS0X38_REQ_DATA_MINLENGTH             (5u) /*0x38 Service request packet length defined*/
#define  DCM_UDS0X3D_REQ_DATA_MINLENGTH             (5u) /*0x3D Service request packet length defined*/
#define  DCM_UDS0X3E_REQ_DATA_LENGTH    			(2U) /*0x3E Service request packet length defined*/
#define  DCM_UDS0X85_REQ_DATA_MINLENGTH    			(2U) /*0x85 request message minimum length*/
#define  DCM_UDS0X85_REQ_DATA_DTC_MINLENGTH    	    (5U) /*0x85 request message minimum length with DTC*/
#define  DCM_UDS0X28_REQ_DATA_MINLENGTH    			(3U) /*0x28 request message length*/
#define  DCM_UDS0X28_REQ_DATA_MAXLENGTH    			(5U) /*0x28 request message length*/
#define  DCM_UDS0X87_REQ_DATA_MINLENGTH    			(2U) /*0x87 request message length*/
#define  DCM_UDS0X87_SUBFUNC0X01_REQ_DATA_LENGTH    (3U) /*sub-function 0x01 request message length*/
#define  DCM_UDS0X87_SUBFUNC0X02_REQ_DATA_LENGTH    (5U) /*sub-function 0x02 request message length*/
#define  DCM_UDS0X87_SUBFUNC0X03_REQ_DATA_LENGTH    (2U) /*sub-function 0x03 request message length*/
/**********************************************************************
 *             UDS 0x19 service sub-function
 **********************************************************************/
/*UDS 0x19 service,sub-function defined*/
#define  DCM_REPORTNUMBEROFDTCBYSTATUSMASK                      (0x01u)/*reportNumberOfDTCByStatusMask */
#define  DCM_REPORTNUMBEROFDTCBYSEVERITYMASKRECORD              (0x07u)/*reportNumberOfDTCBySeverityMaskRecord*/
#define  DCM_REPORTNUMBEROFMIRRORMEMORYDTCBYSTATUSMASK          (0x11u)/*reportNumberOfMirrorMemoryDTCByStatusMask*/
#define  DCM_REPORTNUMBEROFEMISSOONREALTEOBDDTCBYSTATUSMASK     (0x12u)/*reportNumberOfEmissionsRelatedOBDDTCByStatusMask*/
#define  DCM_REPORTDTCBYSTATUSMASK                              (0x02u)/*reportDTCByStatusMask*/
#define  DCM_REPORTSUPPORTEDDTC                                 (0x0Au)/*reportSupportedDTC*/
#define  DCM_REPORTMIRRORMEMORYDTCBYSTATUSMASK                  (0x0Fu)/*reportMirrorMemoryDTCByStatusMask*/
#define  DCM_REPORTEMISSIONRELATEDOBDDTCBYSTATUSMASK            (0x13u)/*reportEmissionsRelatedOBDDTCByStatusMask*/
#define  DCM_REPORTDTCWITHPERMANENTSTATUS                       (0x15u)/*reportDTCWithPermanentStatus*/
#define  DCM_REPORTDTCBYSEVERITYMASKRECORD                      (0x08u)/*reportDTCBySeverityMaskRecord*/
#define  DCM_REPORTSEVERITYINFORMATIONOFDTC                     (0x09u)/*reportSeverityInformationOfDTC*/
#define  DCM_REPORTDTCEXTENDEDDATARECORDBYDTCNUMBER             (0x06u)/*reportDTCExtendedDataRecordByDTCNumber */
#define  DCM_REPORTMIRRORMEMORYDTCEXTENDEDDATARECORDBYDTCNUMBER (0x10u)/*reportMirrorMemoryDTCExtendedDataRecordByDTCNumber*/
#define  DCM_REPORTREPORTDTCSNAPSHOTIDENTIFICATION              (0x03u)/*reportDTCSnapshotIdentification*/
#define  DCM_REPORTDTCSNAPSHOTRECORDBYDTCNUMBER                 (0x04u)/*reportDTCSnapshotRecordByDTCNumber*/
#define  DCM_REPORTDTCSNAPSHOTRECORDBYRECORDNUMBER              (0x05u)/*reportDTCSnapshotRecordByRecordNumber*/
#define  DCM_REPORTREPORTFIRSTTESTFAILEDDTC                     (0x0Bu)/*reportFirstTestFailedDTC*/
#define  DCM_REPORTREPORTFIRSTCONFIRMEDDTC                      (0x0Cu)/*reportFirstConfirmedDTC*/
#define  DCM_REPORTMOSTRECENTTESTFAILEDDTC                      (0x0Du)/*reportMostRecentTestFailedDTC*/
#define  DCM_REPORTMOSTRECENTCONFIRMEDDTC                       (0x0Eu)/*reportMostRecentConfirmedDTC*/
#define  DCM_REPORTREPORTDTCFAULTDETECTIONCOUNTER               (0x14u)/*reportDTCFaultDetectionCounter*/

#define  ALL_SUPPORTED_DTC				  ((uint8)0xFF)
/***********************************************************************
              UDS 0x2F service InputOutputControlParameter define
 ***********************************************************************/
#define  DCM_UDS0X2F_RETURNCONTROLTOECU   (0u) 		/*ReturnControlToEcu*/
#define  DCM_UDS0X2F_RESETTODEFAULT       (1u) 		/*ResetToDefault*/
#define  DCM_UDS0X2F_FREEZECURRENTSTATE   (2u) 		/*FreezeCurrentState*/
#define  DCM_UDS0X2F_SHORTTERMADJUSTMENT  (3u) 		/*ShortTermAdjustment*/

/**********************************************************************
 *            UDS 0x2C service sub-function define
 **********************************************************************/
#define  DCM_UDS0X2A_01_SLOW                 (1u)     /*sendAtSlowRate*/
#define  DCM_UDS0X2A_02_MEDIUM               (2u)     /*sendAtMediumRate*/
#define  DCM_UDS0X2A_03_FAST                 (3u)     /*sendAtFastRate*/
#define  DCM_UDS0X2A_04_STOP                 (4u)     /*stopSending*/

/**********************************************************************
 *            UDS 0x2C service sub-function define
 **********************************************************************/
#define  DCM_UDS0X2C_01_DDBYDID              (1u)     /*defineByIdentifier*/
#define  DCM_UDS0X2C_02_DDBYMEMORY           (2u)     /*defineByMemoryAddress*/
#define  DCM_UDS0X2C_03_CLEARDDDID           (3u)     /*clearDynamicallyDefinedDataIdentifier*/

/**********************************************************************
 *            UDS 0x31 service sub-function define
 **********************************************************************/
#define  DCM_UDS0X31_STARTROUTINE          (1u)		/*startRoutine*/
#define  DCM_UDS0X31_STOPROUTINE           (2u)		/*stopRoutine*/
#define  DCM_UDS0X31_REQUESTROUTINERESULTS (3u)		/*RequestRoutineResult*/

/**********************************************************************
 *            UDS 0x3E service sub-function define
 **********************************************************************/
#define  DCM_UDS0X3E_ZERO_SUBFUNCTION      (0u)		/*zero sub-function*/

/**********************************************************************
 *            UDS 0x85 service sub-function define
 **********************************************************************/
#define  DCM_UDS0X85_ON     (1u)			/*turn on the setting of DTC*/
#define  DCM_UDS0X85_OFF    (2u)			/*turn off the setting of DTC*/

/**********************************************************************
 *            UDS 0x28 service sub-function define
 **********************************************************************/
#define  DCM_UDS0X28_ENABLE_RX_AND_TX             (0u)/*enable Rx and Tx*/
#define  DCM_UDS0X28_ENABLE_RX_AND_DISABLE_TX     (1u)/*enable Rx and disable Tx*/
#define  DCM_UDS0X28_DISABLE_RX_AND_ENABLE_TX     (2u)/*disable Rx and enable Tx*/
#define  DCM_UDS0X28_DISABLE_RX_AND_TX            (3u)/*disable Rx and Tx*/

#define COMTYPE_NORM							  (1u)/*normal communication message*/
#define COMTYPE_NM							  	  (2u)/*network management communication message*/
#define COMTYPE_NORM_AND_NM						  (3u)/*NORM and NM communication message*/

/**********************************************************************
 *            UDS 0x87 service sub-function define
 **********************************************************************/
#define  DCM_UDS0X87_VERIFY_BAUDRATE_TRANSITION_WITH_FIXED_BAUDRATE      (1u)/*VBTWFBR*/
#define  DCM_UDS0X87_VERIFY_BAUDRATE_TRANSITION_WITH_SPECIFIC_BAUDRATE   (2u)/*VBTWSBR*/
#define  DCM_UDS0X87_TRANSITION_BAUDRATE (3u)		/*transition baud rate*/

/******************************END of dependence****************************************/
#if(STD_ON == DCM_UDS_SERVICE0X87_ENABLED)
/*definition of processing status of link control*/
typedef enum Dcm_LinkControlStatus_t
{
	LINK_CONTROL_IDLE = 0,
	LINK_CONTROL_FBR_VERIFICATION = 1,
	LINK_CONTROL_FBR_TRANSITION = 2,
	LINK_CONTROL_SBR_VERIFICATION = 3,
	LINK_CONTROL_SBR_TRANSITION = 4	
}Dcm_LinkControlStatusType;

/*definition of 'link control' control block*/
typedef struct Dcm_LinkControlCtrlType_t
{
	VAR(Dcm_LinkControlStatusType, TYPEDEF) linkCtrlStatus;
	VAR(uint8, TYPEDEF) fixedBaudrate;
	VAR(uint32, TYPEDEF) specialBaudrate;
}Dcm_LinkControlCtrlType;
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2C_ENABLED)
#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern VAR(Dcm_DDDidTypes, DCM_VAR_NOINIT) Dcm_DDDid[DCM_DSP_DDDID_MAX_NUMBER];
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif


/*SID Table*/
#define SID_DIAGNOSTIC_SESSION_CONTROL				0x10u
#define SID_ECU_RESET								0x11u
#define SID_CLEAR_DIAGNOSTIC_INFORMATION			0x14u
#define SID_READ_DTC_INFORMATION					0x19u
#define SID_READ_DATA_BY_IDENTIFIER					0x22u
#define SID_READ_DATA_BY_MEMORYADDRESS              0x23u
#define SID_READ_SCALING_DATA_BY_IDENTIFIER         0x24u
#define SID_SECURITY_ACCESS							0x27u
#define SID_COMMUNICATION_CONTROL 					0x28u
#define SID_READ_DATA_BY_PERIODIC_IDENTIFER         0x2Au
#define SID_DYNAMICALLY_DEFINE_DATA_IDENTIFER       0x2Cu
#define SID_WRITE_DATA_BY_IDENTIFIER				0x2Eu
#define SID_INPUT_OUTPUT_CONTROL_BY_IDENTIFIER		0x2Fu
#define SID_ROUTINE_CONTROL							0x31u
#define SID_REQUEST_DOWNLOAD                        0x34u
#define SID_REQUEST_UPLOAD                          0x35u
#define SID_TRANSFER_DATA                           0x36u
#define SID_REQUEST_TRANSFER_EXIT                   0x37u
#define SID_REQUEST_FILE_TRANSFER                   0x38u
#define SID_WIRTE_DATA_BY_MEMORYADDRESS             0x3Du
#define SID_TESTER_PRESENT							0x3Eu
#define SID_CONTROL_DTC_SETTING						0x85u
#define SID_LINK_CONTROL 							0x87u
#define SID_TEST                                    0xB0u
/*END OF SID Table*/

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern VAR(Dcm_DspProgramType, DCM_VAR_NOINIT) Dcm_DspProgram;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_UDS_FUNC_ENABLED)
/****************************** declarations *********************************/
#if(STD_ON == DCM_UDS_SERVICE0X10_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x10(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X11_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x11(
		Dcm_OpStatusType OpStatus,
		uint8 ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X27_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x27(
		Dcm_OpStatusType OpStatus,
		uint8 ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X28_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x28(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X3E_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x3E(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X85_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x85(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X87_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x87(uint8  ProtocolCtrlId);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X14_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x14(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X19_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x19(
		Dcm_OpStatusType OpStatus,
		uint8 ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X22_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x22(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X23_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x23(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X24_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x24(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x2A(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(void, DCM_CODE)  Dcm_UDS0x2ACheckNewSession(Dcm_SesCtrlType NewSes);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(void, DCM_CODE)  Dcm_UDS0x2ACheckNewSecurity(void);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern VAR(SchedulerQueueTypes, DCM_VAR_NOINIT) SchedulerQueue[DCM_DSP_MAX_PERIODIC_DID_SCHEDULER];
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(Std_ReturnType, DCM_CODE)DspInternalUDS0x2A_DDDid_Read_Data(
        Dcm_OpStatusType OpStatus,
        uint16     RecDid,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)Data);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(Std_ReturnType, DCM_CODE)DspInternalUDS0x2A_Did_Read_Data(
                        uint16     RecDid,
                        P2VAR(uint8, AUTOMATIC, AUTOMATIC)Data);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#endif


#if(STD_ON == DCM_UDS_SERVICE0X2C_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x2C(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2E_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x2E(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2F_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x2F(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if (DCM_DSP_DID_FOR_2F_NUM > 0)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(void, DCM_CODE)  Dcm_UDS0x2FCheckNewSession(Dcm_SesCtrlType NewSes);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(void, DCM_CODE)  Dcm_UDS0x2FCheckNewSecurity(Dcm_SecLevelType  NewSec);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#endif

#if(STD_ON == DCM_UDS_SERVICE0X31_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x31(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X34_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x34(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X35_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x35(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X36_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x36(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X37_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x37(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X38_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0x38(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X3D_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x3D(
        Dcm_OpStatusType OpStatus,
        uint8  ProtocolCtrlId,
        P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_TEST_SERVICE0XB0_ENABLED)
#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(Std_ReturnType, DCM_CODE)  Dcm_UDS0xB0(uint8  ProtocolCtrlId);
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#endif/*end of 'STD_ON == DCM_UDS_FUNC_ENABLED'*/

#if(STD_ON == DCM_UDS_SERVICE0X23_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern Dcm_ReturnReadMemoryType Dcm_ReadMemory(Dcm_OpStatusType OpStatus,
        uint8 MemoryIdentifier,
        uint32 MemoryAddress,
        uint32 MemorySize,
        uint8* MemoryData,
        Dcm_NegativeResponseCodeType* ErrorCode);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X3D_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern Dcm_ReturnWriteMemoryType Dcm_WriteMemory(Dcm_OpStatusType OpStatus,
                                            uint8 MemoryIdentifier,
                                            uint32 MemoryAddress,
                                            uint32 MemorySize,
                                            uint8* MemoryData,
                                            Dcm_NegativeResponseCodeType* ErrorCode);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if (DCM_UDS_SERVICE0X34_ENABLED == STD_ON)
/*for 0x34 service to request download*/
extern Std_ReturnType Dcm_ProcessRequestDownload(Dcm_OpStatusType OpStatus,
                                            uint8 DataFormatIdentifier,
                                            uint32 MemoryAddress,
                                            uint32 MemorySize,
                                            uint32* BlockLength,
                                            Dcm_NegativeResponseCodeType* ErrorCode);
#endif

#if (DCM_UDS_SERVICE0X35_ENABLED == STD_ON)
/*for 0x35 service to request upload*/
extern Std_ReturnType Dcm_ProcessRequestUpload(Dcm_OpStatusType OpStatus,
                                        uint8 DataFormatIdentifier,
                                        uint32 MemoryAddress,
                                        uint32 MemorySize,
                                        uint32* BlockLength,
                                        Dcm_NegativeResponseCodeType* ErrorCode);
#endif

#if (DCM_UDS_SERVICE0X37_ENABLED == STD_ON)
/*for 0x37 service to request transfer exit*/
extern Std_ReturnType Dcm_ProcessRequestTransferExit(Dcm_OpStatusType OpStatus,
                                                uint8* transferRequestParameterRecord,
                                                uint32 transferRequestParameterRecordSize,
                                                uint8* transferResponseParameterRecord,
                                                uint32* transferResponseParameterRecordSize,
                                                Dcm_NegativeResponseCodeType* ErrorCode);
#endif

#if (DCM_UDS_SERVICE0X38_ENABLED == STD_ON)
/*for 0x38 service to request file transfer*/
extern Std_ReturnType Dcm_ProcessRequestFileTransfer(Dcm_OpStatusType OpStatus,
                                                uint8 modeofOperation,
                                                uint16 fileSizeParameterLength,
                                                uint8* filePathAndName,
                                                uint8 dataFormatIdentifier,
                                                uint8* fileSizeUncompressedOrDirInfoLength,
                                                uint8* fileSizeCompressed,
                                                uint32* BlockLength,
                                                Dcm_NegativeResponseCodeType* ErrorCode);
#endif

extern FUNC(void, DCM_CODE)
Dcm_MemoryCopy(
    P2CONST(void, AUTOMATIC, DCM_APPL_CONST) Source,
    P2VAR(void, AUTOMATIC, DCM_APPL_DATA) Dest,
    uint16 Length);

#if(BSWM_DCM_ENABLE != STD_ON)
extern void Rte_DcmControlCommunicationMode(NetworkHandleType DcmDspComMChannelId,Dcm_CommunicationModeType RequestedMode);
#endif

#define DCM_MINIMUMLENGTH_CHECK_ENABLED STD_ON
/****************************** definitions *********************************/

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif /* UDS_H_ */
