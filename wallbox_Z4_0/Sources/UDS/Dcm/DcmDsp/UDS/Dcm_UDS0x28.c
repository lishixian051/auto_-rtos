/*
 * Dcm_UDS0x28.c
 *
 *  Created on: 2018-1-16
 *      Author: Shute
 */
#include "UDS.h"
/****************************************************************
             UDS:CommunicationControl (28 hex) service
 ***************************************************************/
#if(STD_ON == DCM_UDS_SERVICE0X28_ENABLED)

static FUNC(void, DCM_CODE)DspInternalUDS0x28_SubFuction_0x00_0x03(
        uint8  ProtocolCtrlId,
        uint8  subFunc,
        uint8 comType);
		


static FUNC(Std_ReturnType, DCM_CODE)DspInternalUDS0x28_SubFuction_0x04_0x05(
        uint16 Subnet,
        uint8  ProtocolCtrlId,
        uint8  subFunc,
        uint8 comType);
		
/********************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void, DCM_CODE)DspInternalUDS0x28_SubFuction_0x00_0x03(
        uint8  ProtocolCtrlId,
        uint8  subFunc,
        uint8 comType)
{
	uint8 iloop;
	Dcm_CommunicationModeType RequestedMode = DCM_ENABLE_RX_TX_NORM;

	if ((subFunc == 0x00u) && (comType == 0x01u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NORM;
	}
	else if ((subFunc == 0x00u) && (comType == 0x02u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NM;
	}
	else if ((subFunc == 0x00u) && (comType == 0x03u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NORM_NM;
	}
	else if ((subFunc == 0x01u) && (comType == 0x01u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NORM;
	}
	else if ((subFunc == 0x01u) && (comType == 0x02u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NM;
	}
	else if ((subFunc == 0x01u) && (comType == 0x03u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NORM_NM;
	}
	else if ((subFunc == 0x02u) && (comType == 0x01u))
	{
		RequestedMode = DCM_DISABLE_RX_ENABLE_TX_NORM;
	}
	else if ((subFunc == 0x02u) && (comType == 0x02u))
	{
		RequestedMode = DCM_DISABLE_RX_ENABLE_TX_NM;
	}
	else if ((subFunc == 0x02u) && (comType == 0x03u))
	{
		RequestedMode = DCM_DISABLE_RX_ENABLE_TX_NORM_NM;
	}
	else if ((subFunc == 0x03u) && (comType == 0x01u))
	{
		RequestedMode = DCM_DISABLE_RX_TX_NORMAL;
	}
	else if ((subFunc == 0x03u) && (comType == 0x02u))
	{
		RequestedMode = DCM_DISABLE_RX_TX_NM;
	}
	else if ((subFunc == 0x03u) && (comType == 0x03u))
	{
		RequestedMode = DCM_DISABLE_RX_TX_NORM_NM;
	}
	else
	{
		/*idle*/
	}

	if((NULL_PTR != Dcm_Cfg.pDcmDspCfg)
			&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl))
	{
		if (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel)
		{
			for (iloop = 0;
			        iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannelNum;
			        iloop++)
			{
				if (TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->
				        DcmDspComControlAllChannel->DcmDspComControlAllChannelUsed)
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel->DcmDspComMChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel->DcmDspComMChannelId,
					        RequestedMode);
#endif
				}
			}
		}
		if ((NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->
				        pDcmDslProtocolRow[ProtocolCtrlId].pDcmDslConnection)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->
				        pDcmDslProtocolRow[ProtocolCtrlId].pDcmDslConnection->pDcmDslMainConnection))
		{
#if(BSWM_DCM_ENABLE == STD_ON)
			BswM_Dcm_CommunicationMode_CurrentState(
			        Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
			        pDcmDslConnection->pDcmDslMainConnection->DcmDslProtocolComMChannelId,
			        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
					        pDcmDslConnection->pDcmDslMainConnection->DcmDslProtocolComMChannelId,
					        RequestedMode);
#endif
		}
		if (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel)
		{
			for (iloop = 0;
			        iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannelNum;
			        iloop++)
			{
				if (TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
				        DcmDspComControlSpecificChannelUsed)
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
					        DcmDspSpecificComMChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
					        DcmDspSpecificComMChannelId,
					        RequestedMode);
#endif
				}
			}
		}
		if (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode)
		{
			for (iloop = 0;
			        iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNodeNum;
			        iloop++)
			{
				if (TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
				        DcmDspComControlSubNodeUsed)
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
					        DcmDspComMSubNodeChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
					        DcmDspComMSubNodeChannelId,
					        RequestedMode);
#endif
				}
			}
		}
	}
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Std_ReturnType, DCM_CODE)DspInternalUDS0x28_SubFuction_0x04_0x05(
        uint16 Subnet,
        uint8  ProtocolCtrlId,
        uint8  subFunc,
        uint8 comType)
{
	uint8 iloop;
	boolean Find = FALSE;
	Dcm_CommunicationModeType RequestedMode = DCM_ENABLE_RX_TX_NORM;

	if ((subFunc == 0x04u) && (comType == 0x01u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NORM;
	}
	else if ((subFunc == 0x04u) && (comType == 0x02u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NM;
	}
	else if ((subFunc == 0x04u) && (comType == 0x03u))
	{
		RequestedMode = DCM_ENABLE_RX_DISABLE_TX_NORM_NM;
	}
	else if ((subFunc == 0x05u) && (comType == 0x01u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NORM;
	}
	else if ((subFunc == 0x05u) && (comType == 0x02u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NM;
	}
	else if ((subFunc == 0x05u) && (comType == 0x03u))
	{
		RequestedMode = DCM_ENABLE_RX_TX_NORM_NM;
	}
	else
	{
		/*idle*/
	}
	if(Subnet == 0x00u)
	{
		if ((NULL_PTR != Dcm_Cfg.pDcmDspCfg)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel))
		{
			for (iloop = 0;
			        iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannelNum;
			        iloop++)
			{
				if (TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel->
				        DcmDspComControlAllChannelUsed)
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel->
					        DcmDspComMChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlAllChannel->
					        DcmDspComMChannelId,
					        RequestedMode);
#endif
				}
			}
		}
	}
	if(Subnet == 0x0Fu)
	{
		if ((NULL_PTR != Dcm_Cfg.pDcmDslCfg)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
				        pDcmDslConnection)
				&& (NULL_PTR != Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
				        pDcmDslConnection->pDcmDslMainConnection))
		{
#if(BSWM_DCM_ENABLE == STD_ON)
			BswM_Dcm_CommunicationMode_CurrentState(
			        Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
			        pDcmDslConnection->pDcmDslMainConnection->DcmDslProtocolComMChannelId,
			        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDslCfg->pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
					        pDcmDslConnection->pDcmDslMainConnection->DcmDslProtocolComMChannelId,
					        RequestedMode);
#endif
		}
	}
	if((Subnet > 0x00u) && (Subnet < 0x0Fu))
	{
		if ((NULL_PTR != Dcm_Cfg.pDcmDspCfg)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel))
		{
			for (iloop = 0;
			        (iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannelNum)
			                && (Find == FALSE);
			        iloop++)
			{
				if ((TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
				        DcmDspComControlSpecificChannelUsed)
						&& (Subnet == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->
						        DcmDspComControlSpecificChannel->DcmDspSubnetNumber))
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
					        DcmDspSpecificComMChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSpecificChannel->
					        DcmDspSpecificComMChannelId,
					        RequestedMode);
#endif
					Find = TRUE;
				}
			}
		}
		if (Find == FALSE)
		{
			return E_NOT_OK;
		}
	}
	else
	{
		if ((NULL_PTR != Dcm_Cfg.pDcmDspCfg)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl)
				&& (NULL_PTR != Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode))
		{
			for (iloop = 0;
			        (iloop < Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNodeNum)
			                && (Find == FALSE);
			        iloop++)
			{
				if ((TRUE == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
				        DcmDspComControlSubNodeUsed)
						&& (Subnet == Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->
						        DcmDspComControlSubNode->DcmDspComControlSubNodeId))
				{
#if(BSWM_DCM_ENABLE == STD_ON)
					BswM_Dcm_CommunicationMode_CurrentState(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
					        DcmDspComMSubNodeChannelId,
					        RequestedMode);
#else
					Rte_DcmControlCommunicationMode(
					        Dcm_Cfg.pDcmDspCfg->pDcmDspComControl->DcmDspComControlSubNode->
					        DcmDspComMSubNodeChannelId,
					        RequestedMode);
#endif
                    Find = TRUE;
				}
			}
		}
		if (Find == FALSE)
		{
			return E_NOT_OK;
		}
	}
	return E_OK;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#define DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE)Dcm_UDS0x28(
		Dcm_OpStatusType OpStatus,
		uint8  ProtocolCtrlId,
		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode)
{
	uint8  MsgCtrlId;
	uint8  TxChannelCtrlIndex;
	uint8  TxChannelCfgIndex;
	uint8  subFunc;
	uint8  iloop;
	uint8  comType;
	uint16 Offset;
	Std_ReturnType ret;
	uint16 Subnet;
    uint8  SidTabId;
    uint8  Service_Num;
    uint16 SidTabIndex;
    uint16 ServiceIndex;
    boolean  Flag = FALSE;

	/*************************************************/
	/*if the required protocol is configuted,get the index of runtime datum*/
	MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
	/*session check,check whether the current session supports the request service*/
    ret = DsdInternal_SesCheck(ProtocolCtrlId,SID_COMMUNICATION_CONTROL);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-073[DCM211]****/
        /*the current session does not support the request service,send NRC = 0x7F*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif

    /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    /*security check,check whether the current security supports the request service*/
    ret = DsdInternal_SecurityCheck(ProtocolCtrlId,SID_COMMUNICATION_CONTROL);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-074[DCM217]****/
        /*the current security does not support the request service,send NRC = 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif

	/*check the massage length*/
	if(DCM_UDS0X28_REQ_DATA_MINLENGTH > Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
	{
		/*the length of massage is not correct,send NRC 0x13*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
		return E_NOT_OK;
	}
	
    /*get the sub-function */
    subFunc =Dcm_MsgCtrl[MsgCtrlId].Subfunction;

    SidTabId  = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCtrlId].DcmDslServiceTableID;
    /*find SidTabId configuration position in the service configuration table*/
    for(SidTabIndex=0;(SidTabIndex<Dcm_DsdCfg.DcmDsdServiceTable_Num)&&(FALSE==Flag);SidTabIndex++)
    {
        if(SidTabId == ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].DcmDsdSidTabId))
        {
            Service_Num = (Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].DcmDsdSidTab_ServiceNum;
            for(ServiceIndex=0;(ServiceIndex<Service_Num)&&(FALSE==Flag);ServiceIndex++)
            {
                if(0x28u == ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].
                        pDcmDsdService[ServiceIndex].DcmDsdServiceId))
                {
                    if ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].
                            pDcmDsdService[ServiceIndex].DcmDsdSubfuncAvial == TRUE)
                    {
                        for (iloop = 0; ((iloop < ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].
                                pDcmDsdService[ServiceIndex].DcmDsdSubService_Num))
                        && (Flag == FALSE)); iloop++)
                        {
                            if (subFunc == (Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].
                                    pDcmDsdService[ServiceIndex].DcmDsdSubService[iloop].DcmDsdSubServiceId)
                            {
                                Flag = TRUE;
                            }
                        }
                    }
                    else
                    {
                        /*the required sub-function is not supported,send NRC 0x12*/
                        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SUBFUNCTIONNOTSUPPORTED);
                        DsdInternal_ProcessingDone(ProtocolCtrlId);
                        return E_NOT_OK;
                    }
                }
            }
        }
    }
    if (Flag == FALSE)
    {
        /*the required sub-function is not supported,send NRC 0x12*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SUBFUNCTIONNOTSUPPORTED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
	
	/*check the massage length*/
	if (((DCM_UDS0X28_REQ_DATA_MAXLENGTH != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
	        && ((subFunc == 0x04u) || (subFunc == 0x05u)))
		|| ((DCM_UDS0X28_REQ_DATA_MINLENGTH != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
		        && (subFunc < 0x04u))
		|| (((DCM_UDS0X28_REQ_DATA_MINLENGTH != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen)
		        || (DCM_UDS0X28_REQ_DATA_MAXLENGTH != Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen))
		        && (subFunc > 0x05u)))
	{
		/*the length of massage is not correct,send NRC 0x13*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
		return E_NOT_OK;
	}
	/*************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
	/*session check,check whether the current session supports the request service*/
    ret = DsdInternal_SubSesCheck(ProtocolCtrlId,SID_COMMUNICATION_CONTROL);
    if(E_NOT_OK == ret)
    {
        /****SWS_Dcm_00616****/
        /*the current session does not support the request sub service,send NRC = 0x7E*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif
    /*************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    /*security check,check whether the current security supports the request service*/
    ret = DsdInternal_SubSecurityCheck(ProtocolCtrlId,SID_COMMUNICATION_CONTROL);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-074[DCM217]****/
        /*the current security does not support the request service,send NRC = 0x33*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SECURITYACCESSDENIED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
#endif
	/*get the communicationType*/
	comType =Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[2];

    if ((comType < COMTYPE_NORM) || (comType > COMTYPE_NORM_AND_NM))
    {
		/*communicationType is not supported,send NRC 0x31*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_REQUESTOUTOFRANGE);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
		return E_NOT_OK;
    }

//    ret = RTE_PreConditonCheck();
//
//    if(E_OK != ret)
//    {
//        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_CONDITIONSNOTCORRECT);
//        DsdInternal_ProcessingDone(ProtocolCtrlId);
//       	return E_NOT_OK;
//    }
    /*******************************************************
     *callback function make communicaiton control
     *******************************************************/
    if (subFunc < 0x04u)
	{
    	DspInternalUDS0x28_SubFuction_0x00_0x03(ProtocolCtrlId,subFunc,comType);
    }
    else if((subFunc == 0x04u) || (subFunc == 0x05u))
    {
    	Subnet = (uint16)((uint16)((uint16)Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[3] << 8u)
    	        | (uint16)Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData[4]);
    	if (E_NOT_OK == DspInternalUDS0x28_SubFuction_0x04_0x05(
    	        Subnet,
    	        ProtocolCtrlId,
    	        subFunc,
    	        comType))
    	{
			/*NO DcmDspSubnetNumber Finded ,send NRC 0x31*/
			(void)DsdInternal_SetNrc(ProtocolCtrlId, DCM_E_REQUESTOUTOFRANGE);
			DsdInternal_ProcessingDone(ProtocolCtrlId);
			return E_NOT_OK;
    	}
    }
    else
    {
    	/*idle*/
    }
    /**********************************************************
     * assemble positive response
     *********************************************************/
	/*the processing is successful,assemble positive response*/
	TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;
	TxChannelCfgIndex  = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
	Offset = (Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;
    /* check tx data length */
    if((0x02u) > (Dcm_DslCfg.pDcmChannelCfg[TxChannelCfgIndex].Dcm_DslBufferSize))
    {
        /*Pdu length is bigger than buffer size,ignore the request message */
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_RESPONSETOOLONG);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
	SchM_Enter_Dcm(Dcm_Channel);
	Dcm_Channel[Offset] = 0x68;   		/*response SID*/
	Dcm_Channel[Offset + 1u] = subFunc;  /*echo of 0 - 6 bits of sub-function*/
	SchM_Exit_Dcm(Dcm_Channel);
	SchM_Enter_Dcm(Dcm_MsgCtrl);
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen = 0x02;
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen    = 0x02;
	Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData      = &Dcm_Channel[Offset];
	SchM_Exit_Dcm(Dcm_MsgCtrl);
	DsdInternal_ProcessingDone(ProtocolCtrlId);
	return  ret;
}
#define DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
