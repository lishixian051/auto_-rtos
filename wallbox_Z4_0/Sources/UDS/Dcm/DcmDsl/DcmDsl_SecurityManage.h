/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <DcmDsl_SecurityManage.h>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <shushi>
 *  @date       <2018-3-20>
 */
/*============================================================================*/

#ifndef DCMDSL_SECURITYMANAGE_H
#define DCMDSL_SECURITYMANAGE_H
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCMDSL_SECURITYMANAGE_H_AR_MAJOR_VERSION  4
#define DCMDSL_SECURITYMANAGE_H_AR_MINOR_VERSION  2
#define DCMDSL_SECURITYMANAGE_H_AR_PATCH_VERSION  2
#define DCMDSL_SECURITYMANAGE_H_SW_MAJOR_VERSION  1
#define DCMDSL_SECURITYMANAGE_H_SW_MINOR_VERSION  0
#define DCMDSL_SECURITYMANAGE_H_SW_PATCH_VERSION  2

/****************************** references *********************************/
#include "Dcm_Types.h"

#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
/****************************** declarations *********************************/
/**********************************************************************
 *************Security level management data structure****************
 *********************************************************************/
typedef  enum
{
   DCM_SECTIMER_ON  = 1u,        /*Security level timer "on" state*/
   DCM_SECTIMER_OFF = 0u         /*Security level timer "oFF" state*/
}Dcm_SecTimerStateType;

typedef  struct
{
   uint32  Dcm_SecCurTimer[DCM_SECURITY_NUM];     /*Security level timer curent Timer*/
   uint32  Dcm_SecExpiredTimer[DCM_SECURITY_NUM]; /*Security level timer timeout Timer*/
   Dcm_SecTimerStateType  Dcm_SecTimerState[DCM_SECURITY_NUM];
}Dcm_SecTimerCtrlType;

typedef  enum
{
   DCM_SERVICE_IDLE = 0u,       /* "Idle" */
   DCM_SERVICE_SEED = 1u,       /* After "receiving seed"*/
   DCM_SERVICE_KEY  = 2u        /* After "comparing key" */
}Dcm_SecServiceStateType;

typedef  struct
{
   uint8    Dcm_SubfunctionForSeed;     		/*Request seed sub-functions.*/
   uint8    Dcm_FalseAcessCount[DCM_SECURITY_NUM];  /*the number of Compare key failures and consecutive Request Seed*/
   Dcm_SecLevelType  Dcm_ActiveSec;     		/*DCM module current level of security*/
   Dcm_SecLevelType  Dcm_NewSec;        		/*DCM module to be changed security level*/
   Dcm_SecServiceStateType Dcm_SecServiceState;	/*Security level change process status*/
   Dcm_SecTimerCtrlType    Dcm_RunDlyCtrl;     	/*When you reach the number of failure,the delay access control block*/
   Dcm_OpStatusType    Dcm_OpStatus;
}Dcm_SecCtrlType;

/********************************************************
 ******************resource statement********************
********************************************************/
#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern   VAR(Dcm_SecCtrlType,DCM_VAR_NOINIT) Dcm_SecCtrl; /*Security level management control block*/
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <initial of Security level control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <Dcm_Init()>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)DslInternal_InitSecCtrl(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Setting the security level>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <NewSec:To be changed the security level>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE) DslInternal_SetSecurityLevel(Dcm_SecLevelType  NewSec);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Set SecurityAccess service process ,receives seed/key status>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Status:State to be modified>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)DslInternal_SetSecurityAccessStatus(Dcm_SecServiceStateType Status);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern FUNC(Std_ReturnType,DCM_CODE)DslInternal_GetSecurityCfgBySecLevel(
        Dcm_SecLevelType  Dcm_SecLevel,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)SecCfgIndex);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#endif /* DCMDSL_SECURITYMANAGE_H_ */
