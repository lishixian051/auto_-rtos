/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <DcmDsl_SecurityManage.c>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <shushi>
 *  @date       <2018-3-20>
 */
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCMDSL_SECURITYMANAGE_C_AR_MAJOR_VERSION  4
#define DCMDSL_SECURITYMANAGE_C_AR_MINOR_VERSION  2
#define DCMDSL_SECURITYMANAGE_C_AR_PATCH_VERSION  2
#define DCMDSL_SECURITYMANAGE_C_SW_MAJOR_VERSION  1
#define DCMDSL_SECURITYMANAGE_C_SW_MINOR_VERSION  0
#define DCMDSL_SECURITYMANAGE_C_SW_PATCH_VERSION  2

/******************************* references ************************************/
#include "Dcm_Include.h"
#include "UDS.h"

#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)

/*=======[V E R S I O N  C H E C K]===========================================*/
#if (DCMDSL_SECURITYMANAGE_C_AR_MAJOR_VERSION != DCMDSL_SECURITYMANAGE_H_AR_MAJOR_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_SECURITYMANAGE_C_AR_MINOR_VERSION != DCMDSL_SECURITYMANAGE_H_AR_MINOR_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_SECURITYMANAGE_C_AR_PATCH_VERSION != DCMDSL_SECURITYMANAGE_H_AR_PATCH_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_SECURITYMANAGE_C_SW_MAJOR_VERSION != DCMDSL_SECURITYMANAGE_H_SW_MAJOR_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_SECURITYMANAGE_C_SW_MINOR_VERSION != DCMDSL_SECURITYMANAGE_H_SW_MINOR_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_SECURITYMANAGE_C_SW_PATCH_VERSION != DCMDSL_SECURITYMANAGE_H_SW_PATCH_VERSION)
  #error "DcmDsl_SecurityManage.c : Mismatch in Specification Major Version"
#endif

/********************************************************
 *******************************************************/
#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_SecCtrlType,DCM_VAR_NOINIT) Dcm_SecCtrl;  /*Security level management control block*/
#define DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <initial of Security level control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <Dcm_Init()>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DslInternal_InitSecCtrl(void)
{
	Std_ReturnType ret;
    P2CONST(Dcm_DspSecurityRowType,AUTOMATIC,DCM_CONST)pSecurityRow;
    uint8 SecCfgIndex;

    SchM_Enter_Dcm(Dcm_SecCtrl);
    Dcm_SecCtrl.Dcm_SubfunctionForSeed = 0u;
    /****@req DCM-FUNR-030[DCM033]****/
    Dcm_SecCtrl.Dcm_ActiveSec          = DCM_SEC_LEV_LOCKED;
    Dcm_SecCtrl.Dcm_NewSec             = DCM_SEC_LEV_LOCKED;
    Dcm_SecCtrl.Dcm_SecServiceState    = DCM_SERVICE_IDLE;

    for (SecCfgIndex = 0; SecCfgIndex < DCM_SECURITY_NUM; SecCfgIndex++)
    {
        pSecurityRow = &(Dcm_DspCfg.pDcm_DspSecurity->pDcm_DspSecurityRow[SecCfgIndex]);
        if (pSecurityRow != NULL_PTR)
        {
            /*SWS_Dcm_01157*/
            if ((TRUE == pSecurityRow->DcmDspSecurityAttemptCounterEnabled)
               &&((USE_ASYNCH_FNC == pSecurityRow->DcmDspSecurityUsePort)
               ||(USE_ASYNCH_CLIENT_SERVER == pSecurityRow->DcmDspSecurityUsePort)))
            {
                if (pSecurityRow->Dcm_GetSecurityAttemptCounterFnc != NULL_PTR)
                {
                    /*SWS_Dcm_01154*/
                    ret = pSecurityRow->Dcm_GetSecurityAttemptCounterFnc(DCM_INITIAL,&Dcm_SecCtrl.Dcm_FalseAcessCount[SecCfgIndex]);
                    if (E_NOT_OK == ret )
                    {
                        /*SWS_Dcm_01156*/
                        Dcm_SecCtrl.Dcm_FalseAcessCount[SecCfgIndex] = 0;
                    }
                    if(Dcm_SecCtrl.Dcm_FalseAcessCount[SecCfgIndex] >= pSecurityRow->DcmDspSecurityNumAttDelay)
                    {
                        Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecCurTimer[SecCfgIndex] = Frt_ReadOutMS();
                        Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecExpiredTimer[SecCfgIndex] = 
						(Dcm_DspCfg.pDcm_DspSecurity->pDcm_DspSecurityRow)[SecCfgIndex].DcmDspSecurityDelayTime;
                        Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecTimerState[SecCfgIndex] = DCM_SECTIMER_ON;
                    }
                }
                else
                {
                    Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecCurTimer[SecCfgIndex] = Frt_ReadOutMS();
                    Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecExpiredTimer[SecCfgIndex] =
                    (Dcm_DspCfg.pDcm_DspSecurity->pDcm_DspSecurityRow)[SecCfgIndex].DcmDspSecurityDelayTime;
                    Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecTimerState[SecCfgIndex] = DCM_SECTIMER_ON;
                }
            }
        }
    }

    SchM_Exit_Dcm(Dcm_SecCtrl);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Setting the security level>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <NewSec:To be changed the security level>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-029[DCM020]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE) DslInternal_SetSecurityLevel(Dcm_SecLevelType  NewSec)
{
    SchM_Enter_Dcm(Dcm_SecCtrl);
    Dcm_SecCtrl.Dcm_ActiveSec = NewSec;
    SchM_Exit_Dcm(Dcm_SecCtrl);

    SchM_Enter_Dcm(Dcm_MkCtrl);
    Dcm_MkCtrl.Dcm_ActiveSec  = NewSec;
    SchM_Exit_Dcm(Dcm_MkCtrl);

#if (STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
    /*check 2A is supported in NewSec*/
    Dcm_UDS0x2ACheckNewSecurity();
#endif

#if (DCM_DSP_DID_FOR_2F_NUM > 0)
    /*check 2F is supported in NewSec*/
    Dcm_UDS0x2FCheckNewSecurity(NewSec);
#endif
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Set SecurityAccess service process ,receives seed/key status>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Status:State to be modified>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DslInternal_SetSecurityAccessStatus(Dcm_SecServiceStateType Status)
{
    SchM_Enter_Dcm(Dcm_SecCtrl);
    Dcm_SecCtrl.Dcm_SecServiceState = Status;
    SchM_Exit_Dcm(Dcm_SecCtrl);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DslInternal_GetSecurityCfgBySecLevel(
        Dcm_SecLevelType  Dcm_SecLevel,
        P2VAR(uint8, AUTOMATIC, AUTOMATIC)SecCfgIndex)
{
    uint8 Index;
    uint8 SecurityRowNum;
    uint8 SecCfgLevel;
    boolean Flag = FALSE;
    Std_ReturnType ret;

    SecurityRowNum = Dcm_DspCfg.pDcm_DspSecurity->DcmDspSecurityRow_Num;
    for(Index=0;(Index<SecurityRowNum)&&(FALSE==Flag);Index++)
    {
        SecCfgLevel = (Dcm_DspCfg.pDcm_DspSecurity->pDcm_DspSecurityRow)[Index].DcmDspSecurityLevel;
        if(Dcm_SecLevel == SecCfgLevel)
        {
            *SecCfgIndex = Index;
            Flag = TRUE;
        }
    }

    if (TRUE == Flag)
    {
        ret = E_OK;
    }
    else
    {
        ret = E_NOT_OK;
    }

    return ret;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

