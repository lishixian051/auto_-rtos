/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <DcmDsl_CommManage.h>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <shushi>
 *  @date       <2018-3-20>
 */
/*============================================================================*/

#ifndef DCMDSL_COMMMANAGE_H
#define DCMDSL_COMMMANAGE_H
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCMDSL_COMMANAGE_H_AR_MAJOR_VERSION  4
#define DCMDSL_COMMANAGE_H_AR_MINOR_VERSION  2
#define DCMDSL_COMMANAGE_H_AR_PATCH_VERSION  2
#define DCMDSL_COMMANAGE_H_SW_MAJOR_VERSION  1
#define DCMDSL_COMMANAGE_H_SW_MINOR_VERSION  0
#define DCMDSL_COMMANAGE_H_SW_PATCH_VERSION  2

/**************************************************************/
#include "Dcm_Types.h"

/***************************************************************************************
 *************Communication Management sub-function data structure*****
 **************************************************************************************/
typedef  enum
{
   DCM_COMM_NO_COMMUNICATION      = 0, /* conmmunication is "NO Communication"status */
   DCM_COMM_SILENT_COMMUNICATION  = 1, /* conmmunication is "Silent Communication"status */
   DCM_COMM_FULL_COMMUNICATION    = 2  /* conmmunication is "Full Communication"status*/
}Dcm_CommStateType;

typedef  enum
{
	DCM_COMM_ACTIVE      = 0,
	DCM_COMM_NOT_ACTIVE  = 1,
}Dcm_ActiveDiagnosticType;

typedef struct
{
   Dcm_CommStateType  Dcm_CommState;
   Dcm_ActiveDiagnosticType Dcm_ActiveDiagnostic;
   NetworkHandleType DcmDslProtocolComMChannelId;
}Dcm_CommCtrlType;


#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern  VAR(Dcm_CommCtrlType,DCM_VAR_NOINIT)Dcm_CommCtrl[DCM_MAINCONNECTION_NUM];  /*Diagnostic Communication Control*/
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
/*********************************************************************************
 *****************Communication Management function declarations******************
 ********************************************************************************/
/*************************************************************************/
/*
 * Brief               <initialization of Comm Submodule>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)DslInternal_InitComMCtrl(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#endif /* DCMDSL_COMMMANAGE_H_ */
