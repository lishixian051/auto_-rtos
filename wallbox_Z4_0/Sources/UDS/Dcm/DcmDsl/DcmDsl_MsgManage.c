/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <DcmDsl_MsgManage.c>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <shushi>
 *  @date       <2018-3-20>
 */
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCMDSL_MSGMANAGE_C_AR_MAJOR_VERSION  4
#define DCMDSL_MSGMANAGE_C_AR_MINOR_VERSION  2
#define DCMDSL_MSGMANAGE_C_AR_PATCH_VERSION  2
#define DCMDSL_MSGMANAGE_C_SW_MAJOR_VERSION  1
#define DCMDSL_MSGMANAGE_C_SW_MINOR_VERSION  0
#define DCMDSL_MSGMANAGE_C_SW_PATCH_VERSION  2

/*=======[I N C L U D E S]===================================================*/
#include "Dcm_Include.h"

/*=======[V E R S I O N  C H E C K]===========================================*/
#if (DCMDSL_MSGMANAGE_C_AR_MAJOR_VERSION != DCMDSL_MSGMANAGE_H_AR_MAJOR_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_MSGMANAGE_C_AR_MINOR_VERSION != DCMDSL_MSGMANAGE_H_AR_MINOR_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_MSGMANAGE_C_AR_PATCH_VERSION != DCMDSL_MSGMANAGE_H_AR_PATCH_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_MSGMANAGE_C_SW_MAJOR_VERSION != DCMDSL_MSGMANAGE_H_SW_MAJOR_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_MSGMANAGE_C_SW_MINOR_VERSION != DCMDSL_MSGMANAGE_H_SW_MINOR_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSL_MSGMANAGE_C_SW_PATCH_VERSION != DCMDSL_MSGMANAGE_H_SW_PATCH_VERSION)
  #error "DcmDsl_MsgManage.c : Mismatch in Specification Major Version"
#endif



/******************************************/

#if(STD_ON == DCM_DEV_ERROR_DETECT)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType, DCM_CODE) Dcm_StartOfReception_CheckInputParameter(
		PduIdType id,
		PduLengthType TpSduLength,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) bufferSizePtr);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType, DCM_CODE) Dcm_CopyRxData_CheckInputParameter(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_VAR)  info);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/******************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static   FUNC(Std_ReturnType,DCM_CODE)Dcm_TpRxIndication_IsFuncAddress(PduIdType DcmRxPduId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/******************************************/
#if(STD_ON == DCM_DEV_ERROR_DETECT)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static   FUNC(Std_ReturnType,DCM_CODE)Dcm_CopyTxData_CheckInputParameter( PduIdType DcmTxPduId,
																	  P2CONST(PduInfoType,AUTOMATIC,DCM_VAR)PduInfoPtr,
																	  P2VAR(PduLengthType,AUTOMATIC,DCM_VAR)availableDataPtr,
																	  P2VAR(uint8,AUTOMATIC,DCM_VAR)pDetErrorId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/******************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static   FUNC(Std_ReturnType,DCM_CODE)Dcm_CopyTxData_CheckCtrlStatus(PduIdType DcmTxPduId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(Dcm_ServiceType, DCM_CODE) Dcm_ClassifyServiceType(Dcm_ProtocolType protocolType);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

static  FUNC(Std_ReturnType, DCM_CODE) Dcm_CopyRxData_CheckProtocol(PduIdType id,Dcm_ServiceType  ServiceType);
/********************************************************
 *******************Resource definition******************
********************************************************/
/*Static channel allocation of resources*/
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(uint8,DCM_VAR_POWER_ON_INIT)Dcm_Channel[DCM_CHANNEL_LENGTH] = {0u};
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_ChannelCtrlType,DCM_VAR_NOINIT)Dcm_ChannelCtrl[DCM_CHANNEL_NUM];/*Channel control block*/
VAR(Dcm_MsgCtrlType,DCM_VAR_NOINIT) Dcm_MsgCtrl[DCM_MSG_NUM]; 			/*Message control block, corresponding to uds service*/
VAR(Dcm_ConnectionCtrlType,DCM_VAR_NOINIT) Dcm_ConnectionCtrl[DCM_CONNECTION_NUM];
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_UDS_FUNC_ENABLED)
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
static   VAR(Dcm_FunctionMessageType,DCM_VAR_POWER_ON_INIT)Dcm_FunctionalMessage; /*Function addressing packets resources,power on init*/
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
static   VAR(boolean,DCM_VAR_POWER_ON_INIT)CheckProtocolFaild = FALSE; /*CheckProtocolFaild Then Rxindication is come shall send NRC*/
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
static   VAR(boolean,DCM_VAR_POWER_ON_INIT)StopProtocolFaild = FALSE; /*StopProtocolFaild Then Rxindication is come shall send NRC*/
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_UDS_FUNC_ENABLED)
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_OBDMessageType,DCM_VAR_POWER_ON_INIT)Dcm_OBDMessage;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif
/****************************** implementations ********************************/
/*************************************************************************/
/*
 * Brief               Utility function:copy memory data.
 * ServiceId           N/A
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      Source memory address to copy from
 *                     Dest memory address to copy to
 *                     Length memory length to copy
 * Param-Name[out]     N/A
 * Param-Name[in/out]  N/A
 * Return              N/A
 * PreCondition        N/A
 * CallByAPI           CanTp Internal
 */
/*************************************************************************/
FUNC(void, DCM_CODE)
Dcm_MemoryCopy(
    P2CONST(void, AUTOMATIC, DCM_APPL_CONST) Source,
    P2VAR(void, AUTOMATIC, DCM_APPL_DATA) Dest,
    uint16 Length)
{
    uint8 * pDest = (uint8 *)Dest;
    const uint8 * pSrc = (const uint8 *)Source;
    while (Length > 0u)
    {
        Length--;
        pDest[Length] = pSrc[Length];
    }
    return;
}
/*************************************************************************/
/*
 * Brief               <Initializing the corresponding channel control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ChannelCtrlId:channel control block ID>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <Dcm_Init()>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DslInternal_InitChannelCtrl(uint8 ChannelCtrlId)
{
    if(ChannelCtrlId>=DCM_CHANNEL_NUM)
    {
        return E_NOT_OK;
    }
    SchM_Enter_Dcm(Dcm_ChannelCtrl);
    Dcm_ChannelCtrl[ChannelCtrlId].Dcm_ChannelCfgIndex = DCM_INVALID_UINT8;
    Dcm_ChannelCtrl[ChannelCtrlId].Dcm_ChannelRxState  = DCM_CH_IDLE;
    Dcm_ChannelCtrl[ChannelCtrlId].Dcm_ChannelTxState  = DCM_CH_IDLE;
    Dcm_ChannelCtrl[ChannelCtrlId].Dcm_BufferCunrentPosition = 0u;
    Dcm_ChannelCtrl[ChannelCtrlId].Dcm_BufferErasePosition = 0u;
    SchM_Exit_Dcm(Dcm_ChannelCtrl);
    Dcm_FunctionalMessage.Length = 0u;
    Dcm_OBDMessage.Length = 0u;
    return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <initialization message control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <MsgCtrlId:message control block ID Index>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <Dcm_Init()>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DslInternal_InitMsgCtrl(uint8  MsgCtrlId)
{
    if(MsgCtrlId >= DCM_MSG_NUM)
    {
        return E_NOT_OK;
    }
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    Dcm_MsgCtrl[MsgCtrlId].SID  = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].Subfunction = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].NRC  = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].SendFlag = FALSE;
    Dcm_MsgCtrl[MsgCtrlId].RspStyle     = DCM_POS_RSP;
    Dcm_MsgCtrl[MsgCtrlId].DcmTxPduId   = DCM_INVALID_PDUID;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_RxCtrlChannelIndex   = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex   = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.DcmRxPduId    = DCM_INVALID_PDUID;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.pReqData      = NULL_PTR;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ReqDataLen    = 0;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData      = NULL_PTR;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen    = DCM_INVALID_UINT32;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.MsgAddInfo.ReqType = (uint8)DCM_PHYSICAL;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.MsgAddInfo.SuppressPosResponse = FALSE;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.MsgAddInfo.CancelOperation     = FALSE;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen = DCM_INVALID_UINT32;
    Dcm_MsgCtrl[MsgCtrlId].MsgContext.IdContext     = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2CurTimer     = DCM_INVALID_UINT32;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2ExpiredTimer = DCM_INVALID_UINT32;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.PendingNum    = 0u;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2State   = DCM_P2TIMER_OFF;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState = DCM_MSG_WAIT;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_Ret = E_NOT_OK;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus = DCM_INITIAL;
#if (STD_ON == DCM_GENERIC_CONNECTION)
    Dcm_MsgCtrl[MsgCtrlId].Dcm_MetaData[0] = DCM_INVALID_UINT8;
    Dcm_MsgCtrl[MsgCtrlId].Dcm_MetaData[1] = DCM_INVALID_UINT8;
#endif
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <initialization module control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous/Asynchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <description...>
 * CallByAPI           <Dcm_Init()>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DslInternal_InitMkCtrl(void)
{
    SchM_Enter_Dcm(Dcm_MkCtrl);
    Dcm_MkCtrl.Dcm_ActiveSes 		= DCM_DEFAULT_SESSION;
    Dcm_MkCtrl.Dcm_ActiveSec 		= DCM_SEC_LEV_LOCKED;
    Dcm_MkCtrl.Dcm_ActiveProtocol 	= DCM_NO_PROTOCOL;
    Dcm_MkCtrl.Dcm_ActiveNetwork    = DCM_INVALID_UINT8;
    Dcm_MkCtrl.Dcm_MkState   		= DCM_ON;
    SchM_Exit_Dcm(Dcm_MkCtrl);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Reset protocol control block members MsgCtrlIndex +
 *                      Reset corresponding message control block +
 *                      Reset corresponding receive / transmit channel control block>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:protocol control block ID>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <>
 * CallByAPI           <>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DslInternal_ResetResource(uint8  ProtocolCtrlId)
{
   PduIdType TxPduId;
   uint8  ConnectionCfgCtrlId;
   NetworkHandleType NetworkChannel;
   uint8    MsgCtrlIndexx;
   uint8    RxChannelCtrlIndex;
   uint8    TxChannelCtrlIndex;
   uint8    Index;
   boolean  Flag;

   SchM_Enter_Dcm(Dcm_ProtocolCtrl);
   MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
   SchM_Exit_Dcm(Dcm_ProtocolCtrl);

   SchM_Enter_Dcm(Dcm_MsgCtrl);
   TxPduId       = Dcm_MsgCtrl[MsgCtrlIndexx].DcmTxPduId;
   SchM_Exit_Dcm(Dcm_MsgCtrl);

   ConnectionCfgCtrlId = Dsl_Protocol_Connection_TxCfg[TxPduId].DcmDslParentConnectionCtrlId;
   NetworkChannel  = Dcm_MkCtrl.Dcm_ActiveNetwork;
   /**********************************************************/
   /*ComM Inactive processing*/
   /****@req DCM-FUNR-059[DCM170]****/
   if(DCM_DEFAULT_SESSION == Dcm_MkCtrl.Dcm_ActiveSes)
   {
       /****@req DCM-FUNR-053[SWS_Dcm_00164]****/
       Flag = FALSE;
       for(Index=0;(Index<DCM_MSG_NUM)&&(FALSE==Flag);Index++)
       {
          if((ProtocolCtrlId != Index)
              &&(DCM_MSG_WAIT != Dcm_MsgCtrl[Index].Dcm_MsgState))
          {
              Flag = TRUE;
          }
       }
       if(FALSE == Flag)
       {
           /*Notice ComM exit "FULL Communication"*/
    	   ComM_DCM_InactiveDiagnostic(NetworkChannel);
    	   Dcm_MkCtrl.Dcm_ActiveNetwork = DCM_INVALID_UINT8;
       }
   }
   /*****************S3Serer timer****************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
   /****@req DCM-FUNR-035[SWS_Dcm_00141]****/
   if(DCM_DEFAULT_SESSION != Dcm_MkCtrl.Dcm_ActiveSes)
   {
       Flag = FALSE;
       for(Index=0;(Index<DCM_MSG_NUM)&&(FALSE==Flag);Index++)
       {
          if((ProtocolCtrlId != Index)
              &&(DCM_MSG_WAIT != Dcm_MsgCtrl[Index].Dcm_MsgState))
          {
              Flag = TRUE;
          }
       }
       if(FALSE == Flag)
       {
           /*restart s3timer */
          DslInternal_S3ServerStart();
       }
   }
#endif
   /*****************Reset Message*****************/
   SchM_Enter_Dcm(Dcm_MsgCtrl);
   RxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_RxCtrlChannelIndex;
   TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_TxCtrlChannelIndex;
   SchM_Exit_Dcm(Dcm_MsgCtrl);

   SchM_Enter_Dcm(Dcm_ProtocolCtrl);
   Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex = DCM_INVALID_UINT8;
   SchM_Exit_Dcm(Dcm_ProtocolCtrl);

   (void)DslInternal_InitConnectionCtrl(ConnectionCfgCtrlId);
    (void)DslInternal_InitMsgCtrl(MsgCtrlIndexx);
    /****@req DCM-FUNR-003[DCM241]****/
   (void)DslInternal_InitChannelCtrl(RxChannelCtrlIndex);
   (void)DslInternal_InitChannelCtrl(TxChannelCtrlIndex);
   return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
/************************************************************************/
static FUNC(Dcm_ServiceType, DCM_CODE) Dcm_ClassifyServiceType(Dcm_ProtocolType protocolType)
{
	Dcm_ServiceType serviceType;
	switch (protocolType)
	{
		case DCM_OBD_ON_CAN:
		case DCM_OBD_ON_FLEXRAY:
		case DCM_OBD_ON_IP:
		{
			serviceType = DCM_OBD;
			break;
		}
		case DCM_UDS_ON_CAN:
		case DCM_UDS_ON_FLEXRAY:
		case DCM_UDS_ON_IP:
		{
			serviceType = DCM_UDS;
			break;
		}
		case DCM_ROE_ON_CAN:
		case DCM_ROE_ON_FLEXRAY:
		case DCM_ROE_ON_IP:
		{
			serviceType = DCM_ROE;
			break;
		}
		case DCM_PERIODIC_ON_CAN:
		case DCM_PERIODIC_ON_FLEXRAY:
		case DCM_PERIODIC_ON_IP:
		{
			serviceType = DCM_PERIODIC;
			break;
		}
		default:
			serviceType = DCM_NO_SERVICE;
			break;
	}
	return serviceType;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_DEV_ERROR_DETECT)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType, DCM_CODE) Dcm_StartOfReception_CheckInputParameter(
		PduIdType id,
		PduLengthType TpSduLength,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) bufferSizePtr)
{
	uint8  DetErrorId;
	Std_ReturnType ret = E_NOT_OK;

    /*check whether Dcm is initialized*/
    if(DCM_ON != (Dcm_MkCtrl.Dcm_MkState))
    {
        DetErrorId = DCM_E_UNINIT;
        ret = E_NOT_OK;
    }
    else if (NULL_PTR == bufferSizePtr)
    {
    	/*Entry parameter validity checks*/
    	DetErrorId = DCM_E_PARAM_POINTER;
    	ret = E_NOT_OK;
    }
    else if (((id >= DCM_DSL_RX_ID_NUM) || (id < 0u))
    		||(TpSduLength < 0u))
    {
    	/*Entry parameter validity checks*/
		DetErrorId = DCM_E_PARAM;
		ret = E_NOT_OK;
    }
    else
    {
    	ret = E_OK;
    }

    if (ret != E_OK)
    {
    	Det_ReportError(DCM_MODULE_ID, DCM_INSTANCE_ID, DCM_STARTOFRECEPTION_ID, DetErrorId);
    }

    return ret;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType, DCM_CODE) Dcm_CopyRxData_CheckInputParameter(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_VAR)  info)
{
	uint8  DetErrorId;
    Std_ReturnType ret = E_NOT_OK;

	/*The DCM module status check*/
	if(DCM_ON != (Dcm_MkCtrl.Dcm_MkState))
	{
		DetErrorId = DCM_E_UNINIT;
		ret = E_NOT_OK;
	}
	else if((NULL_PTR == info)
			||((id >= DCM_DSL_RX_ID_NUM) || (id < 0u)))
	{
		/*Parameter validation*/
		DetErrorId = DCM_E_PARAM;
		ret = E_NOT_OK;
	}
	else
	{
		ret = E_OK;
	}

	if(E_NOT_OK == ret)
	{
		Det_ReportError(DCM_MODULE_ID,
						DCM_INSTANCE_ID,
						DCM_COPYRXDATA_ID,
						DetErrorId);
	}

	return  ret;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif


#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType, DCM_CODE) Dcm_CopyRxData_CheckProtocol(PduIdType id,Dcm_ServiceType  ServiceType)
{
    uint8  ConnectionCfgId;
    uint8  ProtocolCfgId;
    uint8  FindProtocolCfgId;
    uint8 iloop = 0;
    boolean FindProtocolValid = FALSE;
    uint8 OldProtocolPri;
    uint8 NewProtocolPri;
#if (PREEMPTION_PROTOCOL_CANCEL_SUPPORT == STD_ON)
    PduIdType    DcmTxPduIdx;
#endif
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[id].DcmDslParentConnectionCtrlId;
    ProtocolCfgId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
    if(DCM_PROTOCOL_VALID == Dcm_ProtocolCtrl[ProtocolCfgId].Dcm_ProtocolState)
    {
        /*protocol is already started*/
        return E_OK;
    }
    /*check all protocol whether have one protocol had already started*/
    while ((iloop < DCM_DSLPROTOCOLROW_NUM_MAX) && (FindProtocolValid == FALSE))
    {
        FindProtocolCfgId  = Dsl_Protocol_ConnectionCfg[iloop].DcmDslParentProtocolRowCtrlId;
        if (DCM_PROTOCOL_VALID == Dcm_ProtocolCtrl[FindProtocolCfgId].Dcm_ProtocolState)
        {
            FindProtocolValid = TRUE;/*have finded a starting protocol*/
        }
        iloop++;
    }
    if (FindProtocolValid == TRUE)
    {
        if (ServiceType == DCM_OBD)
        {
            if (Dcm_OBDMessage.Buffer[0] == 0x3Eu)
            {/*[SWS_Dcm_01144]*/
                return E_NOT_OK;
            }
            Dcm_MsgCtrl[ProtocolCfgId].SID = Dcm_OBDMessage.Buffer[0];
        }
        else
        {
            if ((Dcm_MsgCtrl[ProtocolCfgId].MsgContext.pReqData)[0] == 0x3Eu)
            {/*[SWS_Dcm_01144]*/
                return E_NOT_OK;
            }
            Dcm_MsgCtrl[ProtocolCfgId].SID = (Dcm_MsgCtrl[ProtocolCfgId].MsgContext.pReqData)[0];
        }
        OldProtocolPri = Dcm_ProtocolCtrl[FindProtocolCfgId].ProtocolPri;
        NewProtocolPri = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgId].DcmDslProtocolPriority;
        if (NewProtocolPri < OldProtocolPri)
        {
            if(E_OK != DslInternal_ProtocolStop(FindProtocolCfgId))
            {
            	StopProtocolFaild = TRUE;
            	return E_NOT_OK;
            }
            else
            {/* protocol Preemption successful*/
#if (PREEMPTION_PROTOCOL_CANCEL_SUPPORT == STD_ON)
                DcmTxPduIdx   = Dcm_MsgCtrl[Dcm_ProtocolCtrl[FindProtocolCfgId].MsgCtrlIndex].DcmTxPduId;
                switch(Dcm_MsgCtrl[FindProtocolCfgId].Dcm_MsgState)
                {
                    case DCM_MSG_TRANSMISSION:
                        (void)PduR_DcmCancelTransmit(DcmTxPduIdx);/*[SWS_Dcm_00079][SWS_Dcm_00460]*/
                        break;
                    case DCM_MSG_RECEIVED:
                        (void)PduR_DcmCancelReceive(DcmTxPduIdx);/*[SWS_Dcm_00575][SWS_Dcm_00576]*/
                        break;
                    default:
                        break;
                }
#endif
                SchM_Enter_Dcm(Dcm_MsgCtrl);
                Dcm_MsgCtrl[Dcm_ProtocolCtrl[FindProtocolCfgId].MsgCtrlIndex].Dcm_OpStatus = DCM_CANCEL;/*[SWS_Dcm_01046] */
                SchM_Exit_Dcm(Dcm_MsgCtrl);
                (void)DslInternal_ResetResource(FindProtocolCfgId);
                return E_OK;
            }
        }
        else
        {
            /*[SWS_Dcm_00625] [SWS_Dcm_00728]*/
            if (((Dcm_MsgCtrl[FindProtocolCfgId].Dcm_MsgState == DCM_MSG_WAIT)
                || (Dcm_MsgCtrl[FindProtocolCfgId].Dcm_MsgState == DCM_MSG_CONFIRMATION))
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
            && (DCM_DEFAULT_SESSION == Dcm_MkCtrl.Dcm_ActiveSes)
#endif
            )
            {
                if(E_OK != DslInternal_ProtocolStop(FindProtocolCfgId))
                {
                	StopProtocolFaild = TRUE;
                	return E_NOT_OK;
                }
                else
                {/* protocol Preemption successful*/
                    (void)DslInternal_ResetResource(FindProtocolCfgId);
                    return E_OK;
                }
            }
            return E_NOT_OK;
        }
    }
    return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(BufReq_ReturnType, DCM_CODE) Dcm_StartOfReception(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_VAR)  info,
		PduLengthType TpSduLength,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) bufferSizePtr)
{
    uint8  ConnectionCfgCtrlId;
    uint8  ProtocolCfgCtrlId;
    uint8  MsgCtrlIndex;
    uint8  RxChannelCfgCtrlId;
    uint8  TxChannelCfgCtrlId;
    Dcm_ProtocolType ProtocolId;
    Dcm_ServiceType ServiceType;
#if (STD_ON == DCM_GENERIC_CONNECTION)
    uint8  MetaDataLen;
#endif
    uint16 Offset;
	uint16 len = 0;
	Dcm_DslProtocolRxAddrType RxAddrType;
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    Std_ReturnType ret;
#endif
    BufReq_ReturnType bufRet = BUFREQ_E_NOT_OK;

    /************************************************/
	#if(STD_ON == DCM_DEV_ERROR_DETECT)
	ret = Dcm_StartOfReception_CheckInputParameter(id, TpSduLength, bufferSizePtr);
	if (E_OK == ret)
	#endif
	{
		ConnectionCfgCtrlId = Dsl_Protocol_Connection_RxCfg[id].DcmDslParentConnectionCtrlId;
		ProtocolCfgCtrlId   = Dsl_Protocol_ConnectionCfg[ConnectionCfgCtrlId].DcmDslParentProtocolRowCtrlId;
	    MsgCtrlIndex        = ProtocolCfgCtrlId;
	    RxChannelCfgCtrlId  = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolRxBufferRef->Dcm_DslBufferId;
	    TxChannelCfgCtrlId  = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolTxBufferRef->Dcm_DslBufferId;
		/************************************************/
		ProtocolId = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolID;
	    ServiceType = Dcm_ClassifyServiceType(ProtocolId);
		switch (ServiceType)
		{
			#if (DCM_UDS_FUNC_ENABLED == STD_ON)
			case DCM_UDS:
			{
				len = (Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].Dcm_DslBufferSize;
				break;
			}
			#endif
			#if (DCM_OBD_FUNC_ENABLED == STD_ON)
			case DCM_OBD:
			{
                len = 8u;
                break;
            }
            #endif
            default:
                break;
        }

        /*Compare application buffer length and configuration of the channel length*/
        if(len < TpSduLength)
        {
            /****@req DCM-APIR-014[DCM444]****/
            bufRet = BUFREQ_E_OVFL;/*before modifying: BUFREQ_E_OVEL*/
        }
        else if (0u == TpSduLength)
        {
            /*SWS_Dcm_00642*/
            bufRet = BUFREQ_OK;
        }
        else
        {
            #if(STD_ON == DCM_SESSION_FUNC_ENABLED)
            /*Start of a multi-frame request message or Reception of single-frame request message.*/
            /*Stop S3Server timer*/
            if(DCM_DEFAULT_SESSION != Dcm_MkCtrl.Dcm_ActiveSes)
            {
                /*SWS_Dcm_00141*/
                DslInternal_S3ServerStop();
            }
            #endif

			/************************************************/
			RxAddrType = Dsl_Protocol_Connection_RxCfg[id].DcmDslProtocolRxAddrType;
			if ((Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_ChannelRxState  == DCM_CH_OCCUPIED)
					||(Dcm_ChannelCtrl[TxChannelCfgCtrlId].Dcm_ChannelTxState == DCM_CH_OCCUPIED))
			{
				if (FALSE == Dcm_ConnectionCtrl[ConnectionCfgCtrlId].Dcm_ConnectionActive)
				{
					 /*new diagnostic request with different connection*/
					if (DCM_FUNCTIONAL == RxAddrType)
					{
						/*new diagnostic request is test present or OBD service for function addressing*/
						if (DCM_DEFAULT_SESSION != Dcm_MkCtrl.Dcm_ActiveSes)
						{
							/*the current session is a non-default session*/
							/*SWS_Dcm_01145,SWS_Dcm_01146*/
							bufRet = BUFREQ_OK;
						}
					}
					else
					{
						if (TRUE == Dcm_DslCfg.pDcmDslDiagRespCfg->DcmDslDiagRespOnSecondDeclinedRequest)
						{
							/*SWS_Dcm_00789*/
							/*the Dcm respond with a NRC 0x21*/
							Dcm_ProtocolCtrl[ProtocolCfgCtrlId].MsgCtrlIndex = MsgCtrlIndex;
							(void)DsdInternal_SetNrc(ProtocolCfgCtrlId,DCM_E_BUSYREPEATREQUEST);
							/*SWS_Dcm_00788*/
							bufRet = BUFREQ_OK;
						}
						else
						{
							/*SWS_Dcm_00790*/
							bufRet = BUFREQ_E_NOT_OK;
						}
					}
				}
				else
				{
					/*SWS_Dcm_00557,new diagnostic request with the same DcmDslConnection*/
					if (DCM_FUNCTIONAL == RxAddrType)
					{
						/*functional addressing eg. concurrent Tester Present*/
						bufRet = BUFREQ_OK;
					}
					else
					{
						bufRet = BUFREQ_E_NOT_OK;
					}
				}
			}
			else
			{
				/*no diagnostic request is processing*/
				bufRet = BUFREQ_OK;
			}
		}/*ENDOF len < TpSduLength*/

		if (BUFREQ_OK == bufRet)
		{
			if (info != NULL_PTR)
			{
				if (len >= info->SduLength)
				{
					*bufferSizePtr = len;
				}
				else
				{
					*bufferSizePtr = 0;
				}
			}
			else
			{
				*bufferSizePtr = len;
			}
		}

		if (info != NULL_PTR)
		{
			#if (STD_ON == DCM_GENERIC_CONNECTION)
            MetaDataLen = Dsl_Protocol_Connection_RxCfg[id].DcmDslMetaDataLength;
			if ((MetaDataLen > 0u)&&(MetaDataLen <= info->SduLength))
			{
				/*save meta data at the end of sdu in dcm buffer*/
				Dcm_MemoryCopy( &info->SduDataPtr[info->SduLength - MetaDataLen],
								&Dcm_Channel[TpSduLength],
								MetaDataLen);
				/*SWS_Dcm_00848,SWS_Dcm_00849,exchange the site of meta data for response later*/
				Dcm_MsgCtrl[MsgCtrlIndex].Dcm_MetaData[0] = Dcm_Channel[TpSduLength + 1u];
				Dcm_MsgCtrl[MsgCtrlIndex].Dcm_MetaData[1] = Dcm_Channel[TpSduLength];
			}
			#endif
		}

		if (bufRet == BUFREQ_OK)
		{
            Offset = (Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].offset;

            if (info != NULL_PTR)
            {
                if (Dsl_Protocol_Connection_RxCfg[id].DcmDslMetaDataLength < info->SduLength)
                {
                    /************************************************/
                    RxAddrType = Dsl_Protocol_Connection_RxCfg[id].DcmDslProtocolRxAddrType;
                    if(DCM_FUNCTIONAL == RxAddrType)
                    {
                        switch (ServiceType)
                        {
                            #if(STD_ON == DCM_UDS_FUNC_ENABLED)
                            case DCM_UDS:
                            {
                                   /*The UDS protocol functions addressing message, bypass treatment*/
                                    Dcm_MemoryCopy(
                                                    info->SduDataPtr,
                                                    &(Dcm_FunctionalMessage.Buffer[0]),
                                                    info->SduLength);

                                   Dcm_FunctionalMessage.Length = info->SduLength;/*save request length*/
                                   bufRet = BUFREQ_OK;
                                break;
                            }
                            #endif
                            #if(STD_ON == DCM_UDS_FUNC_ENABLED)
                            case DCM_OBD:
                            {
                                    /*The OBD protocol only have functions addressing message*/
                                    Dcm_MemoryCopy(
                                                    info->SduDataPtr,
                                                    &(Dcm_OBDMessage.Buffer[0]),
                                                    info->SduLength);
                                    Dcm_OBDMessage.Length = info->SduLength;/*save request length*/
                                    bufRet = BUFREQ_OK;
                                break;
                            }
                            #endif
                            default:
                                break;
                        }
                    }
                    else
                    {
                        /*SWS_Dcm_00342, lock buffer*/
                        Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_ChannelRxState = DCM_CH_OCCUPIED;

                        /*SWS_Dcm_00443*/
                        Dcm_MemoryCopy( info->SduDataPtr,
                                        &Dcm_Channel[Offset],
                                        info->SduLength);
                        Offset = Offset + info->SduLength;
                        bufRet = BUFREQ_OK;
                    }
                }
            }

            Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_BufferCunrentPosition = Offset;
            Dcm_ConnectionCtrl[ConnectionCfgCtrlId].Dcm_ConnectionActive = TRUE;
            SchM_Enter_Dcm(Dcm_MkCtrl);
            Dcm_MkCtrl.Dcm_ActiveNetwork = Dsl_Protocol_ConnectionCfg[ConnectionCfgCtrlId].pDcmDslMainConnection->DcmDslProtocolComMChannelId;
            SchM_Exit_Dcm(Dcm_MkCtrl);

            SchM_Enter_Dcm(Dcm_MsgCtrl);
            Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.OldReqDataLen =
                    Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.ReqDataLen;
            Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.ReqDataLen = TpSduLength;
            Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.pReqData   = &Dcm_Channel[(Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].offset];
            SchM_Exit_Dcm(Dcm_MsgCtrl);
		}
	}/*ENDOF DET*/

    return  bufRet;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(BufReq_ReturnType,DCM_CODE)Dcm_CopyRxData(PduIdType id,
											P2CONST(PduInfoType, AUTOMATIC, DCM_VAR)  info,
											P2VAR(PduLengthType,AUTOMATIC,DCM_VAR) bufferSizePtr)
{
    uint8  Sid = 0;
    uint8  SubFunction = 0;
    uint8  ConnectionCfgId;
    uint8  ProtocolCfgId;
    uint8  RxChannelCfgCtrlId;
    uint16 Offset;
    uint16 AvailableBufferSize;
    Dcm_DslProtocolRxAddrType RxAddrType;
    Dcm_ProtocolType  ProtocolRowID;
    Dcm_ServiceType  ServiceType;
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    Std_ReturnType ret;
#endif
    BufReq_ReturnType bufRet = BUFREQ_E_NOT_OK;

	#if(STD_ON == DCM_DEV_ERROR_DETECT)
    ret = Dcm_CopyRxData_CheckInputParameter(id, info);
	if (E_OK == ret)
	#endif
	{
		/************************************************/
		ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[id].DcmDslParentConnectionCtrlId;
		ProtocolCfgId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
		RxChannelCfgCtrlId = Dsl_ProtocolRowCfg[ProtocolCfgId].DcmDslProtocolRxBufferRef->Dcm_DslBufferId;
		/************************************************/
		RxAddrType = Dsl_Protocol_Connection_RxCfg[id].DcmDslProtocolRxAddrType;
		ProtocolRowID = ((Dcm_DslCfg.pDcmDslProtocol)->pDcmDslProtocolRow)[ProtocolCfgId].DcmDslProtocolID;
		ServiceType = Dcm_ClassifyServiceType(ProtocolRowID);
		switch (ServiceType)
		{
			#if(STD_ON == DCM_UDS_FUNC_ENABLED)
			case DCM_UDS:
			{
				if(DCM_FUNCTIONAL == RxAddrType)
				{
				   /*The UDS protocol functions addressing message, bypass treatment*/
					Dcm_MemoryCopy(
									info->SduDataPtr,
									&(Dcm_FunctionalMessage.Buffer[Dcm_FunctionalMessage.Length]),
									info->SduLength);

				   Dcm_FunctionalMessage.Length += info->SduLength;/*save request length*/
                   Sid = Dcm_FunctionalMessage.Buffer[0];
    	           SubFunction = Dcm_FunctionalMessage.Buffer[1];
				   bufRet = BUFREQ_OK;
				}
				break;
			}
			#endif
            #if(STD_ON == DCM_UDS_FUNC_ENABLED)
            case DCM_OBD:
            {
                if (DCM_FUNCTIONAL == RxAddrType)
                {
                    /*The OBD protocol only have functions addressing message*/
                    Dcm_MemoryCopy(
                                    info->SduDataPtr,
                                    &(Dcm_OBDMessage.Buffer[Dcm_OBDMessage.Length]),
                                    info->SduLength);
                    Dcm_OBDMessage.Length += info->SduLength;/*save request length*/
                    bufRet = BUFREQ_OK;
                }
                break;
            }
            #endif
            default:
                break;
        }
        /***********************************************/
        /*check Protocol*/
        if (E_OK != Dcm_CopyRxData_CheckProtocol(id,ServiceType))
        {
            if (Dcm_DslCfg.pDcmDslDiagRespCfg->DcmDslDiagRespOnSecondDeclinedRequest == TRUE)
            {/*[SWS_Dcm_00727] [SWS_Dcm_00729] */
            	CheckProtocolFaild = TRUE;
            }
            bufRet = BUFREQ_E_NOT_OK;
        }
        /************************************************/
        if(!((0x3Eu == Sid) &&(0x80u == SubFunction) &&(0x02u == Dcm_FunctionalMessage.Length)))
        {
            Offset = Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_BufferCunrentPosition;
            AvailableBufferSize = Dcm_DslCfg.pDcmChannelCfg[RxChannelCfgCtrlId].Dcm_DslBufferSize - (Offset - (Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].offset);
            /*SWS_Dcm_00996*/
            if (0u == info->SduLength)
            {
                *bufferSizePtr = AvailableBufferSize;
                bufRet = BUFREQ_OK;
            }
            else if(info->SduLength > AvailableBufferSize)
            {
                bufRet = BUFREQ_E_NOT_OK;
            }
            else
            {
                /*SWS_Dcm_00342, lock buffer*/
                Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_ChannelRxState = DCM_CH_OCCUPIED;

                /*SWS_Dcm_00443*/
                Dcm_MemoryCopy( info->SduDataPtr,
                                &Dcm_Channel[Offset],
                                info->SduLength);
                Offset = Offset + info->SduLength;
                Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_BufferCunrentPosition = Offset;
                *bufferSizePtr = Dcm_DslCfg.pDcmChannelCfg[RxChannelCfgCtrlId].Dcm_DslBufferSize - (Offset - (Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].offset);
                bufRet = BUFREQ_OK;
            }
        }
        else
        {
        	Dcm_MsgCtrl[ProtocolCfgId].MsgContext.ReqDataLen =
        			Dcm_MsgCtrl[ProtocolCfgId].MsgContext.OldReqDataLen;
        	Dcm_MsgCtrl[ProtocolCfgId].MsgContext.OldReqDataLen = 0;

        }
	}

    return  bufRet;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*********************************************************
 ********************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)Dcm_TpRxIndication_IsFuncAddress(PduIdType DcmRxPduId)
{
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
    uint8  Sid;
    uint16  Index;
    uint8  SubFunction;
#endif
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    uint8  Flag;
#endif
    uint16 Offset;
    uint8  ProtocolCfgId;
    uint8  ConnectionCfgId;
    uint8  MsgCtrlIndex;
    uint8  RxChannelCfgCtrlId;
    Dcm_DslProtocolRxAddrType DslProtocolRxAddrType;

    /*****************************************************/
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[DcmRxPduId].DcmDslParentConnectionCtrlId;
    ProtocolCfgId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
	MsgCtrlIndex       = ProtocolCfgId;
	RxChannelCfgCtrlId = Dsl_ProtocolRowCfg[ProtocolCfgId].DcmDslProtocolRxBufferRef->Dcm_DslBufferId;

    /*****************************************************/
    DslProtocolRxAddrType = Dsl_Protocol_Connection_RxCfg[DcmRxPduId].DcmDslProtocolRxAddrType;
    Offset = (Dcm_DslCfg.pDcmChannelCfg)[RxChannelCfgCtrlId].offset;

    if(DCM_FUNCTIONAL == DslProtocolRxAddrType)
    {
#if(STD_ON == DCM_UDS_FUNC_ENABLED)
    	/****@req DCM-FUNR-004[SWS_Dcm_00112]****/
    	/****@req DCM-FUNR-005[SWS_Dcm_00113]****/
    	/*SWS_Dcm_01168*/
    	Sid = Dcm_FunctionalMessage.Buffer[0];
    	SubFunction = Dcm_FunctionalMessage.Buffer[1];
    	if((0x3Eu == Sid) &&(0x80u == SubFunction) &&(0x02u == Dcm_FunctionalMessage.Length))
    	{
        	/*This is TesterPresent Request */
		#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    		if(DCM_DEFAULT_SESSION != Dcm_MkCtrl.Dcm_ActiveSes)
    		{
    			Flag = FALSE;
    			for(Index=0;(Index<DCM_MSG_NUM)&&(FALSE==Flag);Index++)
    			{
    				if(DCM_MSG_WAIT != Dcm_MsgCtrl[Index].Dcm_MsgState)
    				{
    					Flag = TRUE;
    				}
    			}
    			if(FALSE == Flag)
    			{
    				/*restart S3timer*/
    				DslInternal_S3ServerStart();
    			}
    		}
		  #endif
        	Dcm_FunctionalMessage.Length = 0U;
    		return(E_NOT_OK);
    	}
#endif
        /*(SID=0x3E + subfunction(0x00)) || (SID != 0x3E),Need to be submitted to Dsd layer handle*/
        if(DCM_MSG_WAIT != Dcm_MsgCtrl[MsgCtrlIndex].Dcm_MsgState)
        {
            /*The same protocol message being processed*/
            return(E_NOT_OK);
        }
        /*The main channel is idle,copy data to main channel*/
        for(Index=0u;Index<(Dcm_FunctionalMessage.Length);Index++)
        {
            Dcm_Channel[Offset + Index] = Dcm_FunctionalMessage.Buffer[Index];
        }
        Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.ReqDataLen = (Dcm_MsgLenType)Dcm_FunctionalMessage.Length;
        Dcm_MsgCtrl[MsgCtrlIndex].MsgContext.pReqData   = &Dcm_Channel[Offset];
    }
    return(E_OK);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <This is called by the PduR to indicate the competion of a reception>
 * ServiceId           <0x03>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <DcmRxPduId:ID of DCM I-PDU that has been received
 *                      Result: The result of the diagnostic request message received>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <this function must be called after call of Dcm_StartOfReception() and Dcm_CopyRxData()>
 * CallByAPI           <None>
 */
/*************************************************************************/
/****@req Dcm_APIR_016[DCM093]****/
/****@req DCM-APIR-018[DCM345]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)Dcm_TpRxIndication(PduIdType id, Std_ReturnType result)
{
    P2CONST(Dcm_DslConnectionType,AUTOMATIC,DCM_CONST)pDslConnection;
	Dcm_ProtocolType ProtocolId;
	Dcm_ServiceType  ServiceType;
    Dcm_ChannelStateType  TxChannelTxState;
    uint8  ProtocolCfgCtrlId;
    uint8  ConnectionCfgId;
    uint8  MsgCtrlIndexx;
    uint8  RxChannelCfgCtrlId;
    uint8  TxChannelCfgCtrlId;
    Dcm_DslProtocolRxAddrType DslProtocolRxAddrType;
    NetworkHandleType NetworkChannel;
    Std_ReturnType  ret;
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    uint8  DetErrorId;
#endif
    uint8    Index;
	#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    boolean  Flag = FALSE;
	#endif

    /************************************************/
	/*To determine whether the DCM module initialization*/
    if(DCM_ON != (Dcm_MkCtrl.Dcm_MkState))
    {
#if(STD_ON == DCM_DEV_ERROR_DETECT)
        DetErrorId = DCM_E_UNINIT;
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_TPRXINDICATION_ID,
                        DetErrorId);
		#endif
        return;
    }

    /************************************************/
    /*To find DcmRxPduId corresponding protocol index number,
     *in the protocol configuration table */
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[id].DcmDslParentConnectionCtrlId;
    ProtocolCfgCtrlId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
	MsgCtrlIndexx      = ProtocolCfgCtrlId;
	RxChannelCfgCtrlId = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolRxBufferRef->Dcm_DslBufferId;
	TxChannelCfgCtrlId = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolTxBufferRef->Dcm_DslBufferId;

    /************************************************/
    /*Determine relationship between the message control block and the corresponding protocol control block */
    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    Dcm_ProtocolCtrl[ProtocolCfgCtrlId].ProtocolId  = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgCtrlId].DcmDslProtocolID;
    Dcm_ProtocolCtrl[ProtocolCfgCtrlId].ProtocolPri = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgCtrlId].DcmDslProtocolPriority;
    Dcm_ProtocolCtrl[ProtocolCfgCtrlId].ProtocolPreemptTime = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgCtrlId].DcmDslProtocolPreemptTime;
    Dcm_ProtocolCtrl[ProtocolCfgCtrlId].MsgCtrlIndex = MsgCtrlIndexx;
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    SchM_Enter_Dcm(Dcm_MsgCtrl);
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_RxCtrlChannelIndex = RxChannelCfgCtrlId;
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_TxCtrlChannelIndex = TxChannelCfgCtrlId;
    SchM_Exit_Dcm(Dcm_MsgCtrl);

    /**No MsgCtrlIndex ,ResetResource will not clear connect************/
    /**********************************************/
    /*Receive complete results analysis*/
	/*@req DCM-APIR-017[SWS_Dcm_00344]*/
	if(result != E_OK )
	{
		#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
		/*session control function is enabled */
        if(DCM_DEFAULT_SESSION != Dcm_MkCtrl.Dcm_ActiveSes)
        {
            for(Index=0;(Index<DCM_MSG_NUM)&&(FALSE==Flag);Index++)
            {
               if(DCM_MSG_WAIT != Dcm_MsgCtrl[Index].Dcm_MsgState)
               {
                   Flag = TRUE;
               }
            }
            if(FALSE == Flag)
            {
            	/*current session is non default session and have error.if other session state is not wait,
            	 *should start S3server*/
               /*SWS_Dcm_00141,Indicates an error during the reception of a multi-frame request message.*/
               DslInternal_S3ServerStart();
            }
        }
		#endif
        (void)DslInternal_InitChannelCtrl(RxChannelCfgCtrlId);
        return;
	}
    /***********************************************/
    /*functional request packet processing*/
	ProtocolId = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslProtocolID;
	ServiceType = Dcm_ClassifyServiceType(ProtocolId);
	if (DCM_UDS == ServiceType)
	{
		/*check for UDS*/
		ret = Dcm_TpRxIndication_IsFuncAddress(id);
		if(E_NOT_OK == ret)
		{
			return;
		}
	}

    /************************************************/
    pDslConnection = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgCtrlId].pDcmDslConnection;
    NetworkChannel = pDslConnection[ConnectionCfgId].pDcmDslMainConnection->DcmDslProtocolComMChannelId;
    DslProtocolRxAddrType = (pDslConnection[ConnectionCfgId].pDcmDslMainConnection->pDcmDslProtocolRx)[id].DcmDslProtocolRxAddrType;

    /************************************************/
    /* Determine relationship between the static configuration channel and receive/transmit channel control block */
    /****@req DCM-FUNR-003[SWS_Dcm_00241]****/
    SchM_Enter_Dcm(Dcm_ChannelCtrl);
    Dcm_ChannelCtrl[TxChannelCfgCtrlId].Dcm_ChannelCfgIndex = TxChannelCfgCtrlId;
    Dcm_ChannelCtrl[RxChannelCfgCtrlId].Dcm_ChannelCfgIndex = RxChannelCfgCtrlId;
    TxChannelTxState = Dcm_ChannelCtrl[TxChannelCfgCtrlId].Dcm_ChannelTxState;
    if(DCM_CH_OCCUPIED == TxChannelTxState)
    {
        /*Corresponding transmit channel status is not normal*/
        (void)DslInternal_ResetResource(ProtocolCfgCtrlId);
        SchM_Exit_Dcm(Dcm_ChannelCtrl);
        return;
    }
    else
    {
    	Dcm_ChannelCtrl[TxChannelCfgCtrlId].Dcm_ChannelTxState = DCM_CH_OCCUPIED;
    }
    SchM_Exit_Dcm(Dcm_ChannelCtrl);
    /************************************************/
    /*Determine relationships between the receive/transmit channel control block and the corresponding message control block */
    if (ServiceType == DCM_OBD)
    {
        SchM_Enter_Dcm(Dcm_MsgCtrl);
        Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ReqDataLen = (Dcm_MsgLenType)Dcm_OBDMessage.Length;
        Dcm_MsgCtrl[MsgCtrlIndexx].SID = Dcm_OBDMessage.Buffer[0];
        Dcm_MsgCtrl[MsgCtrlIndexx].Subfunction = Dcm_OBDMessage.Buffer[1];
        Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData    = &Dcm_OBDMessage.Buffer[0];
        SchM_Exit_Dcm(Dcm_MsgCtrl);
    }
    else
    {
        SchM_Enter_Dcm(Dcm_MsgCtrl);
        Dcm_MsgCtrl[MsgCtrlIndexx].SID = (Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData)[0];
        Dcm_MsgCtrl[MsgCtrlIndexx].Subfunction = (Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData)[1];
        SchM_Exit_Dcm(Dcm_MsgCtrl);
    }
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    /****@req DCM-FUNR-067[SWS_Dcm_00198]****/
    Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType = (uint8)DslProtocolRxAddrType;
    Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.DcmRxPduId  = id;
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_RxCtrlChannelIndex = RxChannelCfgCtrlId;
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_TxCtrlChannelIndex = TxChannelCfgCtrlId;
    if (NULL_PTR == pDslConnection[ConnectionCfgId].pDcmDslMainConnection->pDcmDslProtocolTx)
    {
    	Dcm_MsgCtrl[MsgCtrlIndexx].DcmTxPduId = DCM_INVALID_UINT8;
    }
    else
    {
    	Dcm_MsgCtrl[MsgCtrlIndexx].DcmTxPduId = pDslConnection[ConnectionCfgId].pDcmDslMainConnection->pDcmDslProtocolTx->DcmDslTxPduRPduId;
    }

    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MsgState = DCM_MSG_RECEIVED;
    SchM_Exit_Dcm(Dcm_MsgCtrl);

    /*************************************************/
	/*CheckProtocolFaild Then Rxindication is come shall send NRC*/
	if ((CheckProtocolFaild == TRUE) && (StopProtocolFaild != TRUE))
	{
		CheckProtocolFaild = FALSE;
		Dcm_MsgCtrl[ProtocolCfgCtrlId].NRC = DCM_E_BUSYREPEATREQUEST;
		Dcm_MsgCtrl[ProtocolCfgCtrlId].RspStyle = DCM_NEG_RSP;
		Dcm_MsgCtrl[Dcm_ProtocolCtrl[ProtocolCfgCtrlId].MsgCtrlIndex].Dcm_MsgState = DCM_MSG_PROCESSED;
		DsdInternal_ProcessingDone(ProtocolCfgCtrlId);
		return;
	}
	if (StopProtocolFaild == TRUE)
    {/*[SWS_Dcm_01190] */
		StopProtocolFaild = FALSE;
        Dcm_MsgCtrl[ProtocolCfgCtrlId].NRC = DCM_E_CONDITIONSNOTCORRECT;
        Dcm_MsgCtrl[ProtocolCfgCtrlId].RspStyle = DCM_NEG_RSP;
		Dcm_MsgCtrl[Dcm_ProtocolCtrl[ProtocolCfgCtrlId].MsgCtrlIndex].Dcm_MsgState = DCM_MSG_PROCESSED;
		DsdInternal_ProcessingDone(ProtocolCfgCtrlId);
        return;
    }
	for (Index = 0; Index < DCM_MAINCONNECTION_NUM; Index++)
	{
	    if(NetworkChannel == Dcm_CommCtrl[Index].DcmDslProtocolComMChannelId)
	    {
	        /*ComM interactive processing*/
	        if((DCM_COMM_ACTIVE == Dcm_CommCtrl[Index].Dcm_ActiveDiagnostic)
	          &&(DCM_DEFAULT_SESSION == Dcm_MkCtrl.Dcm_ActiveSes))
	        {
	            /*The notification ComM into the "FULL Communication"*/
	            /***SWS_Dcm_00163****/
	            ComM_DCM_ActiveDiagnostic(NetworkChannel);
	        }
	    }
	}

    /***********************************************/
    /*Notice the application to check for new protocol boot environment*/
    ret = DslInternal_ProtocolStart(ProtocolCfgCtrlId);
    if(E_OK != ret)
    {
    	/*SWS_Dcm_00674*/
        (void)DsdInternal_SetNrc(ProtocolCfgCtrlId,DCM_E_CONDITIONSNOTCORRECT);
        DsdInternal_ProcessingDone(ProtocolCfgCtrlId);
        return;
    }

    /************************************************/
    /*Start P2Timer Timer*/
    /*SWS_Dcm_01142*/
    (void)DslInternal_P2ServerStart(ProtocolCfgCtrlId);
    /************************************************/
    if (DCM_E_BUSYREPEATREQUEST == Dcm_MsgCtrl[MsgCtrlIndexx].NRC)
    {
        /*SWS_Dcm_00789*/
        DsdInternal_ProcessingDone(ProtocolCfgCtrlId);
        return;
    }

    /***********************************************/
    /* save valid Protocol ID number*/
    SchM_Enter_Dcm(Dcm_MkCtrl);
    Dcm_MkCtrl.Dcm_ActiveProtocol = Dcm_ProtocolCtrl[ProtocolCfgCtrlId].ProtocolId;
    SchM_Exit_Dcm(Dcm_MkCtrl);
    /**********************************************/
    /****Notification DSD layer, and reception processing****/
    /****@req DCM-FUNR-002[SWS_Dcm_00111]****/
    (void)DsdInternal_RxIndication(ProtocolCfgCtrlId);
    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
#if(STD_ON == DCM_DEV_ERROR_DETECT)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static   FUNC(Std_ReturnType,DCM_CODE)Dcm_CopyTxData_CheckInputParameter(PduIdType DcmTxPduId,
																	  P2CONST(PduInfoType,AUTOMATIC,DCM_VAR)PduInfoPtr,
																	  P2VAR(PduLengthType,AUTOMATIC,DCM_VAR)availableDataPtr,
																	  P2VAR(uint8,AUTOMATIC,DCM_VAR)pDetErrorId)
{
    /***********************************/
    /*The DCM module status check*/
    if(DCM_ON != (Dcm_MkCtrl.Dcm_MkState))
    {
        /****@req DCM-FUNR-243[DCM042]****/
        /****@req DCM-FUNR-244[DCM043]****/
        /****@req DCM-FUNR-245[DCM048]****/
        /****@req DCM-FUNR-246[DCM049]****/
        (*pDetErrorId) = DCM_E_UNINIT;
        return(E_NOT_OK);
    }

    /***********************************/
    /*Parameter validation*/
    if((NULL_PTR == PduInfoPtr)||(NULL_PTR == availableDataPtr)||
    		((DcmTxPduId < 0u)||(DcmTxPduId >= DCM_DSL_TX_ID_NUM)))
    {
        (*pDetErrorId) = DCM_E_PARAM;
        return(E_NOT_OK);
    }
    return(E_OK);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static   FUNC(Std_ReturnType,DCM_CODE)Dcm_CopyTxData_CheckCtrlStatus(PduIdType DcmTxPduId)
{
    uint8  ProtocolCfgId;
    uint8  ConnectionCfgId;
    uint8  MsgCtrlId;
    uint8  RxChannelCtrlId;
    uint8  TxChannelCtrlId;

    /*********************************************************/
    /*To find DcmTxPduId corresponding Protocol index number,
     *in the protocol configuration table*/
    ConnectionCfgId = Dsl_Protocol_Connection_TxCfg[DcmTxPduId].DcmDslParentConnectionCtrlId;
    ProtocolCfgId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
    MsgCtrlId       = Dcm_ProtocolCtrl[ProtocolCfgId].MsgCtrlIndex;

    if(DCM_INVALID_UINT8 == MsgCtrlId)
    {
        return(E_NOT_OK);
    }
    RxChannelCtrlId = Dcm_MsgCtrl[MsgCtrlId].Dcm_RxCtrlChannelIndex;
    TxChannelCtrlId = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;

    /***********************************************************/
    if((DCM_MSG_TRANSMISSION != Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState)
#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
    	&&(Dcm_PageBufferData.TotalSize == 0UL)
#endif
    	)
    {
        /*The state machine does not operate properly, return BUFREQ_E_NOT_OK*/
        return(E_NOT_OK);
    }

    if(DCM_CH_OCCUPIED != (Dcm_ChannelCtrl[RxChannelCtrlId].Dcm_ChannelRxState))
    {
    	/*Receiver channel status is not correct*/
        return(E_NOT_OK);
    }
    if(DCM_CH_OCCUPIED != (Dcm_ChannelCtrl[TxChannelCtrlId].Dcm_ChannelTxState))
    {
    	/*The send channel status is incorrect*/
        return(E_NOT_OK);
    }
    return(E_OK);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(BufReq_ReturnType,DCM_CODE)Dcm_CopyTxData(PduIdType id,
												P2CONST(PduInfoType, AUTOMATIC, DCM_VAR)  info,
												P2VAR(RetryInfoType,AUTOMATIC,DCM_VAR) retry,
												P2VAR(PduLengthType,AUTOMATIC,DCM_VAR) availableDataPtr)
{
    uint8  ProtocolCfgId;
    uint8  ConnectionCfgId;
    uint8  MsgCtrlId;
    uint8  TxChannelCtrlIndex;
    uint16 Position;
    uint16 Len;
    Std_ReturnType  ret;
    BufReq_ReturnType bufRet;
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    uint8  DetErrorId;

    /*******************************************/
	ret = Dcm_CopyTxData_CheckInputParameter( id,
											  info,
											  availableDataPtr,
											  &DetErrorId);
	if (E_OK == ret)
	#endif
	{
	    /*******************************************/
	    ret = Dcm_CopyTxData_CheckCtrlStatus(id);
	    if(E_NOT_OK == ret)
	    {
 	    	bufRet = BUFREQ_E_NOT_OK;
	    }
	    else
	    {
	        /*******************************************/
	    	ConnectionCfgId = Dsl_Protocol_Connection_TxCfg[id].DcmDslParentConnectionCtrlId;
	    	ProtocolCfgId   = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
	        MsgCtrlId       = Dcm_ProtocolCtrl[ProtocolCfgId].MsgCtrlIndex;
	        TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlId].Dcm_TxCtrlChannelIndex;
	        Position = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition;
	        Len = Position + info->SduLength;

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
	        if(Dcm_PageBufferData.TotalSize != 0UL)
	        {
	            /*check the pagebuffer timer whether timeout*/
	            if (Dcm_PageBufferData.TimeOut == TRUE)
	            {
                    DslInternal_InitPageBuffer();
                    (void)DslInternal_ResetResource(ProtocolCfgId);
	                return BUFREQ_E_NOT_OK;
	            }

	            if (Dcm_PageBufferData.Filled == FALSE)
	            {
                    bufRet = BUFREQ_E_BUSY;
	            }
	            else
	            {
                    if ((Dcm_PageBufferData.ThisPageTxSize + info->SduLength) > Dcm_PageBufferData.ThisPageSize)
                    {
                        /*last one message not has 8 byte, memory it for next page to send*/
                        if ((Dcm_PageBufferData.ThisPageSize - Dcm_PageBufferData.ThisPageTxSize) != 0u)
                        {
                            Dcm_PageBufferData.LastNotTxDataSize = Dcm_PageBufferData.ThisPageSize - Dcm_PageBufferData.ThisPageTxSize;
                            Dcm_MemoryCopy( &(Dcm_PageBufferData.pResData[Dcm_PageBufferData.ThisPageTxSize]),
                                            Dcm_PageBufferData.LastNotTxData,
                                            Dcm_PageBufferData.LastNotTxDataSize);
                        }
                        /*busy should call the api to fill the page*/
                        DspInternal_DcmUpdatePage(ProtocolCfgId);
                        bufRet = BUFREQ_E_BUSY;
                    }
                    else
                    {
                        if (info->SduLength != 0u)
                        {
                            /*Store the response data to the corresponding transmit channel*/
                            Dcm_PageBufferData.pResData = &(Dcm_Channel[(Dcm_DslCfg.pDcmChannelCfg)[Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex].offset]);

                            if (Dcm_PageBufferData.LastNotTxDataSize != 0u)
                            {
                                /*last one message not has 8 byte, to send it with next page data filled 8 byte*/
                                Dcm_MemoryCopy( Dcm_PageBufferData.LastNotTxData,
                                                info->SduDataPtr,
                                                Dcm_PageBufferData.LastNotTxDataSize);
                                Dcm_MemoryCopy( &(Dcm_PageBufferData.pResData[Dcm_PageBufferData.ThisPageTxSize]),
                                                &(info->SduDataPtr[Dcm_PageBufferData.LastNotTxDataSize]),
                                                (info->SduLength - Dcm_PageBufferData.LastNotTxDataSize));
                                Dcm_PageBufferData.ThisPageTxSize += (info->SduLength - Dcm_PageBufferData.LastNotTxDataSize);
                                Dcm_PageBufferData.LastNotTxDataSize = 0u;
                            }
                            else
                            {
                                Dcm_MemoryCopy( &(Dcm_PageBufferData.pResData[Dcm_PageBufferData.ThisPageTxSize]),
                                                info->SduDataPtr,
                                                info->SduLength);
                                Dcm_PageBufferData.ThisPageTxSize += info->SduLength;
                            }
                            Dcm_PageBufferData.AlreadyPageSize += info->SduLength;
                        }
                        *availableDataPtr = (PduLengthType)(Dcm_PageBufferData.TotalSize -
                                Dcm_PageBufferData.AlreadyPageSize);
                        bufRet = BUFREQ_OK;
                    }
	            }
	        }
	        else
#endif
	        {
				/*ResDataLen is total length*/
				if (Len > Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResDataLen)
				{
					bufRet = BUFREQ_E_BUSY;
				}
				else
				{
					if ((NULL_PTR == retry)||(TP_DATACONF == retry->TpDataState))
					{
						Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferErasePosition =
								Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition;
					}
					else if (TP_DATARETRY == retry->TpDataState )
					{
						Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition =
								Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition -	retry->TxTpDataCnt;
					}
					else
					{
						/*TP_CONFPENDING,do nothing*/
					}

					if (info->SduLength != 0u)
					{
						Dcm_MemoryCopy( &(Dcm_MsgCtrl[MsgCtrlId].MsgContext.pResData[Position]),
										info->SduDataPtr,
										info->SduLength);
						Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition =	Position + info->SduLength;
					}

					*availableDataPtr = (PduLengthType)(Dcm_MsgCtrl[MsgCtrlId].MsgContext.ResMaxDataLen -
							Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_BufferCunrentPosition);
					bufRet = BUFREQ_OK;
	           }
	        }
	    }
	}
	#if(STD_ON == DCM_DEV_ERROR_DETECT)
	else
	{
		Det_ReportError(DCM_MODULE_ID,
						DCM_INSTANCE_ID,
						DCM_COPYTXDATA_ID,
						DetErrorId );

		bufRet = BUFREQ_E_NOT_OK;
	}
	#endif

	return bufRet;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <This is called by the PduR to confirm a Transmit>
 * ServiceId           <0x05>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <DcmTxPduId:ID of DCM I-PDU that has been transmitted.
 *                      Result: NTFRSLT_OK: the complete N-PDU has been transmitted.
 *                              NTFRSLT_E_CANCELATION_OK: the N-PDU has been successfully cancelled.
 *                              NTFRSLT_E_CANCELATION_NOT_OK: an error occurred when cancelling the N-PDU.
 *                              any other value: an error occurred during transmission>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <this function must be called after call of DcmProvideTxBuffer().>
 * CallByAPI           <>
 */
/*************************************************************************/
/****@req DCM-FUNR-088[DCM235]****/
/****@req DCM-FUNR-245[DCM048]****/
/****@req DCM_APIR_025[DCM351]****/
/****@req DCM-APIR-026[DCM352]****/
/****@req DCM-APIR-027[DCM353]****/
/****@req DCM-APIR-028[DCM354]****/

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)Dcm_TpTxConfirmation(PduIdType id, Std_ReturnType result)
{
    uint8  ProtocolCfgCtrlId;
    uint8  ConnectionCfgId;
    uint8  MsgCtrlIndex;

    /******************************************************************/
    /*The DCM module status check*/
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    SchM_Enter_Dcm(Dcm_MkCtrl);
    if(DCM_ON != (Dcm_MkCtrl.Dcm_MkState))
    {
        /****@req DCM-FUNR-243[DCM042]****/
        /****@req DCM-FUNR-244[DCM043]****/
        /****@req DCM-FUNR-245[DCM048]****/
        /****@req DCM-FUNR-246[DCM049]****/
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_TXCONFIRMATION_ID,
                        DCM_E_UNINIT);
        SchM_Exit_Dcm(Dcm_MkCtrl);
        return;
    }
    SchM_Exit_Dcm(Dcm_MkCtrl);
#endif

    /*****************************************************************/
    /*find DcmTxPduId corresponding protocol index number,in the protocol configuration table*/
    ConnectionCfgId = Dsl_Protocol_Connection_TxCfg[id].DcmDslParentConnectionCtrlId;
    ProtocolCfgCtrlId  = Dsl_Protocol_ConnectionCfg[ConnectionCfgId].DcmDslParentProtocolRowCtrlId;
    MsgCtrlIndex       = Dsl_ProtocolRowCfg[ProtocolCfgCtrlId].DcmDslServiceTableID;

    /*****************************************************************/
    switch(result)
    {
       case  E_NOT_OK:
    	     /*Confirmation message shows: response failed to send*/
    	     (void)DslInternal_ResetResource(ProtocolCfgCtrlId);
    	     break;
       case  E_OK:
    	     /*Confirmation message shows: the response is sent to notify the DSD layer further confirmation*/
    	     /****@req DCM-FUNR-008[DCM117]****/
    	     /******************************************************************/
    	     /*Set corresponding to the message status = "DCM_MSG_CONFIRMATION*/
    	     SchM_Enter_Dcm(Dcm_MsgCtrl);
    	     Dcm_MsgCtrl[MsgCtrlIndex].Dcm_MsgState = DCM_MSG_CONFIRMATION;
#if(STD_ON == DCM_DSLDIAGRESP_FORCERESPENDEN)
    	     if (DCM_E_FORCE_RCRRP == Dcm_MsgCtrl[MsgCtrlIndex].Dcm_Ret)
    	     {
    	    	 Dcm_MsgCtrl[MsgCtrlIndex].Dcm_OpStatus = DCM_FORCE_RCRRP_OK;
    	     }
#endif
    	     SchM_Exit_Dcm(Dcm_MsgCtrl);
   	         DsdInternal_TxConfirmation(ProtocolCfgCtrlId);
    	     break;
       default:
    	     (void)DslInternal_ResetResource(ProtocolCfgCtrlId);
    	     break;
    }
    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)Dcm_TxConfirmation(PduIdType  DcmTxPduId)
{

#if(STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
    uint8 index;

    for (index=0;index<DCM_PERIODICCONNECTION_NUM;index++)
    {
        if (Scheduler_0x2A_Transmit[index].PduId == DcmTxPduId)
        {
            Scheduler_0x2A_Transmit[index].Transmit = FALSE;
        }
    }
#endif
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
/*******************internal functions****************************/
/*************************************************************************/
/*
 * Brief               <DSL layer sends the response function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:Protocol control block index number.>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <DsdInternal_ProcessingDone>
 */
/*************************************************************************/
/****@req DCM-FUNR-086[SWS_Dcm_00232]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DslInternal_ProcessingDone(uint8  ProtocolCtrlId)
{
	PduIdType    DcmTxPduIdx;
    PduInfoType  PduInfo;
    uint8        MsgCtrlIndexx;
    Std_ReturnType  ret;
    uint8 Index;
    uint8 iloop;

    /***********************************************************/
    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    DcmTxPduIdx   = Dcm_MsgCtrl[MsgCtrlIndexx].DcmTxPduId;
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

#if (STD_ON == DCM_GENERIC_CONNECTION)
    PduInfo.SduDataPtr = &Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MetaData[0];
#else
    PduInfo.SduDataPtr = NULL_PTR;
#endif
    PduInfo.SduLength  = (PduLengthType)(Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResMaxDataLen);


    for (Index = 0; Index < DCM_CONNECTION_NUM; Index++)
    {
        if (Dsl_Protocol_ConnectionCfg[Index].DcmDslParentProtocolRowCtrlId == ProtocolCtrlId)
        {
            for (iloop = 0; iloop < DCM_MAINCONNECTION_NUM; iloop++)
            {
                if((Dsl_Protocol_ConnectionCfg[Index].pDcmDslMainConnection != NULL_PTR)
                  && (Dsl_Protocol_ConnectionCfg[Index].pDcmDslMainConnection->DcmDslProtocolComMChannelId
                        == Dcm_CommCtrl[iloop].DcmDslProtocolComMChannelId))
                {
                    /*SWS_Dcm_01142*/
                    if(Dcm_CommCtrl[iloop].Dcm_CommState != DCM_COMM_FULL_COMMUNICATION)
                    {
                        ret = DslInternal_IsLargerThanP2ServerMax(ProtocolCtrlId);
                        if (E_OK == ret)
                        {
                            /*time expired*/
                            (void)DslInternal_ResetResource(ProtocolCtrlId);
                        }
                        return;
                    }
                }
            }
        }
    }

    /*SWS_Dcm_01166, the Multiplicity of DcmDslProtocolTx is "0"*/
    if (DcmTxPduIdx == DCM_INVALID_UINT8)
    {
    	/*process the received diagnostic request without sending a response*/
    	(void)DslInternal_ResetResource(ProtocolCtrlId);
    	return;
    }

    /****@req DCM-FUNR-007[DCM115]****/
    ret = DslInternal_IsLargerThanP2ServerMin(ProtocolCtrlId);
    if(E_NOT_OK == ret)
    {
        /*Response time <P2ServerMin*/
        return;
    }
    /****@req DCM-FUNR-087[DCM237]****/
    ret = PduR_DcmTransmit(DcmTxPduIdx, &PduInfo);
    if(E_NOT_OK == ret)
    {
        /*Failed to send*/
        /****@req DCM-FUNR-009[DCM118]****/
        (void)DslInternal_ResetResource(ProtocolCtrlId);
        return;
    }
    /*****************************************/
    Dcm_MsgCtrl[MsgCtrlIndexx].SendFlag = TRUE;

    /****Close P2Server Timer****/
    (void)DslInternal_P2ServerStop(ProtocolCtrlId);
    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
/*************************************************************************/
/*
 * Brief               <>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:Protocol control block index number.>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DslInternal_RxIndication(uint8  ProtocolCtrlId)
{
    /***********************************************/
    /**Notify the application start the Protocol**/
    (void)DslInternal_ProtocolStart(ProtocolCtrlId);

    /***********************************************/
    /*Save the valid protocol ID number*/
    SchM_Enter_Dcm(Dcm_MkCtrl);
    Dcm_MkCtrl.Dcm_ActiveProtocol = Dcm_ProtocolCtrl[ProtocolCtrlId].ProtocolId;
    SchM_Exit_Dcm(Dcm_MkCtrl);
    /**********************************/
    /**Notification DSD layer reception processing**/
    /****SWS_Dcm_00111****/
    (void)DsdInternal_RxIndication(ProtocolCtrlId);

    return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
