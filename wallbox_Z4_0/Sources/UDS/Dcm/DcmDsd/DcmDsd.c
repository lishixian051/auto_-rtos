/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <DcmDsd.c>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <shushi>
 *  @date       <2018-3-20>
 */
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCMDSD_C_AR_MAJOR_VERSION  4
#define DCMDSD_C_AR_MINOR_VERSION  2
#define DCMDSD_C_AR_PATCH_VERSION  2
#define DCMDSD_C_SW_MAJOR_VERSION  1
#define DCMDSD_C_SW_MINOR_VERSION  0
#define DCMDSD_C_SW_PATCH_VERSION  2
/******************************* references ************************************/
#include "Dcm_Include.h"

/*=======[V E R S I O N  C H E C K]===========================================*/
#if (DCMDSD_C_AR_MAJOR_VERSION != DCMDSD_H_AR_MAJOR_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSD_C_AR_MINOR_VERSION != DCMDSD_H_AR_MINOR_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSD_C_AR_PATCH_VERSION != DCMDSD_H_AR_PATCH_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSD_C_SW_MAJOR_VERSION != DCMDSD_H_SW_MAJOR_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSD_C_SW_MINOR_VERSION != DCMDSD_H_SW_MINOR_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif
#if (DCMDSD_C_SW_PATCH_VERSION != DCMDSD_H_SW_PATCH_VERSION)
  #error "DcmDsd.c : Mismatch in Specification Major Version"
#endif

/******************************* references ************************************/
/*****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SuppressPosRsp(uint8 ProtocolCtrlId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SidCheck(uint8  ProtocolCtrlId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_AddressingFormatCheck(uint8  ProtocolCtrlId,uint8 Sid);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_ManufacturerIndication(uint8 ProtocolCtrlId);
static  FUNC(void,DCM_CODE)DsdInternal_ManufacturerConfirmation(uint8 ProtocolCtrlId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if (STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SupplierIndication(uint8 ProtocolCtrlId);
static  FUNC(void,DCM_CODE)DsdInternal_SupplierConfirmation(uint8 ProtocolCtrlId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/*******************internal functions****************************/
/*************************************************************************/
/*
 * Brief               <Find SID corresponding service configuration table index
 *                      and the service index in corresponding service configuration table.>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Sid: Service Id;
 *                      ProtocolCtrlId: The corresponding protocol control block ID number>
 * Param-Name[out]     <(*pSidTabIndex):Request packet index number in the service configuration table;
 *                      (*pSidTabServieCfgIndex): the service index in corresponding service configuration table>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-062[DCM192]****/
/****@req DCM-FUNR-063[DCM193]****/
/****@req DCM-FUNR-065[DCM196]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE) DsdInternal_SearchSidTabServiceIndex(uint8   Sid,
																   uint8   ProtocolCtrlId,
																   P2VAR(uint16,AUTOMATIC, DCM_VAR)pSidTabIndex,
																   P2VAR(uint16,AUTOMATIC, DCM_VAR)pSidTabServieCfgIndex)
{
    uint8  SidTabId;
    uint8  ProtocolCfgIndex;
    uint8  Service_Num;
    uint16 SidTabIndex;
    uint16 ServiceIndex;
    boolean  Flag;
    P2CONST(Dcm_DsdServiceCfgType, AUTOMATIC,DCM_CONST)pDsdSidTab;

    if(  (NULL_PTR == pSidTabIndex)
       ||(NULL_PTR == pSidTabServieCfgIndex) )
    {
        return E_NOT_OK;
    }
    ProtocolCfgIndex = ProtocolCtrlId;
    /*Read the protocol includes the service table ID */
    SidTabId  = (Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow)[ProtocolCfgIndex].DcmDslServiceTableID;
    /*find SidTabId configuration position in the service configuration table*/
    Flag = FALSE;
    for(SidTabIndex=0;(SidTabIndex<Dcm_DsdCfg.DcmDsdServiceTable_Num)&&(FALSE==Flag);SidTabIndex++)
    {
        if(SidTabId == ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].DcmDsdSidTabId))
        {
            Flag = TRUE;
        }
    }
    if(FALSE == Flag)
    {
        /*Did not find the service table ID,in the configuration table*/
        return  E_NOT_OK;
    }
    (*pSidTabIndex) = SidTabIndex - (uint16)1;

    /*Read the first address of the corresponding service table*/
    pDsdSidTab  = (Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex-1u].pDcmDsdService;

    /*Read the number of services,in the corresponding service table*/
    Service_Num = (Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex-1u].DcmDsdSidTab_ServiceNum;
    Flag = FALSE;
    for(ServiceIndex=0;(ServiceIndex<Service_Num)&&(FALSE==Flag);ServiceIndex++)
    {
        if((Sid == pDsdSidTab[ServiceIndex].DcmDsdServiceId)
                && (pDsdSidTab[ServiceIndex].DcmDsdServiceUsed == TRUE))
        {
           Flag = TRUE;
        }
    }
    if(FALSE == Flag)
    {
      /*Did not find the service ID,in the corresponding service table*/
      return  E_NOT_OK;
    }
    (*pSidTabServieCfgIndex) = (ServiceIndex - (uint16)1);

    return  E_OK;
}

FUNC(Std_ReturnType,DCM_CODE) DsdInternal_SearchSidTabSubServiceIndex(uint8   ProtocolCtrlId,
																	  uint16  SidTabIndex,
																	  uint16  ServiceIndex,
																	  P2VAR(uint16,AUTOMATIC, DCM_VAR)pSubServieCfgIndex)
{
	uint8  MsgCtrlId;
	uint8  SubFunctionId;
    uint8  SubService_Num;
    uint16 SubServiceIndex;
    boolean  Flag;

	MsgCtrlId =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    SubFunctionId = Dcm_MsgCtrl[MsgCtrlId].Subfunction;
    SubService_Num = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].pDcmDsdService)[ServiceIndex].DcmDsdSubService_Num;
    if(SubService_Num > 0u)
    {
		Flag = FALSE;
		for (SubServiceIndex = 0;(SubServiceIndex<SubService_Num)&&(FALSE==Flag);SubServiceIndex++)
		{
			if (SubFunctionId == (((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabIndex].pDcmDsdService)[ServiceIndex].DcmDsdSubService)[SubServiceIndex].DcmDsdSubServiceId)
			{
				Flag = TRUE;
			}
		}
		if(FALSE == Flag)
		{
		  return  E_NOT_OK;
		}
		(*pSubServieCfgIndex) = SubServiceIndex - (uint16)1;
    }
    else
    {
    	(*pSubServieCfgIndex) = DCM_INVALID_UINT16;
    }

    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
/*************************************************************************/
/*
 * Brief               <Save a positive response prohibit information>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <Sid: Service Id;
 *                      ProtocolCtrlId:The corresponding protocol control block ID number;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-068[DCM200]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SuppressPosRsp(uint8 ProtocolCtrlId)
{
    uint8  MsgCtrlIndexx;
    uint8  SubFunction;
    uint8  Sid;
    uint16 SidTabCfgIndex;
    uint16 SidTabServiceCfgIndex;
    boolean  SubFuncAvial;
    Std_ReturnType  ret;

	MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
	Sid = Dcm_MsgCtrl[MsgCtrlIndexx].SID;
    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
       return  E_NOT_OK;
    }

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    /****@req DCM-FUNR-072[DCM204]****/
    SubFuncAvial = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSubfuncAvial;
    if( TRUE == SubFuncAvial )
    {
    	SubFunction = Dcm_MsgCtrl[MsgCtrlIndexx].Subfunction;
        Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.SuppressPosResponse  = FALSE;
        if(0x80u == (SubFunction & 0x80u))
        {
          Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.SuppressPosResponse = TRUE;
          /*Set response type "DCM_POS_RSP_SUPPRESS"*/
          /****@req DCM-FUNR-068[DCM200]****/
          Dcm_MsgCtrl[MsgCtrlIndexx].RspStyle = DCM_POS_RSP_SUPPRESS;
        }
        /*Clear positive response Prohibition information*/
        /****@req DCM-FUNR-069[DCM201]****/
        (Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData)[1] = (SubFunction & 0x7Fu);
        Dcm_MsgCtrl[MsgCtrlIndexx].Subfunction = (SubFunction & 0x7Fu);
    }
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Check request packet whether the SID is configured in the service table>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Sid: Service Id;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SidCheck(uint8  ProtocolCtrlId)
{
    uint16  SidTabCfgIndex;
    uint16  SidTabServiceCfgIndex;
    uint8   MsgCtrlIndexx;
    uint8   Sid;
    Std_ReturnType  ret;

	MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
	Sid = Dcm_MsgCtrl[MsgCtrlIndexx].SID;

    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);

    return(ret);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Check request packet addressing type whether is correct.>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Sid: Service Id;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_AddressingFormatCheck(uint8  ProtocolCtrlId,
                                                                       uint8  Sid)
{
    uint8  MsgCtrlIndexx;
    uint16 SidTabCfgIndex;
    uint16 SidTabServiceCfgIndex;
    Dcm_DslProtocolRxAddrType  RxServiceAddressingFormat;
    Dcm_DslProtocolRxAddrType  CfgServiceAddressingFormat;
    Std_ReturnType  ret;

    /*******************************************************/
    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
        return(E_NOT_OK);
    }
    /*Read the service configuration support addressing*/
    CfgServiceAddressingFormat = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].AddressingFormat;
    if(DCM_PHYANDFUNC == CfgServiceAddressingFormat)
    {
        /*The service supports functional addressing and physical addressing*/
        return(E_OK);
    }
    /*Read request packet addressing type*/
    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    RxServiceAddressingFormat = (Dcm_DslProtocolRxAddrType)Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;
    if(CfgServiceAddressingFormat != RxServiceAddressingFormat)
    {
        /*Configure addressing type is different from the actual addressing type*/
        return(E_NOT_OK);
    }
    return(E_OK);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#if(STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
/*************************************************************************/
/*
 * Brief               <Diagnostic request packet is whether allowed in the current application environment>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant/Non Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Sid: Service Id;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_ManufacturerIndication(uint8 ProtocolCtrlId)
{
    uint8  MsgCtrlIndexx;
    uint16  PortNum;
    uint16  PortIndex;
    uint8  NotAcceptNum = 0u;
    uint8  Env_NokNum = 0u;
    uint16 ReqDatalen;
	uint8  ReqType;
	uint8  Sid;
	PduIdType RxPduId;
	uint8  ConnectionCfgId;
	uint16 SourceAddress;
    boolean  Flag;
    Std_ReturnType  ret = E_NOT_OK;
    P2VAR(uint8, AUTOMATIC, DCM_VAR)pReqDatax;
    Dcm_NegativeResponseCodeType ErrorCode;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Sid           =  Dcm_MsgCtrl[MsgCtrlIndexx].SID;
    pReqDatax     =  &Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData[0];
    ReqDatalen    =  (uint16)(Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ReqDataLen - 1u);
    ReqType       =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;

    /*find out the source address of request message */
    RxPduId		  =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.DcmRxPduId;
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[RxPduId].DcmDslParentConnectionCtrlId;
    SourceAddress =  Dsl_Protocol_ConnectionCfg[ConnectionCfgId].pDcmDslMainConnection->DcmDslProtocolRxTesterSourceAddr;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    /****@req DCM-FUNR-075[DCM218]****/
    Flag = FALSE;
    PortNum = Dcm_DsdCfg.DcmDsdServiceReqManufacturerNoti_PortNum;

    for(PortIndex=0;(PortIndex<PortNum)&&(FALSE==Flag);PortIndex++)
    {
        if(NULL_PTR == ((Dcm_DsdCfg.pDcmDsdServiceReqManufacturerNoti)[PortIndex].Indication))
        {
            Flag = TRUE;
        }
        else
        {
            ret = (*((Dcm_DsdCfg.pDcmDsdServiceReqManufacturerNoti)[PortIndex].Indication))(
            		Sid,
					pReqDatax,
					ReqDatalen,
					ReqType,
					SourceAddress,
					&ErrorCode);
            switch(ret)
            {
                case E_OK:
                     break;
                case E_REQUEST_NOT_ACCEPTED:
                     NotAcceptNum++;
                     break;
                case E_NOT_OK:
                     Env_NokNum++;
                     break;
                default:
                	 Env_NokNum++;
                     break;
            }
        }
    }
    if(0u!=NotAcceptNum)
    {
    	/*the request is not accepted*/
    	/*SWS_Dcm_00462*/
    	/*SWS_Dcm_01172*/
    	DsdInternal_ManufacturerConfirmation(ProtocolCtrlId);
    	/*SWS_Dcm_00677*/
    	(void)DslInternal_ResetResource(ProtocolCtrlId);
    	ret = E_NOT_OK;
    }
    else if((0u!=Env_NokNum)||(TRUE == Flag))
    {
    	/*SWS_Dcm_00463*/
		(void)DsdInternal_SetMsgState(ProtocolCtrlId,DCM_MSG_PROCESSED);
		/*SWS_Dcm_00678*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId, ErrorCode);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
        ret = E_NOT_OK;
    }
    else
    {
        ret = E_OK;
    }
    return  ret;
}

static  FUNC(void,DCM_CODE)DsdInternal_ManufacturerConfirmation(uint8 ProtocolCtrlId)
{
    uint8  MsgCtrlIndexx;
    uint16  PortNum;
    uint16  PortIndex;
    uint8  Sid;
	uint8  ReqType;
	PduIdType RxPduId;
	uint8  ConnectionCfgId;
	uint16 SourceAddress;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Sid           =  Dcm_MsgCtrl[MsgCtrlIndexx].SID;
    ReqType       =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;

    /*find out the source address of request message */
    RxPduId		  =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.DcmRxPduId;
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[RxPduId].DcmDslParentConnectionCtrlId;
    SourceAddress =  Dsl_Protocol_ConnectionCfg[ConnectionCfgId].pDcmDslMainConnection->DcmDslProtocolRxTesterSourceAddr;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    /****SWS_Dcm_01172****/
    PortNum = Dcm_DsdCfg.DcmDsdServiceReqManufacturerNoti_PortNum;
    for(PortIndex=0; (PortIndex<PortNum); PortIndex++)
    {
		(void)(*((Dcm_DsdCfg.pDcmDsdServiceReqManufacturerNoti)[PortIndex].Confirmation))(
				Sid,
				ReqType,
				SourceAddress,
				DCM_RES_NEG_NOT_OK);
    }

    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if (STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static  FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SupplierIndication(uint8 ProtocolCtrlId)
{
    uint8  MsgCtrlIndexx;
    uint16  PortNum;
    uint16  PortIndex;
    uint8  NotAcceptNum = 0u;
    uint8  Env_NokNum = 0u;
    uint16 ReqDatalen;
	uint8  ReqType;
	uint8  Sid;
	PduIdType RxPduId;
	uint8  ConnectionCfgId;
	uint16 SourceAddress;
    boolean  Flag;
    Std_ReturnType  ret = E_NOT_OK;
    P2VAR(uint8, AUTOMATIC, DCM_VAR)pReqDatax;
    Dcm_NegativeResponseCodeType ErrorCode;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Sid           =  Dcm_MsgCtrl[MsgCtrlIndexx].SID;
    pReqDatax     =  &Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pReqData[0];
    ReqDatalen    =  (uint16)(Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ReqDataLen - 1u);
    ReqType       =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;

    /*find out the source address of request message */
    RxPduId		  =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.DcmRxPduId;
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[RxPduId].DcmDslParentConnectionCtrlId;
    SourceAddress =  Dsl_Protocol_ConnectionCfg[ConnectionCfgId].pDcmDslMainConnection->DcmDslProtocolRxTesterSourceAddr;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    Flag = FALSE;
    PortNum = Dcm_DsdCfg.DcmDsdServiceReqSupplierNoti_PortNum;

    for(PortIndex=0;(PortIndex<PortNum)&&(FALSE==Flag);PortIndex++)
    {
        if(NULL_PTR == ((Dcm_DsdCfg.pDcmDsdServiceReqSupplierNoti)[PortIndex].Indication))
        {
            Flag = TRUE;
        }
        else
        {
            ret = (*((Dcm_DsdCfg.pDcmDsdServiceReqSupplierNoti)[PortIndex].Indication))(
            		Sid,
					pReqDatax,
					ReqDatalen,
					ReqType,
					SourceAddress,
					&ErrorCode);
            switch(ret)
            {
                case E_OK:
                     break;
                case E_REQUEST_NOT_ACCEPTED:
                     NotAcceptNum++;
                     break;
                case E_NOT_OK:
                     Env_NokNum++;
                     break;
                default:
                	 Env_NokNum++;
                     break;
            }
        }
    }
    if(0u!=NotAcceptNum)
    {
    	/*the request is not accepted*/
    	/*SWS_Dcm_00517*/
    	/*SWS_Dcm_00677*/
    	(void)DslInternal_ResetResource(ProtocolCtrlId);
    	ret = E_NOT_OK;
    }
    else if((0u!=Env_NokNum)||(TRUE == Flag))
    {
    	/*SWS_Dcm_00518*/
		(void)DsdInternal_SetMsgState(ProtocolCtrlId,DCM_MSG_PROCESSED);
		/*SWS_Dcm_00678*/
		(void)DsdInternal_SetNrc(ProtocolCtrlId, ErrorCode);
		DsdInternal_ProcessingDone(ProtocolCtrlId);
        ret = E_NOT_OK;
    }
    else
    {
        ret = E_OK;
    }
    return  ret;
}

static  FUNC(void,DCM_CODE)DsdInternal_SupplierConfirmation(uint8 ProtocolCtrlId)
{
    uint8  MsgCtrlIndexx;
    uint16  PortNum;
    uint16  PortIndex;
    uint8  Sid;
	uint8  ReqType;
	PduIdType RxPduId;
	uint8  ConnectionCfgId;
	uint16 SourceAddress;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx =  Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Sid           =  Dcm_MsgCtrl[MsgCtrlIndexx].SID;
    ReqType       =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;

    /*find out the source address of request message */
    RxPduId		  =  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.DcmRxPduId;
    ConnectionCfgId = Dsl_Protocol_Connection_RxCfg[RxPduId].DcmDslParentConnectionCtrlId;
    SourceAddress =  Dsl_Protocol_ConnectionCfg[ConnectionCfgId].pDcmDslMainConnection->DcmDslProtocolRxTesterSourceAddr;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    /****SWS_Dcm_01172****/
    PortNum = Dcm_DsdCfg.DcmDsdServiceReqSupplierNoti_PortNum;
    for(PortIndex=0; (PortIndex<PortNum); PortIndex++)
    {
		(void)(*((Dcm_DsdCfg.pDcmDsdServiceReqSupplierNoti)[PortIndex].Confirmation))(
				Sid,
				ReqType,
				SourceAddress,
				DCM_RES_NEG_NOT_OK);
    }

    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
/*************************************************************************/
/*
 * Brief               <Determine the diagnostic request packet is allowed in the current session state.>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Sid: Service Id;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SesCheck(uint8 ProtocolCtrlId, uint8 Sid)
{
    uint8   ActiveSes;
    uint8   SesRefNum;
    uint8   SesCfgIndex;
    uint16  SidTabCfgIndex;
    uint16  SidTabServiceCfgIndex;
    boolean Flag;
    Std_ReturnType  ret;

    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
       /*Did not find the corresponding service in the configuration table*/
       return  E_NOT_OK;
    }

    /******************************/
    SesRefNum = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSessionLevel_Num;
    if(0u == SesRefNum)
    {
       /*No session state constraints*/
       return  E_OK;
    }
    /******************************/
    /*get Currently valid session state*/
    SchM_Enter_Dcm(Dcm_MkCtrl);
    ActiveSes = Dcm_MkCtrl.Dcm_ActiveSes;
    SchM_Exit_Dcm(Dcm_MkCtrl);

    Flag = FALSE;
    for(SesCfgIndex=0;(SesCfgIndex<SesRefNum)&&(FALSE==Flag);SesCfgIndex++)
    {
        if(ActiveSes == ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].pDcmDsdSessionLevelRef[SesCfgIndex])
        {
            Flag = TRUE;
        }
    }
    if(FALSE == Flag)
    {
        /*this service is not supported in the current valid session status*/
        return  E_NOT_OK;
    }
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SubSesCheck(uint8 ProtocolCtrlId, uint8 Sid)
{
    uint8   ActiveSes;
    uint8   SubSesRefNum;
    uint8   SubServiceNum;
    uint8   SesCfgIndex;
    uint16  SidTabCfgIndex;
    uint16  SidTabServiceCfgIndex;
    uint16  SubServieCfgIndex;
    P2CONST(Dcm_DsdSubServiceCfgType,AUTOMATIC,DCM_VAR) pDcmDsdSubService;
    boolean Flag;
    Std_ReturnType  ret;

    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
       /*Did not find the corresponding service in the configuration table*/
       return  E_NOT_OK;
    }

    /******************************/
	ret = DsdInternal_SearchSidTabSubServiceIndex(ProtocolCtrlId,
												  SidTabCfgIndex,
												  SidTabServiceCfgIndex,
												  &SubServieCfgIndex);
	if (E_NOT_OK == ret)
	{
		return E_NOT_OK;
	}
    SubServiceNum = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSubService_Num;
    if (0u == SubServiceNum)
    {
    	return E_OK;
    }
    pDcmDsdSubService = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSubService;
    SubSesRefNum = pDcmDsdSubService[SubServieCfgIndex].DcmDsdSubServiceSessionLevel_Num;
    if(0u == SubSesRefNum)
    {
       /*No session state constraints*/
       return  E_OK;
    }
    /******************************/
    /*get Currently valid session state*/
    SchM_Enter_Dcm(Dcm_MkCtrl);
    ActiveSes = Dcm_MkCtrl.Dcm_ActiveSes;
    SchM_Exit_Dcm(Dcm_MkCtrl);

    Flag = FALSE;
    for(SesCfgIndex=0;(SesCfgIndex<SubSesRefNum)&&(FALSE==Flag);SesCfgIndex++)
    {
        if(ActiveSes == pDcmDsdSubService[SubServieCfgIndex].DcmDsdSubServiceSessionLevelRef[SesCfgIndex])
        {
            Flag = TRUE;
        }
    }
    if(FALSE == Flag)
    {
        /*this service is not supported in the current valid session status*/
        return  E_NOT_OK;
    }
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/*************************************************************************/
/*
 * Brief               <determine the diagnostic request packet is allowed in the current security level>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Sid: Service Id;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SecurityCheck(uint8 ProtocolCtrlId,uint8 Sid)
{
    uint8   ActiveSec;
    uint8   SecRefNum;
    uint8   SecCfgIndex;
    uint16  SidTabCfgIndex;
    uint16  SidTabServiceCfgIndex;
    P2CONST(uint8,AUTOMATIC,DCM_CONST)pSecCfg;
    boolean Flag;
    Std_ReturnType  ret;

    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
       /*Did not find the corresponding service in the configuration table*/
       return  E_NOT_OK;
    }

	/*get Currently valid security level*/
	SchM_Enter_Dcm(Dcm_MkCtrl);
	ActiveSec = Dcm_MkCtrl.Dcm_ActiveSec;
	SchM_Exit_Dcm(Dcm_MkCtrl);

	/******************************/
    SecRefNum = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSecurityLevel_Num;
    if(SecRefNum != 0u)
    {
		Flag = FALSE;
		pSecCfg = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].pDcmDsdSecurityLevelRef;
		for(SecCfgIndex=0;(SecCfgIndex<SecRefNum)&&(FALSE==Flag);SecCfgIndex++)
		{
			if(ActiveSec == pSecCfg[SecCfgIndex])
			{
				Flag = TRUE;
			}
		}
		if(FALSE == Flag)
		{
			/*this service is not supported in the current valid security level*/
			return  E_NOT_OK;
		}
    }
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SubSecurityCheck(uint8 ProtocolCtrlId,uint8 Sid)
{
    uint8   ActiveSec;
    uint8   SubSecRefNum;
    uint8   SubServiceNum;
    uint8   SecCfgIndex;
    uint16  SidTabCfgIndex;
    uint16  SidTabServiceCfgIndex;
    uint16  SubServieCfgIndex;
    P2CONST(Dcm_DsdSubServiceCfgType,AUTOMATIC,DCM_CONST) pSubService;
    boolean Flag = FALSE;
    Std_ReturnType  ret;

    ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                               ProtocolCtrlId,
                                               &SidTabCfgIndex,
                                               &SidTabServiceCfgIndex);
    if(E_NOT_OK == ret)
    {
       /*Did not find the corresponding service in the configuration table*/
       return  E_NOT_OK;
    }

    /******************************/
    ret = DsdInternal_SearchSidTabSubServiceIndex(ProtocolCtrlId,
                                                  SidTabCfgIndex,
                                                  SidTabServiceCfgIndex,
                                                  &SubServieCfgIndex);
    if (E_NOT_OK == ret)
    {
        return E_NOT_OK;
    }

    /*get Currently valid security level*/
    SchM_Enter_Dcm(Dcm_MkCtrl);
    ActiveSec = Dcm_MkCtrl.Dcm_ActiveSec;
    SchM_Exit_Dcm(Dcm_MkCtrl);

    /*SWS_Dcm_00617*/
    SubServiceNum = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSubService_Num;
    if (0u == SubServiceNum)
    {
        return E_OK;
    }
    pSubService = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSubService;
    SubSecRefNum = pSubService[SubServieCfgIndex].DcmDsdSubServiceSecurityLevel_Num;
    if(0u == SubSecRefNum)
    {
        /*No security state constraints*/
        return  E_OK;
    }
    for(SecCfgIndex=0;(SecCfgIndex<SubSecRefNum)&&(FALSE==Flag);SecCfgIndex++)
    {
        if(ActiveSec == pSubService[SubServieCfgIndex].DcmDsdSubServiceSecurityLevelRef[SecCfgIndex])
        {
            Flag = TRUE;
        }
    }
    if(FALSE == Flag)
    {
        /*this service is not supported in the current valid security level*/
        return  E_NOT_OK;
    }
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
/*******************public functions******************************/
/*************************************************************************/
/*
 * Brief               <Set the negative response code NRC;>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      Nrc: the negative response code;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SetNrc(uint8 ProtocolCtrlId,
        Dcm_NegativeResponseCodeType Nrc)
{
    uint8  MsgCtrlIndexx;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Dcm_MsgCtrl[MsgCtrlIndexx].NRC 		= Nrc;
    Dcm_MsgCtrl[MsgCtrlIndexx].RspStyle 	= DCM_NEG_RSP;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
/*************************************************************************/
/*
 * Brief               <Set corresponding message processing state>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;
 *                      MsgState: message processing state>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_SetMsgState(uint8 ProtocolCtrlId,
		                                             Dcm_MsgStateType MsgState)
{
    uint8  MsgCtrlIndexx;

    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MsgState = MsgState;
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <The diagnostic request processing in DSD layer>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-060[SWS_Dcm_00178]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)DsdInternal_RxIndication(uint8  ProtocolCtrlId)
{
    uint8  Sid;
    uint8  MsgCtrlId;
    uint16 SidTabCfgIndex;
    uint16 SidTabServiceCfgIndex;
    Dcm_MsgStateType MsgState;
    Dcm_ProtocolStateType ProtocolState;
    Dcm_NegativeResponseCodeType errorCode;
    Std_ReturnType ret;
    P2FUNC(Std_ReturnType,DCM_APPL_CODE,Dcm_DiagnosticServicex)(
    		Dcm_OpStatusType OpStatus,
			uint8  ProtocolCtrlId,
    		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);

    /*************************************************/
    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    ProtocolState =  Dcm_ProtocolCtrl[ProtocolCtrlId].Dcm_ProtocolState;
    MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    MsgState  = Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState;
    Sid       = Dcm_MsgCtrl[MsgCtrlId].SID;
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    /*************************************************/
    if(DCM_MSG_RECEIVED != MsgState)
    {
        /*The error message status, reset message processing*/
        (void)DslInternal_ResetResource(ProtocolCtrlId);
        return  E_NOT_OK;
    }
    /*************************************************/
    if ( DCM_PROTOCOL_VALID != ProtocolState )
    {
    	/*xxx_startProtocol in DslInternal_ProtocolStart not return all ok */
        /*SWS_Dcm_00674*/
    	(void)DsdInternal_SetMsgState(ProtocolCtrlId,DCM_MSG_PROCESSED);
    	(void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_CONDITIONSNOTCORRECT);
    	DsdInternal_ProcessingDone(ProtocolCtrlId);
    	return E_NOT_OK;
    }
    /*************************************************/
    /*SID range checks*/
    /****@req DCM-FUNR-061[SWS_Dcm_00084]****/
#if(STD_OFF == DCM_RESPOND_ALL_REQUEST)
    if(((Sid>=0x40u)&&(Sid<=0x7Fu))||(Sid>=0xC0u))
    {
        /*(0x40=<SID<=0x7F)||(0xC0=<SID<=0xFF)*/
        (void)DslInternal_ResetResource(ProtocolCtrlId);
        return  E_NOT_OK;
    }
#endif
    /*************************************************/
    /*Check whether the SID is configured in the corresponding service table*/
    ret = DsdInternal_SidCheck(ProtocolCtrlId);
    if(E_NOT_OK == ret)
    {
        /****@req DCM-FUNR-066[SWS_Dcm_00197]****/
        /*Set message status "DCM_MSG_PROCESSED"*/
        (void)DsdInternal_SetMsgState(ProtocolCtrlId,DCM_MSG_PROCESSED);
        /*Set Nrc=0x11(service not supported )*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SERVICENOTSUPPORTED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    /*************************************************/
    /*Check whether received packets addressing modes support*/
    ret = DsdInternal_AddressingFormatCheck(ProtocolCtrlId,Sid);
    if(E_NOT_OK == ret)
    {
        /*Set message status "DCM_MSG_PROCESSED"*/
        (void)DsdInternal_SetMsgState(ProtocolCtrlId,DCM_MSG_PROCESSED);
        /*Set Nrc=0x11(service not supported )*/
        (void)DsdInternal_SetNrc(ProtocolCtrlId,DCM_E_SERVICENOTSUPPORTED);
        DsdInternal_ProcessingDone(ProtocolCtrlId);
        return E_NOT_OK;
    }
    /*************************************************/
    /*positive response  Suppress bit processing*/
    ret = DsdInternal_SuppressPosRsp(ProtocolCtrlId);
    if(E_NOT_OK == ret)
    {
    	(void)DslInternal_ResetResource(ProtocolCtrlId);
        return E_NOT_OK;
    }
    /*************************************************/
    /*Application environment and permissions verification*/
	#if (STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
    ret = DsdInternal_ManufacturerIndication(ProtocolCtrlId);
	#endif
	#if (STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
    ret = DsdInternal_SupplierIndication(ProtocolCtrlId);
	#endif
#if ((STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED) || (STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED))
    if(E_OK == ret)
#endif
    {
        SchM_Enter_Dcm(Dcm_ProtocolCtrl);
        SchM_Enter_Dcm(Dcm_MsgCtrl);
        MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
        Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState = DCM_MSG_PROCESSED;

        SchM_Exit_Dcm(Dcm_MsgCtrl);
        SchM_Exit_Dcm(Dcm_ProtocolCtrl);
        ret = DsdInternal_SearchSidTabServiceIndex(Sid,
                                                   ProtocolCtrlId,
                                                   &SidTabCfgIndex,
                                                   &SidTabServiceCfgIndex);
        if (ret != E_OK)
        {
        	return E_NOT_OK;
        }
        /****@req DCM-FUNR-070[DCM202]****/
        /****@req DCM-FUNR-078[DCM221]****/
        Dcm_DiagnosticServicex = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSidTabFnc;
        /*Enter the specific service handler*/
        if (Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus != DCM_CANCEL)
        {
            ret = (*Dcm_DiagnosticServicex)(Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus,ProtocolCtrlId,&errorCode);
        }
    }
    return ret;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DsdInternal_ProcessPage(uint8 ProtocolCtrlId)
{
    PduInfoType  PduInfo;
    PduIdType    DcmTxPduIdx;
    uint8        MsgCtrlIndexx;

    /*check the pagebuffer timer whether timeout*/
    if (Dcm_PageBufferData.TimeOut == TRUE)
    {
        return;
    }
	/*DSP requests transmission of filled page*/
    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);
    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    DcmTxPduIdx   = Dcm_MsgCtrl[MsgCtrlIndexx].DcmTxPduId;
    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

	#if (STD_ON == DCM_GENERIC_CONNECTION)
    PduInfo.SduDataPtr = &Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MetaData[0];
	#else
    PduInfo.SduDataPtr = NULL_PTR;
	#endif
    /*total length to be transmit of all pages*/
	PduInfo.SduLength  = (PduLengthType)Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResMaxDataLen;

	(void)PduR_DcmTransmit(DcmTxPduIdx, &PduInfo);

    /****Close P2Server Timer****/
    (void)DslInternal_P2ServerStop(ProtocolCtrlId);

    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DsdInternal_StartPagedProcessing(uint8 ProtocolCtrlId)
{
    /****Close P2Server Timer****/
    (void)DslInternal_P2ServerStop(ProtocolCtrlId);
	Dcm_PageBufferData.PageIndex = 0;
	Dcm_PageBufferData.Filled = FALSE;
	DspInternal_DcmUpdatePage(ProtocolCtrlId);
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
/*************************************************************************/
/*
 * Brief               <Response to a diagnosis request processing>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-079[DCM222]****/
/****@req DCM-FUNR-080[DCM223]****/
/****@req DCM-FUNR-081[DCM224]****/
/****@req DCM-FUNR-082[DCM225]****/
/****@req DCM-FUNR-083[DCM228]****/
/****@req DCM-FUNR-084[DCM231]****/
/****@req DCM-FUNR-091[DCM240]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DsdInternal_ProcessingDone(uint8  ProtocolCtrlId)
{
    uint8   Nrc;
    uint8   MsgCtrlIndexx;
    uint8   TxChannelCtrlIndex;
    uint8   TxChannelCfgIndex;
    uint16  Offset;
    Dcm_RspType   RspStylex;
    Dcm_MsgStateType  MsgState;
    Dcm_DslProtocolRxAddrType ReqTypex;
    /*Send NRC = 0x78 channel (Note: The channel length range: 8 ~ 4095)*/
    static VAR(uint8,DCM_VAR_POWER_ON_INIT)Dcm_Nrc78Channel[8] = {0};

    /*********************************************************/
    SchM_Enter_Dcm(Dcm_ProtocolCtrl);
    SchM_Enter_Dcm(Dcm_MsgCtrl);

    MsgCtrlIndexx = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
    MsgState      = Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MsgState;

    if(DCM_MSG_PROCESSED != MsgState)
    {
        /*Message processing error*/
    	(void)DslInternal_ResetResource(ProtocolCtrlId);
        SchM_Exit_Dcm(Dcm_MsgCtrl);
        SchM_Exit_Dcm(Dcm_ProtocolCtrl);
        return;
    }
    /*Set message status "DCM_MSG_TRANSMISSION "*/
    Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_MsgState = DCM_MSG_TRANSMISSION;
    TxChannelCtrlIndex = Dcm_MsgCtrl[MsgCtrlIndexx].Dcm_TxCtrlChannelIndex;

    SchM_Exit_Dcm(Dcm_MsgCtrl);
    SchM_Exit_Dcm(Dcm_ProtocolCtrl);

    RspStylex =  Dcm_MsgCtrl[MsgCtrlIndexx].RspStyle;
    switch(RspStylex)
    {
        case  DCM_POS_RSP:  /*--------------positive response---------------*/
              TxChannelCfgIndex = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
              Offset = (Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;
              /*Store the response data to the corresponding transmit channel*/
              SchM_Enter_Dcm(Dcm_MsgCtrl);
              Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pResData = &Dcm_Channel[Offset];
              SchM_Exit_Dcm(Dcm_MsgCtrl);
              /****@req DCM-FUNR-006[SWS_Dcm_00114]****/
              DslInternal_ProcessingDone(ProtocolCtrlId);
              break;

        case  DCM_NEG_RSP: /*---------------Negative response---------------*/
              SchM_Enter_Dcm(Dcm_MsgCtrl);
              /* Diagnosis  request type */
              ReqTypex  = (Dcm_DslProtocolRxAddrType)Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.MsgAddInfo.ReqType;
              Nrc      = Dcm_MsgCtrl[MsgCtrlIndexx].NRC;
              if((DCM_FUNCTIONAL == ReqTypex)
                 &&( (DCM_E_SERVICENOTSUPPORTED == Nrc)
                     ||(DCM_E_SUBFUNCTIONNOTSUPPORTED == Nrc)
                     ||(DCM_E_REQUESTOUTOFRANGE == Nrc)
					 ||(DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION == Nrc)
					 ||(DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION == Nrc)) )
              {
                  /****SWS_Dcm_00001,SWS_Dcm_00697****/
            	  (void)DslInternal_ResetResource(ProtocolCtrlId);
                  SchM_Exit_Dcm(Dcm_MsgCtrl);
              }
              else if(DCM_E_RESPONSE_PENDING == Nrc)
              {
                  /*NRC=0x78*/
                  /****@req DCM-FUNR-011[SWS_Dcm_00119]****/
                  /****@req DCM-FUNR-071[SWS_Dcm_00203]****/
                  /****@req DCM-FUNR-095[SWS_Dcm_00038]****/
                  Dcm_Nrc78Channel[0] = 0x7Fu;
                  Dcm_Nrc78Channel[1] = Dcm_MsgCtrl[MsgCtrlIndexx].SID;
                  Dcm_Nrc78Channel[2] = DCM_E_RESPONSE_PENDING;
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pResData      = &Dcm_Nrc78Channel[0];
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResDataLen    = 3u;
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResMaxDataLen = 3u;
                  SchM_Exit_Dcm(Dcm_MsgCtrl);
                  /****@req DCM-FUNR-006[DCM114]****/
                  DslInternal_ProcessingDone(ProtocolCtrlId);
              }
              else
              {
                  /*Other Nrc code*/
                  /****@req DCM-FUNR-095[SWS_Dcm_00038]****/
                  TxChannelCfgIndex  = Dcm_ChannelCtrl[TxChannelCtrlIndex].Dcm_ChannelCfgIndex;
                  Offset =(Dcm_DslCfg.pDcmChannelCfg)[TxChannelCfgIndex].offset;

                  SchM_Enter_Dcm(Dcm_Channel);
                  Dcm_Channel[Offset]   = 0x7Fu;
                  Dcm_Channel[Offset+1u] = Dcm_MsgCtrl[MsgCtrlIndexx].SID;
                  Dcm_Channel[Offset+2u] = (uint8)(Dcm_MsgCtrl[MsgCtrlIndexx].NRC);
                  SchM_Exit_Dcm(Dcm_Channel);
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.pResData      = &Dcm_Channel[Offset];
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResDataLen    = 3u;
                  Dcm_MsgCtrl[MsgCtrlIndexx].MsgContext.ResMaxDataLen = 3u;
                  SchM_Exit_Dcm(Dcm_MsgCtrl);
                  /****@req DCM-FUNR-006[DCM114]****/
                  DslInternal_ProcessingDone(ProtocolCtrlId);
              }
              break;

        case  DCM_POS_RSP_SUPPRESS: /*---------------positive response suppress---------------*/
                /*SWS_Dcm_00741,SWS_Dcm_00742*/
                #if (STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
                DsdInternal_ManufacturerConfirmation(ProtocolCtrlId);
                #endif

                #if (STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
                DsdInternal_SupplierConfirmation(ProtocolCtrlId);
                #endif
              /****@req DCM-FUNR-035[DCM141]****/
              /****@req DCM-FUNR-090[SWS_Dcm_00238]****/
              DspInternal_DcmConfirmation(ProtocolCtrlId);



              break;
        default:
        	  (void)DslInternal_ResetResource(ProtocolCtrlId);
              break;
    }
    return;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <Notify the DSD layer, respond to the confirmation>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <ProtocolCtrlId:The corresponding protocol control block ID number;>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DsdInternal_TxConfirmation(uint8  ProtocolCtrlId)
{
    /*SWS_Dcm_00741,SWS_Dcm_00742*/
    #if (STD_ON == DCM_DSD_REQUEST_MANUFACTURER_NOTIFICATION_ENABLED)
    DsdInternal_ManufacturerConfirmation(ProtocolCtrlId);
    #endif

    #if (STD_ON == DCM_DSD_REQUEST_SUPPLIER_NOTIFICATION_ENABLED)
    DsdInternal_SupplierConfirmation(ProtocolCtrlId);
    #endif
    /****@req DCM-FUNR-089[DCM236]****/
    DspInternal_DcmConfirmation(ProtocolCtrlId);


}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
