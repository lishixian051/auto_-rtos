/*******************************************************************************
**                                                                            **
** Copyright (C)    (2017)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dcm_Cbk.h                                                   **
**                                                                            **
**  Created on  :                                                             **
**  Author      : shushi                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : Callback declarations of DCM       				          **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


#ifndef COMMUNICATION_DCM_DCM_CBK_H_
#define COMMUNICATION_DCM_DCM_CBK_H_
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

#include "ComStack_Types.h"
/*************************************************************************/
/*
 * Brief               <This function is called at the start of receiving an N-SDU. The N-SDU might be
						fragmented into multiple N-PDUs (FF with one or more following CFs) or might
						consist of a single N-PDU (SF)>
 * ServiceId           <0x46>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <DcmRxPduId:  the received data PduId;
 *                      TpSduLength: This length identifies the overall number of bytes to be received.>
 * Param-Name[out]     <PduInfoPtr:  Pointer to pointer to PduInfoType containing data pointer and length of a receive buffe>
 * Param-Name[in/out]  <None>
 * Return              <BUFREQ_OK:
 *                      BUFREQ_E_NOT_OK:
 *                      BUFREQ_E_OVFL:
 *                      BUFREQ_E_BUSY:>
 * PreCondition        <None>
 * CallByAPI           <>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(BufReq_ReturnType,DCM_CODE)Dcm_StartOfReception(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_CONST) info,
		PduLengthType TpSduLength,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) bufferSizePtr);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern  FUNC(BufReq_ReturnType,DCM_CODE)Dcm_CopyRxData(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_CONST) info,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) bufferSizePtr);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_TpRxIndication(PduIdType id, Std_ReturnType result);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(BufReq_ReturnType,DCM_CODE)Dcm_CopyTxData(
		PduIdType id,
		P2CONST(PduInfoType, AUTOMATIC, DCM_CONST) info,
		P2VAR(RetryInfoType, AUTOMATIC, DCM_VAR) retry,
		P2VAR(PduLengthType, AUTOMATIC, DCM_VAR) availableDataPtr);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_TpTxConfirmation(PduIdType id, Std_ReturnType result);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_TxConfirmation(PduIdType  DcmTxPduId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <ComM module notice DCM modules, network communication mode is DCM_COMM_NO_COMMUNICATION>
 * ServiceId           <0x21>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_ComM_NoComModeEntered(uint8 NetworkId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <ComM module notice DCM modules, network communication mode is DCM_COMM_SILENT_COMMUNICATION>
 * ServiceId           <0x22>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_ComM_SilentComModeEntered(uint8 NetworkId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <ComM module notice DCM modules, network communication mode is DCM_COMM_FULL_COMMUNICATION>
 * ServiceId           <0x23>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <>
 * CallByAPI           <>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
extern   FUNC(void,DCM_CODE)Dcm_ComM_FullComModeEntered(uint8 NetworkId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#endif /* COMMUNICATION_DCM_DCM_CBK_H_ */
