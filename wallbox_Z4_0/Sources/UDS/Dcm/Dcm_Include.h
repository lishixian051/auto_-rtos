/*============================================================================*/
/*  Copyright (C) 2009-2011,  INFRASTRUCTURE SOFTWARE CO.,LTD.
 *
 *  All rights reserved. This software is  property. Duplication
 *  or disclosure without  written authorization is prohibited.
 *
 *  @file        <Dcm_Include.h>
 *  @brief       <>
 *
 *  <Compiler: CodeWarrior    MCU:XXX>
 *
 *  @author     <chen maosen>
 *  @date       <2013-03-20>
 */
/*============================================================================*/

#ifndef DCM_INCLUDE_H
#define DCM_INCLUDE_H


/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/
/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCM_INCLUDE_H_AR_MAJOR_VERSION  4
#define DCM_INCLUDE_H_AR_MINOR_VERSION  2
#define DCM_INCLUDE_H_AR_PATCH_VERSION  2
#define DCM_INCLUDE_H_SW_MAJOR_VERSION  1
#define DCM_INCLUDE_H_SW_MINOR_VERSION  0
#define DCM_INCLUDE_H_SW_PATCH_VERSION  2

/****************************** references *********************************/
#include "Dcm.h"
#include "Dcm_Cbk.h"
#include "Dcm_Internal.h"
#include "DcmDsl.h"
#include "DcmDsd.h"
#include "DcmDsp.h"
#include "PduR_Dcm.h"
#include "SchM_Dcm.h"
#include "ComM_Dcm.h"
#if(BSWM_DCM_ENABLE == STD_ON)
#include "BswM_Dcm.h"
#endif
#include "FreeRTimer.h"
#if(NVM_ENABLE == STD_ON)
#include "NvM_Types.h"
#include "NvM.h"
#endif

/****************************** declarations *********************************/

/****************************** definitions *********************************/

#endif /* DCM_INCLUDE_H_ */
