/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dcm_MemMap.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : ss                                                          **
**  Vendor      :                                                             **
**  DESCRIPTION : Memory mapping abstraction declaration of DCM               **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
#ifndef DCM_MEMMAP_H_
#define DCM_MEMMAP_H_
/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define DCM_MEMMAP_VENDOR_ID        62
#define DCM_MEMMAP_MODULE_ID        53
#define DCM_MEMMAP_AR_MAJOR_VERSION  4
#define DCM_MEMMAP_AR_MINOR_VERSION  2
#define DCM_MEMMAP_AR_PATCH_VERSION  2
#define DCM_MEMMAP_SW_MAJOR_VERSION  1
#define DCM_MEMMAP_SW_MINOR_VERSION  0
#define DCM_MEMMAP_SW_PATCH_VERSION  0
#define DCM_MEMMAP_VENDOR_API_INFIX  0

/*=======[M E M M A P  S Y M B O L  D E F I N E]==============================*/
#endif
#include "MemMap.h"

/*=======[E N D   O F   F I L E]==============================================*/
