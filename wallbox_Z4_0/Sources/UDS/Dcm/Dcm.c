/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dcm.c                                                       **
**                                                                            **
**  Created on  :                                                             **
**  Author      : shushi                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : Implementation of Dcm                                       **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/



/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "Dcm_Include.h"
#include "UDS.h"
/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define  DCM_C_AR_MAJOR_VERSION   (0x04u)
#define  DCM_C_AR_MINOR_VERSION   (0x02u)
#define  DCM_C_AR_PATCH_VERSION   (0x02u)
#define  DCM_C_SW_MAJOR_VERSION   (0x01u)    /*Major Version*/
#define  DCM_C_SW_MINOR_VERSION   (0x00u)    /*Minor Version*/
#define  DCM_C_SW_PATCH_VERSION   (0x02u)    /*Patch version*/
/*=======[V E R S I O N  C H E C K]===========================================*/
#if (DCM_C_AR_MAJOR_VERSION != DCM_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCM_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCM_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCM_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCM_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCM_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSD_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSD_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSD_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSD_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSD_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSD_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_COMMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_COMMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_COMMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_COMMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_COMMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_COMMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_MSGMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_MSGMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_MSGMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_MSGMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_MSGMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_MSGMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_PENDINGMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_PENDINGMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_PENDINGMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_PENDINGMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_PENDINGMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_PENDINGMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_PROTOCOLMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_PROTOCOLMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_PROTOCOLMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_PROTOCOLMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_PROTOCOLMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_PROTOCOLMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_SECURITYMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_SECURITYMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_SECURITYMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_SECURITYMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_SECURITYMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_SECURITYMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSL_SESSIONMANAGE_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSL_SESSIONMANAGE_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSL_SESSIONMANAGE_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSL_SESSIONMANAGE_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSL_SESSIONMANAGE_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSL_SESSIONMANAGE_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif

#if (DCM_C_AR_MAJOR_VERSION != DCMDSP_H_AR_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_MINOR_VERSION != DCMDSP_H_AR_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_AR_PATCH_VERSION != DCMDSP_H_AR_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MAJOR_VERSION != DCMDSP_H_SW_MAJOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_MINOR_VERSION != DCMDSP_H_SW_MINOR_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
#if (DCM_C_SW_PATCH_VERSION != DCMDSP_H_SW_PATCH_VERSION)
  #error "Dcm.c : Mismatch in Specification Major Version"
#endif
/*******************************************************************************
**                      Private Macro Definitions                             **
*******************************************************************************/

/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
typedef struct
{
    uint8 Dcm_VIN[17];
    boolean Flag;
}Dcm_VINType;

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
static VAR(Dcm_VINType,DCM_VAR_POWER_ON_INIT)Dcm_VIN;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_PendingManage(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_P2ServerTimer(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_S3Timer(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_SecTimer(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#if(DCM_PAGEDBUFFER_ENABLED == STD_ON)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_PageBufferTimer(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_GetUDSPhyPduID(P2VAR(PduIdType,AUTOMATIC,DCM_VAR)rxPhyPduId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_BootloaderResponse(void);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if (STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_Scheduler_0x2A(uint8  ProtocolCtrlId);
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
/*******************************************************************************
**                      Global Constant Definitions                           **
*******************************************************************************/



/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_MkCtrlType, DCM_VAR_NOINIT) Dcm_MkCtrl;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
uint8 gAppl_UpdataOK_ResponseFlag = FALSE;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"
VAR(uint16, DCM_VAR_NOINIT) SchedulerNum = 0U;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"
VAR(uint16, DCM_VAR_NOINIT) SchedulerIndex = 0U;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
VAR(uint8, DCM_VAR_NOINIT) SchedulerIndexChanged = FALSE;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
uint8 ReProgramingRequest = FALSE;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_ProgConditionsType, DCM_VAR_NOINIT) ProgConditions;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_PageBufferDataType,DCM_VAR_POWER_ON_INIT)Dcm_PageBufferData;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(Dcm_DspProgramType, DCM_VAR_NOINIT) Dcm_DspProgram;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
VAR(SchedulerQueueTransmitTypes,DCM_VAR_POWER_ON_INIT)Scheduler_0x2A_Transmit[DCM_PERIODICCONNECTION_NUM];
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"
VAR(uint16, DCM_VAR_NOINIT) SchedulerPengingDid = 0u;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_16BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
VAR(uint8, DCM_VAR_NOINIT) SchedulerPengingDDDid = FALSE;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
#endif
/*******************************************************************************
**                      Private Constant Definitions                          **
*******************************************************************************/

/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/


/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)DslInternal_InitPageBuffer(void)
{
    Dcm_PageBufferData.AlreadyPageSize = 0u;
    Dcm_PageBufferData.IloopOne = 0u;
    Dcm_PageBufferData.TotalDtcCount = 0u;
    Dcm_PageBufferData.TotalSize = 0UL;
    Dcm_PageBufferData.Filled = FALSE;
    Dcm_PageBufferData.PageTxOK = FALSE;
    Dcm_PageBufferData.PageIndex = 0u;
    Dcm_PageBufferData.LastFilled = TRUE;
    Dcm_PageBufferData.ThisPageSize = 0u;
    Dcm_PageBufferData.ReqOffset = 0u;
    Dcm_PageBufferData.LastFilledSize = 0u;
    Dcm_PageBufferData.ThisPageTxSize = 0u;
    Dcm_PageBufferData.LastNotTxDataSize = 0u;
    Dcm_PageBufferData.TimerStart = FALSE;
    Dcm_PageBufferData.TimeOut = FALSE;
    Dcm_PageBufferData.CurTimer = 0UL;
    Dcm_PageBufferData.ExpiredTimer = 0UL;
}
#define DCM_STOP_SEC_CODE
#include  "Dcm_MemMap.h"
#endif
/*************************************************************************/
/*
 * Brief                <initialization of DCM module.>
 * ServiceId            <0x01>
 * Sync/Async           <Synchronous>
 * Reentrancy           <Non Reentrant>
 * Param-Name[in]       <None>
 * Param-Name[out]      <None>
 * Param-Name[in/out]   <None>
 * Return               <None>
 * PreCondition         <None>
 * CallByAPI            <>
 */
/*************************************************************************/
/****req DCM-APIR-002[DCM037]****/
/****req DCM-APIR-003[DCM334]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)Dcm_Init(P2CONST(Dcm_CfgType, DCM_CONST, DCM_CONST_PBCFG)ConfigPtr)
{
    uint16  Index;
#if (STD_ON == DCM_DSP_DATA_FUNC_ENABLED)
    uint16  Offest = 0;
    P2VAR(uint8, AUTOMATIC, DCM_VAR)VIN_Data;
    Dcm_NegativeResponseCodeType ErrorCode;
#endif
    for(Index=0;Index<DCM_CHANNEL_NUM;Index++)
    {
        (void)DslInternal_InitChannelCtrl((uint8)Index);
    }
    for(Index=0;Index<DCM_MSG_NUM;Index++)
    {
    	(void)DslInternal_InitMsgCtrl((uint8)Index);
    }
    for(Index=0;Index<DCM_DSLPROTOCOLROW_NUM_MAX;Index++)
    {
    	(void)DslInternal_InitProtocolCtrl((uint8)Index);
    }
    for(Index=0;Index<DCM_CONNECTION_NUM;Index++)
    {
    	(void)DslInternal_InitConnectionCtrl((uint8)Index);
    }
    /*Clear Buffer*/
    for(Index=0;Index<DCM_CHANNEL_LENGTH;Index++)
    {
        Dcm_Channel[Index] = 0x00;
    }

#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
    DslInternal_InitSesCtrl();
#endif

#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
    DslInternal_InitSecCtrl();
#endif

#if ((STD_ON == DCM_DSP_ROUTINE_FUNC_ENABLED) && (DCM_DSP_ROUTINE_MAX_NUM > 0))
    Dsp_InitRoutineStates();
#endif
    DslInternal_InitComMCtrl();
    DslInternal_InitMkCtrl();

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
    DslInternal_InitPageBuffer();
#endif

    Dcm_DspProgram.address = 0UL;
    Dcm_DspProgram.blockId = 0x00;
    Dcm_DspProgram.Status = DCM_UDS0X36_INIT;
    Dcm_DspProgram.MemoryIdInfoIndex = 0xFF;
    Dcm_DspProgram.BlockLength = 0;

#if (DCM_DDDID_STORAGE == STD_ON)
#if(NVM_ENABLE == STD_ON)
    (void)NvM_ReadBlock(DCM_DDDID_STORAGE_BLOCKID, &Dcm_DDDid);
#endif
#else
#if(STD_ON == DCM_UDS_SERVICE0X2C_ENABLED)
    for(Index=0;Index<DCM_DSP_DDDID_MAX_NUMBER;Index++)
    {
        Dcm_DDDid[Index].DDDid = 0;
        Dcm_DDDid[Index].DDDIDNumOfElements = 0;
    }
#endif
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
    for(Index=0;Index<DCM_DSP_MAX_PERIODIC_DID_SCHEDULER;Index++)
    {
        SchedulerQueue[Index].Did = 0;
        SchedulerQueue[Index].Counter = 0;
        SchedulerQueue[Index].SubFunction = 0;
        SchedulerQueue[Index].Length = 0;
        SchedulerQueue[Index].ProtocolCtrlId = 0xFF;
        SchedulerQueue[Index].Data[0] = 0;
        SchedulerQueue[Index].Data[1] = 0;
        SchedulerQueue[Index].Data[2] = 0;
        SchedulerQueue[Index].Data[3] = 0;
        SchedulerQueue[Index].Data[4] = 0;
        SchedulerQueue[Index].Data[5] = 0;
        SchedulerQueue[Index].Data[6] = 0;
        SchedulerQueue[Index].Data[7] = 0;
        SchedulerQueue[Index].DDDid = FALSE;
    }
    for (Index=0;Index<DCM_PERIODICCONNECTION_NUM;Index++)
    {
        Scheduler_0x2A_Transmit[Index].PduId = 0xFF;
        Scheduler_0x2A_Transmit[Index].Transmit = FALSE;
    }
#endif

    /*get VIN [SWS_Dcm_01174]*/
    Dcm_VIN.Flag = FALSE;
#if (STD_ON == DCM_DSP_DATA_FUNC_ENABLED)
    if (NULL_PTR != Dcm_GeneralCfg.DcmVinRef)
    {
        Dcm_VIN.Flag = TRUE;
        for (Index = 0; Index < 17u; Index++)
        {
            Dcm_VIN.Dcm_VIN[Index] = 0xFF;
        }
        for (Index = 0;
             (Index < Dcm_GeneralCfg.DcmVinRef->DcmDspDidSignalNum)
                     && (Dcm_VIN.Flag == TRUE); Index++)
        {
            VIN_Data = &(Dcm_VIN.Dcm_VIN[Offest]);
            if (NULL_PTR != Dcm_GeneralCfg.DcmVinRef->pDcmDspDidSignal[Index].
                    pDcmDspDidData->DcmDspDataReadFnc)
            {
                if(E_OK != Dcm_GeneralCfg.DcmVinRef->pDcmDspDidSignal[Index].
                        pDcmDspDidData->DcmDspDataReadFnc(DCM_INITIAL,VIN_Data,&ErrorCode))
                {
                    Dcm_VIN.Flag = FALSE;
                }
                else
                {
                    Offest += ((Dcm_GeneralCfg.DcmVinRef->pDcmDspDidSignal[Index].
                            pDcmDspDidData->DcmDspDataSize +7u) >> 3u);
                }
            }
        }
    }
#endif
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#if(STD_ON == DCM_VERSION_INFO_API)
/*************************************************************************/
/*
 * Brief               <Returns the version information of this module>
 * ServiceId           <0x24>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <VersionInfo:Pointer to where to store the version information of this module>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <None>
 */
/*************************************************************************/
/****@req DCM-FUNR-245[DCM048]****/
/****@req DCM-APIR-004[DCM065]****/
/****@req DCM-APIR-005[DCM335]****/
/****@req DCM-APIR-006[DCM336]****/
/****@req DCM-APIR-007[DCM337]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE)Dcm_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, DCM_VAR)VersionInfo )
{
#if(STD_ON == DCM_DEV_ERROR_DETECT)
   SchM_Enter_Dcm(Dcm_MkCtrl);
   if(DCM_ON !=Dcm_MkCtrl.Dcm_MkState)
   {
       SchM_Exit_Dcm(Dcm_MkCtrl);
       Det_ReportError(DCM_MODULE_ID,
                       DCM_INSTANCE_ID,
                       DCM_GETVERSIONINFO_ID,
                       DCM_E_UNINIT);
       return;
   }
   SchM_Exit_Dcm(Dcm_MkCtrl);
#endif

#if(STD_ON == DCM_DEV_ERROR_DETECT)
   if(NULL_PTR == VersionInfo)
   {
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETVERSIONINFO_ID,
                        DCM_E_PARAM);
       return;
   }
#endif
   VersionInfo->vendorID = DCM_VENDOR_ID;
   VersionInfo->moduleID = DCM_MODULE_ID;
   VersionInfo->sw_major_version = DCM_H_SW_MAJOR_VERSION;
   VersionInfo->sw_minor_version = DCM_H_SW_MINOR_VERSION;
   VersionInfo->sw_patch_version = DCM_H_SW_PATCH_VERSION;
 }
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE) Dcm_GetVin(uint8* Data)
{
    Std_ReturnType ret = E_OK;
    if (Dcm_VIN.Flag == TRUE)
    {
        Dcm_MemoryCopy(Dcm_VIN.Dcm_VIN,Data,17);
    }
    else
    {
        ret = E_NOT_OK;
    }
	return  ret;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType, DCM_CODE) Dcm_DemTriggerOnDTCStatus(
		uint32 DTC,
		Dem_UdsStatusByteType DTCStatusOld,
		Dem_UdsStatusByteType DTCStatusNew)
{
	return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
/*************************************************************************/
/*
 * Brief               <This function provides the active security level value.>
 * ServiceId           <0x0d>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <SecLevel:Pointer to Active Security Level value >
 * Param-Name[in/out]   <None>
 * Return               <E_OK:
 * 						E_NOT_OK:>
 * PreCondition         <...>
 * CallByAPI            <APIName>
 */
/*************************************************************************/
/****@req DCM-FUNR-245[DCM048]****/
/****@req Dcm-APIR-008[DCM338]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_GetSecurityLevel(P2VAR(Dcm_SecLevelType, AUTOMATIC, DCM_VAR)SecLevel )
{
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    SchM_Enter_Dcm(Dcm_MkCtrl);
    if(DCM_ON !=Dcm_MkCtrl.Dcm_MkState)
    {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETSECURITYLEVEL_ID,
                        DCM_E_UNINIT);
        return  E_NOT_OK;
    }
#endif

#if(STD_ON == DCM_DEV_ERROR_DETECT)
   if( NULL_PTR == SecLevel)
   {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETSECURITYLEVEL_ID,
                        DCM_E_PARAM);
       return E_NOT_OK;
   }
#endif
   (*SecLevel) = Dcm_MkCtrl.Dcm_ActiveSec;
   SchM_Exit_Dcm(Dcm_MkCtrl);
   return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <This function provides the active session control type value. >
 * ServiceId           <0x06>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <SesType:Active Session Control Type value>
 * Param-Name[in/out]  <None>
 * Return              <E_OK,E_NOT_OK>
 * PreCondition        <>
 * CallByAPI           <>
 */
/*************************************************************************/
/****@req DCM-FUNR-245[DCM048]****/
/****@req Dcm-APIR-009[DCM339]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_GetSesCtrlType(P2VAR(Dcm_SesCtrlType, AUTOMATIC, DCM_VAR)SesType )
{
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    SchM_Enter_Dcm(Dcm_MkCtrl);
    if(DCM_ON !=Dcm_MkCtrl.Dcm_MkState)
    {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETSESCTRLTYPE_ID,
                        DCM_E_UNINIT);
        return  E_NOT_OK;
    }
#endif

#if(STD_ON == DCM_DEV_ERROR_DETECT)
    if( NULL_PTR == SesType)
    {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETSESCTRLTYPE_ID,
                        DCM_E_PARAM);
        return E_NOT_OK;
    }
#endif
    (*SesType) = Dcm_MkCtrl.Dcm_ActiveSes;
    SchM_Exit_Dcm(Dcm_MkCtrl);
    return  E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <This function returns the active protocol name. >
 * ServiceId           <0x0f>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Reentrant>
 * Param-Name[in]      <description...>
 * Param-Name[out]     <ActiveProtocol:Active protocol type value >
 * Param-Name[in/out]  <None>
 * Return              <Std_ReturnType>
 * PreCondition        <>
 * CallByAPI           <>
 */
/*************************************************************************/
/****@req DCM-FUNR-245[DCM048]****/
/****@req Dcm_APIR_010[DCM340]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_GetActiveProtocol(P2VAR(Dcm_ProtocolType, AUTOMATIC, DCM_VAR)ActiveProtocol )
{
#if(STD_ON == DCM_DEV_ERROR_DETECT)
    SchM_Enter_Dcm(Dcm_MkCtrl);
    if(DCM_ON !=Dcm_MkCtrl.Dcm_MkState)
    {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETACTIVEPROTOCOL_ID,
                        DCM_E_UNINIT);
        return  E_NOT_OK;
    }
#endif

#if(STD_ON == DCM_DEV_ERROR_DETECT)
    if(NULL_PTR == ActiveProtocol)
    {
        SchM_Exit_Dcm(Dcm_MkCtrl);
        Det_ReportError(DCM_MODULE_ID,
                        DCM_INSTANCE_ID,
                        DCM_GETACTIVEPROTOCOL_ID,
                        DCM_E_PARAM);
        return E_NOT_OK;
    }
#endif
    (*ActiveProtocol)=Dcm_MkCtrl.Dcm_ActiveProtocol;
    SchM_Exit_Dcm(Dcm_MkCtrl);
    return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"


#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_ResetToDefaultSession(void)
{
	/*SWS_Dcm_01062*/
	SchM_Enter_Dcm(Dcm_SesCtrl);
	Dcm_SesCtrl.Dcm_NewSes = DCM_DEFAULT_SESSION;
	SchM_Exit_Dcm(Dcm_SesCtrl);
	DslInternal_SesRefresh(Dcm_SesCtrl.Dcm_NewSes);
	SchM_Switch_DcmDiagnosticSessionControl(RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION);

	return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if 0
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_TriggerOnEvent(uint8 RoeEventId)
{

}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(Std_ReturnType,DCM_CODE)Dcm_SetActiveDiagnostic(boolean active)
{
    uint8 Index;
	if (FALSE == active)
	{
	    for (Index = 0; Index < DCM_MAINCONNECTION_NUM; Index++)
	    {
            SchM_Enter_Dcm(Dcm_CommCtrl);
            /*SWS_Dcm_01071*/
            Dcm_CommCtrl[Index].Dcm_ActiveDiagnostic = DCM_COMM_NOT_ACTIVE;
            SchM_Exit_Dcm(Dcm_CommCtrl);
	    }
	}
	else
	{
        for (Index = 0; Index < DCM_MAINCONNECTION_NUM; Index++)
        {
            SchM_Enter_Dcm(Dcm_CommCtrl);
            /*SWS_Dcm_01070*/
            Dcm_CommCtrl[Index].Dcm_ActiveDiagnostic = DCM_COMM_ACTIVE;
            SchM_Exit_Dcm(Dcm_CommCtrl);
            ComM_DCM_ActiveDiagnostic(Dcm_CommCtrl[Index].DcmDslProtocolComMChannelId);
        }
	}

	return E_OK;
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <This service is used for processing the tasks of the main loop. >
 * ServiceId           <0x25>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
/****@req Dcm_APIR_046[DCM053]****/
/****@req DCM-APIR-047[DCM362]****/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
FUNC(void,DCM_CODE) Dcm_MainFunction(void)
{
#if(STD_ON == DCM_DEV_ERROR_DETECT)
   SchM_Enter_Dcm(Dcm_MkCtrl);
   if(DCM_ON !=Dcm_MkCtrl.Dcm_MkState)
   {
       SchM_Exit_Dcm(Dcm_MkCtrl);
       Det_ReportError(DCM_MODULE_ID,
                       DCM_INSTANCE_ID,
                       DCM_MAIN_FUNCTION_ID,
                       DCM_E_UNINIT);
       return;
   }
   SchM_Exit_Dcm(Dcm_MkCtrl);
#endif

   /**************************************************/
   /*P2Server Timer background processing function*/
   Dcm_MainFunction_P2ServerTimer();

   /***************************************************/
   /*The Pending message background processing function*/
   Dcm_MainFunction_PendingManage();

   /**************************************************/
   /*S3Timer background processing function*/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
   Dcm_MainFunction_S3Timer();
#endif

   /*************************************************/
   /*SecTimer background processing function*/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
   Dcm_MainFunction_SecTimer();
#endif

   /**************************************************/
   /*Page_Buffer Timer background processing function*/
#if(DCM_PAGEDBUFFER_ENABLED == STD_ON)
   Dcm_MainFunction_PageBufferTimer();
#endif

   /*************************************************/
   Dcm_BootloaderResponse();
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/

/*************************************************************************/
/*
 * Brief               <The Pending message background processing function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_PendingManage(void)
{
	uint8  ProtocolCtrlId;
	uint8  MsgCtrlId;
	uint16 SidTabCfgIndex;
	uint16 SidTabServiceCfgIndex;
    Dcm_NegativeResponseCodeType errorCode;
    Std_ReturnType ret;
    P2FUNC(Std_ReturnType,DCM_APPL_CODE,Dcm_DiagnosticServicex)(
    		Dcm_OpStatusType OpStatus,
			uint8  ProtocolCtrlId,
    		P2VAR(Dcm_NegativeResponseCodeType,AUTOMATIC,DCM_VAR) ErrorCode);

	/************************************/
	/*Pending manage*/
	for(ProtocolCtrlId=0;ProtocolCtrlId<DCM_DSLPROTOCOLROW_NUM_MAX;ProtocolCtrlId++)
	{
		MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
		if(DCM_INVALID_UINT8 != MsgCtrlId)
		{
			if(DCM_MSG_RECEIVED == Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState)
			{
				if(DCM_PROTOCOL_INVALID == Dcm_ProtocolCtrl[ProtocolCtrlId].Dcm_ProtocolState)
				{
					/*the protocol not start yet*/
					/*DCM indication App start the protocol,App return E_PENDING*/
					(void)DslInternal_RxIndication(ProtocolCtrlId);
				}
				else
				{
					/*DCM indication Application to receive a new request,App return E_PENDING*/
					(void)DsdInternal_RxIndication(ProtocolCtrlId);
				}
			}
			else if(DCM_MSG_PROCESSED == Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState)
			{
				/*when Message is "DCM_MSG_PROCESSED" status,App return E_PENDING"*/
				(void)DsdInternal_SearchSidTabServiceIndex( Dcm_MsgCtrl[MsgCtrlId].SID,
															ProtocolCtrlId,
															&SidTabCfgIndex,
															&SidTabServiceCfgIndex);
				/*SWS_Dcm_00760*/
	            Dcm_DiagnosticServicex = ((Dcm_DsdCfg.pDcmDsdServiceTable)[SidTabCfgIndex].pDcmDsdService)[SidTabServiceCfgIndex].DcmDsdSidTabFnc;
			   /*Enter the specific service handler*/
	            if (
#if(STD_ON == DCM_DSLDIAGRESP_FORCERESPENDEN)
	            		(DCM_E_FORCE_RCRRP == Dcm_MsgCtrl[MsgCtrlId].Dcm_Ret) &&
#endif
						(DCM_FORCE_RCRRP_OK == Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus))
	            {
					Dcm_MsgCtrl[MsgCtrlId].Dcm_Ret = E_NOT_OK;
	            }
	            else if ((DCM_PENDING == Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus))
	            {
	            	/*do nothing, not return.*/
	            }
	            else
	            {
	            	/*DCM_CANCEL*/
	            	return;
	            }
	            (void)(*Dcm_DiagnosticServicex)(Dcm_MsgCtrl[MsgCtrlId].Dcm_OpStatus, ProtocolCtrlId, &errorCode);
			}
			else if(  (FALSE == Dcm_MsgCtrl[MsgCtrlId].SendFlag)
	                 &&(DCM_MSG_TRANSMISSION == Dcm_MsgCtrl[MsgCtrlId].Dcm_MsgState))
			{
				DslInternal_ProcessingDone(ProtocolCtrlId);
			}
			else
			{
				/*Avoid Misra error*/
			}
		}

#if (STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
		Dcm_MainFunction_Scheduler_0x2A(ProtocolCtrlId);
#endif
	}
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <P2Server Timer background processing function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_P2ServerTimer(void)
{
	uint8  ProtocolCtrlId;
	uint8  MsgCtrlId;
	uint32 OldTimer;
	uint32 ExpireTimer;
	uint32 Timer;

	/************************************************************/
	for(ProtocolCtrlId=0;ProtocolCtrlId<DCM_DSLPROTOCOLROW_NUM_MAX;ProtocolCtrlId++)
	{
		MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
		if(DCM_INVALID_UINT8 !=  MsgCtrlId)
		{
			if(DCM_P2TIMER_ON == (Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2State))
			{
				OldTimer    = Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2CurTimer;
				ExpireTimer = Dcm_MsgCtrl[MsgCtrlId].Dcm_P2Ctrl.Dcm_P2ExpiredTimer;
				Timer       = Frt_CalculateElapsedMS(OldTimer);
				if(Timer >= ExpireTimer)
				{/*P2Server Timer timeout*/
					(void)DslInternal_P2ServerTimeout(ProtocolCtrlId);
				}
			}
		}
	}
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

/*************************************************************************/
/*
 * Brief               <S3Timer background processing function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#if(STD_ON == DCM_SESSION_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_S3Timer(void)
{
	uint32 OldTimer;
	uint32 ExpireTimer;
	uint32 Timer;

	/************************************************************/
	/*S3Server timer*/
	if(DCM_S3TIMER_ON == (Dcm_SesCtrl.Dcm_S3Ctrl.Dcm_S3State))
	{
		OldTimer    = Dcm_SesCtrl.Dcm_S3Ctrl.Dcm_S3CurTimer;
		ExpireTimer = Dcm_SesCtrl.Dcm_S3Ctrl.Dcm_S3ExpiredTimer;
		Timer       = Frt_CalculateElapsedMS(OldTimer);
		if(Timer >= ExpireTimer)
		{
			/*S3Server Timer timeout*/
			DslInternal_S3ServerTimeout();
		}
	}
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/*************************************************************************/
/*
 * Brief               <SecTimer background processing function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#if(STD_ON == DCM_SECURITY_FUNC_ENABLED)
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_SecTimer(void)
{
    P2CONST(Dcm_DspSecurityRowType,AUTOMATIC,DCM_CONST)pSecurityRow;
	uint32 OldTimer;
	uint32 ExpireTimer;
	uint32 Timer;
	uint8    SecCfgIndex;

	/***************************************/
	/*security timer*/
	for (SecCfgIndex = 0; SecCfgIndex < DCM_SECURITY_NUM; SecCfgIndex++)
	{
        if(DCM_SECTIMER_ON==(Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecTimerState[SecCfgIndex]))
        {
            OldTimer     = Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecCurTimer[SecCfgIndex];
            ExpireTimer  = Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecExpiredTimer[SecCfgIndex];
            Timer        = Frt_CalculateElapsedMS(OldTimer);
            if(Timer >= ExpireTimer)
            {
                /*security timeout*/
                SchM_Enter_Dcm(Dcm_SecCtrl);
                Dcm_SecCtrl.Dcm_RunDlyCtrl.Dcm_SecTimerState[SecCfgIndex] = DCM_SECTIMER_OFF;
                SchM_Exit_Dcm(Dcm_SecCtrl);
                /*********************************/
                Dcm_SecCtrl.Dcm_FalseAcessCount[SecCfgIndex] = 2;
                Dcm_SecCtrl.Dcm_SubfunctionForSeed = 0;
                Dcm_SecCtrl.Dcm_SecServiceState = DCM_SERVICE_IDLE;
                pSecurityRow = &(Dcm_DspCfg.pDcm_DspSecurity->pDcm_DspSecurityRow[SecCfgIndex]);
                if (pSecurityRow != NULL_PTR)
                {
                    /*SWS_Dcm_01157*/
                    if ((TRUE == pSecurityRow->DcmDspSecurityAttemptCounterEnabled)
                       &&((USE_ASYNCH_FNC == pSecurityRow->DcmDspSecurityUsePort)
                               || (USE_ASYNCH_CLIENT_SERVER == pSecurityRow->DcmDspSecurityUsePort)))
                    {
                        if (pSecurityRow->Dcm_SetSecurityAttemptCounterFnc != NULL_PTR)
                        {
                            /*SWS_Dcm_01154*/
                            (void)pSecurityRow->Dcm_SetSecurityAttemptCounterFnc(Dcm_SecCtrl.Dcm_OpStatus,
                                    Dcm_SecCtrl.Dcm_FalseAcessCount[SecCfgIndex]);
                        }
                    }
                }
            }
        }
	}
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/**********************************************************************/
/*
 * Brief
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/***********************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_GetUDSPhyPduID(P2VAR(PduIdType,AUTOMATIC,DCM_VAR)rxPhyPduId)
{
    uint8  protocolIndex;
    uint8  rxpduidindex;
    P2CONST(Dcm_DslProtocolRxType,AUTOMATIC,DCM_CONST)pDslProtocolRx;

    if(NULL_PTR == rxPhyPduId)
    {
        return;
    }

    protocolIndex = (Dcm_DslCfg.pDcmDslProtocol)->DcmDslProtocolRow_Num;
    while(protocolIndex > 0u)
    {
    	protocolIndex--;
    	if(DCM_UDS_ON_CAN == (Dcm_DslCfg.pDcmDslProtocol)->
    	                     pDcmDslProtocolRow[protocolIndex].DcmDslProtocolID)
    	{
            /** regard a protocol only have a connection here */
			pDslProtocolRx = (Dcm_DslCfg.pDcmDslProtocol)->
    		                  pDcmDslProtocolRow[protocolIndex].
    		                  pDcmDslConnection[0].pDcmDslMainConnection->
    		                  pDcmDslProtocolRx;
		    rxpduidindex = (Dcm_DslCfg.pDcmDslProtocol)->
		                    pDcmDslProtocolRow[protocolIndex].pDcmDslConnection[0].
		                    pDcmDslMainConnection->DcmDslProtocolRx_Num;
		    while(rxpduidindex > 0u)
		    {
		        rxpduidindex--;
		        if(DCM_PHYSICAL == pDslProtocolRx[rxpduidindex].DcmDslProtocolRxAddrType)
		        {
		            *rxPhyPduId = pDslProtocolRx[rxpduidindex].DcmDslProtocolRxPduId;
		        }
		    }
    		break;
    	}
    }
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if(DCM_PAGEDBUFFER_ENABLED == STD_ON)
/*************************************************************************/
/*
 * Brief               <Page_Buffer Timer background processing function>
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/*************************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_PageBufferTimer(void)
{
    uint8  ProtocolCtrlId;
    uint8  MsgCtrlId;
    uint32 OldTimer;
    uint32 ExpireTimer;
    uint32 Timer;

    /************************************************************/
    for(ProtocolCtrlId=0;ProtocolCtrlId<DCM_DSLPROTOCOLROW_NUM_MAX;ProtocolCtrlId++)
    {
        MsgCtrlId = Dcm_ProtocolCtrl[ProtocolCtrlId].MsgCtrlIndex;
        if(DCM_INVALID_UINT8 !=  MsgCtrlId)
        {
            if(TRUE == Dcm_PageBufferData.TimerStart)
            {
                OldTimer    = Dcm_PageBufferData.CurTimer;
                ExpireTimer = Dcm_PageBufferData.ExpiredTimer;
                Timer       = Frt_CalculateElapsedMS(OldTimer);
                if(Timer >= ExpireTimer)
                {/*PageBuffer Timer timeout*/
                    Dcm_PageBufferData.TimeOut = TRUE;
                }
            }
        }
    }
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif

/**********************************************************************/
/*
 * Brief
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/***********************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_BootloaderResponse(void)
{
   PduInfoType  PduBuffer;
   PduInfoType  *PduInfo = &PduBuffer;
   PduIdType pduRxId = 0;
   PduLengthType availabeBufferLength;
   uint8 Data[2] = {0};

   if(DCM_WARM_START == Dcm_GetProgConditions(&ProgConditions))/*[SWS_Dcm_00536]*/
   {
      if (ProgConditions.ApplUpdated == TRUE)
      {
          gAppl_UpdataOK_ResponseFlag = TRUE;
          ProgConditions.ApplUpdated = FALSE;
          /*receive App updata flag,response 0x51 01*/
#if(BSWM_DCM_ENABLE == STD_ON)
          BswM_Dcm_ApplicationUpdated();/*[SWS_Dcm_00768] */
#endif

          Dcm_GetUDSPhyPduID(&pduRxId);

          PduInfo->SduLength = 0;
          (void)Dcm_StartOfReception(pduRxId,PduInfo,2,&availabeBufferLength);
          Data[0] = 0x10;
          Data[1] = 0x83;
          PduInfo->SduDataPtr = Data;
          PduInfo->SduLength = 2;
          (void)Dcm_CopyRxData(pduRxId,PduInfo,&availabeBufferLength);
          Dcm_TpRxIndication(pduRxId,E_OK);

          PduInfo->SduLength = 0;
          (void)Dcm_StartOfReception(pduRxId,PduInfo,2,&availabeBufferLength);
          Data[0] = 0x11;
          Data[1] = 0x01;
          PduInfo->SduDataPtr = Data;
          PduInfo->SduLength = 2;
          (void)Dcm_CopyRxData(pduRxId,PduInfo,&availabeBufferLength);
          Dcm_TpRxIndication(pduRxId,E_OK);
      }
   }
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"

#if (STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
/**********************************************************************/
/*
 * Brief
 * ServiceId           <None>
 * Sync/Async          <Synchronous>
 * Reentrancy          <Non Reentrant>
 * Param-Name[in]      <None>
 * Param-Name[out]     <None>
 * Param-Name[in/out]  <None>
 * Return              <None>
 * PreCondition        <None>
 * CallByAPI           <APIName>
 */
/***********************************************************************/
#define  DCM_START_SEC_CODE
#include "Dcm_MemMap.h"
static FUNC(void,DCM_CODE)Dcm_MainFunction_Scheduler_0x2A(uint8  ProtocolCtrlId)
{
    boolean Find;
    boolean Store;
    boolean ProcessContinue = TRUE;
    uint16 iloop;
    uint16 index;
    uint16 cnt;
    P2CONST(Dcm_DslMainConnectionType,TYPEDEF,DCM_CONST)pDcmDslMainConnection;
    PduInfoType  PduInfo;
    Std_ReturnType ret;
    PduIdType TxPduID;

    for (iloop = 0u; iloop < SchedulerNum; iloop++)
    {
        if ((SchedulerQueue[iloop].Counter != 0u)
            && (ProtocolCtrlId == SchedulerQueue[iloop].ProtocolCtrlId))
        {
            SchedulerQueue[iloop].Counter--;
        }
    }

    if ((SchedulerNum == 0U) && (SchedulerPengingDid != 0u)
            && (SchedulerPengingDDDid == TRUE))
    {
        /*[SWS_Dcm_01116],[SWS_Dcm_01115]*/
        (void)DspInternalUDS0x2A_DDDid_Read_Data(
                DCM_CANCEL,
                SchedulerQueue[iloop].Did,
                &(SchedulerQueue[iloop].Data[1]));
        SchedulerPengingDid = 0;
        SchedulerPengingDDDid = FALSE;
    }

    pDcmDslMainConnection =
            Dcm_DslCfg.pDcmDslProtocol->pDcmDslProtocolRow[ProtocolCtrlId].
                    pDcmDslConnection->pDcmDslMainConnection;
    if (pDcmDslMainConnection == NULL_PTR)
    {
        ProcessContinue = FALSE;
    }
    if (ProcessContinue == TRUE)
    {
        for (index = 0u; index < DCM_MAINCONNECTION_NUM;index++)
        {
            if ((Dcm_CommCtrl[index].DcmDslProtocolComMChannelId
                    == pDcmDslMainConnection->DcmDslProtocolComMChannelId)
                && (Dcm_CommCtrl[index].Dcm_CommState != DCM_COMM_FULL_COMMUNICATION))
            {
                ProcessContinue = FALSE;
            }
        }
    }
    if (ProcessContinue == TRUE)
    {
        index = 0u;
        while (index < pDcmDslMainConnection->
                pDcmDslPeriodicTranmissionConRef->DcmPeriodicTxPduIdNum)
        {
            /*[SWS_Dcm_01103]*/
            for (cnt=0u;cnt<DCM_PERIODICCONNECTION_NUM;cnt++)
            {
                if (Scheduler_0x2A_Transmit[cnt].PduId
                        == pDcmDslMainConnection->pDcmDslPeriodicTranmissionConRef->
                        DcmDslPeriodicConnection[index].DcmDslPeriodicTxConfirmationPduId)
                {
                    if (Scheduler_0x2A_Transmit[cnt].Transmit == TRUE)
                    {
                        ProcessContinue = FALSE;
                    }
                }
            }
            if (ProcessContinue == TRUE)
            {
                Find = FALSE;
                for (iloop = SchedulerIndex; (iloop < SchedulerNum)
                && (Find == FALSE); iloop++)
                {
                    if (SchedulerQueue[iloop].Counter == 0u)
                    {
                        /*[SWS_Dcm_01106]*/
                        if (SchedulerQueue[iloop].DDDid == TRUE)
                        {
                            ret = DspInternalUDS0x2A_DDDid_Read_Data(
                                    DCM_INITIAL,
                                    SchedulerQueue[iloop].Did,
                                    &(SchedulerQueue[iloop].Data[1]));
                        }
                        else
                        {
                            ret = DspInternalUDS0x2A_Did_Read_Data(
                                    SchedulerQueue[iloop].Did,
                                    &(SchedulerQueue[iloop].Data[1]));
                        }

                        if (ret == E_OK)
                        {
                            SchedulerQueue[iloop].Data[0] = (uint8)(SchedulerQueue[iloop].Did);
                            PduInfo.SduDataPtr = &(SchedulerQueue[iloop].Data[0]);
                            PduInfo.SduLength = SchedulerQueue[iloop].Length;

                            TxPduID = pDcmDslMainConnection->pDcmDslPeriodicTranmissionConRef->
                                    DcmDslPeriodicConnection[index].DcmDslTxPduRPduId;
                            /*[SWS_Dcm_01101]*/
                            (void)PduR_DcmTransmit(TxPduID,&PduInfo);

                            /*[SWS_Dcm_01102]*/
                            switch (SchedulerQueue[iloop].SubFunction)
                            {
                                case DCM_UDS0X2A_01_SLOW:
                                    SchedulerQueue[iloop].Counter =
                                            (uint8)(Dcm_DspCfg.Dcm_DspPeriodicTransmission->
                                            DcmDspPeriodicTransmissionSlowRate
                                            / Dcm_GeneralCfg.DcmTaskTime);
                                break;
                                case DCM_UDS0X2A_02_MEDIUM:
                                    SchedulerQueue[iloop].Counter =
                                            (uint8)(Dcm_DspCfg.Dcm_DspPeriodicTransmission->
                                            DcmDspPeriodicTransmissionMediumRate
                                            / Dcm_GeneralCfg.DcmTaskTime);
                                break;
                                case DCM_UDS0X2A_03_FAST:
                                    SchedulerQueue[iloop].Counter =
                                            (uint8)(Dcm_DspCfg.Dcm_DspPeriodicTransmission->
                                            DcmDspPeriodicTransmissionFastRate
                                            / Dcm_GeneralCfg.DcmTaskTime);
                                break;
                                default:
                                    break;
                            }
                            Find = TRUE;
                            SchedulerIndex = iloop;
                            Store = FALSE;
                            for (cnt=0u;cnt<DCM_PERIODICCONNECTION_NUM;cnt++)
                            {
                                if (Scheduler_0x2A_Transmit[cnt].PduId
                                        == pDcmDslMainConnection->pDcmDslPeriodicTranmissionConRef->
                                        DcmDslPeriodicConnection[index].DcmDslPeriodicTxConfirmationPduId)
                                {
                                    Store = TRUE;
                                    Scheduler_0x2A_Transmit[cnt].Transmit = TRUE;
                                }
                            }
                            if (Store == FALSE)
                            {
                                for (cnt=0u;cnt<DCM_PERIODICCONNECTION_NUM;cnt++)
                                {
                                    if (Scheduler_0x2A_Transmit[cnt].PduId == 0xFFu)
                                    {
                                        Scheduler_0x2A_Transmit[cnt].PduId = pDcmDslMainConnection->
                                           pDcmDslPeriodicTranmissionConRef->
                                           DcmDslPeriodicConnection[index].DcmDslPeriodicTxConfirmationPduId;
                                        Scheduler_0x2A_Transmit[cnt].Transmit = TRUE;
                                    }
                                }
                            }
                            SchedulerPengingDid = 0;
                            SchedulerPengingDDDid = FALSE;
                        }
                        else if (ret == E_NOT_OK)
                        {
                            SchedulerIndex = iloop;
                            SchedulerPengingDDDid = FALSE;
                            SchedulerPengingDid = 0;
                        }
                        else if (ret == DCM_E_PENDING)
                        {
                            SchedulerPengingDid = SchedulerQueue[iloop].Did;
                            SchedulerPengingDDDid = SchedulerQueue[iloop].DDDid;
                            Find = TRUE;
                        }
                        else
                        {
                            /*idle*/
                        }
                    }
                }
                if (SchedulerIndex >= (SchedulerNum - 1u))
                {
                    SchedulerIndex = 0u;
                }
            }
            index++;
        }
    }
}
#define  DCM_STOP_SEC_CODE
#include "Dcm_MemMap.h"
#endif
/*******************************************************************************
**                            General Notes                                   **
*******************************************************************************/
