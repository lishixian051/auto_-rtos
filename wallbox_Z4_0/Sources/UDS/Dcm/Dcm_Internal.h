/*******************************************************************************
**                                                                            **
** Copyright (C)    (2017)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : Dcm_Internal.h                                              **
**                                                                            **
**  Created on  :                                                             **
**  Author      : shushi                                                      **
**  Vendor      :                                                             **
**  DESCRIPTION : Internal variable and API declaration of DCM   		      **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/

#ifndef COMMUNICATION_DCM_DCM_INTERNAL_H_
#define COMMUNICATION_DCM_DCM_INTERNAL_H_

/*============================================================================*/
/*=======[R E V I S I O N   H I S T O R Y]====================================*
*  <VERSION>    <DATE>       <AUTHOR>    <REVISION LOG>
*  V1.0.0       2018-3-20    shushi      Initial version
*  V1.0.1       2019-12-24   tao.yu      QAC check fix
*  V1.0.2       2020-1-7     tao.yu      Commercial project problem modification
============================================================================*/
/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "Dcm_Types.h"

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define  DCM_INVALID_UINT8      (0xFFu)
#define  DCM_INVALID_UINT16     (0xFFFFu)
#define  DCM_INVALID_UINT32     (0xFFFFFFFFUL)
#define  DCM_INVALID_PDUID      (0xFFu)
/*****************************Development error values ***********************/
#define  DCM_E_INTERFACE_TIMEOUT             (0x01u) /*Application-Interface: Timeout*/
#define  DCM_E_INTERFACE_RETURN_VALUE		 (0x02u) /*Application-Interface: Return-value out of range*/
#define  DCM_E_INTERFACE_BUFFER_OVERFLOW     (0x03u) /*Application-Interface: Buffer Overflow*/
#define  DCM_E_UNINIT                        (0x05u) /*Internal: DCM not initialized*/
#define  DCM_E_PARAM                         (0x06u) /*DCM API function with invalid input parameter*/
#define  DCM_E_PARAM_POINTER                 (0x07u) /*DCM API service invoked with NULL POINTER as parameter*/
#define  DCM_E_INIT_FAILED                   (0x08u) /*Dcm initialisation failed*/
#define  DCM_E_SET_PROG_CONDITIONS_FAIL      (0x09u) /*Storing the ProgConditions failed*/
/*******************************Dcm Module API ID*****************************/
#define  DCM_INIT_ID                         (0x01u) /*Dcm_Init()*/
#define  DCM_STARTOFRECEPTION_ID             (0x46u)
#define  DCM_COPYRXDATA_ID                   (0x44u)
#define  DCM_TPRXINDICATION_ID               (0x45u)
#define  DCM_COPYTXDATA_ID                   (0x43u)
#define  DCM_TPTXCONFIRMATION_ID             (0x48u)
#define  DCM_TXCONFIRMATION_ID               (0x40u)
#define  DCM_GETSESCTRLTYPE_ID               (0x06u) /*Dcm_GetSesCtrlType() */
#define  DCM_GETSECURITYLEVEL_ID             (0x0Du) /*Dcm_GetSecurityLevel()*/
#define  DCM_GETACTIVEPROTOCOL_ID            (0x0Fu) /*Dcm_GetActiveProtocol()*/
#define  DCM_COMM_NOCOMMODEENTERED           (0x21u) /*Dcm_Comm_NoComModeEntered()*/
#define  DCM_COMM_SILENTCOMMODEENTERED       (0x22u) /*Dcm_Comm_SilentComModeEntered()*/
#define  DCM_COMM_FULLCOMMODEENTERED         (0x23u) /*Dcm_Comm_FULLComModeEntered()*/
#define  DCM_GETVERSIONINFO_ID               (0x24u) /*Dcm_GetVersionInfo()*/
#define  DCM_MAIN_FUNCTION_ID                (0x25u) /*Dcm_Main_Function()*/
/****************************************************************************/
#define  E_SESSION_NOT_ALLOWED   ((Std_ReturnType)4) /*Application does not allow the session change*/
#define  E_PROTOCOL_NOT_ALLOWED  ((Std_ReturnType)5) /*Application does not allow further processing of the protocol*/
#define  E_REQUEST_NOT_ACCEPTED  ((Std_ReturnType)8) /**/
#define  E_REQUEST_ENV_NOK       ((Std_ReturnType)9) /**/
#define  DCM_E_PENDING           ((Std_ReturnType)10)/**/
#define  E_COMPARE_KEY_FAILED    ((Std_ReturnType)11)/*Compare key failure*/
#if(STD_ON == DCM_DSLDIAGRESP_FORCERESPENDEN)
#define  DCM_E_FORCE_RCRRP       ((Std_ReturnType)12)/*Application requests sent immediately NRC = 0x78*/
#endif
/****************************************************************************/
typedef  uint8   Dcm_StatusType;
#define  DCM_E_OK      							((Dcm_StatusType)0x00)
#define  DCM_E_ROE_NOT_ACCEPTED      			((Dcm_StatusType)0x06)
#define  DCM_E_PERIODICID_NOT_ACCEPTED      	((Dcm_StatusType)0x07)
/****************************************************************************/
typedef  uint8   Dcm_ReturnReadMemoryType;
#define  DCM_READ_OK      							((Dcm_ReturnReadMemoryType)0x00)
#define  DCM_READ_PENDING      						((Dcm_ReturnReadMemoryType)0x01)
#define  DCM_READ_FAILED      						((Dcm_ReturnReadMemoryType)0x02)
#define  DCM_READ_FORCE_RCRRP      					((Dcm_ReturnReadMemoryType)0x03)
/****************************************************************************/
typedef  uint8   Dcm_ReturnWriteMemoryType;
#define  DCM_WRITE_OK      							((Dcm_ReturnWriteMemoryType)0x00)
#define  DCM_WRITE_PENDING      					((Dcm_ReturnWriteMemoryType)0x01)
#define  DCM_WRITE_FAILED      						((Dcm_ReturnWriteMemoryType)0x02)
#define  DCM_WRITE_FORCE_RCRRP      				((Dcm_ReturnWriteMemoryType)0x03)
/****************************************************************************/
/****************************************************************************/
#define SUPPORT_REQUEST                     0x01u
#define NORMAL_REQUEST                      0x02u

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/


/********************************************
      OBD  buffer
 ********************************************/
typedef  struct
{
    PduLengthType   Length;
    uint8   Buffer[DCM_FRAME_LENGTH];
}Dcm_OBDMessageType;

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
/********************************************
      Page  buffer  Data
 ********************************************/
typedef struct
{
    uint32            TotalSize;
    uint16             IloopOne;
    uint16             TotalDtcCount;
    uint16            ReqOffset;
    uint16            ThisPageSize;
    uint16            ThisPageTxSize;
    uint16            LastFilledSize;
    uint16            AlreadyPageSize;
    boolean           Filled;
    boolean           PageTxOK;
    uint8             PageIndex;
    boolean           LastFilled;
    Dcm_MsgType       pResData;
#if (DCM_FD == STD_OFF)
    uint8             LastNotTxData[8];
#else
    uint8             LastNotTxData[64];
#endif
    uint16            LastNotTxDataSize;
    boolean           TimerStart;
    boolean           TimeOut;
    uint32            CurTimer;
    uint32            ExpiredTimer;
}Dcm_PageBufferDataType;
#endif


/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/
/*DcmGeneral*/
extern    const  Dcm_GeneralCfgType     Dcm_GeneralCfg;

/*Dcm*/
extern    const  Dcm_CfgType            Dcm_Cfg;
/*DcmDsp*/
extern    const  Dcm_DspCfgType         Dcm_DspCfg;
/*DcmDsd*/
extern    const  Dcm_DsdCfgType         Dcm_DsdCfg;
/*DcmDsl*/
extern    const  Dcm_DslCfgType         Dcm_DslCfg;
extern    const  Dcm_DslProtocolRowType Dsl_ProtocolRowCfg[DCM_DSLPROTOCOLROW_NUM_MAX];
extern    const  Dcm_DslProtocolRxType  Dsl_Protocol_Connection_RxCfg[DCM_DSL_RX_ID_NUM];
extern    const  Dcm_DslProtocolTxType  Dsl_Protocol_Connection_TxCfg[DCM_DSL_TX_ID_NUM];
extern    const  Dcm_DslConnectionType  Dsl_Protocol_ConnectionCfg[DCM_CONNECTION_NUM];
extern    const  Dcm_DslMainConnectionType Dsl_Protocol_MainConnectionCfg[DCM_MAINCONNECTION_NUM];


#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern    VAR(uint8,DCM_VAR_POWER_ON_INIT)Dcm_Channel[DCM_CHANNEL_LENGTH];
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_UDS_FUNC_ENABLED)
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern  VAR(Dcm_OBDMessageType,DCM_VAR_POWER_ON_INIT)Dcm_OBDMessage;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif

#if(STD_ON == DCM_UDS_SERVICE0X2A_ENABLED)
#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern VAR(SchedulerQueueTransmitTypes,DCM_VAR_POWER_ON_INIT)Scheduler_0x2A_Transmit[DCM_PERIODICCONNECTION_NUM];
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern  VAR(Dcm_MkCtrlType, DCM_VAR_NOINIT) Dcm_MkCtrl;/*Module control resource*/
#define DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
extern  uint8 gAppl_UpdataOK_ResponseFlag;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"
extern uint8 ReProgramingRequest;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_8BIT
#include "Dcm_MemMap.h"

#define  DCM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern VAR(Dcm_ProgConditionsType, DCM_VAR_NOINIT) ProgConditions;
#define  DCM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "Dcm_MemMap.h"

#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
#define  DCM_START_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
extern  VAR(Dcm_PageBufferDataType,DCM_VAR_POWER_ON_INIT)Dcm_PageBufferData;
#define  DCM_STOP_SEC_VAR_POWER_ON_INIT_UNSPECIFIED
#include "Dcm_MemMap.h"
#endif

/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#if(STD_ON == DCM_PAGEDBUFFER_ENABLED)
extern FUNC(void,DCM_CODE)DslInternal_InitPageBuffer(void);
#endif


#endif /* COMMUNICATION_DCM_DCM_INTERNAL_H_ */
