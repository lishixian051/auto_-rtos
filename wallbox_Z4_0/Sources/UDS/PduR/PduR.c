/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR.c                                                      **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : Implementation for PDUR                                     **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/

/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR.h"
#if(STD_OFF == PDUR_ZERO_COST_OPERATION)
#if(STD_ON == PDUR_DEV_ERROR_DETECT)
#include "Det.h"
#endif
#if(STD_ON == PDUR_CANIF_SUPPORT)
#include "PduR_CanIf.h"
#endif
#if(STD_ON == PDUR_CANTP_SUPPORT)
#include "PduR_CanTp.h"
#endif
#if(STD_ON == PDUR_J1939TP_SUPPORT)
#include "PduR_J1939Tp.h"
#endif
#if(STD_ON == PDUR_COM_SUPPORT)
#include "PduR_Com.h"
#endif
#if(STD_ON == PDUR_DCM_SUPPORT)
#include "PduR_Dcm.h"
#endif
#if(STD_ON == PDUR_J1939DCM_SUPPORT)
#include "PduR_J1939Dcm.h"
#endif
#if(STD_ON == PDUR_IPDUM_SUPPORT)
#include "PduR_IpduM.h"
#endif
#if(STD_ON == PDUR_J1939RM_SUPPORT)
#include "PduR_J1939Rm.h"
#endif
#if(STD_ON == PDUR_LDCOM_SUPPORT)
#include "PduR_LdCom.h"
#endif
#if(STD_ON == PDUR_SECOC_SUPPORT)
#include "PduR_SecOC.h"
#endif
#if(STD_ON == PDUR_DBG_SUPPORT)
#include "PduR_Dbg.h"
#endif
#if(STD_ON == PDUR_CANNM_SUPPORT)
#include "PduR_CanNm.h"
#endif
#if(STD_ON == PDUR_OSEKNM_SUPPORT)
#include "PduR_OsekNm.h"
#endif
#if(STD_ON == PDUR_LINIF_SUPPORT)
#include "PduR_LinIf.h"
#endif
#if(STD_ON == PDUR_LINTP_SUPPORT)
#include "PduR_LinTp.h"
#endif
#if(STD_ON == PDUR_FRIF_SUPPORT)
#include "PduR_FrIf.h"
#endif
#if(STD_ON == PDUR_FRNM_SUPPORT)
#include "PduR_FrNm.h"
#endif
/*******************************************************************************
**                      Imported Compiler Switch Check                        **
*******************************************************************************/
#define PDUR_C_AR_MAJOR_VERSION  4U
#define PDUR_C_AR_MINOR_VERSION  2U
#define PDUR_C_AR_PATCH_VERSION  2U
#define PDUR_C_SW_MAJOR_VERSION  1U
#define PDUR_C_SW_MINOR_VERSION  0U
#define PDUR_C_SW_PATCH_VERSION  0U
/*******************************************************************************
**                       Version  Check                                       **
*******************************************************************************/
#if (PDUR_C_AR_MAJOR_VERSION != PDUR_H_AR_MAJOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_C_AR_MINOR_VERSION != PDUR_H_AR_MINOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_C_AR_PATCH_VERSION != PDUR_H_AR_PATCH_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_C_SW_MAJOR_VERSION != PDUR_H_SW_MAJOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
#if (PDUR_C_SW_MINOR_VERSION != PDUR_H_SW_MINOR_VERSION)
  #error "PduR.c : Mismatch in Specification Major Version"
#endif
/*******************************************************************************
**                      Private Variable Definitions                          **
*******************************************************************************/
/* PduR init status, at first define it as PDUR_UNINIT */
static PduR_StateType PduR_Status = PDUR_UNINIT;

/*If gateway:dest pdu(PduRDestPduDataProvision) is configuration PDUR_DIRECT*/
#if(PDUR_GATEWAY_DIRECT_BUFFER_PDU_SUM > 0)
static PduR_DestStateType PduR_DestPduState[PDUR_GATEWAY_DIRECT_BUFFER_PDU_SUM];
#else
static P2VAR(PduR_DestStateType, AUTOMATIC, PDUR_APPL_DATA) PduR_DestPduState = NULL_PTR;
#endif
/*******************************************************************************
**                      Private Function Declarations                         **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
#if(STD_ON == PDUR_UPMODE_SUPPORT)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info);
#endif/*STD_ON == PDUR_UPMODE_SUPPORT*/

#if(STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeCancelTransmit(PduIdType id);
#endif/*STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT*/

#if(STD_ON == PDUR_UPMODE_CHANGE_PARAMETER)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeChangeParameter(PduIdType id,TPParameterType parameter,uint16 value);
#endif/*STD_ON == PDUR_UPMODE_CHANGE_PARAMETER*/

#if(STD_ON == PDUR_UPMODE_CANCEL_RECEIVE)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeCancelReceive(PduIdType id);
#endif/*STD_ON == PDUR_UPMODE_CANCEL_RECEIVE*/

#if(STD_ON == PDUR_TP_SUPPORT)
static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);

static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);

static FUNC(void, PDUR_CODE)
PduR_LoTpRxIndication
(
	PduIdType id,
	Std_ReturnType result
);

static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
);

static FUNC(void, PDUR_CODE)
PduR_LoTpTxConfirmation(PduIdType id,Std_ReturnType result);
#endif/*STD_ON == PDUR_TP_SUPPORT*/

#if((STD_ON == PDUR_CANIF_SUPPORT) || (STD_ON == PDUR_LINIF_SUPPORT))
/*Rx If pdu gateway to other If Pdus*/
static FUNC(void, PDUR_CODE)
PduR_GateWayIfPdu(uint8 DestModule,PduIdType DestPduId,P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) InfoPtr);
/*store the If Rx Pdu data to buffer*/
static FUNC(void, PDUR_CODE)
PduR_EnQueueBuffer(PduIdType PduId,P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA)PduInfo);
/*clear the buffer(the buffer data have transmit,Whether or not it succeeds)*/
static FUNC(void, PDUR_CODE)
PduR_DeQueueBuffer(PduIdType PduId);
#endif/*(STD_ON == PDUR_CANIF_SUPPORT) || (STD_ON == PDUR_LINIF_SUPPORT)*/
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"
/*******************************************************************************
**                      Global Variable Definitions                           **
*******************************************************************************/
/*PduR PB Configuration Run Time point parameter*/
P2CONST(PduR_PBConfigType, PDUR_CONST, PDUR_CONST_PBCFG) PduR_ConfigStd;

/*all route dest pdus is enable or disable*/
#if(PDUR_DEST_PDU_SUM > 0)
boolean PduRIsEnabled[PDUR_DEST_PDU_SUM];
#else
P2VAR(boolean, AUTOMATIC, PDUR_APPL_DATA) PduRIsEnabled = NULL_PTR;
#endif

/*up module Tx 1:n(include If and Tp)*/
#if(PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM > 0)
boolean PduR_UpTxState[PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM];
#else
P2VAR(boolean, AUTOMATIC,PDUR_APPL_DATA) PduR_UpTxState = NULL_PTR;
#endif

/*up module Tx 1:n (only Tp)*/
#if(PDUR_SRC_UP_MULTICAST_TX_TP_SUM > 0)
PduR_UpTxTpMulPduRunTimeType PduR_UpTxTpMulRoute[PDUR_SRC_UP_MULTICAST_TX_TP_SUM];
#else
P2VAR(PduR_UpTxTpMulPduRunTimeType, AUTOMATIC,PDUR_APPL_DATA) PduR_UpTxTpMulRoute = NULL_PTR;
#endif

/*gateway Tp routing path:all dest pdu(SF/MF,route to Tp or Up)*/
#if(PDUR_DEST_GATEWAY_TP_PDU_SUM > 0)
PduR_GateWayDestTpRunTimeType PduR_GateWayDestTpRTSate[PDUR_DEST_GATEWAY_TP_PDU_SUM];
#else
P2VAR(PduR_GateWayDestTpRunTimeType, AUTOMATIC, PDUR_APPL_DATA) PduR_GateWayDestTpRTSate = NULL_PTR;
#endif
/*******************************************************************************
**                      Global Function Definitions                           **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
/******************************************************************************/
/*
 * Brief               Initializes the PDU Router
 * ServiceId           0xf0
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      ConfigPtr: Pointer to Post build configuration data.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_Init(P2CONST(PduR_PBConfigType, AUTOMATIC, PDUR_CONST_PBCFG) ConfigPtr)
{
    #if (PDUR_ROUTING_PATH_GROUP_SUM > 0)
	uint16 destPduNumber;
	uint16 cnt;
	uint16 destIndex;
    #endif
	uint16 index;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_UNINIT != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_INIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == ConfigPtr))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_INIT_ID, PDUR_E_INIT_FAILED);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_ConfigStd = ConfigPtr;
#if (0u < PDUR_GATEWAY_DIRECT_BUFFER_PDU_SUM)
		/*if source pdu gateway to other if dest pdus which configed PDUR_DIRECT */
		for(index = 0u;index < PDUR_GATEWAY_DIRECT_BUFFER_PDU_SUM;index++)
		{
			PduR_DestPduState[index] = PDUR_IDLE;
		}
#endif/* 0u < PDUR_GATEWAY_DIRECT_BUFFER_PDU_SUM */
		/* init all dest pdu state base on RoutingPathGroup parameter PduRIsEnabledAtInit��
		 * default state is enabled for dest pdus not included any path group */
		for(index = 0u;index < PDUR_DEST_PDU_SUM;index++)
		{
			PduRIsEnabled[index] = TRUE;
		}
#if (0u < PDUR_ROUTING_PATH_GROUP_SUM)
		for(index = 0u;index < PDUR_ROUTING_PATH_GROUP_SUM;index++)
		{
			if(FALSE == PduR_ConfigStd->PduRRoutingPathGroupRef[index].PduRIsEnabledAtInit)
			{
				destPduNumber = PduR_ConfigStd->PduRRoutingPathGroupRef[index].PduRDestPduRefNumber;
				for(cnt = 0u;cnt < destPduNumber;cnt++)
				{
					destIndex = PduR_ConfigStd->PduRRoutingPathGroupRef[index].PduRDestPduRef[cnt];
					PduRIsEnabled[destIndex] = FALSE;
				}
			}
		}
#endif/*(0u < PDUR_ROUTING_PATH_GROUP_SUM)*/
#if (0u<(PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM))
		/*up module 1:n(n>1)transmit,used to handle up TxConfirmation*/
		for(index = 0u;index < PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM;index++)
		{
			PduR_UpTxState[index] = FALSE;
		}
#endif
#if (0u < PDUR_SRC_UP_MULTICAST_TX_TP_SUM)
		/*up module 1:n(n>1) tp transmit*/
		for(index = 0u;index < PDUR_SRC_UP_MULTICAST_TX_TP_SUM;index++)
		{
			PduR_UpTxTpMulRoute[index].ActiveDestNumber = 0;
			PduR_UpTxTpMulRoute[index].DestCopyDataTimer = 0;
		}
#endif
#if (0u < PDUR_DEST_GATEWAY_TP_PDU_SUM)
		/*gateway tp routing path,Run Time State of all dest pdu(include up pdu and tp pdu)*/
		for(index = 0u;index < PDUR_DEST_GATEWAY_TP_PDU_SUM;index++)
		{
			PduR_GateWayDestTpRTSate[index].ActiveTpBufferId = 0xff;
			PduR_GateWayDestTpRTSate[index].CopyCompleteTpBufferNumber = 0;
			PduR_GateWayDestTpRTSate[index].NeedGateWayOnTheFly = FALSE;
			PduR_GateWayDestTpRTSate[index].TpTxConfirmation = TRUE;
			PduR_GateWayDestTpRTSate[index].TxBufferOffset = 0;
		}
#endif
		PduR_Status = PDUR_ONLINE;
	}

	return;
}
/******************************************************************************/
/*
 * Brief               Returns the unique identifier of the post-build time
 *                     configuration of the PDU Router
 * ServiceId           0xf2
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              PduR_PBConfigIdType: Identifier of the post-build time configuration
 */
/******************************************************************************/
FUNC(PduR_PBConfigIdType, PDUR_CODE)
PduR_GetConfigurationId(void)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_GETCONFIGURATIONID_ID, PDUR_E_INVALID_REQUEST);
	}
	#endif
	return PduR_ConfigStd->PduRConfigId;
}
/******************************************************************************/
/*
 * Brief               Enables a routing path table
 * ServiceId           0xf3
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the routing path group.
 *                     Routing path groups are defined in the PDU router configuration
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_EnableRouting(PduR_RoutingPathGroupIdType id)
{
	uint16 destPduId;
	uint16 destPduNumber;
	uint16 index;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_ENABLEROUTING_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)
#if (0u < PDUR_ROUTING_PATH_GROUP_SUM)
			&& (id >= PDUR_ROUTING_PATH_GROUP_SUM)
#endif
			)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_ENABLEROUTING_ID, PDUR_E_ROUTING_PATH_GROUP_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		destPduNumber =  PduR_ConfigStd->PduRRoutingPathGroupRef[id].PduRDestPduRefNumber;
		for(index = 0u;index < destPduNumber;index++)
		{
			destPduId = PduR_ConfigStd->PduRRoutingPathGroupRef[id].PduRDestPduRef[index];
		    PduRIsEnabled[destPduId] = TRUE;
		}
	}

	return;
}
/******************************************************************************/
/*
 * Brief               Disables a routing path table
 * ServiceId           0xf4
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the routing path group.
 *                     Routing path groups are defined in the PDU router configuration
 *                     initialize (true,false)
 *                     true: initialize single buffers to the default value
 *                     false: retain current value of single buffers
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_DisableRouting(PduR_RoutingPathGroupIdType id,boolean initialize)
{
    uint16 destPduNumber;
    uint16 index;
    uint16 cnt;
	uint16 destPduId;
	uint16 txBufferId;
	uint8 txBufferDepth;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DISABLEROUTING_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}

	if ((TRUE == detNoErr)
#if (0u < PDUR_ROUTING_PATH_GROUP_SUM)
			&& (id >= PDUR_ROUTING_PATH_GROUP_SUM)
#endif
			)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DISABLEROUTING_ID, PDUR_E_ROUTING_PATH_GROUP_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		destPduNumber =  PduR_ConfigStd->PduRRoutingPathGroupRef[id].PduRDestPduRefNumber;
		for(index = 0u;index < destPduNumber;index++)
		{
			destPduId = PduR_ConfigStd->PduRRoutingPathGroupRef[id].PduRDestPduRef[index];
			if(TRUE == PduRIsEnabled[destPduId])
			{
			    txBufferId = PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDestTxBufferRef;
			    if((TRUE == initialize) && (0xffff != txBufferId) && (PDUR_TRIGGERTRANSMIT == PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDestPduDataProvision))
			    {
			    	txBufferDepth = PduR_TxBuffer[txBufferId].PduRTxBufferDepth;
			    	if(1 == txBufferDepth)
			    	{
			    		if(NULL_PTR != PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDefaultValueRef)
			    		{
			    			PduR_TxBuffer[txBufferId].PduRTxBufferRef[0].SduLength = PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDefaultValueRef->DefaultValueLength;
			    			PduR_Memcpy
			    			(
			    				PduR_TxBuffer[txBufferId].PduRTxBufferRef[0].TxBufferData,
			    				&PduR_Default_value[PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDefaultValueRef->DefaultValueStart],
			    				PduR_ConfigStd->PduRDestPduRef[destPduId].PduRDefaultValueRef->DefaultValueLength
			    			);
			    		}
			    	}
			    	else/*txBufferDepth > 1*/
			    	{
			    		if(TRUE == PduR_TxBuffer[txBufferId].PduRTxBufferRef[0].used)
			    		{
							#if(STD_ON == PDUR_DEV_ERROR_DETECT)
							Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DISABLEROUTING_ID, PDUR_E_PDU_INSTANCES_LOST);
							#endif
			    		}
	                    for(cnt = 0u;cnt < txBufferDepth;cnt++)
	                    {
	                    	PduR_TxBuffer[txBufferId].PduRTxBufferRef[cnt].used = FALSE;
	                    }
			    	}
			    }
			    PduRIsEnabled[destPduId] = FALSE;
			}
		}
	}
	return;
}
#if(STD_ON == PDUR_COM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0x89
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_ComTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0x8a
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_COM_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_ComCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		#if( STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
		result = PduR_UpModeCancelTransmit(id);
#endif
	}
	return result;
}
#endif/*STD_ON == PDUR_COM_CANCEL_TRANSMIT*/
/******************************************************************************/
/*
 * Brief               Request to change a specific transport protocol parameter (e.g. block size).
 * ServiceId           0x8b
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identifiaction of the I-PDU which the parameter change shall affect.
 *                     parameter: The parameter that shall change.
 *                     value: The new value of the parameter
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: The parameter was changed successfully.
 *                     E_NOT_OK: The parameter change was rejected.
 */
/******************************************************************************/
#if(STD_ON == PDUR_COM_CHANGE_PARAMETER)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_ComChangeParameter(PduIdType id,TPParameterType parameter,uint16 value)/* a received I-PDU */
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCHANGEPARAMETER_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCHANGEPARAMETER_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeChangeParameter(id,parameter,value);
    }
	return result;
}
#endif/*STD_ON == PDUR_COM_CHANGE_PARAMETER*/
/******************************************************************************/
/*
 * Brief               Requests cancellation of an ongoing reception of an I-PDU in a lower layer transport protocol module.
 * ServiceId           0x8c
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK: Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_COM_CANCEL_RECEIVE)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_ComCancelReceive(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCANCELRECEIVE_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_COMCANCELRECEIVE_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeCancelReceive(id);
    }
	return result;
}
#endif/*STD_ON == PDUR_COM_CANCEL_RECEIVE*/
#endif/*STD_ON == PDUR_COM_SUPPORT*/
#if(STD_ON == PDUR_LDCOM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0x089
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_LdComTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0x08a
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_LDCOM_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_LdComCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
#if( STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
		result = PduR_UpModeCancelTransmit(id);
#endif
	}
	return result;
}
#endif/*STD_ON == PDUR_LDCOM_CANCEL_TRANSMIT*/
/******************************************************************************/
/*
 * Brief               Request to change a specific transport protocol parameter (e.g. block size).
 * ServiceId           0x08b
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identifiaction of the I-PDU which the parameter change shall affect.
 *                     parameter: The parameter that shall change.
 *                     value: The new value of the parameter
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: The parameter was changed successfully.
 *                     E_NOT_OK: The parameter change was rejected.
 */
/******************************************************************************/
#if(STD_ON == PDUR_LDCOM_CHANGE_PARAMETER)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_LdComChangeParameter(PduIdType id,TPParameterType parameter,uint16 value)/* a received I-PDU */
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCHANGEPARAMETER_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCHANGEPARAMETER_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeChangeParameter(id,parameter,value);
    }
	return result;
}
#endif/*STD_ON == PDUR_LDCOM_CHANGE_PARAMETER*/
/******************************************************************************/
/*
 * Brief               Requests cancellation of an ongoing reception of an I-PDU in a lower layer transport protocol module.
 * ServiceId           0x08c
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK: Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_LDCOM_CANCEL_RECEIVE)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_LdComCancelReceive(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCANCELRECEIVE_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LDCOMCANCELRECEIVE_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeCancelReceive(id);
    }
	return result;
}
#endif/*STD_ON == PDUR_LDCOM_CANCEL_RECEIVE*/
#endif/*STD_ON == PDUR_LDCOM_SUPPORT*/
#if(STD_ON == PDUR_DCM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0x99
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0x9a
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
#if( STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
		result = PduR_UpModeCancelTransmit(id);
#endif
	}
	return result;
}
#endif/*STD_ON == PDUR_DCM_CANCEL_TRANSMIT*/
/******************************************************************************/
/*
 * Brief               Request to change a specific transport protocol parameter (e.g. block size).
 * ServiceId           0x9b
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identifiaction of the I-PDU which the parameter change shall affect.
 *                     parameter: The parameter that shall change.
 *                     value: The new value of the parameter
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: The parameter was changed successfully.
 *                     E_NOT_OK: The parameter change was rejected.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CHANGE_PARAMETER)
FUNC(Std_ReturnType, PDUR_CODE)/*receive pdu id*/
PduR_DcmChangeParameter(PduIdType id,TPParameterType parameter,uint16 value)/* a received I-PDU */
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCHANGEPARAMETER_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCHANGEPARAMETER_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeChangeParameter(id,parameter,value);
    }
	return result;
}
#endif/*STD_ON == PDUR_DCM_CHANGE_PARAMETER*/
/******************************************************************************/
/*
 * Brief               Requests cancellation of an ongoing reception of an I-PDU in a lower layer transport protocol module.
 * ServiceId           0x9c
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK: Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CANCEL_RECEIVE)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmCancelReceive(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCANCELRECEIVE_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_DCMCANCELRECEIVE_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeCancelReceive(id);
    }
	return result;
}
#endif/*STD_ON == PDUR_DCM_CANCEL_RECEIVE*/
#endif/*STD_ON == PDUR_DCM_SUPPORT*/
#if(STD_ON == PDUR_J1939DCM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0xf9
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939DcmTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0xfa
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_J1939DCM_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939DcmCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
#if( STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
		result = PduR_UpModeCancelTransmit(id);
#endif
	}
	return result;
}
#endif/*STD_ON == PDUR_J1939DCM_CANCEL_TRANSMIT*/
/******************************************************************************/
/*
 * Brief               Request to change a specific transport protocol parameter (e.g. block size).
 * ServiceId           0xfb
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identifiaction of the I-PDU which the parameter change shall affect.
 *                     parameter: The parameter that shall change.
 *                     value: The new value of the parameter
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: The parameter was changed successfully.
 *                     E_NOT_OK: The parameter change was rejected.
 */
/******************************************************************************/
#if(STD_ON == PDUR_J1939DCM_CHANGE_PARAMETER)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939DcmChangeParameter(PduIdType id,TPParameterType parameter,uint16 value)/* a received I-PDU */
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCHANGEPARAMETER_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCHANGEPARAMETER_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
#if( STD_ON == PDUR_UPMODE_CHANGE_PARAMETER)
    	result = PduR_UpModeChangeParameter(id,parameter,value);
#endif
    }
	return result;
}
#endif/*STD_ON == PDUR_J1939DCM_CHANGE_PARAMETER*/
/******************************************************************************/
/*
 * Brief               Requests cancellation of an ongoing reception of an I-PDU in a lower layer transport protocol module.
 * ServiceId           0xfc
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK: Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_J1939DCM_CANCEL_RECEIVE)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939DcmCancelReceive(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCANCELRECEIVE_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
    {
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939DCMCANCELRECEIVE_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
    }
    if(TRUE == detNoErr)
    #endif
    {
    	result = PduR_UpModeCancelReceive(id);
    }
	return result;
}
#endif/*STD_ON == PDUR_J1939DCM_CANCEL_RECEIVE*/
#endif/*STD_ON == PDUR_J1939DCM_SUPPORT*/
#if(STD_ON == PDUR_J1939RM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0xe9
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939RmTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939RMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939RMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939RMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0xea
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_J1939RM_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_J1939RmCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939RMCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939RMCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
#if( STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
		result = PduR_UpModeCancelTransmit(id);
#endif
	}
	return result;
}
#endif/*STD_ON == PDUR_J1939RM_CANCEL_TRANSMIT*/
#endif/*STD_ON == PDUR_J1939RM_SUPPORT*/
#if(STD_ON == PDUR_CANIF_SUPPORT)
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x01
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_CanIfRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	uint8 pduDestSum;
    uint8 cnt;
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
        pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduDestSum;
        for(cnt = 0u;cnt < pduDestSum;cnt++)
        {
	        pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[cnt];
	        destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
	        destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
	        destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
	        if(TRUE == PduRIsEnabled[pduRDestPduId])
	        {
	            switch(destModule)
				{
                    #if(STD_ON == PDUR_COM_SUPPORT)
					case PDUR_COM:
						Com_RxIndication(destModulePduId, PduInfoPtr);
						break;
                    #endif
					#if(STD_ON == PDUR_LDCOM_SUPPORT)
					case PDUR_LDCOM:
						LdCom_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_SECOC_SUPPORT)
					case PDUR_SECOC:
						SecOC_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_IPDUM_SUPPORT)
					case PDUR_IPDUM:
						IpduM_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_J1939DCM_SUPPORT)
					case PDUR_J1939DCM:
						J1939Dcm_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_J1939RM_SUPPORT)
					case PDUR_J1939RM:
						J1939Rm_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					default:
						PduR_GateWayIfPdu(destModule,pduRDestPduId,PduInfoPtr);
						break;
				}
	        }
        }
	}
	return;
}
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x02
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_CANIF_TX_CONFIRMATION)
FUNC(void, PDUR_CODE)
PduR_CanIfTxConfirmation(PduIdType TxPduId)
{
    PduIdType destModulePduId;
    PduIdType srcUpPduId;
    PduInfoType pduInfo;
    uint16 bufferId;
    Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
	    	switch(srcPduModule)
			{
                #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							Com_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								Com_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
                #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							LdCom_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								LdCom_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							SecOC_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								SecOC_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_IPDUM_SUPPORT)
				case PDUR_IPDUM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							IpduM_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								IpduM_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							Dcm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								Dcm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							J1939Dcm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								J1939Dcm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939RM_SUPPORT)
				case PDUR_J1939RM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							J1939Rm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								J1939Rm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				default:/*If layer:Gateway pdu*/
					bufferId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestTxBufferRef;
					/*the pdu config buffer and it's PDUR_DIRECT mode*/
					if((0xffff != bufferId) && (PDUR_DIRECT == PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestPduDataProvision))
					{
						PduR_DestPduState[PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRGatewayDirectTxStateIndex] = PDUR_IDLE;
						if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
						{
							pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
							pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
							destModulePduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestModulePduIndex;
							result = CanIf_Transmit(destModulePduId,&pduInfo);
							if(E_OK == result)
							{
								PduR_DestPduState[PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRGatewayDirectTxStateIndex] = PDUR_BUSY;
							}
							else
							{
								/*transmit not ok(FIFO),report det error*/
								if(1 < PduR_TxBuffer[bufferId].PduRTxBufferDepth)
								{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFTXCONFIRMATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
								}
							}
							PduR_DeQueueBuffer(TxPduId);
						}
					}
					break;
		    }
	    }
	}
    return;
}
#endif/*STD_ON == PDUR_CANIF_TX_CONFIRMATION*/
/******************************************************************************/
/*
 * Brief               Within this API, the upper layer module (called module) shall check whether the
 *                     available data fits into the buffer size reported by PduInfoPtr->SduLength.
 *                     If it fits, it shall copy its data into the buffer provided by PduInfoPtr->SduDataPtr
 *                     and update the length of the actual copied data in PduInfoPtr->SduLength.
 *                     If not, it returns E_NOT_OK without changing PduInfoPtr.
 * ServiceId           0x03
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the SDU that is requested to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  PduInfoPtr: Contains a pointer to a buffer (SduDataPtr) to where the SDU
 *                     data shall be copied, and the available buffer size in SduLengh.
 *                     On return, the service will indicate the length of the copied SDU
 *                     data in SduLength.
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_CANIF_TRIGGER_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_CanIfTriggerTransmit(PduIdType TxPduId,P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    PduIdType srcUpPduId;
    uint8 srcModuleIndex;
    uint8 srcModule;
    uint16 bufferId;
    PduIdType destModulePduId;
    PduInfoType pduInfo;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFTRIGGERTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFTRIGGERTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		if(TRUE == PduRIsEnabled[TxPduId])
		{
			srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
			srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
			srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
		    switch(srcModule)
			{
	            #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
					    srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = Com_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
	            #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = LdCom_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = SecOC_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				#if(STD_ON == PDUR_IPDUM_SUPPORT)
				case PDUR_IPDUM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = IpduM_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				default:/*If layer:Gateway pdu*/
					bufferId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestTxBufferRef;
					if(PduInfoPtr->SduLength >= PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength)
					{
						PduInfoPtr->SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
		                PduR_Memcpy(PduInfoPtr->SduDataPtr,PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData,PduInfoPtr->SduLength);
		                if(1 < PduR_TxBuffer[bufferId].PduRTxBufferDepth)
		                {
		                	PduR_DeQueueBuffer(TxPduId);
		                	if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
							{
								destModulePduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestModulePduIndex;
								pduInfo.SduDataPtr = NULL_PTR;
								pduInfo.SduLength = 0;
								(void)CanIf_Transmit(destModulePduId,&pduInfo);
							}
		                }
		                result = E_OK;
					}
					break;
			}
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_CANIF_TRIGGER_TRANSMIT*/
#endif/*STD_ON == PDUR_CANIF_SUPPORT*/

#if(STD_ON == PDUR_LINIF_SUPPORT)
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x51
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_LinIfRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	uint8 pduDestSum;
    uint8 cnt;
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
        pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduDestSum;
        for(cnt = 0u;cnt < pduDestSum;cnt++)
        {
	        pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[cnt];
	        destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
	        destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
	        destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
	        if(TRUE == PduRIsEnabled[pduRDestPduId])
	        {
	            switch(destModule)
				{
                    #if(STD_ON == PDUR_COM_SUPPORT)
					case PDUR_COM:
						Com_RxIndication(destModulePduId, PduInfoPtr);
						break;
                    #endif
					#if(STD_ON == PDUR_LDCOM_SUPPORT)
					case PDUR_LDCOM:
						LdCom_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_SECOC_SUPPORT)
					case PDUR_SECOC:
						SecOC_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_IPDUM_SUPPORT)
					case PDUR_IPDUM:
						IpduM_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_J1939DCM_SUPPORT)
					case PDUR_J1939DCM:
						J1939Dcm_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					#if(STD_ON == PDUR_J1939RM_SUPPORT)
					case PDUR_J1939RM:
						J1939Rm_RxIndication(destModulePduId, PduInfoPtr);
						break;
					#endif
					default:
						PduR_GateWayIfPdu(destModule,pduRDestPduId,PduInfoPtr);
						break;
				}
	        }
        }
	}
	return;
}
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x52
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_LINIF_TX_CONFIRMATION)
FUNC(void, PDUR_CODE)
PduR_LinIfTxConfirmation(PduIdType TxPduId)
{
    PduIdType destModulePduId;
    PduIdType srcUpPduId;
    PduInfoType pduInfo;
    uint16 bufferId;
    Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
	    	switch(srcPduModule)
			{
                #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							Com_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								Com_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
                #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							LdCom_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								LdCom_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							SecOC_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								SecOC_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_IPDUM_SUPPORT)
				case PDUR_IPDUM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							IpduM_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								IpduM_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							Dcm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								Dcm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							J1939Dcm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								J1939Dcm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939RM_SUPPORT)
				case PDUR_J1939RM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						/*route 1:1 Pdu from up module to lo If module*/
						if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
						{
							J1939Rm_TxConfirmation(srcUpPduId);
						}
						/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
						else
						{
							if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
							{
								J1939Rm_TxConfirmation(srcUpPduId);
								PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
							}
						}
					}
					break;
				#endif
				default:/*If layer:Gateway pdu*/
					bufferId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestTxBufferRef;
					/*the pdu config buffer and it's PDUR_DIRECT mode*/
					if((0xffff != bufferId) && (PDUR_DIRECT == PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestPduDataProvision))
					{
						PduR_DestPduState[PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRGatewayDirectTxStateIndex] = PDUR_IDLE;
						if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
						{
							pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
							pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
							destModulePduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestModulePduIndex;
							result = LinIf_Transmit(destModulePduId,&pduInfo);
							if(E_OK == result)
							{
								PduR_DestPduState[PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRGatewayDirectTxStateIndex] = PDUR_BUSY;
							}
							else
							{
								/*transmit not ok(FIFO),report det error*/
								if(1 < PduR_TxBuffer[bufferId].PduRTxBufferDepth)
								{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFTXCONFIRMATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
								}
							}
							PduR_DeQueueBuffer(TxPduId);
						}
					}
					break;
		    }
	    }
	}
    return;
}
#endif/*STD_ON == PDUR_LINIF_TX_CONFIRMATION*/
/******************************************************************************/
/*
 * Brief               Within this API, the upper layer module (called module) shall check whether the
 *                     available data fits into the buffer size reported by PduInfoPtr->SduLength.
 *                     If it fits, it shall copy its data into the buffer provided by PduInfoPtr->SduDataPtr
 *                     and update the length of the actual copied data in PduInfoPtr->SduLength.
 *                     If not, it returns E_NOT_OK without changing PduInfoPtr.
 * ServiceId           0x53
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the SDU that is requested to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  PduInfoPtr: Contains a pointer to a buffer (SduDataPtr) to where the SDU
 *                     data shall be copied, and the available buffer size in SduLengh.
 *                     On return, the service will indicate the length of the copied SDU
 *                     data in SduLength.
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_LINIF_TRIGGER_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_LinIfTriggerTransmit(PduIdType TxPduId,P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    PduIdType srcUpPduId;
    uint8 srcModuleIndex;
    uint8 srcModule;
    uint16 bufferId;
    PduIdType destModulePduId;
    PduInfoType pduInfo;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFTRIGGERTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFTRIGGERTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		if(TRUE == PduRIsEnabled[TxPduId])
		{
			srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
			srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
			srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
		    switch(srcModule)
			{
	            #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
					    srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = Com_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
	            #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = LdCom_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = SecOC_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				#if(STD_ON == PDUR_IPDUM_SUPPORT)
				case PDUR_IPDUM:
					if(TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						result = IpduM_TriggerTransmit(srcUpPduId,PduInfoPtr);
					}
					break;
				#endif
				default:/*If layer:Gateway pdu*/
					bufferId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestTxBufferRef;
					if(PduInfoPtr->SduLength >= PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength)
					{
						PduInfoPtr->SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
						PduR_Memcpy(PduInfoPtr->SduDataPtr,PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData,PduInfoPtr->SduLength);
						if(1 < PduR_TxBuffer[bufferId].PduRTxBufferDepth)
						{
							PduR_DeQueueBuffer(TxPduId);
							if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
							{
								destModulePduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRDestModulePduIndex;
								pduInfo.SduDataPtr = NULL_PTR;
								pduInfo.SduLength = 0;
								(void)LinIf_Transmit(destModulePduId,&pduInfo);
							}
						}
						result = E_OK;
					}
					break;
			}
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_LINIF_TRIGGER_TRANSMIT*/
#endif/*STD_ON == PDUR_LINIF_SUPPORT*/

#if((STD_ON == PDUR_CANNM_SUPPORT) && (STD_ON == PDUR_COM_SUPPORT))
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x11
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_CanNmRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[0];
		destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
		if((TRUE == PduRIsEnabled[pduRDestPduId]) && (PDUR_COM == destModule))
		{
			Com_RxIndication(destModulePduId, PduInfoPtr);
		}
	}
	return;
}
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x12
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_CANNM_TX_CONFIRMATION)
FUNC(void, PDUR_CODE)
PduR_CanNmTxConfirmation(PduIdType TxPduId)
{
    PduIdType srcUpPduId;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
			if((PDUR_COM == srcPduModule) && (TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf))
			{
				srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
				/*route 1:1 Pdu from up module to lo If module*/
				if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
				{
					Com_TxConfirmation(srcUpPduId);
				}
				/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
				else
				{
					if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
					{
						Com_TxConfirmation(srcUpPduId);
						PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
					}
				}
			}
	    }
	}
    return;
}
#endif/*STD_ON == PDUR_CANNM_TX_CONFIRMATION*/
/******************************************************************************/
/*
 * Brief               Within this API, the upper layer module (called module) shall check whether the
 *                     available data fits into the buffer size reported by PduInfoPtr->SduLength.
 *                     If it fits, it shall copy its data into the buffer provided by PduInfoPtr->SduDataPtr
 *                     and update the length of the actual copied data in PduInfoPtr->SduLength.
 *                     If not, it returns E_NOT_OK without changing PduInfoPtr.
 * ServiceId           0x13
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the SDU that is requested to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  PduInfoPtr: Contains a pointer to a buffer (SduDataPtr) to where the SDU
 *                     data shall be copied, and the available buffer size in SduLengh.
 *                     On return, the service will indicate the length of the copied SDU
 *                     data in SduLength.
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_CANNM_TRIGGER_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_CanNmTriggerTransmit(PduIdType TxPduId,P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    PduIdType srcUpPduId;
    uint8 srcModuleIndex;
    uint8 srcModule;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMTRIGGERTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANNMTRIGGERTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		if(TRUE == PduRIsEnabled[TxPduId])
		{
			srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
			srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
			srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
			if((PDUR_COM == srcModule) && (TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit))
			{
			    srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
				result = Com_TriggerTransmit(srcUpPduId,PduInfoPtr);
			}
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_CANNM_TRIGGER_TRANSMIT*/
#endif/*(STD_ON == PDUR_CANNM_SUPPORT) && (STD_ON == PDUR_COM_SUPPORT)*/

#if((STD_ON == PDUR_OSEKNM_SUPPORT) && (STD_ON == PDUR_COM_SUPPORT))
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x81
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_OsekNmRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[0];
		destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
		if((TRUE == PduRIsEnabled[pduRDestPduId]) && (PDUR_COM == destModule))
		{
			Com_RxIndication(destModulePduId, PduInfoPtr);
		}
	}
	return;
}
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x82
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_OSEKNM_TX_CONFIRMATION)
FUNC(void, PDUR_CODE)
PduR_OsekNmTxConfirmation(PduIdType TxPduId)
{
    PduIdType srcUpPduId;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
			if((PDUR_COM == srcPduModule) && (TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf))
			{
				srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
				/*route 1:1 Pdu from up module to lo If module*/
				if(0xffff == PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex)
				{
					Com_TxConfirmation(srcUpPduId);
				}
				/*route 1:n Pdu from up module to lo If module,only the first Lo TxConfirm call Up TxConfirm*/
				else
				{
					if(FALSE == PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex])
					{
						Com_TxConfirmation(srcUpPduId);
						PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[srcPduId].UpTxconfirmStateIndex] = TRUE;
					}
				}
			}
	    }
	}
    return;
}
#endif/*STD_ON == PDUR_OSEKNM_TX_CONFIRMATION*/
/******************************************************************************/
/*
 * Brief               Within this API, the upper layer module (called module) shall check whether the
 *                     available data fits into the buffer size reported by PduInfoPtr->SduLength.
 *                     If it fits, it shall copy its data into the buffer provided by PduInfoPtr->SduDataPtr
 *                     and update the length of the actual copied data in PduInfoPtr->SduLength.
 *                     If not, it returns E_NOT_OK without changing PduInfoPtr.
 * ServiceId           0x83
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the SDU that is requested to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  PduInfoPtr: Contains a pointer to a buffer (SduDataPtr) to where the SDU
 *                     data shall be copied, and the available buffer size in SduLengh.
 *                     On return, the service will indicate the length of the copied SDU
 *                     data in SduLength.
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_OSEKNM_TRIGGER_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_OsekNmTriggerTransmit(PduIdType TxPduId,P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    PduIdType srcUpPduId;
    uint8 srcModuleIndex;
    uint8 srcModule;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMTRIGGERTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_OSEKNMTRIGGERTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		if(TRUE == PduRIsEnabled[TxPduId])
		{
			srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
			srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
			srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
			if((PDUR_COM == srcModule) && (TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit))
			{
			    srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
				result = Com_TriggerTransmit(srcUpPduId,PduInfoPtr);
			}
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_OSEKNM_TRIGGER_TRANSMIT*/
#endif/*(STD_ON == PDUR_OSEKNM_SUPPORT) && (STD_ON == PDUR_COM_SUPPORT)*/

#if(STD_ON == PDUR_SECOC_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0xC9
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_SecOCTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0xCa
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_SECOC_CANCEL_TRANSMIT)
FUNC(Std_ReturnType, PDUR_CODE)
PduR_SecOCCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCCANCELTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCCANCELTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeCancelTransmit(id);
	}
	return result;
}
#endif/*STD_ON == PDUR_SECOC_CANCEL_TRANSMIT*/
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x71
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_SecOCRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[0];
		destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
		if(TRUE == PduRIsEnabled[pduRDestPduId])
		{
			switch(destModule)
			{
				#if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					Com_RxIndication(destModulePduId, PduInfoPtr);
					break;
				#endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					LdCom_RxIndication(destModulePduId, PduInfoPtr);
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					J1939Dcm_RxIndication(destModulePduId, PduInfoPtr);
					break;
				#endif
				#if(STD_ON == PDUR_J1939RM_SUPPORT)
				case PDUR_J1939RM:
					J1939Rm_RxIndication(destModulePduId, PduInfoPtr);
					break;
				#endif
				default:
					break;
			}
		}
	}
	return;
}
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x72
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_SECOC_TX_CONFIRMATION)
FUNC(void, PDUR_CODE)
PduR_SecOCTxConfirmation(PduIdType TxPduId)
{
    PduIdType destModulePduId;
    PduIdType srcUpPduId;
    PduInfoType pduInfo;
    uint16 bufferId;
    Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_SECOCTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
	    	switch(srcPduModule)
			{
                #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						Com_TxConfirmation(srcUpPduId);
					}
					break;
                #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						LdCom_TxConfirmation(srcUpPduId);
					}
					break;
				#endif
				#if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						Dcm_TxConfirmation(srcUpPduId);
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						J1939Dcm_TxConfirmation(srcUpPduId);
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939RM_SUPPORT)
				case PDUR_J1939RM:
					if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
					{
						srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
						J1939Rm_TxConfirmation(srcUpPduId);
					}
					break;
				#endif
				default:
					break;
		    }
	    }
	}
    return;
}
#endif/*STD_ON == PDUR_SECOC_TX_CONFIRMATION*/
#endif/*STD_ON == PDUR_SECOC_SUPPORT*/

#if(STD_ON == PDUR_IPDUM_SUPPORT)
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0xa9
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
FUNC(Std_ReturnType, PDUR_CODE)
PduR_IpduMTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == info))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTRANSMIT_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr)&&(id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		result = PduR_UpModeTransmit(id,info);
	}
	return result;
}
/******************************************************************************/
/*
 * Brief               Within this API, the upper layer module (called module) shall check whether the
 *                     available data fits into the buffer size reported by PduInfoPtr->SduLength.
 *                     If it fits, it shall copy its data into the buffer provided by PduInfoPtr->SduDataPtr
 *                     and update the length of the actual copied data in PduInfoPtr->SduLength.
 *                     If not, it returns E_NOT_OK without changing PduInfoPtr.
 * ServiceId           0x23
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the SDU that is requested to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  PduInfoPtr: Contains a pointer to a buffer (SduDataPtr) to where the SDU
 *                     data shall be copied, and the available buffer size in SduLengh.
 *                     On return, the service will indicate the length of the copied SDU
 *                     data in SduLength.
 * Return              None
 */
/******************************************************************************/
#if((STD_ON == PDUR_IPDUM_TRIGGER_TRANSMIT) && (STD_ON == PDUR_COM_SUPPORT))
FUNC(Std_ReturnType, PDUR_CODE)
PduR_IpduMTriggerTransmit(PduIdType TxPduId,P2VAR(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
	Std_ReturnType result = E_NOT_OK;
    PduIdType srcPduId;
    PduIdType srcUpPduId;
    uint8 srcModuleIndex;
    uint8 srcModule;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTRIGGERTRANSMIT_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTRIGGERTRANSMIT_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		if(TRUE == PduRIsEnabled[TxPduId])
		{
			srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
			srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
			srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
			if((PDUR_COM == srcModule) && (TRUE == PduR_BswModuleConfigData[srcModuleIndex].PduRTriggertransmit))
			{
			    srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
				result = Com_TriggerTransmit(srcUpPduId,PduInfoPtr);
			}
		}
	}
	return result;
}
#endif/*(STD_ON == PDUR_IPDUM_TRIGGER_TRANSMIT) && (STD_ON == PDUR_COM_SUPPORT)*/
/******************************************************************************/
/*
 * Brief               Indication of a received I-PDU from a lower layer communication interface module.
 * ServiceId           0x21
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      RxPduId: ID of the received I-PDU.
 *                     PduInfoPtr: Contains the length (SduLength) of the received I-PDU and a pointer to a buffer (SduDataPtr) containing the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if(STD_ON == PDUR_COM_SUPPORT)
FUNC(void, PDUR_CODE)
PduR_IpduMRxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) PduInfoPtr)
{
    PduIdType pduRDestPduId;
    PduIdType destModulePduId;
    uint8 destModuleIndex;
    uint8 destModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
    if ((TRUE == detNoErr)&&((NULL_PTR == PduInfoPtr)||(NULL_PTR == PduInfoPtr->SduDataPtr)))
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMRXINDICATION_ID, PDUR_E_PARAM_POINTER);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (RxPduId >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[RxPduId].PduRDestPduIdRef[0];
		destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
		if((TRUE == PduRIsEnabled[pduRDestPduId]) && (PDUR_COM == destModule))
		{
			Com_RxIndication(destModulePduId, PduInfoPtr);
		}
	}
	return;
}
#endif/*STD_ON == PDUR_COM_SUPPORT*/
/******************************************************************************/
/*
 * Brief               The lower layer communication interface module confirms the transmission of an IPDU.
 * ServiceId           0x22
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different PduIds. Non reentrant for the same PduId.
 * Param-Name[in]      TxPduId: ID of the I-PDU that has been transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if((STD_ON == PDUR_IPDUM_TX_CONFIRMATION) && (STD_ON == PDUR_COM_SUPPORT))
FUNC(void, PDUR_CODE)
PduR_IpduMTxConfirmation(PduIdType TxPduId)
{
    PduIdType srcUpPduId;
    PduIdType srcPduId;
    uint8 srcPduModuleIndex;
    uint8 srcPduModule;
    #if(STD_ON == PDUR_DEV_ERROR_DETECT)
    boolean detNoErr = TRUE;
    if(PDUR_ONLINE != PduR_Status)
    {
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
        detNoErr = FALSE;
    }
	if ((TRUE == detNoErr) && (TxPduId >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_IPDUMTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
    if(TRUE == detNoErr)
    #endif
	{
	    if(TRUE == PduRIsEnabled[TxPduId])
	    {
	    	srcPduId = PduR_ConfigStd->PduRDestPduRef[TxPduId].PduRSrcPduRef;
	    	srcPduModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPduId].BswModuleIndex;
	    	srcPduModule = PduR_BswModuleConfigData[srcPduModuleIndex].PduRBswModuleRef;
	    	if(PDUR_COM == srcPduModule)
	    	{
				if(TRUE == PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRSrcPduUpTxConf)
				{
					srcUpPduId = PduR_ConfigStd->PduRSrcPduRef[srcPduId].PduRDestModulePduIndex;
					Com_TxConfirmation(srcUpPduId);
				}
	    	}
	    }
	}
    return;
}
#endif/*(STD_ON == PDUR_IPDUM_TX_CONFIRMATION) && (STD_ON == PDUR_COM_SUPPORT)*/
#endif/*STD_ON == PDUR_IPDUM_SUPPORT*/

#if(STD_ON == PDUR_CANTP_SUPPORT)
/******************************************************************************/
/*
 * Brief               This function is called at the start of receiving an N-SDU. The N-SDU might be
 *                     fragmented into multiple N-PDUs (FF with one or more following CFs) or might
 *                     consist of a single N-PDU (SF).
 * ServiceId           0x06
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Pointer to a PduInfoType structure containing the payload
 *                     data (without protocol information) and payload length of the
 *                     first frame or single frame of a transport protocol I-PDU
 *                     reception. Depending on the global parameter
 *                     MetaDataLength, additional bytes containing MetaData (e.g.
 *                     the CAN ID) are appended after the payload data, increasing
 *                     the length accordingly. If neither first/single frame data nor
 *                     MetaData are available, this parameter is set to NULL_PTR.
 *                     TpSduLength: Total length of the N-SDU to be received.
 * Param-Name[out]     bufferSizePtr: Available receive buffer in the receiving module. This
 *                     parameter will be used to compute the Block Size (BS) in the transport protocol module.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK,BUFREQ_E_OVFL)
 *                     BUFREQ_OK: Connection has been accepted. bufferSizePtr
 *                     indicates the available receive buffer; reception is continued.
 *                     If no buffer of the requested size is available, a receive buffer
 *                     size of 0 shall be indicated by bufferSizePtr.
 *                     BUFREQ_E_NOT_OK: Connection has been rejected;
 *                     reception is aborted. bufferSizePtr remains unchanged.
 *                     BUFREQ_E_OVFL: No buffer of the required length can be
 *                     provided; reception is aborted. bufferSizePtr remains unchanged.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPSTARTOFRECEPTION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPSTARTOFRECEPTION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == bufferSizePtr))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPSTARTOFRECEPTION_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpStartOfReception(id,info,TpSduLength,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called to provide the received data of an I-PDU segment (N-PDU) to the upper layer.
 *                     Each call to this function provides the next part of the I-PDU data.
 *                     The size of the remaining data is written to the position indicated by bufferSizePtr.
 * ServiceId           0x04
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     info: Provides the source buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     An SduLength of 0 can be used to query the current amount of available buffer in the upper
 *                     layer module. In this case, the SduDataPtr may be a NULL_PTR.
 * Param-Name[out]     bufferSizePtr: Available receive buffer after data has been copied.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data copied successfully
 *                     BUFREQ_E_NOT_OK: Data was not copied because an error occurred.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPCOPYRXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPCOPYRXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && ((NULL_PTR == info) || (NULL_PTR == bufferSizePtr)))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPCOPYRXDATA_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyRxData(id,info,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               Called after an I-PDU has been received via the TP API, the result indicates
 *                     whether the transmission was successful or not.
 * ServiceId           0x05
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     result: Result of the reception.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_CanTpRxIndication
(
	PduIdType id,
	Std_ReturnType result
)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpRxIndication(id,result);
	}
	return;
}
/******************************************************************************/
/*
 * Brief               This function is called to acquire the transmit data of an I-PDU segment (N-PDU).
 *                     Each call to this function provides the next part of the I-PDU data unless retry-
 *                     >TpDataState is TP_DATARETRY. In this case the function restarts to copy the
 *                     data beginning at the offset from the current position indicated by retry-
 *                     >TxTpDataCnt. The size of the remaining data is written to the position indicated
 *                     by availableDataPtr.
 * ServiceId           0x07
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     info: Provides the destination buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     If not enough transmit data is available, no data is copied by the upper layer module and
 *                     BUFREQ_E_BUSY is returned.The lower layer module may retry the call.An SduLength of 0 can be used to
 *                     indicate state changes in the retry parameter or to query the current amount of available data in the
 *                     upper layer module. In this case, the SduDataPtr may be a NULL_PTR.
 *                     retry: This parameter is used to acknowledge transmitted data or to retransmit data after transmission problems.
 *                     If the retry parameter is a NULL_PTR, it indicates that the transmit data can be removed from the buffer immediately
 *                     after it has been copied. Otherwise, the retry parameter must point to a valid RetryInfoType element.
 *                     If TpDataState indicates TP_CONFPENDING, the previously copied data must remain in the TP buffer to be available for
 *                     error recovery.TP_DATACONF indicates that all data that has been copied before this call is confirmed and can be
 *                     removed from the TP buffer. Data copied by this API call is excluded and will be confirmed later.TP_DATARETRY indicates
 *                     that this API call shall copy previously copied data in order to recover from an error. In this case TxTpDataCnt specifies
 *                     the offset in bytes from the current data copy position.
 * Param-Name[out]     availableDataPtr: Indicates the remaining number of bytes that are available in the upper layer module's Tx buffer.
 *                     availableDataPtr can be used by TP modules that support dynamic payload lengths (e.g. FrIsoTp) to determine the size
 *                     of the following CFs.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType (BUFREQ_OK,BUFREQ_E_BUSY,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data has been copied to the transmit buffer completely as requested.
 *                     BUFREQ_E_BUSY: Request could not be fulfilled, because the required amount of Tx data is not available. The lower layer
 *                     module may retry this call later on. No data has been copied.
 *                     BUFREQ_E_NOT_OK: Data has not been copied. Request failed.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPCOPYTXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPCOPYTXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyTxData(id,info,retry,availableDataPtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called after the I-PDU has been transmitted on its network, the
 *                     result indicates whether the transmission was successful or not.
 * ServiceId           0x08
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     result: Result of the transmission of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_CanTpTxConfirmation(PduIdType id,Std_ReturnType result)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANTPTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpTxConfirmation(id,result);
	}
	return;
}
#endif/*STD_ON == PDUR_CANTP_SUPPORT*/

#if(STD_ON == PDUR_LINTP_SUPPORT)
/******************************************************************************/
/*
 * Brief               This function is called at the start of receiving an N-SDU. The N-SDU might be
 *                     fragmented into multiple N-PDUs (FF with one or more following CFs) or might
 *                     consist of a single N-PDU (SF).
 * ServiceId           0x56
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Pointer to a PduInfoType structure containing the payload
 *                     data (without protocol information) and payload length of the
 *                     first frame or single frame of a transport protocol I-PDU
 *                     reception. Depending on the global parameter
 *                     MetaDataLength, additional bytes containing MetaData (e.g.
 *                     the CAN ID) are appended after the payload data, increasing
 *                     the length accordingly. If neither first/single frame data nor
 *                     MetaData are available, this parameter is set to NULL_PTR.
 *                     TpSduLength: Total length of the N-SDU to be received.
 * Param-Name[out]     bufferSizePtr: Available receive buffer in the receiving module. This
 *                     parameter will be used to compute the Block Size (BS) in the transport protocol module.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK,BUFREQ_E_OVFL)
 *                     BUFREQ_OK: Connection has been accepted. bufferSizePtr
 *                     indicates the available receive buffer; reception is continued.
 *                     If no buffer of the requested size is available, a receive buffer
 *                     size of 0 shall be indicated by bufferSizePtr.
 *                     BUFREQ_E_NOT_OK: Connection has been rejected;
 *                     reception is aborted. bufferSizePtr remains unchanged.
 *                     BUFREQ_E_OVFL: No buffer of the required length can be
 *                     provided; reception is aborted. bufferSizePtr remains unchanged.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LinTpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPSTARTOFRECEPTION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPSTARTOFRECEPTION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == bufferSizePtr))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPSTARTOFRECEPTION_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpStartOfReception(id,info,TpSduLength,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called to provide the received data of an I-PDU segment (N-PDU) to the upper layer.
 *                     Each call to this function provides the next part of the I-PDU data.
 *                     The size of the remaining data is written to the position indicated by bufferSizePtr.
 * ServiceId           0x54
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     info: Provides the source buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     An SduLength of 0 can be used to query the current amount of available buffer in the upper
 *                     layer module. In this case, the SduDataPtr may be a NULL_PTR.
 * Param-Name[out]     bufferSizePtr: Available receive buffer after data has been copied.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data copied successfully
 *                     BUFREQ_E_NOT_OK: Data was not copied because an error occurred.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LinTpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPCOPYRXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPCOPYRXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && ((NULL_PTR == info) || (NULL_PTR == bufferSizePtr)))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPCOPYRXDATA_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyRxData(id,info,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               Called after an I-PDU has been received via the TP API, the result indicates
 *                     whether the transmission was successful or not.
 * ServiceId           0x55
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     result: Result of the reception.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_LinTpRxIndication
(
	PduIdType id,
	Std_ReturnType result
)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpRxIndication(id,result);
	}
	return;
}
/******************************************************************************/
/*
 * Brief               This function is called to acquire the transmit data of an I-PDU segment (N-PDU).
 *                     Each call to this function provides the next part of the I-PDU data unless retry-
 *                     >TpDataState is TP_DATARETRY. In this case the function restarts to copy the
 *                     data beginning at the offset from the current position indicated by retry-
 *                     >TxTpDataCnt. The size of the remaining data is written to the position indicated
 *                     by availableDataPtr.
 * ServiceId           0x57
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     info: Provides the destination buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     If not enough transmit data is available, no data is copied by the upper layer module and
 *                     BUFREQ_E_BUSY is returned.The lower layer module may retry the call.An SduLength of 0 can be used to
 *                     indicate state changes in the retry parameter or to query the current amount of available data in the
 *                     upper layer module. In this case, the SduDataPtr may be a NULL_PTR.
 *                     retry: This parameter is used to acknowledge transmitted data or to retransmit data after transmission problems.
 *                     If the retry parameter is a NULL_PTR, it indicates that the transmit data can be removed from the buffer immediately
 *                     after it has been copied. Otherwise, the retry parameter must point to a valid RetryInfoType element.
 *                     If TpDataState indicates TP_CONFPENDING, the previously copied data must remain in the TP buffer to be available for
 *                     error recovery.TP_DATACONF indicates that all data that has been copied before this call is confirmed and can be
 *                     removed from the TP buffer. Data copied by this API call is excluded and will be confirmed later.TP_DATARETRY indicates
 *                     that this API call shall copy previously copied data in order to recover from an error. In this case TxTpDataCnt specifies
 *                     the offset in bytes from the current data copy position.
 * Param-Name[out]     availableDataPtr: Indicates the remaining number of bytes that are available in the upper layer module's Tx buffer.
 *                     availableDataPtr can be used by TP modules that support dynamic payload lengths (e.g. FrIsoTp) to determine the size
 *                     of the following CFs.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType (BUFREQ_OK,BUFREQ_E_BUSY,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data has been copied to the transmit buffer completely as requested.
 *                     BUFREQ_E_BUSY: Request could not be fulfilled, because the required amount of Tx data is not available. The lower layer
 *                     module may retry this call later on. No data has been copied.
 *                     BUFREQ_E_NOT_OK: Data has not been copied. Request failed.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LinTpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPCOPYTXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPCOPYTXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyTxData(id,info,retry,availableDataPtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called after the I-PDU has been transmitted on its network, the
 *                     result indicates whether the transmission was successful or not.
 * ServiceId           0x58
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     result: Result of the transmission of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_LinTpTxConfirmation(PduIdType id,Std_ReturnType result)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINTPTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpTxConfirmation(id,result);
	}
	return;
}
#endif/*STD_ON == PDUR_LINTP_SUPPORT*/
#if(STD_ON == PDUR_J1939TP_SUPPORT)
/******************************************************************************/
/*
 * Brief               This function is called at the start of receiving an N-SDU. The N-SDU might be
 *                     fragmented into multiple N-PDUs (FF with one or more following CFs) or might
 *                     consist of a single N-PDU (SF).
 * ServiceId           0x16
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Pointer to a PduInfoType structure containing the payload
 *                     data (without protocol information) and payload length of the
 *                     first frame or single frame of a transport protocol I-PDU
 *                     reception. Depending on the global parameter
 *                     MetaDataLength, additional bytes containing MetaData (e.g.
 *                     the CAN ID) are appended after the payload data, increasing
 *                     the length accordingly. If neither first/single frame data nor
 *                     MetaData are available, this parameter is set to NULL_PTR.
 *                     TpSduLength: Total length of the N-SDU to be received.
 * Param-Name[out]     bufferSizePtr: Available receive buffer in the receiving module. This
 *                     parameter will be used to compute the Block Size (BS) in the transport protocol module.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK,BUFREQ_E_OVFL)
 *                     BUFREQ_OK: Connection has been accepted. bufferSizePtr
 *                     indicates the available receive buffer; reception is continued.
 *                     If no buffer of the requested size is available, a receive buffer
 *                     size of 0 shall be indicated by bufferSizePtr.
 *                     BUFREQ_E_NOT_OK: Connection has been rejected;
 *                     reception is aborted. bufferSizePtr remains unchanged.
 *                     BUFREQ_E_OVFL: No buffer of the required length can be
 *                     provided; reception is aborted. bufferSizePtr remains unchanged.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_J1939TpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
        Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPSTARTOFRECEPTION_ID, PDUR_E_INVALID_REQUEST);
	    detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
	    Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPSTARTOFRECEPTION_ID, PDUR_E_PDU_ID_INVALID);
	    detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (NULL_PTR == bufferSizePtr))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPSTARTOFRECEPTION_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpStartOfReception(id,info,TpSduLength,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called to provide the received data of an I-PDU segment (N-PDU) to the upper layer.
 *                     Each call to this function provides the next part of the I-PDU data.
 *                     The size of the remaining data is written to the position indicated by bufferSizePtr.
 * ServiceId           0x14
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     info: Provides the source buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     An SduLength of 0 can be used to query the current amount of available buffer in the upper
 *                     layer module. In this case, the SduDataPtr may be a NULL_PTR.
 * Param-Name[out]     bufferSizePtr: Available receive buffer after data has been copied.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data copied successfully
 *                     BUFREQ_E_NOT_OK: Data was not copied because an error occurred.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_J1939TpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPCOPYRXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPCOPYRXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && ((NULL_PTR == info) || (NULL_PTR == bufferSizePtr)))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPSTARTOFRECEPTION_ID, PDUR_E_PARAM_POINTER);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyRxData(id,info,bufferSizePtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               Called after an I-PDU has been received via the TP API, the result indicates
 *                     whether the transmission was successful or not.
 * ServiceId           0x15
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     result: Result of the reception.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_J1939TpRxIndication
(
	PduIdType id,
	Std_ReturnType result
)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPRXINDICATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_SRC_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPRXINDICATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpRxIndication(id,result);
	}
	return;
}
/******************************************************************************/
/*
 * Brief               This function is called to acquire the transmit data of an I-PDU segment (N-PDU).
 *                     Each call to this function provides the next part of the I-PDU data unless retry-
 *                     >TpDataState is TP_DATARETRY. In this case the function restarts to copy the
 *                     data beginning at the offset from the current position indicated by retry-
 *                     >TxTpDataCnt. The size of the remaining data is written to the position indicated
 *                     by availableDataPtr.
 * ServiceId           0x17
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     info: Provides the destination buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     If not enough transmit data is available, no data is copied by the upper layer module and
 *                     BUFREQ_E_BUSY is returned.The lower layer module may retry the call.An SduLength of 0 can be used to
 *                     indicate state changes in the retry parameter or to query the current amount of available data in the
 *                     upper layer module. In this case, the SduDataPtr may be a NULL_PTR.
 *                     retry: This parameter is used to acknowledge transmitted data or to retransmit data after transmission problems.
 *                     If the retry parameter is a NULL_PTR, it indicates that the transmit data can be removed from the buffer immediately
 *                     after it has been copied. Otherwise, the retry parameter must point to a valid RetryInfoType element.
 *                     If TpDataState indicates TP_CONFPENDING, the previously copied data must remain in the TP buffer to be available for
 *                     error recovery.TP_DATACONF indicates that all data that has been copied before this call is confirmed and can be
 *                     removed from the TP buffer. Data copied by this API call is excluded and will be confirmed later.TP_DATARETRY indicates
 *                     that this API call shall copy previously copied data in order to recover from an error. In this case TxTpDataCnt specifies
 *                     the offset in bytes from the current data copy position.
 * Param-Name[out]     availableDataPtr: Indicates the remaining number of bytes that are available in the upper layer module's Tx buffer.
 *                     availableDataPtr can be used by TP modules that support dynamic payload lengths (e.g. FrIsoTp) to determine the size
 *                     of the following CFs.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType (BUFREQ_OK,BUFREQ_E_BUSY,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data has been copied to the transmit buffer completely as requested.
 *                     BUFREQ_E_BUSY: Request could not be fulfilled, because the required amount of Tx data is not available. The lower layer
 *                     module may retry this call later on. No data has been copied.
 *                     BUFREQ_E_NOT_OK: Data has not been copied. Request failed.
 */
/******************************************************************************/
FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_J1939TpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPCOPYTXDATA_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPCOPYTXDATA_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		bufQeqReturn = PduR_LoTpCopyTxData(id,info,retry,availableDataPtr);
	}
	return bufQeqReturn;
}
/******************************************************************************/
/*
 * Brief               This function is called after the I-PDU has been transmitted on its network, the
 *                     result indicates whether the transmission was successful or not.
 * ServiceId           0x18
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     result: Result of the transmission of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
FUNC(void, PDUR_CODE)
PduR_J1939TpTxConfirmation(PduIdType id,Std_ReturnType result)
{
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
	boolean detNoErr = TRUE;
	if(PDUR_ONLINE != PduR_Status)
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPTXCONFIRMATION_ID, PDUR_E_INVALID_REQUEST);
		detNoErr = FALSE;
	}
	if ((TRUE == detNoErr) && (id >= PDUR_DEST_PDU_SUM))
	{
		Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_J1939TPTXCONFIRMATION_ID, PDUR_E_PDU_ID_INVALID);
		detNoErr = FALSE;
	}
	if(TRUE == detNoErr)
	#endif
	{
		PduR_LoTpTxConfirmation(id,result);
	}
	return;
}
#endif/*STD_ON == PDUR_J1939TP_SUPPORT*/
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"
/*******************************************************************************
**                      Private Function Definitions                          **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
#if(STD_ON == PDUR_UPMODE_SUPPORT)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)
{
	Std_ReturnType result = E_NOT_OK;
	Std_ReturnType resultLo = E_NOT_OK;
	PduIdType pduRDestPduId;
	PduIdType loModulePduId;
	uint8 loModuleIndex;
	uint8 loModule;
	uint8 pduDestSum;
	uint8 cnt;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduDestSum;
	/*route one up module pdu to tp modules (pdus),SF support 1:n,MF just support 1:1*/
	if(TRUE == PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].TpRoute)
	{
		if(1 == pduDestSum)
		{
			pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[0];
			loModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
			loModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
			loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
			if(TRUE == PduRIsEnabled[pduRDestPduId])
			{
				switch(loModule)
				{
					#if(STD_ON == PDUR_CANTP_SUPPORT)
					case PDUR_CANTP:
						result = CanTp_Transmit(loModulePduId,info);
						break;
					#endif/*STD_ON == PDUR_CANTP_SUPPORT*/
					#if(STD_ON == PDUR_J1939TP_SUPPORT)
					case PDUR_J1939TP:
						result = J1939Tp_Transmit(loModulePduId,info);
						break;
					#endif/*STD_ON == PDUR_J1939TP_SUPPORT*/
					#if(STD_ON == PDUR_LINTP_SUPPORT)
					case PDUR_LINTP:
						result = LinTp_Transmit(loModulePduId,info);
						break;
					#endif/*STD_ON == PDUR_LINTP_SUPPORT*/
					default:
						break;
				}
			}
		}
		/*the SF route 1:n*/
		else
		{
			PduR_UpTxTpMulRoute[PduR_ConfigStd->PduRSrcPduRef[id].TxTpMulticastIndex].DestCopyDataTimer = 0;
			PduR_UpTxTpMulRoute[PduR_ConfigStd->PduRSrcPduRef[id].TxTpMulticastIndex].ActiveDestNumber = 0;
			for(cnt = 0u;cnt < pduDestSum;cnt++)
			{
				pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[cnt];
				loModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
				loModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
				loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					switch(loModule)
					{
						#if(STD_ON == PDUR_CANTP_SUPPORT)
						case PDUR_CANTP:
							resultLo = CanTp_Transmit(loModulePduId,info);
							if(E_OK == resultLo)
							{
								result = E_OK;
								PduR_UpTxTpMulRoute[PduR_ConfigStd->PduRSrcPduRef[id].TxTpMulticastIndex].ActiveDestNumber +=1;
							}
							break;
						#endif/*STD_ON == PDUR_CANTP_SUPPORT*/
						#if(STD_ON == PDUR_J1939TP_SUPPORT)
						case PDUR_J1939TP:
							resultLo = J1939Tp_Transmit(loModulePduId,info);
							if(E_OK == resultLo)
							{
								result = E_OK;
								PduR_UpTxTpMulRoute[PduR_ConfigStd->PduRSrcPduRef[id].TxTpMulticastIndex].ActiveDestNumber +=1;
							}
							break;
						#endif/*STD_ON == PDUR_J1939TP_SUPPORT*/
						#if(STD_ON == PDUR_LINTP_SUPPORT)
						case PDUR_LINTP:
							resultLo = LinTp_Transmit(loModulePduId,info);
							if(E_OK == resultLo)
							{
								result = E_OK;
								PduR_UpTxTpMulRoute[PduR_ConfigStd->PduRSrcPduRef[id].TxTpMulticastIndex].ActiveDestNumber +=1;
							}
							break;
						#endif/*STD_ON == PDUR_LINTP_SUPPORT*/
						default:
							break;
					}
				}
			}
			if(E_OK == result)
			{
				/*define to up TxConfirmation with E_OK or E_NOT_OK*/
				PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[id].UpTxconfirmStateIndex] = FALSE;
			}
		}
	}
	/*route one up module pdu to if modules (pdus)*/
	else
	{
		for(cnt = 0u;cnt < pduDestSum;cnt++)
		{
			pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[cnt];
			loModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
			loModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
			loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
			if(TRUE == PduRIsEnabled[pduRDestPduId])
			{
				switch(loModule)
				{
					#if(STD_ON == PDUR_CANIF_SUPPORT)
					case PDUR_CANIF:
						resultLo = CanIf_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_CANIF_SUPPORT*/
					#if(STD_ON == PDUR_CANNM_SUPPORT)
					case PDUR_CANNM:
						resultLo = CanNm_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_CANNM_SUPPORT*/
					#if(STD_ON == PDUR_OSEKNM_SUPPORT)
					case PDUR_OSEKNM:
						resultLo = OsekNm_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_OSEKNM_SUPPORT*/
					#if(STD_ON == PDUR_SECOC_SUPPORT)
					case PDUR_SECOC:
						resultLo = SecOC_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_SECOC_SUPPORT*/
					#if(STD_ON == PDUR_IPDUM_SUPPORT)
					case PDUR_IPDUM:
						resultLo = IpduM_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_IPDUM_SUPPORT*/
					#if(STD_ON == PDUR_LINIF_SUPPORT)
					case PDUR_LINIF:
						resultLo = LinIf_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_LINIF_SUPPORT*/
					#if(STD_ON == PDUR_FRIF_SUPPORT)
					case PDUR_FRIF:
						resultLo = FrIf_Transmit(loModulePduId,info);
						if(E_OK == resultLo)
						{
							result = E_OK;
						}
						break;
					#endif/*STD_ON == PDUR_FRIF_SUPPORT*/
					default:
						break;
				}
			}
		}
		if((1 < pduDestSum) && (E_OK == result))
		{
			if(TRUE == PduR_ConfigStd->PduRSrcPduRef[id].PduRSrcPduUpTxConf)
			{
				/*route 1:n,used to handle just the first dest TxConfirm will call up TxConfirm*/
				PduR_UpTxState[PduR_ConfigStd->PduRSrcPduRef[id].UpTxconfirmStateIndex] = FALSE;
			}
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_UPMODE_SUPPORT*/

#if(STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeCancelTransmit(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	boolean resultOk = TRUE;
	PduIdType pduRDestPduId;
	PduIdType loModulePduId;
	uint8 loModuleIndex;
	uint8 loModule;
	uint8 pduDestSum;
	uint8 cnt;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduDestSum;
	for(cnt = 0u;(cnt < pduDestSum)&&(TRUE == resultOk);cnt++)
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[cnt];
		loModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		loModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
		if((TRUE == PduRIsEnabled[pduRDestPduId])&&(TRUE == PduR_BswModuleConfigData[loModuleIndex].PduRCancelTransmit))
		{
			switch(loModule)
			{
				#if(STD_ON == PDUR_CANIF_SUPPORT)
				case PDUR_CANIF:
					if(E_NOT_OK == CanIf_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_CANTP_SUPPORT)
				case PDUR_CANTP:
					if(E_NOT_OK == CanTp_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_J1939TP_SUPPORT)
				case PDUR_J1939TP:
					if(E_NOT_OK == J1939Tp_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					if(E_NOT_OK == SecOC_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_LINIF_SUPPORT)
				case PDUR_LINIF:
					if(E_NOT_OK == LinIf_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_LINTP_SUPPORT)
				case PDUR_LINTP:
					if(E_NOT_OK == LinTp_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				#if(STD_ON == PDUR_FRIF_SUPPORT)
				case PDUR_FRIF:
					if(E_NOT_OK == FrIf_CancelTransmit(loModulePduId))
					{
						resultOk = FALSE;
					}
					break;
				#endif
				default:
					break;
			}
		}
		else
		{
			resultOk = FALSE;
		}
	}
	if(TRUE == resultOk)
	{
		result = E_OK;
	}
	return result;
}
#endif/*STD_ON == PDUR_UPMODE_CANCEL_TRANSMIT*/

#if(STD_ON == PDUR_UPMODE_CHANGE_PARAMETER)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeChangeParameter(PduIdType id,TPParameterType parameter,uint16 value)
{
	Std_ReturnType result = E_NOT_OK;
	PduIdType pduRSourcePduId;
	PduIdType loModulePduId;
	uint8 loModuleIndex;
	uint8 loModule;
	pduRSourcePduId = PduR_ConfigStd->PduRDestPduRef[id].PduRSrcPduRef;
	loModulePduId = PduR_ConfigStd->PduRSrcPduRef[pduRSourcePduId].PduRDestModulePduIndex;
	loModuleIndex = PduR_ConfigStd->PduRSrcPduRef[pduRSourcePduId].BswModuleIndex;
	if((TRUE == PduRIsEnabled[id])&&(TRUE == PduR_BswModuleConfigData[loModuleIndex].PduRChangeParameterApi))
	{
		loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
		switch(loModule)
		{
			#if(STD_ON == PDUR_CANTP_SUPPORT)
			case PDUR_CANTP:
				result = CanTp_ChangeParameter(loModulePduId,parameter,value);
				break;
			#endif
			#if(STD_ON == PDUR_J1939TP_SUPPORT)
			case PDUR_J1939TP:
				result = J1939Tp_ChangeParameter(loModulePduId,parameter,value);
				break;
			#endif
			#if(STD_ON == PDUR_LINTP_SUPPORT)
			case PDUR_LINTP:
				result = LinTp_ChangeParameter(loModulePduId,parameter,value);
				break;
			#endif
			default:
				break;
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_UPMODE_CHANGE_PARAMETER*/

#if(STD_ON == PDUR_UPMODE_CANCEL_RECEIVE)
static FUNC(Std_ReturnType, PDUR_CODE)
PduR_UpModeCancelReceive(PduIdType id)
{
	Std_ReturnType result = E_NOT_OK;
	PduIdType pduRSourcePduId;
	PduIdType loModulePduId;
	uint8 loModuleIndex;
	uint8 loModule;
	pduRSourcePduId = PduR_ConfigStd->PduRDestPduRef[id].PduRSrcPduRef;
	loModulePduId = PduR_ConfigStd->PduRSrcPduRef[pduRSourcePduId].PduRDestModulePduIndex;
	loModuleIndex = PduR_ConfigStd->PduRSrcPduRef[pduRSourcePduId].BswModuleIndex;
	if((TRUE == PduRIsEnabled[id])&&(TRUE == PduR_BswModuleConfigData[loModuleIndex].PduRCancelReceive))
	{
		loModule = PduR_BswModuleConfigData[loModuleIndex].PduRBswModuleRef;
		switch(loModule)
		{
			#if(STD_ON == PDUR_CANTP_SUPPORT)
			case PDUR_CANTP:
				result = CanTp_CancelReceive(loModulePduId);
				break;
			#endif
			#if(STD_ON == PDUR_J1939TP_SUPPORT)
			case PDUR_J1939TP:
				result = J1939Tp_CancelReceive(loModulePduId);
				break;
			#endif
			#if(STD_ON == PDUR_LINTP_SUPPORT)
			case PDUR_LINTP:
				result = LinTp_CancelReceive(loModulePduId);
				break;
			#endif
			default:
				break;
		}
	}
	return result;
}
#endif/*STD_ON == PDUR_UPMODE_CANCEL_RECEIVE*/

#if(STD_ON == PDUR_TP_SUPPORT)
static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	uint8 pduDestSum;
	PduIdType pduRDestPduId;
	uint8 destModuleIndex;
	uint8 destModule;
	PduIdType upPduId;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduDestSum;
	/*route tp pdu 1:1*/
	if(1 == pduDestSum)
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[0];
		if(STD_ON == PduRIsEnabled[pduRDestPduId])
		{
			destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
			destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
			upPduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
			switch(destModule)
			{
                #if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					bufQeqReturn = Com_StartOfReception(upPduId,info,TpSduLength,bufferSizePtr);
					break;
                #endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					bufQeqReturn = LdCom_StartOfReception(upPduId,info,TpSduLength,bufferSizePtr);
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					bufQeqReturn = SecOC_StartOfReception(upPduId,info,TpSduLength,bufferSizePtr);
					break;
				#endif
                #if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					bufQeqReturn = Dcm_StartOfReception(upPduId,info,TpSduLength,bufferSizePtr);
					break;
                #endif
                #if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					bufQeqReturn = J1939Dcm_StartOfReception(upPduId,info,TpSduLength,bufferSizePtr);
					break;
                #endif
				default:/*Tp layer:Gateway pdu*/
					bufQeqReturn = PduR_StartOfReceptionToOneTpHandle(id,pduRDestPduId,info,TpSduLength,bufferSizePtr);
					break;
			}
		}
		else
		{
			bufQeqReturn = BUFREQ_E_NOT_OK;
		}
	}
	/*Receive Tp pdu and route the pdu to more than one module*/
	else
	{
		/*at least one dest pdu is enabled*/
		if(TRUE == PduR_AtLeastOneDestIsEnabled(id))
		{
			bufQeqReturn = PduR_StartOfReceptionToMoreModuleHandle(id,pduDestSum,info,TpSduLength,bufferSizePtr);
		}
		else
		{
			bufQeqReturn = BUFREQ_E_NOT_OK;
		}
	}
	return bufQeqReturn;
}

static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	uint8 pduDestSum;
	PduIdType pduRDestPduId;
	uint8 destModuleIndex;
	uint8 destModule;
	PduIdType upPduId;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduDestSum;
	/*one dest pdu*/
	if(1 == pduDestSum)
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[0];
		if(TRUE == PduRIsEnabled[pduRDestPduId])
		{
			destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
			destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
			upPduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
			switch(destModule)
			{
				#if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					bufQeqReturn = Com_CopyRxData(upPduId,info,bufferSizePtr);
					break;
				#endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					bufQeqReturn = LdCom_CopyRxData(upPduId,info,bufferSizePtr);
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					bufQeqReturn = SecOC_CopyRxData(upPduId,info,bufferSizePtr);
					break;
				#endif
				#if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					bufQeqReturn = Dcm_CopyRxData(upPduId,info,bufferSizePtr);
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					bufQeqReturn = J1939Dcm_CopyRxData(upPduId,info,bufferSizePtr);
					break;
				#endif
				default:/*Tp layer:Gateway pdu*/
					if(TRUE == PduR_AtLeastOneDestIsNotTxConfirm(id))
					{
						bufQeqReturn = PduR_CopyRxDataToOneTpHandle(id,pduRDestPduId,info,bufferSizePtr);
					}
					else
					{
						bufQeqReturn = BUFREQ_E_NOT_OK;
					}
					break;
			}
		}
		else
		{
			bufQeqReturn = BUFREQ_E_NOT_OK;
		}
	}
	/*more than one dest pdu*/
	else
	{
		bufQeqReturn = PduR_CopyRxDataToMoreModuleHandle(id,pduDestSum,info,bufferSizePtr);
	}
	return bufQeqReturn;
}

static FUNC(void, PDUR_CODE)
PduR_LoTpRxIndication
(
	PduIdType id,
	Std_ReturnType result
)
{
	uint8 pduDestSum;
	PduIdType pduRDestPduId;
	uint8 destModuleIndex;
	uint8 destModule;
	PduIdType destModulePduId;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduDestSum;
	/*rx tp pdu to only one dest pdu*/
	if(1 == pduDestSum)
	{
		pduRDestPduId = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[id].PduRDestPduIdRef[0];
		destModuleIndex = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].BswModuleIndex;
		destModule = PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef;
		destModulePduId = PduR_ConfigStd->PduRDestPduRef[pduRDestPduId].PduRDestModulePduIndex;
		switch(destModule)
		{
			#if(STD_ON == PDUR_COM_SUPPORT)
			case PDUR_COM:
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					Com_TpRxIndication(destModulePduId,result);
				}
				else
				{
					Com_TpRxIndication(destModulePduId,E_NOT_OK);
				}
				break;
			#endif
			#if(STD_ON == PDUR_LDCOM_SUPPORT)
			case PDUR_LDCOM:
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					LdCom_TpRxIndication(destModulePduId,result);
				}
				else
				{
					LdCom_TpRxIndication(destModulePduId,E_NOT_OK);
				}
				break;
			#endif
			#if(STD_ON == PDUR_SECOC_SUPPORT)
			case PDUR_SECOC:
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					SecOC_TpRxIndication(destModulePduId,result);
				}
				else
				{
					SecOC_TpRxIndication(destModulePduId,E_NOT_OK);
				}
				break;
			#endif
			#if(STD_ON == PDUR_DCM_SUPPORT)
			case PDUR_DCM:
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					Dcm_TpRxIndication(destModulePduId,result);
				}
				else
				{
					Dcm_TpRxIndication(destModulePduId,E_NOT_OK);
				}
				break;
			#endif
			#if(STD_ON == PDUR_J1939DCM_SUPPORT)
			case PDUR_J1939DCM:
				if(TRUE == PduRIsEnabled[pduRDestPduId])
				{
					J1939Dcm_TpRxIndication(destModulePduId,result);
				}
				else
				{
					J1939Dcm_TpRxIndication(destModulePduId,E_NOT_OK);
				}
				break;
			#endif
			default:/*Tp layer:Gateway pdu*/
				if((E_OK == result) && (TRUE == PduRIsEnabled[pduRDestPduId]))
				{
					PduR_RxIndicationToOneTpHandle(id,pduRDestPduId);
				}
				else
				{
					PduR_ClearBufferAndStateOfGateWayTpPdu(id);
				}
				break;
		}
	}
	/*rx tp pdu to more than one dest pdu*/
	else
	{
		if(E_OK == result)
		{
			PduR_RxIndicationToMoreModuleHandle(id,pduDestSum);
		}
		else
		{
			PduR_ClearBufferAndStateOfGateWayTpPdu(id);
		}
	}
	return;
}

static FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_LoTpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
)
{
	BufReq_ReturnType bufQeqReturn = BUFREQ_E_NOT_OK;
	PduIdType srcPdu;
	uint8 srcModuleIndex;
	uint8 srcModule;
	PduIdType upPduId;
	uint8 pduDestSum;
	srcPdu = PduR_ConfigStd->PduRDestPduRef[id].PduRSrcPduRef;
	srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPdu].BswModuleIndex;
	srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[srcPdu].PduDestSum;
	upPduId = PduR_ConfigStd->PduRSrcPduRef[srcPdu].PduRDestModulePduIndex;
	/*one dest pdu route*/
	if(pduDestSum == 1)
	{
		if(TRUE == PduRIsEnabled[id])
		{
			switch(srcModule)
			{
				#if(STD_ON == PDUR_COM_SUPPORT)
				case PDUR_COM:
					bufQeqReturn = Com_CopyTxData(upPduId,info,retry,availableDataPtr);
					break;
				#endif
				#if(STD_ON == PDUR_LDCOM_SUPPORT)
				case PDUR_LDCOM:
					bufQeqReturn = LdCom_CopyTxData(upPduId,info,retry,availableDataPtr);
					break;
				#endif
				#if(STD_ON == PDUR_SECOC_SUPPORT)
				case PDUR_SECOC:
					bufQeqReturn = SecOC_CopyTxData(upPduId,info,retry,availableDataPtr);
					break;
				#endif
				#if(STD_ON == PDUR_DCM_SUPPORT)
				case PDUR_DCM:
					bufQeqReturn = Dcm_CopyTxData(upPduId,info,retry,availableDataPtr);
					break;
				#endif
				#if(STD_ON == PDUR_J1939DCM_SUPPORT)
				case PDUR_J1939DCM:
					bufQeqReturn = J1939Dcm_CopyTxData(upPduId,info,retry,availableDataPtr);
					break;
				#endif
				/*Tp layer:Gateway pdu*/
				default:
					bufQeqReturn = PduR_OneDestCopyTxDataFromTpHandle(srcPdu,id,info,retry,availableDataPtr);
				break;
			}
		}
		else
		{
			bufQeqReturn = BUFREQ_E_NOT_OK;
		}
	}
	/*more than one dest pdu route*/
	else
	{
		switch(srcModule)
		{
            #if(STD_ON == PDUR_COM_SUPPORT)
			case PDUR_COM:
            #endif
			#if(STD_ON == PDUR_LDCOM_SUPPORT)
			case PDUR_LDCOM:
			#endif
			#if(STD_ON == PDUR_SECOC_SUPPORT)
			case PDUR_SECOC:
			#endif
			#if(STD_ON == PDUR_DCM_SUPPORT)
			case PDUR_DCM:
            #endif
		    #if(STD_ON == PDUR_J1939DCM_SUPPORT)
			case PDUR_J1939DCM:
            #endif
				bufQeqReturn = PduR_UpMulticastTpCopyData(srcModule,srcPdu,id,info,availableDataPtr);
				break;
			/*Tp layer:Gateway pdu*/
			default:
				bufQeqReturn = PduR_MoreDestCopyTxDataFromTpHandle(srcPdu,id,info,retry,availableDataPtr);
				break;
		}
	}
	return bufQeqReturn;
}

static FUNC(void, PDUR_CODE)
PduR_LoTpTxConfirmation(PduIdType id,Std_ReturnType result)
{
	PduIdType srcPdu;
	uint8 srcModuleIndex;
	uint8 srcModule;
	uint8 pduDestSum;
	PduIdType gateWayTpRunTimeId;
	srcPdu = PduR_ConfigStd->PduRDestPduRef[id].PduRSrcPduRef;
	srcModuleIndex = PduR_ConfigStd->PduRSrcPduRef[srcPdu].BswModuleIndex;
	srcModule = PduR_BswModuleConfigData[srcModuleIndex].PduRBswModuleRef;
	pduDestSum = PduR_ConfigStd->PduRRoutingTableRef[PduR_ConfigStd->PduRConfigId].PduRRoutingPathRef[srcPdu].PduDestSum;
	switch(srcModule)
	{
		#if(STD_ON == PDUR_COM_SUPPORT)
		case PDUR_COM:
		#endif
		#if(STD_ON == PDUR_LDCOM_SUPPORT)
		case PDUR_LDCOM:
		#endif
		#if(STD_ON == PDUR_SECOC_SUPPORT)
		case PDUR_SECOC:
		#endif
		#if(STD_ON == PDUR_DCM_SUPPORT)
		case PDUR_DCM:
		#endif
		#if(STD_ON == PDUR_J1939DCM_SUPPORT)
		case PDUR_J1939DCM:
		#endif
			PduR_UpTpTxConfirmationHandle(srcModule,srcPdu,pduDestSum,id,result);
			break;
		/*Tp layer:Gateway pdu*/
		default:
			gateWayTpRunTimeId = PduR_ConfigStd->PduRDestPduRef[id].GateWayTpRunTimeIndex;
			PduR_GateWayDestTpRTSate[gateWayTpRunTimeId].TpTxConfirmation = TRUE;
			if(FALSE == PduR_AtLeastOneDestIsNotTxConfirm(srcPdu))
			{
				PduR_ClearBufferAndStateOfGateWayTpPdu(srcPdu);
			}
			break;
	}
	return;
}
#endif/*STD_ON == PDUR_TP_SUPPORT*/

#if((STD_ON == PDUR_CANIF_SUPPORT) || (STD_ON == PDUR_LINIF_SUPPORT))
static FUNC(void, PDUR_CODE)
PduR_EnQueueBuffer(PduIdType PduId,P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA)PduInfo)
{
    uint8 bufferDepth;
    uint16 bufferId;
    uint8 cnt;
    boolean findIdleBuffer = FALSE;
    uint32 pduMaxLength;
    bufferId = PduR_ConfigStd->PduRDestPduRef[PduId].PduRDestTxBufferRef;
    pduMaxLength = PduR_TxBuffer[bufferId].PduRPduMaxLength;
    bufferDepth = PduR_TxBuffer[bufferId].PduRTxBufferDepth;
	#if(STD_ON == PDUR_DEV_ERROR_DETECT)
    uint8 destModuleIndex;
	#endif
    if(pduMaxLength > PduInfo->SduLength)
    {
    	pduMaxLength = PduInfo->SduLength;
    }
    if(1 == bufferDepth)
    {
    	PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used = TRUE;
    	PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength = pduMaxLength;
    	PduR_Memcpy
    	(
    		PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData,
    		PduInfo->SduDataPtr,
    		pduMaxLength
    	);
    }
    if(bufferDepth > 1)
    {
        for(cnt = 0u;(cnt < bufferDepth)&&(FALSE == findIdleBuffer);cnt++)
        {
        	if(FALSE == PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt].used)
        	{
        		findIdleBuffer = TRUE;
            	PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt].used = TRUE;
            	PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt].SduLength = pduMaxLength;
            	PduR_Memcpy
            	(
            		PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt].TxBufferData,
            		PduInfo->SduDataPtr,
            		pduMaxLength
            	);
        	}
        }
        /*all buffer is used, the FIFO is flushed */
        if(FALSE == findIdleBuffer)
        {
        	PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used = TRUE;
        	PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength = pduMaxLength;
        	PduR_Memcpy
        	(
        		PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData,
        		PduInfo->SduDataPtr,
        		pduMaxLength
        	);
        	for(cnt = 1u;cnt < bufferDepth;cnt++)
        	{
        		PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt].used = FALSE;
        	}
            #if(STD_ON == PDUR_DEV_ERROR_DETECT)
	        destModuleIndex = PduR_ConfigStd->PduRDestPduRef[PduId].BswModuleIndex;
	        switch(PduR_BswModuleConfigData[destModuleIndex].PduRBswModuleRef)
	        {
				#if(STD_ON == PDUR_CANIF_SUPPORT)
				case PDUR_CANIF:
					Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
					break;
                #endif
				#if(STD_ON == PDUR_LINIF_SUPPORT)
				case PDUR_LINIF:
					Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
					break;
				#endif
				default:
					break;
	        }
            #endif
        }
    }
    return;
}

static FUNC(void, PDUR_CODE)
PduR_DeQueueBuffer(PduIdType PduId)
{
    uint8 bufferDepth;
    uint16 bufferId;
    uint8 cnt;
    bufferId = PduR_ConfigStd->PduRDestPduRef[PduId].PduRDestTxBufferRef;
    bufferDepth =PduR_TxBuffer[bufferId].PduRTxBufferDepth;
    if(1 == bufferDepth)
    {
    	PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used = FALSE;
    }
    if(bufferDepth > 1)
    {
        for(cnt = 0u;cnt < bufferDepth-1;cnt++)
        {
        	PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt] = PduR_TxBuffer[bufferId].PduRTxBufferRef[cnt+1];
        }
        PduR_TxBuffer[bufferId].PduRTxBufferRef[bufferDepth-1].used = FALSE;
    }

   return;
}

static FUNC(void, PDUR_CODE)
PduR_GateWayIfPdu(uint8 DestModule,PduIdType DestPduId,P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) InfoPtr)
{
	PduR_DestPduDataProvisionType provisionType;
	PduInfoType pduInfo;
    PduIdType gateWayIfPduDirectStateId;
    uint16 bufferId;
    PduIdType destModulePduId;
    destModulePduId = PduR_ConfigStd->PduRDestPduRef[DestPduId].PduRDestModulePduIndex;
    bufferId = PduR_ConfigStd->PduRDestPduRef[DestPduId].PduRDestTxBufferRef;
	provisionType = PduR_ConfigStd->PduRDestPduRef[DestPduId].PduRDestPduDataProvision;
	gateWayIfPduDirectStateId = PduR_ConfigStd->PduRDestPduRef[DestPduId].PduRGatewayDirectTxStateIndex;
    switch(DestModule)
	{
        #if(STD_ON == PDUR_CANIF_SUPPORT)
		case PDUR_CANIF:
			if(PDUR_TRIGGERTRANSMIT == provisionType)
			{
				PduR_EnQueueBuffer(DestPduId,InfoPtr);
				pduInfo.SduDataPtr = NULL_PTR;
				pduInfo.SduLength = 0;
				(void)CanIf_Transmit(destModulePduId,&pduInfo);
			}
			else
			{
				/*don't configuration buffer*/
				if(0xffff == bufferId)
				{
					(void)CanIf_Transmit(destModulePduId,InfoPtr);
				}
				else
				{
					/*only 1 buffer,��Last is best�� behavior*/
                    if(1 == PduR_TxBuffer[bufferId].PduRTxBufferDepth)
                    {
                    	if(PDUR_IDLE == PduR_DestPduState[gateWayIfPduDirectStateId])
                    	{
                    		/*transmit the old data in buffer,store the new data to buffer*/
                    		if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
                    		{
            					pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
            			 		pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
            					if(E_OK == CanIf_Transmit(destModulePduId,&pduInfo))
            					{
            						PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
            					}
            					PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    		}
                    		/*transmit the new data,don't store the data even the transmit not ok*/
                    		else
                    		{
								if(E_OK == CanIf_Transmit(destModulePduId,InfoPtr))
								{
									PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
								}
                    		}
                    	}
                    	/*gateway if pdu(direct),the pdu is busy.Store the data to buffer*/
                    	else
                    	{
                    		PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    	}
                    }
                    /*FIFO*/
                    else
                    {
                    	if(PDUR_IDLE == PduR_DestPduState[gateWayIfPduDirectStateId])
                    	{
                    		/*transmit the oldest data in buffer,store the new data to buffer*/
                    		if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
                    		{
            					pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
            			 		pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
            					if(E_OK == CanIf_Transmit(destModulePduId,&pduInfo))
            					{
            						PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
            					}
            					else
            					{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
            					}
            					PduR_DeQueueBuffer(DestPduId);
            					PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    		}
                    		/*transmit the new data,don't store the data even the transmit not ok*/
                    		else
                    		{
								if(E_OK == CanIf_Transmit(destModulePduId,InfoPtr))
								{
									PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
								}
								else
								{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_CANIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
								}
                    		}
                    	}
                    	/*gateway if pdu(direct),the pdu is busy.Store the data to buffer*/
                    	else
                    	{
                    		PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    	}
                    }
				}
			}
			break;
        #endif
		#if(STD_ON == PDUR_LINIF_SUPPORT)
		case PDUR_LINIF:
			if(PDUR_TRIGGERTRANSMIT == provisionType)
			{
				PduR_EnQueueBuffer(DestPduId,InfoPtr);
				pduInfo.SduDataPtr = NULL_PTR;
				pduInfo.SduLength = 0;
				(void)LinIf_Transmit(destModulePduId,&pduInfo);
			}
			else
			{
				/*don't configuration buffer*/
				if(0xffff == bufferId)
				{
					(void)LinIf_Transmit(destModulePduId,InfoPtr);
				}
				else
				{
					/*only 1 buffer,��Last is best�� behavior*/
                    if(1 == PduR_TxBuffer[bufferId].PduRTxBufferDepth)
                    {
                    	if(PDUR_IDLE == PduR_DestPduState[gateWayIfPduDirectStateId])
                    	{
                    		/*transmit the old data in buffer,store the new data to buffer*/
                    		if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
                    		{
            					pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
            			 		pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
            					if(E_OK == LinIf_Transmit(destModulePduId,&pduInfo))
            					{
            						PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
            					}
            					PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    		}
                    		/*transmit the new data,don't store the data even the transmit not ok*/
                    		else
                    		{
								if(E_OK == CanIf_Transmit(destModulePduId,InfoPtr))
								{
									PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
								}
                    		}
                    	}
                    	/*gateway if pdu(direct),the pdu is busy.Store the data to buffer*/
                    	else
                    	{
                    		PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    	}
                    }
                    /*FIFO*/
                    else
                    {
                    	if(PDUR_IDLE == PduR_DestPduState[gateWayIfPduDirectStateId])
                    	{
                    		/*transmit the oldest data in buffer,store the new data to buffer*/
                    		if(TRUE == PduR_TxBuffer[bufferId].PduRTxBufferRef[0].used)
                    		{
            					pduInfo.SduDataPtr = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].TxBufferData;
            			 		pduInfo.SduLength = PduR_TxBuffer[bufferId].PduRTxBufferRef[0].SduLength;
            					if(E_OK == LinIf_Transmit(destModulePduId,&pduInfo))
            					{
            						PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
            					}
            					else
            					{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
            					}
            					PduR_DeQueueBuffer(DestPduId);
            					PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    		}
                    		/*transmit the new data,don't store the data even the transmit not ok*/
                    		else
                    		{
								if(E_OK == CanIf_Transmit(destModulePduId,InfoPtr))
								{
									PduR_DestPduState[gateWayIfPduDirectStateId] = PDUR_BUSY;
								}
								else
								{
									#if(STD_ON == PDUR_DEV_ERROR_DETECT)
									Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_LINIFRXINDICATION_ID, PDUR_E_PDU_INSTANCES_LOST);
									#endif
								}
                    		}
                    	}
                    	/*gateway if pdu(direct),the pdu is busy.Store the data to buffer*/
                    	else
                    	{
                    		PduR_EnQueueBuffer(DestPduId,InfoPtr);
                    	}
                    }
				}
			}
			break;
		#endif
		#if(STD_ON == PDUR_FRIF_SUPPORT)
		case PDUR_FRIF:

			break;
		#endif
		default:
			break;
	}
    return;
}
#endif/*(STD_ON == PDUR_CANIF_SUPPORT) || (STD_ON == PDUR_LINIF_SUPPORT)*/
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"

#endif /* STD_OFF == PDUR_ZERO_COST_OPERATION */
/*=======[E N D   O F   F I L E]==============================================*/


