/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_Types.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : Type definitions of PDUR                                    **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef  PDUR_TYPES_H
#define  PDUR_TYPES_H
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define PDUR_TYPES_H_AR_MAJOR_VERSION  4
#define PDUR_TYPES_H_AR_MINOR_VERSION  2
#define PDUR_TYPES_H_AR_PATCH_VERSION  2
#define PDUR_TYPES_H_SW_MAJOR_VERSION  1
#define PDUR_TYPES_H_SW_MINOR_VERSION  0
#define PDUR_TYPES_H_SW_PATCH_VERSION  0
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR_Cfg.h"
#include "ComStack_Types.h"
#if(STD_OFF == PDUR_ZERO_COST_OPERATION)
/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
typedef enum
{
	PDUR_DIRECT = 0u,
	PDUR_TRIGGERTRANSMIT/* In case of PDUR_TRIGGERTRANSMIT the parameter PduRDestTxBufferRef is required*/
}PduR_DestPduDataProvisionType;

typedef uint16 PduR_PBConfigIdType;
typedef uint16 PduR_RoutingPathGroupIdType;
/*****************************************************************************************************************/
typedef struct
{
	const uint32 DefaultValueStart;
	const uint32 DefaultValueLength;
}PduRDefaultValueType;

typedef struct
{
	const PduIdType PduRSourcePduHandleId;/*RoutingPath Id is same to the PduRSourcePduHandleId*/
	const boolean PduRSrcPduUpTxConf;/*define if forward tx confirm to up layer(COM,DCM,J1939DCM...)*/
	const PduIdType UpTxconfirmStateIndex;/*if not up tx(1:n(n>1)),the value is 0xffff��IF module is before of TP module.*/
    /*PB config,get from other module.replace autosar config:PduRSrcPduRef*/
	const PduIdType PduRDestModulePduIndex;
	const uint8 BswModuleIndex;
    #if(STD_ON == PDUR_META_DATA_SUPPORT)
	const uint8 MetaDataLength;
    #endif
	const PduIdType TxTpMulticastIndex;/*used for 1:n (up module transmit)*/
}PduRSrcPduType;

typedef struct
{
	const PduIdType PduRDestPduHandleId;
	const PduR_DestPduDataProvisionType PduRDestPduDataProvision;/*only for If gatewayed I-PDUs*/
	const PduIdType PduRGatewayDirectTxStateIndex;/*used for gateway direct pdu.*/
	const uint16 PduRTpThreshold;
	const PduIdType GateWayTpRunTimeIndex;/*used for gateway Tp.*/
	const boolean PduRTransmissionConfirmation;/*This parameter is only for communication interfaces. Transport protocol modules will always call the TxConfirmation function*/
	const PduIdType PduRSrcPduRef;/*simple implementation for PduR_CanIfTxConfirmation.*/
	/*PB config,get from other module.replace autosar config:PduRDestPduRef*/
	const PduIdType PduRDestModulePduIndex;
	const uint8 BswModuleIndex;
	const uint16 PduRDestTxBufferRef;/*Reference to [ PduRTxBuffer ],no buffer the default value is 0xffff*/
	P2CONST(PduRDefaultValueType, AUTOMATIC, PDUR_CONST_PBCFG) PduRDefaultValueRef;/*no default value is null*/
}PduRDestPduType;

typedef struct
{
	const uint16 PduRSrcPduId;
	const uint8 PduDestSum;
	P2CONST(uint16, AUTOMATIC, PDUR_CONST_PBCFG) PduRDestPduIdRef;
	const boolean TpRoute;/*If route or Tp route*/
	const boolean TpPduIsSF;/*Tp route:SF or MF*/
	const boolean GatewayOnTheFly;/*gateway-on-the-fly mode or not*/
	const uint16 PduRTpMaxThreshold;/*gateway-on-the-fly mode,used to select valid tp buffer*/
}PduRRoutingPathType;

typedef struct
{
	P2CONST(PduRRoutingPathType, AUTOMATIC, PDUR_CONST_PBCFG) PduRRoutingPathRef;
}PduRRoutingTableType;
/*****************************************************************************************************************/
typedef struct
{
	const boolean PduRIsEnabledAtInit;
	const PduR_RoutingPathGroupIdType PduRRoutingPathGroupId;
	const uint16 PduRDestPduRefNumber;
	P2CONST(uint16, AUTOMATIC, PDUR_CONST_PBCFG) PduRDestPduRef;
}PduRRoutingPathGroupType;
/*****************************************************************************************************************/
typedef struct
{
	const PduR_PBConfigIdType PduRConfigId;
	P2CONST(PduRRoutingPathGroupType, AUTOMATIC, PDUR_CONST_PBCFG) PduRRoutingPathGroupRef;
	P2CONST(PduRRoutingTableType, AUTOMATIC, PDUR_CONST_PBCFG) PduRRoutingTableRef;
	P2CONST(PduRSrcPduType, AUTOMATIC, PDUR_CONST_PBCFG) PduRSrcPduRef;
	P2CONST(PduRDestPduType, AUTOMATIC, PDUR_CONST_PBCFG) PduRDestPduRef;
}PduR_PBConfigType;

typedef struct
{
	const uint8	PduRBswModuleRef;
	const boolean PduRCancelReceive; /*TP module:CancelReceive API*/
	const boolean PduRCancelTransmit;
	const boolean PduRChangeParameterApi;
	const boolean PduRCommunicationInterface;
	const boolean PduRTransportProtocol;
	const boolean PduRLowerModule;
	const boolean PduRUpperModule;
	const boolean PduRRetransmission;/*TP retry*/
	const boolean PduRTriggertransmit;
	const boolean PduRTxConfirmation;
	const boolean PduRUseTag;/* PduR_<Up>CancelReceive,PduR_<Up>CancelTransmit,PduR_<Up>ChangeParameter:<Up> */
}PduRBswModuleType;

typedef struct
{
	uint16 RxBufferOffset;
	const uint16 TpBufferLength;
	uint16 SduLength;
	P2VAR(uint8, AUTOMATIC, PDUR_CONST_PBCFG) TpBufferData;
	PduIdType PduHandleId;
	boolean used;
    #if(STD_ON == PDUR_META_DATA_SUPPORT)
	uint8 MetaData[4];
    #endif
	uint8 NextTpBufferId;/*used for gateway-on-the-fly*/
}PduR_TpBufferTableType;

typedef struct
{
	uint16 SduLength;
	P2VAR(uint8, AUTOMATIC, PDUR_CONST_PBCFG) TxBufferData;
	boolean used;
    #if(STD_ON == PDUR_META_DATA_SUPPORT)
	uint8 MetaData[4];
    #endif
}PduR_TxBufferType;

typedef struct
{
	uint32 PduRPduMaxLength;
	uint8 PduRTxBufferDepth;/*for TP SF:always 1*/
	P2VAR(PduR_TxBufferType, AUTOMATIC, PDUR_CONST_PBCFG) PduRTxBufferRef;
}PduR_TxBufferTableType;
#endif/*STD_OFF == PDUR_ZERO_COST_OPERATION*/

#endif  /* end of PDUR_TYPES_H */


