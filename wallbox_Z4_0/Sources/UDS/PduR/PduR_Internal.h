/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_Internal.h                                             **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : PDUR internal header for internal API declarations          **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef  PDUR_INTERNAL_H
#define  PDUR_INTERNAL_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR.h"
#if(STD_OFF == PDUR_ZERO_COST_OPERATION)
/*******************************************************************************
**                      External function declarations                        **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
#if(STD_ON == PDUR_TP_SUPPORT)
/*Rx Tp Pdu gateway to only one Tp Module Pdu handle,when start of reception*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_StartOfReceptionToOneTpHandle
(
    PduIdType SrcPduId,
	PduIdType DestPduId,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType Length,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);
/*1:n Rx Tp Pdu GateWay(more than one Tp Module,zero or one Up Module),when start of reception*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_StartOfReceptionToMoreModuleHandle
(
	PduIdType SrcPduId,
	uint8 DestPduSum,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType Length,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);
/*at least one dest pdu state is enabled*/
extern FUNC(boolean, PDUR_CODE)
PduR_AtLeastOneDestIsEnabled(uint16 PduRSrcPduId);
/*Rx Tp Pdu gateway to only one Tp Module Pdu handle,when copy Rx Data*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CopyRxDataToOneTpHandle
(
	PduIdType SrcPduId,
	PduIdType DestPduId,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA)BufferSizePtr
);
/*1:n Rx Tp Pdu GateWay(more than one Tp Module,zero or one Up Module),when copy Rx Data*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CopyRxDataToMoreModuleHandle
(
	PduIdType SrcPduId,
	uint8 DestPduSum,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA)BufferSizePtr
);
/*Rx Tp Pdu gateway to only one Tp Module Pdu handle,when Rx Indication*/
extern FUNC(void, PDUR_CODE)
PduR_RxIndicationToOneTpHandle(PduIdType SrcPduId,PduIdType DestPduId);
/*1:n Rx Tp Pdu GateWay(more than one Tp Module,zero or one Up Module),when Rx Indication*/
extern FUNC(void, PDUR_CODE)
PduR_RxIndicationToMoreModuleHandle(PduIdType SrcPduId,uint8 DestPduSum);
/*one Tp Pdu route to one Tp Pdu,the dest pdu copy tx data handle*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_OneDestCopyTxDataFromTpHandle
(
	PduIdType SrcPduId,
	PduIdType DestPduId,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
);
/*one Tp Pdu route to more module Pdus,one dest Tp Pdu copy tx data handle*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_MoreDestCopyTxDataFromTpHandle
(
	PduIdType SrcPduId,
	PduIdType DestPduId,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
);
/*one up module Tx tp pdu(SF) to more Tp Module Pdus,the Lo Tp copy tx data*/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_UpMulticastTpCopyData
(
	uint8 SoureModule,
	PduIdType SrcPduId,
	PduIdType DestPduId,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
);
/*one up module Tx Tp Pdu to one or more dest Tp Pdu,one Tp Pdu call the TpTxConfirmation Handle*/
extern FUNC(void, PDUR_CODE)
PduR_UpTpTxConfirmationHandle(uint8 SoureModule,PduIdType SrcPduId,uint8 DestPduSum,PduIdType DestPduId,Std_ReturnType Result);
/*clear buffer and gateway state for gateway tp pdu*/
extern FUNC(void, PDUR_CODE)
PduR_ClearBufferAndStateOfGateWayTpPdu(uint16 PduRSrcPduId);
/*1:n(n>1)Tp route,at least one dest pdu is still need transmit,return TRUE*/
extern FUNC(boolean, PDUR_CODE)
PduR_AtLeastOneDestIsNotTxConfirm(uint16 PduRSrcPduId);
#endif/*STD_ON == PDUR_TP_SUPPORT*/
/*copy data from source to dest*/
extern FUNC(void, PDUR_CODE)
PduR_Memcpy
(
    P2VAR(uint8, AUTOMATIC, AUTOMATIC) dest,
    P2CONST(uint8, AUTOMATIC, AUTOMATIC) source,
    uint16 length
);
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"

#endif/*STD_OFF == PDUR_ZERO_COST_OPERATION*/

#endif/* end of PDUR_INTERNAL_H */


