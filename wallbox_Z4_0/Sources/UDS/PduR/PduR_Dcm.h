/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_Dcm.h                                                  **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration supplied by PDUR to DCM                     **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef  PDUR_DCM_H
#define  PDUR_DCM_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define PDUR_DCM_H_AR_MAJOR_VERSION      4U
#define PDUR_DCM_H_AR_MINOR_VERSION      2U
#define PDUR_DCM_H_AR_PATCH_VERSION      2U
#define PDUR_DCM_H_SW_MAJOR_VERSION      1U
#define PDUR_DCM_H_SW_MINOR_VERSION      0U
#define PDUR_DCM_H_SW_PATCH_VERSION      0U

#if(STD_ON == PDUR_DCM_SUPPORT)
/* Zero cost enable */
#if(STD_ON == PDUR_ZERO_COST_OPERATION)
#define PduR_DcmTransmit CanTp_Transmit
#define PduR_DcmCancelTransmit CanTp_CancelTransmit
#define PduR_DcmChangeParameter CanTp_ChangeParameter
#define PduR_DcmCancelReceive CanTp_CancelReceive
#else /* STD_ON == PDUR_ZERO_COST_OPERATION */
/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
/******************************************************************************/
/*
 * Brief               Requests transmission of an I-PDU.
 * ServiceId           0x99
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Length and pointer to the buffer of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - request is accepted by the destination module; transmission is continued.
 *                     E_NOT_OK - request is not accepted by the destination module;transmission is aborted.
 */
/******************************************************************************/
extern FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmTransmit(PduIdType id, P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info)/*Only have LoTp,no LoIf*/;
/******************************************************************************/
/*
 * Brief			   Requests cancellation of an ongoing transmission of an I-PDU in a lower
 *                     layer communication interface or transport protocol module.
 * ServiceId           0x9a
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return			   Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK - Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK - Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CANCEL_TRANSMIT)
extern FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmCancelTransmit(PduIdType id);
#endif
/******************************************************************************/
/*
 * Brief               Request to change a specific transport protocol parameter (e.g. block size).
 * ServiceId           0x9b
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identifiaction of the I-PDU which the parameter change shall affect.
 *                     parameter: The parameter that shall change.
 *                     value: The new value of the parameter
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: The parameter was changed successfully.
 *                     E_NOT_OK: The parameter change was rejected.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CHANGE_PARAMETER)
extern FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmChangeParameter(PduIdType id,TPParameterType parameter,uint16 value);
#endif
/******************************************************************************/
/*
 * Brief               Requests cancellation of an ongoing reception of an I-PDU in a lower layer transport protocol module.
 * ServiceId           0x9c
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      id: Identification of the I-PDU to be cancelled.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType (E_OK,E_NOT_OK)
 *                     E_OK: Cancellation was executed successfully by the destination module.
 *                     E_NOT_OK: Cancellation was rejected by the destination module.
 */
/******************************************************************************/
#if(STD_ON == PDUR_DCM_CANCEL_RECEIVE)
extern FUNC(Std_ReturnType, PDUR_CODE)
PduR_DcmCancelReceive(PduIdType id);
#endif
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"

#endif /*STD_ON == PDUR_ZERO_COST_OPERATION */

#endif /*STD_ON == PDUR_DCM_SUPPORT*/

#endif  /* end of PDUR_DCM_H */


