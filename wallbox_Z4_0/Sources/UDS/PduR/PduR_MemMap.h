/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_MemMap.h                                               **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION : Memory mapping abstraction declaration of PDUR              **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef PDUR_MEMMAP_H
#define PDUR_MEMMAP_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define PDUR_MEMMAP_VENDOR_ID  		62
#define PDUR_MEMMAP_MODULE_ID  		51
#define PDUR_MEMMAP_AR_MAJOR_VERSION  4
#define PDUR_MEMMAP_AR_MINOR_VERSION  2
#define PDUR_MEMMAP_AR_PATCH_VERSION  2
#define PDUR_MEMMAP_SW_MAJOR_VERSION  1
#define PDUR_MEMMAP_SW_MINOR_VERSION  0
#define PDUR_MEMMAP_SW_PATCH_VERSION  0
#define PDUR_MEMMAP_VENDOR_API_INFIX  0

/*=======[M E M M A P  S Y M B O L  D E F I N E]==============================*/

#include "MemMap.h"


#endif /* PDUR_MEMMAP_H */
