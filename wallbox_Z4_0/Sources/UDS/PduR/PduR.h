/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR.h                                                      **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration and type definitions of PDUR                **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
#ifndef  PDUR_H
#define  PDUR_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR_Types.h"
#include "PduR_Cfg.h"
#if(STD_ON == PDUR_CANIF_SUPPORT)
#include "CanIf.h"
#include "CanIf_Cbk.h"
#endif
#if(STD_ON == PDUR_CANTP_SUPPORT)
#include "CanTp.h"
#include "CanTp_Cbk.h"
#endif
#if(STD_ON == PDUR_J1939TP_SUPPORT)
#include "J1939Tp.h"
#include "J1939Tp_Cbk.h"
#endif
#if(STD_ON == PDUR_COM_SUPPORT)
//#include "Com.h"
//#include "Com_Cbk.h"
#endif
#if(STD_ON == PDUR_DCM_SUPPORT)
#include "Dcm.h"
#include "Dcm_Cbk.h"
#endif
#if(STD_ON == PDUR_J1939DCM_SUPPORT)
#include "J1939Dcm.h"
#include "J1939Dcm_Cbk.h"
#endif
#if(STD_ON == PDUR_IPDUM_SUPPORT)
#include "IpduM.h"
#include "IpduM_Cbk.h"
#endif
#if(STD_ON == PDUR_J1939RM_SUPPORT)
#include "J1939Rm.h"
#include "J1939Rm_Cbk.h"
#endif
#if(STD_ON == PDUR_LDCOM_SUPPORT)
#include "LdCom.h"
#include "LdCom_Cbk.h"
#endif
#if(STD_ON == PDUR_SECOC_SUPPORT)
#include "SecOC.h"
#include "SecOC_Cbk.h"
#endif
#if(STD_ON == PDUR_DBG_SUPPORT)
#include "Dbg.h"
#include "Dbg_Cbk.h"
#endif
#if(STD_ON == PDUR_CANNM_SUPPORT)
#include "CanNm.h"
#include "CanNm_Cbk.h"
#endif
#if(STD_ON == PDUR_OSEKNM_SUPPORT)
#include "OsekNm.h"
#include "OsekNm_Cbk.h"
#endif
#if(STD_ON == PDUR_LINIF_SUPPORT)
#include "LinIf.h"
#include "LinIf_Cbk.h"
#endif
#if(STD_ON == PDUR_FRIF_SUPPORT)
#include "FrIf.h"
#include "FrIf_Cbk.h"
#endif
#if(STD_ON == PDUR_FRNM_SUPPORT)
#include "FrNm.h"
#include "FrNm_Cbk.h"
#endif
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define PDUR_VENDOR_ID               62U
#define PDUR_MODULE_ID               51U
#define PDUR_INSTANCE_ID             0U
#define PDUR_H_AR_MAJOR_VERSION      4U
#define PDUR_H_AR_MINOR_VERSION      2U
#define PDUR_H_AR_PATCH_VERSION      2U
#define PDUR_H_SW_MAJOR_VERSION      1U
#define PDUR_H_SW_MINOR_VERSION      0U
#define PDUR_H_SW_PATCH_VERSION      0U

#if(STD_OFF == PDUR_ZERO_COST_OPERATION)
#include "PduR_Internal.h"

#if(STD_ON == PDUR_DEV_ERROR_DETECT)
/* General function id */
#define PDUR_INIT_ID                       	 	((uint8)0xf0)
#define PDUR_GETVERSIONINFO_ID               	((uint8)0xf1)
#define PDUR_GETCONFIGURATIONID_ID              ((uint8)0xf2)
#define PDUR_ENABLEROUTING_ID                   ((uint8)0xf3)
#define PDUR_DISABLEROUTING_ID                  ((uint8)0xf4)
/* CanIf function id */
#define PDUR_CANIFRXINDICATION_ID               ((uint8)0x01)
#define PDUR_CANIFTXCONFIRMATION_ID             ((uint8)0x02)
#define PDUR_CANIFTRIGGERTRANSMIT_ID            ((uint8)0x03)
/* LinIf function id */
#define PDUR_LINIFRXINDICATION_ID               ((uint8)0x51)
#define PDUR_LINIFTXCONFIRMATION_ID             ((uint8)0x52)
#define PDUR_LINIFTRIGGERTRANSMIT_ID            ((uint8)0x53)
/* CanNm function id */
#define PDUR_CANNMRXINDICATION_ID               ((uint8)0x11)
#define PDUR_CANNMTXCONFIRMATION_ID             ((uint8)0x12)
#define PDUR_CANNMTRIGGERTRANSMIT_ID            ((uint8)0x13)
/* OsekNm function id */
#define PDUR_OSEKNMRXINDICATION_ID              ((uint8)0x81)
#define PDUR_OSEKNMTXCONFIRMATION_ID            ((uint8)0x82)
#define PDUR_OSEKNMTRIGGERTRANSMIT_ID           ((uint8)0x83)
/*CanTP function id */
#define PDUR_CANTPCOPYRXDATA_ID                 ((uint8)0x04)
#define PDUR_CANTPRXINDICATION_ID               ((uint8)0x05)
#define PDUR_CANTPSTARTOFRECEPTION_ID           ((uint8)0x06)
#define PDUR_CANTPCOPYTXDATA_ID                 ((uint8)0x07)
#define PDUR_CANTPTXCONFIRMATION_ID             ((uint8)0x08)
/*LinTP function id */
#define PDUR_LINTPCOPYRXDATA_ID                 ((uint8)0x54)
#define PDUR_LINTPRXINDICATION_ID               ((uint8)0x55)
#define PDUR_LINTPSTARTOFRECEPTION_ID           ((uint8)0x56)
#define PDUR_LINTPCOPYTXDATA_ID                 ((uint8)0x57)
#define PDUR_LINTPTXCONFIRMATION_ID             ((uint8)0x58)
/*J1939CanTP function id */
#define PDUR_J1939TPCOPYRXDATA_ID               ((uint8)0x14)
#define PDUR_J1939TPRXINDICATION_ID             ((uint8)0x15)
#define PDUR_J1939TPSTARTOFRECEPTION_ID         ((uint8)0x16)
#define PDUR_J1939TPCOPYTXDATA_ID               ((uint8)0x17)
#define PDUR_J1939TPTXCONFIRMATION_ID           ((uint8)0x18)
/* Com function id */
#define PDUR_COMTRANSMIT_ID            			((uint8)0x89)
#define PDUR_COMCANCELTRANSMIT_ID               ((uint8)0x8a)
#define PDUR_COMCHANGEPARAMETER_ID              ((uint8)0x8b)
#define PDUR_COMCANCELRECEIVE_ID                ((uint8)0x8c)

/* LdCom function id */
#define PDUR_LDCOMTRANSMIT_ID            		((uint8)0x89)
#define PDUR_LDCOMCANCELTRANSMIT_ID             ((uint8)0x8a)
#define PDUR_LDCOMCHANGEPARAMETER_ID            ((uint8)0x8b)
#define PDUR_LDCOMCANCELRECEIVE_ID              ((uint8)0x8c)

/* SecOC function id */
#define PDUR_SECOCTRANSMIT_ID            	    ((uint8)0xc9)
#define PDUR_SECOCCANCELTRANSMIT_ID             ((uint8)0xca)
#define PDUR_SECOCRXINDICATION_ID               ((uint8)0x71)
#define PDUR_SECOCTXCONFIRMATION_ID             ((uint8)0x72)

/* IpduM function id */
#define PDUR_IPDUMTRANSMIT_ID            	    ((uint8)0xa9)
#define PDUR_IPDUMTRIGGERTRANSMIT_ID            ((uint8)0x23)
#define PDUR_IPDUMRXINDICATION_ID               ((uint8)0x21)
#define PDUR_IPDUMTXCONFIRMATION_ID             ((uint8)0x22)

/* J1939Rm function id */
#define PDUR_J1939RMTRANSMIT_ID            	    ((uint8)0xe9)
#define PDUR_J1939RMCANCELTRANSMIT_ID           ((uint8)0xea)
/* Dcm function id */
#define PDUR_DCMTRANSMIT_ID            			((uint8)0x99)
#define PDUR_DCMCANCELTRANSMIT_ID               ((uint8)0x9a)
#define PDUR_DCMCHANGEPARAMETER_ID              ((uint8)0x9b)
#define PDUR_DCMCANCELRECEIVE_ID                ((uint8)0x9c)
/* J1939Dcm function id */
#define PDUR_J1939DCMTRANSMIT_ID            	((uint8)0xf9)
#define PDUR_J1939DCMCANCELTRANSMIT_ID          ((uint8)0xfa)
#define PDUR_J1939DCMCHANGEPARAMETER_ID         ((uint8)0xfb)
#define PDUR_J1939DCMCANCELRECEIVE_ID           ((uint8)0xfc)

/* Error Classification */
#define PDUR_E_INIT_FAILED                   ((uint8)0x00)
#define PDUR_E_PARAM_POINTER	             ((uint8)0x09)
#define PDUR_E_PDU_INSTANCES_LOST            ((uint8)0x0a)
#define PDUR_E_INVALID_REQUEST               ((uint8)0x01)
#define PDUR_E_PDU_ID_INVALID                ((uint8)0x02)
#define PDUR_E_TP_TX_REQ_REJECTED            ((uint8)0x03)
#define PDUR_E_ROUTING_PATH_GROUP_ID_INVALID ((uint8)0x08)

#endif/*STD_ON == PDUR_DEV_ERROR_DETECT*/
/*******************************************************************************
**                      Private Type Definitions                              **
*******************************************************************************/
typedef struct
{
	boolean NeedGateWayOnTheFly;/*gateway-on-the-fly mode,decide the pdu need call transmit API or not*/
	uint8 ActiveTpBufferId;
	uint8 CopyCompleteTpBufferNumber;/*gateway-on-the-fly mode(1:n),used to decide the tp buffer(all dest have copyed) need to be cleared*/
	uint16 TxBufferOffset;/*gateway tp(MF),record where to copy data*/
	boolean TpTxConfirmation;/*E_OK or E_NOT_OK*/
}PduR_GateWayDestTpRunTimeType;

typedef struct
{
	uint8 DestCopyDataTimer;
	uint8 ActiveDestNumber;
}PduR_UpTxTpMulPduRunTimeType;

typedef enum
{
    PDUR_BUSY = 0u,
    PDUR_IDLE
} PduR_DestStateType;

typedef enum
{
    PDUR_UNINIT = 0u, /* not initialized */
    PDUR_ONLINE /* initialized successfully */
} PduR_StateType;
/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/
/*define in PduR_PBCfg.c*/
#define PDUR_START_CONST_PBCFG_ROOT
#include "PduR_MemMap.h"
extern CONST(PduR_PBConfigType, PDUR_CONST_PBCFG) PduR_PBConfigData;
#define PDUR_STOP_CONST_PBCFG_ROOT
#include "PduR_MemMap.h"

#if(PDUR_TP_BUFFER_SUM > 0)
extern PduR_TpBufferTableType PduR_TpBuffer[PDUR_TP_BUFFER_SUM];
#else
extern P2VAR(PduR_TpBufferTableType, AUTOMATIC, PDUR_APPL_DATA) PduR_TpBuffer;
#endif

#if(PDUR_TX_BUFFER_SUM > 0)
extern const PduR_TxBufferTableType PduR_TxBuffer[PDUR_TX_BUFFER_SUM];
#else
extern CONSTP2CONST(PduR_TxBufferTableType, AUTOMATIC, PDUR_APPL_DATA) PduR_TxBuffer;
#endif

#if(PDUR_DEFAULT_VALUE_LENGTH > 0)
extern const uint8 PduR_Default_value[PDUR_DEFAULT_VALUE_LENGTH];
#else
extern CONSTP2CONST(uint8, AUTOMATIC, PDUR_APPL_DATA) PduR_Default_value;
#endif

#if(PDUR_BSW_MODULE_SUM > 0)
extern const PduRBswModuleType PduR_BswModuleConfigData[PDUR_BSW_MODULE_SUM];
#else
extern CONSTP2CONST(PduRBswModuleType, AUTOMATIC, PDUR_APPL_DATA) PduR_BswModuleConfigData;
#endif

/*define in PduR.c*/
/*PduR PB Configuration Run Time point parameter*/
extern P2CONST(PduR_PBConfigType, PDUR_CONST, PDUR_CONST_PBCFG) PduR_ConfigStd;
/*all route dest pdus is enable or disable*/
#if(PDUR_DEST_PDU_SUM > 0)
extern boolean PduRIsEnabled[PDUR_DEST_PDU_SUM];
#else
extern P2VAR(boolean, AUTOMATIC, PDUR_APPL_DATA) PduRIsEnabled;
#endif

/*up module Tx 1:n(include If and Tp)*/
#if(PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM > 0)
extern boolean PduR_UpTxState[PDUR_SRC_UP_MULTICAST_TX_IF_SUM + PDUR_SRC_UP_MULTICAST_TX_TP_SUM];
#else
extern P2VAR(boolean, AUTOMATIC, PDUR_APPL_DATA) PduR_UpTxState;
#endif

/*up module Tx 1:n (only Tp)*/
#if(PDUR_SRC_UP_MULTICAST_TX_TP_SUM > 0)
extern PduR_UpTxTpMulPduRunTimeType PduR_UpTxTpMulRoute[PDUR_SRC_UP_MULTICAST_TX_TP_SUM];
#else
extern P2VAR(PduR_UpTxTpMulPduRunTimeType, AUTOMATIC, PDUR_APPL_DATA) PduR_UpTxTpMulRoute;
#endif

/*gateway Tp routing path:all dest pdu(SF/MF,route to Tp or Up)*/
#if(PDUR_DEST_GATEWAY_TP_PDU_SUM > 0)
extern PduR_GateWayDestTpRunTimeType PduR_GateWayDestTpRTSate[PDUR_DEST_GATEWAY_TP_PDU_SUM];
#else
extern P2VAR(PduR_GateWayDestTpRunTimeType, AUTOMATIC, PDUR_APPL_DATA) PduR_GateWayDestTpRTSate;
#endif

/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
/******************************************************************************/
/*
 * Brief               Returns the version information of this module.
 * ServiceId           0xf1
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     versionInfo:Pointer to where to store the version
 * 						information of this module.
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
#if (STD_ON == PDUR_VERSION_INFO_API)
	#if (STD_ON == PDUR_DEV_ERROR_DETECT)
	#define PduR_GetVersionInfo(VersionInfo) \
	    do{\
	        if (NULL_PTR == (VersionInfo))\
	        {\
	            Det_ReportError(PDUR_MODULE_ID, PDUR_INSTANCE_ID, PDUR_GETVERSIONINFO_ID, PDUR_E_PARAM_POINTER);\
	        }\
	        else \
	        {\
	            (VersionInfo)->vendorID = PDUR_VENDOR_ID; \
	            (VersionInfo)->moduleID = PDUR_MODULE_ID; \
	            (VersionInfo)->sw_major_version = PDUR_H_SW_MAJOR_VERSION; \
	            (VersionInfo)->sw_minor_version = PDUR_H_SW_MINOR_VERSION; \
	            (VersionInfo)->sw_patch_version = PDUR_H_SW_PATCH_VERSION; \
	        }\
	    }while(0)
	#else
	#define PduR_GetVersionInfo(VersionInfo) \
	    do{\
	          (VersionInfo)->vendorID = PDUR_VENDOR_ID; \
	          (VersionInfo)->moduleID = PDUR_MODULE_ID; \
	          (VersionInfo)->sw_major_version = PDUR_H_SW_MAJOR_VERSION; \
	          (VersionInfo)->sw_minor_version = PDUR_H_SW_MINOR_VERSION; \
	          (VersionInfo)->sw_patch_version = PDUR_H_SW_PATCH_VERSION; \
	    }while(0)
	#endif
#endif
/******************************************************************************/
/*
 * Brief               Initializes the PDU Router
 * ServiceId           0xf0
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      ConfigPtr: Pointer to Post build configuration data.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
extern FUNC(void, PDUR_CODE)
PduR_Init(P2CONST(PduR_PBConfigType, AUTOMATIC, PDUR_CONST_PBCFG) ConfigPtr);
/******************************************************************************/
/*
 * Brief               Returns the unique identifier of the post-build time
 *                     configuration of the PDU Router
 * ServiceId           0xf2
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              PduR_PBConfigIdType: Identifier of the post-build time configuration
 */
/******************************************************************************/
extern FUNC(PduR_PBConfigIdType, PDUR_CODE)
PduR_GetConfigurationId(void);
/******************************************************************************/
/*
 * Brief               Enables a routing path table
 * ServiceId           0xf3
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the routing path group.
 *                     Routing path groups are defined in the PDU router configuration
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
extern FUNC(void, PDUR_CODE)
PduR_EnableRouting(PduR_RoutingPathGroupIdType id);
/******************************************************************************/
/*
 * Brief               Disables a routing path table
 * ServiceId           0xf4
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the routing path group.
 *                     Routing path groups are defined in the PDU router configuration
 *                     initialize (true,false)
 *                     true: initialize single buffers to the default value
 *                     false: retain current value of single buffers
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
extern FUNC(void, PDUR_CODE)
PduR_DisableRouting(PduR_RoutingPathGroupIdType id,boolean initialize);
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"

#endif/*STD_OFF == PDUR_ZERO_COST_OPERATION*/

#endif  /* end of PDUR_H */

/*=======[E N D   O F   F I L E]==============================================*/

