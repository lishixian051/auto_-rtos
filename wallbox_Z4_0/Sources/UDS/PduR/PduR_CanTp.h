/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : PduR_CanTp.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration supplied by PDUR to CANTP                   **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
#ifndef  PDUR_CANTP_H
#define  PDUR_CANTP_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "PduR.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define PDUR_CANTP_H_AR_MAJOR_VERSION      4U
#define PDUR_CANTP_H_AR_MINOR_VERSION      2U
#define PDUR_CANTP_H_AR_PATCH_VERSION      2U
#define PDUR_CANTP_H_SW_MAJOR_VERSION      1U
#define PDUR_CANTP_H_SW_MINOR_VERSION      0U
#define PDUR_CANTP_H_SW_PATCH_VERSION      0U

#if(STD_ON == PDUR_CANTP_SUPPORT)
/* Zero cost enable */
#if(STD_ON == PDUR_ZERO_COST_OPERATION)
#define PduR_CanTpStartOfReception Dcm_StartOfReception
#define PduR_CanTpCopyRxData Dcm_CopyRxData
#define PduR_CanTpRxIndication Dcm_TpRxIndication
#define PduR_CanTpCopyTxData Dcm_CopyTxData
#define PduR_CanTpTxConfirmation Dcm_TpTxConfirmation
#else	/*STD_ON == PDUR_ZERO_COST_OPERATION*/
/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define PDUR_START_SEC_CODE
#include "PduR_MemMap.h"
/******************************************************************************/
/*
 * Brief               This function is called at the start of receiving an N-SDU. The N-SDU might be
 *                     fragmented into multiple N-PDUs (FF with one or more following CFs) or might
 *                     consist of a single N-PDU (SF).
 * ServiceId           0x06
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the I-PDU.
 *                     info: Pointer to a PduInfoType structure containing the payload
 *                     data (without protocol information) and payload length of the
 *                     first frame or single frame of a transport protocol I-PDU
 *                     reception. Depending on the global parameter
 *                     MetaDataLength, additional bytes containing MetaData (e.g.
 *                     the CAN ID) are appended after the payload data, increasing
 *                     the length accordingly. If neither first/single frame data nor
 *                     MetaData are available, this parameter is set to NULL_PTR.
 *                     TpSduLength: Total length of the N-SDU to be received.
 * Param-Name[out]     bufferSizePtr: Available receive buffer in the receiving module. This
 *                     parameter will be used to compute the Block Size (BS) in the transport protocol module.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK,BUFREQ_E_OVFL)
 *                     BUFREQ_OK: Connection has been accepted. bufferSizePtr
 *                     indicates the available receive buffer; reception is continued.
 *                     If no buffer of the requested size is available, a receive buffer
 *                     size of 0 shall be indicated by bufferSizePtr.
 *                     BUFREQ_E_NOT_OK: Connection has been rejected;
 *                     reception is aborted. bufferSizePtr remains unchanged.
 *                     BUFREQ_E_OVFL: No buffer of the required length can be
 *                     provided; reception is aborted. bufferSizePtr remains unchanged.
 */
/******************************************************************************/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpStartOfReception
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	PduLengthType TpSduLength,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);
/******************************************************************************/
/*
 * Brief               This function is called to provide the received data of an I-PDU segment (N-PDU) to the upper layer.
 *                     Each call to this function provides the next part of the I-PDU data.
 *                     The size of the remaining data is written to the position indicated by bufferSizePtr.
 * ServiceId           0x04
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     info: Provides the source buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     An SduLength of 0 can be used to query the current amount of available buffer in the upper
 *                     layer module. In this case, the SduDataPtr may be a NULL_PTR.
 * Param-Name[out]     bufferSizePtr: Available receive buffer after data has been copied.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType(BUFREQ_OK,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data copied successfully
 *                     BUFREQ_E_NOT_OK: Data was not copied because an error occurred.
 */
/******************************************************************************/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpCopyRxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) bufferSizePtr
);
/******************************************************************************/
/*
 * Brief               Called after an I-PDU has been received via the TP API, the result indicates
 *                     whether the transmission was successful or not.
 * ServiceId           0x05
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the received I-PDU.
 *                     result: Result of the reception.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
extern FUNC(void, PDUR_CODE)
PduR_CanTpRxIndication
(
	PduIdType id,
	Std_ReturnType result
);
/******************************************************************************/
/*
 * Brief               This function is called to acquire the transmit data of an I-PDU segment (N-PDU).
 *                     Each call to this function provides the next part of the I-PDU data unless retry-
 *                     >TpDataState is TP_DATARETRY. In this case the function restarts to copy the
 *                     data beginning at the offset from the current position indicated by retry-
 *                     >TxTpDataCnt. The size of the remaining data is written to the position indicated
 *                     by availableDataPtr.
 * ServiceId           0x07
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     info: Provides the destination buffer (SduDataPtr) and the number of bytes to be copied (SduLength).
 *                     If not enough transmit data is available, no data is copied by the upper layer module and
 *                     BUFREQ_E_BUSY is returned.The lower layer module may retry the call.An SduLength of 0 can be used to
 *                     indicate state changes in the retry parameter or to query the current amount of available data in the
 *                     upper layer module. In this case, the SduDataPtr may be a NULL_PTR.
 *                     retry: This parameter is used to acknowledge transmitted data or to retransmit data after transmission problems.
 *                     If the retry parameter is a NULL_PTR, it indicates that the transmit data can be removed from the buffer immediately
 *                     after it has been copied. Otherwise, the retry parameter must point to a valid RetryInfoType element.
 *                     If TpDataState indicates TP_CONFPENDING, the previously copied data must remain in the TP buffer to be available for
 *                     error recovery.TP_DATACONF indicates that all data that has been copied before this call is confirmed and can be
 *                     removed from the TP buffer. Data copied by this API call is excluded and will be confirmed later.TP_DATARETRY indicates
 *                     that this API call shall copy previously copied data in order to recover from an error. In this case TxTpDataCnt specifies
 *                     the offset in bytes from the current data copy position.
 * Param-Name[out]     availableDataPtr: Indicates the remaining number of bytes that are available in the upper layer module's Tx buffer.
 *                     availableDataPtr can be used by TP modules that support dynamic payload lengths (e.g. FrIsoTp) to determine the size
 *                     of the following CFs.
 * Param-Name[in/out]  None
 * Return              BufReq_ReturnType (BUFREQ_OK,BUFREQ_E_BUSY,BUFREQ_E_NOT_OK)
 *                     BUFREQ_OK: Data has been copied to the transmit buffer completely as requested.
 *                     BUFREQ_E_BUSY: Request could not be fulfilled, because the required amount of Tx data is not available. The lower layer
 *                     module may retry this call later on. No data has been copied.
 *                     BUFREQ_E_NOT_OK: Data has not been copied. Request failed.
 */
/******************************************************************************/
extern FUNC(BufReq_ReturnType, PDUR_CODE)
PduR_CanTpCopyTxData
(
	PduIdType id,
	P2CONST(PduInfoType, AUTOMATIC, PDUR_APPL_DATA) info,
	P2VAR(RetryInfoType, AUTOMATIC, PDUR_APPL_DATA) retry,
	P2VAR(PduLengthType, AUTOMATIC, PDUR_APPL_DATA) availableDataPtr
);
/******************************************************************************/
/*
 * Brief               This function is called after the I-PDU has been transmitted on its network, the
 *                     result indicates whether the transmission was successful or not.
 * ServiceId           0x08
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      id: Identification of the transmitted I-PDU.
 *                     result: Result of the transmission of the I-PDU.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 */
/******************************************************************************/
extern FUNC(void, PDUR_CODE)
PduR_CanTpTxConfirmation(PduIdType id,Std_ReturnType result);
#define PDUR_STOP_SEC_CODE
#include "PduR_MemMap.h"

#endif /*STD_ON == PDUR_ZERO_COST_OPERATION*/

#endif /*STD_ON == PDUR_CANTP_SUPPORT*/

#endif /* end of PDUR_CANTP_H */


