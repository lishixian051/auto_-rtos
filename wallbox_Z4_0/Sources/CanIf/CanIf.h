/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : CanIf.h                                                     **
**                                                                            **
**  Created on  :                                                             **
**  Author      : zhengfei.li                                                 **
**  Vendor      :                                                             **
**  DESCRIPTION : API declaration and type definitions of CANIF               **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/
/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
#ifndef CANIF_H
#define CANIF_H
/*******************************************************************************
**                      Includes                                              **
*******************************************************************************/
#include "CanIf_Cfg.h"
#include "CanIf_Types.h"
/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define CANIF_MODULE_ID            60U
#define CANIF_H_VENDOR_ID          62U
#define CANIF_H_AR_MAJOR_VERSION   4U
#define CANIF_H_AR_MINOR_VERSION   2U
#define CANIF_H_AR_PATCH_VERSION   2U
#define CANIF_H_SW_MAJOR_VERSION   1U
#define CANIF_H_SW_MINOR_VERSION   0U
#define CANIF_H_SW_PATCH_VERSION   1U

#define CANIF_INSTANCE_ID                   (0U)

#if(STD_ON == CANIF_PUBLIC_DEV_ERROR_DETECT)
/*Service ID*/
#define CANIF_INIT_ID                       ((uint8)0x01U)
#define CANIF_SETCONTROLLER_MODE_ID         ((uint8)0x03U)
#define CANIF_GETCONTROLLER_MODE_ID         ((uint8)0x04U)
#define CANIF_TRANSMIT_ID                   ((uint8)0x05U)
#define CANIF_CANCELTRANSMIT_ID             ((uint8)0x18U)
#define CANIF_READRXPDUDATA_ID              ((uint8)0x06U)
#define CANIF_READTXNOTIFSTATUS_ID          ((uint8)0x07U)
#define CANIF_READRXNOTIFSTATUS_ID          ((uint8)0x08U)
#define CANIF_SETPDUMODE_ID                 ((uint8)0x09U)
#define CANIF_GETPDUMODE_ID                 ((uint8)0x0AU)
#define CANIF_GETVERSIONINFO_ID             ((uint8)0x0BU)
#define CANIF_SETDYNAMICTXID_ID             ((uint8)0x0CU)
#define CANIF_SETTRCVMODE_ID                ((uint8)0x0DU)
#define CANIF_GETTRCVMODE_ID                ((uint8)0x0EU)
#define CANIF_GETTRCVWAKEUPREASON_ID        ((uint8)0x0FU)
#define CANIF_SETTRCVWAKEUPMODE_ID          ((uint8)0x10U)
#define CANIF_CHECKWAKEUP_ID                ((uint8)0x11U)
#define CANIF_CHECKVALIDATION_ID            ((uint8)0x12U)
#define CANIF_GETTXCONFIRMATIONSTATE        ((uint8)0x19U)
#define CANIF_CLEARTRCVWUFFLAG_ID           ((uint8)0x1EU)
#define CANIF_CHECKTRCVWAKEFLAG_ID          ((uint8)0x1FU)
#define CANIF_SETBAUDRATE_ID                ((uint8)0x27U)
#define CANIF_SETICOMCONFIGURATION_ID       ((uint8)0x25U)

#define CANIF_TRIGGERTRANSMIT_ID            ((uint8)0x41U)
#define CANIF_TXCONFIRMATION_ID             ((uint8)0x13U)
#define CANIF_RXINDICATION_ID               ((uint8)0x14U)
#define CANIF_CONTROLLERBUSOFF_ID           ((uint8)0x16U)
#define CANIF_CONFIRMPNAVAILABILITY_ID      ((uint8)0x1AU)
#define CANIF_CLEARTRCVWUFFLAGINDICATION_ID    ((uint8)0x20U)
#define CANIF_CHECKTRCVWAKEFLAGINDICATION_ID   ((uint8)0x21U)
#define CANIF_CONTROLLERMODEINDICATION_ID      ((uint8)0x17U)
#define CANIF_TRCVMODEINDICATION_ID            ((uint8)0x22U)
#define CANIF_CURRENTICOMCONFIGURATION_ID      ((uint8)0x26U)

/* Error detection */
#define CANIF_E_PARAM_CANID                 ((uint8)10U)
#define CANIF_E_PARAM_HOH                   ((uint8)12U)
#define CANIF_E_PARAM_LPDU                  ((uint8)13U)
#define CANIF_E_PARAM_CONTROLLER            ((uint8)14U)
#define CANIF_E_PARAM_CONTROLLERID          ((uint8)15U)
#define CANIF_E_PARAM_WAKEUPSOURCE          ((uint8)16U)
#define CANIF_E_PARAM_TRCV                  ((uint8)17U)
#define CANIF_E_PARAM_TRCVMODE              ((uint8)18U)
#define CANIF_E_PARAM_TRCVWAKEUPMODE        ((uint8)19U)
#define CANIF_E_PARAM_CTRLMODE              ((uint8)21U)
#define CANIF_E_PARAM_PDU_MODE              ((uint8)22U)

#define CANIF_E_PARAM_POINTER               ((uint8)20U)
#define CANIF_E_UNINIT                      ((uint8)30U)
#define CANIF_E_INVALID_TXPDUID             ((uint8)50U)
#define CANIF_E_INVALID_RXPDUID             ((uint8)60U)
#define CANIF_E_INVALID_DLC                 ((uint8)61U)
#define CANIF_E_DATA_LENGTH_MISMATCH        ((uint8)62U)
#define CANIF_E_STOPPED                     ((uint8)70U)
#define CANIF_E_NOT_SLEEP                   ((uint8)71U)
#define CANIF_E_INIT_FAILED                 ((uint8)80U)

#endif

/* max extended can id */
#define CANIF_EXTENDED_CANID_MAX            (0x1FFFFFFFU)
/* max standard can id */
#define CANIF_STANDARD_CANID_MAX            (0x7FFU)
/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/
extern CONST(CanIf_ConfigType,CANIF_CONST_PBCFG)CanIf_InitCfgSet;
/* Callout functions with respect to the upper layers. */
extern CONST(CanIf_DispatchConfigType, CANIF_CONST) CanIf_DispatchConfigData;
/*Can Driver API*/
extern CONST(Can_DriverApiType,CANIF_CONST_PBCFG) Can_DriverApi[];
/*Can Trcv API*/
#if (CANIF_TRCVDRIVER_NUMBER > 0)
extern CONST(Can_TrcvApiType,CANIF_CONST_PBCFG) Can_TrcvApi[];
#endif
#if (CANIF_TXBUFFER_NUMBER > 0)
extern VAR(CanIf_TxBufferType, CANIF_VAR) CanIf_TxBuffer[];
#endif
#if (CANIF_RXBUFFER_NUMBER > 0)
extern VAR(CanIf_RxPduBufferType, CANIF_VAR) CanIf_RxBuffer[];
#endif
/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/
#define CANIF_START_SEC_CODE
#include "CanIf_MemMap.h"
#if (STD_ON == CANIF_PUBLIC_VERSION_INFO_API)
/*************************************************************************/
/*
 * Brief               This service returns the version information of the called CAN Interface module.
 * ServiceId           0x0b
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      None
 * Param-Name[out]     VersionInfo: Pointer to where to store the version information of this module.
 * Param-Name[in/out]  None
 * Return              None
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
#if (STD_ON == CANIF_PUBLIC_DEV_ERROR_DETECT)
#define CanIf_GetVersionInfo(VersionInfo) \
    do{\
        if (NULL_PTR == (VersionInfo))\
        { \
            Det_ReportError(CANIF_MODULE_ID, CANIF_INSTANCE_ID, CANIF_GETVERSIONINFO_ID, CANIF_E_PARAM_POINTER);\
        }\
        else\
        {\
            (VersionInfo)->vendorID = CANIF_H_VENDOR_ID; \
            (VersionInfo)->moduleID = CANIF_MODULE_ID; \
            (VersionInfo)->sw_major_version = CANIF_H_SW_MAJOR_VERSION; \
            (VersionInfo)->sw_minor_version = CANIF_H_SW_MINOR_VERSION; \
            (VersionInfo)->sw_patch_version = CANIF_H_SW_PATCH_VERSION; \
        }\
    }while(0)
#else
#define CanIf_GetVersionInfo(VersionInfo) \
    do{\
          (VersionInfo)->vendorID = CANIF_H_VENDOR_ID; \
          (VersionInfo)->moduleID = CANIF_MODULE_ID; \
          (VersionInfo)->sw_major_version = CANIF_H_SW_MAJOR_VERSION; \
          (VersionInfo)->sw_minor_version = CANIF_H_SW_MINOR_VERSION; \
          (VersionInfo)->sw_patch_version = CANIF_H_SW_PATCH_VERSION; \
    }while(0)
#endif /* STD_ON == CANIF_PUBLIC_DEV_ERROR_DETECT */
#endif/* STD_ON == CANIF_PUBLIC_VERSION_INFO_API */
/*************************************************************************/
/*
 * Brief               This service Initializes internal and external interfaces of the CAN Interface for the further processing.
 * ServiceId           0x01
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      ConfigPtr: Pointer to configuration parameter set, used e.g. for post build parameters
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(void, CANIF_CODE)
CanIf_Init(P2CONST(CanIf_ConfigType, AUTOMATIC, CANIF_CONST_PBCFG) ConfigPtr);
/*************************************************************************/
/*
 * Brief               This service calls the corresponding CAN Driver service
 *                     for changing of the CAN controller mode.
 * ServiceId           0x03
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant (Not for the same controller)
 * Param-Name[in]      ControllerId: Abstracted CanIf ControllerId which is assigned
 *                     to a CAN controller, which is requested for mode transition.
 *                     ControllerMode: Requested mode transition
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Controller mode request has been accepted
 *                     E_NOT_OK: Controller mode request has not been accepted
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType, CANIF_CODE)
CanIf_SetControllerMode(uint8 ControllerId, CanIf_ControllerModeType ControllerMode);
/*************************************************************************/
/*
 * Brief               This service reports about the current status of the requested CAN controller.
 * ServiceId           0x04
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      ControllerId: Abstracted CanIf ControllerId which is assigned to a
 *                     CAN controller, which is requested for current operation mode.
 * Param-Name[out]     ControllerModePtr: Pointer to a memory location, where the current
 *                     mode of the CAN controller will be stored.
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Controller mode request has been accepted.
 *                     E_NOT_OK: Controller mode request has not been accepted.
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_GetControllerMode
(
    uint8 ControllerId,
    P2VAR(CanIf_ControllerModeType, AUTOMATIC, CANIF_APPL_DATA)ControllerModePtr
);
/*************************************************************************/
/*
 * Brief               This service initiates a request for transmission of the CAN L-PDU
 *                     specified by the CanTxSduId and CAN related data in the L-SDU structure.
 * ServiceId           0x05
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      CanIfTxSduId: L-SDU handle to be transmitted.This handle specifies the
 *                     corresponding CAN LSDU ID and implicitly the CAN Driver instance as well
 *                     as the corresponding CAN controller device.
 *                     CanIfTxInfoPtr: Pointer to a structure with CAN L-SDU related data:
 *                     DLC and pointer to CAN L-SDU buffer including the MetaData of dynamic L-PDUs.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Transmit request has been accepted
 *                     E_NOT_OK: Transmit request has not been accepted
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType, CANIF_CODE)
CanIf_Transmit
(
    PduIdType CanIfTxSduId,
    P2CONST(PduInfoType, AUTOMATIC, CANIF_APPL_DATA) PduInfoPtr
);
#if(STD_ON == CANIF_PUBLIC_CANCEL_TRANSMIT_SUPPORT)
/*************************************************************************/
/*
 * Brief               This is a dummy method introduced for interface compatibility.
 * ServiceId           0x18
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      CanIfTxSduId: L-SDU handle to be transmitted.This handle specifies the
 *                     corresponding CAN LSDU ID and implicitly the CAN Driver instance as well
 *                     as the corresponding CAN controller device.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     Always return E_OK
 * PreCondition        CANIF_PUBLIC_CANCEL_TRANSMIT_SUPPORT is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType, CANIF_CODE)
CanIf_CancelTransmit(PduIdType CanIfTxSduId);
#endif/*STD_ON == CANIF_PUBLIC_CANCEL_TRANSMIT_SUPPORT*/
#if(STD_ON == CANIF_PUBLIC_READ_RX_PDU_DATA_API)
/*************************************************************************/
/*
 * Brief               This service provides the CAN DLC and the received data of the requested CanIfRxSduId to the calling upper layer.
 * ServiceId           0x06
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      CanIfRxSduId: Receive L-SDU handle specifying the corresponding CAN L-SDU ID and implicitly the CAN
 *                     Driver instance as well as the corresponding CAN controller device.
 * Param-Name[out]     CanIfRxInfoPtr: Pointer to a structure with CAN L-SDU related data:DLC and pointer to CAN L-SDU
 *                     buffer including the MetaData of dynamic L-PDUs.
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Request for L-SDU data has been accepted
 *                     E_NOT_OK: No valid data has been received
 * PreCondition        CANIF_PUBLIC_READ_RX_PDU_DATA_API is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType, CANIF_CODE)
CanIf_ReadRxPduData
(
    PduIdType CanIfRxSduId,
    P2VAR(PduInfoType, AUTOMATIC, CANIF_APPL_DATA) CanIfRxInfoPtr
);
#endif /* STD_ON == CANIF_PUBLIC_READ_RX_PDU_DATA_API*/
#if(STD_ON == CANIF_PUBLIC_READ_TX_PDU_NOTIFY_STATUS_API)
/*************************************************************************/
/*
 * Brief               This service returns the confirmation status (confirmation occurred or
 *                     not) of a specific static or dynamic CAN Tx L-PDU, requested by the CanIfTxSduId.
 * ServiceId           0x07
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      CanIfTxSduId: L-SDU handle to be transmitted.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              CanIf_NotifStatusType:Current confirmation status of the corresponding CAN Tx L-PDU.
 * PreCondition        CANIF_PUBLIC_READ_TX_PDU_NOTIFY_STATUS_API is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(CanIf_NotifStatusType, CANIF_CODE)
CanIf_ReadTxNotifStatus(PduIdType CanIfTxSduId);
#endif /* STD_ON == CANIF_PUBLIC_READ_TX_PDU_NOTIFY_STATUS_API */
#if(STD_ON == CANIF_PUBLIC_READ_RX_PDU_NOTIFY_STATUS_API)
/*************************************************************************/
/*
 * Brief               This service returns the indication status (indication occurred or not) of a
 *                     specific CAN Rx L-PDU, requested by the CanIfRxSduId.
 * ServiceId           0x08
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      CanIfRxSduId: Receive L-SDU handle specifying the corresponding CAN L-SDU ID and implicitly
 *                     the CAN Driver instance as well as the corresponding CAN controller device.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              CanIf_NotifStatusType:Current indication status of the corresponding CAN Rx L-PDU.
 * PreCondition        CANIF_PUBLIC_READ_RX_PDU_NOTIFY_STATUS_API is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(CanIf_NotifStatusType, CANIF_CODE)
CanIf_ReadRxNotifStatus(PduIdType CanIfRxSduId);
#endif /* STD_ON == CANIF_PUBLIC_READ_RX_PDU_NOTIFY_STATUS_API */
/*************************************************************************/
/*
 * Brief               This service sets the requested mode at the L-PDUs of a predefined logical PDU channel.
 * ServiceId           0x09
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      ControllerId: All PDUs of the own ECU connected to the corresponding CanIf ControllerId,
 *                     which is assigned to a physical CAN controller are addressed.
 *                     PduModeRequest: Requested PDU mode change
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Request for mode transition has been accepted.
 *                     E_NOT_OK: Request for mode transition has not been accepted.
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_SetPduMode(uint8 ControllerId, CanIf_PduModeType PduModeRequest);
/*************************************************************************/
/*
 * Brief               This service reports the current mode of a requested PDU channel.
 * ServiceId           0x0a
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant (Not for the same channel)
 * Param-Name[in]      ControllerId: All PDUs of the own ECU connected to the corresponding CanIf
 *                     ControllerId, which is assigned to a physical CAN controller are addressed.
 * Param-Name[out]     PduModePtr: Pointer to a memory location, where the current mode of the
 *                     logical PDU channel will be stored.
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: PDU mode request has been accepted
 *                     E_NOT_OK: PDU mode request has not been accepted
 * PreCondition        None
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_GetPduMode
(
    uint8 ControllerId,
    P2VAR(CanIf_PduModeType, AUTOMATIC, CANIF_APPL_DATA) PduModePtr
);
#if(CANIF_DYNAMIC_TXPDU_NUMBER > 0)
/*************************************************************************/
/*
 * Brief               This service reconfigures the corresponding CAN identifier of the requested CAN L-PDU.
 * ServiceId           0x0c
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      CanIfTxSduId: L-SDU handle to be transmitted.This handle specifies the corresponding
 *                     CAN LSDU ID and implicitly the CAN Driver instance as well as the corresponding CAN controller device.
 *                     CanId: Standard/Extended CAN ID of CAN L-SDU that shall be transmitted as FD or conventional CAN frame.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              None
 * PreCondition        CANIF_DYNAMIC_TXPDU_NUMBER > 0
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(void, CANIF_CODE)
CanIf_SetDynamicTxId(PduIdType CanIfTxSduId, Can_IdType CanId);
#endif /* CANIF_DYNAMIC_TXPDU_NUMBER > 0 */
#if(CANIF_TRCV_NUMBER > 0)
/*************************************************************************/
/*
 * Brief               This service changes the operation mode of the tansceiver TransceiverId,
 *                     via calling the corresponding CAN Transceiver Driver service.
 * ServiceId           0x0d
 * Sync/Async          Asynchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      TransceiverId: Abstracted CanIf TransceiverId, which is assigned to a CAN
 *                     transceiver, which is requested for mode transition
 *                     TransceiverMode: Requested mode transition
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Transceiver mode request has been accepted.
 *                     E_NOT_OK: Transceiver mode request has not been accepted.
 * PreCondition        CANIF_TRCV_NUMBER > 0
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_SetTrcvMode
(
    uint8 TransceiverId,
    CanTrcv_TrcvModeType TransceiverMode
);
/*************************************************************************/
/*
 * Brief               This function invokes CanTrcv_GetOpMode and updates the parameter
 *                     TransceiverModePtr with the value OpMode provided by CanTrcv.
 * ServiceId           0x0e
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      TransceiverId: Abstracted CanIf TransceiverId, which is assigned to a
 *                     CAN transceiver, which is requested for current operation mode.
 * Param-Name[out]     TransceiverModePtr: Requested mode of requested network the Transceiver is connected to.
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Transceiver mode request has been accepted.
 *                     E_NOT_OK: Transceiver mode request has not been accepted.
 * PreCondition        CANIF_TRCV_NUMBER > 0
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_GetTrcvMode
(
    P2VAR(CanTrcv_TrcvModeType, AUTOMATIC, CANIF_APPL_DATA) TransceiverModePtr,
    uint8 TransceiverId
);
/*************************************************************************/
/*
 * Brief               This service returns the reason for the wake up of the transceiver
 *                     TransceiverId, via calling the corresponding CAN Transceiver Driver service.
 * ServiceId           0x0f
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      TransceiverId: Abstracted CanIf TransceiverId, which is assigned to a CAN
 *                     transceiver, which is requested for wake up reason.
 * Param-Name[out]     TrcvWuReasonPtr: provided pointer to where the requested transceiver wake
 *                     up reason shall be returned
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Transceiver wake up reason request has been accepted.
 *                     E_NOT_OK: Transceiver wake up reason request has not been accepted.
 * PreCondition        CANIF_TRCV_NUMBER > 0
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_GetTrcvWakeupReason
(
    uint8 TransceiverId,
    P2VAR(CanTrcv_TrcvWakeupReasonType, AUTOMATIC, CANIF_APPL_DATA)TrcvWuReasonPtr
);
/*************************************************************************/
/*
 * Brief               This function shall call CanTrcv_SetTrcvWakeupMode.
 * ServiceId           0x10
 * Sync/Async          Synchronous
 * Reentrancy          Non Reentrant
 * Param-Name[in]      TransceiverId: Abstracted CanIf TransceiverId, which is assigned to a
 *                     CAN transceiver, which is requested for wake up notification mode transition.
 *                     TrcvWakeupMode: Requested transceiver wake up notification mode
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Will be returned, if the wake up notifications state has been changed to the requested mode.
 *                     E_NOT_OK: Will be returned, if the wake up notifications state change has failed or the parameter is
 *                     out of the allowed range. The previous state has not been changed.
 * PreCondition        CANIF_TRCV_NUMBER > 0
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_SetTrcvWakeupMode
(
    uint8 TransceiverId,
    CanTrcv_TrcvWakeupModeType TrcvWakeupMode
);
#endif/*CANIF_TRCV_NUMBER > 0*/
#if (STD_ON == CANIF_WAKE_UP_SUPPORT)
/*************************************************************************/
/*
 * Brief               This service checks, whether an underlying CAN driver or a CAN
 *                     transceiver driver already signals a wakeup event.
 * ServiceId           0x11
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      WakeupSource: Source device, which initiated the wake up event:CAN controller or CAN transceiver
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Will be returned, if the check wake up request has been accepted
 *                     E_NOT_OK: Will be returned, if the check wake up request has not been accepted
 * PreCondition        CANIF_WAKE_UP_SUPPORT is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_CheckWakeup(EcuM_WakeupSourceType WakeupSource);
#endif/*STD_ON == CANIF_WAKE_UP_SUPPORT*/
#if (STD_ON == CANIF_PUBLIC_WAKEUP_CHECK_VALID_SUPPORT)
/*************************************************************************/
/*
 * Brief               This service is performed to validate a previous wakeup event.
 * ServiceId           0x12
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant
 * Param-Name[in]      WakeupSource: Source device which initiated the wake-up event and
 *                     which has to be validated: CAN controller or CAN transceiver
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Will be returned, if the check validation request has been accepted.
 *                     E_NOT_OK: Will be returned, if the check validation request has not been accepted.
 * PreCondition        CANIF_PUBLIC_WAKEUP_CHECK_VALID_SUPPORT is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_CheckValidation(EcuM_WakeupSourceType WakeupSource);
#endif/*STD_ON == CANIF_PUBLIC_WAKEUP_CHECK_VALID_SUPPORT*/
#if(STD_ON == CANIF_PUBLIC_TX_CONFIRM_POLLING_SUPPORT)
/*************************************************************************/
/*
 * Brief               This service reports, if any TX confirmation has been done for the whole
 *                     CAN controller since the last CAN controller start.
 * ServiceId           0x19
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant (Not for the same controller)
 * Param-Name[in]      ControllerId: Abstracted CanIf ControllerId which is assigned to a CAN controller
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              CanIf_NotifStatusType:Combined TX confirmation status for all TX PDUs of the CAN controller
 * PreCondition        CANIF_PUBLIC_TX_CONFIRM_POLLING_SUPPORT is TRUE
 * CallByAPI           Up layer(CanSM)
 */
/*************************************************************************/
extern FUNC(CanIf_NotifStatusType, CANIF_CODE)
CanIf_GetTxConfirmationState(uint8 ControllerId);
#endif/*STD_ON == CANIF_PUBLIC_TX_CONFIRM_POLLING_SUPPORT*/
#if(STD_ON == CANIF_PUBLIC_PN_SUPPORT)
/*************************************************************************/
/*
 * Brief               Requests the CanIf module to clear the WUF flag of the designated CAN transceiver.
 * ServiceId           0x1e
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant for different CAN transceivers
 * Param-Name[in]      TransceiverId: Abstract CanIf TransceiverId, which is assigned to the designated CAN transceiver.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Request has been accepted
 *                     E_NOT_OK: Request has not been accepted
 * PreCondition        CANIF_PUBLIC_PN_SUPPORT is TRUE
 * CallByAPI           Up layer(CanSM)
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_ClearTrcvWufFlag(uint8 TransceiverId);
/*************************************************************************/
/*
 * Brief               Requests the CanIf module to check the Wake flag of the designated CAN transceiver.
 * ServiceId           0x1f
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant for different CAN transceivers
 * Param-Name[in]      TransceiverId: Abstract CanIf TransceiverId, which is assigned to the designated CAN transceiver.
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Request has been accepted
 *                     E_NOT_OK: Request has not been accepted
 * PreCondition        None
 * CallByAPI           Up layer(CanSM)
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_CheckTrcvWakeFlag(uint8 TransceiverId);
#endif/*STD_ON == CANIF_PUBLIC_PN_SUPPORT*/
#if(STD_ON == CANIF_SET_BAUDRATE_API)
/*************************************************************************/
/*
 * Brief               This service shall set the baud rate configuration of the CAN controller.
 *                     Depending on necessary baud rate modifications the controller might have to reset.
 * ServiceId           0x27
 * Sync/Async          Synchronous
 * Reentrancy          Reentrant for different ControllerIds. Non reentrant for the same ControllerId.
 * Param-Name[in]      ControllerId: Abstract CanIf ControllerId which is assigned to a CAN controller, whose baud rate shall be set.
 *                     BaudRateConfigID: references a baud rate configuration by ID (see CanControllerBaudRateConfigID)
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Service request accepted, setting of (new) baud rate started
 *                     E_NOT_OK: Service request not accepted
 * PreCondition        CANIF_SET_BAUDRATE_API is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_SetBaudrate(uint8 ControllerId,uint16 BaudRateConfigID);
#endif/*STD_ON == CANIF_SET_BAUDRATE_API*/
#if(STD_ON == CANIF_PUBLIC_ICOM_SUPPORT)
/*************************************************************************/
/*
 * Brief               This service shall change the Icom Configuration of a CAN controller to the requested one.
 * ServiceId           0x25
 * Sync/Async          Asynchronous
 * Reentrancy          Reentrant only for different controller Ids
 * Param-Name[in]      ControllerId: Abstracted CanIf Controller Id which is assigned to a CAN controller.
 *                     ConfigurationId: Requested Configuration
 * Param-Name[out]     None
 * Param-Name[in/out]  None
 * Return              Std_ReturnType
 *                     E_OK: Request accepted
 *                     E_NOT_OK: Request denied
 * PreCondition        CANIF_PUBLIC_ICOM_SUPPORT is TRUE
 * CallByAPI           Up layer
 */
/*************************************************************************/
extern FUNC(Std_ReturnType , CANIF_CODE)
CanIf_SetIcomConfiguration(uint8 ControllerId,IcomConfigIdType ConfigurationId);
#endif/*STD_ON == CANIF_PUBLIC_ICOM_SUPPORT*/
#define CANIF_STOP_SEC_CODE
#include "CanIf_MemMap.h"

void User_ControllerBusOff(uint8 ControllerId);

void User_ControllerModeIndication(uint8 ControllerId,CanIf_ControllerModeType ControllerMode);


#endif/* end of CANIF_H */


