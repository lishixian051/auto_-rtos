/******************************************************************************
**                                                                           **
** Copyright (C)    (2016)                                              **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to .        **
** Passing on and copying of this document, and communication                **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  FILENAME   : ComStack_Cfg.h                                              **
**                                                                           **
**  AUTHOR      :                                                            **
**                                                                           **
**  VENDOR      :                                                            **
**                                                                           **
**  DESCRIPTION :This file is partly ECU dependent.                          **
**               Implemented SWS: Communication Stack Types (generated)      **
**                                                                           **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                      **
**                                                                           **
******************************************************************************/
#ifndef COMSTACK_CFG_H
#define COMSTACK_CFG_H

#include "Std_Types.h"

typedef uint16 PduIdType;

#define COMSTACK_PDUID_INVALID  (uint8)0xFF

typedef uint16 PduLengthType;

/*Length of CAN identifier*/
#define COMSTACK_CAN_ID_LENGTH_BIT    32

/*Represents the hardware object handles of a CAN hardware unit. */
//typedef uint8 Can_HwHandleType;

#endif /* COMSTACK_CFG_H */
