/******************************************************************************
**                                                                           **
** Copyright (C)    (2016)                                              **
**                                                                           **
** All rights reserved.                                                      **
**                                                                           **
** This document contains proprietary information belonging to .        **
** Passing on and copying of this document, and communication                **
** of its contents is not permitted without prior written authorization.     **
**                                                                           **
*******************************************************************************
**                                                                           **
**  $FILENAME   : EcuM_Types.h $                                             **
**                                                                           **
**  AUTHOR      :                                                            **
**                                                                           **
**  VENDOR      :                                                            **
**                                                                           **
**  DESCRIPTION :This file is partly ECU dependent.                          **
**                                                                           **
**                                                                           **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                      **
**                                                                           **
******************************************************************************/
#ifndef ECUM_TYPES_H
#define ECUM_TYPES_H
/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "Std_Types.h"

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
/*A service was called prior to initialization*/
#define ECUM_E_UNINIT                          0
/*A function was called which was disabled by configuration*/
#define ECUM_E_SERVICE_DISABLED                1
/*A invalid pointer was passed as an argument*/
#define ECUM_E_NULL_POINTER                    2
/*A parameter was invalid (unspecific)*/
#define ECUM_E_INVALID_PAR                     3
/*A state, passed as an argument to a service, was out of range (specific
 * parameter test)*/
#define ECUM_E_STATE_PAR_OUT_OF_RANGE          4
/*An unknown wakeup source was passed as a parameter to an API*/
#define ECUM_E_UNKNOWN_WAKEUP_SOURCE           5
/*The initialization failed*/
#define ECUM_E_INIT_FAILED                     6
/*The RAM check during wakeup failed*/
#define ECUM_E_RAM_CHECK_FAILED                7
/*Postbuild configuration data is inconsistent*/
#define ECUM_E_CONFIGURATION_DATA_INCONSISTENT 8

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
/*Encode states and sub-states of the ECU Manager module.
 *States shall be encoded in the high-nibble, sub-states in the low-nibble.
 */
typedef uint8 EcuM_StateType;
#define ECUM_SUBSTATE_MASK       (uint8)0x0f
#define ECUM_STATE_STARTUP       (uint8)0x10
#define ECUM_STATE_RUN           (uint8)0x30
#define ECUM_STATE_APP_RUN       (uint8)0x32
#define ECUM_STATE_APP_POST_RUN  (uint8)0x33
#define ECUM_STATE_SHUTDOWN      (uint8)0x40
#define ECUM_STATE_SLEEP         (uint8)0x50

/*Result of the Run Request Protocol sent to BswM*/
typedef uint8 EcuM_RunStatusType;
#define ECUM_RUNSTATUS_UNKNOWN    0
#define ECUM_RUNSTATUS_REQUESTED  (uint8)1
#define ECUM_RUNSTATUS_RELEASED   (uint8)2

/*The bit field provides one bit for each wake up source.
 *In WAKEUP, all bits cleared indicates that no wake up source is known.
 *In STARTUP, all bits cleared indicates that no reason for restart or reset is known.
 *In this case, ECUM_WKSOURCE_RESET shall be assumed.
 *EcuM defines a bit field with 5 pre-defined positions.
 */
typedef uint32 EcuM_WakeupSourceType;
#define ECUM_WKSOURCE_POWER             (uint32)0x00000001
#define ECUM_WKSOURCE_RESET             (uint32)0x00000002
#define ECUM_WKSOURCE_INTERNAL_RESET    (uint32)0x00000004
#define ECUM_WKSOURCE_INTERNAL_WDG      (uint32)0x00000008
#define ECUM_WKSOURCE_EXTERNAL_WDG      (uint32)0x00000010
#define ECUM_WKSOURCE_EXTERNAL_IO       (uint32)0x00000020


/*The type describes the possible states of a wakeup source.*/
typedef uint8 EcuM_WakeupStatusType;
#define ECUM_WKSTATUS_NONE         0
#define ECUM_WKSTATUS_PENDING      (uint8)1
#define ECUM_WKSTATUS_VALIDATED    (uint8)2
#define ECUM_WKSTATUS_EXPIRED      (uint8)3
#define ECUM_WKSTATUS_ENABLED      (uint8)6

/*This type describes the reset mechanisms supported by the ECU State Manager.
 *It can be extended by configuration.*/
typedef uint8 EcuM_ResetType;
#define ECUM_RESET_MCU    0
#define ECUM_RESET_WDG    (uint8)1
#define ECUM_RESET_IO     (uint8)2
#if 0
/*********************Configuration Types***************************/
/*Post build configuration root type definition*/
typedef struct
{
	uint32 configConsistencyHash; /*Configuration consistency hash code for
	                              pre-compile and link time configure*/
	AppModeType defaultAppMode;   /*EcuMDefaultAppMode*/
	EcuM_ShutdownTargetType defaultShutdownTgt;  /*EcuMDefaultState*/
	EcuM_ShutdownModeType   defaultShutdownMode; /*EcuMDefaultResetMode or
	                                               EcuMDefaultSleepMode*/
	EcuM_GenBSWPbCfgType modulePBCfg; /*PB CFG data pointer for modules*/
} EcuM_ConfigType;
#endif
#endif /* ECUM_TYPES_H */
