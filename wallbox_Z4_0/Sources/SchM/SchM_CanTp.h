/*******************************************************************************
**                                                                            **
** Copyright (C)    (2016)                                               **
**                                                                            **
** All rights reserved.                                                       **
**                                                                            **
** This document contains proprietary information belonging to .         **
** Passing on and copying of this document, and communication                 **
** of its contents is not permitted without prior written authorization.      **
**                                                                            **
********************************************************************************
**                                                                            **
**  FILENAME    : SchM_CanTp.h                                                **
**                                                                            **
**  Created on  :                                                             **
**  Author      : stanleyluo                                                  **
**  Vendor      :                                                             **
**  DESCRIPTION : Critical entity and schedule entity declaration of CANTP    **
**                                                                            **
**  SPECIFICATION(S) :   AUTOSAR classic Platform 4.2.2                       **
**                                                                            **
*******************************************************************************/


/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/




#ifndef SCHM_CANTP_H
#define SCHM_CANTP_H

/*=======[V E R S I O N  I N F O R M A T I O N]===============================*/
#define SCHM_CANTP_H_AR_MAJOR_VERSION  2
#define SCHM_CANTP_H_AR_MINOR_VERSION  3
#define SCHM_CANTP_H_AR_PATCH_VERSION  0
#define SCHM_CANTP_H_SW_MAJOR_VERSION  1
#define SCHM_CANTP_H_SW_MINOR_VERSION  0
#define SCHM_CANTP_H_SW_PATCH_VERSION  0
/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define CANTP_EXCLUSIVE_AREA_CHANNEL 0
#define CANTP_EXCLUSIVE_AREA_STATE 1

#define SchM_Enter_CanTp(InstanceID, ExclusiceArea)
#define SchM_Exit_CanTp(InstanceID, ExclusiceArea)



/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/



/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/



/*******************************************************************************
**                      Global Functions                                      **
*******************************************************************************/


#endif /* SCHM_CANTP_H */
