################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../SDK/rtos/FreeRTOS_PA/Source/croutine.c" \
"../SDK/rtos/FreeRTOS_PA/Source/event_groups.c" \
"../SDK/rtos/FreeRTOS_PA/Source/list.c" \
"../SDK/rtos/FreeRTOS_PA/Source/queue.c" \
"../SDK/rtos/FreeRTOS_PA/Source/stream_buffer.c" \
"../SDK/rtos/FreeRTOS_PA/Source/tasks.c" \
"../SDK/rtos/FreeRTOS_PA/Source/timers.c" \

C_SRCS += \
../SDK/rtos/FreeRTOS_PA/Source/croutine.c \
../SDK/rtos/FreeRTOS_PA/Source/event_groups.c \
../SDK/rtos/FreeRTOS_PA/Source/list.c \
../SDK/rtos/FreeRTOS_PA/Source/queue.c \
../SDK/rtos/FreeRTOS_PA/Source/stream_buffer.c \
../SDK/rtos/FreeRTOS_PA/Source/tasks.c \
../SDK/rtos/FreeRTOS_PA/Source/timers.c \

OBJS_OS_FORMAT += \
./SDK/rtos/FreeRTOS_PA/Source/croutine.o \
./SDK/rtos/FreeRTOS_PA/Source/event_groups.o \
./SDK/rtos/FreeRTOS_PA/Source/list.o \
./SDK/rtos/FreeRTOS_PA/Source/queue.o \
./SDK/rtos/FreeRTOS_PA/Source/stream_buffer.o \
./SDK/rtos/FreeRTOS_PA/Source/tasks.o \
./SDK/rtos/FreeRTOS_PA/Source/timers.o \

C_DEPS_QUOTED += \
"./SDK/rtos/FreeRTOS_PA/Source/croutine.d" \
"./SDK/rtos/FreeRTOS_PA/Source/event_groups.d" \
"./SDK/rtos/FreeRTOS_PA/Source/list.d" \
"./SDK/rtos/FreeRTOS_PA/Source/queue.d" \
"./SDK/rtos/FreeRTOS_PA/Source/stream_buffer.d" \
"./SDK/rtos/FreeRTOS_PA/Source/tasks.d" \
"./SDK/rtos/FreeRTOS_PA/Source/timers.d" \

OBJS += \
./SDK/rtos/FreeRTOS_PA/Source/croutine.o \
./SDK/rtos/FreeRTOS_PA/Source/event_groups.o \
./SDK/rtos/FreeRTOS_PA/Source/list.o \
./SDK/rtos/FreeRTOS_PA/Source/queue.o \
./SDK/rtos/FreeRTOS_PA/Source/stream_buffer.o \
./SDK/rtos/FreeRTOS_PA/Source/tasks.o \
./SDK/rtos/FreeRTOS_PA/Source/timers.o \

OBJS_QUOTED += \
"./SDK/rtos/FreeRTOS_PA/Source/croutine.o" \
"./SDK/rtos/FreeRTOS_PA/Source/event_groups.o" \
"./SDK/rtos/FreeRTOS_PA/Source/list.o" \
"./SDK/rtos/FreeRTOS_PA/Source/queue.o" \
"./SDK/rtos/FreeRTOS_PA/Source/stream_buffer.o" \
"./SDK/rtos/FreeRTOS_PA/Source/tasks.o" \
"./SDK/rtos/FreeRTOS_PA/Source/timers.o" \

C_DEPS += \
./SDK/rtos/FreeRTOS_PA/Source/croutine.d \
./SDK/rtos/FreeRTOS_PA/Source/event_groups.d \
./SDK/rtos/FreeRTOS_PA/Source/list.d \
./SDK/rtos/FreeRTOS_PA/Source/queue.d \
./SDK/rtos/FreeRTOS_PA/Source/stream_buffer.d \
./SDK/rtos/FreeRTOS_PA/Source/tasks.d \
./SDK/rtos/FreeRTOS_PA/Source/timers.d \


# Each subdirectory must supply rules for building sources it contributes
SDK/rtos/FreeRTOS_PA/Source/croutine.o: ../SDK/rtos/FreeRTOS_PA/Source/croutine.c
	@echo 'Building file: $<'
	@echo 'Executing target #47 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/croutine.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/croutine.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/event_groups.o: ../SDK/rtos/FreeRTOS_PA/Source/event_groups.c
	@echo 'Building file: $<'
	@echo 'Executing target #48 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/event_groups.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/event_groups.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/list.o: ../SDK/rtos/FreeRTOS_PA/Source/list.c
	@echo 'Building file: $<'
	@echo 'Executing target #49 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/list.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/list.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/queue.o: ../SDK/rtos/FreeRTOS_PA/Source/queue.c
	@echo 'Building file: $<'
	@echo 'Executing target #50 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/queue.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/queue.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/stream_buffer.o: ../SDK/rtos/FreeRTOS_PA/Source/stream_buffer.c
	@echo 'Building file: $<'
	@echo 'Executing target #51 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/stream_buffer.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/stream_buffer.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/tasks.o: ../SDK/rtos/FreeRTOS_PA/Source/tasks.c
	@echo 'Building file: $<'
	@echo 'Executing target #52 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/tasks.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/tasks.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/timers.o: ../SDK/rtos/FreeRTOS_PA/Source/timers.c
	@echo 'Building file: $<'
	@echo 'Executing target #53 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/timers.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/timers.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


