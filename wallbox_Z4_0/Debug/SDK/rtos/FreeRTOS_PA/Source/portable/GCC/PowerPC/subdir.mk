################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.c" \
"../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.c" \

S_SRCS_QUOTED += \
"../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.s" \

S_SRCS += \
../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.s \

C_SRCS += \
../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.c \
../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.c \

OBJS_OS_FORMAT += \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.o \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.o \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.o \

C_DEPS_QUOTED += \
"./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.d" \
"./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.d" \

OBJS += \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.o \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.o \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.o \

OBJS_QUOTED += \
"./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.o" \
"./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.o" \
"./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.o" \

C_DEPS += \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.d \
./SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.d \


# Each subdirectory must supply rules for building sources it contributes
SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.o: ../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.c
	@echo 'Building file: $<'
	@echo 'Executing target #54 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/port.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.o: ../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.s
	@echo 'Building file: $<'
	@echo 'Executing target #55 $<'
	@echo 'Invoking: Standard S32DS Assembler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.args" -c -o "SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/portasm.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.o: ../SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.c
	@echo 'Building file: $<'
	@echo 'Executing target #56 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "SDK/rtos/FreeRTOS_PA/Source/portable/GCC/PowerPC/porttimer.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


