################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Core1Src/Bsp/DevCp/dev_cp.c" \

C_SRCS += \
../Core1Src/Bsp/DevCp/dev_cp.c \

OBJS_OS_FORMAT += \
./Core1Src/Bsp/DevCp/dev_cp.o \

C_DEPS_QUOTED += \
"./Core1Src/Bsp/DevCp/dev_cp.d" \

OBJS += \
./Core1Src/Bsp/DevCp/dev_cp.o \

OBJS_QUOTED += \
"./Core1Src/Bsp/DevCp/dev_cp.o" \

C_DEPS += \
./Core1Src/Bsp/DevCp/dev_cp.d \


# Each subdirectory must supply rules for building sources it contributes
Core1Src/Bsp/DevCp/dev_cp.o: ../Core1Src/Bsp/DevCp/dev_cp.c
	@echo 'Building file: $<'
	@echo 'Executing target #1 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Core1Src/Bsp/DevCp/dev_cp.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Core1Src/Bsp/DevCp/dev_cp.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


