################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Core1Src/System/os/src/TskTest.c" \
"../Core1Src/System/os/src/rtos.c" \
"../Core1Src/System/os/src/rtos_config.c" \

C_SRCS += \
../Core1Src/System/os/src/TskTest.c \
../Core1Src/System/os/src/rtos.c \
../Core1Src/System/os/src/rtos_config.c \

OBJS_OS_FORMAT += \
./Core1Src/System/os/src/TskTest.o \
./Core1Src/System/os/src/rtos.o \
./Core1Src/System/os/src/rtos_config.o \

C_DEPS_QUOTED += \
"./Core1Src/System/os/src/TskTest.d" \
"./Core1Src/System/os/src/rtos.d" \
"./Core1Src/System/os/src/rtos_config.d" \

OBJS += \
./Core1Src/System/os/src/TskTest.o \
./Core1Src/System/os/src/rtos.o \
./Core1Src/System/os/src/rtos_config.o \

OBJS_QUOTED += \
"./Core1Src/System/os/src/TskTest.o" \
"./Core1Src/System/os/src/rtos.o" \
"./Core1Src/System/os/src/rtos_config.o" \

C_DEPS += \
./Core1Src/System/os/src/TskTest.d \
./Core1Src/System/os/src/rtos.d \
./Core1Src/System/os/src/rtos_config.d \


# Each subdirectory must supply rules for building sources it contributes
Core1Src/System/os/src/TskTest.o: ../Core1Src/System/os/src/TskTest.c
	@echo 'Building file: $<'
	@echo 'Executing target #5 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Core1Src/System/os/src/TskTest.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Core1Src/System/os/src/TskTest.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Core1Src/System/os/src/rtos.o: ../Core1Src/System/os/src/rtos.c
	@echo 'Building file: $<'
	@echo 'Executing target #6 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Core1Src/System/os/src/rtos.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Core1Src/System/os/src/rtos.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Core1Src/System/os/src/rtos_config.o: ../Core1Src/System/os/src/rtos_config.c
	@echo 'Building file: $<'
	@echo 'Executing target #7 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Core1Src/System/os/src/rtos_config.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Core1Src/System/os/src/rtos_config.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


