################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Generated_Code/Cpu.c" \
"../Generated_Code/adConv0.c" \
"../Generated_Code/adConv1.c" \
"../Generated_Code/bctu.c" \
"../Generated_Code/clockMan.c" \
"../Generated_Code/dmaController.c" \
"../Generated_Code/dspi0.c" \
"../Generated_Code/eMIOS_Ic1.c" \
"../Generated_Code/eMIOS_Mc1.c" \
"../Generated_Code/eMIOS_Mc2.c" \
"../Generated_Code/eMIOS_Pwm1.c" \
"../Generated_Code/eMIOS_Pwm2.c" \
"../Generated_Code/linflexd_uart.c" \
"../Generated_Code/pin_mux.c" \
"../Generated_Code/pit.c" \

C_SRCS += \
../Generated_Code/Cpu.c \
../Generated_Code/adConv0.c \
../Generated_Code/adConv1.c \
../Generated_Code/bctu.c \
../Generated_Code/clockMan.c \
../Generated_Code/dmaController.c \
../Generated_Code/dspi0.c \
../Generated_Code/eMIOS_Ic1.c \
../Generated_Code/eMIOS_Mc1.c \
../Generated_Code/eMIOS_Mc2.c \
../Generated_Code/eMIOS_Pwm1.c \
../Generated_Code/eMIOS_Pwm2.c \
../Generated_Code/linflexd_uart.c \
../Generated_Code/pin_mux.c \
../Generated_Code/pit.c \

OBJS_OS_FORMAT += \
./Generated_Code/Cpu.o \
./Generated_Code/adConv0.o \
./Generated_Code/adConv1.o \
./Generated_Code/bctu.o \
./Generated_Code/clockMan.o \
./Generated_Code/dmaController.o \
./Generated_Code/dspi0.o \
./Generated_Code/eMIOS_Ic1.o \
./Generated_Code/eMIOS_Mc1.o \
./Generated_Code/eMIOS_Mc2.o \
./Generated_Code/eMIOS_Pwm1.o \
./Generated_Code/eMIOS_Pwm2.o \
./Generated_Code/linflexd_uart.o \
./Generated_Code/pin_mux.o \
./Generated_Code/pit.o \

C_DEPS_QUOTED += \
"./Generated_Code/Cpu.d" \
"./Generated_Code/adConv0.d" \
"./Generated_Code/adConv1.d" \
"./Generated_Code/bctu.d" \
"./Generated_Code/clockMan.d" \
"./Generated_Code/dmaController.d" \
"./Generated_Code/dspi0.d" \
"./Generated_Code/eMIOS_Ic1.d" \
"./Generated_Code/eMIOS_Mc1.d" \
"./Generated_Code/eMIOS_Mc2.d" \
"./Generated_Code/eMIOS_Pwm1.d" \
"./Generated_Code/eMIOS_Pwm2.d" \
"./Generated_Code/linflexd_uart.d" \
"./Generated_Code/pin_mux.d" \
"./Generated_Code/pit.d" \

OBJS += \
./Generated_Code/Cpu.o \
./Generated_Code/adConv0.o \
./Generated_Code/adConv1.o \
./Generated_Code/bctu.o \
./Generated_Code/clockMan.o \
./Generated_Code/dmaController.o \
./Generated_Code/dspi0.o \
./Generated_Code/eMIOS_Ic1.o \
./Generated_Code/eMIOS_Mc1.o \
./Generated_Code/eMIOS_Mc2.o \
./Generated_Code/eMIOS_Pwm1.o \
./Generated_Code/eMIOS_Pwm2.o \
./Generated_Code/linflexd_uart.o \
./Generated_Code/pin_mux.o \
./Generated_Code/pit.o \

OBJS_QUOTED += \
"./Generated_Code/Cpu.o" \
"./Generated_Code/adConv0.o" \
"./Generated_Code/adConv1.o" \
"./Generated_Code/bctu.o" \
"./Generated_Code/clockMan.o" \
"./Generated_Code/dmaController.o" \
"./Generated_Code/dspi0.o" \
"./Generated_Code/eMIOS_Ic1.o" \
"./Generated_Code/eMIOS_Mc1.o" \
"./Generated_Code/eMIOS_Mc2.o" \
"./Generated_Code/eMIOS_Pwm1.o" \
"./Generated_Code/eMIOS_Pwm2.o" \
"./Generated_Code/linflexd_uart.o" \
"./Generated_Code/pin_mux.o" \
"./Generated_Code/pit.o" \

C_DEPS += \
./Generated_Code/Cpu.d \
./Generated_Code/adConv0.d \
./Generated_Code/adConv1.d \
./Generated_Code/bctu.d \
./Generated_Code/clockMan.d \
./Generated_Code/dmaController.d \
./Generated_Code/dspi0.d \
./Generated_Code/eMIOS_Ic1.d \
./Generated_Code/eMIOS_Mc1.d \
./Generated_Code/eMIOS_Mc2.d \
./Generated_Code/eMIOS_Pwm1.d \
./Generated_Code/eMIOS_Pwm2.d \
./Generated_Code/linflexd_uart.d \
./Generated_Code/pin_mux.d \
./Generated_Code/pit.d \


# Each subdirectory must supply rules for building sources it contributes
Generated_Code/Cpu.o: ../Generated_Code/Cpu.c
	@echo 'Building file: $<'
	@echo 'Executing target #8 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/Cpu.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/Cpu.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/adConv0.o: ../Generated_Code/adConv0.c
	@echo 'Building file: $<'
	@echo 'Executing target #9 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/adConv0.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/adConv0.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/adConv1.o: ../Generated_Code/adConv1.c
	@echo 'Building file: $<'
	@echo 'Executing target #10 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/adConv1.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/adConv1.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/bctu.o: ../Generated_Code/bctu.c
	@echo 'Building file: $<'
	@echo 'Executing target #11 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/bctu.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/bctu.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/clockMan.o: ../Generated_Code/clockMan.c
	@echo 'Building file: $<'
	@echo 'Executing target #12 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/clockMan.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/clockMan.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/dmaController.o: ../Generated_Code/dmaController.c
	@echo 'Building file: $<'
	@echo 'Executing target #13 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/dmaController.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/dmaController.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/dspi0.o: ../Generated_Code/dspi0.c
	@echo 'Building file: $<'
	@echo 'Executing target #14 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/dspi0.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/dspi0.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/eMIOS_Ic1.o: ../Generated_Code/eMIOS_Ic1.c
	@echo 'Building file: $<'
	@echo 'Executing target #15 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/eMIOS_Ic1.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/eMIOS_Ic1.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/eMIOS_Mc1.o: ../Generated_Code/eMIOS_Mc1.c
	@echo 'Building file: $<'
	@echo 'Executing target #16 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/eMIOS_Mc1.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/eMIOS_Mc1.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/eMIOS_Mc2.o: ../Generated_Code/eMIOS_Mc2.c
	@echo 'Building file: $<'
	@echo 'Executing target #17 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/eMIOS_Mc2.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/eMIOS_Mc2.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/eMIOS_Pwm1.o: ../Generated_Code/eMIOS_Pwm1.c
	@echo 'Building file: $<'
	@echo 'Executing target #18 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/eMIOS_Pwm1.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/eMIOS_Pwm1.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/eMIOS_Pwm2.o: ../Generated_Code/eMIOS_Pwm2.c
	@echo 'Building file: $<'
	@echo 'Executing target #19 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/eMIOS_Pwm2.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/eMIOS_Pwm2.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/linflexd_uart.o: ../Generated_Code/linflexd_uart.c
	@echo 'Building file: $<'
	@echo 'Executing target #20 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/linflexd_uart.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/linflexd_uart.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/pin_mux.o: ../Generated_Code/pin_mux.c
	@echo 'Building file: $<'
	@echo 'Executing target #21 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/pin_mux.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/pin_mux.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Generated_Code/pit.o: ../Generated_Code/pit.c
	@echo 'Building file: $<'
	@echo 'Executing target #22 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Generated_Code/pit.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Generated_Code/pit.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


