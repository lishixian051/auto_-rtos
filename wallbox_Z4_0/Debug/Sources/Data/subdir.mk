################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Data/XcpMeasuredata.c" \
"../Sources/Data/Xcp_Cal.c" \
"../Sources/Data/Xcp_Caldata.c" \
"../Sources/Data/Xcp_test.c" \

C_SRCS += \
../Sources/Data/XcpMeasuredata.c \
../Sources/Data/Xcp_Cal.c \
../Sources/Data/Xcp_Caldata.c \
../Sources/Data/Xcp_test.c \

OBJS_OS_FORMAT += \
./Sources/Data/XcpMeasuredata.o \
./Sources/Data/Xcp_Cal.o \
./Sources/Data/Xcp_Caldata.o \
./Sources/Data/Xcp_test.o \

C_DEPS_QUOTED += \
"./Sources/Data/XcpMeasuredata.d" \
"./Sources/Data/Xcp_Cal.d" \
"./Sources/Data/Xcp_Caldata.d" \
"./Sources/Data/Xcp_test.d" \

OBJS += \
./Sources/Data/XcpMeasuredata.o \
./Sources/Data/Xcp_Cal.o \
./Sources/Data/Xcp_Caldata.o \
./Sources/Data/Xcp_test.o \

OBJS_QUOTED += \
"./Sources/Data/XcpMeasuredata.o" \
"./Sources/Data/Xcp_Cal.o" \
"./Sources/Data/Xcp_Caldata.o" \
"./Sources/Data/Xcp_test.o" \

C_DEPS += \
./Sources/Data/XcpMeasuredata.d \
./Sources/Data/Xcp_Cal.d \
./Sources/Data/Xcp_Caldata.d \
./Sources/Data/Xcp_test.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Data/XcpMeasuredata.o: ../Sources/Data/XcpMeasuredata.c
	@echo 'Building file: $<'
	@echo 'Executing target #76 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Data/XcpMeasuredata.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Data/XcpMeasuredata.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Data/Xcp_Cal.o: ../Sources/Data/Xcp_Cal.c
	@echo 'Building file: $<'
	@echo 'Executing target #77 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Data/Xcp_Cal.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Data/Xcp_Cal.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Data/Xcp_Caldata.o: ../Sources/Data/Xcp_Caldata.c
	@echo 'Building file: $<'
	@echo 'Executing target #78 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Data/Xcp_Caldata.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Data/Xcp_Caldata.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Data/Xcp_test.o: ../Sources/Data/Xcp_test.c
	@echo 'Building file: $<'
	@echo 'Executing target #79 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Data/Xcp_test.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Data/Xcp_test.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


