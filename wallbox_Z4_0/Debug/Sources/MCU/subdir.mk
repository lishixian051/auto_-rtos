################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/MCU/Mcu.c" \

C_SRCS += \
../Sources/MCU/Mcu.c \

OBJS_OS_FORMAT += \
./Sources/MCU/Mcu.o \

C_DEPS_QUOTED += \
"./Sources/MCU/Mcu.d" \

OBJS += \
./Sources/MCU/Mcu.o \

OBJS_QUOTED += \
"./Sources/MCU/Mcu.o" \

C_DEPS += \
./Sources/MCU/Mcu.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/MCU/Mcu.o: ../Sources/MCU/Mcu.c
	@echo 'Building file: $<'
	@echo 'Executing target #81 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/MCU/Mcu.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/MCU/Mcu.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


