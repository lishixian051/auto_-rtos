################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Can/Can.c" \
"../Sources/Can/Can_Hw.c" \
"../Sources/Can/Can_Irq.c" \

C_SRCS += \
../Sources/Can/Can.c \
../Sources/Can/Can_Hw.c \
../Sources/Can/Can_Irq.c \

OBJS_OS_FORMAT += \
./Sources/Can/Can.o \
./Sources/Can/Can_Hw.o \
./Sources/Can/Can_Irq.o \

C_DEPS_QUOTED += \
"./Sources/Can/Can.d" \
"./Sources/Can/Can_Hw.d" \
"./Sources/Can/Can_Irq.d" \

OBJS += \
./Sources/Can/Can.o \
./Sources/Can/Can_Hw.o \
./Sources/Can/Can_Irq.o \

OBJS_QUOTED += \
"./Sources/Can/Can.o" \
"./Sources/Can/Can_Hw.o" \
"./Sources/Can/Can_Irq.o" \

C_DEPS += \
./Sources/Can/Can.d \
./Sources/Can/Can_Hw.d \
./Sources/Can/Can_Irq.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Can/Can.o: ../Sources/Can/Can.c
	@echo 'Building file: $<'
	@echo 'Executing target #59 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Can/Can.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Can/Can.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Can/Can_Hw.o: ../Sources/Can/Can_Hw.c
	@echo 'Building file: $<'
	@echo 'Executing target #60 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Can/Can_Hw.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Can/Can_Hw.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Can/Can_Irq.o: ../Sources/Can/Can_Irq.c
	@echo 'Building file: $<'
	@echo 'Executing target #61 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Can/Can_Irq.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Can/Can_Irq.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


