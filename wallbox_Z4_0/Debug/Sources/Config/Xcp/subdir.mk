################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Config/Xcp/Xcp_Cfg.c" \
"../Sources/Config/Xcp/Xcp_PBcfg.c" \

C_SRCS += \
../Sources/Config/Xcp/Xcp_Cfg.c \
../Sources/Config/Xcp/Xcp_PBcfg.c \

OBJS_OS_FORMAT += \
./Sources/Config/Xcp/Xcp_Cfg.o \
./Sources/Config/Xcp/Xcp_PBcfg.o \

C_DEPS_QUOTED += \
"./Sources/Config/Xcp/Xcp_Cfg.d" \
"./Sources/Config/Xcp/Xcp_PBcfg.d" \

OBJS += \
./Sources/Config/Xcp/Xcp_Cfg.o \
./Sources/Config/Xcp/Xcp_PBcfg.o \

OBJS_QUOTED += \
"./Sources/Config/Xcp/Xcp_Cfg.o" \
"./Sources/Config/Xcp/Xcp_PBcfg.o" \

C_DEPS += \
./Sources/Config/Xcp/Xcp_Cfg.d \
./Sources/Config/Xcp/Xcp_PBcfg.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Config/Xcp/Xcp_Cfg.o: ../Sources/Config/Xcp/Xcp_Cfg.c
	@echo 'Building file: $<'
	@echo 'Executing target #74 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/Xcp/Xcp_Cfg.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/Xcp/Xcp_Cfg.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Config/Xcp/Xcp_PBcfg.o: ../Sources/Config/Xcp/Xcp_PBcfg.c
	@echo 'Building file: $<'
	@echo 'Executing target #75 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/Xcp/Xcp_PBcfg.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/Xcp/Xcp_PBcfg.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


