################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Config/UDS/Dcm/Dcm_Callout.c" \
"../Sources/Config/UDS/Dcm/Dcm_Cfg.c" \
"../Sources/Config/UDS/Dcm/Rte_Dcm.c" \

C_SRCS += \
../Sources/Config/UDS/Dcm/Dcm_Callout.c \
../Sources/Config/UDS/Dcm/Dcm_Cfg.c \
../Sources/Config/UDS/Dcm/Rte_Dcm.c \

OBJS_OS_FORMAT += \
./Sources/Config/UDS/Dcm/Dcm_Callout.o \
./Sources/Config/UDS/Dcm/Dcm_Cfg.o \
./Sources/Config/UDS/Dcm/Rte_Dcm.o \

C_DEPS_QUOTED += \
"./Sources/Config/UDS/Dcm/Dcm_Callout.d" \
"./Sources/Config/UDS/Dcm/Dcm_Cfg.d" \
"./Sources/Config/UDS/Dcm/Rte_Dcm.d" \

OBJS += \
./Sources/Config/UDS/Dcm/Dcm_Callout.o \
./Sources/Config/UDS/Dcm/Dcm_Cfg.o \
./Sources/Config/UDS/Dcm/Rte_Dcm.o \

OBJS_QUOTED += \
"./Sources/Config/UDS/Dcm/Dcm_Callout.o" \
"./Sources/Config/UDS/Dcm/Dcm_Cfg.o" \
"./Sources/Config/UDS/Dcm/Rte_Dcm.o" \

C_DEPS += \
./Sources/Config/UDS/Dcm/Dcm_Callout.d \
./Sources/Config/UDS/Dcm/Dcm_Cfg.d \
./Sources/Config/UDS/Dcm/Rte_Dcm.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Config/UDS/Dcm/Dcm_Callout.o: ../Sources/Config/UDS/Dcm/Dcm_Callout.c
	@echo 'Building file: $<'
	@echo 'Executing target #68 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/UDS/Dcm/Dcm_Callout.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/UDS/Dcm/Dcm_Callout.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Config/UDS/Dcm/Dcm_Cfg.o: ../Sources/Config/UDS/Dcm/Dcm_Cfg.c
	@echo 'Building file: $<'
	@echo 'Executing target #69 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/UDS/Dcm/Dcm_Cfg.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/UDS/Dcm/Dcm_Cfg.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Config/UDS/Dcm/Rte_Dcm.o: ../Sources/Config/UDS/Dcm/Rte_Dcm.c
	@echo 'Building file: $<'
	@echo 'Executing target #70 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/UDS/Dcm/Rte_Dcm.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/UDS/Dcm/Rte_Dcm.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


