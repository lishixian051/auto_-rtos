################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Config/UDS/Dem/Dem_Cfg.c" \
"../Sources/Config/UDS/Dem/Rte_Dem.c" \

C_SRCS += \
../Sources/Config/UDS/Dem/Dem_Cfg.c \
../Sources/Config/UDS/Dem/Rte_Dem.c \

OBJS_OS_FORMAT += \
./Sources/Config/UDS/Dem/Dem_Cfg.o \
./Sources/Config/UDS/Dem/Rte_Dem.o \

C_DEPS_QUOTED += \
"./Sources/Config/UDS/Dem/Dem_Cfg.d" \
"./Sources/Config/UDS/Dem/Rte_Dem.d" \

OBJS += \
./Sources/Config/UDS/Dem/Dem_Cfg.o \
./Sources/Config/UDS/Dem/Rte_Dem.o \

OBJS_QUOTED += \
"./Sources/Config/UDS/Dem/Dem_Cfg.o" \
"./Sources/Config/UDS/Dem/Rte_Dem.o" \

C_DEPS += \
./Sources/Config/UDS/Dem/Dem_Cfg.d \
./Sources/Config/UDS/Dem/Rte_Dem.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Config/UDS/Dem/Dem_Cfg.o: ../Sources/Config/UDS/Dem/Dem_Cfg.c
	@echo 'Building file: $<'
	@echo 'Executing target #71 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/UDS/Dem/Dem_Cfg.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/UDS/Dem/Dem_Cfg.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Config/UDS/Dem/Rte_Dem.o: ../Sources/Config/UDS/Dem/Rte_Dem.c
	@echo 'Building file: $<'
	@echo 'Executing target #72 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Config/UDS/Dem/Rte_Dem.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Config/UDS/Dem/Rte_Dem.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


