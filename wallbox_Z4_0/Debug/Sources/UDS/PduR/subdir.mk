################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/PduR/PduR.c" \
"../Sources/UDS/PduR/PduR_Internal.c" \

C_SRCS += \
../Sources/UDS/PduR/PduR.c \
../Sources/UDS/PduR/PduR_Internal.c \

OBJS_OS_FORMAT += \
./Sources/UDS/PduR/PduR.o \
./Sources/UDS/PduR/PduR_Internal.o \

C_DEPS_QUOTED += \
"./Sources/UDS/PduR/PduR.d" \
"./Sources/UDS/PduR/PduR_Internal.d" \

OBJS += \
./Sources/UDS/PduR/PduR.o \
./Sources/UDS/PduR/PduR_Internal.o \

OBJS_QUOTED += \
"./Sources/UDS/PduR/PduR.o" \
"./Sources/UDS/PduR/PduR_Internal.o" \

C_DEPS += \
./Sources/UDS/PduR/PduR.d \
./Sources/UDS/PduR/PduR_Internal.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/PduR/PduR.o: ../Sources/UDS/PduR/PduR.c
	@echo 'Building file: $<'
	@echo 'Executing target #130 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/PduR/PduR.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/PduR/PduR.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/PduR/PduR_Internal.o: ../Sources/UDS/PduR/PduR_Internal.c
	@echo 'Building file: $<'
	@echo 'Executing target #131 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/PduR/PduR_Internal.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/PduR/PduR_Internal.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


