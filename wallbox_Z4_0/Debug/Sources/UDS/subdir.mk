################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/Dem_Fls.c" \
"../Sources/UDS/Rte_Diag.c" \

C_SRCS += \
../Sources/UDS/Dem_Fls.c \
../Sources/UDS/Rte_Diag.c \

OBJS_OS_FORMAT += \
./Sources/UDS/Dem_Fls.o \
./Sources/UDS/Rte_Diag.o \

C_DEPS_QUOTED += \
"./Sources/UDS/Dem_Fls.d" \
"./Sources/UDS/Rte_Diag.d" \

OBJS += \
./Sources/UDS/Dem_Fls.o \
./Sources/UDS/Rte_Diag.o \

OBJS_QUOTED += \
"./Sources/UDS/Dem_Fls.o" \
"./Sources/UDS/Rte_Diag.o" \

C_DEPS += \
./Sources/UDS/Dem_Fls.d \
./Sources/UDS/Rte_Diag.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/Dem_Fls.o: ../Sources/UDS/Dem_Fls.c
	@echo 'Building file: $<'
	@echo 'Executing target #127 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem_Fls.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem_Fls.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Rte_Diag.o: ../Sources/UDS/Rte_Diag.c
	@echo 'Building file: $<'
	@echo 'Executing target #128 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Rte_Diag.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Rte_Diag.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


