################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/CanTp/CanTp.c" \
"../Sources/UDS/CanTp/CanTp_Cbk.c" \
"../Sources/UDS/CanTp/CanTp_RX.c" \
"../Sources/UDS/CanTp/CanTp_TX.c" \

C_SRCS += \
../Sources/UDS/CanTp/CanTp.c \
../Sources/UDS/CanTp/CanTp_Cbk.c \
../Sources/UDS/CanTp/CanTp_RX.c \
../Sources/UDS/CanTp/CanTp_TX.c \

OBJS_OS_FORMAT += \
./Sources/UDS/CanTp/CanTp.o \
./Sources/UDS/CanTp/CanTp_Cbk.o \
./Sources/UDS/CanTp/CanTp_RX.o \
./Sources/UDS/CanTp/CanTp_TX.o \

C_DEPS_QUOTED += \
"./Sources/UDS/CanTp/CanTp.d" \
"./Sources/UDS/CanTp/CanTp_Cbk.d" \
"./Sources/UDS/CanTp/CanTp_RX.d" \
"./Sources/UDS/CanTp/CanTp_TX.d" \

OBJS += \
./Sources/UDS/CanTp/CanTp.o \
./Sources/UDS/CanTp/CanTp_Cbk.o \
./Sources/UDS/CanTp/CanTp_RX.o \
./Sources/UDS/CanTp/CanTp_TX.o \

OBJS_QUOTED += \
"./Sources/UDS/CanTp/CanTp.o" \
"./Sources/UDS/CanTp/CanTp_Cbk.o" \
"./Sources/UDS/CanTp/CanTp_RX.o" \
"./Sources/UDS/CanTp/CanTp_TX.o" \

C_DEPS += \
./Sources/UDS/CanTp/CanTp.d \
./Sources/UDS/CanTp/CanTp_Cbk.d \
./Sources/UDS/CanTp/CanTp_RX.d \
./Sources/UDS/CanTp/CanTp_TX.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/CanTp/CanTp.o: ../Sources/UDS/CanTp/CanTp.c
	@echo 'Building file: $<'
	@echo 'Executing target #82 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/CanTp/CanTp.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/CanTp/CanTp.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/CanTp/CanTp_Cbk.o: ../Sources/UDS/CanTp/CanTp_Cbk.c
	@echo 'Building file: $<'
	@echo 'Executing target #83 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/CanTp/CanTp_Cbk.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/CanTp/CanTp_Cbk.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/CanTp/CanTp_RX.o: ../Sources/UDS/CanTp/CanTp_RX.c
	@echo 'Building file: $<'
	@echo 'Executing target #84 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/CanTp/CanTp_RX.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/CanTp/CanTp_RX.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/CanTp/CanTp_TX.o: ../Sources/UDS/CanTp/CanTp_TX.c
	@echo 'Building file: $<'
	@echo 'Executing target #85 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/CanTp/CanTp_TX.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/CanTp/CanTp_TX.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


