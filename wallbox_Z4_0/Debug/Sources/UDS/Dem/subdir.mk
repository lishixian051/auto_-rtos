################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/Dem/Dem.c" \
"../Sources/UDS/Dem/Dem_Dcm.c" \
"../Sources/UDS/Dem/Dem_EventDebounce.c" \
"../Sources/UDS/Dem/Dem_EventMemory.c" \
"../Sources/UDS/Dem/Dem_EventQueue.c" \
"../Sources/UDS/Dem/Dem_ExtendedData.c" \
"../Sources/UDS/Dem/Dem_FreezeFrame.c" \
"../Sources/UDS/Dem/Dem_Internal.c" \
"../Sources/UDS/Dem/Dem_J1939.c" \
"../Sources/UDS/Dem/Dem_OBD.c" \

C_SRCS += \
../Sources/UDS/Dem/Dem.c \
../Sources/UDS/Dem/Dem_Dcm.c \
../Sources/UDS/Dem/Dem_EventDebounce.c \
../Sources/UDS/Dem/Dem_EventMemory.c \
../Sources/UDS/Dem/Dem_EventQueue.c \
../Sources/UDS/Dem/Dem_ExtendedData.c \
../Sources/UDS/Dem/Dem_FreezeFrame.c \
../Sources/UDS/Dem/Dem_Internal.c \
../Sources/UDS/Dem/Dem_J1939.c \
../Sources/UDS/Dem/Dem_OBD.c \

OBJS_OS_FORMAT += \
./Sources/UDS/Dem/Dem.o \
./Sources/UDS/Dem/Dem_Dcm.o \
./Sources/UDS/Dem/Dem_EventDebounce.o \
./Sources/UDS/Dem/Dem_EventMemory.o \
./Sources/UDS/Dem/Dem_EventQueue.o \
./Sources/UDS/Dem/Dem_ExtendedData.o \
./Sources/UDS/Dem/Dem_FreezeFrame.o \
./Sources/UDS/Dem/Dem_Internal.o \
./Sources/UDS/Dem/Dem_J1939.o \
./Sources/UDS/Dem/Dem_OBD.o \

C_DEPS_QUOTED += \
"./Sources/UDS/Dem/Dem.d" \
"./Sources/UDS/Dem/Dem_Dcm.d" \
"./Sources/UDS/Dem/Dem_EventDebounce.d" \
"./Sources/UDS/Dem/Dem_EventMemory.d" \
"./Sources/UDS/Dem/Dem_EventQueue.d" \
"./Sources/UDS/Dem/Dem_ExtendedData.d" \
"./Sources/UDS/Dem/Dem_FreezeFrame.d" \
"./Sources/UDS/Dem/Dem_Internal.d" \
"./Sources/UDS/Dem/Dem_J1939.d" \
"./Sources/UDS/Dem/Dem_OBD.d" \

OBJS += \
./Sources/UDS/Dem/Dem.o \
./Sources/UDS/Dem/Dem_Dcm.o \
./Sources/UDS/Dem/Dem_EventDebounce.o \
./Sources/UDS/Dem/Dem_EventMemory.o \
./Sources/UDS/Dem/Dem_EventQueue.o \
./Sources/UDS/Dem/Dem_ExtendedData.o \
./Sources/UDS/Dem/Dem_FreezeFrame.o \
./Sources/UDS/Dem/Dem_Internal.o \
./Sources/UDS/Dem/Dem_J1939.o \
./Sources/UDS/Dem/Dem_OBD.o \

OBJS_QUOTED += \
"./Sources/UDS/Dem/Dem.o" \
"./Sources/UDS/Dem/Dem_Dcm.o" \
"./Sources/UDS/Dem/Dem_EventDebounce.o" \
"./Sources/UDS/Dem/Dem_EventMemory.o" \
"./Sources/UDS/Dem/Dem_EventQueue.o" \
"./Sources/UDS/Dem/Dem_ExtendedData.o" \
"./Sources/UDS/Dem/Dem_FreezeFrame.o" \
"./Sources/UDS/Dem/Dem_Internal.o" \
"./Sources/UDS/Dem/Dem_J1939.o" \
"./Sources/UDS/Dem/Dem_OBD.o" \

C_DEPS += \
./Sources/UDS/Dem/Dem.d \
./Sources/UDS/Dem/Dem_Dcm.d \
./Sources/UDS/Dem/Dem_EventDebounce.d \
./Sources/UDS/Dem/Dem_EventMemory.d \
./Sources/UDS/Dem/Dem_EventQueue.d \
./Sources/UDS/Dem/Dem_ExtendedData.d \
./Sources/UDS/Dem/Dem_FreezeFrame.d \
./Sources/UDS/Dem/Dem_Internal.d \
./Sources/UDS/Dem/Dem_J1939.d \
./Sources/UDS/Dem/Dem_OBD.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/Dem/Dem.o: ../Sources/UDS/Dem/Dem.c
	@echo 'Building file: $<'
	@echo 'Executing target #117 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_Dcm.o: ../Sources/UDS/Dem/Dem_Dcm.c
	@echo 'Building file: $<'
	@echo 'Executing target #118 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_Dcm.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_Dcm.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_EventDebounce.o: ../Sources/UDS/Dem/Dem_EventDebounce.c
	@echo 'Building file: $<'
	@echo 'Executing target #119 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_EventDebounce.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_EventDebounce.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_EventMemory.o: ../Sources/UDS/Dem/Dem_EventMemory.c
	@echo 'Building file: $<'
	@echo 'Executing target #120 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_EventMemory.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_EventMemory.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_EventQueue.o: ../Sources/UDS/Dem/Dem_EventQueue.c
	@echo 'Building file: $<'
	@echo 'Executing target #121 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_EventQueue.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_EventQueue.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_ExtendedData.o: ../Sources/UDS/Dem/Dem_ExtendedData.c
	@echo 'Building file: $<'
	@echo 'Executing target #122 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_ExtendedData.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_ExtendedData.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_FreezeFrame.o: ../Sources/UDS/Dem/Dem_FreezeFrame.c
	@echo 'Building file: $<'
	@echo 'Executing target #123 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_FreezeFrame.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_FreezeFrame.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_Internal.o: ../Sources/UDS/Dem/Dem_Internal.c
	@echo 'Building file: $<'
	@echo 'Executing target #124 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_Internal.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_Internal.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_J1939.o: ../Sources/UDS/Dem/Dem_J1939.c
	@echo 'Building file: $<'
	@echo 'Executing target #125 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_J1939.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_J1939.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dem/Dem_OBD.o: ../Sources/UDS/Dem/Dem_OBD.c
	@echo 'Building file: $<'
	@echo 'Executing target #126 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dem/Dem_OBD.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dem/Dem_OBD.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


