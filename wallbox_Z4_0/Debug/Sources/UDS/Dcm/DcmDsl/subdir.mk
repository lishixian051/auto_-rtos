################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.c" \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.c" \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.c" \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.c" \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.c" \
"../Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.c" \

C_SRCS += \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.c \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.c \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.c \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.c \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.c \
../Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.c \

OBJS_OS_FORMAT += \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.o \

C_DEPS_QUOTED += \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.d" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.d" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.d" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.d" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.d" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.d" \

OBJS += \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.o \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.o \

OBJS_QUOTED += \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.o" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.o" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.o" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.o" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.o" \
"./Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.o" \

C_DEPS += \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.d \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.d \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.d \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.d \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.d \
./Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #88 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_CommManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #89 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_MsgManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #90 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_PendingManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #91 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_ProtocolManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #92 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_SecurityManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.o: ../Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.c
	@echo 'Building file: $<'
	@echo 'Executing target #93 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsl/DcmDsl_SessionManage.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


