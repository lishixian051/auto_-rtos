################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/UDS/Dcm/DcmDsp/DcmDsp.c" \

C_SRCS += \
../Sources/UDS/Dcm/DcmDsp/DcmDsp.c \

OBJS_OS_FORMAT += \
./Sources/UDS/Dcm/DcmDsp/DcmDsp.o \

C_DEPS_QUOTED += \
"./Sources/UDS/Dcm/DcmDsp/DcmDsp.d" \

OBJS += \
./Sources/UDS/Dcm/DcmDsp/DcmDsp.o \

OBJS_QUOTED += \
"./Sources/UDS/Dcm/DcmDsp/DcmDsp.o" \

C_DEPS += \
./Sources/UDS/Dcm/DcmDsp/DcmDsp.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/UDS/Dcm/DcmDsp/DcmDsp.o: ../Sources/UDS/Dcm/DcmDsp/DcmDsp.c
	@echo 'Building file: $<'
	@echo 'Executing target #94 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/UDS/Dcm/DcmDsp/DcmDsp.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/UDS/Dcm/DcmDsp/DcmDsp.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


