################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Xcp/Xcp.c" \
"../Sources/Xcp/Xcp_Cal.c" \
"../Sources/Xcp/Xcp_Daq.c" \
"../Sources/Xcp/Xcp_Interface.c" \
"../Sources/Xcp/Xcp_Pgm.c" \
"../Sources/Xcp/Xcp_Ram.c" \
"../Sources/Xcp/Xcp_Std.c" \

C_SRCS += \
../Sources/Xcp/Xcp.c \
../Sources/Xcp/Xcp_Cal.c \
../Sources/Xcp/Xcp_Daq.c \
../Sources/Xcp/Xcp_Interface.c \
../Sources/Xcp/Xcp_Pgm.c \
../Sources/Xcp/Xcp_Ram.c \
../Sources/Xcp/Xcp_Std.c \

OBJS_OS_FORMAT += \
./Sources/Xcp/Xcp.o \
./Sources/Xcp/Xcp_Cal.o \
./Sources/Xcp/Xcp_Daq.o \
./Sources/Xcp/Xcp_Interface.o \
./Sources/Xcp/Xcp_Pgm.o \
./Sources/Xcp/Xcp_Ram.o \
./Sources/Xcp/Xcp_Std.o \

C_DEPS_QUOTED += \
"./Sources/Xcp/Xcp.d" \
"./Sources/Xcp/Xcp_Cal.d" \
"./Sources/Xcp/Xcp_Daq.d" \
"./Sources/Xcp/Xcp_Interface.d" \
"./Sources/Xcp/Xcp_Pgm.d" \
"./Sources/Xcp/Xcp_Ram.d" \
"./Sources/Xcp/Xcp_Std.d" \

OBJS += \
./Sources/Xcp/Xcp.o \
./Sources/Xcp/Xcp_Cal.o \
./Sources/Xcp/Xcp_Daq.o \
./Sources/Xcp/Xcp_Interface.o \
./Sources/Xcp/Xcp_Pgm.o \
./Sources/Xcp/Xcp_Ram.o \
./Sources/Xcp/Xcp_Std.o \

OBJS_QUOTED += \
"./Sources/Xcp/Xcp.o" \
"./Sources/Xcp/Xcp_Cal.o" \
"./Sources/Xcp/Xcp_Daq.o" \
"./Sources/Xcp/Xcp_Interface.o" \
"./Sources/Xcp/Xcp_Pgm.o" \
"./Sources/Xcp/Xcp_Ram.o" \
"./Sources/Xcp/Xcp_Std.o" \

C_DEPS += \
./Sources/Xcp/Xcp.d \
./Sources/Xcp/Xcp_Cal.d \
./Sources/Xcp/Xcp_Daq.d \
./Sources/Xcp/Xcp_Interface.d \
./Sources/Xcp/Xcp_Pgm.d \
./Sources/Xcp/Xcp_Ram.d \
./Sources/Xcp/Xcp_Std.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Xcp/Xcp.o: ../Sources/Xcp/Xcp.c
	@echo 'Building file: $<'
	@echo 'Executing target #132 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Cal.o: ../Sources/Xcp/Xcp_Cal.c
	@echo 'Building file: $<'
	@echo 'Executing target #133 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Cal.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Cal.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Daq.o: ../Sources/Xcp/Xcp_Daq.c
	@echo 'Building file: $<'
	@echo 'Executing target #134 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Daq.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Daq.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Interface.o: ../Sources/Xcp/Xcp_Interface.c
	@echo 'Building file: $<'
	@echo 'Executing target #135 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Interface.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Interface.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Pgm.o: ../Sources/Xcp/Xcp_Pgm.c
	@echo 'Building file: $<'
	@echo 'Executing target #136 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Pgm.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Pgm.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Ram.o: ../Sources/Xcp/Xcp_Ram.c
	@echo 'Building file: $<'
	@echo 'Executing target #137 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Ram.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Ram.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/Xcp/Xcp_Std.o: ../Sources/Xcp/Xcp_Std.c
	@echo 'Building file: $<'
	@echo 'Executing target #138 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	powerpc-eabivle-gcc "@Sources/Xcp/Xcp_Std.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "Sources/Xcp/Xcp_Std.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


